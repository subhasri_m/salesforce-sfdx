trigger Align_Contact_Owner_with_Account on Contact (before update,before insert, after update, after Insert) {
    public static Boolean runOnce = true; // prevent recursion
    if (trigger.isBefore && (trigger.isinsert || trigger.isUpdate)){
        for (Contact c : Trigger.new)
        {
            c.ownerid = c.Account_Owner_ID__c;
        }    
    }
    
    if (runOnce && trigger.isAfter && trigger.isUpdate && Trigger.new.size() == 1){// no bulk processing; will only run from the UI
        runOnce = false;
        Contact newCon = Trigger.new[0]; 
        if (Trigger.old[0].User_Enabled__c != newCon.User_Enabled__c && newCon.User_Enabled__c ){
            List<User> usrlst = [Select id from user where username =: newCon.Email];
            if(!usrlst.isEmpty()){
                newCon.Email.addError('Email already exist: Please modify email');
            }
            CCFPUserUtil.upsertUser(newCon.Id,newCon.User_Enabled__c);    
        }else if (Trigger.old[0].User_Enabled__c != newCon.User_Enabled__c && !newCon.User_Enabled__c){
            CCFPUserUtil.upsertUser(newCon.Id,newCon.User_Enabled__c);
            CCFPUserUtil.deleteCAPM(newCon.Id);
        }
        
    }
    
    if (runOnce && trigger.isAfter && trigger.isInsert && Trigger.new.size() == 1){// no bulk processing; will only run from the UI
        runOnce = false;
        Contact newCon = Trigger.new[0];
        if (newCon.User_Enabled__c){
            	List<User> usrlst = [Select id from user where username =: newCon.Email];
            	if(!usrlst.isEmpty()){
                newCon.Email.addError('Email already exist: Please modify email');
            }
                CCFPUserUtil.upsertUser(newCon.Id,newCon.User_Enabled__c);    
        }       
    }
}