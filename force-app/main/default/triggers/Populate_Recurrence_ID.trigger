trigger Populate_Recurrence_ID on Task (before insert, before update) {

    for (Task T : Trigger.new)
    {
        t.Recurring_Task_ID__c = t.RecurrenceActivityId;
        t.Recurrence_End_Date__c = t.RecurrenceEndDateOnly;
        t.due_date_trigger__c = t.ActivityDate;
}}