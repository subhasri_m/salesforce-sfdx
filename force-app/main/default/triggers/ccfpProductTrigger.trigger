trigger ccfpProductTrigger on ccrz__E_Product__c (before update) {
    if(trigger.isBefore && trigger.isUpdate){
        if(!UserInfo.getUserId().equalsIgnoreCase(Label.SEOManager)) {
            for (ccrz__E_Product__c prod : trigger.new){
                if (trigger.OldMap.get(prod.id).ccrz__SEOId__c !=null ){
                    prod.ccrz__SEOId__c = trigger.OldMap.get(prod.id).ccrz__SEOId__c;
                }
            }
        }
        
    }
    
}