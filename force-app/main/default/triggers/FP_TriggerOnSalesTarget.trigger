/*
 Name : FP_TriggerOnSalesTarget
 Description: Trigger on SalesTarget Object
 Owner : Fleet Pride
 Modified By: lalit Arora
*/ 

trigger FP_TriggerOnSalesTarget on Sales_Target__c  (before insert,before update) {

        SalesTargetHelper var = new SalesTargetHelper(trigger.New);      
}