trigger Ride_Along_Trigger on Ride_Along__c (before update, before insert, after update,after insert) {
    if (trigger.isInsert && trigger.isAfter){
        Handler_Ride_Along_Trigger.shareRecordWithManager(trigger.new);
    }
    if (trigger.isUpdate && trigger.isAfter){
        for (Ride_Along__c RAnew: trigger.new){
        if (RAnew.Sales_Call_Complete__c ==true && RAnew.Sales_Call_Complete__c!= trigger.oldMap.get(RAnew.id).Sales_Call_Complete__c)
        	Handler_Ride_Along_Trigger.processEmailWithAttach(String.ValueOf(RAnew.Id));
            break; // It is to prevent bulk emails.
        }
    }
}