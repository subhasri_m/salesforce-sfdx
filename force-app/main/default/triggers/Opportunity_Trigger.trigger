trigger Opportunity_Trigger on Opportunity (before update, after update) {
    Map<String, String> mapOppIDNAMgroupID = new Map<String, String>(); // map of OpportunityID with National_Account_group number
    List<Opportunity> NAMopp = new List<Opportunity>();  // list of all NAM Header Opportunities 
    if (trigger.isUpdate && trigger.isBefore){
        for (Opportunity opp : Trigger.new){
            if (!System.isbatch() && opp.StageName == '90-Closed Lost' && Trigger.OldMap.get(opp.id).StageName != '90-Closed Lost' ){
                opp.CloseDate= System.Today();
            }
        }
    }
    if (trigger.isUpdate && trigger.isAfter){
        for (Opportunity opp : [select Id,Amount,Account.AccountNumber,Account.National_Account_Owner__c,StageName, Account.National_Account_Group__c,Account.ParentId,RecordTypeId from Opportunity where Id =: Trigger.new]){
            Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('National Header Account').getRecordTypeId();
            if (test.isRunningTest()){
               mapOppIDNAMgroupID.put(opp.id,Opp.Account.National_Account_Group__c);
                NAMopp.add(opp); 
            }
            if (!System.isbatch() && opp.StageName == '50-Closed Won' && Trigger.OldMap.get(opp.id).StageName != '50-Closed Won' && opp.RecordTypeId == recTypeId 	&& Opp.Account.National_Account_Group__c != null && Opp.Account.AccountNumber ==null && opp.Account.National_Account_Owner__c != 'Not Assigned'  ){
               system.debug('Inside main block');
                mapOppIDNAMgroupID.put(opp.id,Opp.Account.National_Account_Group__c);
                NAMopp.add(opp);
            }
        }
        if (!NAMopp.isEmpty() && NAMopp != null){
            String NAMoppJSON = JSON.serialize(NAMopp);
    		String newMapJSON = JSON.serialize(Trigger.newMap);
            Opportunity_TriggerHelper.createChildOppotunities(mapOppIDNAMgroupID,NAMoppJSON,newMapJSON); // create same opp for all child NAM accounts
        }
    }
    
}