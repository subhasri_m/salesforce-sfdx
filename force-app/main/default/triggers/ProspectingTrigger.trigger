trigger ProspectingTrigger on Prospecting__c (before insert, after insert, before update) {
    
    if(Trigger.isBefore){ // && Trigger.isInsert){
        ProspectingTriggerHandler.onBeforeInsert(Trigger.new);
    }
    
    if(Trigger.isAfter && Trigger.isInsert){
        ProspectingTriggerHandler.onAfterInsert(Trigger.new);
    }
    
    

}