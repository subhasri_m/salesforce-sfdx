/*
    Description: Trigger consolidation for Task Object
    Author: Alvin Claridad
    History:
    --------------------------------------------------------------------------
    05.26.2015          Alvin Claridad(Cloud Sherpas)          Created
    06.19.2015          Sid Pajutrao(Cloud Sherpas)            Created - Before Delete
*/

trigger TaskTrigger on Task (before insert, after insert, before update, after update, after delete, before delete) {
    
       if(trigger.isBefore && trigger.isUpdate){
        //Added By Lalit
        tasktriggerHandler.populateRelatedAccountInfo(trigger.new);
          if (!tasktriggerHandler.IsRecursive)
        tasktriggerHandler.updateLatestFiveCompltActivitiesFlagOnUpdate(trigger.new, Trigger.OldMap);
       }
  
    //Added by Sid Pajutrao 06.19.2015
    if(trigger.isBefore && trigger.isDelete){
        //Added By Lalit
        TaskTriggerHandler.CheckMonthlyCallonDelete(trigger.Old);
        
        for(Task t : trigger.Old){
            if(t.Type == 'Sales Call' && t.IsRecurrence == True){
                TaskTriggerHandler.DepopulateAccountRecussrenceEndDate(trigger.OldMap);
            }
        }
    }
 
    if(trigger.isAfter && trigger.isInsert){
        System.debug('@@@@@@@@@@@@@  insert');
        TaskTriggerHandler.PopulateEndOfRecurrence(trigger.new, trigger.OldMap);
        TaskTriggerHandler.CheckMonthlyCallonInsert(trigger.new); 
    }
    
    if (trigger.isAfter && trigger.isUpdate) {
        System.debug('@@@@@@@@@@@@@ after insert');

        TaskTriggerHandler.updateDaysSinceLastSalesCall(trigger.new, trigger.OldMap); 
        TaskTriggerHandler.populateNumberOfSalesCall(trigger.new);  
        //Added by Lalit
        TaskTriggerHandler.CheckMonthlyCallonUpdate(trigger.new, trigger.OldMap);
       
    }
    
    if(trigger.isBefore && trigger.isInsert){
        System.debug('@@@@@@@@@@@@@ before insert');
        TaskTriggerHandler.populateNumberOfSalesCall(trigger.new);
        TaskTriggerHandler.populateRelatedAccountInfo(trigger.new);
        TaskTriggerHandler.updateLatestFiveCompltActivitiesFlagOnInsert(Trigger.new);
    } 
    
}