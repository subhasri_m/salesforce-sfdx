trigger TriggerOnSiteRequestHandler on SiteRequestHandler__c (after insert) {
    
    if(trigger.isinsert)
    {
    TriggerOnSiteRequestHandler_utility Handler = new TriggerOnSiteRequestHandler_utility();
    Handler.InsertCaseOnVisitorQuery(Trigger.new);

    }
}