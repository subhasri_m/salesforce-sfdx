trigger Update_Account_Owner on Account (before update) {

Set<String> salesmanIdSet = new Set<String>();

    for(Account act : Trigger.new){
    If (act.accountnumber == Null) {return;}
        if(trigger.oldmap.get(act.id).Salesman_Number__c != act.Salesman_Number__c && act.Salesman_Number__c !=Null ){
        
            salesmanIdSet.add(act.Salesman_Number__c) ;
        }
    }
    
    Map<String,User> salesmanIdToUser = new Map<String,User>();
    
    for(User u : [SELECT Id,Name, Salesman_Number__c FROM User WHERE Salesman_Number__c IN : salesmanIdSet])
    {
        salesmanIdToUser.put(u.Salesman_Number__c, u) ;
    }
    
    List<User> defaultUser = [SELECT Id, Name FROM User WHERE Username = 'secure.agent@fleetpride.com.uat' LIMIT 1] ; // Provide user name on ABC@XYZ which you want as defaultUser if salesmanId not found.
    
    for(Account act : Trigger.new)
    {
        if(salesmanIdToUser.get(act.Salesman_Number__c)!=null){
            act.OwnerID = salesmanIdToUser.get(act.Salesman_Number__c).Id ;
        
        }else if(act.Salesman_Number__c == null && defaultUser.size() > 0  && act.Iseries_company_code__c != '2') {
            act.OwnerID = defaultUser[0].Id ;
        }
        
    }}