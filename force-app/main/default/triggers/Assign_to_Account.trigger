trigger Assign_to_Account on Phone_call__c (before update) {

Set<String> AcctIdSet = new Set<String>();

    for(Phone_Call__c phone_call: Trigger.new){
    If (phone_call.Account__c == Null) {return;}
        if(phone_call.Account__c != NULL){
        
            AcctIdSet.add(phone_call.Account_Number__c) ;
        }
    }
    
    Map<String,Account> AccountNumberToAccount = new Map<String,Account>();
    
    for(Account act : [SELECT Id, AccountNumber FROM Account WHERE AccountNumber IN : AcctIdSet])
    {
        AccountNumberToAccount.put(act.AccountNumber, act) ;
    }
    
    
    for(Phone_Call__c phone_call: Trigger.new)
    {
        if(AccountNumberToAccount.get(phone_call.Account_Number__c) != NULL){
            phone_call.Account_Number__c = AccountNumberToAccount.get(phone_call.Account_Number__c).Id ;
        
        }
        
    }}