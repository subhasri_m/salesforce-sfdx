trigger PopNumberOfOSRTrigger on User (before insert, before update) {
    
    if(trigger.isBefore && trigger.isInsert){
        System.debug('@@@@@@@@@@@@@ Insert');
        PopNumberOfOSRTrigger_Handler.populateNoOSRField(trigger.New);
    }
    
    if(trigger.isBefore && trigger.isUpdate){
        System.debug('@@@@@@@@@@@@@ Update');
       PopNumberOfOSRTrigger_Handler.populateNoOSRField(trigger.New);
    }
        
}