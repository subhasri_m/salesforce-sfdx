trigger SalesTargetAccount on Account (after update) {
    if(trigger.isAfter){
        if (trigger.isUpdate) AccountTriggerHandler.syncSalesOrderSummary(trigger.oldMap, trigger.new);
    }
}