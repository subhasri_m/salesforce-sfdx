/**
* @description     FP_TriggerOnLiveAgentFeedback
* @author          Lalit Arora
* @Company         FP
* @date            29.Nov.2017
*
* HISTORY
* - 29.Nov.2016    Lalit      Created.
*/ 


trigger FP_TriggerOnLiveAgentFeedback on Live_Agent_Feedback__c (after insert,after Update) {
    
 //  FP_TriggerOnLiveAgentFeedbackHandler Handler = new FP_TriggerOnLiveAgentFeedbackHandler();
 //   
 //   if (trigger.isInsert && Trigger.isafter)
 //   {
 //       Handler.UpdateChatInfo(Trigger.new);          
 //   } 
}