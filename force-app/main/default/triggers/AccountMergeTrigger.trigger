trigger AccountMergeTrigger on Account_Merge_Request__c (before insert, after insert, before update, after update, after delete, before delete) {
    if(trigger.isBefore && trigger.isInsert){
    }
    
    if(trigger.isBefore && trigger.isUpdate){    
     AccountMergeTriggerHandler.AccountMergeRequestTriggerHandler(trigger.new, trigger.OldMap);
    }
  

    if(trigger.isBefore && trigger.isDelete){

    }
 
    if(trigger.isAfter && trigger.isInsert){
       
    }
    
    if (trigger.isAfter && trigger.isUpdate) {
      //  AccountMergeTriggerHandler.AccountMergeRequestTriggerHandler(trigger.new, trigger.OldMap);
      System.Debug('>>>>' + trigger.new);
    }
}