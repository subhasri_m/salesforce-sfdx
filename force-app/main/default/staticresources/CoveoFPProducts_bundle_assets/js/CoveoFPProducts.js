(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("Coveo"), require("_"));
	else if(typeof define === 'function' && define.amd)
		define(["Coveo", "_"], factory);
	else if(typeof exports === 'object')
		exports["CoveoFleetPrideExtension"] = factory(require("Coveo"), require("_"));
	else
		root["CoveoFleetPrideExtension"] = factory(root["Coveo"], root["_"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE__0__, __WEBPACK_EXTERNAL_MODULE__2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 67);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__0__;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

(function webpackUniversalModuleDefinition(root, factory) {
	if(true)
		module.exports = factory(__webpack_require__(0));
	else {}
})(window, function(__WEBPACK_EXTERNAL_MODULE__1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.component = void 0;
var coveo_search_ui_1 = __webpack_require__(1);
function component(constructor) {
    coveo_search_ui_1.Initialization.registerAutoCreateComponent(constructor);
    return constructor;
}
exports.component = component;


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__1__;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(3);


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(__webpack_require__(4), exports);
__exportStar(__webpack_require__(6), exports);


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(__webpack_require__(0), exports);
__exportStar(__webpack_require__(5), exports);


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.lazyComponent = exports.lazyDependentComponent = void 0;
var coveo_search_ui_1 = __webpack_require__(1);
var component_1 = __webpack_require__(0);
function lazyDependentComponent(dependentComponentId) {
    return function (constructor) {
        if (!coveo_search_ui_1.LazyInitialization) {
            return component_1.component(constructor);
        }
        coveo_search_ui_1.LazyInitialization.registerLazyComponent(constructor.ID, function () {
            return coveo_search_ui_1.load(dependentComponentId).then(function () { return component_1.component(constructor); });
        });
        return constructor;
    };
}
exports.lazyDependentComponent = lazyDependentComponent;
function lazyComponent(constructor) {
    if (!coveo_search_ui_1.LazyInitialization) {
        return component_1.component(constructor);
    }
    coveo_search_ui_1.LazyInitialization.registerLazyComponent(constructor.ID, component_1.component(constructor));
    return constructor;
}
exports.lazyComponent = lazyComponent;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(__webpack_require__(7), exports);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// Webpack output a library target with a temporary name.
// It does not take care of merging the namespace if the global variable already exists.
// If another piece of code in the page use the Coveo namespace (eg: extension), then they get overwritten
// This code swap the current module to the "real" Coveo variable, without overwriting the whole global var.
// This is to allow end user to put CoveoPSComponents.js before or after the main CoveoJsSearch.js, without breaking
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.swapVar = void 0;
function swapVar(scope) {
    if (!window['Coveo']) {
        window['Coveo'] = scope;
        return;
    }
    window['Coveo'] = __assign(__assign({}, scope), window['Coveo']);
}
exports.swapVar = swapVar;


/***/ })
/******/ ]);
});

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__2__;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.SVGIcons = void 0;
var SVGIcons = /** @class */ (function () {
    function SVGIcons() {
    }
    SVGIcons.icons = {
        search: __webpack_require__(12),
        eye: __webpack_require__(13),
        checkboxHookExclusionMore: __webpack_require__(14),
        loading: __webpack_require__(15),
        star: __webpack_require__(16),
        mainClear: __webpack_require__(17),
        arrowUp: __webpack_require__(18),
        arrowDown: __webpack_require__(19),
        increment: __webpack_require__(20),
        decrement: __webpack_require__(21),
        branch: __webpack_require__(22),
        filter: __webpack_require__(23),
        check: __webpack_require__(24),
        plus: __webpack_require__(25),
        info: __webpack_require__(26),
        localDelivery: __webpack_require__(27),
        shippingBox: __webpack_require__(28),
        listLayout: __webpack_require__(29),
        cardLayout: __webpack_require__(30),
        exclamationCircle: __webpack_require__(31),
    };
    return SVGIcons;
}());
exports.SVGIcons = SVGIcons;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomEvents = void 0;
var CustomEvents = /** @class */ (function () {
    function CustomEvents() {
    }
    // Global product events
    CustomEvents.updateProductInfo = 'updateProductInfo';
    CustomEvents.invalidProductInfo = 'invalidProductInfo';
    // Inventory events
    // static updateInventoryInfo = 'updateInventoryInfo';
    // static invalidBranchInventory = 'invalidBranchInventory';
    // Price events
    // static udpateCustomerPrice = 'udpateResultTemplatePrice';
    // static invalidBranchPrice = 'invalidBranchPrice';
    // Priduct Category
    CustomEvents.ProductCategoryChange = 'ProductCategoryChange';
    // Vehicle Fit events
    CustomEvents.vehicleFitSearch = 'vehicleFitSearch';
    CustomEvents.vehicleFitSelect = 'vehicleFitSelect';
    CustomEvents.vehicleFitClear = 'vehicleFitClear';
    // VIN search events
    CustomEvents.vehicleVinSearch = 'vehicleVinSearch';
    CustomEvents.vehicleVinFound = 'vehicleVinFound';
    // Dynamic Facet Generator events
    CustomEvents.updateFacetDictionary = 'updateFacetDictionary';
    // Branch Location events
    CustomEvents.branchLocationChange = 'branchLocationChange';
    // Shipping method events
    CustomEvents.changeShippingMethod = 'changeShippingMethod';
    // Add to cart events
    CustomEvents.addToCartCountChange = 'addToCartCountChange';
    CustomEvents.addToCartRemoteAction = 'addToCartRemoteAction';
    return CustomEvents;
}());
exports.CustomEvents = CustomEvents;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.CoveoUtils = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var CoveoUtils = /** @class */ (function () {
    function CoveoUtils() {
    }
    /**
     * Gets the current FieldValue from the current {@link IQueryResult}.
     *
     * @returns {any} The current FieldValue or an empty string.
     */
    CoveoUtils.getCleanFieldValue = function (result, field) {
        var value = coveo_search_ui_1.Utils.getFieldValue(result, field);
        if (!_.isArray(value) && _.isObject(value)) {
            value = '';
        }
        return value || '';
    };
    /**
     * Gets the current FieldValue from the current {@link IQueryResult} as a string.
     * If the field has multiple values, they are joined into one single string
     *
     * @returns {string} The current FieldValue as a string
     */
    CoveoUtils.getCleanSingleStringFieldValue = function (result, field) {
        var value = CoveoUtils.getCleanFieldValue(result, field);
        if (!value) {
            // the current result has no part number
            return null;
        }
        if (Array.isArray(value) && value.length > 0) {
            return value.join(';');
        }
        else {
            return value.toLowerCase();
        }
    };
    return CoveoUtils;
}());
exports.CoveoUtils = CoveoUtils;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.SVGDom = void 0;
var SVGDom = /** @class */ (function () {
    function SVGDom() {
    }
    SVGDom.addClassToSVGInContainer = function (svgContainer, classToAdd) {
        try {
            var svgElement = svgContainer.querySelector('svg');
            svgElement.setAttribute('class', "" + SVGDom.getClass(svgElement) + classToAdd);
        }
        catch (error) {
            console.log(error);
        }
    };
    SVGDom.removeClassFromSVGInContainer = function (svgContainer, classToRemove) {
        var svgElement = svgContainer.querySelector('svg');
        svgElement.setAttribute('class', SVGDom.getClass(svgElement).replace(classToRemove, ''));
    };
    SVGDom.addStyleToSVGInContainer = function (svgContainer, styleToAdd) {
        var svgElement = svgContainer.querySelector('svg');
        _.each(styleToAdd, function (styleValue, styleKey) {
            svgElement.style[styleKey] = styleValue;
        });
    };
    SVGDom.getClass = function (svgElement) {
        var className = svgElement.getAttribute('class');
        return className ? className + ' ' : '';
    };
    return SVGDom;
}());
exports.SVGDom = SVGDom;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ItemPrice = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var CoveoUtils_1 = __webpack_require__(5);
var CustomEvents_1 = __webpack_require__(4);
var underscore_1 = __webpack_require__(2);
var SVGIcons_1 = __webpack_require__(3);
var turbo_core_1 = __webpack_require__(1);
var ItemPrice = /** @class */ (function (_super) {
    __extends(ItemPrice, _super);
    /**
     * Creates a new `ItemPrice` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `ItemPrice` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     * @param result The result to associate the component with.
     */
    function ItemPrice(element, options, bindings, result) {
        var _this = _super.call(this, element, ItemPrice_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.result = result;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, ItemPrice_1, options);
        _this.showPlaceholder();
        _this.bind.oneRootElement(CustomEvents_1.CustomEvents.updateProductInfo, function (args) {
            _this.handleUdpateProduct(args);
        });
        _this.bind.oneRootElement(CustomEvents_1.CustomEvents.invalidProductInfo, function (err) {
            _this.handleInvalidProductInfo();
        });
        return _this;
    }
    ItemPrice_1 = ItemPrice;
    ItemPrice.prototype.findPriceFromList = function (prices, key) {
        if (prices && prices.length > 0 && key) {
            var match = underscore_1.findWhere(prices, { PartNumber: ("" + key).toUpperCase() });
            return match ? match.PRICE : null;
        }
        else {
            // this.logger.error(`Unable to find key "${key}" in the results`);
            return null;
        }
    };
    ItemPrice.prototype.handleInvalidProductInfo = function () {
        this.buildNoPriceAvailable();
    };
    ItemPrice.prototype.isMultiPack = function () {
        return this.getSalesPackNumber() > 1;
    };
    ItemPrice.prototype.getSalesPackNumber = function () {
        var salesPack = CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, this.options.salespackField);
        // The field was created as a string instead of a number
        return salesPack === '' ? null : parseInt(salesPack, 10);
    };
    ItemPrice.prototype.handleUdpateProduct = function (args) {
        var prices = args.prices;
        var price = this.findPriceFromList(prices, CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, args.options.partNumberField));
        var coreCharge = this.findPriceFromList(prices, CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, args.options.corePartNumberField));
        if (price) {
            if (price === ItemPrice_1.DEFAULT_VALUE) {
                // The Pricing api return a value of ItemPrice.DEFAULT_VALUE if the price is not available for this branch
                this.logger.warn('Price unavailable for this region');
                this.buildNoPriceAvailable();
            }
            else {
                this.redrawPrices(price, coreCharge);
            }
        }
        else {
            this.buildNoPriceAvailable();
        }
    };
    ItemPrice.prototype.showPlaceholder = function () {
        var container = coveo_search_ui_1.$$('div', { className: 'item-placeholder-container' });
        container.append(coveo_search_ui_1.$$('div', { className: 'item-placeholder', style: 'height:16px;width:54px;margin:3px 0;' }).el);
        container.append(coveo_search_ui_1.$$('div', { className: 'item-placeholder', style: 'height:24px;width:74px;margin:5px 0;' }).el);
        this.element.appendChild(container.el);
    };
    ItemPrice.prototype.shouldShowPrice = function (price) {
        // Do not show price if inferior or equal than the list price
        var listPrice = CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, this.options.listPriceField);
        var condition = listPrice && !Number.isNaN(listPrice) ? price <= listPrice : true;
        return condition;
    };
    ItemPrice.prototype.buildNoPriceAvailable = function () {
        this.clearElement();
        if (this.options.mobile) {
            this.element.appendChild(this.buildTextForMobile());
        }
        else {
            this.element.appendChild(this.buildText());
        }
        this.element.classList.remove('text-align-right');
    };
    ItemPrice.prototype.getBranchInfo = function () {
        return this.options.getBranchInfo();
    };
    ItemPrice.prototype.buildTextForMobile = function () {
        var element = coveo_search_ui_1.$$('span', { className: 'coveops-item-no-price' });
        var branchInfo = this.getBranchInfo();
        if (branchInfo || branchInfo.city || branchInfo.region || branchInfo.local_phone) {
            element.append(coveo_search_ui_1.$$('p', {}, "Pricing unavailable for this item. Contact your local branch <a href=\"tel:" + branchInfo.local_phone + "\">" + branchInfo.local_phone + "</a> or chat for price.").el); // TODO: add option instead of hard coding text
        }
        return element.el;
    };
    ItemPrice.prototype.buildText = function () {
        return underscore_1.isFunction(this.options.buildText) ? this.options.buildText() : this.buildTextNative();
    };
    ItemPrice.prototype.buildTextNative = function () {
        var element = coveo_search_ui_1.$$('span', { className: 'coveops-item-no-price' });
        var branchInfo = this.getBranchInfo();
        if (branchInfo || branchInfo.city || branchInfo.region || branchInfo.local_phone) {
            // Make sure to not return an invalid text
            element.append(coveo_search_ui_1.$$('p', {}, this.options.unablePricingCaption).el);
        }
        if (branchInfo && branchInfo.city && branchInfo.region && branchInfo.local_phone) {
            var text = coveo_search_ui_1.$$('p');
            text.setHtml("For pricing please call " + branchInfo.city + " " + branchInfo.region + " branch: <a href=\"tel:" + branchInfo.local_phone + "\">" + branchInfo.local_phone + "</a>");
            element.append(text.el);
            element.append(coveo_search_ui_1.$$('p', {}, 'Or Chat below').el);
        }
        return element.el;
    };
    ItemPrice.prototype.clearElement = function () {
        this.element.innerHTML = '';
    };
    ItemPrice.prototype.redrawPrices = function (price, coreCharge) {
        this.clearElement();
        this.buildListPrice(price);
        this.buildDisplayedPrice(price);
        if (!this.options.mobile) {
            this.buildCoreCharge(coreCharge);
        }
    };
    ItemPrice.prototype.buildListPrice = function (price) {
        var listPriceValue = CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, this.options.listPriceField);
        if (listPriceValue && this.shouldShowPrice(price)) {
            this.element.appendChild(coveo_search_ui_1.$$('div', { className: 'coveops-item-list-price' }, this.formatPrice(listPriceValue)).el);
        }
    };
    ItemPrice.prototype.buildDisplayedPrice = function (price) {
        if (this.isMultiPack() && this.getSalesPackNumber() > 1) {
            var packPrice = coveo_search_ui_1.$$('div', { className: 'coveops-item-displayed-price' }, this.formatPrice(this.getMultiPackPrice(price)));
            packPrice.append(coveo_search_ui_1.$$('span', {}, " (" + this.formatPrice(price) + " each)").el);
            this.element.appendChild(packPrice.el);
        }
        else {
            this.element.appendChild(coveo_search_ui_1.$$('div', { className: 'coveops-item-displayed-price' }, this.formatPrice(price)).el);
        }
    };
    ItemPrice.prototype.buildCoreCharge = function (coreCharge) {
        if (coreCharge) {
            var coreChargeElement = coveo_search_ui_1.$$('div', { className: 'coveops-item-core-charge' });
            var label = coveo_search_ui_1.$$('div', { className: 'coveops-item-core-label' }, this.options.coreChargeCaption);
            var svg = coveo_search_ui_1.$$('span', {}, SVGIcons_1.SVGIcons.icons.info);
            label.append(svg.el);
            coreChargeElement.append(label.el);
            coreChargeElement.append(coveo_search_ui_1.$$('div', { className: 'coveops-item-core-price' }, this.formatPrice(coreCharge)).el);
            this.element.appendChild(coreChargeElement.el);
            try {
                // Since this method depends on Jquery and Bootstrap, we don't want any error to affect the rest of the Coveo execution
                this.attachToolTip(svg.el);
            }
            catch (error) {
                this.logger.error('Something went wrong with while attaching tooltip to the core charge', error);
            }
        }
    };
    ItemPrice.prototype.attachToolTip = function (element) {
        // TODO: load jquery or Bootstrap definitions
        if (window.$ && window.$(element).tooltip && underscore_1.isFunction(window.$(element).tooltip)) {
            var text = "<p>CORE CHARGE</p>\n      <p>A Core Charge is a deposit paid at the time of purchase for parts with recyclable components. The deposit will be refunded when the recyclable component of this item is returned after use.</p>";
            window.$(element).tooltip({ title: text, html: true });
        }
        else {
            this.logger.error('Bootstrap is not available');
        }
        // This is using Bootstrap tooltip
    };
    ItemPrice.prototype.getMultiPackPrice = function (unitPrice) {
        coveo_search_ui_1.Assert.check(underscore_1.isNumber(unitPrice), 'Item price should be a number');
        coveo_search_ui_1.Assert.check(underscore_1.isNumber(this.getSalesPackNumber()), 'Multi pack numbershould be a number');
        return unitPrice * this.getSalesPackNumber();
    };
    ItemPrice.prototype.formatPrice = function (price) {
        if (price) {
            return "" + Coveo.CurrencyUtils.currencyToString(price, { decimals: 2 });
        }
        else {
            return '';
        }
    };
    var ItemPrice_1;
    ItemPrice.ID = 'ItemPrice';
    ItemPrice.DEFAULT_VALUE = 10000;
    ItemPrice.options = {
        /**
         * ???
         *
         */
        listPriceCaption: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'List Price:' }),
        /**
         * ???
         *
         */
        unablePricingCaption: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: coveo_search_ui_1.l('Pricing unavailable for this item:') }),
        /**
         * Specifies the field used for the item' list price
         *
         */
        listPriceField: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_listprice' }),
        /**
         * Specifies the field to determine if an item is sold in multipack
         *
         */
        salespackField: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_salespack' }),
        mobile: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: false }),
        coreChargeCaption: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: '+ Core Charge:' }),
        buildText: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        getBranchInfo: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
    };
    ItemPrice = ItemPrice_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component renders the price information of an item.
         * This component depends on Bootstrap to render the Core Charge tooltip.
         *
         * This component is a result template component (see [Result Templates](https://docs.coveo.com/en/413/)).
         *
         * @export
         * @class ItemPrice
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], ItemPrice);
    return ItemPrice;
}(coveo_search_ui_1.Component));
exports.ItemPrice = ItemPrice;
coveo_search_ui_1.Initialization.registerComponentFields(ItemPrice.ID, ['fp_prd_listprice', 'fp_prd_partnumber']);


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && btoa) {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.CommerceDataLayerEvents = void 0;
var CommerceDataLayerEvents = /** @class */ (function () {
    function CommerceDataLayerEvents() {
    }
    CommerceDataLayerEvents.impression = 'CommerceDataLayerEventImpression';
    CommerceDataLayerEvents.productClick = 'CommerceDataLayerEventProductClick';
    CommerceDataLayerEvents.productDetail = 'CommerceDataLayerEventProductDetail';
    CommerceDataLayerEvents.addToCart = 'CommerceDataLayerEventAddToCart';
    CommerceDataLayerEvents.removeFromCart = 'CommerceDataLayerEventRemoveFromCart';
    return CommerceDataLayerEvents;
}());
exports.CommerceDataLayerEvents = CommerceDataLayerEvents;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.KEYBOARD = void 0;
var KEYBOARD;
(function (KEYBOARD) {
    KEYBOARD[KEYBOARD["BACKSPACE"] = 8] = "BACKSPACE";
    KEYBOARD[KEYBOARD["TAB"] = 9] = "TAB";
    KEYBOARD[KEYBOARD["ENTER"] = 13] = "ENTER";
    KEYBOARD[KEYBOARD["SHIFT"] = 16] = "SHIFT";
    KEYBOARD[KEYBOARD["CTRL"] = 17] = "CTRL";
    KEYBOARD[KEYBOARD["ALT"] = 18] = "ALT";
    KEYBOARD[KEYBOARD["ESCAPE"] = 27] = "ESCAPE";
    KEYBOARD[KEYBOARD["SPACEBAR"] = 32] = "SPACEBAR";
    KEYBOARD[KEYBOARD["PAGE_UP"] = 33] = "PAGE_UP";
    KEYBOARD[KEYBOARD["PAGE_DOWN"] = 34] = "PAGE_DOWN";
    KEYBOARD[KEYBOARD["HOME"] = 36] = "HOME";
    KEYBOARD[KEYBOARD["LEFT_ARROW"] = 37] = "LEFT_ARROW";
    KEYBOARD[KEYBOARD["UP_ARROW"] = 38] = "UP_ARROW";
    KEYBOARD[KEYBOARD["RIGHT_ARROW"] = 39] = "RIGHT_ARROW";
    KEYBOARD[KEYBOARD["DOWN_ARROW"] = 40] = "DOWN_ARROW";
    KEYBOARD[KEYBOARD["INSERT"] = 45] = "INSERT";
    KEYBOARD[KEYBOARD["DELETE"] = 46] = "DELETE";
})(KEYBOARD = exports.KEYBOARD || (exports.KEYBOARD = {}));


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FleetprideCusto = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var SVGIcons_1 = __webpack_require__(3);
var FleetprideCusto = /** @class */ (function (_super) {
    __extends(FleetprideCusto, _super);
    /**
     * Creates a new `Fleetpride` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `ResultList` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     */
    function FleetprideCusto(element, options, bindings) {
        var _this = _super.call(this, element, FleetprideCusto_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, FleetprideCusto_1, options);
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.querySuccess, _this.handleQuerySuccess);
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.buildingQuery, _this.handleBuildingQuery);
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.preprocessResults, _this.setMobileFlag);
        _this.bind.onRootElement(coveo_search_ui_1.InitializationEvents.afterInitialization, _this.replaceResultLayoutIcons);
        return _this;
    }
    FleetprideCusto_1 = FleetprideCusto;
    FleetprideCusto.prototype.setupPipelineContext = function (data) {
        this.context = _.pick(data, _.identity) || {};
    };
    FleetprideCusto.prototype.getPipelineContext = function () {
        return this.context;
    };
    FleetprideCusto.prototype.replaceResultLayoutIcons = function () {
        var listLayoutIcon = this.root.querySelector('.CoveoResultLayout .coveo-icon.coveo-list-layout-icon');
        if (listLayoutIcon) {
            listLayoutIcon.innerHTML = SVGIcons_1.SVGIcons.icons.listLayout;
        }
        var cardLayoutIcon = this.root.querySelector('.CoveoResultLayout .coveo-icon.coveo-card-layout-icon');
        if (cardLayoutIcon) {
            cardLayoutIcon.innerHTML = SVGIcons_1.SVGIcons.icons.cardLayout;
        }
    };
    FleetprideCusto.prototype.setMobileFlag = function (data) {
        data.results.results.map(function (res) {
            res.raw.ismobile = "" + Coveo.DeviceUtils.isMobileDevice();
        });
    };
    FleetprideCusto.prototype.handleBuildingQuery = function (data) {
        data.queryBuilder.addContext(this.context);
    };
    FleetprideCusto.prototype.handleQuerySuccess = function (data) {
        var summary = this.root.querySelector('.coveo-summary-section');
        if (summary) {
            summary.classList.toggle('hidden', data.results.results.length > 0);
        }
    };
    var FleetprideCusto_1;
    FleetprideCusto.ID = 'FleetprideCusto';
    FleetprideCusto.options = {
        pipelineContext: coveo_search_ui_1.ComponentOptions.buildJsonOption(),
    };
    FleetprideCusto = FleetprideCusto_1 = __decorate([
        turbo_core_1.lazyComponent
    ], FleetprideCusto);
    return FleetprideCusto;
}(coveo_search_ui_1.Component));
exports.FleetprideCusto = FleetprideCusto;


/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = "<svg alt=\"Search\" focusable=\"false\" enable-background=\"new 0 0 20 20\" viewBox=\"0 0 20 20\" xmlns=\"http://www.w3.org/2000/svg\"><g fill=\"currentColor\"><path class=\"coveo-magnifier-circle-svg\" d=\"m8.368 16.736c-4.614 0-8.368-3.754-8.368-8.368s3.754-8.368 8.368-8.368 8.368 3.754 8.368 8.368-3.754 8.368-8.368 8.368m0-14.161c-3.195 0-5.793 2.599-5.793 5.793s2.599 5.793 5.793 5.793 5.793-2.599 5.793-5.793-2.599-5.793-5.793-5.793\"></path><path d=\"m18.713 20c-.329 0-.659-.126-.91-.377l-4.552-4.551c-.503-.503-.503-1.318 0-1.82.503-.503 1.318-.503 1.82 0l4.552 4.551c.503.503.503 1.318 0 1.82-.252.251-.581.377-.91.377\"></path></g></svg>"

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 576 512\"><path d=\"M288 144a110.94 110.94 0 0 0-31.24 5 55.4 55.4 0 0 1 7.24 27 56 56 0 0 1-56 56 55.4 55.4 0 0 1-27-7.24A111.71 111.71 0 1 0 288 144zm284.52 97.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400c-98.65 0-189.09-55-237.93-144C98.91 167 189.34 112 288 112s189.09 55 237.93 144C477.1 345 386.66 400 288 400z\"></path></svg>"

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = "<svg alt=\"Exclusion\" focusable=\"false\" enable-background=\"new 0 0 11 11\" viewBox=\"0 0 11 11\" xmlns=\"http://www.w3.org/2000/svg\"><g class=\"coveo-more-svg\" fill=\"none\"><path d=\"m10.083 4.583h-3.666v-3.666c0-.524-.393-.917-.917-.917s-.917.393-.917.917v3.667h-3.666c-.524-.001-.917.392-.917.916s.393.917.917.917h3.667v3.667c-.001.523.392.916.916.916s.917-.393.917-.917v-3.666h3.667c.523 0 .916-.393.916-.917-.001-.524-.394-.917-.917-.917z\"></path></g><g class=\"coveo-line-svg\" fill=\"none\"><path d=\"m10 6.5h-9c-.552 0-1-.448-1-1 0-.552.448-1 1-1h9c .552 0 1 .448 1 1 0 .552-.448 1-1 1z\"></path></g><g class=\"coveo-exclusion-svg\" fill=\"none\"><path d=\"m9.233 7.989-2.489-2.489 2.489-2.489c.356-.356.356-.889 0-1.244-.356-.356-.889-.356-1.244 0l-2.489 2.489-2.489-2.489c-.356-.356-.889-.356-1.244 0-.356.356-.356.889 0 1.244l2.489 2.489-2.489 2.489c-.356.356-.356.889 0 1.244.356.356.889.356 1.244 0l2.489-2.489 2.489 2.489c.356.356.889.356 1.244 0 .356-.355.356-.889 0-1.244z\"></path></g><g class=\"coveo-hook-svg\" fill=\"none\"><path d=\"m10.252 2.213c-.155-.142-.354-.211-.573-.213-.215.005-.414.091-.561.24l-4.873 4.932-2.39-2.19c-.154-.144-.385-.214-.57-.214-.214.004-.415.09-.563.24-.148.147-.227.343-.222.549.005.207.093.4.249.542l2.905 2.662c.168.154.388.239.618.239h.022.003c.237-.007.457-.101.618-.266l5.362-5.428c.148-.148.228-.344.223-.551s-.093-.399-.248-.542z\"></path></g></svg>"

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = "<svg alt=\"Loading\" focusable=\"false\" enable-background=\"new 0 0 18 18\" viewBox=\"0 0 18 18\" xmlns=\"http://www.w3.org/2000/svg\"><g fill=\"currentColor\"><path d=\"m16.76 8.051c-.448 0-.855-.303-.969-.757-.78-3.117-3.573-5.294-6.791-5.294s-6.01 2.177-6.79 5.294c-.134.537-.679.861-1.213.727-.536-.134-.861-.677-.728-1.212 1.004-4.009 4.594-6.809 8.731-6.809 4.138 0 7.728 2.8 8.73 6.809.135.536-.191 1.079-.727 1.213-.081.02-.162.029-.243.029z\"></path><path d=\"m9 18c-4.238 0-7.943-3.007-8.809-7.149-.113-.541.234-1.071.774-1.184.541-.112 1.071.232 1.184.773.674 3.222 3.555 5.56 6.851 5.56s6.178-2.338 6.852-5.56c.113-.539.634-.892 1.184-.773.54.112.887.643.773 1.184-.866 4.142-4.57 7.149-8.809 7.149z\"></path></g></svg>"

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = "<svg alt=\"Clear\" focusable=\"false\" enable-background=\"new 0 0 13 13\" viewBox=\"0 0 13 13\" xmlns=\"http://www.w3.org/2000/svg\"><g fill=\"currentColor\"><path d=\"m7.881 6.501 4.834-4.834c.38-.38.38-1.001 0-1.381s-1.001-.38-1.381 0l-4.834 4.834-4.834-4.835c-.38-.38-1.001-.38-1.381 0s-.38 1.001 0 1.381l4.834 4.834-4.834 4.834c-.38.38-.38 1.001 0 1.381s1.001.38 1.381 0l4.834-4.834 4.834 4.834c.38.38 1.001.38 1.381 0s .38-1.001 0-1.381z\"></path></g></svg>"

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = "<svg alt=\"Arrow Up\" focusable=\"false\" enable-background=\"new 0 0 10 6\" viewBox=\"0 0 10 6\" xmlns=\"http://www.w3.org/2000/svg\"><g fill=\"currentColor\"><path d=\"m5 .068c.222 0 .443.084.612.253l4.134 4.134c.338.338.338.886 0 1.224s-.886.338-1.224 0l-3.522-3.521-3.523 3.521c-.336.338-.886.338-1.224 0s-.337-.886.001-1.224l4.134-4.134c.168-.169.39-.253.612-.253z\"></path></g></svg>"

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = "<svg alt=\"Arrow Down\" focusable=\"false\" enable-background=\"new 0 0 10 6\" viewBox=\"0 0 10 6\" xmlns=\"http://www.w3.org/2000/svg\"><g fill=\"currentColor\"><path d=\"m5 5.932c-.222 0-.443-.084-.612-.253l-4.134-4.134c-.338-.338-.338-.886 0-1.224s.886-.338 1.224 0l3.522 3.521 3.523-3.521c.336-.338.886-.338 1.224 0s .337.886-.001 1.224l-4.135 4.134c-.168.169-.39.253-.611.253z\"></path></g></svg>"

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 24 24\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>Utility Icons / dash</title><g id=\"Symbols\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"Button/add/medium/added\" transform=\"translate(-139.000000, -8.000000)\"><g id=\"Quantity-Button\" transform=\"translate(1.000000, 0.000000)\"><g id=\"Utility-Icons-/-dash\" transform=\"translate(19.000000, 7.338000)\"></g><g id=\"Utility-Icons-/-add\" transform=\"translate(138.000000, 8.000000)\" fill=\"#01319F\" fill-rule=\"nonzero\"><g id=\"Group-600\"><path d=\"M14.181819,13.6363649 L23.1818199,13.6363649 C23.6181831,13.6363649 24,13.2545462 24,12.8181829 L24,11.181819 C24,10.7454557 23.6181831,10.363637 23.1818199,10.363637 L14.181819,10.363637 C13.8545465,10.363637 13.6363649,10.1454554 13.6363649,9.81818199 L13.6363649,0.818181872 C13.6363649,0.381818205 13.2545462,0 12.8181829,0 L11.181819,0 C10.7454557,0 10.363637,0.381818205 10.363637,0.818181872 L10.363637,9.81818199 C10.363637,10.1454554 10.1454554,10.363637 9.81818199,10.363637 L0.818181872,10.363637 C0.381818205,10.363637 0,10.7454557 0,11.181819 L0,12.8181829 C0,13.2545462 0.381818205,13.6363649 0.818181872,13.6363649 L9.81818199,13.6363649 C10.1454554,13.6363649 10.363637,13.8545465 10.363637,14.181819 L10.363637,23.1818199 C10.363637,23.6181831 10.7454557,24 11.181819,24 L12.8181829,24 C13.2545462,24 13.6363649,23.6181831 13.6363649,23.1818199 L13.6363649,14.181819 C13.6363649,13.8545465 13.8545465,13.6363649 14.181819,13.6363649 Z\" id=\"🎨-Icon-Color\"></path></g></g></g></g></g></svg>"

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 24 24\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>Utility Icons / dash</title><g id=\"Symbols\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"Button/add/medium/added\" transform=\"translate(-20.000000, -8.000000)\" fill-rule=\"nonzero\"><g id=\"Quantity-Button\" transform=\"translate(1.000000, 0.000000)\"><g id=\"Utility-Icons-/-dash\" transform=\"translate(19.000000, 7.338000)\" fill=\"#01319F\"><g id=\"Group-674\" transform=\"translate(0.000000, 11.000000)\"><path d=\"M24,0.752807558 C24,0.351310134 23.6500015,0 23.25,0 L0.75,0 C0.349999994,0 0,0.351310134 0,0.752807558 L0,2.25842285 C0,2.65992022 0.349999994,3.01123023 0.75,3.01123023 L23.25,3.01123023 C23.6500015,3.01123023 24,2.65992022 24,2.25842285 L24,0.752807558 Z\" id=\"🎨-Icon-Color\"></path></g></g></g></g></g></svg>"

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 25 25\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>Icon/branch/green</title><g id=\"Symbols\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"Icon/branch/green\" transform=\"translate(0.000000, 3.000000)\" fill=\"rgba(136, 139, 141, 0.5)\" fill-rule=\"nonzero\"><g id=\"noun_Garage_2740475\" transform=\"translate(0.000000, 0.000000)\"><path d=\"M0.984615385,0.0138596076 L22.8251748,3.36238411 C23.3622378,3.45288477 23.6307692,3.90538808 23.6307692,4.35789139 L23.6307692,18.2949934 C23.6307692,18.8379974 23.1832168,19.2 22.7356643,19.2 L18.7972028,19.2 C18.2601399,19.2 17.9020979,18.8379974 17.9020979,18.2949934 L17.9020979,16.1229775 C13.7846154,16.1229775 9.75664336,16.1229775 5.63916084,16.1229775 L5.63916084,18.2949934 C5.63916084,18.8379974 5.28111888,19.2 4.83356643,19.2 L0.805594406,19.2 C0.358041958,19.2 0,18.8379974 0,18.2949934 L0,0.828365568 C0,0.285361594 0.447552448,-0.0766410546 0.984615385,0.0138596076 Z M13.0685315,14.3129642 L13.0685315,13.317457 L10.0251748,13.317457 L10.0251748,14.3129642 L13.0685315,14.3129642 Z M13.0685315,11.5979444 L13.0685315,9.06392583 L10.0251748,9.06392583 L10.0251748,11.5979444 L13.0685315,11.5979444 Z M8.23496503,14.3129642 L8.23496503,13.317457 L5.63916084,13.317457 L5.63916084,14.3129642 L8.23496503,14.3129642 Z M8.23496503,11.5979444 L8.23496503,9.06392583 L5.63916084,9.06392583 L5.63916084,11.5979444 L8.23496503,11.5979444 Z M14.7692308,9.06392583 L14.7692308,11.5979444 L17.9020979,11.5979444 L17.9020979,9.06392583 L14.7692308,9.06392583 Z M14.7692308,13.317457 L14.7692308,14.3129642 L17.9020979,14.3129642 L17.9020979,13.317457 L14.7692308,13.317457 Z\" id=\"Shape\"></path></g></g></g></svg>"

/***/ }),
/* 23 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 22 22\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>Icon/filter</title><defs><path d=\"M21.2767751,0 L0.784467453,0 C0.0921597603,0 -0.230917163,0.784615385 0.184467453,1.29230769 L9.13831361,11.7692308 C9.41523668,12.0923077 9.55369822,12.5538462 9.55369822,12.9692308 L9.55369822,19.6153846 C9.55369822,19.9846154 9.92292899,20.3076923 10.2921598,20.3076923 L11.6767751,20.3076923 C12.0460059,20.3076923 12.322929,19.9846154 12.322929,19.6153846 L12.322929,12.9692308 C12.322929,12.5076923 12.5075444,12.0923077 12.8306213,11.7692308 L21.8767751,1.29230769 C22.2921598,0.784615385 21.9690828,4.09928501e-16 21.2767751,4.09928501e-16 L21.2767751,0 Z\" id=\"path-1\"></path></defs><g id=\"Symbols\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"Utility-Icons-/-filterList\" transform=\"translate(0.000000, 1.000000)\"><mask id=\"mask-2\" fill=\"white\"><use xlink:href=\"#path-1\"></use></mask><use id=\"🎨-Icon-Color\" fill=\"#20285D\" xlink:href=\"#path-1\"></use></g></g></svg>"

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = "<svg version=\"1.1\" viewBox=\"0 0 36 36\" preserveAspectRatio=\"xMidYMid meet\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" focusable=\"false\" role=\"img\"><path class=\"clr-i-outline clr-i-outline-path-1\" d=\"M18,6A12,12,0,1,0,30,18,12,12,0,0,0,18,6Zm0,22A10,10,0,1,1,28,18,10,10,0,0,1,18,28Z\"></path><path class=\"clr-i-outline clr-i-outline-path-2\" d=\"M16.34,23.74l-5-5a1,1,0,0,1,1.41-1.41l3.59,3.59,6.78-6.78a1,1,0,0,1,1.41,1.41Z\"></path></svg>"

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 20 20\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>Group-600</title><g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"Sm-Quant-button\" fill=\"#FFFFFF\" fill-rule=\"nonzero\"><g id=\"Group-600\"><path d=\"M11.818182,11.363637 L19.318182,11.363637 C19.681818,11.363637 20,11.045455 20,10.681819 L20,9.31818199 C20,8.95454597 19.681818,8.63636398 19.318182,8.63636398 L11.818182,8.63636398 C11.545455,8.63636398 11.363637,8.45454597 11.363637,8.18181801 L11.363637,0.681818187 C11.363637,0.318181843 11.045455,0 10.681819,0 L9.31818199,0 C8.95454597,0 8.63636398,0.318181843 8.63636398,0.681818187 L8.63636398,8.18181801 C8.63636398,8.45454597 8.45454597,8.63636398 8.18181801,8.63636398 L0.681818187,8.63636398 C0.318181843,8.63636398 0,8.95454597 0,9.31818199 L0,10.681819 C0,11.045455 0.318181843,11.363637 0.681818187,11.363637 L8.18181801,11.363637 C8.45454597,11.363637 8.63636398,11.545455 8.63636398,11.818182 L8.63636398,19.318182 C8.63636398,19.681818 8.95454597,20 9.31818199,20 L10.681819,20 C11.045455,20 11.363637,19.681818 11.363637,19.318182 L11.363637,11.818182 C11.363637,11.545455 11.545455,11.363637 11.818182,11.363637 Z\" id=\"🎨-Icon-Color\"></path></g></g></g></svg>"

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 16 16\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>icons_utility_info_alt</title><g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"info-circle-line\" fill=\"#54698D\" fill-rule=\"nonzero\"><g id=\"icons_utility_info_alt\"><path d=\"M8,0.615384615 C3.90769231,0.615384615 0.615384615,3.90769231 0.615384615,8 C0.615384615,12.0923077 3.90769231,15.3846154 8,15.3846154 C12.0923077,15.3846154 15.3846154,12.0923077 15.3846154,8 C15.3846154,3.90769231 12.0923077,0.615384615 8,0.615384615 Z M8,13.5384615 C4.92307692,13.5384615 2.46153846,11.0769231 2.46153846,8 C2.46153846,4.92307692 4.92307692,2.46153846 8,2.46153846 C11.0769231,2.46153846 13.5384615,4.92307692 13.5384615,8 C13.5384615,11.0769231 11.0769231,13.5384615 8,13.5384615 Z M8,4.33846154 C8.52307692,4.33846154 8.92307692,4.73846154 8.92307692,5.26153846 C8.92307692,5.78461538 8.52307692,6.18461538 8,6.18461538 C7.47692308,6.18461538 7.07692308,5.78461538 7.07692308,5.26153846 C7.07692308,4.73846154 7.47692308,4.33846154 8,4.33846154 Z M9.53846154,10.8 C9.53846154,10.9538462 9.41538462,11.0769231 9.23076923,11.0769231 L6.76923077,11.0769231 C6.61538462,11.0769231 6.46153846,10.9846154 6.46153846,10.8 L6.46153846,10.1846154 C6.46153846,10.0307692 6.58461538,9.84615385 6.76923077,9.84615385 C6.92307692,9.84615385 7.07692308,9.75384615 7.07692308,9.56923077 L7.07692308,8.33846154 C7.07692308,8.18461538 6.95384615,8 6.76923077,8 C6.61538462,8 6.46153846,7.90769231 6.46153846,7.72307692 L6.46153846,7.10769231 C6.46153846,6.95384615 6.58461538,6.76923077 6.76923077,6.76923077 L8.61538462,6.76923077 C8.76923077,6.76923077 8.92307692,6.92307692 8.92307692,7.10769231 L8.92307692,9.56923077 C8.92307692,9.72307692 9.04615385,9.84615385 9.23076923,9.84615385 C9.38461538,9.84615385 9.53846154,10 9.53846154,10.1846154 L9.53846154,10.8 Z\" id=\"path-2\"></path></g></g></g></svg>"

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 25 24\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>C5046620-CD94-431E-9B96-B063A106364E@1x</title><g id=\"Symbols\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"Icon/local-delivery/green\" fill=\"#1C6019\" fill-rule=\"nonzero\"><path d=\"M11.1459158,18.474 C12.6274796,18.474 13.9088317,19.6752677 13.9088317,21.2369158 C13.9088317,22.7585216 12.6675216,23.9998317 11.1459158,23.9998317 C9.62431011,23.9998317 8.38299999,22.7184796 8.38299999,21.2369158 C8.38299999,19.7153101 9.66435239,18.474 11.1459158,18.474 Z M22.4385929,14.7097871 C22.839015,14.5496181 23.2794809,14.7498294 23.4396496,15.1902942 L23.4396496,15.1902942 L23.9601994,16.6718579 C24.120368,17.152365 23.9201565,17.59283 23.5197334,17.7930413 L23.5197334,17.7930413 L15.1509017,20.6360414 C14.9506904,19.4748156 14.3100143,18.4737597 13.349,17.8731256 L13.349,17.8731256 Z M5.26028061,0.0547871233 C5.66070366,-0.105381931 6.10116863,0.0948293784 6.26133728,0.535294233 L6.26133728,0.535294233 L6.74184465,2.01685786 L12.1475496,17.3530426 C11.8272114,17.2329159 11.5068741,17.1928749 11.146493,17.1928749 C10.3456478,17.1928749 9.58484554,17.4331284 8.94416904,17.7935066 L8.94416904,17.7935066 L3.9789288,3.73867535 L1.53635072,4.53952026 C1.13592815,4.69968939 0.695463298,4.49947786 0.535294233,4.05901289 L0.535294233,4.05901289 L0.0547871233,2.57744956 C-0.105381931,2.17702698 0.0948293784,1.73656201 0.535294233,1.57639301 L0.535294233,1.57639301 Z M18.4341123,7.34256221 L20.1959722,12.3078026 C20.3961837,12.7883095 20.1158874,13.3489012 19.5953376,13.5491123 L14.6300981,15.3109722 C14.1495907,15.5111837 13.5489568,15.2308874 13.3887878,14.7103376 L11.6269283,9.74509806 C11.426717,9.26459069 11.7070129,8.66395683 12.2275622,8.50378782 L17.1928026,6.74192834 C17.6733095,6.54171704 18.2739436,6.82201286 18.4341123,7.34256221 Z M14.3494225,0.775562206 L15.5106473,4.13911186 C15.7108588,4.57957683 15.430563,5.18021093 14.9100137,5.38042245 L11.546464,6.54164729 C11.0659568,6.74185882 10.4653228,6.46156297 10.305154,5.94101367 L9.14392834,2.57746396 C8.94371704,2.09695683 9.22401286,1.49632285 9.74456221,1.33615396 L13.1081119,0.174928338 C13.5886192,-0.0252829556 14.1892533,0.255012863 14.3494225,0.775562206 Z\" id=\"Shape\"></path></g></g></svg>"

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 23 24\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>73726046-2E85-4179-8712-E7D90917094B@1x</title><g id=\"Symbols\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"Icon/shipping-box/green\" fill=\"#1C6019\" fill-rule=\"nonzero\"><path d=\"M21.9596792,7.71700001 C22.2082792,7.71700001 22.3740125,7.88273357 22.3740125,8.13133389 L22.3740125,8.13133389 L22.3740125,17.205246 C22.3740125,18.1167803 21.8768116,18.9454479 21.0895773,19.4012156 C17.0567279,21.6938632 14.3773691,23.2130874 13.0515007,23.9588881 C13.0100673,24.0003195 12.9272005,24.0003195 12.8443338,24.0003195 C12.5957334,24.0003195 12.4299999,23.8345861 12.4299999,23.585988 L12.4299999,23.585988 L12.4299999,13.4762406 C12.4299999,13.1862073 12.5957334,12.8961735 12.8443338,12.7718735 L12.8443338,12.7718735 L21.7525116,7.7584334 C21.7939449,7.71700001 21.8768116,7.71700001 21.9596792,7.71700001 Z M0.041433271,7.92413559 C0.165733442,7.71696863 0.414333761,7.63410186 0.621500671,7.75840202 L0.621500671,7.75840202 L9.52967918,12.7718422 C9.77827919,12.8961422 9.94401252,13.186176 9.94401252,13.4762098 L9.94401252,13.4762098 L9.94401252,23.5445244 C9.94401252,23.5859577 9.94401252,23.6688244 9.90258014,23.7516892 C9.77827919,23.9588558 9.52967918,24.0417225 9.32251155,23.9174225 C7.9966439,23.1716231 5.31728502,21.6523996 1.28443491,19.3597519 C0.497200549,18.9039843 -1.19209286e-07,18.0753157 -1.19209286e-07,17.1637804 L-1.19209286e-07,17.1637804 L0.000647277513,8.06979985 C0.00258946768,8.00959194 0.0103582283,7.95521061 0.041433271,7.92413559 Z M9.9027292,0.341825474 C10.6899635,-0.113941815 11.6843654,-0.113941815 12.4715997,0.341825474 C16.5320716,2.66209524 19.2252415,4.19513068 20.5511095,4.94093182 C20.5925428,4.98236467 20.6339761,5.02379801 20.6754095,5.06523182 C20.7997095,5.27239849 20.7582761,5.52099897 20.5511095,5.64529945 L20.5511095,5.64529945 L11.6014978,10.6587391 C11.3528978,10.7830391 11.0214311,10.7830391 10.7728302,10.6587391 L10.7728302,10.6587391 L1.8232189,5.64529945 L1.8232189,5.64529945 L1.69891872,5.52099897 C1.57461856,5.31383182 1.61605195,5.06523182 1.8232189,4.94093182 C3.14908724,4.16750846 5.84225734,2.63447301 9.9027292,0.341825474 Z\" id=\"Shape\"></path></g></g></svg>"

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 24 24\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>icons_utility_rows</title><g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"icons_utility_rows\" fill=\"rgba(136, 139, 141, 0.5)\" fill-rule=\"nonzero\"><path d=\"M22.8131868,4.61538462 L1.18681319,4.61538462 C0.764835165,4.61538462 0.395604396,4.18461538 0.395604396,3.69230769 L0.395604396,1.84615385 C0.395604396,1.35384615 0.764835165,0.923076923 1.18681319,0.923076923 L22.8131868,0.923076923 C23.2351648,0.923076923 23.6043956,1.35384615 23.6043956,1.84615385 L23.6043956,3.69230769 C23.6043956,4.18461538 23.2351648,4.61538462 22.8131868,4.61538462 Z M22.8131868,13.7846154 L1.18681319,13.7846154 C0.764835165,13.7846154 0.395604396,13.3538462 0.395604396,12.8615385 L0.395604396,11.0153846 C0.395604396,10.5846154 0.764835165,10.1538462 1.18681319,10.1538462 L22.8131868,10.1538462 C23.2351648,10.1538462 23.6043956,10.5846154 23.6043956,11.0769231 L23.6043956,12.9230769 C23.6043956,13.3538462 23.2351648,13.7846154 22.8131868,13.7846154 Z M22.8131868,23.0769231 L1.18681319,23.0769231 C0.764835165,23.0769231 0.395604396,22.6461538 0.395604396,22.1538462 L0.395604396,20.3076923 C0.395604396,19.8153846 0.764835165,19.3846154 1.18681319,19.3846154 L22.8131868,19.3846154 C23.2351648,19.3846154 23.6043956,19.8153846 23.6043956,20.3076923 L23.6043956,22.1538462 C23.6043956,22.6461538 23.2351648,23.0769231 22.8131868,23.0769231 Z\" id=\"path-2\"></path></g></g></svg>"

/***/ }),
/* 30 */
/***/ (function(module, exports) {

module.exports = "<svg viewBox=\"0 0 24 24\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><title>icons_utility_apps copy</title><g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\"><g id=\"icons_utility_apps-copy\" fill=\"#54698D\" fill-rule=\"nonzero\"><path d=\"M5.14285714,0.395604396 L1.97802198,0.395604396 C1.08131868,0.395604396 0.395604396,1.08131868 0.395604396,1.97802198 L0.395604396,5.14285714 C0.395604396,6.03956044 1.08131868,6.72527473 1.97802198,6.72527473 L5.14285714,6.72527473 C6.03956044,6.72527473 6.72527473,6.03956044 6.72527473,5.14285714 L6.72527473,1.97802198 C6.72527473,1.08131868 6.03956044,0.395604396 5.14285714,0.395604396 Z M5.14285714,17.2747253 L1.97802198,17.2747253 C1.08131868,17.2747253 0.395604396,17.9604396 0.395604396,18.8571429 L0.395604396,22.021978 C0.395604396,22.9186813 1.08131868,23.6043956 1.97802198,23.6043956 L5.14285714,23.6043956 C6.03956044,23.6043956 6.72527473,22.9186813 6.72527473,22.021978 L6.72527473,18.8571429 C6.72527473,17.9604396 6.03956044,17.2747253 5.14285714,17.2747253 Z M5.14285714,8.83516484 L1.97802198,8.83516484 C1.08131868,8.83516484 0.395604396,9.52087912 0.395604396,10.4175824 L0.395604396,13.5824176 C0.395604396,14.4791209 1.08131868,15.1648352 1.97802198,15.1648352 L5.14285714,15.1648352 C6.03956044,15.1648352 6.72527473,14.4791209 6.72527473,13.5824176 L6.72527473,10.4175824 C6.72527473,9.52087912 6.03956044,8.83516484 5.14285714,8.83516484 Z M13.5824176,0.395604396 L10.4175824,0.395604396 C9.52087912,0.395604396 8.83516484,1.08131868 8.83516484,1.97802198 L8.83516484,5.14285714 C8.83516484,6.03956044 9.52087912,6.72527473 10.4175824,6.72527473 L13.5824176,6.72527473 C14.4791209,6.72527473 15.1648352,6.03956044 15.1648352,5.14285714 L15.1648352,1.97802198 C15.1648352,1.08131868 14.4791209,0.395604396 13.5824176,0.395604396 Z M13.5824176,17.2747253 L10.4175824,17.2747253 C9.52087912,17.2747253 8.83516484,17.9604396 8.83516484,18.8571429 L8.83516484,22.021978 C8.83516484,22.9186813 9.52087912,23.6043956 10.4175824,23.6043956 L13.5824176,23.6043956 C14.4791209,23.6043956 15.1648352,22.9186813 15.1648352,22.021978 L15.1648352,18.8571429 C15.1648352,17.9604396 14.4791209,17.2747253 13.5824176,17.2747253 Z M13.5824176,8.83516484 L10.4175824,8.83516484 C9.52087912,8.83516484 8.83516484,9.52087912 8.83516484,10.4175824 L8.83516484,13.5824176 C8.83516484,14.4791209 9.52087912,15.1648352 10.4175824,15.1648352 L13.5824176,15.1648352 C14.4791209,15.1648352 15.1648352,14.4791209 15.1648352,13.5824176 L15.1648352,10.4175824 C15.1648352,9.52087912 14.4791209,8.83516484 13.5824176,8.83516484 Z M22.021978,0.395604396 L18.8571429,0.395604396 C17.9604396,0.395604396 17.2747253,1.08131868 17.2747253,1.97802198 L17.2747253,5.14285714 C17.2747253,6.03956044 17.9604396,6.72527473 18.8571429,6.72527473 L22.021978,6.72527473 C22.9186813,6.72527473 23.6043956,6.03956044 23.6043956,5.14285714 L23.6043956,1.97802198 C23.6043956,1.08131868 22.9186813,0.395604396 22.021978,0.395604396 Z M22.021978,17.2747253 L18.8571429,17.2747253 C17.9604396,17.2747253 17.2747253,17.9604396 17.2747253,18.8571429 L17.2747253,22.021978 C17.2747253,22.9186813 17.9604396,23.6043956 18.8571429,23.6043956 L22.021978,23.6043956 C22.9186813,23.6043956 23.6043956,22.9186813 23.6043956,22.021978 L23.6043956,18.8571429 C23.6043956,17.9604396 22.9186813,17.2747253 22.021978,17.2747253 Z M22.021978,8.83516484 L18.8571429,8.83516484 C17.9604396,8.83516484 17.2747253,9.52087912 17.2747253,10.4175824 L17.2747253,13.5824176 C17.2747253,14.4791209 17.9604396,15.1648352 18.8571429,15.1648352 L22.021978,15.1648352 C22.9186813,15.1648352 23.6043956,14.4791209 23.6043956,13.5824176 L23.6043956,10.4175824 C23.6043956,9.52087912 22.9186813,8.83516484 22.021978,8.83516484 Z\" id=\"path-2\"></path></g></g></svg>"

/***/ }),
/* 31 */
/***/ (function(module, exports) {

module.exports = "<svg version=\"1.1\" viewBox=\"0 0 36 36\" preserveAspectRatio=\"xMidYMid meet\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" focusable=\"false\" role=\"img\"><path class=\"clr-i-outline clr-i-outline-path-1\" d=\"M18,6A12,12,0,1,0,30,18,12,12,0,0,0,18,6Zm0,22A10,10,0,1,1,28,18,10,10,0,0,1,18,28Z\"></path><path class=\"clr-i-outline clr-i-outline-path-2\" d=\"M18,20.07a1.3,1.3,0,0,1-1.3-1.3v-6a1.3,1.3,0,1,1,2.6,0v6A1.3,1.3,0,0,1,18,20.07Z\"></path><circle class=\"clr-i-outline clr-i-outline-path-3\" cx=\"17.95\" cy=\"23.02\" r=\"1.5\"></circle></svg>"

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductImage = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var CoveoUtils_1 = __webpack_require__(5);
var ProductImage = /** @class */ (function (_super) {
    __extends(ProductImage, _super);
    /**
     * Creates a new `ProductImage` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `ProductImage` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     * @param result The result to associate the component with.
     */
    function ProductImage(element, options, bindings, result) {
        var _this = _super.call(this, element, ProductImage_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.result = result;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, ProductImage_1, options);
        var icon = _this.build();
        if (icon) {
            _this.element.appendChild(_this.build());
        }
        return _this;
    }
    ProductImage_1 = ProductImage;
    ProductImage.prototype.build = function () {
        var rawImage = CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, this.options.field);
        if (rawImage) {
            var productImage = this.getProductImage(rawImage);
            var icon = coveo_search_ui_1.$$('img', { src: productImage });
            // this.element.style.height = `${this.options.width}px`;
            // icon.el.style.width = `${this.options.width}px`;
            return icon.el;
        }
        else {
            return null;
        }
    };
    /**
     * Returns the product image based on value stored in the [`field`]{@link ProductImage.options.field} option.
     *
     * @private
     * @param {string} rawImage
     * @returns the image URL.
     */
    ProductImage.prototype.getProductImage = function (rawImage) {
        // Removing the _Thumb suffix to get the real image
        return rawImage.replace('_Thumb.', '.');
    };
    var ProductImage_1;
    ProductImage.ID = 'ProductImage';
    ProductImage.options = {
        /**
         * Specifies the field used to determine which icon to use
         *
         */
        field: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_imageurl' }),
    };
    ProductImage = ProductImage_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component renders the image of a promotion item. This component assumes that the image url is stored in the item.
         *
         * This component is a result template component (see [Result Templates](https://docs.coveo.com/en/413/)).
         *
         * @export
         * @class ProductImage
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], ProductImage);
    return ProductImage;
}(coveo_search_ui_1.Component));
exports.ProductImage = ProductImage;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddToCart = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var SVGIcons_1 = __webpack_require__(3);
var underscore_1 = __webpack_require__(2);
var CoveoUtils_1 = __webpack_require__(5);
var CustomEvents_1 = __webpack_require__(4);
var ItemPrice_1 = __webpack_require__(7);
var turbo_core_1 = __webpack_require__(1);
var CommerceDataLayerEvents_1 = __webpack_require__(9);
var AddToCart = /** @class */ (function (_super) {
    __extends(AddToCart, _super);
    /**
     * Creates a new `AddToCart` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `AddToCart` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     * @param result The result to associate the component with.
     */
    function AddToCart(element, options, bindings, result) {
        var _this = _super.call(this, element, AddToCart_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.result = result;
        _this.count = null;
        _this.lastProductInfo = null;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, AddToCart_1, options);
        _this.bind.oneRootElement(CustomEvents_1.CustomEvents.updateProductInfo, function (args) {
            !_this.deactivate() && _this.handleProductUpdate(args);
        });
        _this.bind.oneRootElement(CustomEvents_1.CustomEvents.invalidProductInfo, function (err) {
            _this.handleInvalidProduct();
        });
        // Can be fired multiple times if the user changes the shipping method
        _this.bind.onRootElement(CustomEvents_1.CustomEvents.changeShippingMethod, function (data) {
            _this.handleShippingMethodChange(data);
        });
        // this.bind.oneRootElement(CustomEvents.invalidBranchInventory, (err: any) => {
        // TODO: handle error
        //   this.hadnleInvalidBranchInventory(err);
        // });
        if (_this.deactivate()) {
            return _this;
        }
        _this.buildPlacehodler();
        return _this;
    }
    AddToCart_1 = AddToCart;
    /**
     * Find the appropriate price within a list of ICCPrice.
     *
     * @private
     * @param {ICCPrice[]} prices  a list of ICCPrice. There is one ICCPrice per item returned by the index
     * @param {string} key         the key to use to find the appropriate ICCPrice object.
     * @returns {number}           the actual price of the ICCPrice object.
     */
    AddToCart.prototype.findPriceFromList = function (prices, key) {
        if (prices && prices.length > 0 && key) {
            var match = underscore_1.findWhere(prices, { PartNumber: key.toUpperCase() });
            return match ? match.PRICE : null;
        }
        else {
            // this.logger.error(`Unable to find key "${key}" in the results`);
            return null;
        }
    };
    AddToCart.prototype.handleShippingMethodChange = function (data) {
        // Get shipping method for current result
        if (data.index === this.result.index) {
            this.shippingMethod = data.shippingMedhod;
        }
    };
    AddToCart.prototype.handleInvalidProduct = function () {
        this.disableAddToCartButton();
    };
    AddToCart.prototype.findInventoryFromList = function (inventory, key) {
        if (inventory && inventory[key.toUpperCase()]) {
            return inventory[key];
        }
        else {
            // this.logger.error(`Unable to find key "${key}" in the results`);
            return null;
        }
    };
    AddToCart.prototype.getInventoryQuantity = function () {
        var inventoryForCurrentProduct = this.getCurrentInventory();
        return inventoryForCurrentProduct.totalQuantity;
    };
    AddToCart.prototype.isItemAvailable = function () {
        var inventoryForCurrentProduct = this.getCurrentInventory();
        return inventoryForCurrentProduct && Math.floor(inventoryForCurrentProduct.totalQuantity / this.getPackMultiplicator()) > 0;
    };
    AddToCart.prototype.getCurrentInventory = function () {
        var productInventory = this.lastProductInfo.inventory;
        return this.findInventoryFromList(productInventory, CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, this.lastProductInfo.options.skuField));
    };
    AddToCart.prototype.getCurrentPrice = function () {
        var prices = this.lastProductInfo.prices;
        return this.findPriceFromList(prices, CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, this.lastProductInfo.options.partNumberField));
    };
    AddToCart.prototype.getCurrentCorePrice = function () {
        var prices = this.lastProductInfo.prices;
        return this.findPriceFromList(prices, CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, this.lastProductInfo.options.corePartNumberField));
    };
    AddToCart.prototype.isPricingAvailable = function () {
        var price = this.getCurrentPrice();
        return price && price !== ItemPrice_1.ItemPrice.DEFAULT_VALUE;
    };
    AddToCart.prototype.handleProductUpdate = function (args) {
        this.lastProductInfo = args;
        this.count = this.getPackMultiplicator();
        if (this.isItemAvailable() && this.isPricingAvailable()) {
            this.build();
        }
        else {
            // this.clearElement();
            // this.element.appendChild(this.buildAddToCartButton());
            // this.disableAddToCartButton();
            this.element.classList.add('hidden');
        }
    };
    AddToCart.prototype.disableAddToCartButton = function () {
        this.element.classList.add('disabled');
        this.element.classList.add('hidden');
        // this.element.querySelector('.coveops-add-to-cart-button').setAttribute('disabled', 'disabled');
    };
    AddToCart.prototype.buildPlacehodler = function () {
        this.element.appendChild(coveo_search_ui_1.$$('div', { className: 'coveops-add-to-cart-button-placeholder' }).el);
    };
    AddToCart.prototype.deactivate = function () {
        if (this.options.deactivate && underscore_1.isFunction(this.options.deactivate)) {
            return this.options.deactivate();
        }
        else {
            return false;
        }
    };
    AddToCart.prototype.clearElement = function () {
        this.element.innerHTML = '';
    };
    // TODO: implement this
    AddToCart.prototype.getShippingMethod = function () {
        var shipping = 'fp_shipping_option1';
        // possibleValues are  fp_shipping_option1, fp_shipping_option2, fp_shipping_option3
        return shipping;
    };
    AddToCart.prototype.build = function () {
        coveo_search_ui_1.Assert.isNotNull(this.lastProductInfo);
        this.clearElement();
        var firstRow = coveo_search_ui_1.$$('div', { className: 'flex-justify-end' });
        var factor = this.getPackMultiplicator();
        if (factor > 1 && this.options.displayIncrementors) {
            firstRow.append(this.buildMultiPackInfo(factor));
            this.element.appendChild(firstRow.el);
        }
        var secondRow = coveo_search_ui_1.$$('div', { className: 'coveops-add-to-cart-button-section' });
        if (this.options.displayIncrementors) {
            secondRow.append(this.buildIncrementorButtons());
        }
        secondRow.append(this.buildAddToCartButton());
        this.element.appendChild(secondRow.el);
    };
    AddToCart.prototype.buildAddToCartButton = function () {
        var _this = this;
        var button = coveo_search_ui_1.$$('button', { role: 'button', tabindex: 0, className: 'coveops-add-to-cart-button' }, this.options.useIcon ? SVGIcons_1.SVGIcons.icons.plus : coveo_search_ui_1.l(this.options.caption));
        button.on('click', function () { return _this.handleClick(); });
        return button.el;
    };
    AddToCart.prototype.getPackMultiplicator = function () {
        var salespack = CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, this.lastProductInfo.options.salespack);
        return salespack && !isNaN(salespack) ? parseInt(salespack, 10) : 1; // Because the field was initially created as a string
    };
    AddToCart.prototype.buildMultiPackInfo = function (perPack) {
        var element = coveo_search_ui_1.$$('span', {}, "*This product only sells in increments of: " + perPack);
        return element.el;
    };
    AddToCart.prototype.buildIncrementorButtons = function () {
        var _this = this;
        var decrementor = coveo_search_ui_1.$$('button', { role: 'button', className: 'coveops-increment-icon' }, SVGIcons_1.SVGIcons.icons.decrement);
        var incrementor = coveo_search_ui_1.$$('button', { role: 'button', className: 'coveops-increment-icon' }, SVGIcons_1.SVGIcons.icons.increment);
        decrementor.on('click', function () { return _this.decreaseCount(); });
        incrementor.on('click', function () { return _this.increaseCount(); });
        var element = coveo_search_ui_1.$$('div', { className: 'coveops-add-to-cart-increments' });
        element.addClass('in-cart');
        element.append(decrementor.el);
        element.append(coveo_search_ui_1.$$('span', { className: 'coveops-current-qty' }, this.getQuantity().toString()).el);
        element.append(incrementor.el);
        return element.el;
    };
    AddToCart.prototype.getQuantity = function () {
        return this.count;
    };
    AddToCart.prototype.increaseCount = function () {
        var pack = this.getPackMultiplicator();
        var maxQty = this.getInventoryQuantity();
        if (this.count <= maxQty - pack) {
            this.count += pack;
            this.updateCountAppearance();
        }
    };
    AddToCart.prototype.decreaseCount = function () {
        var pack = this.getPackMultiplicator();
        if (this.count >= pack + 1) {
            this.count -= pack;
            this.updateCountAppearance();
        }
    };
    AddToCart.prototype.updateCountAppearance = function () {
        // this.ensureDom();
        var args = { count: this.count, result: this.result };
        this.bind.trigger(this.root, CustomEvents_1.CustomEvents.addToCartCountChange, args);
        this.element.querySelector('.coveops-current-qty').textContent = this.count.toString();
    };
    AddToCart.prototype.getAddToCartPayload = function () {
        coveo_search_ui_1.Assert.isNotNull(this.lastProductInfo);
        coveo_search_ui_1.Assert.check(this.lastProductInfo.options.skuField !== undefined, 'skuField is unavailable');
        var sku = Coveo.Utils.getFieldValue(this.result, this.lastProductInfo.options.skuField);
        var qty = this.getQuantity();
        var partNumberValue = CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, '@fp_prd_dsppartnumber'); // TODO: put fields as option
        var poolNumberValue = CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, '@fp_prd_poolnumber'); // TODO: put fields as option
        var prodList = [{ partNumber: partNumberValue.toString(), poolNumber: poolNumberValue.toString(), quantity: qty.toString() }];
        return [sku, qty, prodList];
    };
    AddToCart.prototype.handleClick = function () {
        var _this = this;
        var _a = this.getAddToCartPayload(), sku = _a[0], qty = _a[1], prodList = _a[2];
        this.enableAnimation();
        if (sku && qty && this.shippingMethod && prodList) {
            var args = {
                sku: sku,
                qty: qty,
                shippingMethod: this.shippingMethod,
                prodList: prodList,
                successCallback: function (data) { return _this.handleAddToCartSuccess(data); },
                errorCallback: function (err) { return _this.handleAddToCartError(err); },
            };
            this.bind.trigger(this.root, CustomEvents_1.CustomEvents.addToCartRemoteAction, args);
            if (this.options.logClickEvent) {
                this.logClickEvent(args.qty);
            }
            this.logCommerceEvent(args.qty);
        }
    };
    AddToCart.prototype.logCommerceEvent = function (qty) {
        var args = { result: this.result, count: qty };
        this.bind.trigger(this.root, CommerceDataLayerEvents_1.CommerceDataLayerEvents.addToCart, args);
    };
    AddToCart.prototype.logClickEvent = function (qty) {
        var cause = {
            type: 'document',
            name: 'addToCart',
        };
        var meta = {
            qty: qty,
        };
        Coveo.logClickEvent(this.root, cause, meta, this.result);
    };
    AddToCart.prototype.buildProductContextForTemplate = function () {
        var productData = {
            mediaImage: coveo_search_ui_1.Utils.getFieldValue(this.result, 'fp_prd_imageurl'),
            name: coveo_search_ui_1.Utils.getFieldValue(this.result, 'fp_prd_brandname') + " " + coveo_search_ui_1.Utils.getFieldValue(this.result, 'fp_prd_shortdescription'),
            price: coveo_search_ui_1.CurrencyUtils.currencyToString(this.getCurrentPrice(), { decimals: 2 }),
        };
        var coreProductPrice = coveo_search_ui_1.CurrencyUtils.currencyToString(this.getCurrentCorePrice(), { decimals: 2 });
        if (coreProductPrice) {
            productData.corePrice = coreProductPrice;
        }
        return { product: productData };
    };
    AddToCart.prototype.handleAddToCartError = function (data) {
        this.logger.error('Unable to add item to cart', data);
        if (this.options.errorCallback && underscore_1.isFunction(this.options.errorCallback)) {
            this.options.errorCallback(this.buildProductContextForTemplate(), data);
        }
        else {
            this.logger.error('errorCallback option is not defined');
        }
        this.restoreButtonInitialState();
    };
    AddToCart.prototype.handleAddToCartSuccess = function (data) {
        this.disableAnimation();
        if (this.options.confirmationCallback && underscore_1.isFunction(this.options.confirmationCallback)) {
            this.options.confirmationCallback(this.buildProductContextForTemplate(), data);
        }
        else {
            this.logger.error('confirmationCallback option is not defined');
        }
        this.restoreButtonInitialState();
    };
    AddToCart.prototype.restoreButtonInitialState = function () {
        if (this.options.useIcon) {
            this.element.querySelector('.coveops-add-to-cart-button').innerHTML = SVGIcons_1.SVGIcons.icons.plus;
        }
        else {
            this.element.querySelector('.coveops-add-to-cart-button').textContent = this.options.caption;
        }
    };
    AddToCart.prototype.disableAnimation = function () {
        var button = this.element.querySelector('.coveops-add-to-cart-button');
        button.classList.remove('coveops-wait-animation');
    };
    AddToCart.prototype.enableAnimation = function () {
        // this.resetButtonHtml();
        // this.ensureDom();
        var button = this.element.querySelector('.coveops-add-to-cart-button');
        if (this.options.useIcon) {
            button.innerHTML = SVGIcons_1.SVGIcons.icons.loading;
            button.classList.add('coveops-wait-animation');
        }
        else {
            button.textContent = this.options.addingToCartCaption;
        }
    };
    var AddToCart_1;
    AddToCart.ID = 'AddToCart';
    AddToCart.options = {
        enableForGuestUsers: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: false }),
        useIcon: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: false }),
        /**
         * Specifies if an "Add to cart" action shoul log a document click. It is set to true by default.
         *
         */
        logClickEvent: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: true }),
        /**
         * Whether to display incrementor button to increase/decrease the number of item to add into the cart
         *
         */
        displayIncrementors: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: true }),
        /**
         * Specifies the add to cart caption
         *
         */
        caption: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'Add To Cart' }),
        /**
         * Specifies the caption when the product is being added to the cart
         *
         */
        addingToCartCaption: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'Adding ...' }),
        /**
         * Specifies the method that determine if the user is logged in or not
         *
         */
        deactivate: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        /**
         * Specifies the method to be called when the product is successfully added to the cart
         *
         */
        confirmationCallback: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        /**
         * Specifies the method to be called when an error occurs when adding a product to the cart
         *
         */
        errorCallback: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
    };
    AddToCart = AddToCart_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component renders an "Add To Cart" button.
         * This component works with the ItemPrice component. Without the ItemPrice, the Add the cart will not render
         *
         * This component is a result template component (see [Result Templates](https://docs.coveo.com/en/413/)).
         *
         * @export
         * @class AddToCart
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], AddToCart);
    return AddToCart;
}(coveo_search_ui_1.Component));
exports.AddToCart = AddToCart;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleFitFilter = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var VehicleFitFilterSearch_1 = __webpack_require__(44);
var VehicleFitFilterQueryController_1 = __webpack_require__(48);
var VehicleFitFilterHeader_1 = __webpack_require__(49);
var CustomEvents_1 = __webpack_require__(4);
var VehicleFitVinSearch_1 = __webpack_require__(51);
var turbo_core_1 = __webpack_require__(1);
var underscore_1 = __webpack_require__(2);
var VehicleFitFilter = /** @class */ (function (_super) {
    __extends(VehicleFitFilter, _super);
    /**
     * Creates a new `VehicleFitFilter` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `VehicleFitFilter` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     */
    function VehicleFitFilter(element, options, bindings) {
        var _this = _super.call(this, element, VehicleFitFilter_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.enabled = true;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, VehicleFitFilter_1, options);
        _this.vehicleFitFilterQueryController = new VehicleFitFilterQueryController_1.VehicleFitFilterQueryController(_this);
        _this.numberOfValues = _this.options.numberOfValues;
        _this.vehicleFitFilterSearches = [];
        _this.isCollapsed = _this.options.enableCollapse && _this.options.collapsedByDefault;
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.buildingQuery, function (args) { return _this.handleBuildingQuery(args); });
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.querySuccess, function (args) { return _this.handleQuerySuccess(args); });
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.deferredQuerySuccess, function () { return _this.handleDeferredQuerySuccess(); });
        _this.bind.onRootElement(coveo_search_ui_1.InitializationEvents.afterComponentsInitialization, function () { return _this.render(); });
        window.addEventListener('resize', function () { return _this.toggleComponent(); });
        _this.buildFacetHeader();
        _this.initQueryStateEvents();
        _this.element.classList.toggle('coveops-facet-mode', _this.options.facetMode);
        _this.toggleComponent();
        return _this;
    }
    VehicleFitFilter_1 = VehicleFitFilter;
    VehicleFitFilter.prototype.handleDeferredQuerySuccess = function () {
        this.updateAppearance();
    };
    VehicleFitFilter.prototype.updateAppearance = function () {
        this.header.toggleClear(this.activePath.length > 0);
        this.header.toggleCollapse(this.isCollapsed);
        coveo_search_ui_1.$$(this.element).toggleClass('vehicle-fit-collapsed', this.isCollapsed);
        for (var i = 0; i < this.vehicleFitFilterSearches.length; i++) {
            if (i >= this.activePath.length) {
                this.vehicleFitFilterSearches[i].clear();
            }
            if (i > this.activePath.length) {
                this.vehicleFitFilterSearches[i].disable();
            }
            else {
                this.vehicleFitFilterSearches[i].enable();
            }
        }
    };
    /**
     * Expands the facet, displaying all of its currently fetched values.
     */
    VehicleFitFilter.prototype.expand = function () {
        if (!this.options.enableCollapse) {
            return this.logger.warn("Calling expand() won't do anything on a facet that has the option \"enableCollapse\" set to \"false\"");
        }
        if (!this.isCollapsed) {
            return;
        }
        this.ensureDom();
        this.logger.info('Expand facet values');
        this.isCollapsed = false;
        this.updateAppearance();
    };
    /**
     * Collapses the facet, displaying only its currently selected values.
     */
    VehicleFitFilter.prototype.collapse = function () {
        if (!this.options.enableCollapse) {
            return this.logger.warn("Calling collapse() won't do anything on a facet that has the option \"enableCollapse\" set to \"false\"");
        }
        if (this.isCollapsed) {
            return;
        }
        this.ensureDom();
        this.logger.info('Collapse facet values');
        this.isCollapsed = true;
        this.updateAppearance();
    };
    /**
     * Collapses or expands the facet depending on it's current state.
     */
    VehicleFitFilter.prototype.toggleCollapse = function () {
        this.isCollapsed ? this.expand() : this.collapse();
    };
    /**
     * Saves the decoded VIN information in a browser cookie.
     * If the "setCookieMethod" method is defined in the options, it will be used to save the cookie. Otherwise, the default Coveo.CookieUtil.set method will be used.
     *
     * If the user is logged in, its account ID will be added in the cookie value
     *
     * This is useful if the same code is being used in different env (Salesforce, Angular).
     *
     * @param {{ Year: string; Make: string; MakeDesc: string; Model: string; ModelDesc: string; Engine: string }} value
     */
    VehicleFitFilter.prototype.setVinCookie = function (value) {
        var _this = this;
        var cookieSetter = underscore_1.isFunction(this.options.setCookieMethod)
            ? function (name, value, expiration) { return _this.options.setCookieMethod(name, value, expiration); }
            : function (name, value, expiration) { return coveo_search_ui_1.Cookie.set(name, value, expiration * 1000 * 60 * 60 * 24); }; // converting milliseconds to days
        cookieSetter("" + this.options.cookiePrefix + this.options.cookieName, JSON.stringify(value), 1);
    };
    /**
     * Clears any the decoded vin from the browser cookies.
     *
     */
    VehicleFitFilter.prototype.clearVinCookie = function () {
        var _this = this;
        var cookieSetter = underscore_1.isFunction(this.options.setCookieMethod)
            ? function (name, value, expiration) { return _this.options.setCookieMethod(name, value, expiration); }
            : function (name, value, expiration) { return coveo_search_ui_1.Cookie.set(name, value, expiration * 1000 * 60 * 60 * 24); }; // converting milliseconds to days
        cookieSetter("" + this.options.cookiePrefix + this.options.cookieName, null, -1);
    };
    VehicleFitFilter.prototype.buildFacetHeader = function () {
        var _this = this;
        this.header = new VehicleFitFilterHeader_1.VehicleFitFilterHeader({
            title: this.options.title,
            enableCollapse: this.options.enableCollapse,
            clear: function () { return _this.reset(); },
            toggleCollapse: function () { return _this.toggleCollapse(); },
            collapse: function () { return _this.collapse(); },
            expand: function () { return _this.expand(); },
        });
        this.element.appendChild(this.header.element);
    };
    Object.defineProperty(VehicleFitFilter.prototype, "activePath", {
        get: function () {
            return this.queryStateModel.get(this.queryStateAttribute) || this.options.basePath;
        },
        set: function (newPath) {
            this.queryStateModel.set(this.queryStateAttribute, newPath);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(VehicleFitFilter.prototype, "queryStateAttribute", {
        get: function () {
            return coveo_search_ui_1.QueryStateModel.getFacetId(this.options.id);
        },
        enumerable: false,
        configurable: true
    });
    VehicleFitFilter.prototype.initQueryStateEvents = function () {
        var _this = this;
        this.queryStateModel.registerNewAttribute(this.queryStateAttribute, this.options.basePath);
        this.bind.onQueryState('change:', this.queryStateAttribute, function (data) { return _this.handleQueryStateChanged(data); });
    };
    VehicleFitFilter.prototype.handleQueryStateChanged = function (data) {
        var _this = this;
        var _a;
        this.activePath.map(function (level, idx) {
            _this.vehicleFitFilterSearches[idx].update(level);
        });
        // this.redrawComponent();
        this.header.updateSelectedValue(this.activePath.join(' '));
        this.header.toggleClear(this.activePath.length > 0);
        // DO NOT EXECUTE ANOTHER WHEN THE PATH CHANGE state. The query is executed on dropdown change
        if (this.vinSearch.lastStateChangeTriggeredByVinChange && ((_a = data === null || data === void 0 ? void 0 : data.value) === null || _a === void 0 ? void 0 : _a.length) > 0) {
            // Execute another search query only if the state was changed by a VIN search
            this.executeQuery(function () { return _this.logSearchEvent(CustomEvents_1.CustomEvents.vehicleFitSelect, _this.activePath); });
        }
        this.vinSearch.lastStateChangeTriggeredByVinChange = false;
    };
    VehicleFitFilter.prototype.resetPath = function () {
        this.changeActivePath(this.options.basePath);
        this.vehicleFitFilterSearches.map(function (v, idx) {
            if (idx !== 0) {
                v.disable();
            }
        });
    };
    /**
     * Resets the facet to its initial state.
     */
    VehicleFitFilter.prototype.reset = function () {
        var _this = this;
        this.resetPath();
        this.vinSearch.reset();
        this.header.toggleClear(this.activePath.length > 0);
        this.executeQuery(function () { return _this.logSearchEvent(CustomEvents_1.CustomEvents.vehicleFitClear, _this.activePath); }); // Reset search result
    };
    VehicleFitFilter.prototype.clearVinSearch = function () {
        this.vinSearch.reset();
    };
    VehicleFitFilter.prototype.render = function () {
        var filterSection = coveo_search_ui_1.$$('div', { className: 'coveo-vehicle-fit-content' });
        if (this.options.enableVinSearch) {
            var topSearch = coveo_search_ui_1.$$('div');
            // Building Vin search
            topSearch.append(this.buildVinSearch());
            // Building My Fleet
            topSearch.append(this.buildMyFleet());
            filterSection.append(topSearch.el);
        }
        // Building YMME filters
        filterSection.append(this.buildYMME());
        this.element.appendChild(filterSection.el);
        // TODO: remove that hack
        // this.element.classList.add('CoveoCategoryFacet');
        this.element.classList.add('coveo-category-facet-non-empty-path');
        // this.element.classList.add('coveo-category-facet-searching');
    };
    VehicleFitFilter.prototype.buildYMME = function () {
        var _this = this;
        var element = coveo_search_ui_1.$$('div');
        this.options.levels.map(function (label, idx) {
            var vehicleFitFilterSearch = new VehicleFitFilterSearch_1.VehicleFitFilterSearch(_this, idx, {
                placeholder: label,
                sort: _this.options.sorts[idx],
            });
            _this.vehicleFitFilterSearches.push(vehicleFitFilterSearch);
            element.append(vehicleFitFilterSearch.build().el);
        });
        return element.el;
    };
    VehicleFitFilter.prototype.buildMyFleet = function () {
        var element = coveo_search_ui_1.$$('div');
        return element.el;
    };
    VehicleFitFilter.prototype.buildVinSearch = function () {
        this.vinSearch = new VehicleFitVinSearch_1.VehicleFitVinSearch(this);
        return this.vinSearch.build();
    };
    /**
     * Changes the active path.
     *
     */
    VehicleFitFilter.prototype.changeActivePath = function (path) {
        this.setVinCookie({
            Year: path[0],
            Make: path[1],
            MakeDesc: path[1],
            Model: path[2],
            ModelDesc: path[2],
            Engine: path[3],
        });
        this.activePath = path;
    };
    VehicleFitFilter.prototype.executeQuery = function (customEvent) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, , 2, 3]);
                        // if (customEvent) {
                        // if (this.enabled) {
                        return [4 /*yield*/, this.queryController.executeQuery({ beforeExecuteQuery: customEvent, ignoreWarningSearchEvent: true })];
                    case 1:
                        // if (customEvent) {
                        // if (this.enabled) {
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2: return [7 /*endfinally*/];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    VehicleFitFilter.prototype.logSearchEvent = function (eventName, path) {
        this.usageAnalytics.logSearchEvent({ name: eventName, type: 'customEventType' }, {
            vehicleFitField: this.options.field.toString(),
            vehicleFitPath: path.join(this.options.delimitingCharacter),
            vehicleFitTitle: this.options.title,
        });
    };
    VehicleFitFilter.prototype.handleQuerySuccess = function (args) {
        // Always show the component - https://dev.azure.com/CoveoPS/FleetPride/_workitems/edit/169505
        // this.element.classList.toggle('hidden', args.results.results.length === 0);
    };
    VehicleFitFilter.prototype.handleBuildingQuery = function (args) {
        // This is required since we are using a category facet instead of the new facet search API
        // This will be removed anyway once we switch to the new DynamicHierachicalFacet
        // Blocked by https://coveord.atlassian.net/browse/SEARCHAPI-4998
        if (this.enabled) {
            // That way, the mobile and desktop version are not both updating the query
            this.positionInQuery = this.vehicleFitFilterQueryController.putCategoryFacetInQueryBuilder(args.queryBuilder, this.activePath, this.numberOfValues + 1);
        }
    };
    VehicleFitFilter.prototype.isMobile = function () {
        var viewportWidth = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
        return viewportWidth <= this.options.responsiveBreakpoint;
    };
    VehicleFitFilter.prototype.toggleComponent = function () {
        // XNOR logic.
        // this.disable() and this.enable() function from Base component does not seem to work as expected
        if (this.isMobile() && this.options.facetMode) {
            // mobile
            this.enabled = true;
        }
        else if (!this.isMobile() && !this.options.facetMode) {
            // desktop
            this.enabled = true;
        }
        else {
            this.enabled = false;
        }
    };
    var VehicleFitFilter_1;
    VehicleFitFilter.ID = 'VehicleFitFilter';
    VehicleFitFilter.WAIT_ELEMENT_CLASS = 'coveo-category-facet-header-wait-animation';
    VehicleFitFilter.options = {
        /**
         * A unique identifier for the facet. Among other things, this identifier serves the purpose of saving
         * the facet state in the URL hash.
         *
         * If you have two facets with the same field on the same page, you should specify an `id` value for at least one of
         * those two facets. This `id` must be unique among the facets.
         *
         * Default value is the [`field`]{@link CategoryFacet.options.field} option value.
         */
        id: coveo_search_ui_1.ComponentOptions.buildStringOption({
            postProcessing: function (value, options) { return value || options.field; },
        }),
        /**
         * Whether to allow the end-user to expand and collapse this facet.
         */
        enableCollapse: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: true }),
        /**
         * Whether to save the decoded vin in a cookie
         */
        saveVinInCookie: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: true }),
        /**
         * If saveVinInCookie is set to true, the component will save the decoded in a cookie with the specified name
         */
        cookieName: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'VINResponse' }),
        /**
         *
         * Whether to add a specific prefix to the cookie
         */
        cookiePrefix: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: '' }),
        /**
         * Method to get specific cookie
         */
        getCookieMethod: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        /**
         * Method to set a cookie
         */
        setCookieMethod: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        /**
         * Whether to render the component in the facet section.
         */
        facetMode: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: false }),
        /**
         * Whether this facet should be collapsed by default.
         */
        collapsedByDefault: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: false, depend: 'enableCollapse' }),
        /**
         * The title to display at the top of the facet.
         *
         * Default value is the localized string for `NoTitle`.
         */
        title: coveo_search_ui_1.ComponentOptions.buildLocalizedStringOption({
            localizedString: function () { return coveo_search_ui_1.l('Vehicle Fit'); },
        }),
        /**
         * The placeholder to display in the VIN search input
         *
         * Default value is the localized string for `VIN (Optional)`.
         */
        vinSearchPlaceholder: coveo_search_ui_1.ComponentOptions.buildLocalizedStringOption({
            localizedString: function () { return coveo_search_ui_1.l('VIN (Optional)'); },
        }),
        /**
         * If the [`enableFacetSearch`]{@link CategoryFacet.options.enableFacetSearch} option is `true`, specifies the delay (in
         * milliseconds) before sending a search request to the server when the user starts typing in the category facet search box.
         *
         * Specifying a smaller value makes results appear faster. However, chances of having to cancel many requests
         * sent to the server increase as the user keeps on typing new characters.
         *
         * Default value is `100`. Minimum value is `0`.
         */
        facetSearchDelay: coveo_search_ui_1.ComponentOptions.buildNumberOption({ defaultValue: 100, min: 0 }),
        /**
         * The maximum number of field values to display by default in the facet before the user
         * clicks the arrow to show more.
         *
         * See also the [`enableMoreLess`]{@link CategoryFacet.options.enableMoreLess} option.
         */
        numberOfValues: coveo_search_ui_1.ComponentOptions.buildNumberOption({ defaultValue: 5, min: 0 }),
        /**
         * The breakpoint in pixel at which the mobile version of this component should take over.
         *
         */
        responsiveBreakpoint: coveo_search_ui_1.ComponentOptions.buildNumberOption({ defaultValue: 768, min: 0 }),
        /**
         * If the [`enableFacetSearch`]{@link CategoryFacet.options.enableFacetSearch} option is `true`, specifies the number of
         * values to display in the facet search results popup.
         *
         * Default value is `15`. Minimum value is `1`.
         */
        numberOfResultsInFacetSearch: coveo_search_ui_1.ComponentOptions.buildNumberOption({ defaultValue: 25, min: 1 }),
        /**
         * Specifies the field used to determine which icon to use
         *
         */
        field: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_imageurl' }),
        /**
         * Specifies the level names of the hierarchy structure. Each level in the list will create a dropdown
         *
         */
        levels: coveo_search_ui_1.ComponentOptions.buildListOption({ defaultValue: ['Year', 'Make', 'Model', 'Engine'] }),
        /**
         * Specifies the sort criteria per level.
         *
         */
        sorts: coveo_search_ui_1.ComponentOptions.buildListOption({
            defaultValue: ['alphadescending', 'alphaascending', 'alphaascending', 'alphaascending'],
        }),
        /**
         * If set to true, the component will allow the user to search for a fitment using a vehicle VIN.
         *
         */
        enableVinSearch: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: false }),
        /**
         * Specifies the remote action to be called on every vin search.
         *
         */
        vinSearchRemoteAction: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        /**
         * Specifies an additional Coveo filter to include on every request generated by this component.
         * One use case would be to always include Generic parts
         *
         */
        additionalFilter: coveo_search_ui_1.ComponentOptions.buildStringOption(),
        /**
         * Specifies the minimum number of characters needed to execute a VIN search
         *
         */
        minimumVinCharacthers: coveo_search_ui_1.ComponentOptions.buildNumberOption({ defaultValue: 17 }),
        /**
         * Specifies the text placeholder to display inside the VIN input element when empty
         *
         */
        vinCaption: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'VIN' }),
        /**
         * The *injection depth* to use.
         *
         * The injection depth determines how many results to scan in the index to ensure that the category facet lists all potential
         * facet values. Increasing this value enhances the accuracy of the listed values at the cost of performance.
         *
         * Default value is `1000`. Minimum value is `0`.
         */
        injectionDepth: coveo_search_ui_1.ComponentOptions.buildNumberOption({ defaultValue: 1000, min: 0 }),
        /**
         * The character that specifies the hierarchical dependency.
         *
         * **Example:**
         *
         * If your field has the following values:
         *
         * `@field: c; c>folder2; c>folder2>folder3;`
         *
         * The delimiting character is `>`.
         *
         * Default value is `|`.
         */
        delimitingCharacter: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: '|' }),
        /**
         * The path to use as the path prefix for every query.
         *
         * **Example:**
         *
         * You have the following files indexed on a file system:
         * ```
         * c:\
         *    folder1\
         *      text1.txt
         *    folder2\
         *      folder3\
         *        text2.txt
         * ```
         * Setting the `basePath` to `c` would display `folder1` and `folder2` in the `CategoryFacet`, but omit `c`.
         *
         * This options accepts an array of values. To specify a "deeper" starting path in your tree, you need to use comma-separated values.
         *
         * For example, setting `data-base-path="c,folder1"` on the component markup would display `folder3` in the `CategoryFacet`, but omit `c` and `folder1`.
         *
         */
        basePath: coveo_search_ui_1.ComponentOptions.buildListOption({ defaultValue: [] }),
    };
    VehicleFitFilter = VehicleFitFilter_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component renders a Facet like component to filter product by Year, Model, Make, engine (Product Terminology)
         *
         * The VehicleFitFilter is probaly the most complex component of this implemantion. It controls the YMME dropdowns as well as the VIN search input
         *
         * @export
         * @class VehicleFitFilter
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], VehicleFitFilter);
    return VehicleFitFilter;
}(coveo_search_ui_1.Component));
exports.VehicleFitFilter = VehicleFitFilter;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleFitFilterHeaderButton = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var SVGDom_1 = __webpack_require__(6);
var VehicleFitFilterHeaderButton = /** @class */ (function () {
    function VehicleFitFilterHeaderButton(rootOptions) {
        this.rootOptions = rootOptions;
        this.create();
    }
    VehicleFitFilterHeaderButton.prototype.create = function () {
        var hasIcon = this.rootOptions.iconSVG && this.rootOptions.iconClassName;
        this.button = coveo_search_ui_1.$$('button', {
            className: ("coveo-dynamic-facet-header-btn " + (this.rootOptions.className || '')).trim(),
            type: 'button',
        }, hasIcon ? this.rootOptions.iconSVG : this.rootOptions.label);
        this.rootOptions.action && this.button.on('click', this.rootOptions.action);
        if (hasIcon) {
            this.button.setAttribute('aria-label', this.rootOptions.label);
            this.button.setAttribute('title', this.rootOptions.label);
            SVGDom_1.SVGDom.addClassToSVGInContainer(this.button.el, this.rootOptions.iconClassName);
        }
        if (this.rootOptions.ariaLabel) {
            this.button.setAttribute('aria-label', this.rootOptions.ariaLabel);
        }
        if (this.rootOptions.shouldDisplay !== undefined) {
            this.toggle(this.rootOptions.shouldDisplay);
        }
        this.element = this.button.el;
    };
    VehicleFitFilterHeaderButton.prototype.toggle = function (shouldDisplay) {
        this.button.toggle(shouldDisplay);
    };
    return VehicleFitFilterHeaderButton;
}());
exports.VehicleFitFilterHeaderButton = VehicleFitFilterHeaderButton;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveToFavorites = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var SVGIcons_1 = __webpack_require__(3);
var SaveToFavorites = /** @class */ (function (_super) {
    __extends(SaveToFavorites, _super);
    /**
     * Creates a new `SaveToFavorites` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `SaveToFavorites` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     * @param result The result to associate the component with.
     */
    function SaveToFavorites(element, options, bindings, result) {
        var _this = _super.call(this, element, SaveToFavorites_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.result = result;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, SaveToFavorites_1, options);
        _this.build();
        return _this;
    }
    SaveToFavorites_1 = SaveToFavorites;
    SaveToFavorites.prototype.build = function () {
        var _this = this;
        var star = coveo_search_ui_1.$$('div', {}, SVGIcons_1.SVGIcons.icons.star);
        star.on('click', function () { return _this.handleClick(); });
        this.element.appendChild(star.el);
    };
    SaveToFavorites.prototype.handleClick = function () {
        this.logCustomEvent();
        this.saveProduct();
    };
    SaveToFavorites.prototype.logCustomEvent = function () {
        // TODO: log custom event
    };
    SaveToFavorites.prototype.saveProduct = function () {
        // TODO: save this.result to favorites
    };
    var SaveToFavorites_1;
    SaveToFavorites.ID = 'SaveToFavorites';
    SaveToFavorites.options = {
        /**
         * Text value for the SaveToFavorites button
         *
         */
        caption: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'Add To Cart' }),
    };
    SaveToFavorites = SaveToFavorites_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component renders an button to save the product into the user's favorites
         *
         * This component is a result template component (see [Result Templates](https://docs.coveo.com/en/413/)).
         *
         * @export
         * @class SaveToFavorites
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], SaveToFavorites);
    return SaveToFavorites;
}(coveo_search_ui_1.Component));
exports.SaveToFavorites = SaveToFavorites;


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.PriceManager = void 0;
var underscore_1 = __webpack_require__(2);
var CoveoUtils_1 = __webpack_require__(5);
var PriceManager = /** @class */ (function () {
    function PriceManager(remoteActionManager) {
        this.remoteActionManager = remoteActionManager;
    }
    PriceManager.prototype.getPricePayload = function (results, partNumberField, poolNumberField, quantity) {
        if (quantity === void 0) { quantity = 1; }
        var payload = results.map(function (result) {
            var partNumberValue = CoveoUtils_1.CoveoUtils.getCleanFieldValue(result, partNumberField);
            var poolNumberValue = CoveoUtils_1.CoveoUtils.getCleanFieldValue(result, poolNumberField);
            if (partNumberValue && poolNumberValue) {
                return {
                    partNumber: partNumberValue.toString(),
                    poolNumber: poolNumberValue.toString(),
                    quantity: quantity.toString(),
                };
            }
            else {
                return null;
            }
        });
        return underscore_1.compact(payload);
    };
    PriceManager.prototype.getCustomerPricePayload = function (results) {
        // TODO: use options instead
        return this.getPricePayload(results, '@fp_prd_dsppartnumber', '@fp_prd_poolnumber');
    };
    PriceManager.prototype.getCoreChargePayload = function (results) {
        // TODO: use options instead
        return this.getPricePayload(results, '@fp_prd_core_partnumber', '@fp_prd_core_poolnumber');
    };
    PriceManager.prototype.parsePrices = function (priceBlob) {
        try {
            return JSON.parse(priceBlob);
        }
        catch (error) {
            this.remoteActionManager.logger.error('Unable to parse prices');
        }
    };
    return PriceManager;
}());
exports.PriceManager = PriceManager;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.AvailabilityManager = void 0;
var underscore_1 = __webpack_require__(2);
var CoveoUtils_1 = __webpack_require__(5);
var AvailabilityManager = /** @class */ (function () {
    function AvailabilityManager(remoteActionManager) {
        this.remoteActionManager = remoteActionManager;
    }
    AvailabilityManager.prototype.getSkus = function (results, skuField) {
        var skus = results.map(function (result) { return CoveoUtils_1.CoveoUtils.getCleanFieldValue(result, skuField); });
        return underscore_1.compact(skus);
    };
    return AvailabilityManager;
}());
exports.AvailabilityManager = AvailabilityManager;


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.PSversion = void 0;
exports.PSversion = '0.8.0';
Coveo.version.ps = exports.PSversion;


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// Webpack output a library target with a temporary name.
// It does not take care of merging the namespace if the global variable already exists.
// If another piece of code in the page use the Coveo namespace (eg: extension), then they get overwritten
// This code swap the current module to the "real" Coveo variable, without overwriting the whole global var.
// This is to allow end user to put CoveoPSComponents.js before or after the main CoveoJsSearch.js, without breaking
Object.defineProperty(exports, "__esModule", { value: true });
exports.swapVar = void 0;
function swapVar(scope) {
    if (window['Coveo'] == undefined) {
        window['Coveo'] = scope;
    }
    else {
        _.each(_.keys(scope), function (k) {
            window['Coveo'][k] = scope[k];
        });
    }
}
exports.swapVar = swapVar;


/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "js/cultures/en.js");

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "js/cultures/fr.js");

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "js/cultures/es-es.js");

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleFitFilterSearch = void 0;
var underscore_1 = __webpack_require__(2);
var coveo_search_ui_1 = __webpack_require__(0);
var VehicleFitSearchElement_1 = __webpack_require__(45);
var VehicleFitFilter_1 = __webpack_require__(34);
var CustomEvents_1 = __webpack_require__(4);
var VehicleFitFilterSearch = /** @class */ (function () {
    function VehicleFitFilterSearch(vehicleFitFilter, level, options) {
        var _this = this;
        this.vehicleFitFilter = vehicleFitFilter;
        this.level = level;
        this.options = options;
        this.moreValuesToFetch = true;
        this.facetSearchElement = new VehicleFitSearchElement_1.VehicleFitSearchElement(this, options);
        this.displayNewValues = underscore_1.debounce(this.getDisplayNewValuesFunction(), this.vehicleFitFilter.options.facetSearchDelay);
        this.vehicleFitFilter.root.addEventListener('click', function (e) { return _this.handleClickElsewhere(e); });
        this.numberOfValuesToFetch = this.vehicleFitFilter.options.numberOfResultsInFacetSearch;
        this.currentlyDisplayedResults = null;
    }
    Object.defineProperty(VehicleFitFilterSearch.prototype, "facetType", {
        get: function () {
            return VehicleFitFilter_1.VehicleFitFilter.ID;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(VehicleFitFilterSearch.prototype, "facetTitle", {
        get: function () {
            return this.vehicleFitFilter.options.title || this.vehicleFitFilter.options.field.toString();
        },
        enumerable: false,
        configurable: true
    });
    VehicleFitFilterSearch.prototype.setExpandedFacetSearchAccessibilityAttributes = function (searchResultsElements) {
        this.container.setAttribute('aria-expanded', 'true');
    };
    VehicleFitFilterSearch.prototype.setCollapsedFacetSearchAccessibilityAttributes = function () {
        this.container.setAttribute('aria-expanded', 'false');
    };
    VehicleFitFilterSearch.prototype.enable = function () {
        this.facetSearchElement.input.removeAttribute('disabled');
        this.container.removeClass('coveo-disabled-search-input');
    };
    VehicleFitFilterSearch.prototype.disable = function () {
        this.facetSearchElement.input.setAttribute('disabled', 'disabled');
        this.container.addClass('coveo-disabled-search-input');
    };
    VehicleFitFilterSearch.prototype.clear = function () {
        this.facetSearchElement.clearSearchInput();
    };
    VehicleFitFilterSearch.prototype.update = function (term) {
        this.facetSearchElement.updateSearchInput(term);
    };
    VehicleFitFilterSearch.prototype.build = function () {
        this.container = coveo_search_ui_1.$$('div', {
            className: 'coveo-vehicle-fit-search-container',
            role: 'button',
        });
        this.container.append(this.facetSearchElement.build());
        coveo_search_ui_1.$$(this.facetSearchElement.search).addClass('without-animation');
        if (this.level !== 0) {
            // Only display the initial dropdown to start with
            this.disable();
        }
        return this.container;
    };
    VehicleFitFilterSearch.prototype.dismissSearchResults = function () {
        this.removeNoResultsCssClasses();
        coveo_search_ui_1.$$(this.facetSearchElement.searchResults).empty();
        this.facetSearchElement.hideSearchResultsElement();
        this.currentlyDisplayedResults = null;
        this.numberOfValuesToFetch = this.vehicleFitFilter.options.numberOfResultsInFacetSearch;
        this.moreValuesToFetch = true;
    };
    VehicleFitFilterSearch.prototype.keyboardEventDefaultHandler = function () {
        this.moreValuesToFetch = true;
        this.displayNewValues();
    };
    VehicleFitFilterSearch.prototype.keyboardNavigationEnterPressed = function () {
        this.selectCurrentResult();
        this.dismissSearchResults();
    };
    VehicleFitFilterSearch.prototype.fetchMoreValues = function () {
        // TODO: FIX the count since we do not append every results
        this.numberOfValuesToFetch += this.vehicleFitFilter.options.numberOfResultsInFacetSearch;
        this.displayNewValues();
    };
    VehicleFitFilterSearch.prototype.getCaptions = function () {
        var searchResults = coveo_search_ui_1.$$(this.facetSearchElement.searchResults);
        var captions = searchResults
            .findAll('.coveo-category-facet-search-value-caption')
            .concat(searchResults.findAll('.coveo-category-facet-search-path-parents'))
            .concat(searchResults.findAll('.coveo-category-facet-search-path-last-value'));
        return captions;
    };
    VehicleFitFilterSearch.prototype.updateAriaLive = function (text) {
        this.vehicleFitFilter.searchInterface.ariaLive.updateText(text);
    };
    VehicleFitFilterSearch.prototype.selectCurrentResult = function () {
        var _this = this;
        if (this.facetSearchElement.currentResult) {
            var currentResultPathData = this.facetSearchElement.currentResult.el.dataset.path;
            var delimiter = this.vehicleFitFilter.options.delimitingCharacter;
            var path_1 = currentResultPathData.split(delimiter);
            this.vehicleFitFilter.changeActivePath(path_1);
            this.vehicleFitFilter.executeQuery(function () { return _this.vehicleFitFilter.logSearchEvent(CustomEvents_1.CustomEvents.vehicleFitSelect, path_1); });
            this.facetSearchElement.updateSearchInput(this.getValueFromPathAndLevel(path_1));
        }
    };
    VehicleFitFilterSearch.prototype.getValueFromPathAndLevel = function (path) {
        return path[this.level] ? path[this.level] : underscore_1.last(path);
    };
    VehicleFitFilterSearch.prototype.handleClickElsewhere = function (e) {
        // const closestContainer = $$(e.target as HTMLElement).closest('.coveo-category-facet-search-container'); // TODO: remove
        var closestContainer = coveo_search_ui_1.$$(e.target).closest('.coveo-vehicle-fit-search-container');
        var isSelfContainer = this.container && closestContainer === this.container.el;
        if (!closestContainer || !isSelfContainer) {
            this.dismissSearchResults();
        }
    };
    VehicleFitFilterSearch.prototype.getSearchInputValue = function () {
        return this.facetSearchElement.inputValue;
    };
    VehicleFitFilterSearch.prototype.getDisplayNewValuesFunction = function () {
        var _this = this;
        return function () { return __awaiter(_this, void 0, void 0, function () {
            var categoryFacetValues;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.facetSearchElement.showFacetSearchWaitingAnimation();
                        this.vehicleFitFilter.logger.info('Triggering new Category Facet search');
                        return [4 /*yield*/, this.vehicleFitFilter.vehicleFitFilterQueryController.searchFacetValues(this.getSearchInputValue(), this.vehicleFitFilter.activePath, this.numberOfValuesToFetch, this.level, this.options.sort, this.vehicleFitFilter.options.delimitingCharacter)];
                    case 1:
                        categoryFacetValues = _a.sent();
                        // this.logCustomSearchEvent(); // To verbose
                        if (categoryFacetValues.length < this.numberOfValuesToFetch) {
                            this.moreValuesToFetch = false;
                        }
                        if (categoryFacetValues.length === 0) {
                            this.noFacetSearchResults();
                            return [2 /*return*/];
                        }
                        this.removeNoResultsCssClasses();
                        this.setFacetSearchResults(categoryFacetValues);
                        if (this.shouldPositionSearchResults) {
                            this.facetSearchElement.positionSearchResults();
                        }
                        this.facetSearchElement.hideFacetSearchWaitingAnimation();
                        return [2 /*return*/];
                }
            });
        }); };
    };
    VehicleFitFilterSearch.prototype.resetCurrentLevel = function () {
        // Do not refresh search results while the user is searching
        if (this.vehicleFitFilter.activePath.length >= this.level + 1) {
            this.vehicleFitFilter.activePath = this.vehicleFitFilter.activePath.slice(0, this.level + 1);
        }
        this.vehicleFitFilter.updateAppearance();
    };
    VehicleFitFilterSearch.prototype.isResultPartOfTheLevel = function (categoryFacetValue) {
        var path = categoryFacetValue.value.split(this.vehicleFitFilter.options.delimitingCharacter);
        return path.length === this.level + 1 && path[this.level].toLowerCase().match(this.getSearchInputValue().toLowerCase()) != null;
    };
    VehicleFitFilterSearch.prototype.setFacetSearchResults = function (categoryFacetValues) {
        coveo_search_ui_1.$$(this.facetSearchElement.searchResults).empty();
        this.currentlyDisplayedResults = underscore_1.pluck(categoryFacetValues, 'value');
        for (var i = 0; i < categoryFacetValues.length; i++) {
            var searchResult = this.buildFacetSearchValue(categoryFacetValues[i], i);
            if (i === 0) {
                this.facetSearchElement.setAsCurrentResult(searchResult);
            }
            if (this.isResultPartOfTheLevel(categoryFacetValues[i])) {
                this.facetSearchElement.appendToSearchResults(searchResult.el);
            }
        }
        this.highlightCurrentQueryWithinSearchResults();
    };
    VehicleFitFilterSearch.prototype.getFormattedCount = function (count) {
        return Globalize.format(count, 'n0');
    };
    VehicleFitFilterSearch.prototype.buildFacetSearchValue = function (categoryFacetValue, index) {
        var _this = this;
        var path = categoryFacetValue.value.split(this.vehicleFitFilter.options.delimitingCharacter);
        var valueElement = coveo_search_ui_1.$$('span', { className: 'coveo-category-facet-search-value-caption' }, this.getValueFromPathAndLevel(path));
        var num = coveo_search_ui_1.$$('span', { className: 'coveo-category-facet-search-value-number' }, this.getFormattedCount(categoryFacetValue.numberOfResults));
        var firstRow = coveo_search_ui_1.$$('div', { className: 'coveo-category-facet-search-first-row' }, valueElement, num);
        var item = coveo_search_ui_1.$$('li', {
            id: "coveo-category-facet-search-suggestion-" + index,
            role: 'option',
            ariaSelected: 'false',
            className: 'coveo-category-facet-search-value',
            title: path,
        }, firstRow);
        item.el.dataset.path = categoryFacetValue.value;
        var countLabel = coveo_search_ui_1.l('ResultCount', this.getFormattedCount(categoryFacetValue.numberOfResults));
        var label = coveo_search_ui_1.l('SelectValueWithResultCount', underscore_1.last(path), countLabel); // TODO: figure out what this is
        // const button = $$('button', {}, label);
        item.on('click', function () {
            _this.vehicleFitFilter.changeActivePath(path);
            _this.vehicleFitFilter.executeQuery(function () { return _this.vehicleFitFilter.logSearchEvent(CustomEvents_1.CustomEvents.vehicleFitSelect, path); });
            _this.dismissSearchResults();
            _this.facetSearchElement.updateSearchInput(_this.getValueFromPathAndLevel(path));
        });
        return item;
    };
    VehicleFitFilterSearch.prototype.noFacetSearchResults = function () {
        this.facetSearchElement.hideFacetSearchWaitingAnimation();
        this.facetSearchElement.hideSearchResultsElement();
        coveo_search_ui_1.$$(this.facetSearchElement.search).addClass('coveo-facet-search-no-results');
        coveo_search_ui_1.$$(this.vehicleFitFilter.element).addClass('coveo-no-results');
    };
    // TODO: fix css if no results instead of showing an empty dropdown
    VehicleFitFilterSearch.prototype.removeNoResultsCssClasses = function () {
        this.facetSearchElement.search && coveo_search_ui_1.$$(this.facetSearchElement.search).removeClass('coveo-facet-search-no-results');
        coveo_search_ui_1.$$(this.vehicleFitFilter.element).removeClass('coveo-no-results');
    };
    VehicleFitFilterSearch.prototype.highlightCurrentQueryWithinSearchResults = function () {
        var regex = new RegExp("(" + coveo_search_ui_1.StringUtils.stringToRegex(this.getSearchInputValue(), true) + ")", 'ig');
        this.facetSearchElement.highlightCurrentQueryInSearchResults(regex);
    };
    Object.defineProperty(VehicleFitFilterSearch.prototype, "shouldPositionSearchResults", {
        get: function () {
            var searchResults = this.facetSearchElement.searchResults;
            return searchResults && !searchResults.parentElement;
        },
        enumerable: false,
        configurable: true
    });
    return VehicleFitFilterSearch;
}());
exports.VehicleFitFilterSearch = VehicleFitFilterSearch;


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleFitSearchElement = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var DropdownNavigator_1 = __webpack_require__(46);
var SVGDom_1 = __webpack_require__(6);
var KeyboardUtils_1 = __webpack_require__(10);
var SVGIcons_1 = __webpack_require__(3);
var VehicleFitSearchUserInputHandler_1 = __webpack_require__(47);
var VehicleFitSearchElement = /** @class */ (function () {
    function VehicleFitSearchElement(facetSearch, options) {
        this.facetSearch = facetSearch;
        this.options = options;
        // private keyPressed = false;
        this.lastSearchTerm = '';
        this.triggeredScroll = false;
        this.facetSearchId = underscore_1.uniqueId('coveo-vehicle-fit-search-results');
        this.facetSearchUserInputHandler = new VehicleFitSearchUserInputHandler_1.VehicleFitSearchUserInputHandler(this.facetSearch);
        this.initSearchResults();
    }
    VehicleFitSearchElement.prototype.build = function () {
        var _this = this;
        this.search = document.createElement('div');
        coveo_search_ui_1.$$(this.search).addClass('coveo-vehicle-fit-search');
        this.dropdownIcon = this.buildDownArrowIcon();
        this.search.appendChild(this.dropdownIcon);
        this.wait = this.buildWaitIcon();
        this.search.appendChild(this.wait);
        this.hideFacetSearchWaitingAnimation();
        this.combobox = this.buildCombobox();
        this.search.appendChild(this.combobox);
        this.input = this.buildInputElement();
        coveo_search_ui_1.Component.pointElementsToDummyForm(this.input);
        this.combobox.appendChild(this.input);
        this.input.onkeyup = function (e) { return _this.facetSearchUserInputHandler.handleKeyboardEvent(e); };
        this.input.onfocus = function () { return _this.handleFacetSearchFocus(); };
        this.input.onblur = function () { return _this.handleFacetSearchBlur(); };
        this.dropdownIcon.onclick = function () { return _this.input.focus(); };
        this.initSearchDropdownNavigator();
        return this.search;
    };
    VehicleFitSearchElement.prototype.initSearchResults = function () {
        var _this = this;
        this.searchResults = coveo_search_ui_1.$$('ul', { id: this.facetSearchId, className: 'coveo-facet-search-results', role: 'listbox' }).el;
        coveo_search_ui_1.$$(this.searchResults).on('scroll', function () { return _this.handleScrollEvent(); });
        coveo_search_ui_1.$$(this.searchResults).on('keyup', function (e) {
            if (e.which === KeyboardUtils_1.KEYBOARD.ESCAPE) {
                _this.facetSearch.dismissSearchResults();
            }
        });
        coveo_search_ui_1.$$(this.searchResults).hide();
    };
    VehicleFitSearchElement.prototype.initSearchDropdownNavigator = function () {
        var _this = this;
        var config = {
            input: this.input,
            searchResults: this.searchResults,
            setScrollTrigger: function (val) { return (_this.triggeredScroll = val); },
        };
        this.searchDropdownNavigator = new DropdownNavigator_1.DropdownNavigator(config);
    };
    VehicleFitSearchElement.prototype.buildCombobox = function () {
        return coveo_search_ui_1.$$('div', {
            className: 'coveo-vehicle-fit-search-middle',
            ariaHaspopup: 'listbox',
            ariaExpanded: 'true',
        }).el;
    };
    VehicleFitSearchElement.prototype.showFacetSearchWaitingAnimation = function () {
        coveo_search_ui_1.$$(this.wait).show();
    };
    VehicleFitSearchElement.prototype.getValueInInputForFacetSearch = function () {
        return this.input.value.trim();
    };
    VehicleFitSearchElement.prototype.hideFacetSearchWaitingAnimation = function () {
        coveo_search_ui_1.$$(this.dropdownIcon).show();
        coveo_search_ui_1.$$(this.wait).hide();
    };
    VehicleFitSearchElement.prototype.positionSearchResults = function () {
        if (this.searchResults != null) {
            coveo_search_ui_1.$$(this.searchResults).insertAfter(this.search);
            coveo_search_ui_1.$$(this.searchResults).show();
            if (coveo_search_ui_1.$$(this.searchResults).css('display') === 'none') {
                this.searchResults.style.display = '';
            }
            // const searchBar = $$(this.search);
            // if (searchBar.css('display') === 'none') { // TODO: why display would be none??
            //   if ($$(this.searchResults).css('display') === 'none') {
            //     this.searchResults.style.display = '';
            //   }
            //   EventsUtils.addPrefixedEvent(this.search, 'AnimationEnd', () => {
            //     EventsUtils.removePrefixedEvent(this.search, 'AnimationEnd', this);
            //   });
            // }
        }
        this.addAriaAttributes();
    };
    VehicleFitSearchElement.prototype.setAsCurrentResult = function (toSet) {
        this.searchDropdownNavigator.setAsCurrentResult(toSet);
    };
    Object.defineProperty(VehicleFitSearchElement.prototype, "inputValue", {
        get: function () {
            // return this.keyPressed ? this.input.value : '';
            return this.input.value;
        },
        set: function (value) {
            if (value) {
                this.input.value = value;
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(VehicleFitSearchElement.prototype, "currentResult", {
        get: function () {
            return this.searchDropdownNavigator.currentResult;
        },
        enumerable: false,
        configurable: true
    });
    VehicleFitSearchElement.prototype.moveCurrentResultDown = function () {
        this.searchDropdownNavigator.focusNextElement();
    };
    VehicleFitSearchElement.prototype.moveCurrentResultUp = function () {
        this.searchDropdownNavigator.focusPreviousElement();
    };
    VehicleFitSearchElement.prototype.highlightCurrentQueryInSearchResults = function (regex) {
        var captions = this.facetSearch.getCaptions();
        captions.forEach(function (caption) {
            caption.innerHTML = coveo_search_ui_1.$$(caption).text().replace(regex, '<span class="coveo-highlight">$1</span>');
        });
    };
    VehicleFitSearchElement.prototype.appendToSearchResults = function (el) {
        this.searchResults.appendChild(el);
        this.setupFacetSearchResultsEvents(el);
    };
    VehicleFitSearchElement.prototype.focus = function () {
        this.input.focus();
        this.handleFacetSearchFocus();
    };
    VehicleFitSearchElement.prototype.handleFacetSearchBlur = function () {
        this.inputValue = this.lastSearchTerm;
    };
    VehicleFitSearchElement.prototype.handleFacetSearchFocus = function () {
        this.lastSearchTerm = this.inputValue;
        this.facetSearch.resetCurrentLevel();
        this.clearSearchInput();
        if (this.facetSearch.currentlyDisplayedResults === null) {
            this.facetSearch.displayNewValues();
            this.addAriaAttributes();
        }
    };
    VehicleFitSearchElement.prototype.setupFacetSearchResultsEvents = function (el) {
        var _this = this;
        coveo_search_ui_1.$$(el).on('mousemove', function () {
            _this.setAsCurrentResult(coveo_search_ui_1.$$(el));
        });
        // Prevent closing the search results on the end of a touch drag
        var touchDragging = false;
        var mouseDragging = false;
        coveo_search_ui_1.$$(el).on('mousedown', function () { return (mouseDragging = false); });
        coveo_search_ui_1.$$(el).on('mousemove', function () { return (mouseDragging = true); });
        coveo_search_ui_1.$$(el).on('touchmove', function () { return (touchDragging = true); });
        coveo_search_ui_1.$$(el).on('mouseup touchend', function () {
            if (!touchDragging && !mouseDragging) {
                setTimeout(function () {
                    _this.facetSearch.dismissSearchResults();
                }, 0); // setTimeout is to give time to trigger the click event before hiding the search menu.
            }
            touchDragging = false;
            mouseDragging = false;
        });
    };
    VehicleFitSearchElement.prototype.hideSearchResultsElement = function () {
        this.removeAriaAttributes();
        coveo_search_ui_1.$$(this.searchResults).hide();
        coveo_search_ui_1.$$(this.searchResults).remove();
    };
    VehicleFitSearchElement.prototype.updateSearchInput = function (input) {
        if (this.input) {
            this.input.value = input;
        }
    };
    VehicleFitSearchElement.prototype.clearSearchInput = function () {
        if (this.input) {
            this.input.value = '';
        }
    };
    VehicleFitSearchElement.prototype.buildDownArrowIcon = function () {
        var downArrow = document.createElement('div');
        downArrow.innerHTML = SVGIcons_1.SVGIcons.icons.arrowDown;
        coveo_search_ui_1.$$(downArrow).addClass('coveo-vehicle-fit-search-arrow');
        SVGDom_1.SVGDom.addClassToSVGInContainer(downArrow, 'coveo-vehicle-fit-search-arrow-svg');
        this.search.appendChild(downArrow);
        return downArrow;
    };
    VehicleFitSearchElement.prototype.buildWaitIcon = function () {
        var wait = document.createElement('div');
        wait.innerHTML = SVGIcons_1.SVGIcons.icons.loading;
        coveo_search_ui_1.$$(wait).addClass('coveo-facet-search-wait-animation');
        SVGDom_1.SVGDom.addClassToSVGInContainer(wait, 'coveo-facet-search-wait-animation-svg');
        return wait;
    };
    VehicleFitSearchElement.prototype.buildInputElement = function () {
        return coveo_search_ui_1.$$('input', {
            className: 'coveo-vehicle-fit-search-input',
            type: 'text',
            placeholder: this.options.placeholder,
            autocapitalize: 'off',
            autocorrect: 'off',
            ariaLabel: coveo_search_ui_1.l('SearchFacetResults', this.facetSearch.facetTitle),
            ariaHaspopup: 'true',
            ariaAutocomplete: 'list',
        }).el;
    };
    VehicleFitSearchElement.prototype.handleScrollEvent = function () {
        if (this.triggeredScroll) {
            this.triggeredScroll = false;
        }
        else {
            this.facetSearchUserInputHandler.handleFacetSearchResultsScroll();
        }
    };
    VehicleFitSearchElement.prototype.addAriaAttributes = function () {
        if (!this.input || !this.combobox) {
            return;
        }
        this.combobox.setAttribute('role', 'combobox');
        this.combobox.setAttribute('aria-owns', this.facetSearchId);
        this.input.setAttribute('aria-controls', this.facetSearchId);
        this.input.setAttribute('aria-expanded', 'true');
        this.facetSearch.setExpandedFacetSearchAccessibilityAttributes(this.searchResults);
    };
    VehicleFitSearchElement.prototype.removeAriaAttributes = function () {
        if (!this.input || !this.combobox) {
            return;
        }
        this.combobox.removeAttribute('role');
        this.combobox.removeAttribute('aria-owns');
        this.input.removeAttribute('aria-controls');
        this.input.removeAttribute('aria-activedescendant');
        this.input.setAttribute('aria-expanded', 'false');
        this.facetSearch.setCollapsedFacetSearchAccessibilityAttributes();
    };
    return VehicleFitSearchElement;
}());
exports.VehicleFitSearchElement = VehicleFitSearchElement;


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.DropdownNavigator = void 0;
var underscore_1 = __webpack_require__(2);
var coveo_search_ui_1 = __webpack_require__(0);
var DropdownNavigator = /** @class */ (function () {
    function DropdownNavigator(config) {
        this.config = config;
    }
    DropdownNavigator.prototype.setAsCurrentResult = function (toSet) {
        this.currentResult && this.currentResult.removeClass('coveo-facet-search-current-result');
        this.currentResult = toSet;
        toSet.addClass('coveo-facet-search-current-result');
        this.updateSelectedOption(toSet);
    };
    DropdownNavigator.prototype.focusNextElement = function () {
        this.moveCurrentResultDown();
    };
    DropdownNavigator.prototype.focusPreviousElement = function () {
        this.moveCurrentResultUp();
    };
    DropdownNavigator.prototype.moveCurrentResultDown = function () {
        var nextResult = this.currentResult.el.nextElementSibling;
        if (!nextResult) {
            nextResult = underscore_1.first(this.searchResults.children);
        }
        this.setAsCurrentResult(coveo_search_ui_1.$$(nextResult));
        this.highlightAndShowCurrentResultWithKeyboard();
    };
    DropdownNavigator.prototype.moveCurrentResultUp = function () {
        var previousResult = this.currentResult.el.previousElementSibling;
        if (!previousResult) {
            previousResult = underscore_1.last(this.searchResults.children);
        }
        this.setAsCurrentResult(coveo_search_ui_1.$$(previousResult));
        this.highlightAndShowCurrentResultWithKeyboard();
    };
    DropdownNavigator.prototype.highlightAndShowCurrentResultWithKeyboard = function () {
        this.currentResult.addClass('coveo-facet-search-current-result');
        this.config.setScrollTrigger(true);
        this.searchResults.scrollTop = this.currentResult.el.offsetTop;
    };
    Object.defineProperty(DropdownNavigator.prototype, "searchResults", {
        get: function () {
            return this.config.searchResults;
        },
        enumerable: false,
        configurable: true
    });
    DropdownNavigator.prototype.updateSelectedOption = function (option) {
        this.config.input.setAttribute('aria-activedescendant', option.getAttribute('id'));
        var previouslySelectedOption = coveo_search_ui_1.$$(this.searchResults).find('[aria-selected^="true"]');
        previouslySelectedOption && previouslySelectedOption.setAttribute('aria-selected', 'false');
        option.setAttribute('aria-selected', 'true');
    };
    return DropdownNavigator;
}());
exports.DropdownNavigator = DropdownNavigator;


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleFitSearchUserInputHandler = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var VehicleFitSearchUserInputHandler = /** @class */ (function () {
    function VehicleFitSearchUserInputHandler(facetSearch) {
        this.facetSearch = facetSearch;
    }
    VehicleFitSearchUserInputHandler.prototype.handleKeyboardEvent = function (event) {
        switch (event.which) {
            case coveo_search_ui_1.KEYBOARD.ENTER:
                this.facetSearch.keyboardNavigationEnterPressed(event);
                break;
            case coveo_search_ui_1.KEYBOARD.DELETE:
                this.facetSearch.keyboardNavigationDeletePressed && this.facetSearch.keyboardNavigationDeletePressed(event);
                break;
            case coveo_search_ui_1.KEYBOARD.ESCAPE:
                this.facetSearch.dismissSearchResults();
                break;
            case coveo_search_ui_1.KEYBOARD.DOWN_ARROW:
                this.facetSearch.facetSearchElement.moveCurrentResultDown();
                break;
            case coveo_search_ui_1.KEYBOARD.UP_ARROW:
                this.facetSearch.facetSearchElement.moveCurrentResultUp();
                break;
            default:
                this.facetSearch.keyboardEventDefaultHandler();
        }
    };
    VehicleFitSearchUserInputHandler.prototype.handleFacetSearchResultsScroll = function () {
        if (this.facetSearch.facetSearchPromise ||
            this.facetSearch.facetSearchElement.getValueInInputForFacetSearch() !== '' ||
            !this.facetSearch.moreValuesToFetch) {
            return;
        }
        var elementHeight = this.facetSearch.facetSearchElement.searchResults.clientHeight;
        var scrollHeight = this.facetSearch.facetSearchElement.searchResults.scrollHeight;
        var bottomPosition = this.facetSearch.facetSearchElement.searchResults.scrollTop + elementHeight;
        if (scrollHeight - bottomPosition < elementHeight / 2) {
            this.facetSearch.fetchMoreValues();
        }
    };
    return VehicleFitSearchUserInputHandler;
}());
exports.VehicleFitSearchUserInputHandler = VehicleFitSearchUserInputHandler;


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleFitFilterQueryController = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var VehicleFitFilterQueryController = /** @class */ (function () {
    function VehicleFitFilterQueryController(categoryFacet) {
        this.categoryFacet = categoryFacet;
    }
    VehicleFitFilterQueryController.prototype.putCategoryFacetInQueryBuilder = function (queryBuilder, path, maximumNumberOfValues) {
        var positionInQuery = queryBuilder.categoryFacets.length;
        this.addQueryFilter(queryBuilder, path);
        this.addCategoryFacetRequest(queryBuilder, path, maximumNumberOfValues);
        return positionInQuery;
    };
    // TODO: use options instead of multiple params
    VehicleFitFilterQueryController.prototype.searchFacetValues = function (value, path, numberOfValues, level, sort, delimiter) {
        return __awaiter(this, void 0, void 0, function () {
            var lastQuery, groupByRequest, results;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        lastQuery = __assign({}, this.categoryFacet.queryController.getLastQuery());
                        if (lastQuery.aq) {
                            lastQuery.aq = this.updateLastQuery(lastQuery.aq, path, delimiter);
                        }
                        groupByRequest = {
                            allowedValues: [this.getAllowedValuesPattern(value, level)],
                            allowedValuesPatternType: 'regex',
                            maximumNumberOfValues: numberOfValues,
                            constantQueryOverride: '@uri',
                            field: this.categoryFacet.options.field,
                            sortCriteria: sort ? sort : 'alphadescending',
                            injectionDepth: this.categoryFacet.options.injectionDepth,
                        };
                        lastQuery.groupBy = [groupByRequest];
                        lastQuery.categoryFacets.splice(this.categoryFacet.positionInQuery, 1);
                        return [4 /*yield*/, this.categoryFacet.queryController.getEndpoint().search(lastQuery)];
                    case 1:
                        results = _a.sent();
                        return [2 /*return*/, results.groupByResults[0] ? results.groupByResults[0].values : []];
                }
            });
        });
    };
    VehicleFitFilterQueryController.prototype.addDebugGroupBy = function (queryBuilder, value) {
        queryBuilder.groupByRequests.push({
            field: this.categoryFacet.options.field,
            allowedValues: [".*" + coveo_search_ui_1.Utils.escapeRegexCharacter(value) + ".*"],
            allowedValuesPatternType: 'regex',
        });
    };
    /**
     * This function is required to modify the advanced query expression since doing a facet search is not triggering a buildingquery event.
     * Therefore, we need to manually update the aq with the latest path on a facet search.
     *
     * @private
     * @param {string} filter
     * @param {string[]} path
     * @param {string} delimiter
     * @returns
     */
    VehicleFitFilterQueryController.prototype.updateLastQuery = function (filter, path, delimiter) {
        return filter.replace(/(\@.*==")(.*)(")/, "$1" + path.join(delimiter) + "$3");
    };
    VehicleFitFilterQueryController.prototype.shouldAddFilterToQuery = function (path) {
        return path.length !== 0 && !coveo_search_ui_1.Utils.arrayEqual(path, this.categoryFacet.options.basePath);
    };
    VehicleFitFilterQueryController.prototype.addQueryFilter = function (queryBuilder, path) {
        if (this.shouldAddFilterToQuery(path)) {
            var expression = [
                this.categoryFacet.options.field,
                '==',
                "\"" + [path.join(this.categoryFacet.options.delimitingCharacter)] + "\"",
            ];
            if (this.categoryFacet.options.additionalFilter) {
                queryBuilder.advancedExpression.add("(" + this.categoryFacet.options.additionalFilter + " OR " + expression.join(' ') + ")");
            }
            else {
                queryBuilder.advancedExpression.add("" + expression.join(' '));
            }
        }
    };
    VehicleFitFilterQueryController.prototype.addCategoryFacetRequest = function (queryBuilder, path, maximumNumberOfValues) {
        var categoryFacetsRequest = {
            field: this.categoryFacet.options.field,
            path: path,
            injectionDepth: this.categoryFacet.options.injectionDepth,
            maximumNumberOfValues: maximumNumberOfValues,
            delimitingCharacter: this.categoryFacet.options.delimitingCharacter,
        };
        queryBuilder.categoryFacets.push(categoryFacetsRequest);
    };
    VehicleFitFilterQueryController.prototype.getAllowedValuesPattern = function (value, level) {
        var basePath = this.categoryFacet.options.basePath;
        var delimiter = this.categoryFacet.options.delimitingCharacter;
        if (coveo_search_ui_1.Utils.isNonEmptyArray(basePath)) {
            return "" + basePath.join(delimiter) + delimiter + "*" + value + "*";
        }
        var path = this.categoryFacet.activePath.slice(0, level);
        if (path.length > 0) {
            return path.join('\\' + this.categoryFacet.options.delimitingCharacter) + "\\|" + value + "[^|]*$";
        }
        else {
            return value + "[^|]*$";
        }
    };
    return VehicleFitFilterQueryController;
}());
exports.VehicleFitFilterQueryController = VehicleFitFilterQueryController;


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleFitFilterHeader = void 0;
var VehicleFitFilterHeaderButton_1 = __webpack_require__(35);
var coveo_search_ui_1 = __webpack_require__(0);
var SVGDom_1 = __webpack_require__(6);
var VehicleFitFilterHeaderCollapseToggle_1 = __webpack_require__(50);
var SVGIcons_1 = __webpack_require__(3);
var VehicleFitFilterHeader = /** @class */ (function () {
    function VehicleFitFilterHeader(options) {
        this.options = options;
        this.element = coveo_search_ui_1.$$('div', { className: 'coveo-dynamic-facet-header' }).el;
        this.title = this.createTitle();
        coveo_search_ui_1.$$(this.element).append(this.title.el);
        this.options.enableCollapse && this.enableCollapse();
        coveo_search_ui_1.$$(this.element).append(this.createWaitAnimation());
        coveo_search_ui_1.$$(this.element).append(this.createBreadCrumb());
        coveo_search_ui_1.$$(this.element).append(this.createClearButton());
    }
    VehicleFitFilterHeader.prototype.updateSelectedValue = function (value) {
        this.breadcrumb.text(value);
    };
    VehicleFitFilterHeader.prototype.hideSelectedValue = function () { };
    VehicleFitFilterHeader.prototype.showSelectedValue = function () { };
    VehicleFitFilterHeader.prototype.updateTitleText = function (text) {
        this.title.find('span').textContent = text;
    };
    VehicleFitFilterHeader.prototype.createBreadCrumb = function () {
        this.breadcrumb = coveo_search_ui_1.$$('div', { className: 'vehicle-fit-breadcrumb' });
        return this.breadcrumb.el;
    };
    VehicleFitFilterHeader.prototype.createClearButton = function () {
        var _this = this;
        this.clearButton = new VehicleFitFilterHeaderButton_1.VehicleFitFilterHeaderButton({
            iconSVG: SVGIcons_1.SVGIcons.icons.mainClear,
            iconClassName: 'coveo-vehicle-fit-breadcrumb-clear',
            label: coveo_search_ui_1.l('Clear'),
            ariaLabel: coveo_search_ui_1.l('Clear', this.options.title),
            className: 'coveo-dynamic-facet-header-clear',
            shouldDisplay: false,
            action: function () { return _this.options.clear(); },
        });
        return this.clearButton.element;
    };
    VehicleFitFilterHeader.prototype.createCollapseToggle = function () {
        this.collapseToggle = new VehicleFitFilterHeaderCollapseToggle_1.VehicleFitFilterHeaderCollapseToggle(this.options);
        return this.collapseToggle.element;
    };
    VehicleFitFilterHeader.prototype.enableCollapse = function () {
        var _this = this;
        coveo_search_ui_1.$$(this.element).append(this.createCollapseToggle());
        coveo_search_ui_1.$$(this.title).addClass('coveo-clickable');
        coveo_search_ui_1.$$(this.title).on('click', function () { return _this.options.toggleCollapse(); });
    };
    VehicleFitFilterHeader.prototype.toggleCollapse = function (isCollapsed) {
        this.options.enableCollapse && this.collapseToggle.toggleButtons(isCollapsed);
    };
    VehicleFitFilterHeader.prototype.createTitle = function () {
        return coveo_search_ui_1.$$('h2', {
            className: 'coveo-dynamic-facet-header-title',
            ariaLabel: "" + coveo_search_ui_1.l('FacetTitle', this.options.title),
        }, coveo_search_ui_1.$$('span', { ariaHidden: true, title: this.options.title }, this.options.title));
    };
    VehicleFitFilterHeader.prototype.createWaitAnimation = function () {
        this.waitAnimation = coveo_search_ui_1.$$('div', { className: 'coveo-dynamic-facet-header-wait-animation' }, SVGIcons_1.SVGIcons.icons.loading);
        SVGDom_1.SVGDom.addClassToSVGInContainer(this.waitAnimation.el, 'coveo-dynamic-facet-header-wait-animation-svg');
        this.waitAnimation.toggle(false);
        return this.waitAnimation.el;
    };
    VehicleFitFilterHeader.prototype.toggleClear = function (visible) {
        this.clearButton.toggle(visible);
    };
    VehicleFitFilterHeader.prototype.showLoading = function () {
        var _this = this;
        clearTimeout(this.showLoadingTimeout);
        this.showLoadingTimeout = window.setTimeout(function () { return _this.waitAnimation.toggle(true); }, VehicleFitFilterHeader.showLoadingDelay);
    };
    VehicleFitFilterHeader.prototype.hideLoading = function () {
        clearTimeout(this.showLoadingTimeout);
        this.waitAnimation.toggle(false);
    };
    VehicleFitFilterHeader.showLoadingDelay = 2000;
    return VehicleFitFilterHeader;
}());
exports.VehicleFitFilterHeader = VehicleFitFilterHeader;


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleFitFilterHeaderCollapseToggle = void 0;
var VehicleFitFilterHeaderButton_1 = __webpack_require__(35);
var coveo_search_ui_1 = __webpack_require__(0);
var SVGIcons_1 = __webpack_require__(3);
var VehicleFitFilterHeaderCollapseToggle = /** @class */ (function () {
    function VehicleFitFilterHeaderCollapseToggle(options) {
        this.options = options;
        this.create();
    }
    VehicleFitFilterHeaderCollapseToggle.prototype.create = function () {
        var _this = this;
        var parent = coveo_search_ui_1.$$('div');
        this.collapseButton = new VehicleFitFilterHeaderButton_1.VehicleFitFilterHeaderButton({
            label: coveo_search_ui_1.l('CollapseFacet', this.options.title),
            iconSVG: SVGIcons_1.SVGIcons.icons.arrowUp,
            iconClassName: 'coveo-dynamic-facet-collapse-toggle-svg',
            className: 'coveo-dynamic-facet-header-collapse',
            shouldDisplay: true,
            action: function () { return _this.options.collapse(); },
        });
        this.expandButton = new VehicleFitFilterHeaderButton_1.VehicleFitFilterHeaderButton({
            label: coveo_search_ui_1.l('ExpandFacet', this.options.title),
            iconSVG: SVGIcons_1.SVGIcons.icons.arrowDown,
            iconClassName: 'coveo-dynamic-facet-collapse-toggle-svg',
            className: 'coveo-dynamic-facet-header-expand',
            shouldDisplay: false,
            action: function () { return _this.options.expand(); },
        });
        parent.append(this.collapseButton.element);
        parent.append(this.expandButton.element);
        this.element = parent.el;
    };
    VehicleFitFilterHeaderCollapseToggle.prototype.toggleButtons = function (isCollapsed) {
        this.collapseButton.toggle(!isCollapsed);
        this.expandButton.toggle(isCollapsed);
    };
    return VehicleFitFilterHeaderCollapseToggle;
}());
exports.VehicleFitFilterHeaderCollapseToggle = VehicleFitFilterHeaderCollapseToggle;


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleFitVinSearch = exports.VinResponseCode = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var SimpleCombobox_1 = __webpack_require__(52);
var VinResponseCode = /** @class */ (function () {
    function VinResponseCode() {
    }
    VinResponseCode.clean = 0;
    VinResponseCode.typo = 1;
    VinResponseCode.correctedFull = 2;
    VinResponseCode.corrected = 3;
    VinResponseCode.notSureWhatThisIs = 4;
    VinResponseCode.notFound = 5;
    VinResponseCode.notSureWhatThisIs2 = 6;
    return VinResponseCode;
}());
exports.VinResponseCode = VinResponseCode;
/**
 * This component renders the image of a promotion item. This component assumes that the image url is stored in the item.
 *
 * This component is a result template component (see [Result Templates](https://docs.coveo.com/en/413/)).
 *
 * @export
 * @class VehicleFitVinSearch
 * @extends {Component}
 * @implements {IComponentBindings}
 */
var VehicleFitVinSearch = /** @class */ (function () {
    /**
     * Creates a new `VehicleFitVinSearch` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `VehicleFitVinSearch` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     * @param result The result to associate the component with.
     */
    function VehicleFitVinSearch(vehicleFitFilter) {
        var _this = this;
        this.vehicleFitFilter = vehicleFitFilter;
        this.lastVin = null;
        this.lastStateChangeTriggeredByVinChange = false;
        this.initQueryStateEvents();
        this.vehicleFitFilter.bind.onRootElement(coveo_search_ui_1.BreadcrumbEvents.clearBreadcrumb, function () { return _this.reset(); });
    }
    VehicleFitVinSearch.prototype.build = function () {
        var element = coveo_search_ui_1.$$('div', { className: 'flex-flow relative' }); // TODO: Change once the My Fleet feature is enabled
        element.append(coveo_search_ui_1.$$('div', { className: 'coveo-vehicle-fit-vin-search' }, this.buildInput()).el);
        element.append(coveo_search_ui_1.$$('div', { className: 'coveo-vehicle-fit-response-code' }, this.buildResponseCode()).el);
        return element.el;
    };
    VehicleFitVinSearch.prototype.clearInput = function () {
        this.combobox.setText('');
    };
    VehicleFitVinSearch.prototype.setInput = function (text) {
        this.combobox.setText(text);
    };
    VehicleFitVinSearch.prototype.reset = function (updateState) {
        if (updateState === void 0) { updateState = true; }
        this.lastVin = null;
        this.clearInput();
        this.combobox.clearValidationIcon();
        this.updateMessage('');
        this.vehicleFitFilter.clearVinCookie();
        if (updateState) {
            this.updateQueryStateModel('');
        }
    };
    VehicleFitVinSearch.prototype.updateQueryStateModel = function (vin) {
        // this.listenToQueryStateChange = false; // TODO: check if required
        this.vehicleFitFilter.queryStateModel.set(this.queryStateAttribute, vin);
        // this.listenToQueryStateChange = true;
    };
    VehicleFitVinSearch.prototype.handleQueryStateChanged = function (data) {
        var vin = data.value;
        this.combobox.clearValidationIcon();
        if (vin) {
            if (vin !== this.lastVin) {
                this.setInput(vin);
                this.combobox.triggerRequest(vin);
                // } else {
                //   this.clearInput();
                //   this.lastVin = null;
            }
        }
        else {
            this.clearInput();
        }
        this.lastVin = vin;
    };
    VehicleFitVinSearch.prototype.initQueryStateEvents = function () {
        var _this = this;
        this.vehicleFitFilter.queryStateModel.registerNewAttribute(this.queryStateAttribute, '');
        this.vehicleFitFilter.bind.onQueryState('change:', this.queryStateAttribute, function (data) {
            return _this.handleQueryStateChanged(data);
        });
    };
    Object.defineProperty(VehicleFitVinSearch.prototype, "queryStateAttribute", {
        get: function () {
            return 'vin';
        },
        enumerable: false,
        configurable: true
    });
    VehicleFitVinSearch.prototype.buildResponseCode = function () {
        var element = coveo_search_ui_1.$$('div');
        return element.el;
    };
    VehicleFitVinSearch.prototype.buildInput = function () {
        var _this = this;
        // TODO: check if combobox is the best alternative here
        // Add constraints: * 17 digits max
        this.combobox = new SimpleCombobox_1.SimpleCombobox({
            label: this.vehicleFitFilter.options.vinSearchPlaceholder,
            searchInterface: this.vehicleFitFilter.searchInterface,
            triggerRequestOnSubmit: false,
            requestValue: function (terms) { return _this.searchForVin(terms); },
            responseCallback: function (response) { return _this.handleResponse(response); },
            errorCallback: function (error) { return _this.handleError(error); },
            submit: function (terms) { return _this.handleSubmit(terms); },
            placeholderText: this.vehicleFitFilter.options.vinSearchPlaceholder,
            inputOptions: { maximumNumberOfCharacters: this.vehicleFitFilter.options.minimumVinCharacthers },
        });
        return this.combobox.element;
    };
    VehicleFitVinSearch.prototype.allowNextVinSearch = function (terms) {
        var lengthCondition = terms.length > 0;
        // const queryCondition = terms !== this.lastVin;
        // this.lastVin = terms;
        return lengthCondition;
    };
    VehicleFitVinSearch.prototype.updateMessage = function (message) {
        var messageElement = this.vehicleFitFilter.element.querySelector('.coveo-vehicle-fit-response-code');
        if (messageElement) {
            messageElement.innerHTML = message;
        }
    };
    VehicleFitVinSearch.prototype.handleNoResultForThatVin = function (invalidVin) {
        this.combobox.toggleSuccessIcon(false);
        this.vehicleFitFilter.clearVinCookie();
        this.updateMessage("<span class=\"vin-error\">No results for that VIN</span>");
    };
    VehicleFitVinSearch.prototype.handleVinNotFound = function (invalidVin) {
        this.combobox.toggleSuccessIcon(false);
        this.vehicleFitFilter.clearVinCookie();
        this.updateMessage("<span class=\"vin-error\">Enter a valid VIN or search another way</span>");
    };
    VehicleFitVinSearch.prototype.updateVinState = function (vin, parsed) {
        if (this.vehicleFitFilter.options.saveVinInCookie && parsed.vinResponse) {
            var vinResponse = parsed.vinResponse;
            var formattedCookie = {
                Year: vinResponse.year,
                Make: vinResponse.make,
                MakeDesc: vinResponse.make,
                Model: vinResponse.model,
                ModelDesc: vinResponse.model,
                Engine: vinResponse.engine,
            };
            this.vehicleFitFilter.setVinCookie(formattedCookie);
        }
        this.updateQueryStateModel(vin);
        this.lastStateChangeTriggeredByVinChange = true;
    };
    VehicleFitVinSearch.prototype.handleCorrectedVin = function (validVin) {
        this.updateVinState(validVin.vinResponse.correctedVin, validVin);
        this.combobox.toggleSuccessIcon(true);
        this.setInput(validVin.vinResponse.correctedVin);
        // this.updateMessage(`<span class="vin-success">VIN was automatically corrected to <b>${validVin.vinResponse.correctedVin}</b></span>`);
        this.updateMessage('');
    };
    VehicleFitVinSearch.prototype.handleValidVin = function (validVin) {
        this.updateVinState(validVin.vinResponse.vin, validVin);
        this.combobox.toggleSuccessIcon(true);
        // Add a check icon to let the user know it is a valid vin
        // this.vehicleFitFilter.searchTriggeredByVinChange = true;
        this.vehicleFitFilter.changeActivePath([
            validVin.vinResponse.year,
            validVin.vinResponse.make,
            validVin.vinResponse.model,
            validVin.vinResponse.engine,
        ]);
        // this.vehicleFitFilter.executeQuery(() =>
        //   this.vehicleFitFilter.usageAnalytics.logCustomEvent(
        //     { name: 'vinFound', type: 'customEventType' },
        //     { vin: validVin.vinResponse.vin },
        //     this.vehicleFitFilter.root
        //   )
        // );
    };
    VehicleFitVinSearch.prototype.handleError = function (err) {
        this.combobox.toggleWaitAnimation(false);
        this.combobox.toggleSuccessIcon(false);
        this.updateMessage("<span class=\"vin-error\">Unable to decode VIN. Try again later.</span>");
        this.vehicleFitFilter.logger.error('Something went wrong while decoding VIN', err);
    };
    VehicleFitVinSearch.prototype.handleResponse = function (response) {
        this.combobox.toggleWaitAnimation(false);
        this.updateMessage('');
        try {
            var parsed = JSON.parse(response);
            if (parsed.success) {
                switch (parseInt(parsed.vinResponse.returnCode, 10)) {
                    case VinResponseCode.clean:
                        this.handleValidVin(parsed);
                        break;
                    case VinResponseCode.typo:
                        // TODO: Let the user know there is a typo in the vin search
                        this.handleValidVin(parsed);
                        break;
                    case VinResponseCode.correctedFull:
                        // TODO: The vin was automatically corrected to correctedVin
                        this.handleCorrectedVin(parsed);
                        break;
                    case VinResponseCode.corrected:
                        // TODO: The vin was automatically corrected to correctedVin
                        this.handleCorrectedVin(parsed);
                        break;
                    case VinResponseCode.notFound:
                        this.handleVinNotFound(parsed);
                        break;
                    case VinResponseCode.notSureWhatThisIs:
                        this.handleNoResultForThatVin(parsed);
                        break;
                    case VinResponseCode.notSureWhatThisIs2:
                        this.handleNoResultForThatVin(parsed);
                        break;
                    default:
                        this.vehicleFitFilter.clearVinCookie();
                        this.updateMessage('');
                        break;
                }
            }
        }
        catch (error) {
            this.vehicleFitFilter.logger.error('Unable to parse response from server', response, error);
        }
    };
    VehicleFitVinSearch.prototype.getVinDetails = function (vin) {
        if (this.vehicleFitFilter.options.vinSearchRemoteAction === undefined) {
            this.vehicleFitFilter.logger.error('vinSearchRemoteAction option is not defined');
            return Promise.reject();
        }
        this.vehicleFitFilter.usageAnalytics.logCustomEvent({ name: 'vinSearch', type: 'customEventType' }, { vin: vin }, this.vehicleFitFilter.root);
        return this.vehicleFitFilter.options.vinSearchRemoteAction(vin);
    };
    /**
     * Remove non alphanumeric characters because the API returns 500 errors.
     *
     * @private
     * @param {string} vin
     * @returns {string}
     */
    VehicleFitVinSearch.prototype.cleanVIN = function (str) {
        return str.replace(/[^a-z0-9+]+/gi, '');
    };
    VehicleFitVinSearch.prototype.handleSubmit = function (terms) {
        this.updateQueryStateModel(terms);
    };
    VehicleFitVinSearch.prototype.searchForVin = function (terms) {
        if (this.allowNextVinSearch(terms)) {
            console.log("Searching for VIN " + terms);
            this.combobox.toggleWaitAnimation(true);
            return this.getVinDetails(this.cleanVIN(terms));
        }
        else {
            Promise.resolve();
        }
    };
    VehicleFitVinSearch.ID = 'VehicleFitVinSearch';
    VehicleFitVinSearch.options = {};
    return VehicleFitVinSearch;
}());
exports.VehicleFitVinSearch = VehicleFitVinSearch;


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.SimpleCombobox = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var SVGDom_1 = __webpack_require__(6);
var SVGIcons_1 = __webpack_require__(3);
var SimpleComboboxInput_1 = __webpack_require__(53);
var SimpleCombobox = /** @class */ (function () {
    function SimpleCombobox(options) {
        this.options = options;
        // private isThrottledRequestCancelled = false;
        this.throttlingDelay = 600;
        this.throttledTriggerNewRequest = underscore_1.throttle(this.triggerRequest, this.throttlingDelay, {
            leading: true,
            trailing: true,
        });
        this.id = underscore_1.uniqueId('coveo-combobox-');
        this.create();
    }
    SimpleCombobox.prototype.create = function () {
        this.element = coveo_search_ui_1.$$('div', { className: underscore_1.compact(['coveo-combobox-wrapper', this.options.wrapperClassName]).join(' ') }).el;
        this.createAndAppendInput();
        this.createAndAppendWaitAnimation();
        // this.createAndAppendValues();
    };
    SimpleCombobox.prototype.createAndAppendInput = function () {
        this.input = new SimpleComboboxInput_1.SimpleComboboxInput(this);
        this.element.appendChild(this.input.element);
    };
    SimpleCombobox.prototype.createAndAppendWaitAnimation = function () {
        this.waitAnimationElement = coveo_search_ui_1.$$('div', { className: 'coveo-combobox-wait-animation' }, SVGIcons_1.SVGIcons.icons.loading).el;
        this.successIconElement = coveo_search_ui_1.$$('div', { className: 'coveo-combobox-success-icon' }, SVGIcons_1.SVGIcons.icons.check).el;
        this.errorIconElement = coveo_search_ui_1.$$('div', { className: 'coveo-combobox-error-icon' }, SVGIcons_1.SVGIcons.icons.exclamationCircle).el;
        SVGDom_1.SVGDom.addClassToSVGInContainer(this.waitAnimationElement, 'coveo-combobox-wait-animation-svg');
        this.toggleWaitAnimation(false);
        this.clearValidationIcon();
        this.input.element.appendChild(this.successIconElement);
        this.input.element.appendChild(this.waitAnimationElement);
        this.input.element.appendChild(this.errorIconElement);
    };
    SimpleCombobox.prototype.clearValidationIcon = function () {
        this.input.element.classList.remove('error');
        this.successIconElement.classList.add('hidden');
        this.errorIconElement.classList.add('hidden');
    };
    SimpleCombobox.prototype.showErrorIcon = function () {
        this.input.element.classList.add('error');
        this.errorIconElement.classList.remove('hidden');
    };
    SimpleCombobox.prototype.showSuccessIcon = function () {
        this.input.element.classList.remove('error');
        this.successIconElement.classList.remove('hidden');
    };
    SimpleCombobox.prototype.toggleSuccessIcon = function (success) {
        // clearing previous icons just in case
        this.toggleWaitAnimation(false);
        this.clearValidationIcon();
        if (success) {
            this.showSuccessIcon();
        }
        else {
            this.showErrorIcon();
        }
    };
    SimpleCombobox.prototype.toggleWaitAnimation = function (show) {
        // TODO: check it is not showing
        coveo_search_ui_1.$$(this.waitAnimationElement).toggle(show);
        if (show) {
            this.clearValidationIcon();
        }
    };
    // private createAndAppendValues() {
    //   this.values = new ComboboxValues(this);
    //   this.element.appendChild(this.values.element);
    // }
    SimpleCombobox.prototype.getText = function () {
        return this.input.getTextInputValue();
    };
    SimpleCombobox.prototype.setText = function (text) {
        this.input.setTextInputValue(text);
    };
    SimpleCombobox.prototype.clear = function () {
        // this.values.clearValues();
        this.cancelRequest();
    };
    SimpleCombobox.prototype.cancelRequest = function () {
        this.throttledTriggerNewRequest.cancel();
    };
    SimpleCombobox.prototype.onInputChange = function (value) {
        if (this.options.inputOptions.triggerOnChangeAsYouType) {
            this.throttledTriggerNewRequest(value);
        }
    };
    SimpleCombobox.prototype.onInputBlur = function () {
        // TODO: check if really necessary
        this.clear();
    };
    SimpleCombobox.prototype.updateAccessibilityAttributes = function (attributes) {
        this.input.updateAccessibilityAttributes(attributes);
    };
    SimpleCombobox.prototype.updateAriaLive = function (text) {
        this.options.searchInterface.ariaLive.updateText(text);
    };
    // TODO: find a better name
    SimpleCombobox.prototype.submit = function (terms) {
        this.options.submit(terms);
        if (this.options.triggerRequestOnSubmit) {
            this.triggerRequest(terms);
        }
    };
    SimpleCombobox.prototype.triggerRequest = function (terms) {
        var _this = this;
        // this.isThrottledRequestCancelled = false;
        // const response = await this.options.requestValue(terms)
        this.options
            .requestValue(terms)
            .then(function (response) {
            _this.options.responseCallback(response);
        })
            .catch(function (err) {
            console.error('Unable to decode VIN', err);
            _this.options.errorCallback(err);
        });
        // if (!this.isThrottledRequestCancelled) {
        //   this.values.renderFromResponse(response);
        // }
    };
    return SimpleCombobox;
}());
exports.SimpleCombobox = SimpleCombobox;


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SimpleComboboxInput = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var KeyboardUtils_1 = __webpack_require__(10);
var SVGIcons_1 = __webpack_require__(3);
var TextInput_1 = __webpack_require__(54);
var SimpleComboboxInput = /** @class */ (function () {
    function SimpleComboboxInput(combobox) {
        this.combobox = combobox;
        this.inputOptions = {
            usePlaceholder: true,
            className: 'fleetpride-combobox',
            triggerOnChangeAsYouType: false,
            isRequired: false,
        };
        this.create();
        this.element = this.textInput.getElement();
        this.element.appendChild(this.buildButton());
        this.inputElement = coveo_search_ui_1.$$(this.element).find('input');
        this.addEventListeners();
        this.addAccessibilityAttributes();
    }
    SimpleComboboxInput.prototype.getTextInputValue = function () {
        return this.textInput.getValue();
    };
    SimpleComboboxInput.prototype.setTextInputValue = function (text) {
        this.textInput.setValue(text);
    };
    SimpleComboboxInput.prototype.buildButton = function () {
        var _this = this;
        var button = coveo_search_ui_1.$$('div', { className: 'fleetpride-combobox-button' }, SVGIcons_1.SVGIcons.icons.arrowDown);
        button.on('click', function () {
            var text = _this.combobox.getText();
            if (text) {
                _this.combobox.submit(text);
            }
        });
        return button.el;
    };
    SimpleComboboxInput.prototype.create = function () {
        var _this = this;
        this.textInput = new TextInput_1.TextInput(function (inputInstance) { return _this.combobox.onInputChange(inputInstance.getValue()); }, this.combobox.options.placeholderText, __assign(__assign({}, this.inputOptions), this.combobox.options.inputOptions));
    };
    SimpleComboboxInput.prototype.addEventListeners = function () {
        var _this = this;
        coveo_search_ui_1.$$(this.combobox.element).on('focusout', function (e) { return _this.handleFocusOut(e); });
        coveo_search_ui_1.$$(this.combobox.element).on('keyup', function (e) { return _this.handleKeyboardEnterEscape(e); });
    };
    SimpleComboboxInput.prototype.addAccessibilityAttributes = function () {
        var listboxId = this.combobox.id + "-listbox";
        this.inputElement.setAttribute('role', 'combobox');
        this.inputElement.setAttribute('aria-owns', listboxId);
        this.inputElement.setAttribute('aria-haspopup', 'listbox');
        this.inputElement.setAttribute('aria-autocomplete', 'list');
        this.inputElement.setAttribute('id', this.combobox.id + "-input");
        this.inputElement.setAttribute('aria-label', this.combobox.options.label);
        this.updateAccessibilityAttributes({
            activeDescendant: '',
            expanded: false,
        });
    };
    SimpleComboboxInput.prototype.updateAccessibilityAttributes = function (attributes) {
        this.inputElement.setAttribute('aria-expanded', attributes.expanded ? 'true' : 'false');
        coveo_search_ui_1.Utils.isEmptyString(attributes.activeDescendant)
            ? this.inputElement.removeAttribute('aria-activedescendant')
            : this.inputElement.setAttribute('aria-activedescendant', attributes.activeDescendant);
    };
    SimpleComboboxInput.prototype.clearInput = function () {
        this.textInput.reset();
    };
    SimpleComboboxInput.prototype.handleFocusOut = function (event) {
        var newTarget = event.relatedTarget;
        var isFocusedOnInput = this.combobox.element.contains(newTarget);
        if (isFocusedOnInput) {
            return;
        }
        this.combobox.onInputBlur();
    };
    SimpleComboboxInput.prototype.handleKeyboardEnterEscape = function (event) {
        // tslint:disable-next-line: switch-default
        switch (event.which) {
            case KeyboardUtils_1.KEYBOARD.ENTER:
                this.combobox.submit(this.combobox.getText());
                break;
            case KeyboardUtils_1.KEYBOARD.ESCAPE:
                if (coveo_search_ui_1.Utils.isNonEmptyString(this.textInput.getValue())) {
                    event.stopPropagation();
                }
                this.combobox.clear();
                break;
        }
    };
    return SimpleComboboxInput;
}());
exports.SimpleComboboxInput = SimpleComboboxInput;


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TextInput = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var KeyboardUtils_1 = __webpack_require__(10);
var defaultOptions = {
    usePlaceholder: false,
    className: 'coveo-input',
    triggerOnChangeAsYouType: false,
    isRequired: true,
};
/**
 * A text input widget with standard styling.
 */
var TextInput = /** @class */ (function () {
    // private lastQueryText: string = ''; // That is not necessary. As a matter of fact it does not work when you paste something in the input
    /**
     * Creates a new `TextInput`.
     * @param onChange The function to call when the value entered in the text input changes. This function takes the
     * current `TextInput` instance as an argument.
     * @param name The text to display in the text input label or placeholder.
     */
    function TextInput(onChange, name, options) {
        if (onChange === void 0) { onChange = function (textInput) { }; }
        this.onChange = onChange;
        this.name = name;
        this.options = options;
        this.options = __assign(__assign({}, defaultOptions), this.options);
        this.buildContent();
    }
    /**
     * Gets the element on which the text input is bound.
     * @returns {HTMLElement} The text input element.
     */
    TextInput.prototype.getElement = function () {
        return this.element;
    };
    /**
     * Gets the value currently entered in the text input.
     * @returns {string} The text input current value.
     */
    TextInput.prototype.getValue = function () {
        return coveo_search_ui_1.$$(this.element).find('input').value;
    };
    /**
     * Sets the value in the text input.
     * @param value The value to set the text input to.
     */
    TextInput.prototype.setValue = function (value) {
        var currentValue = this.getValue();
        coveo_search_ui_1.$$(this.element).find('input').value = value;
        if (currentValue !== value) {
            this.onChange(this);
        }
    };
    /**
     * Resets the text input.
     */
    TextInput.prototype.reset = function () {
        var currentValue = this.getValue();
        coveo_search_ui_1.$$(this.element).find('input').value = '';
        if (currentValue !== '') {
            this.onChange(this);
        }
    };
    /**
     * Gets the element on which the text input is bound.
     * @returns {HTMLElement} The text input element.
     */
    TextInput.prototype.build = function () {
        return this.element;
    };
    /**
     * Gets the `input` element (the text input itself).
     * @returns {HTMLElement} The `input` element.
     */
    TextInput.prototype.getInput = function () {
        return coveo_search_ui_1.$$(this.element).find('input');
    };
    TextInput.prototype.buildContent = function () {
        this.element = coveo_search_ui_1.$$('div', { className: this.options.className }).el;
        this.input = coveo_search_ui_1.$$('input', { type: 'text', autocomplete: 'off' });
        if (this.options.maximumNumberOfCharacters) {
            this.input.setAttribute('maxlength', "" + this.options.maximumNumberOfCharacters);
        }
        this.options.isRequired && this.input.setAttribute('required', 'true');
        this.options.ariaLabel && this.input.setAttribute('aria-label', this.options.ariaLabel);
        this.addEventListeners();
        this.element.appendChild(this.input.el);
        this.name && this.createLabelOrPlaceholder();
    };
    TextInput.prototype.addEventListeners = function () {
        this.options.triggerOnChangeAsYouType ? this.addOnTypeEventListener() : this.addOnChangeEventListener();
    };
    TextInput.prototype.addOnChangeEventListener = function () {
        var _this = this;
        this.input.on(['keydown', 'blur'], function (e) {
            if (e.type === 'blur' || e.keyCode === KeyboardUtils_1.KEYBOARD.ENTER) {
                _this.triggerChange();
            }
        });
    };
    TextInput.prototype.addOnTypeEventListener = function () {
        var _this = this;
        this.input.on(['keyup'], function () {
            _this.triggerChange();
        });
    };
    TextInput.prototype.createLabelOrPlaceholder = function () {
        if (this.options.usePlaceholder) {
            return this.input.setAttribute('placeholder', this.name);
        }
        var label = coveo_search_ui_1.$$('label');
        label.text(this.name);
        this.element.appendChild(label.el);
    };
    TextInput.prototype.triggerChange = function () {
        if (this.input.el.value) {
            this.onChange(this);
        }
    };
    return TextInput;
}());
exports.TextInput = TextInput;


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemoteActionManager = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var CustomEvents_1 = __webpack_require__(4);
var PriceManager_1 = __webpack_require__(37);
var AvailabilityManager_1 = __webpack_require__(38);
var turbo_core_1 = __webpack_require__(1);
var RemoteActionManager = /** @class */ (function (_super) {
    __extends(RemoteActionManager, _super);
    /**
     * Creates a new `RemoteActionManager` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `ResultList` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     */
    function RemoteActionManager(element, options, bindings) {
        var _this = _super.call(this, element, RemoteActionManager_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, RemoteActionManager_1, options);
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.newQuery, _this.handleNewQuery);
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.deferredQuerySuccess, _this.handleQuerySuccess);
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.fetchMoreSuccess, _this.handleFetchMoreSuccess);
        _this.bind.onRootElement(CustomEvents_1.CustomEvents.addToCartRemoteAction, function (data) {
            return _this.handleAddToCartRemoteAction(data);
        });
        _this.bind.onRootElement(coveo_search_ui_1.AnalyticsEvents.changeAnalyticsCustomData, function (args) {
            return _this.handleAnalyticsCustomEvent(args);
        });
        _this.priceManager = new PriceManager_1.PriceManager(_this);
        _this.availabilityManager = new AvailabilityManager_1.AvailabilityManager(_this);
        return _this;
    }
    RemoteActionManager_1 = RemoteActionManager;
    RemoteActionManager.prototype.getAllRemoteActions = function (results) {
        var _this = this;
        if (this.options.availabilityInfoRemoteAction === undefined && this.options.priceListRemoteAction === undefined) {
            this.logger.error('undefined remote action');
        }
        var customOptions = {
            skuField: this.options.skuField,
            partNumberField: this.options.partNumberField,
            corePartNumberField: this.options.corePartNumberField,
            salespack: this.options.salespack,
        };
        var customerPricePayload = this.priceManager.getCustomerPricePayload(results);
        var coreChargePayload = this.priceManager.getCoreChargePayload(results);
        Promise.all([
            this.options.availabilityInfoRemoteAction(this.availabilityManager.getSkus(results, this.options.skuField)),
            this.options.priceListRemoteAction(__spreadArrays(customerPricePayload, coreChargePayload)),
            // We wait until the newResultsDisplayed phase was passed to trigger the custom result event. Otherwise we end up with a race condition.
            // There is no garanty remote action calls will resolve after results are displayed.
            // If that is the case, this method will trigger custom events BEFORE the result template components start listening on them...
            this.resultReadyDeferred,
        ])
            .then(function (data) {
            var inventory = data[0];
            var prices = _this.priceManager.parsePrices(data[1]); // TODO: leverage ICCRemoteActionReturnsArgs
            _this.bind.trigger(_this.root, CustomEvents_1.CustomEvents.updateProductInfo, {
                inventory: inventory.data,
                prices: prices,
                options: customOptions,
            });
        })
            .catch(function (err) {
            _this.logger.error('Unable to get inventory info', err);
            // Make sure to trigger the event when results are rendered
            _this.resultReadyDeferred.then(function () {
                _this.bind.trigger(_this.root, CustomEvents_1.CustomEvents.invalidProductInfo, { err: err });
            });
        });
    };
    RemoteActionManager.prototype.handleFetchMoreSuccess = function (data) {
        try {
            this.getAllRemoteActions(data.results.results);
        }
        catch (error) {
            this.logger.error('Something went wrong while calling Salesforce Remote Action', error);
        }
    };
    RemoteActionManager.prototype.handleAddToCartRemoteAction = function (data) {
        this.options
            .addToCartRemoteAction(data.sku, data.qty, data.shippingMethod, data.prodList)
            .then(function (response) {
            if (response.success) {
                data.successCallback(response);
            }
            else {
                data.errorCallback(new Error('Something went wrong while adding to cart'));
            }
        })
            .catch(function (err) { return data.errorCallback(err); });
    };
    RemoteActionManager.prototype.handleAnalyticsCustomEvent = function (data) {
        if (data && data && data.actionCause && data.actionCause === coveo_search_ui_1.analyticsActionCauseList.resultsLayoutChange.name) {
            try {
                var results = this.queryController.getLastResults().results;
                this.getAllRemoteActions(results);
            }
            catch (error) {
                this.logger.error('Something went wrong while calling Salesforce Remote Action', error);
            }
        }
    };
    /**
     * TODO: document the purpose of this function
     *
     * @private
     */
    RemoteActionManager.prototype.handleNewQuery = function () {
        var _this = this;
        this.resultReadyDeferred = new Promise(function (resolve, reject) {
            _this.bind.oneRootElement(coveo_search_ui_1.ResultListEvents.newResultsDisplayed, function () {
                resolve();
            });
        });
    };
    RemoteActionManager.prototype.handleQuerySuccess = function (data) {
        if (data.results.results.length > 0) {
            try {
                this.getAllRemoteActions(data.results.results);
            }
            catch (error) {
                this.logger.error('Something went wrong while calling Salesforce Remote Action', error);
            }
        }
    };
    var RemoteActionManager_1;
    RemoteActionManager.ID = 'RemoteActionManager';
    RemoteActionManager.options = {
        /**
         * Text value for the ShippingMethod button
         *
         */
        skuField: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_sku' }),
        /**
         * Used the find the price in the pricing service response
         *
         */
        partNumberField: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_partnumber' }),
        /**
         *  ???
         *
         */
        corePartNumberField: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_core_partnumber' }),
        /**
         *  ???
         *
         */
        salespack: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_salespack' }),
        priceListRemoteAction: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        availabilityInfoRemoteAction: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        addToCartRemoteAction: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
    };
    RemoteActionManager = RemoteActionManager_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component acts as a bridge between Coveo JSUI component and Salesforce remote action. There is no need to decouple this component into multiple sub remote action component because Salesforce batches Apex calls.
         *
         * @export
         * @class RemoteActionManager
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], RemoteActionManager);
    return RemoteActionManager;
}(coveo_search_ui_1.Component));
exports.RemoteActionManager = RemoteActionManager;


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CCStateManager = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var CustomEvents_1 = __webpack_require__(4);
var CCStateManager = /** @class */ (function (_super) {
    __extends(CCStateManager, _super);
    function CCStateManager(element, options, bindings) {
        var _this = _super.call(this, element, CCStateManager_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.lastLocation = null;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, CCStateManager_1, options);
        // Listening on the custom CC events defined by FleetPride then notifying other JSUI components
        if (_this.isInCloudCraze()) {
            window.CCRZ.pubSub.on('view:productSearchView:refresh', function () { return _this.handleProductSearchRefresh(); });
            window.CCRZ.pubSub.on('view:FPbranchlocationView:refresh', function () { return _this.handleCCBranchLocationRefresh(); });
        }
        return _this;
    }
    CCStateManager_1 = CCStateManager;
    /**
     * Checks it the code is running in CloudCraze.
     *
     * @private
     * @returns true if running in CloudCraze. False, otherwise.
     */
    CCStateManager.prototype.isInCloudCraze = function () {
        return window.CCRZ;
    };
    /**
     * This method was implemented to overcome an issue after integrating Coveo in CloudCraze.
     * The Coveo searchbox needs to be re-initialized every time the search page header is re-rendered. Otherwise, the searchbox gets detached from the DOM.
     *
     * @private
     * @param {string} query
     */
    CCStateManager.prototype.updateSearchBoxValue = function (query) {
        var _this = this;
        var searchboxRoot = document.getElementById('searchbox');
        if (searchboxRoot) {
            this.bind.one(searchboxRoot, coveo_search_ui_1.InitializationEvents.afterComponentsInitialization, function () {
                if (query && _this.getSearchBoxText() === '') {
                    var omnibox = Coveo.get(document.querySelector('.CoveoOmnibox'));
                    omnibox.setText(query);
                }
            });
            // Initializing the searchbox multiple times in case there are multiple view refreshes
            Coveo.Initialization.initExternalComponents(this.root, {
                externalComponents: [searchboxRoot],
            });
            if (query && this.getSearchBoxText() === '') {
                var omnibox = Coveo.get(document.querySelector('.CoveoOmnibox'));
                omnibox.setText(query);
            }
        }
    };
    CCStateManager.prototype.getSearchBoxText = function () {
        var omnibox = Coveo.get(document.querySelector('.CoveoOmnibox'));
        return omnibox.getText();
    };
    /**
     * Executes a new query every time the user changes its location.
     *
     * @param {string} loc             location
     * @param {boolean} [force=false]  force location change
     */
    CCStateManager.prototype.updateLocation = function (loc, force) {
        var _this = this;
        if (force === void 0) { force = false; }
        // FIXME: we don't want to execute the query if the this.lastLocation is undefined because it means that the page just loaded.
        // However, user might have selected an invalid branch. so we need to find a logic that will handle this invalid branch selection
        if (force || (this.lastLocation && this.lastLocation !== loc)) {
            this.queryController.executeQuery({
                beforeExecuteQuery: function () { return _this.logSearchEvent(loc); },
                ignoreWarningSearchEvent: true,
                logInActionsHistory: false,
            });
        }
        this.lastLocation = loc;
    };
    CCStateManager.prototype.logSearchEvent = function (loc) {
        this.usageAnalytics.logSearchEvent({ name: 'locationChange', type: 'customEventType' }, { location: loc });
    };
    /**
     * This method is called when the branch location changes in CloudCraze.
     *
     * @private
     */
    CCStateManager.prototype.handleCCBranchLocationRefresh = function () {
        var _a, _b, _c, _d, _e, _f;
        var branchId = (_b = (_a = CCRZ.pagevars) === null || _a === void 0 ? void 0 : _a.currentBranchLocation) === null || _b === void 0 ? void 0 : _b.fid;
        var branchName = (_d = (_c = CCRZ.pagevars) === null || _c === void 0 ? void 0 : _c.currentBranchLocation) === null || _d === void 0 ? void 0 : _d.city;
        var zipCode = (_f = (_e = CCRZ.pagevars) === null || _e === void 0 ? void 0 : _e.currentBranchLocation) === null || _f === void 0 ? void 0 : _f.customerZipCode;
        this.handleGenericBranchLocationRefresh({ branchId: branchId, branchName: branchName, zipCode: zipCode });
    };
    CCStateManager.prototype.handleGenericBranchLocationRefresh = function (args) {
        this.bind.trigger(this.root, CustomEvents_1.CustomEvents.branchLocationChange, {
            first: this.lastLocation === null,
            branchId: args.branchId,
            branchName: args.branchName,
            zipCode: args.zipCode,
        });
        this.updateLocation(args.branchId);
    };
    // Hack to persist query in standalone searchbox searchbox
    CCStateManager.prototype.handleProductSearchRefresh = function () {
        try {
            // Populating the standalone searchbox with the query from the state
            var query = this.searchInterface.queryStateModel.attributes.q;
            this.updateSearchBoxValue(query);
        }
        catch (error) {
            this.logger.error('Unable to populate searchbox with state');
        }
    };
    var CCStateManager_1;
    CCStateManager.ID = 'CCStateManager';
    CCStateManager.options = {};
    CCStateManager = CCStateManager_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component contains some CloudCraze specific logic.
         * It notifies other components whenever custom Fleetpride CC events are triggered.
         *
         *
         * @export
         * @class CCStateManager
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], CCStateManager);
    return CCStateManager;
}(coveo_search_ui_1.Component));
exports.CCStateManager = CCStateManager;


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingMethod = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var SVGIcons_1 = __webpack_require__(3);
var CustomEvents_1 = __webpack_require__(4);
var CoveoUtils_1 = __webpack_require__(5);
var underscore_1 = __webpack_require__(2);
var turbo_core_1 = __webpack_require__(1);
var ShippingMethod = /** @class */ (function (_super) {
    __extends(ShippingMethod, _super);
    /**
     * Creates a new `ShippingMethod` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `ShippingMethod` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     * @param result The result to associate the component with.
     */
    function ShippingMethod(element, options, bindings, result) {
        var _this = _super.call(this, element, ShippingMethod_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.result = result;
        _this.lastUpdatedProductInfo = null;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, ShippingMethod_1, options);
        _this.ensureMethodOptions();
        _this.showPlaceholder();
        // Set default shipping method
        var initialShippingMethod;
        if (_this.options.enablePickUpMethod()) {
            initialShippingMethod = _this.options.pickupMethod;
        }
        else if (_this.options.enableShippingMethod()) {
            initialShippingMethod = _this.options.shippingMethod;
        }
        else if (_this.options.enableLocalDeliveryMethod()) {
            initialShippingMethod = _this.options.localDeliveryMethod;
        }
        _this.setShippingMethod(initialShippingMethod);
        // Since the element continues to exist in memory, we only want it to listen once
        _this.bind.oneRootElement(CustomEvents_1.CustomEvents.updateProductInfo, function (args) {
            // Just once
            _this.handleUdpateProduct(args);
        });
        _this.bind.oneRootElement(CustomEvents_1.CustomEvents.invalidProductInfo, function (err) {
            // Just once
            _this.handleInvalidProductInfo(err);
        });
        _this.bind.onRootElement(CustomEvents_1.CustomEvents.addToCartCountChange, function (args) {
            // every time
            _this.handleAddToCartCountChange(args);
        });
        return _this;
    }
    ShippingMethod_1 = ShippingMethod;
    ShippingMethod.prototype.ensureMethodOptions = function () {
        if (!underscore_1.isFunction(this.options.enableShippingMethod)) {
            this.options.enableShippingMethod = function () { return false; };
        }
        if (!underscore_1.isFunction(this.options.enablePickUpMethod)) {
            this.options.enablePickUpMethod = function () { return false; };
        }
        if (!underscore_1.isFunction(this.options.enableLocalDeliveryMethod)) {
            this.options.enableLocalDeliveryMethod = function () { return false; };
        }
        if (!underscore_1.isFunction(this.options.isGuest)) {
            this.options.isGuest = function () { return false; };
        }
    };
    ShippingMethod.prototype.showPlaceholder = function () {
        var container = coveo_search_ui_1.$$('div', { className: 'item-placeholder-container' });
        this.options.enablePickUpMethod() &&
            container.append(coveo_search_ui_1.$$('div', { className: 'item-placeholder', style: 'height:20px;width:190px;margin:8px 0;' }).el);
        this.options.enableShippingMethod() &&
            container.append(coveo_search_ui_1.$$('div', { className: 'item-placeholder', style: 'height:20px;width:160px;margin:8px 0;' }).el);
        this.options.enableLocalDeliveryMethod() &&
            container.append(coveo_search_ui_1.$$('div', { className: 'item-placeholder', style: 'height:20px;width:242px;margin:8px 0;' }).el);
        // container.append($$('div', { className: 'item-placeholder', style: 'height:24px;width:74px;margin:5px 0;' }).el); // TODO: add delivery info
        this.element.appendChild(container.el);
    };
    ShippingMethod.prototype.findInventoryFromList = function (inventory, key) {
        if (inventory && inventory[key.toUpperCase()]) {
            return inventory[key];
        }
        else {
            // this.logger.error(`Unable to find key "${key}" in the results`);
            return null;
        }
    };
    ShippingMethod.prototype.handleInvalidProductInfo = function (err) {
        this.element.innerHTML = '';
        this.element.appendChild(coveo_search_ui_1.$$('div', { className: 'availability-error' }, 'No inventory data for this branch').el);
    };
    ShippingMethod.prototype.handleAddToCartCountChange = function (args) {
        if (args.result === this.result) {
            this.handleUdpateProduct(this.lastUpdatedProductInfo, args.count);
        }
    };
    ShippingMethod.prototype.getPackMultiplicator = function (args) {
        var salespack = CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, args.options.salespack);
        return salespack && !isNaN(salespack) ? parseInt(salespack, 10) : 1;
    };
    ShippingMethod.prototype.isGuest = function () {
        return this.options.isGuest();
    };
    ShippingMethod.prototype.handleUdpateProduct = function (args, orderQuantity) {
        if (orderQuantity === void 0) { orderQuantity = 1; }
        this.lastUpdatedProductInfo = args;
        var inventoryForCurrentProduct = this.findInventoryFromList(args.inventory, CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, args.options.skuField));
        this.redrawShippingMethods(inventoryForCurrentProduct, this.getPackMultiplicator(args), this.options.enableLocalDeliveryMethod(), orderQuantity);
    };
    ShippingMethod.prototype.productEligibleForShipping = function () {
        return CoveoUtils_1.CoveoUtils.getCleanFieldValue(this.result, '@fp_prd_ship_eligibility') === 'Y'; // TODO: do not hard code field here
    };
    ShippingMethod.prototype.buildInventoryInfo = function (message, muted, svgIcon) {
        if (svgIcon === void 0) { svgIcon = SVGIcons_1.SVGIcons.icons.branch; }
        var inventoryElement = coveo_search_ui_1.$$('div', { className: 'coveops-inventory-info' });
        inventoryElement.toggleClass('muted', muted);
        inventoryElement.append(coveo_search_ui_1.$$('div', { className: 'coveops-inventory-icon-container' }, svgIcon).el);
        inventoryElement.append(coveo_search_ui_1.$$('div', {}, message).el);
        return inventoryElement.el;
    };
    /**
     * Redraws the shipping methods as well as the appropriate caption based on the user status, shipping options flags and product inventory.
     *
     * @private
     * @param {ICCInventory} inventory           Product inventory
     * @param {number} perPack                   Quantity of item within a pack
     * @param {boolean} eligibleForLocalDelivery Whether the product is eligible for local delivery
     * @param {number} orderQuantity             Order quantity
     */
    ShippingMethod.prototype.redrawShippingMethods = function (inventory, perPack, eligibleForLocalDelivery, orderQuantity) {
        this.element.innerHTML = '';
        var availableQuantity = inventory && Math.floor(inventory.totalQuantity / perPack) > 0 ? inventory.totalQuantity - (inventory.totalQuantity % perPack) : 0;
        var branchQuantity = inventory && Math.floor(inventory.primaryQuantity / perPack) > 0
            ? inventory.primaryQuantity - (inventory.primaryQuantity % perPack)
            : 0;
        if (this.isGuest()) {
            // For guest users, we need to check it the pickup flag is set to true
            if (inventory.primaryQuantity > 0) {
                // Available in branch
                this.element.appendChild(this.buildInventoryInfo('In stock available today', false));
            }
            else if (inventory.primaryQuantity === 0 && inventory.otherQuantity > 0) {
                // Available in DC but not in local branch
                this.element.appendChild(this.buildInventoryInfo('Available in 3 to 5 business days', false));
            }
            else if (inventory.totalQuantity === 0) {
                // Available nowhere
                this.element.appendChild(this.buildInventoryInfo('Call local branch for availability', true));
            }
        }
        else {
            if (availableQuantity > 0) {
                if (orderQuantity > inventory.primaryQuantity) {
                    this.options.enablePickUpMethod() && this.buildPickupShippingMethodNotAvailableYet();
                    this.options.enableShippingMethod() && this.buildShipToMeShippingMethod(this.productEligibleForShipping(), 'Ship to me');
                    this.options.enableLocalDeliveryMethod() &&
                        this.buildLocalDeliveryShippingMethod(eligibleForLocalDelivery, 'Local delivery in 3 to 5 business days');
                }
                else {
                    this.options.enablePickUpMethod() && this.buildPickupShippingMethod(branchQuantity);
                    this.options.enableShippingMethod() && this.buildShipToMeShippingMethod(this.productEligibleForShipping());
                    this.options.enableLocalDeliveryMethod() && this.buildLocalDeliveryShippingMethod(eligibleForLocalDelivery);
                }
            }
            else {
                this.element.appendChild(this.buildInventoryInfo(branchQuantity > 0 ? 'In stock available today' : this.options.outOfStockMessage, branchQuantity === 0));
            }
        }
    };
    ShippingMethod.prototype.buildPickupShippingMethodNotAvailableYet = function () {
        var _this = this;
        var message = 'Ship to branch in 3 to 5 business days';
        this.element.appendChild(this.buildLabel(this.buildInventoryInfo(message, false), function (input) { return _this.setShippingMethod(_this.options.pickupMethod); }, {
            selected: this.selectedShippingMethod === this.options.pickupMethod,
        }));
    };
    ShippingMethod.prototype.buildPickupShippingMethod = function (qty) {
        var _this = this;
        var message = Coveo.CurrencyUtils.currencyToString(qty, { decimals: 0, symbol: ' ' }) + " available for branch pick up";
        this.element.appendChild(this.buildLabel(this.buildInventoryInfo(message, qty === 0), function (input) { return _this.setShippingMethod(_this.options.pickupMethod); }, {
            selected: this.selectedShippingMethod === this.options.pickupMethod,
        }));
    };
    ShippingMethod.prototype.buildShipToMeShippingMethod = function (eligible, caption) {
        var _this = this;
        this.element.appendChild(this.buildLabel(this.buildInventoryInfo(caption || "In stock ready to ship", !eligible, SVGIcons_1.SVGIcons.icons.shippingBox), function (input) { return _this.setShippingMethod(_this.options.shippingMethod); }, {
            diabled: !eligible,
            selected: this.selectedShippingMethod === this.options.shippingMethod,
        }));
    };
    ShippingMethod.prototype.buildLocalDeliveryShippingMethod = function (eligible, caption) {
        var _this = this;
        this.element.appendChild(this.buildLabel(this.buildInventoryInfo(caption || "Local delivery " + (eligible ? 'available' : 'N/A'), !eligible, SVGIcons_1.SVGIcons.icons.localDelivery), function (input) { return _this.setShippingMethod(_this.options.localDeliveryMethod); }, {
            diabled: !eligible,
            selected: this.selectedShippingMethod === this.options.localDeliveryMethod,
        }));
    };
    ShippingMethod.prototype.buildInputId = function () {
        return ShippingMethod_1.ID + "-" + this.result.index;
    };
    ShippingMethod.prototype.buildLabel = function (shippingElement, onClick, _a) {
        var _b = _a.diabled, diabled = _b === void 0 ? false : _b, _c = _a.selected, selected = _c === void 0 ? false : _c;
        shippingElement.classList.add('coveops-radio-text');
        if (!this.options.interactive) {
            return shippingElement;
        }
        var label = coveo_search_ui_1.$$('label');
        var input = coveo_search_ui_1.$$('input', { type: 'radio', name: this.buildInputId() });
        if (diabled) {
            input.setAttribute('disabled', 'disabled');
            label.addClass('disabled');
        }
        label.append(input.el);
        label.append(coveo_search_ui_1.$$('span', { className: 'coveops-radio-btn', name: ShippingMethod_1.ID }).el);
        label.append(shippingElement);
        input.on('change', function () { return onClick(input.el); });
        if (selected) {
            input.el.checked = true;
        }
        return label.el;
    };
    ShippingMethod.prototype.setShippingMethod = function (shippingMethod) {
        this.selectedShippingMethod = shippingMethod;
        var args = {
            shippingMedhod: shippingMethod,
            index: this.result.index,
        };
        this.bind.trigger(this.root, CustomEvents_1.CustomEvents.changeShippingMethod, args);
    };
    var ShippingMethod_1;
    ShippingMethod.ID = 'ShippingMethod';
    ShippingMethod.options = {
        // enableForGuestUsers: ComponentOptions.buildBooleanOption({ defaultValue: false }),
        // caption: ComponentOptions.buildStringOption({ defaultValue: 'Add To Cart' }),
        pickupMethod: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'fp_ shipping_option1' }),
        shippingMethod: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'fp_shipping_option2' }),
        localDeliveryMethod: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'fp_shipping_option3' }),
        outOfStockMessage: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'Call local branch for availability' }),
        enableShippingMethod: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        enablePickUpMethod: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        enableLocalDeliveryMethod: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
        interactive: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: true }),
        isGuest: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
    };
    ShippingMethod = ShippingMethod_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component renders an "Add To Cart" button
         *
         * This component is a result template component (see [Result Templates](https://docs.coveo.com/en/413/)).
         *
         * @export
         * @class ShippingMethod
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], ShippingMethod);
    return ShippingMethod;
}(coveo_search_ui_1.Component));
exports.ShippingMethod = ShippingMethod;


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DynamicFacetGenerator = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var ComponentsTypes_1 = __webpack_require__(59);
var CustomEvents_1 = __webpack_require__(4);
var UrlUtils_1 = __webpack_require__(60);
var turbo_core_1 = __webpack_require__(1);
var DynamicFacetGenerator = /** @class */ (function (_super) {
    __extends(DynamicFacetGenerator, _super);
    /**
     * Creates a new `Fleetpride` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `ResultList` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     */
    function DynamicFacetGenerator(element, options, bindings) {
        var _this = _super.call(this, element, DynamicFacetGenerator_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.dictionary = null;
        _this.previousSelectedCategory = null;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, DynamicFacetGenerator_1, options);
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.buildingQuery, function (args) { return _this.handleBuildingQuery(args); });
        Promise.all([coveo_search_ui_1.load('DynamicFacet')]).then(function () {
            _this.init();
        });
        return _this;
    }
    DynamicFacetGenerator_1 = DynamicFacetGenerator;
    DynamicFacetGenerator.prototype.initLocalStorage = function () {
        this.localStorage = new coveo_search_ui_1.LocalStorageUtils(DynamicFacetGenerator_1.ID);
    };
    DynamicFacetGenerator.prototype.init = function () {
        // First check if a dictionnary has been passed as an option
        this.dictionary = this.options.dictionary;
        if (this.dictionary == null) {
            if (this.options.useCache) {
                this.initLocalStorage();
                this.loadDictionaryFromCache();
            }
            // If there is nothing in the cache and the dictionary is still null, load from async method
            if (this.dictionary == null) {
                this.fetchDictionaryAsync();
            }
        }
    };
    DynamicFacetGenerator.prototype.isParentAHierarchyFacet = function () {
        var _a;
        return ((_a = this.getParentFacet()) === null || _a === void 0 ? void 0 : _a.type) === coveo_search_ui_1.DynamicHierarchicalFacet.ID;
    };
    DynamicFacetGenerator.prototype.updateDictionary = function (dictionary) {
        this.dictionary = dictionary;
    };
    /**
     * Clears all current Dynamic Facets that have generated by this component
     *
     */
    DynamicFacetGenerator.prototype.clearGeneratedFacet = function () {
        if (this.element.children) {
            var rescueCounter = 1000; // In case it goes into an infinite loop. It is unlikely, but just in case...
            while (this.element.firstChild && rescueCounter > 0) {
                rescueCounter--;
                var child = this.element.firstChild;
                if (child) {
                    var facet = coveo_search_ui_1.get(child, 'DynamicFacet');
                    if (facet) {
                        // Disabling the Facet
                        facet.disable();
                        // Removing the Facet element from the DOM
                        this.element.removeChild(child);
                        var existingFacet = this.componentStateModel.attributes[coveo_search_ui_1.QueryStateModel.getFacetId(facet.options.id)];
                        if (existingFacet && existingFacet.length) {
                            // Even if we disable the Facet component and remove the HTML element form the DOM, it will continue to exist in the componentStateModel. So we need to manually remove it from the state.
                            this.componentStateModel.attributes[coveo_search_ui_1.QueryStateModel.getFacetId(facet.options.id)] = [];
                        }
                    }
                }
            }
        }
    };
    /**
     * Tries to load the dictionary from the Local Storage
     *
     * @private
     */
    DynamicFacetGenerator.prototype.loadDictionaryFromCache = function () {
        this.logger.debug('Loading dictionary from cache');
        this.dictionary = this.localStorage.load();
    };
    /**
     * Tries to load the dictionary from an async method provided as an option
     *
     * @private
     */
    DynamicFacetGenerator.prototype.fetchDictionaryAsync = function () {
        var _this = this;
        if (this.options.getDictionaryPromise) {
            this.options
                .getDictionaryPromise()
                .then(function (dict) {
                _this.dictionary = dict;
                if (_this.options.useCache) {
                    _this.logger.debug('Saving dict into local storage');
                    _this.localStorage.save(dict);
                }
                if (_this.getAllowedFacets(_this.getCurrentSelectedParentValue())) {
                    // Do not reload the search if the parent facet is not selected
                    _this.reloadSearch();
                }
            })
                .catch(function (err) {
                _this.logger.error('Unable to fetch dictionary', err);
            });
        }
    };
    /**
     * Reloads the results by executing a search query. This method will only be called in the callback of the async method.
     *
     * @private
     */
    DynamicFacetGenerator.prototype.reloadSearch = function () {
        var _this = this;
        this.queryController.executeQuery({
            beforeExecuteQuery: function () { return _this.logSearchEvent(CustomEvents_1.CustomEvents.updateFacetDictionary); },
            ignoreWarningSearchEvent: true,
            logInActionsHistory: false,
        });
    };
    DynamicFacetGenerator.prototype.logSearchEvent = function (eventName) {
        this.usageAnalytics.logSearchEvent({ name: eventName, type: 'customEventType' }, {});
    };
    /**
     * Get the parent Facet to drive the generation of dynamic Facets
     *
     * @private
     * @returns {Component}
     */
    DynamicFacetGenerator.prototype.getParentFacet = function () {
        var _this = this;
        var masterFacetComponent = ComponentsTypes_1.ComponentsTypes.getAllFacetInstancesFromElement(this.root).filter(function (cmp) {
            // Right now, the component has only been tested with DynamicFacet and DynamicHierarchicalFacet components
            var idFacet = _.reduce(ComponentsTypes_1.ComponentsTypes.allFacetsType, function (memo, type) { return cmp instanceof type || memo; }, false);
            return idFacet && cmp.options.id === _this.options.dependsOn;
        });
        if (!masterFacetComponent.length) {
            this.logger.warn("Unable to find a Facet with the id or field \"" + this.options.dependsOn + "\".", "The master facet values can't be updated.");
            return;
        }
        if (masterFacetComponent.length > 1) {
            this.logger.warn("Multiple facets with id \"" + this.options.dependsOn + "\" found.", "A given facet may only depend on a single other facet.", "Ensure that each facet in your search interface has a unique id.", masterFacetComponent);
            return;
        }
        return masterFacetComponent[0];
    };
    /**
     * Generate the Facets passed as parameters
     *
     * @private
     * @param {IFacetTransformArgs[]} facets to generate
     */
    DynamicFacetGenerator.prototype.generateFacets = function (facets) {
        var _this = this;
        facets.map(function (facet) {
            var element = coveo_search_ui_1.$$('div');
            _this.element.appendChild(element.el);
            var generatedFacet = new coveo_search_ui_1.DynamicFacet(element.el, {
                field: facet.field,
                title: facet.facetTitle,
                collapsedByDefault: _this.options.collapseAdditionalFacets,
            });
            _this.ensureFacetState(generatedFacet);
        });
    };
    /**
     * This is required to update the generated facets based on the state. Since they are generated at a later stage, they cannot listen to state change. This method ensures the state of every dynamically generated facet is preserved and correctly handled
     *
     * @private
     * @param {DynamicFacet} facet
     */
    DynamicFacetGenerator.prototype.ensureFacetState = function (facet) {
        // First read the url
        var params = UrlUtils_1.UrlUtils.getUrlParams(location.href);
        var facetId = coveo_search_ui_1.QueryStateModel.getFacetId(facet.options.id);
        // check if the facet state is consistent between the url and the component
        // This is required because the facets are generated too late
        if (params && params[facetId] && params[facetId] !== JSON.stringify(this.queryStateModel.attributes[facetId])) {
            // if there is a facet value selected in the state but not in the UI.
            // This happens because the facets could be generated dynamically at any moment
            try {
                var values = _.toArray(params[facetId]).slice(1, -1).join('').split(',');
                if (values) {
                    // There is a facet value in the url that is not saved into the state
                    facet.selectMultipleValues(values);
                }
            }
            catch (error) {
                this.logger.error('Unable to parse facet state in the url', params[facetId]);
            }
        }
    };
    DynamicFacetGenerator.prototype.updateDynamicFacetAppareance = function () {
        var _this = this;
        var selectedCategory = this.getCurrentSelectedParentValue();
        if (selectedCategory !== this.previousSelectedCategory) {
            // Do not clear if same parent selected
            this.clearGeneratedFacet();
            // Do not regenerate facets if parent facet has not changed
            var facets = this.getAllowedFacets(selectedCategory);
            if (facets && facets.length > 0) {
                if (underscore_1.isFunction(this.options.transformer)) {
                    this.generateFacets(facets.map(function (facet) { return _this.options.transformer(facet); }));
                }
                else {
                    this.logger.error('transformer option is not defined');
                }
            }
            this.previousSelectedCategory = selectedCategory;
        }
    };
    /**
     * Returns the approrpiate Facets names based on the selected parent value.
     *
     * @private
     * @param {string} key selected parent value. Should be a key in the dictionary
     * @returns {string[]} Facet names
     */
    DynamicFacetGenerator.prototype.getAllowedFacets = function (key) {
        return this.dictionary ? this.dictionary[key] : null;
    };
    /**
     * Get the current selected value on the parent Facet
     *
     * @private
     * @returns {(string | null)} The parent selected value or null if no value is found
     */
    DynamicFacetGenerator.prototype.getCurrentSelectedParentValue = function () {
        var facetAttributes = this.queryStateModel.attributes[coveo_search_ui_1.QueryStateModel.getFacetId(this.options.dependsOn)];
        if (facetAttributes) {
            return facetAttributes.length > 0 ? (this.isParentAHierarchyFacet() ? underscore_1.last(facetAttributes) : facetAttributes[0]) : null;
        }
        else {
            this.logger.warn('Unable to find facet attribute', this.options.dependsOn);
            return null;
        }
    };
    DynamicFacetGenerator.prototype.handleBuildingQuery = function (args) {
        if (this.dictionary && this.getParentFacet()) {
            this.updateDynamicFacetAppareance();
        }
    };
    var DynamicFacetGenerator_1;
    DynamicFacetGenerator.ID = 'DynamicFacetGenerator';
    DynamicFacetGenerator.options = {
        useCache: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: false }),
        collapseAdditionalFacets: coveo_search_ui_1.ComponentOptions.buildBooleanOption({ defaultValue: false }),
        allowedFields: coveo_search_ui_1.ComponentOptions.buildListOption(),
        /**
         * Specifies a dictionary with
         * To specify the parent facet, use its id.
         */
        dictionary: coveo_search_ui_1.ComponentOptions.buildJsonOption(),
        /**
         * Specifies whether this facet only appears when a value is selected in its "parent" facet.
         * To specify the parent facet, use its id.
         */
        dependsOn: coveo_search_ui_1.ComponentOptions.buildStringOption(),
        /**
         * A function that verifies whether the current state of the `dependsOn` facet allows the dependent facet to be displayed.
         *
         * TODO: update doc
         * If specified, the function receives a reference to the resolved `dependsOn` facet component instance as an argument, and must return a boolean.
         * The function's argument should typically be treated as read-only.
         *
         * By default, the dependent facet is displayed whenever one or more values are selected in its `dependsOn` facet.
         *
         */
        // dependsOnCondition: ComponentOptions.buildCustomOption<any>(() => {
        //   return null;
        // }),
        transformer: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () { return null; }),
        getDictionaryPromise: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
    };
    DynamicFacetGenerator = DynamicFacetGenerator_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * Dynamically generates facets when a condition is met
         *
         * @export
         * @class DynamicFacetGenerator
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], DynamicFacetGenerator);
    return DynamicFacetGenerator;
}(coveo_search_ui_1.Component));
exports.DynamicFacetGenerator = DynamicFacetGenerator;


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.ComponentsTypes = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var ComponentsTypes = /** @class */ (function () {
    function ComponentsTypes() {
    }
    Object.defineProperty(ComponentsTypes, "allFacetsType", {
        get: function () {
            return [coveo_search_ui_1.DynamicFacet, coveo_search_ui_1.DynamicHierarchicalFacet];
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ComponentsTypes, "allFacetsTypeString", {
        get: function () {
            return ComponentsTypes.allFacetsType.map(function (type) { return type.ID; });
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ComponentsTypes, "allFacetsClassname", {
        get: function () {
            return ComponentsTypes.allFacetsTypeString.map(function (type) { return "Coveo" + type; });
        },
        enumerable: false,
        configurable: true
    });
    ComponentsTypes.getAllFacetElementsFromElement = function (root) {
        var selectors = ComponentsTypes.allFacetsClassname.map(function (className) { return "." + className; }).join(', ');
        return root.querySelectorAll(selectors);
    };
    ComponentsTypes.getAllFacetInstancesFromElement = function (root) {
        return underscore_1.map(ComponentsTypes.getAllFacetElementsFromElement(root), function (element) { return coveo_search_ui_1.Component.get(element); });
    };
    return ComponentsTypes;
}());
exports.ComponentsTypes = ComponentsTypes;


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.UrlUtils = void 0;
var UrlUtils = /** @class */ (function () {
    function UrlUtils() {
    }
    UrlUtils.getUrlParams = function (query) {
        if (!query) {
            return {};
        }
        var parser = document.createElement('a');
        var search = '';
        parser.href = query;
        var hash = parser.hash.substring(1);
        if (hash) {
            var hashParser = document.createElement('a');
            hashParser.href = hash;
            search = hashParser.search.substring(1);
        }
        else {
            search = parser.search.substring(1);
        }
        search = search || query;
        return (/^[?#]/.test(search) ? search.slice(1) : search).split('&').reduce(function (params, param) {
            var _a = param.split('='), key = _a[0], value = _a[1];
            params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
            return params;
        }, {});
    };
    UrlUtils.getLocationFromUri = function (query) {
        if (!query) {
            return {};
        }
        var anchor = document.createElement('a');
        anchor.href = query;
        var retVal = {
            href: anchor.href,
            pathname: anchor.pathname,
            hostname: anchor.hostname,
            host: anchor.host,
            search: anchor.search,
            protocol: anchor.protocol,
            hash: anchor.hash,
        };
        return retVal;
    };
    return UrlUtils;
}());
exports.UrlUtils = UrlUtils;


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlaceholderOverride = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var PlaceholderOverride = /** @class */ (function (_super) {
    __extends(PlaceholderOverride, _super);
    function PlaceholderOverride(element, options, bindings, result) {
        var _this = _super.call(this, element, PlaceholderOverride_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.result = result;
        _this.bind.onRootElement(coveo_search_ui_1.InitializationEvents.afterInitialization, _this.handleAfterInitialization);
        return _this;
    }
    PlaceholderOverride_1 = PlaceholderOverride;
    PlaceholderOverride.prototype.handleAfterInitialization = function () {
        var _this = this;
        underscore_1.each(this.getResults(), function (res) {
            _this.override(res);
        });
    };
    PlaceholderOverride.prototype.override = function (element) {
        var template = this.listTemplate();
        element.innerHTML = template;
    };
    PlaceholderOverride.prototype.getResults = function () {
        return coveo_search_ui_1.$$(this.root).findAll('.coveo-result-frame.coveo-placeholder-result');
    };
    PlaceholderOverride.prototype.cartTemplate = function () {
        // TODO:
        //   return `<div class="item-placeholder-container">
        //   <div class="flex-column">
        //     <div class="item-placeholder ProductImagePlaceholder" style="min-width: 254px; height: 254px;"></div>
        //     <div class="flex-column flex-space-between">
        //       <div>
        //         <div class="item-placeholder" style="height: 20px; width: 290px; margin: 8px 0;"></div>
        //       </div>
        //       <div class="flex-column">
        //         <div class="flex flex-justify-end">
        //           <div class="item-placeholder-container flex-column flex-align-end">
        //             <div class="item-placeholder" style="height: 16px; width: 54px; margin: 8px 0;"></div>
        //             <div class="item-placeholder" style="height: 24px; width: 74px; margin: 5px 0;"></div>
        //           </div>
        //         </div>
        //         <div class="flex-space-between flex-wrap flex-align-center">
        //           <div class="item-placeholder-container flex-column">
        //             <div class="item-placeholder" style="height: 20px; width: 190px; margin: 8px 0;"></div>
        //             <div class="item-placeholder" style="height: 20px; width: 160px; margin: 8px 0;"></div>
        //             <div class="item-placeholder" style="height: 20px; width: 242px; margin: 8px 0;"></div>
        //           </div>
        //           <div class="item-placeholder" style="height: 40px; width: 186px; margin: 3px 0;"></div>
        //         </div>
        //       </div>
        //     </div>
        //   </div>
        // </div>
        // `;
    };
    PlaceholderOverride.prototype.listTemplate = function () {
        return "<div class=\"item-placeholder-container\">\n    <div class=\"item-placeholder ProductImagePlaceholder\" style=\"margin-right:23px;min-width:254px;height:254px\"></div>\n\n    <div class=\"flex-column flex-space-between\">\n      <div>\n        <div class=\"item-placeholder\" style=\"height:20px;width:290px;margin:8px 0;\"></div>\n\n        <div class=\"flex\">\n          <div class=\"item-placeholder\" style=\"height:20px;width:110px;margin:8px 28px 3px 0;\"></div>\n          <div class=\"item-placeholder\" style=\"height:20px;width:130px;margin:8px 0;\"></div>\n        </div>\n      </div>\n\n      <div class=\"flex-column\">\n        <div class=\"flex flex-justify-end\">\n          <div class=\"item-placeholder-container flex-column flex-align-end\">\n            <div class=\"item-placeholder\" style=\"height:16px;width:54px;margin:8px 0;\"></div>\n            <div class=\"item-placeholder\" style=\"height:24px;width:74px;margin:5px 0;\"></div>\n          </div>\n        </div>\n\n        <div class=\"flex-space-between flex-wrap flex-align-center\">\n          <div class=\"item-placeholder-container flex-column\">\n            <div class=\"item-placeholder\" style=\"height:20px; width: 190px; margin:8px 0\"></div>\n            <div class=\"item-placeholder\" style=\"height:20px; width: 160px; margin:8px 0\"></div>\n            <div class=\"item-placeholder\" style=\"height:20px; width: 242px; margin:8px 0\"></div>\n          </div>\n\n          <div class=\"item-placeholder\" style=\"height:40px; width:186px; margin: 3px 0\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n  ";
    };
    var PlaceholderOverride_1;
    PlaceholderOverride.ID = 'PlaceholderOverride';
    PlaceholderOverride.options = {};
    PlaceholderOverride = PlaceholderOverride_1 = __decorate([
        turbo_core_1.lazyComponent
    ], PlaceholderOverride);
    return PlaceholderOverride;
}(coveo_search_ui_1.Component));
exports.PlaceholderOverride = PlaceholderOverride;


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductCategoryFilter = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var ProductCategoryFilter = /** @class */ (function (_super) {
    __extends(ProductCategoryFilter, _super);
    function ProductCategoryFilter(element, options, bindings) {
        var _this = _super.call(this, element, ProductCategoryFilter_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.productBreadcrumb = null;
        _this.productSubCategories = null;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, ProductCategoryFilter_1, options);
        _this.element.classList.add('hidden');
        _this.initHtmleleElements();
        _this.initQueryEvents();
        _this.initQueryStateEvents();
        return _this;
    }
    ProductCategoryFilter_1 = ProductCategoryFilter;
    ProductCategoryFilter.prototype.shouldClearFiltersOnNewQuery = function () {
        var searchboxElement = this.root.querySelector('.CoveoSearchbox');
        if (!searchboxElement) {
            return false;
        }
        var searchboxInstance = coveo_search_ui_1.get(searchboxElement, 'Searchbox');
        return searchboxInstance ? searchboxInstance.options.clearFiltersOnNewQuery : false;
    };
    ProductCategoryFilter.prototype.initHtmleleElements = function () {
        this.productBreadcrumb = coveo_search_ui_1.$$('div');
        this.element.appendChild(this.productBreadcrumb.el);
        this.productSubCategories = coveo_search_ui_1.$$('div', { className: 'product-sub-categories flex flex-wrap' });
        this.element.appendChild(this.productSubCategories.el);
    };
    Object.defineProperty(ProductCategoryFilter.prototype, "facetQueryStateAttribute", {
        get: function () {
            return "f:" + this.options.field;
        },
        enumerable: false,
        configurable: true
    });
    ProductCategoryFilter.prototype.initQueryEvents = function () {
        this.bind.onRootElement(coveo_search_ui_1.QueryEvents.newQuery, this.handleNewQuery);
        this.bind.onRootElement(coveo_search_ui_1.QueryEvents.buildingQuery, this.handleBuildingQuery);
        this.bind.onRootElement(coveo_search_ui_1.QueryEvents.querySuccess, this.handleQuerySuccess);
    };
    ProductCategoryFilter.prototype.initQueryStateEvents = function () {
        var _this = this;
        this.queryStateModel.registerNewAttribute('category', 0);
        this.bind.onQueryState('change:', 'category', function (data) { return _this.handleCategoryPathChange(data); });
        this.bind.onQueryState('change:', this.facetQueryStateAttribute, function (data) {
            return _this.handleCategoryPathChange(data);
        });
    };
    Object.defineProperty(ProductCategoryFilter.prototype, "currentCategory", {
        get: function () {
            return this.queryStateModel.get(this.facetQueryStateAttribute) || [];
        },
        enumerable: false,
        configurable: true
    });
    ProductCategoryFilter.prototype.handleCategoryPathChange = function (data) {
        var condition = this.isCategoryPage() && this.currentCategory.length > 0;
        this.element.classList.toggle('hidden', !condition);
    };
    ProductCategoryFilter.prototype.getPartTypeElement = function () {
        var facetElement = this.root.querySelector(".CoveoDynamicHierarchicalFacet[data-field=\"" + this.options.field + "\"]");
        return facetElement;
    };
    ProductCategoryFilter.prototype.getPartTypeFacetInstance = function () {
        var facetElement = this.getPartTypeElement();
        return facetElement ? coveo_search_ui_1.get(facetElement, 'DynamicHierarchicalFacet') : null;
    };
    ProductCategoryFilter.prototype.togglePartTypeFacet = function (hide) {
        var partTypeFacet = this.getPartTypeElement();
        if (partTypeFacet) {
            partTypeFacet.classList.toggle('hidden', hide);
        }
    };
    ProductCategoryFilter.prototype.buildBreadcrumbLink = function (title, href) {
        var anchor = coveo_search_ui_1.$$('li', {}, coveo_search_ui_1.$$('a', { href: "" + href }, title));
        return anchor.el;
    };
    ProductCategoryFilter.prototype.updateCoveoText = function (text) {
        var coveoText = this.root.querySelector('.CoveoText.coveops-super-title');
        if (coveoText) {
            coveoText.textContent = text;
        }
    };
    ProductCategoryFilter.prototype.buildHref = function (path) {
        // Since we did not find how to reroute the user to the coveo page while keeping the same CloudCraze URL, we are always redirecting to the Coveo subscriber page.
        // In the future, we have to figure out how to plug Coveo in a native CloudCraze category page.
        return this.options.coveoSearchPage + "#category=1&" + this.facetQueryStateAttribute + "=[" + path.map(function (m) {
            return encodeURIComponent(m);
        }) + "]&layout=card";
        if (path.length >= this.options.maximumNumberOfLevels) {
            // We have reached the last level of category. Redirecting to OOTB CC category page
            // So if the homePathName is "/parts/ccrz__HomePage?cclcl=en_US/", the storefront will be parts
            var storefront = underscore_1.compact(this.options.homePagePathname.split('/') || [])[0];
            return "/" + storefront + "/" + path.map(function (m) { return encodeURIComponent(m.replace(/[\W_]+/g, '-').toLowerCase()); }).join('/');
        }
        return this.options.coveoSearchPage + "#category=1&" + this.facetQueryStateAttribute + "=[" + path.map(function (m) {
            return encodeURIComponent(m);
        }) + "]&layout=card";
    };
    ProductCategoryFilter.prototype.redrawSubCategories = function (subCategories) {
        var _this = this;
        this.productSubCategories.el.querySelectorAll('*').forEach(function (n) { return n.remove(); });
        if (!this.isLastLevelOfCategory()) {
            underscore_1.each(subCategories, function (sub) {
                _this.productSubCategories.append(_this.buildSubCategory(sub));
            });
        }
    };
    ProductCategoryFilter.prototype.buildSubCategory = function (sub) {
        var element = coveo_search_ui_1.$$('div', { className: 'flex-align-center' });
        element.append(coveo_search_ui_1.$$('a', { href: this.buildHref(this.currentCategory.concat(sub)) }, sub).el);
        return element.el;
    };
    ProductCategoryFilter.prototype.redrawProductBreadcrumb = function () {
        var _this = this;
        this.productBreadcrumb.setHtml('');
        var breadcrumb = coveo_search_ui_1.$$('ol', { className: 'coveops-product-breadcrumb' }, '');
        breadcrumb.append(this.buildBreadcrumbLink('Home', this.options.homePagePathname));
        this.currentCategory.map(function (level, idx) {
            breadcrumb.append(coveo_search_ui_1.$$('li', { className: 'coveops-product-breadcrumb-separator' }, '/').el);
            breadcrumb.append(_this.buildBreadcrumbLink(level, _this.buildHref(_this.currentCategory.slice(0, idx + 1))));
        });
        this.productBreadcrumb.append(breadcrumb.el);
    };
    ProductCategoryFilter.prototype.recursivelyBuildCategoryTree = function (facetValues, subCategories, numberOfLevels, securityCount) {
        var _this = this;
        if (securityCount === void 0) { securityCount = 10; }
        if (securityCount-- < 0) {
            this.logger.error('Something went wrong in the recursive function');
            return;
        }
        if (numberOfLevels-- === 0) {
            // FIXME: does not work
            // This is a leaf
            this.logger.info('End of hierarchical tree', facetValues);
            return;
        }
        if (facetValues.length === 0) {
            return;
        }
        else {
            underscore_1.each(facetValues, function (f) {
                if (f.children.length > 0) {
                    _this.recursivelyBuildCategoryTree(f.children, subCategories, numberOfLevels);
                }
                else {
                    subCategories.push(f.value);
                }
            });
        }
    };
    ProductCategoryFilter.prototype.handleQuerySuccess = function (data) {
        var _this = this;
        // TODO: add a condition for last element in the tree
        // if (this.isCategoryPage() && this.currentCategory.length < this.options.maximumNumberOfLevels) {
        if (this.isCategoryPage() && this.currentCategory.length > 0) {
            var facetResponse = underscore_1.find(data.results.facets, function (f) { return f.facetId === _this.options.field; });
            if (facetResponse) {
                var subCategories = [];
                this.recursivelyBuildCategoryTree(facetResponse.values, subCategories, this.options.maximumNumberOfLevels);
                this.redrawSubCategories(subCategories);
                this.redrawProductBreadcrumb();
                this.updateCoveoText(underscore_1.last(this.currentCategory));
            }
        }
        else {
            this.updateCoveoText('SEARCH RESULTS');
        }
        this.togglePartTypeFacet(this.isCategoryPage());
    };
    ProductCategoryFilter.prototype.isLastLevelOfCategory = function () {
        return this.currentCategory.length === this.options.maximumNumberOfLevels;
    };
    ProductCategoryFilter.prototype.handleNewQuery = function (data) {
        // Clearing category on new query if the user has entered a query
        if (this.shouldClearFiltersOnNewQuery() && this.getLastQuery()) {
            this.clearCategoryState();
        }
    };
    ProductCategoryFilter.prototype.handleBuildingQuery = function (data) {
        if (this.isCategoryPage() && this.currentCategory.length > 0) {
            var facet = this.getPartTypeFacetInstance();
            facet.selectPath(this.currentCategory);
        }
    };
    ProductCategoryFilter.prototype.isCategoryPage = function () {
        return this.queryStateModel.get('category') === 1;
    };
    ProductCategoryFilter.prototype.clearCategoryState = function () {
        this.queryStateModel.set('category', 0);
    };
    ProductCategoryFilter.prototype.getLastQuery = function () {
        return coveo_search_ui_1.state(this.root, 'q');
    };
    var ProductCategoryFilter_1;
    ProductCategoryFilter.ID = 'ProductCategoryFilter';
    ProductCategoryFilter.options = {
        field: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_categoryhierarchy' }),
        homePagePathname: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: '' }),
        hierarchicalFieldSeparator: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: '|' }),
        coveoSearchPage: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: '' }),
        maximumNumberOfLevels: coveo_search_ui_1.ComponentOptions.buildNumberOption({ defaultValue: 3 }),
    };
    ProductCategoryFilter = ProductCategoryFilter_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component handles category pages.
         * Since category pages and the global search page are using the same url, this component takes care of changing the appearance of the search page.
         *
         * It uses the category query paramater as a flag.
         * If &category=1 is appended to the url, the search page is a category page.
         *
         * Category pages have the following behaviour:
         * 1. Category breadcrumb at the top
         * 2. Part type facet hidden
         * 3. Part categories are rendered at the top of the search page.
         *
         * @export
         * @class ProductCategoryFilter
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], ProductCategoryFilter);
    return ProductCategoryFilter;
}(coveo_search_ui_1.Component));
exports.ProductCategoryFilter = ProductCategoryFilter;


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommerceDataLayer = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var CommerceDataLayerEvents_1 = __webpack_require__(9);
var CommerceDataLayer = /** @class */ (function (_super) {
    __extends(CommerceDataLayer, _super);
    function CommerceDataLayer(element, options, bindings) {
        var _this = _super.call(this, element, CommerceDataLayer_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.displayedProducts = [];
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, CommerceDataLayer_1, options);
        if (typeof _this.options.productFormatter === 'function') {
            // Ensure that we have a valid dataLayer
            window.dataLayer = window.dataLayer || [];
            // Query Success
            _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.querySuccess, function (args) { return _this.handleQuerySuccess(args); });
            // Analytics Events
            _this.bind.onRootElement(coveo_search_ui_1.AnalyticsEvents.changeAnalyticsCustomData, function (args) {
                return _this.handleChangeAnalyticsCustomData(args);
            });
            // ResultList Events
            _this.bind.onRootElement(coveo_search_ui_1.ResultListEvents.newResultDisplayed, function (args) {
                return _this.handleNewResultDisplayed(args);
            });
            _this.bind.onRootElement(coveo_search_ui_1.ResultListEvents.newResultsDisplayed, function () { return _this.handleNewResultsDisplayed(); });
            // Commerce Data Layer Events
            _this.bind.onRootElement(CommerceDataLayerEvents_1.CommerceDataLayerEvents.productClick, function (args) { return _this.handleProductClick(args); });
            _this.bind.onRootElement(CommerceDataLayerEvents_1.CommerceDataLayerEvents.addToCart, function (args) { return _this.handleAddToCart(args); });
            _this.bind.onRootElement(CommerceDataLayerEvents_1.CommerceDataLayerEvents.productDetail, function (args) { return _this.handleProductDetail(args); });
        }
        else {
            _this.logger.error('Missing valid function for productFormatter option.');
        }
        return _this;
    }
    CommerceDataLayer_1 = CommerceDataLayer;
    CommerceDataLayer.prototype.productFormatter = function (res) {
        try {
            return this.options.productFormatter(res);
        }
        catch (error) {
            this.logger.error('Something went wrong in the fomratter', error);
            return null;
        }
    };
    CommerceDataLayer.prototype.pushToDataLayer = function (commerceActivity) {
        try {
            this.logger.info('Pushing to dataLayer.', commerceActivity);
            window.dataLayer.push(commerceActivity);
        }
        catch (error) {
            this.logger.error('Cannot push to dataLayer.');
        }
    };
    /**
     * Query Success
     */
    CommerceDataLayer.prototype.handleQuerySuccess = function (args) {
        this.searchUid = args.results.searchUid;
        this.handleSearch(args);
    };
    /**
     * Change Analytics Custom Data
     */
    CommerceDataLayer.prototype.handleChangeAnalyticsCustomData = function (args) {
        if (args.type === 'ClickEvent') {
            this.handleProductClick([args['resultData']]);
        }
    };
    CommerceDataLayer.prototype.handleNewResultDisplayed = function (args) {
        var product = this.productFormatter(args.result);
        this.displayedProducts.push(product);
    };
    CommerceDataLayer.prototype.handleNewResultsDisplayed = function () {
        this.pushToDataLayer({
            searchUid: "coveo:search:" + this.searchUid,
            event: 'coveo-impression-data-loaded',
            ecommerce: {
                impressions: this.displayedProducts,
            },
        });
    };
    CommerceDataLayer.prototype.handleSearch = function (args) {
        this.pushToDataLayer({
            event: 'coveo-search-data-loaded',
            'search-data': {
                q: args.query.q || '',
                searchUid: this.searchUid,
                count: args.results.totalCount,
            },
        });
    };
    /**
     * Measuring Product Clicks.
     *
     * This function is executed when a user clicks on a product link. This function uses the event
     * callback datalayer variable to handle navigation after the ecommerce data has been sent
     * to Google Analytics.
     *
     * @param {ICommerceBaseData[]} args
     */
    CommerceDataLayer.prototype.handleProductClick = function (args) {
        var _this = this;
        this.pushToDataLayer({
            event: 'productClick',
            ecommerce: {
                click: {
                    actionField: { list: 'Product Listing Page', searchUid: "coveo:search:" + this.searchUid },
                    products: args.map(function (a) { return _this.productFormatter(a); }),
                },
            },
            eventCallback: function () {
                // document.location = args.url
            },
        });
    };
    /**
     * Measuring Additions or Removals from a Shopping Cart
     *
     * @param {ICommerceProductData[]} args
     */
    CommerceDataLayer.prototype.handleAddToCart = function (args) {
        this.pushToDataLayer({
            event: 'addToCart',
            ecommerce: {
                add: {
                    actionField: { list: 'Product Listing Page', searchUid: "coveo:search:" + this.searchUid },
                    products: underscore_1.extend(this.productFormatter(args.result), { quantity: args.count }),
                },
            },
        });
    };
    CommerceDataLayer.prototype.handleProductDetail = function (args) {
        // TODO: to implement
        // const product: ICommerceDataLayerProduct = this.options.productFormatter(args);
        // this.pushToDataLayer({
        //   coveoSearchUid: this.searchUid,
        //   ecommerce: {
        //     detail: {
        //       actionField: { list: 'Product Listing Page', searchUid: `coveo:search:${this.searchUid}` },
        //       products: [product],
        //     },
        //   },
        // });
    };
    var CommerceDataLayer_1;
    CommerceDataLayer.ID = 'CommerceDataLayer';
    CommerceDataLayer.options = {
        productFormatter: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () { return null; }),
    };
    CommerceDataLayer = CommerceDataLayer_1 = __decorate([
        turbo_core_1.lazyComponent
    ], CommerceDataLayer);
    return CommerceDataLayer;
}(coveo_search_ui_1.Component));
exports.CommerceDataLayer = CommerceDataLayer;


/***/ }),
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(68);
module.exports = __webpack_require__(75);


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var FleetprideCusto_1 = __webpack_require__(11);
Object.defineProperty(exports, "FleetprideCusto", { enumerable: true, get: function () { return FleetprideCusto_1.FleetprideCusto; } });
var ProductImage_1 = __webpack_require__(32);
Object.defineProperty(exports, "ProductImage", { enumerable: true, get: function () { return ProductImage_1.ProductImage; } });
var ItemPrice_1 = __webpack_require__(7);
Object.defineProperty(exports, "ItemPrice", { enumerable: true, get: function () { return ItemPrice_1.ItemPrice; } });
var AddToCart_1 = __webpack_require__(33);
Object.defineProperty(exports, "AddToCart", { enumerable: true, get: function () { return AddToCart_1.AddToCart; } });
var VehicleFitFilter_1 = __webpack_require__(34);
Object.defineProperty(exports, "VehicleFitFilter", { enumerable: true, get: function () { return VehicleFitFilter_1.VehicleFitFilter; } });
var SaveToFavorites_1 = __webpack_require__(36);
Object.defineProperty(exports, "SaveToFavorites", { enumerable: true, get: function () { return SaveToFavorites_1.SaveToFavorites; } });
var RemoteActionManager_1 = __webpack_require__(55);
Object.defineProperty(exports, "RemoteActionManager", { enumerable: true, get: function () { return RemoteActionManager_1.RemoteActionManager; } });
var AvailabilityFacet_1 = __webpack_require__(69);
Object.defineProperty(exports, "AvailabilityFacet", { enumerable: true, get: function () { return AvailabilityFacet_1.AvailabilityFacet; } });
var CCStateManager_1 = __webpack_require__(56);
Object.defineProperty(exports, "CCStateManager", { enumerable: true, get: function () { return CCStateManager_1.CCStateManager; } });
var ShippingMethod_1 = __webpack_require__(57);
Object.defineProperty(exports, "ShippingMethod", { enumerable: true, get: function () { return ShippingMethod_1.ShippingMethod; } });
var DynamicFacetGenerator_1 = __webpack_require__(58);
Object.defineProperty(exports, "DynamicFacetGenerator", { enumerable: true, get: function () { return DynamicFacetGenerator_1.DynamicFacetGenerator; } });
var PlaceholderOverride_1 = __webpack_require__(61);
Object.defineProperty(exports, "PlaceholderOverride", { enumerable: true, get: function () { return PlaceholderOverride_1.PlaceholderOverride; } });
var BetterSortDropdown_1 = __webpack_require__(70);
Object.defineProperty(exports, "BetterSortDropdown", { enumerable: true, get: function () { return BetterSortDropdown_1.BetterSortDropdown; } });
var CustomCrossReference_1 = __webpack_require__(72);
Object.defineProperty(exports, "CustomCrossReference", { enumerable: true, get: function () { return CustomCrossReference_1.CustomCrossReference; } });
var ProductCategoryFilter_1 = __webpack_require__(62);
Object.defineProperty(exports, "ProductCategoryFilter", { enumerable: true, get: function () { return ProductCategoryFilter_1.ProductCategoryFilter; } });
var CommerceDataLayer_1 = __webpack_require__(63);
Object.defineProperty(exports, "CommerceDataLayer", { enumerable: true, get: function () { return CommerceDataLayer_1.CommerceDataLayer; } });
var WishList_1 = __webpack_require__(73);
Object.defineProperty(exports, "WishList", { enumerable: true, get: function () { return WishList_1.WishList; } });
var AdvancedResultTagging_1 = __webpack_require__(74);
Object.defineProperty(exports, "AdvancedResultTagging", { enumerable: true, get: function () { return AdvancedResultTagging_1.AdvancedResultTagging; } });
var Version_1 = __webpack_require__(39);
Object.defineProperty(exports, "PSversion", { enumerable: true, get: function () { return Version_1.PSversion; } });
var SwapVar_1 = __webpack_require__(40);
SwapVar_1.swapVar(this);
// cultures for webpack
__webpack_require__(41);
__webpack_require__(42);
__webpack_require__(43);


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AvailabilityFacet = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var SVGIcons_1 = __webpack_require__(3);
var CustomEvents_1 = __webpack_require__(4);
var AvailabilityFacet = /** @class */ (function (_super) {
    __extends(AvailabilityFacet, _super);
    function AvailabilityFacet(element, options, bindings) {
        var _this = _super.call(this, element, AvailabilityFacet_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.branchInfo = null;
        _this.isCollapsed = false;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, AvailabilityFacet_1, options);
        _this.ensureMethodOptions();
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.buildingQuery, _this.handleBuildingQuery);
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.deferredQuerySuccess, _this.handleQuerySuccess);
        _this.bind.onRootElement(coveo_search_ui_1.BreadcrumbEvents.populateBreadcrumb, _this.handlePopulateBreadcrumb);
        _this.bind.onRootElement(CustomEvents_1.CustomEvents.branchLocationChange, function (data) {
            // This is a event fired by CloudCraze and might resolve before Coveo loads required Lazy components.
            return _this.deferred.then(function () { return _this.handleBranchLocationRefresh(data); });
        });
        // We first need to ensure the DynamicFacet component is loaded from the JSUI.
        _this.deferred = new Promise(function (resolve, reject) {
            Promise.all([coveo_search_ui_1.load('DynamicFacet'), coveo_search_ui_1.load('Checkbox')]).then(function () {
                resolve();
            });
        });
        _this.deferred.then(function () {
            _this.init();
        });
        _this.bind.onQueryState('change:', "f:@" + _this.options.availabilityField, function (data) { return _this.handleQueryStateChanged(data); });
        _this.bind.onQueryState('change:', "f:@" + _this.options.eligibleForShippingField, function (data) { return _this.handleQueryStateChanged(data); });
        // Initially hide Custom facet
        _this.element.classList.add('hidden');
        return _this;
    }
    AvailabilityFacet_1 = AvailabilityFacet;
    /**
     * Ensures the enableShippingMethod method is defined for later used
     *
     * @private
     */
    AvailabilityFacet.prototype.ensureMethodOptions = function () {
        if (!underscore_1.isFunction(this.options.enableShippingMethod)) {
            this.options.enableShippingMethod = function () { return false; };
        }
    };
    /**
     * Initializes the two sub-facets. They are never added to the DOM.
     *
     * @private
     */
    AvailabilityFacet.prototype.init = function () {
        this.pickUpFacet = new coveo_search_ui_1.DynamicFacet(coveo_search_ui_1.$$('dummy').el, {
            field: this.options.availabilityField,
            enableFacetSearch: false,
            enableMoreLess: false,
            title: this.options.availabilityTitle,
            // numberOfValues: 500,
            // valueCaption: this.getValueCaption()
            includeInBreadcrumb: false,
        }, this.bindings, AvailabilityFacet_1.ID // TODO: check if should have different class ID
        );
        this.shipFacet = new coveo_search_ui_1.DynamicFacet(coveo_search_ui_1.$$('dummy').el, {
            field: this.options.eligibleForShippingField,
            enableFacetSearch: false,
            enableMoreLess: false,
            collapsedByDefault: this.isCollapsed,
            title: this.options.eligibleForShippingField,
            // numberOfValues: 500,
            // valueCaption: this.getValueCaption()
            includeInBreadcrumb: false,
        }, this.bindings, AvailabilityFacet_1.ID);
    };
    AvailabilityFacet.prototype.build = function () {
        this.attachHeader();
        this.buildPickUpFacetValue();
        if (this.options.enableShippingMethod()) {
            this.builShipToFacetValue();
        }
    };
    AvailabilityFacet.prototype.attachHeader = function () {
        var _this = this;
        this.pickUpFacet.ensureDom();
        this.pickUpFacet.header.element.onclick = function () {
            _this.pickUpFacet.header.toggleCollapse(!_this.isCollapsed);
            _this.isCollapsed = !_this.isCollapsed;
            _this.element.classList.toggle('coveo-dynamic-facet-collapsed', _this.isCollapsed);
        };
        this.element.appendChild(this.pickUpFacet.header.element);
    };
    AvailabilityFacet.prototype.builShipToFacetValue = function () {
        var _this = this;
        this.shipCheckbox = new coveo_search_ui_1.Checkbox(function () { return _this.selectShipping(_this.branchInfo); }, "Ship to " + this.branchInfo.zipCode, this.branchInfo.zipCode);
        var button = coveo_search_ui_1.$$('div', { className: 'coveo-dynamic-facet-value shipping-facet-value' });
        button.append(this.shipCheckbox.getElement());
        this.element.appendChild(button.el);
    };
    AvailabilityFacet.prototype.buildPickUpFacetValue = function () {
        var _this = this;
        this.pickupCheckbox = new coveo_search_ui_1.Checkbox(function () { return _this.selectAvailability(_this.branchInfo); }, "In Stock at " + this.branchInfo.branchName + " branch", this.branchInfo.branchName);
        var button = coveo_search_ui_1.$$('div', { className: 'coveo-dynamic-facet-value availability-facet-value' });
        button.append(this.pickupCheckbox.getElement());
        this.element.appendChild(button.el);
    };
    AvailabilityFacet.prototype.resetFacet = function (facet) {
        var _this = this;
        facet.reset();
        facet.triggerNewQuery(function () {
            return _this.logAnalyticsEvent(coveo_search_ui_1.analyticsActionCauseList.breadcrumbFacet, {
                facetField: facet.options.field,
                facetTitle: facet.options.title,
                facetId: facet.options.id,
            });
        });
    };
    AvailabilityFacet.prototype.handlePopulateBreadcrumb = function (data) {
        var _this = this;
        var _a, _b;
        if ((_a = this.pickUpFacet) === null || _a === void 0 ? void 0 : _a.hasActiveValues) {
            var item = {
                element: coveo_search_ui_1.$$('div', { className: 'coveo-dynamic-facet-breadcrumb' }, coveo_search_ui_1.$$('button', { className: 'coveo-dynamic-facet-breadcrumb-value' }, "In Stock at " + this.branchInfo.branchName + " branch", coveo_search_ui_1.$$('span', { className: 'coveo-dynamic-facet-breadcrumb-value-clear' }, SVGIcons_1.SVGIcons.icons.mainClear)).el).el,
            };
            item.element.onclick = function () {
                _this.resetFacet(_this.pickUpFacet);
            };
            data.breadcrumbs.push(item);
        }
        if ((_b = this.shipFacet) === null || _b === void 0 ? void 0 : _b.hasActiveValues) {
            var item = {
                element: coveo_search_ui_1.$$('div', { className: 'coveo-dynamic-facet-breadcrumb' }, coveo_search_ui_1.$$('button', { className: 'coveo-dynamic-facet-breadcrumb-value' }, "Ship to " + this.branchInfo.zipCode, coveo_search_ui_1.$$('span', { className: 'coveo-dynamic-facet-breadcrumb-value-clear' }, SVGIcons_1.SVGIcons.icons.mainClear)).el).el,
            };
            item.element.onclick = function () {
                _this.resetFacet(_this.shipFacet);
            };
            data.breadcrumbs.push(item);
        }
    };
    AvailabilityFacet.prototype.handleBranchLocationRefresh = function (data) {
        this.branchInfo = data;
        if (!data.first) {
            // Do not clear facet selection on first branch location change since it is a page load
            this.ensureFacetValueClear();
            this.element.innerHTML = '';
        }
        if (data) {
            this.build();
        }
    };
    AvailabilityFacet.prototype.ensureFacetValueClear = function () {
        if (!this.pickUpFacet) {
            return;
        }
        if (this.pickUpFacet.values.selectedValues.length > 0) {
            this.pickUpFacet.reset();
            // this.pickUpFacet.triggerNewQuery();
        }
    };
    AvailabilityFacet.prototype.logAnalyticsEvent = function (actionCause, facetMeta) {
        this.usageAnalytics.logSearchEvent(actionCause, facetMeta);
    };
    AvailabilityFacet.prototype.selectShipping = function (data) {
        var _this = this;
        this.shipFacet.toggleSelectValue('Y'); // TODO: parametrize that value
        try {
            this.shipFacet.triggerNewQuery(function () {
                return _this.logAnalyticsEvent(_this.shipFacet.hasSelectedValue('Y') // TODO: parametrize that value
                    ? coveo_search_ui_1.analyticsActionCauseList.dynamicFacetSelect
                    : coveo_search_ui_1.analyticsActionCauseList.dynamicFacetDeselect, {
                    facetField: _this.options.availabilityField,
                    facetTitle: _this.options.availabilityTitle,
                    facetId: _this.options.availabilityField,
                });
            });
        }
        catch (error) {
            this.logger.error('Unable to trigger a query with new Location selected', error);
        }
    };
    AvailabilityFacet.prototype.selectAvailability = function (data) {
        var _this = this;
        if (data === undefined) {
            this.logger.error('Your current branch is invalid');
            return;
        }
        // First make sure all other possible values are deselected
        var isSelected = this.pickUpFacet.hasSelectedValue(data.branchId);
        this.pickUpFacet.reset();
        if (!isSelected) {
            this.pickUpFacet.selectValue(data.branchId);
        }
        try {
            this.pickUpFacet.triggerNewQuery(function () {
                return _this.logAnalyticsEvent(_this.pickUpFacet.hasSelectedValue(data.branchId)
                    ? coveo_search_ui_1.analyticsActionCauseList.dynamicFacetSelect
                    : coveo_search_ui_1.analyticsActionCauseList.dynamicFacetDeselect, {
                    facetField: _this.options.availabilityField,
                    facetTitle: _this.options.availabilityTitle,
                    facetId: _this.options.availabilityField,
                });
            });
        }
        catch (error) {
            this.logger.error('Unable to trigger a query with new Location selected', error);
        }
    };
    AvailabilityFacet.prototype.handleQueryStateChanged = function (data) {
        this.redrawCheckbox();
    };
    AvailabilityFacet.prototype.redrawCheckbox = function () {
        if (!this.pickupCheckbox || !this.pickUpFacet) {
            // this.checkbox is not defined here at the first page load
            return;
        }
        var pickupFacetValueElement = this.element.querySelector('.availability-facet-value');
        if (this.branchInfo && this.branchInfo.branchId && this.pickUpFacet.hasSelectedValue(this.branchInfo.branchId)) {
            this.pickupCheckbox.select(false);
            // TODO: clean that. because there will be multiple checkboxes
            if (pickupFacetValueElement) {
                pickupFacetValueElement.classList.add('coveo-selected');
            }
        }
        else {
            if (pickupFacetValueElement) {
                pickupFacetValueElement.classList.remove('coveo-selected');
            }
        }
        if (!this.shipCheckbox || !this.shipFacet) {
            // this.checkbox is not defined here at the first page load
            return;
        }
        var shipFacetValueElement = this.element.querySelector('.shipping-facet-value');
        if (this.shipFacet.hasSelectedValue('Y')) {
            this.shipCheckbox.select(false);
            if (shipFacetValueElement) {
                shipFacetValueElement.classList.add('coveo-selected');
            }
        }
        else {
            if (shipFacetValueElement) {
                shipFacetValueElement.classList.remove('coveo-selected');
            }
        }
    };
    AvailabilityFacet.prototype.handleBuildingQuery = function (data) {
        if (this.pickUpFacet.hasActiveValues) {
            data.queryBuilder.addContextValue('locationFilter', true);
            data.queryBuilder.advancedExpression.add("@sfccrz__qtyavailable__c>0");
        }
    };
    AvailabilityFacet.prototype.handleQuerySuccess = function (data) {
        this.element.classList.remove('hidden');
        this.redrawCheckbox();
        // Hide the facet if no results
        // const facetResponse = find(data.results.facets, (f) => f.facetId === this.options.availabilityField);
        // if (facetResponse) {
        //   this.element.classList.toggle('hidden', facetResponse.values.length === 0);
        // }
    };
    var AvailabilityFacet_1;
    AvailabilityFacet.ID = 'AvailabilityFacet';
    AvailabilityFacet.options = {
        availabilityField: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@sfccrz__inventorylocationcode__c' }),
        eligibleForShippingField: coveo_search_ui_1.ComponentOptions.buildFieldOption({ defaultValue: '@fp_prd_ship_eligibility' }),
        availabilityTitle: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'Availability' }),
        enableShippingMethod: coveo_search_ui_1.ComponentOptions.buildCustomOption(function () {
            return null;
        }),
    };
    AvailabilityFacet = AvailabilityFacet_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component renders a custom facet to filter items based on the inventory.
         * It is composed of 2 DynamicFacets:
         *  1. The first sub-facet enables the user to filter by product availability.
         *     This facet leverages the catalog to only return items for which the inventory is greater than 0. (for the current branch).
         *  2. The second sub-facet is a simple filter on a Coveo field.
         *     It enables the user to filter products based on their eligibility for shipping.
         *
         * This component should be placed along with the rest of the facets.
         *
         * @export
         * @class AvailabilityFacet
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], AvailabilityFacet);
    return AvailabilityFacet;
}(coveo_search_ui_1.Component));
exports.AvailabilityFacet = AvailabilityFacet;


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BetterSortDropdown = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var SimpleDropdown_1 = __webpack_require__(71);
/**
 * A better looking sort dropdown.
 *
 * @export
 * @class BetterSortDropdown
 * @extends {Component}
 * @implements {IComponentBindings}
 */
var BetterSortDropdown = /** @class */ (function (_super) {
    __extends(BetterSortDropdown, _super);
    /**
     * Creates a new `BetterSortDropdown` component.
     * @param element The HTMLElement on which to instantiate the component.
     * @param options The options for the `BetterSortDropdown` component.
     * @param bindings The bindings that the component requires to function normally. If not set, these will be
     * automatically resolved (with a slower execution time).
     */
    function BetterSortDropdown(element, options, bindings) {
        var _this = _super.call(this, element, BetterSortDropdown_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.sortComponents = [];
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, BetterSortDropdown_1, options);
        _this.removeTabSupport();
        _this.bind.oneRootElement(coveo_search_ui_1.InitializationEvents.afterInitialization, function () { return _this.handleAfterInitialization(); });
        _this.bind.onQueryState('change:', 'sort', function (args) { return _this.handleQueryStateChanged(args); });
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.querySuccess, function (args) { return _this.handleQuerySuccess(args); });
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.queryError, function (args) { return _this.handleQueryError(args); });
        return _this;
    }
    BetterSortDropdown_1 = BetterSortDropdown;
    BetterSortDropdown.prototype.handleAfterInitialization = function () {
        this.element.appendChild(coveo_search_ui_1.$$('span', { className: 'coveops-sort-label' }, 'Sort').el);
        this.buildDropdown();
    };
    BetterSortDropdown.prototype.buildDropdown = function () {
        var _this = this;
        this.sortComponents = this.getEnabledSortComponents();
        this.clearDropdown();
        if (!this.sortComponents.length) {
            return;
        }
        this.dropdown = new SimpleDropdown_1.SimpleDropdown(function () { return _this.handleChange(); }, this.getValuesForDropdown(), function (value) { return _this.getCaptionForValue(value); });
        this.element.appendChild(coveo_search_ui_1.$$('div', { className: 'coveops-dropdown-container' }, this.dropdown.getElement()).el);
        this.update();
    };
    BetterSortDropdown.prototype.clearDropdown = function () {
        var container = this.element.querySelector('.coveops-dropdown-container');
        if (container) {
            this.dropdown && container.removeChild(this.dropdown.getElement());
            this.element.removeChild(container);
        }
        this.dropdown = null;
    };
    BetterSortDropdown.prototype.getValuesForDropdown = function () {
        return this.sortComponents.map(function (sort) { return sort.options.sortCriteria.toString(); });
    };
    BetterSortDropdown.prototype.handleChange = function () {
        var selectedValue = this.dropdown.getValue();
        var sortIndex = this.getSortIndex(selectedValue);
        sortIndex > -1 && this.sortComponents[sortIndex].selectAndExecuteQuery();
    };
    BetterSortDropdown.prototype.getCaptionForValue = function (value) {
        var sortIndex = this.getSortIndex(value);
        return sortIndex > -1 ? this.sortComponents[sortIndex].options.caption : '';
    };
    BetterSortDropdown.prototype.getSortIndex = function (itemValue) {
        return underscore_1.findIndex(this.sortComponents, function (sort) { return sort.options.sortCriteria.toString() === itemValue; });
    };
    BetterSortDropdown.prototype.getEnabledSortComponents = function () {
        var _this = this;
        var sortComponents = coveo_search_ui_1.$$(this.element)
            .findAll("." + coveo_search_ui_1.Component.computeCssClassNameForType('Sort'))
            .map(function (sortElement) {
            var sortCmp = coveo_search_ui_1.Component.get(sortElement, coveo_search_ui_1.Sort);
            if (sortCmp.options.sortCriteria.length === 1) {
                return sortCmp;
            }
            else {
                _this.logger.warn("Each Sort component inside a SortDropdown should have only one sort criteria. Skipping " + sortCmp.options.caption + " in the SortDropdown.");
                return;
            }
        })
            .filter(function (sortCmp) { return sortCmp && !sortCmp.disabled; });
        return sortComponents;
    };
    BetterSortDropdown.prototype.handleQueryStateChanged = function (data) {
        this.update();
    };
    BetterSortDropdown.prototype.update = function () {
        if (!this.dropdown) {
            return;
        }
        var sortCriteria = this.queryStateModel.get('sort');
        this.select(sortCriteria);
    };
    /**
     * Selects a sort criteria from the options.
     * @param sortCriteria The sort criteria to select.
     * @param executeQuery Whether to execute a query after changing the sort criteria
     */
    BetterSortDropdown.prototype.select = function (sortCriteria, executeQuery) {
        if (executeQuery === void 0) { executeQuery = false; }
        var sortIndex = this.getSortIndex(sortCriteria);
        sortIndex > -1 && this.dropdown.select(sortIndex, executeQuery);
        coveo_search_ui_1.$$(this.dropdown.getElement()).toggleClass('coveo-selected', sortIndex > -1);
    };
    BetterSortDropdown.prototype.handleQuerySuccess = function (data) {
        if (!data.results.results.length) {
            return this.hideElement();
        }
        this.buildDropdown();
        if (!this.sortComponents.length) {
            return this.hideElement();
        }
        this.showElement();
    };
    BetterSortDropdown.prototype.handleQueryError = function (data) {
        this.hideElement();
    };
    BetterSortDropdown.prototype.showElement = function () {
        this.element.classList.remove('hidden');
    };
    BetterSortDropdown.prototype.hideElement = function () {
        this.element.classList.add('hidden');
    };
    var BetterSortDropdown_1;
    BetterSortDropdown.ID = 'BetterSortDropdown';
    BetterSortDropdown.options = {};
    BetterSortDropdown = BetterSortDropdown_1 = __decorate([
        turbo_core_1.lazyComponent
    ], BetterSortDropdown);
    return BetterSortDropdown;
}(coveo_search_ui_1.Component));
exports.BetterSortDropdown = BetterSortDropdown;


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.SimpleDropdown = void 0;
var coveo_search_ui_1 = __webpack_require__(0);
/**
 * A Simpledropdown widget with standard styling.
 */
var SimpleDropdown = /** @class */ (function () {
    /**
     * Creates a new `SimpleDropdown`.
     * @param onChange The function to call when the Simpledropdown selected value changes. This function takes the current
     * `SimpleDropdown` instance as an argument.
     * @param listOfValues The selectable values to display in the Simpledropdown.
     * @param getDisplayValue An optional function to modify the display values, rather than using the values as they
     * appear in the `listOfValues`.
     * @param label The label to use for the input for accessibility purposes.
     */
    function SimpleDropdown(onChange, listOfValues, getDisplayValue, label) {
        if (onChange === void 0) { onChange = function (Simpledropdown) { }; }
        if (getDisplayValue === void 0) { getDisplayValue = coveo_search_ui_1.l; }
        this.onChange = onChange;
        this.listOfValues = listOfValues;
        this.getDisplayValue = getDisplayValue;
        this.label = label;
        this.optionsElement = [];
        this.buildContent();
        this.select(0, false);
        this.bindEvents();
    }
    /**
     * Resets the Simpledropdown.
     */
    SimpleDropdown.prototype.reset = function () {
        this.select(0, false);
    };
    SimpleDropdown.prototype.setId = function (id) {
        coveo_search_ui_1.$$(this.element).setAttribute('id', id);
    };
    /**
     * Gets the element on which the Simpledropdown is bound.
     * @returns {HTMLElement} The Simpledropdown element.
     */
    SimpleDropdown.prototype.getElement = function () {
        return this.element;
    };
    /**
     * Gets the currently selected Simpledropdown value.
     * @returns {string} The currently selected Simpledropdown value.
     */
    SimpleDropdown.prototype.getValue = function () {
        return this.selectElement.value;
    };
    /**
     * Selects a value from the Simpledropdown [`listofValues`]{@link SimpleDropdown.listOfValues}.
     * @param index The 0-based index position of the value to select in the `listOfValues`.
     * @param executeOnChange Indicates whether to execute the [`onChange`]{@link SimpleDropdown.onChange} function when this
     * method changes the Simpledropdown selection.
     */
    SimpleDropdown.prototype.select = function (index, executeOnChange) {
        if (executeOnChange === void 0) { executeOnChange = true; }
        this.selectOption(this.optionsElement[index], executeOnChange);
    };
    /**
     * Gets the element on which the Simpledropdown is bound.
     * @returns {HTMLElement} The Simpledropdown element.
     */
    SimpleDropdown.prototype.build = function () {
        return this.element;
    };
    /**
     * Sets the Simpledropdown value.
     * @param value The value to set the Simpledropdown to.
     */
    SimpleDropdown.prototype.setValue = function (value) {
        var _this = this;
        _.each(this.optionsElement, function (option) {
            if (coveo_search_ui_1.$$(option).getAttribute('data-value') === value) {
                _this.selectOption(option);
            }
        });
    };
    SimpleDropdown.prototype.selectOption = function (option, executeOnChange) {
        if (executeOnChange === void 0) { executeOnChange = true; }
        this.selectElement.value = option.value;
        if (executeOnChange) {
            this.onChange(this);
        }
    };
    SimpleDropdown.prototype.buildContent = function () {
        var _this = this;
        this.selectElement = coveo_search_ui_1.$$('select', {
            className: 'coveo-Simpledropdown',
        }).el;
        if (this.label) {
            this.selectElement.setAttribute('aria-label', coveo_search_ui_1.l(this.label));
        }
        var selectOptions = this.buildOptions();
        _.each(selectOptions, function (opt) {
            coveo_search_ui_1.$$(_this.selectElement).append(opt);
        });
        this.element = this.selectElement;
    };
    SimpleDropdown.prototype.buildOptions = function () {
        var _this = this;
        var ret = [];
        _.each(this.listOfValues, function (value) {
            ret.push(_this.buildOption(value));
        });
        return ret;
    };
    SimpleDropdown.prototype.buildOption = function (value) {
        var option = coveo_search_ui_1.$$('option');
        option.setAttribute('data-value', value);
        option.setAttribute('value', value);
        option.text(this.getDisplayValue(value));
        this.optionsElement.push(option.el);
        return option.el;
    };
    SimpleDropdown.prototype.bindEvents = function () {
        var _this = this;
        coveo_search_ui_1.$$(this.selectElement).on('change', function () { return _this.onChange(_this); });
    };
    return SimpleDropdown;
}());
exports.SimpleDropdown = SimpleDropdown;


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomCrossReference = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var CustomCrossReference = /** @class */ (function (_super) {
    __extends(CustomCrossReference, _super);
    function CustomCrossReference(element, options, bindings) {
        var _this = _super.call(this, element, CustomCrossReference_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, CustomCrossReference_1, options);
        _this.bind.onRootElement(coveo_search_ui_1.QueryEvents.buildingQuery, _this.handleBuildingQuery);
        return _this;
    }
    CustomCrossReference_1 = CustomCrossReference;
    /**
     * Builds an encoded customer cross-reference string.
     *
     * @param {string} query
     * @param {*} [separator=this.options.separator]  string to separator customer cross-reference parts (see component documentation)
     * @returns {string}                              encoded customer cross-reference string.
     */
    CustomCrossReference.prototype.buildCrossRef = function (query, separator) {
        if (separator === void 0) { separator = this.options.separator; }
        // Syntax
        // <COMPY>CoveoSEP<CUSTNO>CoveoSEP<CUSTBR>CoveoSEP<CUCIPART#>
        // <COMPY>CoveoSEP<CUSTNO>CoveoSEP<CUSTBR>CoveoSEP<CUDESC>
        return [this.options.companyNumber, this.options.accountNumber, this.options.branch, this.cleanQuery(query)].join(separator);
    };
    /**
     * Removes non-alpha numeric character from a string
     *
     * @private
     * @param {string} query
     * @returns
     */
    CustomCrossReference.prototype.cleanQuery = function (query) {
        return query.replace(/[\W_]+/g, '');
    };
    /**
     * Ensure the the 3 options that defines a customer (company number, account number, customer branch) are defined.
     *
     * @private
     * @returns
     */
    CustomCrossReference.prototype.isValidCustomer = function () {
        return this.options.companyNumber !== '' && this.options.accountNumber !== '' && this.options.branch !== '';
    };
    CustomCrossReference.prototype.handleBuildingQuery = function (data) {
        var query = coveo_search_ui_1.state(this.root, 'q');
        if (query && this.isValidCustomer()) {
            data.queryBuilder.addContextValue('customercross', this.buildCrossRef(query));
        }
    };
    var CustomCrossReference_1;
    CustomCrossReference.ID = 'CustomCrossReference';
    CustomCrossReference.options = {
        separator: coveo_search_ui_1.ComponentOptions.buildStringOption({ defaultValue: 'CoveoSEP' }),
        companyNumber: coveo_search_ui_1.ComponentOptions.buildStringOption(),
        accountNumber: coveo_search_ui_1.ComponentOptions.buildStringOption(),
        branch: coveo_search_ui_1.ComponentOptions.buildStringOption(),
    };
    CustomCrossReference = CustomCrossReference_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component enables CustomerCrossReference searches (sorry for the error in the component name).
         * A customer cross-reference is constructed of 5 items:
         *  1. Company number
         *  2. Customer number
         *  3. Customer branch
         *  4. cross reference (query)
         *  5. Separator
         *
         * The syntax of the encoded cross-reference string is
         * <COMPY>CoveoSEP<CUSTNO>CoveoSEP<CUSTBR>CoveoSEP<CROSS_REF>
         *
         * The encoded string is then used by a dynamic thesaurus rule to surface the appropriate document.
         *
         * @export
         * @class ShippingMethod
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], CustomCrossReference);
    return CustomCrossReference;
}(coveo_search_ui_1.Component));
exports.CustomCrossReference = CustomCrossReference;


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WishList = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var WishList = /** @class */ (function (_super) {
    __extends(WishList, _super);
    function WishList(element, options, bindings, result) {
        var _this = _super.call(this, element, WishList_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.result = result;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, WishList_1, options);
        return _this;
    }
    WishList_1 = WishList;
    var WishList_1;
    WishList.ID = 'WishList';
    WishList.options = {};
    WishList = WishList_1 = __decorate([
        turbo_core_1.lazyComponent
    ], WishList);
    return WishList;
}(coveo_search_ui_1.Component));
exports.WishList = WishList;


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdvancedResultTagging = void 0;
var turbo_core_1 = __webpack_require__(1);
var coveo_search_ui_1 = __webpack_require__(0);
var underscore_1 = __webpack_require__(2);
var AdvancedResultTagging = /** @class */ (function (_super) {
    __extends(AdvancedResultTagging, _super);
    function AdvancedResultTagging(element, options, bindings, result) {
        var _this = _super.call(this, element, AdvancedResultTagging_1.ID, bindings) || this;
        _this.element = element;
        _this.options = options;
        _this.bindings = bindings;
        _this.result = result;
        _this.options = coveo_search_ui_1.ComponentOptions.initComponentOptions(element, AdvancedResultTagging_1, options);
        // This component depends on the CustomCrossReference component
        coveo_search_ui_1.load('CustomCrossReference').then(function (comp) {
            _this.build();
        });
        return _this;
    }
    AdvancedResultTagging_1 = AdvancedResultTagging;
    AdvancedResultTagging.prototype.build = function () {
        var lastQuery = this.getLastQuery();
        if (lastQuery) {
            this.checkForPartNumberMatch(lastQuery) &&
                this.checkForGenericCrossReferenceMatch(lastQuery) &&
                this.checkForCustomerCrossReferenceMatch(lastQuery);
        }
    };
    /**
     * TODO:
     *
     * @private
     * @param {string} term
     * @returns {boolean} returns true if there was a match (full or partial)
     */
    AdvancedResultTagging.prototype.checkForPartNumberMatch = function (term) {
        var partNumberMatch = this.getMatchingValue(this.getPartNumberFields());
        // Search for part number match
        if (partNumberMatch) {
            if (this.isFullMatch(partNumberMatch, term)) {
                this.appendTagToResult('Part Number Match');
            }
            else if (this.isPartialMatch(partNumberMatch, term)) {
                this.appendTagToResult('Partial Part Number Match');
            }
            this.buildMatchHighlight(partNumberMatch, this.getLastQuery());
            return true;
        }
        return false;
    };
    /**
     * Checks for Generic Cross references
     *
     * @private
     * @param {string} term
     * @returns {boolean} returns true if there was a match (full or partial)
     */
    AdvancedResultTagging.prototype.checkForGenericCrossReferenceMatch = function (term) {
        var crossReferenceMatch = this.getMatchingValue(this.getGenericCrossReferenceFields());
        if (crossReferenceMatch) {
            if (this.isFullMatch(crossReferenceMatch, term)) {
                this.appendTagToResult('Cross reference Match');
            }
            else if (this.isPartialMatch(crossReferenceMatch, term)) {
                this.appendTagToResult('Partial Cross Reference Match');
            }
            this.buildMatchHighlight(crossReferenceMatch, this.getLastQuery());
            return true;
        }
        return false;
    };
    /**
     * Checks for Customer Cross references.
     *
     * @private
     * @param {string} term
     * @returns {boolean} returns true if there was a match (full or partial)
     */
    AdvancedResultTagging.prototype.checkForCustomerCrossReferenceMatch = function (term) {
        var customCrossRefElement = this.root.querySelector('.CoveoCustomCrossReference');
        if (customCrossRefElement) {
            // Retrieve CustomeCrossReference JSUI component
            var customCrossRefInstance = coveo_search_ui_1.get(customCrossRefElement, 'CustomCrossReference');
            // Generate the encoded customercross reference term
            var crossRefTerm = customCrossRefInstance.buildCrossRef(term);
            // Get customer cross reference match from the document
            var customerCrossReferenceMatch = this.getMatchingValue(this.getCustomerCrossReferenceFields());
            if (customerCrossReferenceMatch) {
                // A customer cross reference exists in the document metadata
                if (this.isFullMatch(customerCrossReferenceMatch, crossRefTerm)) {
                    this.appendTagToResult('Cross reference Match');
                    // this.buildMatchHighlight(customerCrossReferenceMatch, this.getLastQuery());
                }
                else if (this.isPartialMatch(customerCrossReferenceMatch, crossRefTerm)) {
                    this.appendTagToResult('Partial Cross Reference Match');
                    // this.buildMatchHighlight(customerCrossReferenceMatch, this.getLastQuery());
                }
                this.buildMatchHighlight(term, this.getLastQuery());
                return true;
            }
        }
        return false;
    };
    AdvancedResultTagging.prototype.buildMatchHighlight = function (fullText, textToHighlight) {
        // First make sure the text to highlight is part of the full text
        var fullTextLower = fullText.toLowerCase();
        var textToHighlightLower = textToHighlight.toLowerCase();
        if (fullTextLower.indexOf(textToHighlightLower) === -1 && fullTextLower !== textToHighlightLower) {
            this.logger.error('Text to highlight is not part of the full text', fullTextLower, textToHighlightLower);
            return;
        }
        var matchingStart = fullTextLower.indexOf(textToHighlightLower);
        var matchingEnd = matchingStart + textToHighlightLower.length;
        var highlightElement = coveo_search_ui_1.$$('div', { className: 'ml-10' }, coveo_search_ui_1.$$('span', {}, fullText.slice(0, matchingStart)), coveo_search_ui_1.$$('span', { className: 'coveops-highlight' }, fullText.slice(matchingStart, matchingEnd)), coveo_search_ui_1.$$('span', {}, fullText.slice(matchingEnd)));
        // const match = this.getMatchingValue();
        this.element.appendChild(highlightElement.el);
    };
    AdvancedResultTagging.prototype.appendTagToResult = function (caption) {
        this.element.appendChild(coveo_search_ui_1.$$('span', {}, caption).el);
    };
    AdvancedResultTagging.prototype.getLastQuery = function () {
        return this.queryController.getLastQuery().q || '';
    };
    AdvancedResultTagging.prototype.getCleanedLastQuery = function () {
        return this.getLastQuery().replace(/[^a-z0-9+]+/gi, '');
    };
    /**
     * Checks if there is match between the fieldsToCheck parameter and the last query.
     * There could be 2 possible match types:
     *  1. Full match
     *  2. Partial match
     *
     * @private
     * @param {IFieldOption[]} fieldsToCheck
     * @returns {(string | null)} Returns empty string if no match was found. Otherwise returns the matching string (full or partial match)
     */
    AdvancedResultTagging.prototype.getMatchingValue = function (fieldsToCheck) {
        var _this = this;
        var isFullMatchCondition = function (partNumber) {
            return typeof partNumber === 'string' && partNumber.toLowerCase() === _this.getLastQuery().toLowerCase();
        };
        var isPartialMatchCondition = function (partNumber) {
            return typeof partNumber === 'string' && partNumber.toLowerCase().indexOf(_this.getLastQuery().toLowerCase()) > -1;
        };
        for (var i = 0; i < fieldsToCheck.length; i++) {
            var parts = coveo_search_ui_1.Utils.getFieldValue(this.result, fieldsToCheck[i]);
            if (underscore_1.isArray(parts)) {
                // Handling a multi value field
                for (var j = 0; j < parts.length; j++) {
                    var partNumber = parts[j];
                    if (isFullMatchCondition(partNumber) || isPartialMatchCondition(partNumber)) {
                        return partNumber;
                    }
                }
            }
            if (typeof parts === 'string') {
                // Handling a single value field
                if (isFullMatchCondition(parts) || isPartialMatchCondition(parts)) {
                    return parts;
                }
            }
        }
        return '';
    };
    AdvancedResultTagging.prototype.isFullMatch = function (text1, text2) {
        return text1.toLowerCase() === text2.toLowerCase();
    };
    AdvancedResultTagging.prototype.isPartialMatch = function (text1, text2) {
        return text1.toLowerCase().indexOf(text2.toLowerCase()) > -1;
    };
    AdvancedResultTagging.prototype.getCustomerCrossReferenceFields = function () {
        // TODO: put options
        return ['@fp_prd_dspcustomercrossrefpart']; // The order is important. It define which field will be checked first
    };
    AdvancedResultTagging.prototype.getGenericCrossReferenceFields = function () {
        // TODO: put options
        return ['@fp_prd_dspproductcrossref', '@fp_prd_dspproductcrossref_split']; // The order is important. It define which field will be checked first
    };
    AdvancedResultTagging.prototype.getPartNumberFields = function () {
        // TODO: put options
        return ['@fp_prd_dsppartnumber', '@fp_prd_partnumber_split']; // The order is important. It define which field will be checked first
    };
    var AdvancedResultTagging_1;
    AdvancedResultTagging.ID = 'AdvancedResultTagging';
    AdvancedResultTagging.options = {};
    AdvancedResultTagging = AdvancedResultTagging_1 = __decorate([
        turbo_core_1.lazyComponent
        /**
         * This component renders a tag that specifies if the result is
         *  1. A generic cross reference
         *  2. A customer cross reference
         *  3. A part number search
         *
         * This component is a result template component (see [Result Templates](https://docs.coveo.com/en/413/)).
         *
         * @export
         * @class AdvancedResultTagging
         * @extends {Component}
         * @implements {IComponentBindings}
         */
    ], AdvancedResultTagging);
    return AdvancedResultTagging;
}(coveo_search_ui_1.Component));
exports.AdvancedResultTagging = AdvancedResultTagging;


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(8);
            var content = __webpack_require__(76);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })
/******/ ]);
});
//# sourceMappingURL=CoveoFPProducts.js.map