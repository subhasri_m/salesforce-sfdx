({
	doInitHelper : function(component, event, helper) {
        var action = component.get('c.getCstmPmsns'); 
       
        action.setCallback(this, function(a){
            var state = a.getState(); 
            if(state == 'SUCCESS') {
                debugger;
                var response = a.getReturnValue();
                component.set("v.Critical_C6_Dashboard_BDM",response[0]);
                component.set("v.PBI_PDC_C6Dashboard",response[1]);
                component.set("v.Critical_6_Dashboard",response[2]);
                component.set("v.C6_Dashboard_NAM",response[3]);
                component.set("v.C6_Dashboard_C2",response[4]);
                component.set("v.Adoption_Win_Rates",response[5]);
                component.set("v.Focus_5_Components",response[6]);
                component.set("v.Monthly_Sales_Call_Calendar",response[7]);
                component.set("v.My_Week_Tasks",response[8]);
                component.set("v.Dashbord_Snapshot",response[9]);
                component.set("v.Calender",response[10]);
                component.set("v.My_Task",response[11]);
            }
        });
        $A.enqueueAction(action);
	}
})