({
    doInit : function(component, event, helper) {
        var currConRecId = component.get("v.recordId");
        var action = component.get('c.getContactUserId'); 
        action.setParams({
            "contactId" : currConRecId
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                var userId = a.getReturnValue();
                if(userId != null){
                   // var commUserLoginURLProd = 'https://fleetpride.my.salesforce.com/servlet/servlet.su?oid=00D40000000Mwmn&retURL=%'+currConRecId+'&sunetworkid=0DB1W000000GnLo&sunetworkuserid='+userId;
                    var commUserLoginURLUat = 'https://fleetpride--uat.lightning.force.com/servlet/servlet.su?oid=00D0U0000009nrK&retURL=%'+currConRecId+'&sunetworkid=0DB0U0000005mIe&sunetworkuserid='+userId;
                    var urlEvent = $A.get("e.force:navigateToURL");
                    //alert(commUserLoginURLDev);
                    urlEvent.setParams({
                        "url" : commUserLoginURLDev
                    });
                    urlEvent.fire();
                }
                else{
                    //alert('User Not Enabled'+userId);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Login Failed!",
                        "message": "User Not Enabled.",
                        "type": "warning",
                    });
                    toastEvent.fire();
                }
            }
            
        });
        $A.enqueueAction(action);
        
    }
})       


//Prod Url Below
// "url": 'https://fleetpride.my.salesforce.com/servlet/servlet.su?oid=00D40000000Mwmn&retURL=%252F0031W00002fEudX&sunetworkid=0DB1W000000GnLo&sunetworkuserid=0051W000005PId6'

// Dev URL Below
// "url": 'https://fleetpride--devfullorg.my.salesforce.com/servlet/servlet.su?oid=00D0U0000009nrK&retURL=%252F0030U00000sOZ7d&sunetworkid=0DB0U0000005mIe&sunetworkuserid=0050U000004HVb5'