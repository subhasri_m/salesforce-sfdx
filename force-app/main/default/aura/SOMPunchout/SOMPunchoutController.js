({
    doInit : function(component, event, helper) {
       // window.open('https://fleetpride.my.salesforce.com/servlet/servlet.Integration?lid=00b400000018VMo&eid=0014000000xE8NY&ic=1', '_blank');
      var action = component.get("c.getUIThemeDescription");
        action.setCallback(this, function(a) {
            if(a.getReturnValue()=='Theme3' ||'Theme2'){
                //Lightning Components can be rendered in Salesforce Classic using Lightning Components for Visualforce.
                window.open('https://fleetpride.my.salesforce.com/servlet/servlet.Integration?lid=00b400000018VMo&eid=0014000000xE8NY&ic=1', '_blank');
            }else if(a.getReturnValue()=='Theme4d'){
                //Lightning Components can be rendered natively within Lightning Experience or, alternatively, within a Visualforce Page or within a Lightning Application
				window.open('https://fleetpride.my.salesforce.com/servlet/servlet.Integration?lid=00b400000018VMo&eid=0014000000xE8NY&ic=1', '_blank');
            }else if(a.getReturnValue()=='Theme4t'){
                //Salesforce1 Mobile natively supports Lightning Components.
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": "https://fleetpride.my.salesforce.com/servlet/servlet.Integration?lid=00b400000018VMo&eid=0014000000xE8NY&ic=1"
                });
                urlEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
  /*  Old Code */
        
        //window.open('https://fleetpride.my.salesforce.com/servlet/servlet.Integration?lid=00b400000018VMo&eid=0014000000xE8NY&ic=1', '_blank'); 
       /* var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://fleetpride.my.salesforce.com/servlet/servlet.Integration?lid=00b400000018VMo&eid=0014000000xE8NY&ic=1"
        });
        urlEvent.fire(); */
     
    
    
})