({
	doInit : function(component, event, helper) {
        var action = component.get('c.getContacts'); 
        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set("v.contactList", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    goToContact : function(component, event, helper) {
        var contactRecordId = event.target.id;
        var finalUrl = "/lightning/r/Contact/"+contactRecordId+"/view";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": finalUrl
        });
        urlEvent.fire();
    }
})