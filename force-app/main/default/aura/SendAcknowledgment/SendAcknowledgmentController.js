({
    sendEmail: function(component, event, helper) {
        var recordId = component.get("v.recordId");
        helper.sendHelper(component, recordId);
        },
    handleClick :function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();  
        $A.get('e.force:refreshView').fire();  
    }
})