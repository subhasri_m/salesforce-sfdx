({
    sendHelper : function(component, recordId) {
		var action = component.get("c.processEmail");
               action.setParams({
            'recordId': recordId
          });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.mailStatus", response.getReturnValue());
            }
 
        });
        $A.enqueueAction(action);
	}
})