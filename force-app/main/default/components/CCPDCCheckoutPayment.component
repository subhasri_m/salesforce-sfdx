<apex:component controller="CCPDCCheckoutPaymentController">

<script type="text/javascript">
  CCRZ.uiProperties.CheckoutPaymentView.PaymentProcessor.desktop.tmpl = 'PDC-PaymentProcessor-Desktop';
</script>

<script id="PDC-PaymentProcessor-Desktop" type="text/template">
    <div class="panel cc_panel cc_payment_processor">
        <div class="panel-body cc_body">
            <div class="storedpayment-messagingSection-Error" role="alert" style="display: none"></div>
            <div class="storedpayment-messagingSection-Warning" role="alert" style="display: none"></div>
            <div class="storedpayment-messagingSection-Info" role="alert" style="display: none">
            <button type="button" class="close cc_close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&#215;</span></button>
        </div>
        <div class="panel panel-default cc_panel cc_payment_processor_ponumber">
            <form id="poForm">
            <div class="panel-heading cc_heading">
                <h3 class="panel-title cc_title">{{pageLabelMap 'CC_PDC_Checkout_POTitle'}}</h3>
                <div class='poErrorMessage' style="display: none">Invalid or empty PO Number</div>
            </div>
            <div class="panel-body cc_body">
                <div class="cc_stored_payments_container">
                    <div class="form-group">
                        <div class='row'>
                            <label for="poNumber" class="col-sm-2 control-label poLabel Number fieldLabel cc_po_label_number">{{pageLabelMap 'CC_PDC_Checkout_EnterPO'}}</label>
                            <div class="col-sm-7">
                                <input id="poNumber" type="text" name="poNumber" maxlength="15" minlength="1" class="form-control" value="{{{this.cart.poNumber}}}">
                            </div>
                            <div class="col-sm-3">
                                <input type="button" class="btn btn-default btn-sm button editOrderLines" id="save" value="Edit Line Item PO Numbers" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-2 col-sm-10">
                                {{pageLabelMap '15_Max_Characters'}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
       </div>
    
    </div>
    <div class="paymentMidSection">{{pageLabelMap 'PaymentProcessor_MidSec'}}</div>
    {{#if this.paymentTypes}}
        <div class="cc_payment_types_container">
           {{#ifHasPermission 'isPricingVisible'}}
            <ul class="nav nav-tabs cc_nav-tabs cc_nav-tabs-payment" role="tablist">
            {{#each this.paymentTypes}}
               <li role="presentation" {{#ifEquals @index 1 }} class="active cc_nav-tabs_li" {{/ifEquals}}>
                    <a href="#{{@key}}" aria-controls="{{@key}}" role="tab" class="cc_nav-tabs_anchor" data-toggle="tab">{{pageLabelMapMultiString 'PaymentProcessorTab_' @key}}</a>
                </li>
                                
            {{/each}}
                <li>
                    <a href="#cc_payment_processor_mywallet" aria-controls="{{pageLabelMap 'PaymentProcessor_MyWallet'}}" role="tab" class="cc_nav-tabs_anchor" data-toggle="tab">{{pageLabelMap 'PaymentProcessor_MyWallet'}}</a>
                </li>
            </ul>
             {{/ifHasPermission}}
                                
            <!--***********-->
            <div class="tab-content cc_tab-content">
                {{#each this.paymentTypes}}
                <div role="tabpanel" class="cc_tab-pane tab-pane {{#ifEquals @index 1 }} active {{/ifEquals}} " id="{{@key}}">
                    <div class="paymentTypeContainer {{@key}} err cc_payment_type">
                        {{pageLabelMap 'Payment_LoadingPaymentType'}}
                    </div>
                </div>
                {{/each}}
                <div role="tabpanel" class="cc_tab-pane tab-pane {{#ifEquals @index 1 }} active {{/ifEquals}} " id="cc_payment_processor_mywallet">
                    <div class="paymentTypeContainer cc_payment_processor_mywallet err cc_payment_type">
                        {{#ifDisplay 'WLT.Enabled'}}
                            {{#if this.storedPayments}}
                            <div class="panel panel-default cc_panel">
                                <div class="panel-heading cc_heading">
                                    <h3 class="panel-title cc_title">{{pageLabelMap 'PaymentProcessor_MyWallet'}}</h3>
                                </div>
                                <div class="panel-body cc_body">
                                    <div class="cc_stored_payments_container">
                                        <div class="cc_top_section">{{pageLabelMap 'PaymentProcessor_Top'}}</div>
                                        {{#each this.storedPayments}}
                                        <div class="cc_stored_payments_container {{this.accountType}}">
                                            <div class="radio">
                                                <label for="storedPaymentSelection{{this.sfid}}" class="cc_stored_payments_selector {{this.sfid}}">
                                                <input id="storedPaymentSelection{{this.sfid}}" type="radio" name="storedPaymentSelection" value="{{this.sfid}}" class="storedPaymentSelection {{this.sfid}}" data-id="{{this.sfid}}" {{#ifEquals @root.storedSelected this.sfid}}checked{{/ifEquals}}/>
                                                </label>
                                                <span class="cc_stored_payment_display">
                                                {{pageLabelMapMultiString 'PaymentDisplay_' this.accountType this.accountType this.accountNumber this.subAccountNumber (pageLabelMapMultiString 'PaymentType_' this.paymentType) (pageLabelMapMultiString 'PaymentExpMonth_' this.expirationMonth) (pageLabelMap this.expirationYear) this.displayName}}
                                                </span>
                                            </div>
                                        </div>
                                        {{/each}}
                                        <div class="cc_payment_action_container">
                                            <p class="panel-body pull-right cc_action_buttons">
                                                <button type="button" class="btn btn-default btn-sm useStoredPayment cc_use_stored_payment">{{pageLabelMap 'PaymentProcessor_UseStoredPayment'}}</button>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                           </div>
                          {{/if}}
                        {{/ifDisplay}}
                    </div>
                </div>
            </div>
        </div>
    {{/if}}
    </div>
    <div class="modal fade cc_modal cc_po_modal" id="poModal" tabindex="-1" role="dialog" aria-labelledby="poLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content cc_modal_content">
                <div class="modal-header cc_modal_header">
                    <button type="button" class="close cc_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&#215;</span></button>
                    <h4 class="modal-title cc_modal_title" id="poLabel">{{pageLabelMap 'CC_PDC_Checkout_POLinesTitle'}}</h4>
                </div>
                <div class="modal-body cc_modal_body">
                    <div id="poModalSec" class=""></div>
                </div>
                <div class="modal-footer">
                    <input type="button" class="cancelOrderLines btn-warning" value="{{pageLabelMap 'Cancel'}}"/>
                    <input type="button" class="saveOrderLines" value="{{pageLabelMap 'CC_PDC_Checkout_Apply_PO_Lines'}}"/>
                </div>
            </div>
        </div>
    </div>
</script>

<script id="PDC-PaymentProcessor-POModal" type="text/template">
    <div class="cart_item_list">
    <form id="poLineForm">
    {{#each this.cartItems}}
        <div class="cart_item row">
            {{#ifNotEquals this.cartItemType 'Coupon'}}
                {{#ifGreater this.shippingQuantity 0}}
                    <div class="col-md-5">
                        <p class="cc_item_title">
                            <span class="cc_ext_name">{{displayProductName 'Aggregate_Display' displayProductBean.name mockProduct.name }}</span>
                        </p>
                    </div>
                    <div class="col-md-2">
                        <div class="price cc_price">                    
                            <p class="cc_order_quantity">
                                <span class="cc_quantity_label">Shipping Qty&#58;&#160;</span>
                                <span class="cc_quantity">{{this.shippingQuantity}}</span>
                            </p>                    
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="price cc_price">
                             <p class="cc_order_quantity">
                                <span class="cc_quantity_label">P.O. #&#58;&#160;</span>
                                <input id="poLineNumber{{this.itemID}}" type="text" name="{{this.itemID}}" maxlength="15" minlength="1" class="form-control" value="{{{this.CCFPPONumber}}}">
                            </p>
                        </div>
                     </div>
                 {{/ifGreater}}
            {{/ifNotEquals}}
        </div>
    {{/each}}
    </form>
    </div>

</script>

<script type="text/javascript">

CCRZ.subsc = _.extend(CCRZ.subsc||{});
CCRZ.Checkout = _.extend(CCRZ.Checkout||{},{
    paymentOverride :{
        register : function(index, registrar) {
            CCRZ.views.PDCPaymentProcessorView = CCRZ.views.PaymentProcessorView.extend({
                events: {
                    "click .useStoredPayment" : "useStoredPayment",
                    "click input.storedPaymentSelection" : "handleStoredPaymentSelection",
                    "click .editOrderLines" : "openModal",
                    "click .cancelOrderLines" : "hideModal",
                    "click .saveOrderLines" : "save"
                },
                init: function(options) {
                    this.phoneTemplate     = CCRZ.util.template(options.uiProperties.phone.tmpl);
                    this.desktopTemplate   = CCRZ.util.template(options.uiProperties.desktop.tmpl);
                    this.desktopSelector   = options.uiProperties.desktop.selector;
                    this.phoneSelector     = options.uiProperties.phone.selector;
                    this.paymentData = options.paymentData;
                    this.modalView = new CCRZ.subsc.views.PDCPOModal();
                },
                doRender : function (theTemplate,theSelector){
                    console.log('doRender');
                    this.setElement($(theSelector));
                    var modelData = this.model.toJSON();
                    var cartData = CCRZ.cartCheckoutModel.toJSON();
                    console.log('cartData',cartData);
                    modelData.cart = cartData;
                    console.log(modelData);
                    this.$el.html(theTemplate(modelData));
                    this.oneTimeRender();
                    _.each(this.childPaymentViews,function(cv){
                        cv.render();
                    });

                    $('#poForm').keypress(function(event) {
                    if (event.keyCode == 13) {
                        event.preventDefault();
                    }
                    });                    
                },
                validateInfo : function(formName, fieldData) {
                    var isValid = false;
                    if (CCRZ.subsc.perms.requiresPO) {
                        $("#"+formName).validate({
                            invalidHandler: function(event, validator) {
                                CCRZ.handleValidationErrors(event, validator, 'storedpayment-messagingSection-Error', false);
                            },
                            rules: {
                                poNumber : { required : true }
                            },
                            messages: {
                                poNumber : { required : CCRZ.pagevars.pageLabels['CC_PDC_Checkout_PORequired']}
                            },
                            errorPlacement: function(error, element) {
                            }
                        });
                        return $("#"+formName).valid();
                    }
                    else {
                        isValid = true;
                    }
                    return isValid;
                },
                clearError : function(){
                    $('.storedpayment-messagingSection-Error').hide();
                },
                getPONumber : function(event){
                    var poNumber = null;
                    var formName = 'poForm';
                    var formData = form2js(formName, '.', false, function(node) {}, false);
                    if (this.validateInfo(formName, formData)) {
                        poNumber = formData.poNumber;
                    }
                    return poNumber;
                },
                initializeLinePONumber : function() {
                    var poNumber = null;
                    var formName = 'poForm';
                    var formData = form2js(formName, '.', false, function(node) {}, false);
                    poNumber = formData.poNumber;
                    
                    var storedItem = localStorage.getItem("prevPO");
                    
                    if (poNumber) {
                          
                          localStorage.setItem("prevPO", poNumber);
                        _.each(CCRZ.cartCheckoutModel.attributes.cartItems, function(item) {
                            
                            if (!item.CCFPPONumber) {
                                item.CCFPPONumber = poNumber;
                            }
                            //else if (item.CCFPPONumber && storedItem != poNumber) {
                                
                            // item.CCFPPONumber = poNumber;
                                //}
                        });
                    }
                },
                updateLinePONumber : function() {
                    var formData = form2js('poForm', '.', false, function(node) {}, false);
                    var formLineData = null;
                    if ($('#poLineForm').length) {
                        formLineData = form2js('poLineForm', '.', false, function(node) {}, false);
                    }
                    CCRZ.cartCheckoutModel.attributes.poNumber = formData.poNumber;
                    _.each(CCRZ.cartCheckoutModel.attributes.cartItems, function(item) {
                        if (formLineData && formLineData[item.itemID]) {
                            item.CCFPPONumber = formLineData[item.itemID];
                        }
                        else {
                            item.CCFPPONumber = formData.poNumber;
                        }
                    });
                },
                openModal: function() {
                    this.initializeLinePONumber();
                    this.modalView.render();
                    $('#poModal').modal("show");
                },
                hideModal: function() {
                    $('#poModal').modal("hide");
                },
                save: function() {
                    this.updateLinePONumber();
                    $('#poModal').modal("hide");
                }

 
            });

            CCRZ.views.PDCPaymentView = CCRZ.views.PaymentView.extend({
                init: function() {
                    var v = this;
                    CCRZ.pubSub.on("action:processPayment", function(formData) {
                        v.updateCart(function(response, event) {
                            if (event.status) {
                                if (response && response.success) {
                                    v.model.processPayment(formData);
                                }
                                else {
                                    console.log('CCPDCCheckoutPayment.component %o',response.data.error);
                                    console.log('CCPDCCheckoutPayment.component %o',response.data.stack);
                                    var message = response.data.message;
                                    if (!message) {
                                        message = CCRZ.pagevars.pageLabels['CC_PDC_Checkout_NewOrderError'];
                                    }
                                    CCRZ.pubSub.trigger('pageMessage', CCRZ.createPageMessage('ERROR', 'storedpayment-messagingSection-Error', message)); 

                                    if(message == CCRZ.pagevars.pageLabels['CC_PDC_Checkout_NewOrderError']){
                                        setTimeout(function(){ location.reload(); }, 5000);
                                        
                                    }
                                }
                            }
                            else {
                                document.location = "{!$Page.ccrz__Cart}?cartID=" + CCRZ.pagevars.currentCartID + getCSRQueryString() + '&isTimeout=true';
                            }
                        })
                    });
                },
                loadModel : function(callback){
                    var me = this;
                    this.model.fetch(function(response){

                        if(response && response.data){
                            me.paymentModel = new CCRZ.models.PaymentProcessorModel();
                            if (!CCRZ.subsc.perms.allowCreditCard && response.data.paymentTypes.cc) {
                                delete response.data.paymentTypes.cc;
                                response.data.storedPayments = null;
                            }
                            if (!CCRZ.subsc.perms.allowInvoice && response.data.paymentTypes.io) {
                                delete response.data.paymentTypes.io;
                            }
                            me.paymentModel.set(response.data);
                            //me.paymentModel.attributes.paymentTypes.io = {};
                            //me.paymentModel.attributes.paymentTypes.cc = {};
                            me.paymentView  = new CCRZ.views.PDCPaymentProcessorView({
                             model : me.paymentModel,
                                uiProperties : CCRZ.uiProperties.CheckoutPaymentView.PaymentProcessor,
                                paymentData : {
                                payKey : "pay",
                                 accessmode : "pay"
                                }

                            });
                            if(callback) {
                             callback();
                            }
                        }
                        if(response && response.messages){
                            CCRZ.pubSub.trigger('pageMessage',response);
                        }
                    });
                },
                updateCart : function(callback) {
                    CCRZ.subsc.loadOverlay('#ccloading');
                    var view = this;
                    CCRZ.subsc.paymentView.paymentView.updateLinePONumber()
                    var poNumber = CCRZ.subsc.paymentView.paymentView.getPONumber();
                    //if invalid PO.. 
                    if(CCRZ.subsc.perms.requiresPO && (poNumber == null || poNumber.match(/^[ \t\r\n\s]*$/) )) {                        
                         CCRZ.subsc.unloadOverlay();
                         return;                        
                    }                   
            
                    var cartData = CCRZ.cartCheckoutModel.toJSON();
                    if (cartData) {
                        CCPDCCheckoutPaymentController.updateCart( 
                            CCRZ.pagevars.remoteContext,
                            JSON.stringify(cartData), 
                            function(response, event) {
                                // Check for errors from the inventory check
                                CCRZ.subsc.unloadOverlay();
                                
                                if (callback) {
                                    callback(response, event);
                                }
                            },
                            {timeout : 120000, buffer : false}
                        );
                    }
                }
            });

            CCRZ.subsc.views.PDCPOModal = CCRZ.CloudCrazeView.extend({
                templateBoth : CCRZ.util.template("PDC-PaymentProcessor-POModal"),
                viewName : "PDCPOModal",
                render : function(callback) {
                    var cartData = CCRZ.cartCheckoutModel.toJSON();
                    console.log('cartData',cartData);
                    this.setElement($("#poModalSec")); // where to insert the modal
                    var v = this;
                    v.$el.html(v.templateBoth(cartData));
                },
            });


            CCRZ.pubSub.off("action:processPayment");
            CCRZ.subsc.paymentView = new CCRZ.views.PDCPaymentView({
                model : new CCRZ.models.PaymentModel()
            });

            registrar.registerView(index, CCRZ.pagevars.pageLabels['CC_PDC_CheckOut_PaymentStep'], CCRZ.subsc.paymentView, 'images/payment_review_header.png');
        }
    }

});
</script>

</apex:component>