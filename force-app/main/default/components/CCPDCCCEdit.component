<apex:component controller="CCPDCStoredPaymentController">

<script id="MyAccount-MyWalletCC-Desktop" type="text/template">
    <div class="panel panel-default cc_panel cc_myaccount_mywallet">
        <div class="panel-heading cc_heading">
            <h3 class="panel-title cc_title">{{pageLabelMap 'MyWallet_EditPymtMtd'}}</h3>
        </div>
        <div class="cc_myaccount_content panel-body cc_body">
            <div class="error_messages_section" style="display:none;"></div>
            <form id="editCCForm" class="form-horizontal editCCForm cc_edit_cc_form">
                <h4>{{pageLabelMap 'MyWallet_EditCC'}}</h4>
                <div class="form-group displayName">
                    <label for="displayName" class="col-sm-2 control-label poLabel Name fieldLabel">{{pageLabelMap 'MyWallet_NameOpt'}}</label>
                    <div class="col-sm-10">
                        <input id="displayName" type="text" class="form-control" value="{{this.displayName}}" name="displayName" maxlength="50" />
                    </div>
                </div>
                <div class="form-group accountNumber">
                    <label for="accountNumber" class="col-sm-2 control-label poLabel Number fieldLabel">{{pageLabelMap 'CC_PDC_CC_CreditCardNumber'}}</label>
                    <div class="col-sm-10">
                        <input id="accountNumber" type="text" class="form-control" name="accountNumber" value="{{this.accountNumber}}" maxlength="50" disabled="true" />
                    </div>
                </div>
                <div class="form-group accountNumber">
                    <label for="expDateInput" class="col-sm-2 control-label poLabel Number fieldLabel">{{pageLabelMap 'CC_PDC_CC_Expiration'}}</label>
                    <div class="col-sm-10">
                        <input id="expDateInput" type="text" class="form-control" name="expiration" maxlength="4" placeholder="MMYY" value="{{this.expirationMonth}}{{this.expirationYear}}" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label for="isEnabled" class="poLabel SavePayment fieldLabel">
                                <input type="checkbox" name="isEnabled" id="isEnabled" {{#if this.isEnabled}}checked{{/if}} />{{pageLabelMap 'MyWallet_Enabled'}}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <input type="button" class='btn btn-default btn-sm button cancelCC cc_cancel_cc' id="cancelCC" value="{{pageLabelMap 'MyWallet_Cancel'}}" />
                        <input type="button" class='btn btn-default btn-sm button saveCC cc_save_cc' id="saveCC" value="{{pageLabelMap 'MyWallet_SaveToWall'}}" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</script>

<script>
    var storedPaymentId = "{!JSENCODE(storedPaymentId)}";
    var displayName = "{!JSENCODE(displayName)}";
    var accountNumber = "{!JSENCODE(accountNumber)}";
    var isEnabled = {!isEnabled};
    var expirationMonth = "{!JSENCODE(expirationMonth)}";
    var expirationYear = "{!JSENCODE(expirationYear)}";

    jQuery(function($) {
        CCRZ.models.StoredPaymentsCCModel = CCRZ.CloudCrazeModel.extend({
            updateStoredPayment: function(ccData, callback) {
                var v = this;
                CCPDCStoredPaymentController.updateStoredPayment(
                    CCRZ.pagevars.remoteContext,
                    ccData,
                    function(response) {
                        if(response && response.success ) {
                            callback(response);
                        }
                    }
                );
            }
        });

        CCRZ.views.StoredPaymentsCCView = CCRZ.CloudCrazeView.extend({
            viewName : "StoredPaymentsCCView",
            templateDesktop : CCRZ.util.template('MyAccount-MyWalletCC-Desktop'),
            templatePhone : CCRZ.util.template('MyAccount-MyWalletCC-Desktop'),
            init : function(){
                this.render();
            },
            events: {
                "click #saveCC" : "updateCCNumber",
                "click #cancelCC" : "cancelCCNumber"
            },
            initValidation : function() {
                $('#editCCForm').validate({
                    invalidHandler : function(event, validator) {
                        CCRZ.handleValidationErrors(event, validator, 'error_messages_section', false);
                    },
                    rules : {
                        expiration : {
                            required : true,
                            minlength: 4
                        }
                    },
                    messages : {
                        expiration : CCRZ.pagevars.pageLabels['CC_PDC_CC_ExpirationRequiredError']
                    },
                    errorPlacement : function(error, element) { }
                });
            },
            preRender : function() {
                this.data={
                    'storedPaymentId' : storedPaymentId,
                    'displayName' : displayName,
                    'accountNumber' : accountNumber,
                    'isEnabled' : isEnabled,
                    'expirationMonth' : expirationMonth,
                    'expirationYear' : expirationYear
                };
            },
            renderDesktop : function(){              
                this.setElement($(CCRZ.uiProperties.StoredPaymentsPOView.desktop.selector));
                this.$el.html(this.templateDesktop(this.data));
            },
            renderPhone : function(){
                this.setElement($(CCRZ.uiProperties.StoredPaymentsPOView.phone.selector));
                this.$el.html(this.templatePhone(this.data));
            },
            postRender : function() {
                this.initValidation();
            },
            cancelCCNumber : function(){
                myWallet();
            },
            updateCCNumber : function(event) {
                if ($('#editCCForm').valid()) {
                    var formData = form2js("editCCForm", '.', false, function(node) {}, false);
                    formData.storedPaymentId = this.data.storedPaymentId;
                    formData.isEnabled = formData.isEnabled != null ? true : false;
                    formData.expirationMonth = formData.expiration.substring(0,2);
                    formData.expirationYear = formData.expiration.substring(2);

                    this.model.updateStoredPayment(JSON.stringify(formData), function(response) {
                        if (response.success) {
                            myWallet();
                        }
                        else {
                            var message = response.data.message;
                            if (!message) {
                                message = CCRZ.pagevars.pageLabels['CC_PDC_CC_EditError'];
                            }
                            CCRZ.pubSub.trigger('pageMessage', CCRZ.createPageMessage('ERROR', 'error_messages_section', message)); 
                        }
                    });
                }
            }
        });

        CCRZ.ccView = new CCRZ.views.StoredPaymentsCCView({
            model : new CCRZ.models.StoredPaymentsCCModel
        });
    });
</script>   

</apex:component>