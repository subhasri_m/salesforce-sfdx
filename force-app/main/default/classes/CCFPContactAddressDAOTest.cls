@isTest
public with sharing class CCFPContactAddressDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void getAddressBooksForAccountTest() {
        util.initCallContext();

        ccrz__E_ContactAddr__c item = null;

        Test.startTest();
        List<ccrz__E_AccountAddressBook__c> items = CCFPAddressBookDAO.getAddressBooksForAccount(ccrz.cc_CallContext.currAccountId);
        item = CCFPContactAddressDAO.getAddress(items[0].ccrz__E_ContactAddress__r.Id);
        Test.stopTest();

        System.assert(item != null);
    }

}