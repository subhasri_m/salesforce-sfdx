@isTest
public class CCPDCQuickWishlistControllerTest {
	static CCPDCTestUtil util = new CCPDCTestUtil();
    public static String TEST_JSON_REQUEST_3 = '[{"partNumber":"product-01","quantity":1000}]';
	@testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CC_FP_Contract_Account_Permission_Matrix__c matrix = util.createPermissions(false);
        insert matrix;

        List<ccrz__E_PageLabel__c> labels = new List<ccrz__E_PageLabel__c> {
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_BackOrderModal_Line1', ccrz__Value__c = 'test'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_BackOrderModal_Line2', ccrz__Value__c = 'test'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_WillCall_NoPerm', ccrz__Value__c = 'test'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_BackOrderModal_Wc_Ship', ccrz__Value__c = 'test')
        };
        insert labels;
    }
   
    static testmethod void addBulkTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickWishlistController.addBulk(ctx, CCPDCQuickOrderHelperTest.TEST_JSON_REQUEST_2, false, false,'AVI');
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    } 

    static testmethod void addBulkWithBackOrderTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        Account a = util.getAccount();
        a.OK_to_Backorder__c = 'Yes';
        update a;

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickWishlistController.addBulk(ctx, CCPDCQuickOrderHelperTest.TEST_JSON_REQUEST_2, false, false,'AVI');
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }    

    static testmethod void addBulkIsWillCallFailTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickWishlistController.addBulk(ctx, TEST_JSON_REQUEST_3, true, false,'AVI');
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
    } 

    static testmethod void addBulkIsWillCallTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        update a;

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickWishlistController.addBulk(ctx, TEST_JSON_REQUEST_3, true, false,'AVI');
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }   

    static testmethod void addBulkIsWillCallIsShipremainderTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        a.OK_to_Backorder__c = 'Yes';
        update a;

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickWishlistController.addBulk(ctx, CCPDCQuickOrderHelperTest.TEST_JSON_REQUEST_2, true, true,'AVI');
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void addBulkIsWillCallIsShipremainder2Test() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        a.OK_to_Backorder__c = 'Yes';
        update a;

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickWishlistController.addBulk(ctx, TEST_JSON_REQUEST_3, true, true,'AVI');
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    } 
   
}