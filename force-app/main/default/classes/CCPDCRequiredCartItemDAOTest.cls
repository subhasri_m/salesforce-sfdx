@isTest
public class CCPDCRequiredCartItemDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();

            ccrz__E_Cart__c cart = util.createCartWithSplits(m);
            ccrz__E_Order__c order = util.createOrderWithSplits(m, cart);
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

    }

    static testmethod void getRequiredCartItemsTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c item = util.getCartItem();
        ccrz__E_Product__c product = util.getProduct();
        ccrz__E_Product__c requiredProduct = util.createRequiredProduct(product);

        CC_FP_Required_Cart_Item__c requiredItem = new CC_FP_Required_Cart_Item__c(
            CC_Cart__c = cart.Id,
            CC_Parent_Cart_Item__c = item.Id,
            Quantity__c = 1,
            Price__c = 5.00,
            SubAmount__c = 5.00,
            CC_Product__c = requiredProduct.Id
        );
        insert requiredItem; 

        List<CC_FP_Required_Cart_Item__c> results = null;

        Test.startTest();
        results = CCPDCRequiredCartItemDAO.getRequiredCartItems(new Set<Id> {item.Id});
        Test.stopTest();

        System.assert(results != null);
        System.assertEquals(1, results.size());

    }
}