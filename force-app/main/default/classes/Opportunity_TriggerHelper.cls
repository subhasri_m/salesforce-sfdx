public class Opportunity_TriggerHelper {
    @future
    public static void createChildOppotunities(Map<String, String> mapOppIDNAMgroupID, String NAMoppJSON, String newMapJSON){
        //deserialize the JSON
        Map<Id,Opportunity> OppMap = (Map<Id,Opportunity>)JSON.deserialize(newMapJSON,Map<Id,Opportunity>.class);
        List<Opportunity> NAMopp = (List<Opportunity>)JSON.deserialize(NAMoppJSON,List<Opportunity>.class);
        Map<String, String> mapAccIdOppId = new  Map<String, String>();
        String NAMgroup;
        Map<String,String> mapAccIdOwnerId = new Map<String,String>();
        Map<String,decimal> mapAccIdOppAmount = new Map<String,decimal>();
        List<Opportunity> newOpplst = new List<Opportunity>(); // List of child opps to be inserted
        try{
            for(Opportunity opp : NAMopp ){
                NAMgroup = mapOppIDNAMgroupID.get(opp.id); // getting NAM group number from Opp
                List<Account> acclst  = new List<Account>();
                acclst = CCPDCAccountDAO.getAccountsFromGroup(NAMgroup, opp.AccountId);
                Decimal childOppAmount;
                if (!acclst.IsEmpty()){
                    childOppAmount =  (Opp.Amount).divide(acclst.size(),2,System.RoundingMode.UP); // Validation rule will ensure opp.amnount is greater than 0
                    for(Account acc : acclst){
                        mapAccIdOppId.put(acc.id,opp.id);
                        mapAccIdOwnerId.put(acc.id,acc.ownerid);
                        mapAccIdOppAmount.put(acc.id,childOppAmount);
                    }  
                }
                
            }
            if(!mapAccIdOppId.isEmpty()){
                for(String accid : mapAccIdOppId.keySet()){
                    Opportunity newOpp = OppMap.get(mapAccIdOppId.get(accid)).clone();
                    // Assigning new values as per the requirement
                    newOpp.AccountId = accid;
                    newOpp.StageName = '01-New';
                    newOpp.LeadSource ='FP Intelligence';
                    newOpp.Amount = mapAccIdOppAmount.get(accid); // assigning average amount to child Opp   
                    newOpp.OwnerId = mapAccIdOwnerId.get(accid);   
                    newOpplst.add(newOpp);
                }}
            
            DataBase.Insert(newOpplst); // Inserting child opportunities
        }
        catch(Exception e){
            System.debug('Error Message' + e.getMessage()); 
        }
    }
}