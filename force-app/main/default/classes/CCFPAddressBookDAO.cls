public without sharing class CCFPAddressBookDAO {
    public static List<ccrz__E_AccountAddressBook__c> getAddressBooksForAccount(Id accountId) {
        List<ccrz__E_AccountAddressBook__c> addressBooks = [
            SELECT 
                Id, 
                Name,
                ccrz__Account__c,
                ccrz__AddressBookId__c,
                ccrz__AddressType__c,
                ccrz__E_ContactAddress__c,
                ccrz__E_ContactAddress__r.Id,
                ccrz__E_ContactAddress__r.Name,
                ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c,
                ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c,
                ccrz__E_ContactAddress__r.ccrz__AddressThirdline__c,
                ccrz__E_ContactAddress__r.ccrz__AddrReadOnly__c,
                ccrz__E_ContactAddress__r.ccrz__City__c,
                ccrz__E_ContactAddress__r.ccrz__CompanyName__c,
                ccrz__E_ContactAddress__r.ccrz__ContactAddrId__c,
                ccrz__E_ContactAddress__r.ccrz__Country__c,
                ccrz__E_ContactAddress__r.ccrz__CountryISOCode__c,
                ccrz__E_ContactAddress__r.CC_FP_County__c,
                ccrz__E_ContactAddress__r.ccrz__DaytimePhone__c,
                ccrz__E_ContactAddress__r.ccrz__Email__c,
                ccrz__E_ContactAddress__r.ccrz__FirstName__c,
                ccrz__E_ContactAddress__r.CC_FP_Has_Lift_Gate__c,
                ccrz__E_ContactAddress__r.ccrz__HomePhone__c,
                ccrz__E_ContactAddress__r.ccrz__LastName__c,
                ccrz__E_ContactAddress__r.CC_FP_Location__c,
                ccrz__E_ContactAddress__r.ccrz__MailStop__c,
                ccrz__E_ContactAddress__r.ccrz__MiddleName__c,
                ccrz__E_ContactAddress__r.ccrz__Partner_Id__c,
                ccrz__E_ContactAddress__r.ccrz__PostalCode__c,
                ccrz__E_ContactAddress__r.ccrz__ShippingComments__c,
                ccrz__E_ContactAddress__r.ccrz__State__c,
                ccrz__E_ContactAddress__r.ccrz__StateISOCode__c,
                ccrz__E_ContactAddress__r.CC_FP_Type__c,
                ccrz__Default__c
            FROM 
                ccrz__E_AccountAddressBook__c
            WHERE
                ccrz__Account__c = :accountId AND CC_FP_isActive__c = true
        ];
        return addressBooks;
    }

    public static List<ccrz__E_AccountAddressBook__c> getAddressBooksForAccountShipping(Id accountId) {
        List<ccrz__E_AccountAddressBook__c> addressBooks = [
            SELECT 
                Id, 
                Name,
                ccrz__Account__c,
                ccrz__AddressBookId__c,
                ccrz__AddressType__c,
                ccrz__E_ContactAddress__c,
                ccrz__E_ContactAddress__r.Id,
                ccrz__E_ContactAddress__r.Name,
                ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c,
                ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c,
                ccrz__E_ContactAddress__r.ccrz__AddressThirdline__c,
                ccrz__E_ContactAddress__r.ccrz__AddrReadOnly__c,
                ccrz__E_ContactAddress__r.ccrz__City__c,
                ccrz__E_ContactAddress__r.ccrz__CompanyName__c,
                ccrz__E_ContactAddress__r.ccrz__ContactAddrId__c,
                ccrz__E_ContactAddress__r.ccrz__Country__c,
                ccrz__E_ContactAddress__r.ccrz__CountryISOCode__c,
                ccrz__E_ContactAddress__r.CC_FP_County__c,
                ccrz__E_ContactAddress__r.ccrz__DaytimePhone__c,
                ccrz__E_ContactAddress__r.ccrz__Email__c,
                ccrz__E_ContactAddress__r.ccrz__FirstName__c,
                ccrz__E_ContactAddress__r.CC_FP_Has_Lift_Gate__c,
                ccrz__E_ContactAddress__r.ccrz__HomePhone__c,
                ccrz__E_ContactAddress__r.ccrz__LastName__c,
                ccrz__E_ContactAddress__r.CC_FP_Location__c,
                ccrz__E_ContactAddress__r.ccrz__MailStop__c,
                ccrz__E_ContactAddress__r.ccrz__MiddleName__c,
                ccrz__E_ContactAddress__r.ccrz__Partner_Id__c,
                ccrz__E_ContactAddress__r.ccrz__PostalCode__c,
                ccrz__E_ContactAddress__r.ccrz__ShippingComments__c,
                ccrz__E_ContactAddress__r.ccrz__State__c,
                ccrz__E_ContactAddress__r.ccrz__StateISOCode__c,
                ccrz__E_ContactAddress__r.CC_FP_Type__c,
                ccrz__Default__c
            FROM 
                ccrz__E_AccountAddressBook__c
            WHERE
                ccrz__Account__c = :accountId AND ccrz__AddressType__c = 'Shipping' AND CC_FP_isActive__c = true
        ];
        return addressBooks;
    }

}