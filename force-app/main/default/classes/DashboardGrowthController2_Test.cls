@isTest(seealldata=false)
public class DashboardGrowthController2_Test{

    //Setup Data
     @testSetup static void setupTestData(){
         Profile saProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        
        //Create User Data
         User saUser = new User(Alias = 'newUser', Email='test123@email.com', 
                      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                       LocaleSidKey='en_US', ProfileId = saProfile.Id, 
                       TimeZoneSidKey='America/Los_Angeles', UserName='test123123@email.com',
                      Create_Accounts__c = true, Salesman_Number__c = '1-1234', Region__c= 'Central');   
        insert saUser;
        
         User osrUser = new User(Alias = 'newUser', Email='test1234@email.com', 
                      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                      LocaleSidKey='en_US', ProfileId = saProfile.Id, 
                      TimeZoneSidKey='America/Los_Angeles', UserName='test12341234@email.com',
                      Create_Accounts__c = true, Salesman_Number__c = '1-1234', Region__c= 'Central', Territory__c = 'C1');
                       
         insert osrUser;
        
         Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = saUser.Id);  
         insert acc;
        
         Sales_Target__c st = new Sales_Target__c();
         st.Start_Date__c = date.newInstance(2016, 11, 1);
        st.Sales_Plan_Amount__c = 100000;
         st.GP_MTD_Target_Pct__c = 100;
         st.Sales_MTD_Target_Pct__c = 100;
        st.GP_Target_Amount__c = 100000;
         st.Gross_Profit_Sum__c = 100000;
        st.Sales_Sum__c = 100000;
        st.GP_QTD__c=1000;
         st.GP_YTD__c=10000;
         st.QTD_Sales__c=1000;
         st.YTD_Sales__c=100000;
         st.Growth_Percentage__c = 4;
         st.Goal_Percentage__c = 4;
         st.OwnerId = osrUser.Id;
        
    //     Sales_Order_Summary__c sos = new Sales_Order_Summary__c();
    //     sos.Sales_Target__c = st.Id;
    //     sos.Account__c = acc.Id;
    //     sos.Sales__c = 0;
    //     insert sos;
        
         insert st;
        
    //     Sales_Order_Summary_Details__c sosd = new Sales_Order_Summary_Details__c();
    //     sosd.Cost_of_Goods_Sold__c = 1000;
    //     sosd.Cost_of_Goods_Sold_ICost__c = 1000;
    //     sosd.Sales__c = 1000;
    //     sosd.Sales_Order_Summary__c = sos.Id;
        
    //     insert sosd;

        
     }
    
    public static TestMethod void viewAsOsr(){
        UserRole osrRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'Central_Sales_Rep_C1' LIMIT 1];
        
        User osrUser = [SELECT Id FROM User WHERE isActive = true AND UserRoleId = :osrRole.Id LIMIT 1];
        
        System.runAs(osrUser){
            //Setup PageReference
            PageReference pageRef = Page.DashboardGrowth2;
            Test.setCurrentPage(pageRef);
            DashboardGrowthController2  dmc = new DashboardGrowthController2();
        }
        

    }
    
    public static TestMethod void viewAsTSM(){
        UserRole tsmRole = [SELECT Id, Name FROM UserRole WHERE DeveloperName = 'SE1_Manager' LIMIT 1];
        
        User tsmUser = [SELECT Id FROM User WHERE isActive = true AND UserRoleId = :tsmRole.Id LIMIT 1];
        
        System.runAs(tsmUser){
            //Setup PageReference
            PageReference pageRef = Page.DashboardGrowth2;
            Test.setCurrentPage(pageRef);
            DashboardGrowthController2  dmc = new DashboardGrowthController2();
        }
        

    }
    
    
    public static TestMethod void viewAsSD(){
        UserRole sdRole = [SELECT Id, Name FROM UserRole WHERE DeveloperName = 'West_Sales_Executive' LIMIT 1];
        
        User sdUser = [SELECT Id FROM User WHERE isActive = true AND UserRoleId = :sdRole.Id LIMIT 1];
        
        System.runAs(sdUser){
            //Setup PageReference
            PageReference pageRef = Page.DashboardGrowth2;
            Test.setCurrentPage(pageRef);
            DashboardGrowthController2  dmc = new DashboardGrowthController2();
        }
        

    }
    
    
    public static TestMethod void viewAsSystemAdmin(){
            //Setup PageReference
            PageReference pageRef = Page.DashboardGrowth2;
            Test.setCurrentPage(pageRef);
            DashboardGrowthController2  dmc = new DashboardGrowthController2();

    }

}