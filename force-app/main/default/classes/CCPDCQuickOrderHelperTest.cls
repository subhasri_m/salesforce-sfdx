@isTest
public class CCPDCQuickOrderHelperTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    public static String TEST_REQUEST = 'product-01,10\nproduct-na,20\n';
    public static String TEST_JSON_REQUEST = '[{"partNumber":"product-01","quantity":10},{"partNumber":"product-na","quantity":20}]';
    public static String TEST_JSON_REQUEST_2 = '[{"partNumber":"product-01","quantity":10}]';

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void getQuickOrderItemsTest() {
        util.initCallContext();
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c,DSP_Part_Number__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        product.DSP_Part_Number__c = 'product-01';
        update product;
        List<CCPDCQuickOrderHelper.QuickOrderItem> items = new List<CCPDCQuickOrderHelper.QuickOrderItem>();
        CCPDCQuickOrderHelper.QuickOrderItem item = new CCPDCQuickOrderHelper.QuickOrderItem();
        item.partNumber = 'product-01';
        item.quantity = 10;
        items.add(item);
        item = new CCPDCQuickOrderHelper.QuickOrderItem();
        item.partNumber = 'product-na';
        item.quantity = 20;
        items.add(item);

        List<CCPDCQuickOrderHelper.QuickOrderItem> data = null;
        Test.startTest();
        data = CCPDCQuickOrderHelper.getQuickOrderItems(items);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(2, data.size());
    }

    static testmethod void getQuickOrderItemsConflictTest() {
        util.initCallContext();
        ccrz__E_Product__c prod1 = util.createProduct('DupPart', 'dup-part-1', 'Standard', util.STOREFRONT);
        prod1.DSP_Part_Number__c = 'DupPart';
        insert prod1;
        ccrz__E_Product__c prod2 = util.createProduct('DupPart', 'dup-part-2', 'Standard', util.STOREFRONT);
        prod2.DSP_Part_Number__c = 'DupPart';
        insert prod2;

        List<CCPDCQuickOrderHelper.QuickOrderItem> items = new List<CCPDCQuickOrderHelper.QuickOrderItem>();
        CCPDCQuickOrderHelper.QuickOrderItem item = new CCPDCQuickOrderHelper.QuickOrderItem();
        item.partNumber = 'product-01';
        item.quantity = 10;
        items.add(item);
        item = new CCPDCQuickOrderHelper.QuickOrderItem();
        item.partNumber = 'DupPart';
        item.quantity = 20;
        items.add(item);

        List<CCPDCQuickOrderHelper.QuickOrderItem> data = null;
        Test.startTest();
        data = CCPDCQuickOrderHelper.getQuickOrderItems(items);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(2, data.size());
    }

    static testmethod void validateTest() {
        util.initCallContext();
        List<CCPDCQuickOrderHelper.QuickOrderItem> items = new List<CCPDCQuickOrderHelper.QuickOrderItem>();
        CCPDCQuickOrderHelper.QuickOrderItem item = new CCPDCQuickOrderHelper.QuickOrderItem();
        item.sku = 'product-01';
        item.partNumber = 'product-01';
        item.quantity = 10;
        item.salesPackqty = 2;
        items.add(item);
        item = new CCPDCQuickOrderHelper.QuickOrderItem();
        item.partNumber = 'product-na';
        item.quantity = 20;
        item.salesPackqty = 3;
        items.add(item);

        Boolean data = null;
        Test.startTest();
        data = CCPDCQuickOrderHelper.validate(items);
        Test.stopTest();

        System.assert(data != null);
        System.assert(!data);
    }

    static testmethod void addToCartTest() {
        util.initCallContext();
        List<CCPDCQuickOrderHelper.QuickOrderItem> items = new List<CCPDCQuickOrderHelper.QuickOrderItem>();
        CCPDCQuickOrderHelper.QuickOrderItem item = new CCPDCQuickOrderHelper.QuickOrderItem();
        item.sku = 'product-01';
        item.partNumber = 'product-01';
        item.quantity = 10;
        items.add(item);

        String data = null;
        Test.startTest();
        data = CCPDCQuickOrderHelper.addToCart(items);
        Test.stopTest();

        System.assert(data != null);
    }
    
    static testmethod void parseQuickOrderItemTest() {
        util.initCallContext();

        List<CCPDCQuickOrderHelper.QuickOrderItem> data = null;
        Test.startTest();
        data = CCPDCQuickOrderHelper.parseQuickOrderItem(TEST_JSON_REQUEST);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(2, data.size());
    }

    static testmethod void parseQuickOrderRequestTest() {
        util.initCallContext();

        List<CCPDCQuickOrderHelper.QuickOrderItem> data = null;
        Test.startTest();
        data = CCPDCQuickOrderHelper.parseQuickOrderRequest(TEST_REQUEST);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(2, data.size());
    }

    static testmethod void createQuickOrderRequestTest() {
        util.initCallContext();
        List<CCPDCQuickOrderHelper.QuickOrderItem> items = new List<CCPDCQuickOrderHelper.QuickOrderItem>();
        CCPDCQuickOrderHelper.QuickOrderItem item = new CCPDCQuickOrderHelper.QuickOrderItem();
        item.partNumber = 'product-01';
        item.quantity = 10;
        items.add(item);
        item = new CCPDCQuickOrderHelper.QuickOrderItem();
        item.partNumber = 'product-na';
        item.quantity = 20;
        items.add(item);

        String data = null;
        Test.startTest();
        data = CCPDCQuickOrderHelper.createQuickOrderRequest(items);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(TEST_REQUEST, data);
    }

}