@isTest
public class BatchSalesTarget_Test {


    @testSetup static void setup() { 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'newUser', Email='newuser1234@sampleeeee.commm', 
                      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                      LocaleSidKey='en_US', ProfileId = p.Id, 
                      TimeZoneSidKey='America/Los_Angeles', UserName='exampletest@batch.so',
                      Create_Accounts__c = true, salesman_number__c = '1-6665', Growth_Percentage__c = 4);   
        insert u;
        
        Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = u.Id);  
        insert acc;
            
        Sales_Target__c st = new Sales_Target__c();
        st.Start_Date__c = Date.Today();
        st.OwnerId = u.Id;
        insert st;
        
        Sales_Order_Summary__c sos = new Sales_Order_Summary__c();
        sos.Sales_Target__c = st.Id;
        sos.Account__c = acc.Id;
        sos.Sales__c = 0;
        sos.Order_Date__c = Date.Today();
        sos.salesman_number__c = u.salesman_number__c;
        insert sos;
        
        Sales_Order_Summary_Details__c sosd = new Sales_Order_Summary_Details__c();
        sosd.Cost_of_Goods_Sold__c = 1000;
        sosd.Cost_of_Goods_Sold_ICost__c = 1000;
        sosd.Sales__c = 1000;
        sosd.Sales_Order_Summary__c = sos.Id;
        
        insert sosd;
    }
    

    
    static testMethod void BatchSalesTargetTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Target__c> stList = new List<Sales_Target__c>();
        
        stList = [SELECT Id, salesman_number__c,Start_Date__c FROM Sales_Target__c];
        List<Sales_Order_Summary__c> sosList = [SELECT Id, salesman_number__c,SalesmanDate__c FROM Sales_Order_Summary__c ];
        
        Test.startTest();

            BatchSalesTarget batchApex = new BatchSalesTarget();
            Database.QueryLocator ql = batchApex.start(null);
            batchApex.execute(null, sosList);
            batchApex.Finish(null);
            
        
        Test.stopTest();
      }
  }

}