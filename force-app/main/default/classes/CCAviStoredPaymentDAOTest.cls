@isTest
public class CCAviStoredPaymentDAOTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void getStoredPaymentTest() {
        util.initCallContext();

        List<ccrz__E_StoredPayment__c> data = new List<ccrz__E_StoredPayment__c> {
            createStorePayment('100', ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currUserId, ccrz.cc_CallContext.storefront)
        };
        insert data;

        ccrz__E_StoredPayment__c payment = null;
        Test.startTest();
        payment = CCAviStoredPaymentDAO.getStoredPayment(data[0].Id);
        Test.stopTest();

        System.assert(payment != null);
        System.assertEquals(data[0].Id, payment.Id);
    }

    public static ccrz__E_StoredPayment__c createStorePayment(String accountNumber, Id accountId, Id userId, String storefront) {
        ccrz__E_StoredPayment__c payment = new ccrz__E_StoredPayment__c(
            ccrz__Account__c = accountId, 
            ccrz__AccountAccessible__c  = false,
            ccrz__AccountNumber__c = accountNumber,
            ccrz__AccountType__c = 'cc',
            ccrz__Default__c = false, 
            ccrz__DisplayName__c = accountNumber,
            ccrz__Enabled__c = true, 
            ccrz__ExpMonth__c = 4,
            ccrz__ExpYear__c = 25,
            ccrz__PaymentType__c = 'VISA', 
            ccrz__ReadOnly__c = false,  
            ccrz__StartDate__c = Date.today(),
            ccrz__StoredPaymentId__c  = accountNumber,
            ccrz__Storefront__c = storefront,
            ccrz__Token__c = '1234567890',
            ccrz__User__c = userId 
        );
        return payment;
    }
}