@isTest
public class CCPDCLogicCartAddToTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }

        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.SERVICE_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccServiceCart' => 'c.CCPDCServiceCart'
                }
            },
            ccrz.ccApiTestData.LOGIC_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccLogicCartAddTo' => 'c.CCPDCLogicCartAddTo',
                    'ccLogicProductPricing' => 'c.CCPDCLogicProductPricing'
                }
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = util.STOREFRONT;
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
    }

    static testmethod void processTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>B99</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        List<ccrz.ccApiCart.LineData> newLines = new List<ccrz.ccApiCart.LineData>();
        ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
        newLine.sku = product.ccrz__SKU__c;
        newLine.quantity = 1;
        newLine.price = 30;
        newLines.add(newLine);

        String cartId = CCAviCartManager.getActiveCartId();
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 6, 
            ccrz.ccApiCart.CART_ENCID => cartId,
            ccrz.ccApiCart.LINE_DATA => newLines,
            ccrz.ccApiCart.ISSKIPPRICING =>true    
        };
  
        Map<String,Object> response = null;
        /* To cover addMajorLineItems Method */
        CCPDCLogicCartAddTo ccCart = new CCPDCLogicCartAddTo();
        Test.startTest();
        response = CCPDCCartManager.addTo(request);//;(request);
        try{
            ccCart.addMajorLineItems(request);
        }catch(System.Exception e){}
        Test.stopTest();
        System.assert(response != null);
        Boolean isSuccess = (Boolean) response.get(ccrz.ccAPI.SUCCESS);
        System.assert(isSuccess);

    }

}