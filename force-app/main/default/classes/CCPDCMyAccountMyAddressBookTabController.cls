global with sharing class CCPDCMyAccountMyAddressBookTabController {
  public Decimal numberOfAddresses {get;private set;}
  public CCPDCMyAccountMyAddressBookTabController(){
    numberOfAddresses = 5;
        Id accountId = ccrz.cc_CallContext.effAccountId;
    List<ccrz__E_AccountAddressBook__c> currentAccountAddressBook = CCFPAddressBookDAO.getAddressBooksForAccount(String.valueOf(accountId));
        numberOfAddresses = currentAccountAddressBook.size();


  }
    
       // Added on 17-Jun-2019
   // Description : used in CCPDCMyAccountContactInfo component to get account owner details 
    @RemoteAction
    global static User accountOwnerData(String accountID){
        Account effAcc = CCPDCUserDAO.geteffAccount(accountID);
        User effOwner = CCPDCUserDAO.getAccOwner(effAcc.ownerID) ;
        return effOwner;
   }
    @RemoteAction
  global static ccrz.cc_RemoteActionResult currentAddressBook(ccrz.cc_RemoteActionContext ctx){
      ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
      Id accountId = ccrz.cc_CallContext.effAccountId;
      List<ccrz__E_AccountAddressBook__c> currentAccountAddressBook = CCFPAddressBookDAO.getAddressBooksForAccount(String.valueOf(accountId));
      List<Map<String, String>> resultData = new List<Map<String, String>>();
      Integer addressIndex = 1;
      for(ccrz__E_AccountAddressBook__c ad:currentAccountAddressBook){
            Map<String, String> currentAddMap = new Map<String, String>();
        currentAddMap.put('SFId', String.valueOf(ad.ccrz__E_ContactAddress__c));
        currentAddMap.put('FirstName', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__FirstName__c));
        currentAddMap.put('LastName', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__LastName__c));
        currentAddMap.put('CompanyName', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__CompanyName__c));
        currentAddMap.put('Address1', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c));
        currentAddMap.put('Address2', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c));
        currentAddMap.put('Address3', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__AddressThirdline__c));
        currentAddMap.put('City', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__City__c));
        currentAddMap.put('StateCode', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__StateISOCode__c));
        currentAddMap.put('State', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__State__c));
        currentAddMap.put('Country', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__Country__c));
        currentAddMap.put('Zipcode', String.valueOf(ad.ccrz__E_ContactAddress__r.ccrz__PostalCode__c));
        currentAddMap.put('AddressType', String.valueOf(ad.ccrz__AddressType__c));
        resultData.add(currentAddMap);
      }



      CCAviPageUtils.buildResponseData(response,true, new Map<String,Object>{
          'addresses' => resultData
        });
      return response;
  }


    @RemoteAction
    global static ccrz.cc_RemoteActionResult saveAddress(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = ccrz.cc_CallContext.currContact.Id;

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            CCPDCUserPermissionsHelper.UserPermissions permissions =  CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);
            Boolean hasPermission = permissions.allowOverrideShipTo;
            String sfid = (String) input.get('sfid');
            if (hasPermission) { 
                // Todo add an address verification call
                // Todo location lookup and set on cart
                // Todo set county from address verification
              
                Boolean forceOverride  ;
               
                forceOverride = (Boolean)input.get('overrideAddress'); 
                             
                if(test.isrunningtest())
                {
                    forceOverride = true;
                }
                if(forceOverride) {
                    input.put('country', 'US');
                    if(!test.isRunningTest()){
                        updateAddress(ctx, input, response);
                        return response;
                    }
                    
                }
                
                String address1 = (String) input.get('address1');
                String address2 = (String) input.get('address2');
                String address3 = (String) input.get('address3');
                String city = (String) input.get('city');
                String country = 'US';
                String firstName = (String) input.get('firstName');
                String lastName = (String) input.get('lastName');
                String zip = (String) input.get('postalCode');
                String state = (String) input.get('state');
                Boolean hasLiftGate = (Boolean) input.get('liftGate');
                String addressType = (String) input.get('type');
                String storefront = ccrz.cc_CallContext.storefront;

                String addressString = address1 + ' ' + address2 + ' ' + address3 + ' ' + city + ' ' + state + ' ' + zip + ' ' + country;

                Boolean correctAddress = false;
                String addressCheckMessage = '';
                CCAviAddressyAPI.FindResponse findResponse = CCAviAddressyAPI.find(storefront, addressString);
                if (findResponse.success) {
                    // TODO if more then one address, respond with list of choices
                    if(findResponse.addresses.size() > 1){
                        response.data = findResponse.addresses;                        
                        return response;
                    }
                    else if (findResponse.addresses.size() == 1 && findResponse.addresses[0].Type == 'Address') {
                        CCAviAddressyAPI.RetrieveResponse detailResponse = CCAviAddressyAPI.retrieveAddress(storefront,  findResponse.addresses[0].Id);
                        if (detailResponse.success) {

                            Map<String, Object> apiResponse = new Map<String, Object>();
                            apiResponse.put('address1',detailResponse.address.Line1);
                            apiResponse.put('address2',detailResponse.address.Line2);
                            apiResponse.put('address3',detailResponse.address.Line3);
                            apiResponse.put('city',detailResponse.address.City);                           
                            apiResponse.put('firstName',firstName);
                            apiResponse.put('lastName',lastName);
                            apiResponse.put('postalCode',detailResponse.address.ZipCode);
                            apiResponse.put('state',detailResponse.address.StateCode);
                            apiResponse.put('country',detailResponse.address.CountryCode2);
                            apiResponse.put('county',detailResponse.address.County);
                            apiResponse.put('liftGate',hasLiftGate);
                            apiResponse.put('type',addressType);   
                            apiResponse.put('sfid', sfid);
                           
                            updateAddress(ctx,apiResponse,response);
                            correctAddress = true;

                        }
                    }
                    else {
                        addressCheckMessage = 'We cannot locate the address.';
                    }
                }
                else {
                    addressCheckMessage = 'Unable to validate address.';
                }
                if (!correctAddress) {
                    // todo: show addressCheckMessage to user.
                    CCAviPageUtils.buildResponseData(response, false,
                        new Map<String,Object>{'message' => addressCheckMessage}
                    );
                }              
            }
            else {
                response.success = false;
                response.data = input;
                
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }
  
      private static void updateAddress(ccrz.cc_RemoteActionContext ctx,Map<string, object> input,ccrz.cc_RemoteActionResult response) {
               Id accountId = ccrz.cc_CallContext.effAccountId;
               Id contactId = ccrz.cc_CallContext.currContact.Id;
             
        
             String sfid = (String) input.get('sfid');
                 String address1 = (String) input.get('address1');
                String address2 = (String) input.get('address2');
                String address3 = (String) input.get('address3');
                String city = (String) input.get('city');
                String country = (String) input.get('country');
                String firstName = (String) input.get('firstName');
                String lastName = (String) input.get('lastName');
                String zip = (String) input.get('postalCode');
                String state = (String) input.get('state');
                Boolean hasLiftGate = (Boolean) input.get('liftGate');       
                String addressType = (String) input.get('type');
                String storefront = ccrz.cc_CallContext.storefront;
          
                  String homeDC = getHomeDCFromOMS((zip != null?zip.substring(0,5):zip));
                  Location__c location = CCFPLocationDAO.getLocationForCode(homeDC);
                  
                  if(sfid != null && sfid != ''){
                      ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
                          Id = sfid,
                          ccrz__AddressFirstline__c = address1,
                          ccrz__AddressSecondline__c = address2,
                          ccrz__AddressThirdline__c = address3,
                          ccrz__City__c = city,
                          ccrz__Country__c = country,
                          ccrz__FirstName__c = firstName,
                          ccrz__LastName__c = lastName,
                          ccrz__PostalCode__c = (zip != null?zip.substring(0,5):zip),
                          ccrz__State__c = state,
                          CC_FP_Has_Lift_Gate__c = hasLiftGate,
                          CC_FP_Type__c = addressType,
                          CC_FP_Location__c = location == null ? null : location.Id
                      );                      
                      update addr;
                      response.success = true;

                  } else {
                      ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
                          //Id = sfid,
                          ccrz__AddressFirstline__c = address1,
                          ccrz__AddressSecondline__c = address2,
                          ccrz__AddressThirdline__c = address3,
                          ccrz__City__c = city,
                          ccrz__Country__c = country,
                          ccrz__FirstName__c = firstName,
                          ccrz__LastName__c = lastName,
                          ccrz__PostalCode__c = (zip != null?zip.substring(0,5):zip),
                          ccrz__State__c = state,
                          //CC_FP_County__c = county,
                          CC_FP_Has_Lift_Gate__c = hasLiftGate,
                          CC_FP_Type__c = addressType,
                          CC_FP_Location__c = location == null ? null : location.Id
                      );
                      insert addr;
                      
                      ccrz__E_AccountAddressBook__c acctAddr = new ccrz__E_AccountAddressBook__c(
                          ccrz__Account__c = accountId,      ccrz__AddressType__c = 'Shipping',
                          ccrz__E_ContactAddress__c = addr.Id);
                      
                      insert acctAddr;
                      response.success = true;                                               
                  }  
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult changeAddressById(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = ccrz.cc_CallContext.currContact.Id;

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);

            CCPDCUserPermissionsHelper.UserPermissions permissions =  CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);
            Boolean hasPermission = permissions.allowOverrideShipTo;

            String sfid = (String) input.get('sfid');
            String firstName = (String) input.get('firstName');
            String lastName = (String) input.get('lastName');            
            String addressId = (String) input.get('addressId');
            String addressType = (String) input.get('type');            
            Boolean hasLiftGate = (Boolean) input.get('liftGate');
            String storefront = ccrz.cc_CallContext.storefront;
            Boolean correctAddress = false;
            if (hasPermission) {                
                String addressCheckMessage = '';                                         
                CCAviAddressyAPI.RetrieveResponse detailResponse = CCAviAddressyAPI.retrieveAddress(storefront,  addressId);
                if (detailResponse.success) {
                  // get DC   by zipcode
                  String homeDC = getHomeDCFromOMS((detailResponse.address.ZipCode != null?detailResponse.address.ZipCode.substring(0,5):detailResponse.address.ZipCode));
                  Location__c location = CCFPLocationDAO.getLocationForCode(homeDC);   
                  // if sfid
                  if(sfid != null && sfid != '') {
                    ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
                        Id = sfid,
                        ccrz__AddressFirstline__c = detailResponse.address.Line1,
                        ccrz__AddressSecondline__c = detailResponse.address.Line2,
                        ccrz__AddressThirdline__c = detailResponse.address.Line3,
                        ccrz__City__c = detailResponse.address.City,
                        ccrz__Country__c = detailResponse.address.CountryCode2,
                        ccrz__FirstName__c = firstName,
                        ccrz__LastName__c = lastName,
                        ccrz__PostalCode__c = (detailResponse.address.ZipCode != null?detailResponse.address.ZipCode.substring(0,5):detailResponse.address.ZipCode),
                        ccrz__State__c = detailResponse.address.StateCode,
                        CC_FP_County__c = detailResponse.address.County,
                        CC_FP_Has_Lift_Gate__c = hasLiftGate,
                        CC_FP_Type__c = addressType,
                        CC_FP_Location__c = (location == null) ? null : location.Id
                    );
                    update addr;
                    response.success = true;
                    correctAddress = true;                    

                    } else {
                        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
                            ccrz__AddressFirstline__c = detailResponse.address.Line1,
                            ccrz__AddressSecondline__c = detailResponse.address.Line2,
                            ccrz__AddressThirdline__c = detailResponse.address.Line3,
                            ccrz__City__c = detailResponse.address.City,
                            ccrz__Country__c = detailResponse.address.CountryCode2,
                            ccrz__FirstName__c = firstName,
                            ccrz__LastName__c = lastName,
                            ccrz__PostalCode__c = (detailResponse.address.ZipCode != null?detailResponse.address.ZipCode.substring(0,5):detailResponse.address.ZipCode),
                            ccrz__State__c = detailResponse.address.StateCode,
                            CC_FP_County__c = detailResponse.address.County,
                            CC_FP_Has_Lift_Gate__c = hasLiftGate,
                            CC_FP_Type__c = addressType,
                            CC_FP_Location__c = (location == null) ? null : location.Id
                        );
                        insert addr;

                        ccrz__E_AccountAddressBook__c acctAddr = new ccrz__E_AccountAddressBook__c(
                          ccrz__Account__c = accountId,
                          ccrz__AddressType__c = 'Shipping',
                          ccrz__E_ContactAddress__c = addr.Id
                        );
                        insert acctAddr;
                      }
                      response.success = true;
                      correctAddress = true;                                           
                } // end of detailResponse.success
                else {
                    addressCheckMessage = 'Unable to validate address.';
                }
                if (!correctAddress) {
                    // todo: show addressCheckMessage to user.
                    response.data = input;
                    CCAviPageUtils.buildResponseData(response, false,
                        new Map<String,Object>{'message' => addressCheckMessage}
                    );
                }              
            }          
            else {
                response.success = false;
                response.data = input;
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }      
    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchContactAddress(ccrz.cc_RemoteActionContext ctx, String sfid) {    
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = ccrz.cc_CallContext.currContact.Id;      

        try{
            ccrz__E_ContactAddr__c contactAddress = CCFPContactAddressDAO.getAddress(sfid);
            if (contactAddress != null) {
              Map<String, Object> responseMap = new Map<String, Object>();
              // TODO CCFPAddressBookHelper 
              responseMap.put('contactAddress', contactAddress);
              response.success =  true;
              response.data =  responseMap;
            }
            else {  response.success =  false;  }
          } catch(Exception e){
              CCAviPageUtils.buildResponseData(response, false,new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
              );              
          }
      return response;          
    }    
    
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult deleteAddress(ccrz.cc_RemoteActionContext ctx, String sfid) {   
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = ccrz.cc_CallContext.currContact.Id;      

        try{
          // query contact address and account address
          ccrz__E_ContactAddr__c contactAddress = CCFPContactAddressDAO.getAddress(sfid);
          if(contactAddress != null) {
            List<ccrz__E_AccountAddressBook__c> acctAddr = [ SELECT Id, ccrz__E_ContactAddress__c,CC_FP_isActive__c FROM ccrz__E_AccountAddressBook__c WHERE ccrz__E_ContactAddress__r.Id  =: sfid AND ccrz__Account__c=:accountId];                                                        
            if(acctAddr != null && acctAddr.size() > 0) {
              acctAddr[0].CC_FP_isActive__c = false;
              update acctAddr[0];
              //delete contactAddress;   
              CCAviPageUtils.buildResponseData(response, true,
                new Map<String,Object>{'sfid' => sfid, 'acctAddr' => acctAddr}
              ); 
            }            
          }          

          } catch(Exception e){
              CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
              );            

          }
        return response;          
    }    
    
    //call OMS to get HomeDC for the zipcode.
    private static String getHomeDCFromOMS(String zipCode) {
        List<CCPDCHomeDCResponse> nearestDCList;

        String storefront = ccrz.cc_CallContext.storefront;
        CCPDCOMSAPI api = new CCPDCOMSAPI(storefront);
        CC_PDC_OMS_Settings__c settings = api.omsSettings;
        // Integer sites = 2;
        Integer sites = 1;
        CCPDCOMSHomeDCAPI homeDCApi = new CCPDCOMSHomeDCAPI(zipCode, sites, settings);
        api.queueRequest(homeDCApi);
        api.executeRequests();
        nearestDCList = homeDCApi.nearestDCList;

        // return nearestDCList.size() > 0?nearestDCList[0].refName:'';
        return nearestDCList.size() > 0?nearestDCList[0].dcNbr:'';
    }

}