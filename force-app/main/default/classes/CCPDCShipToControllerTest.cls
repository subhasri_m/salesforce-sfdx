@isTest
public with sharing class CCPDCShipToControllerTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();
    static Map<String, Object> m = new Map<String, Object>();
    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            m = util.initData();
        }
        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.SERVICE_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccServiceCart' => 'c.CCPDCServiceCart'
                }
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void constructorTest() {
        User thisUser = [SELECT Id,ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            util.initCallContext();
            

            String cartId = ApexPages.currentPage().getParameters().put('cartId', 'enc123');
            ccrz__E_Cart__c cart = util.getCart();
            Location__c loc = util.getLocation();
            cart.CC_FP_Location__c = loc.Id;
            update cart;

            CCPDCShipToController controller = null;

            Test.startTest();
            controller = new CCPDCShipToController();
            Test.stopTest();

            System.assert(controller != null);
            System.assert(!controller.isLoaded);
            System.assert(controller.data != null);

        }
    }
    static testmethod void getCartTest() {
        util.initCallContext();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.getCart(ctx);
        Test.stopTest();

        System.assert(result != null);
  //      System.assert(result.success);
    }
    static testmethod void getCartTest2() {


        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__ShipTo__c = null;
        update cart;

        util.initCallContext();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.getCart(ctx);
        Test.stopTest();

        System.assert(result != null);
  //      System.assert(result.success);
    }
    static testmethod void getCartTest3() {

        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__ShipTo__c = null;
        cart.ccrz__BillTo__c = null;
        update cart;
        
        util.initCallContext();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.getCart(ctx);
        Test.stopTest();

        System.assert(result != null);
  //      System.assert(result.success);
    }
    static testmethod void changeFulfillmentWillCallTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;
        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        update a;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeFulfillment(ctx, cart.Id, true, true);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }
    static testmethod void changeFulfillmentWillCallFailTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeFulfillment(ctx, cart.Id, true, true);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
    }
    static testmethod void changeFulfillmentTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeFulfillment(ctx, cart.Id, false, false);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }
    static testmethod void changeAddressTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        List<String> responses = new List<String>{CCAviAddressyAPITest.VALID_ADDRESS_STRING, CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));
        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);   

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddress(ctx, input);
        Test.stopTest();

        ccrz__E_Cart__c updatedCart = [SELECT  Id, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id ];
        
        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(updatedCart.ccrz__ShipTo__c);
        System.assert(result != null);
        System.assert(shipTo != null);
        System.assert(result.success = true);
    }
    static testmethod void changeAddressOverrideTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        List<String> responses = new List<String>{CCAviAddressyAPITest.VALID_ADDRESS_STRING, CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));
        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);   

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"saveToAddressBook":"true","overrideAddress":"true","firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddress(ctx, input);
        Test.stopTest();

        ccrz__E_Cart__c updatedCart = [SELECT  Id, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id ];
        
        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(updatedCart.ccrz__ShipTo__c);
        System.assert(result != null);
        System.assert(shipTo != null);
        System.assert(result.success = true);
    }
    static testmethod void changeAddressnotOverrideTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        List<String> responses = new List<String>{CCAviAddressyAPITest.VALID_ADDRESS_STRING, CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));
        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);   

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"saveToAddressBook":"true","overrideAddress":"false","firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddress(ctx, input);
        Test.stopTest();

        ccrz__E_Cart__c updatedCart = [SELECT  Id, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id ];
        
        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(updatedCart.ccrz__ShipTo__c);
        System.assert(result != null);
        System.assert(shipTo != null);
        System.assert(result.success = true);
    }
    static testmethod void changeAddressNoSFIDTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        List<String> responses = new List<String>{CCAviAddressyAPITest.VALID_ADDRESS_STRING, CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));
        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);      

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{ "firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial" }';
                

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddress(ctx, input);
        Test.stopTest();

        ccrz__E_Cart__c updatedCart = [SELECT  Id, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id ];
        
        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(updatedCart.ccrz__ShipTo__c);
        System.assert(result != null);
        System.assert(shipTo != null);
        System.assert(result.success = true);
    }
    static testmethod void changeAddressMoreThanOneResponseTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        List<String> responses = new List<String>{CCAviAddressyAPITest.VALID_ADDRESS_STRING, CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));
        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"33 N Lasalle St","city":"Chicago","state":"IL","country":"US","postalCode":"60602","liftGate":true,"type":"Commercial","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddress(ctx, input);
        Test.stopTest();
                        
        System.assert(result != null);
        System.assert(result.data != null);                
    }    
    static testmethod void changeAddressSaveToAddressBookTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        List<String> responses = new List<String>{CCAviAddressyAPITest.VALID_ADDRESS_STRING, CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));
        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial","saveToAddressBook":true,"sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddress(ctx, input);
        Test.stopTest();

        ccrz__E_Cart__c updatedCart = [SELECT  Id, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id ];
        
        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(updatedCart.ccrz__ShipTo__c);
        System.assert(result != null);
        System.assert(shipTo != null);
        System.assert(result.success = true);
    }
    static testmethod void changeAddressSaveToAddressBookNoSFIDTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        List<String> responses = new List<String>{CCAviAddressyAPITest.VALID_ADDRESS_STRING, CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));
        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial","saveToAddressBook":true}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddress(ctx, input);
        Test.stopTest();

        ccrz__E_Cart__c updatedCart = [SELECT  Id, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id ];
        
        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(updatedCart.ccrz__ShipTo__c);
        System.assert(result != null);
        System.assert(shipTo != null);
        System.assert(result.success = true);
    }    
    static testmethod void changeAddressFailTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddress(ctx, input);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
    }
	static testmethod void getAddressBookTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.getAddressBook(ctx);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void changeAddressFromBookTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        List<ccrz__E_AccountAddressBook__c> items = CCFPAddressBookDAO.getAddressBooksForAccount(ccrz.cc_CallContext.currAccountId);

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddressFromBook(ctx, items[0].ccrz__E_ContactAddress__r.Id, cart.ccrz__ShipTo__c);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success =true);
    }

    static testmethod void changeAddressByIdTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;        
        Location__c loc = new Location__c();
        loc.Name = 'BB';
        loc.Location__c = 'BB';
        loc.Location_Id__c = 'BB';        
        insert loc;
        Location__c loc2 = new Location__c();
        loc2.Name = 'NH';
        loc2.Location__c = 'NH';
        loc2.Location_Id__c = 'NH';        
        insert loc2;

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);
        
        List<String> responses = new List<String>{CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING2, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));          

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();        
        ccrz__E_ContactAddr__c shipToBefore = CCFPContactAddressDAO.getAddress(cart.ccrz__ShipTo__c);
        System.debug('changeAddressByIdTest shipTo before' + shipToBefore);
        String input = '{"firstName":"Todde","lastName":"Mitchell","liftGate":true,"type":"Commercial", "addressId":"US|US|B|Y214362453|5119","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddressById(ctx, input);
        Test.stopTest();
        
        
        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(cart.ccrz__ShipTo__c);
        System.debug('changeAddressByIdTest shipTo' + shipTo);
        System.assert(shipTo != null, shipTo);
        System.assert(result != null, cart.ccrz__ShipTo__c);
        System.assert(result.success = true);
    } 

    static testmethod void changeAddressByIdNoSFIDTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;        
        Location__c loc = new Location__c();
        loc.Name = 'BB';
        loc.Location__c = 'BB';
        loc.Location_Id__c = 'BB';        
        insert loc;
        Location__c loc2 = new Location__c();
        loc2.Name = 'NH';
        loc2.Location__c = 'NH';
        loc2.Location_Id__c = 'NH';        
        insert loc2;

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);
        
        List<String> responses = new List<String>{CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING2, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));  


        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();        
        String input = '{"firstName":"Todde","lastName":"Mitchell","liftGate":true,"type":"Commercial","addressId":"US|US|B|Y214362453|5119","sfid":""}';  
                
        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddressById(ctx, input);
        Test.stopTest();        
        
        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(cart.ccrz__ShipTo__c);
        System.debug('changeAddressByIdNoSFIDTest shipTo' + shipTo);
        System.assert(shipTo != null, shipTo);
        System.assert(result != null, cart.ccrz__ShipTo__c);
        System.assert(result.success = true);
    }     
    static testmethod void changeAddressByIdSaveToAddressBookTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;
        Test.setMock(HttpCalloutMock.class, new CCAviAddressyAPITest.AddressyAPIMock(CCAviAddressyAPITest.AddressyAPIMockResult.RETRIEVE_SUCCESS));
        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);        
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();        
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial", "addressId":"US|US|A|Y214362453|5119","saveToAddressBook":true,"sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddressById(ctx, input);
        Test.stopTest();
        ccrz__E_Cart__c updatedCart = [SELECT  Id, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: cart.Id ];
        
        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(updatedCart.ccrz__ShipTo__c);
        System.assert(result != null, cart.ccrz__ShipTo__c);
        System.assert(shipTo != null);
        System.assert(result.success = true);
    }     
    static testmethod void changeAddressByIdTestFail() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;
        Test.setMock(HttpCalloutMock.class, new CCAviAddressyAPITest.AddressyAPIMock(CCAviAddressyAPITest.AddressyAPIMockResult.VALID_ADDRESS));
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        //String input = '{"firstName":"Todde","lastName":"Mitchell","liftGate":true,"type":"Commercial","addressId":"XXX","sfid":""';
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","country":"US","postalCode":"55901","liftGate":true,"type":"Commercial", "addressId":"","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.changeAddressById(ctx, input);
        Test.stopTest();

        System.assert(result != null);
        //System.assert(result.success = false);
    }

    static testmethod void setCartWithNewLocationDcTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);

        Location__c loc = new Location__c();
        loc.Name = 'BB';
        loc.Location__c = 'BB';
        loc.Location_Id__c = 'BB';        
        insert loc;
        Location__c loc2 = new Location__c();
        loc2.Name = 'NH';
        loc2.Location__c = 'NH';
        loc2.Location_Id__c = 'NH';        
        insert loc2;
        
        List<String> responses = new List<String>{CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));          

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.setCartWithNewLocationDc(ctx);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }
    static testmethod void setDefaultWillCallTest() {
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        util.initCallContext();
        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
            result = CCPDCShipToController.setDefaultWillCall(ctx,true);
        Test.stopTest();
        System.assert(result != null);


    }
    static testmethod void changeDefaultWillCallTest() {
        Integer result;
        util.initCallContext();
        Test.startTest();
            result = CCPDCShipToController.changeDefaultWillCall(true);
        Test.stopTest();
        System.assertEquals(result,0);
        
    }
    
    static testmethod void doCombinedGetCartSetNewLocationActionTest() {
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        util.initCallContext();
        ccrz.cc_RemoteActionResult result = null;
        system.debug('@@Rahul ' + ctx);
        Test.startTest();
            result = CCPDCShipToController.doCombinedGetCartSetNewLocationAction(ctx);
        Test.stopTest();
        
    }

}