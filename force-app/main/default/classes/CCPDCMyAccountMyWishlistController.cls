global with sharing class CCPDCMyAccountMyWishlistController {
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getWishlists(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            Map<String, Object> request = new Map<String,Object>{
                ccrz.ccApi.API_VERSION => 6, 
                ccrz.ccApiCart.CARTTYPE => 'WishList', 
                ccrz.ccApiCart.CARTSTATUS => 'Open', 
                ccrz.ccApiCart.BYEFFECTIVEACCOUNT => ccrz.cc_CallContext.effAccountId,
                ccrz.ccAPI.SIZING => new Map<String, Object>{
                    ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
                        ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL 
                    }
                }
            };
            Map<String,Object> data = ccrz.ccAPICart.fetch(request);
            //ccrz.ccLog.log('MyAccount_MyWishLists ',data);
            Boolean isSuccess = (Boolean) data.get(ccrz.ccAPI.SUCCESS);
            if (isSuccess) {
                List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) data.get(ccrz.ccAPICart.CART_OBJLIST);
                
                processCartData(outputCartList);
                
                List<Map<String, Object>> productList = (List<Map<String, Object>>) data.get(ccrz.ccAPIProduct.PRODUCTLIST);
                if (productList != null && !productList.isEmpty()) {
                    addProductToCart(outputCartList, productList);
                }
                //outputCartList.add(new Map<String, object>{'Pricing' => true});
                CCAviPageUtils.buildResponseData(response, true, 
                    new Map<String,Object>{'carts' => outputCartList}
                );                
            }
            else {
                response.success = false;
            }

        }
        catch (Exception e) {
            system.debug('Limits.getHeapSize() Excp --> '+ Limits.getHeapSize());
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }

    private static void processCartData(List<Map<String, Object>> outputCartList) {
        
        if (outputCartList != null && !outputCartList.isEmpty()) {
            Set<String> ownerIds = new Set<String>();
            for (Map<String, Object> m : outputCartList) {
                String ownerId = (String) m.get('ownerId');
                
                if (ownerId != null) {
                    ownerIds.add(ownerId);
                    m.put('isOwner', ownerId.equals(ccrz.cc_CallContext.currUserId));
                }
                String lastModifiedDate = String.ValueOf(m.get('lastModifiedDate')) ;
                if (lastModifiedDate != null) {
                    lastModifiedDate = lastModifiedDate.replace('T',' ').replace('Z', '');
                    try {
                        Datetime myDate = Datetime.valueOfGmt(lastModifiedDate);
                        m.put('lastModifiedDateStr', myDate.format('MM/dd/yyyy h:mm a', UserInfo.getTimeZone().getID()));
                    }
                    catch (Exception e) {
                        m.put('lastModifiedDateStr', lastModifiedDate);
                    }
                }
                m.put('isActive', m.get('activeCart'));
                
            }
            if (!ownerIds.isEmpty()) {
                Map<Id, User> userMap = CCPDCUserDAO.getUsersMap(new List<String>(ownerIds));
                for (Map<String, Object> m : outputCartList) {
                    String ownerId = (String) m.get('ownerId');
                    if (ownerId != null) {
                        User u = userMap.get(ownerId);
                        if (u != null) {
                            m.put('ownerName', u.Contact.Name);
                        }
                    }
                }
            }
        }
    }

    private static void addProductToCart(List<Map<String, Object>> outputCartList, List<Map<String, Object>> productList) {
        Map<String, Map<String, Object>> productMap = new Map<String, Map<String, Object>>();
        for (Map<String, Object> m : productList) {
            String key = (String) m.get('sfid');
            productMap.put(key, m);
        }
        Id contactId = String.valueOf(ccrz.cc_CallContext.currContact.Id);
        //List<CC_FP_Contract_Account_Permission_Matrix__c> matrixList = CCFPContactAccountPermissionMatrixDAO.getAccountsForContact(contactId);
        for (Map<String, Object> cart : outputCartList){
            List<Map<String, Object>> items = (List<Map<String, Object>>) cart.get('ECartItemsS');
            List<Map<String, Object>> products = new List<Map<String, Object>>();
            cart.put('productItems', products);  
            if (items!=null){
            for (Map<String, Object> item : items) {
                Map<String, Object> prodMap = new Map<String, Object>();
                products.add(prodMap);
                prodMap.put('uid', item.get('sfid'));
                prodMap.put('parentId', item.get('cart'));
                String key = (String) item.get('product');
                //prodMap.put('CCFPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                Map<String, Object> productItem = productMap.get(key);
                if(productItem != null && !productItem.isEmpty()){
                    productItem.put('id', productItem.get('sfid'));
                    productItem.put('sku', productItem.get('SKU'));
                    productItem.put('ProductStatus', productItem.get('productStatus'));
                    productItem.put('ProductType', productItem.get('productType'));
                    //productItem.put('ProductType_New', 'ProductType_New');
                    
                    prodMap.put('prodBean', productItem);
                    
                }else{
                    productItem = new Map<String, Object>();
                    Map<String, Object> newProductItems = (Map<String, Object>)item.get('productR');
                    productItem.put('id',newProductItems.get('sfid'));
                    productItem.put('sku',newProductItems.get('SKU'));
                    productItem.put('ProductStatus', newProductItems.get('productStatus'));
                    productItem.put('ProductType', newProductItems.get('productType'));
                    //productItem.put('CCFPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                    prodMap.put('prodBean', productItem);
                    
                }

            }
            } 
        }
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult changeCartOwner(ccrz.cc_RemoteActionContext ctx, String cartId) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            if (cartId != null) {
                CCPDCChangeOwnerHelper.changeCartOwnerAll(cartId, ccrz.cc_CallContext.currUserId);
                response.success = true;
            }
            else {
                response.success = false;
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }
	@RemoteAction
    global static ccrz.cc_RemoteActionResult cloneCart(ccrz.cc_RemoteActionContext ctx, String cartId) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        response.success = false;

        try {
            if (cartId != null) {
                ccrz__E_Cart__c cart = CCPDCCartDAO.getCartOwner(cartId);
                Id oldOwner = cart.OwnerId;
                Id oldAccount = cart.ccrz__Account__c;
                Id oldContact = cart.ccrz__Contact__c;

                Id newOwner = ccrz.cc_CallContext.currUserId;
                Id newAccount = ccrz.cc_CallContext.currAccountId;
                Id newContact = ccrz.cc_CallContext.currContact.Id;

                CCPDCChangeOwnerHelper.changeCartOwnerAll(cartId, newOwner, newContact, newAccount);
                String newId = CCAviCartManager.cloneCart(cartId);
                CCPDCChangeOwnerHelper.changeCartOwnerAll(cartId, oldOwner, oldContact, oldAccount);
                if (newId != null) {
                    response.data = new Map<String,Object>{'encId' => newId};
                    response.success = true;
                }
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }
	@RemoteAction
    global static ccrz.cc_RemoteActionResult editWishlist(ccrz.cc_RemoteActionContext ctx, String cartId, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        response.success = false;

        try {
            if (cartId != null && data != null) {
                Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);

                ccrz__E_Cart__c cart = CCPDCCartDAO.getCartOwner(cartId);
                if (cart != null) {
                    String name = (String) input.get('name');
                    String note = (String) input.get('note');
                    Boolean isActive = (Boolean) input.get('isActive');
                    if (String.isNotBlank(name)) {
                        cart.ccrz__Name__c = name;

                    }
                    if (String.isNotBlank(note)) {
                        cart.ccrz__Note__c = note;
                    }
                    if (isActive != null) {
                        cart.ccrz__ActiveCart__c = isActive;
                    }
                    CCPDCChangeOwnerHelper.updateCart(cart);
                    response.success = true;
                }
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }
	@RemoteAction
    global static ccrz.cc_RemoteActionResult addAllItemsToCart(ccrz.cc_RemoteActionContext ctx, String cartsfid){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        response.success = false;
        String currCartId = ccrz.cc_CallContext.currCartId;
        ccrz__E_Cart__c wishlist = CCPDCCartDAO.getCartSfid(cartsfid);
        ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(currCartId);


        List<Map<String, String>> cartData = new List<Map<String, String>>();
        List<ccrz.ccApiCart.LineData> theNewLines = new List<ccrz.ccApiCart.LineData>();

        if(wishlist != null){
            if(!wishlist.ccrz__E_CartItems__r.isEmpty()){
                Boolean wasSuccessful = false;
                String theCartSFID = cart.Id;//presumes that the cart id is passed into this code.

                for (CCRZ__E_CartItem__c item : wishlist.ccrz__E_CartItems__r){
                    ccrz.ccApiCart.LineData theNewLine = new ccrz.ccApiCart.LineData();
                    theNewLine.sku = String.valueOf(item.ccrz__Product__r.ccrz__SKU__c);
                    theNewLine.quantity = Decimal.valueOf(String.valueOf(item.ccrz__Quantity__c));
                    theNewLines.add( theNewLine );

                }
                    try{
                        Map<String,Object> addResults = CCPDCCartManager.addTo(new Map<String,Object>{
                            ccrz.ccApi.API_VERSION => 6,
                            ccrz.ccApiCart.CART_ID => theCartSFID,
                            ccrz.ccApiCart.LINE_DATA => theNewLines
                        });
                      
                        //General check, since the cart id will be blank if not successful we technically do not need this.
                        wasSuccessful = (Boolean)addResults.get(ccrz.ccApi.SUCCESS);
                        response.success = wasSuccessful;
                        CCAviPageUtils.buildResponseData(response, true, 
                                new Map<String,Object>{'data' => addResults});
                    }catch(Exception e){
                        CCAviPageUtils.buildResponseData(response, false,
                            new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
                        );
                    }
                
            }
        }


        return response;
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult addToCart(ccrz.cc_RemoteActionContext ctx, String sku, Integer quantity) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        ccrz.ccLog.log(System.LoggingLevel.INFO,'M:E','addToCart');
        response.success = false;

        try {
            String cartId = ccrz.cc_CallContext.currCartId;
            if (cartId == null) {
                cartId = CCAviCartManager.getActiveCartId();
            }

            if (cartId != null) {
                List<ccrz.ccApiCart.LineData> newLines = new List<ccrz.ccApiCart.LineData>();
                ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
                newLine.sku = sku;
                newLine.quantity = quantity;
                newLines.add(newLine);

                Map<String, Object> request = new Map<String,Object>{
                    ccrz.ccApi.API_VERSION => 6, 
                    ccrz.ccApiCart.CART_ENCID => cartId,
                    ccrz.ccApiCart.LINE_DATA => newLines
                };
        
                Map<String, Object> outputData = CCPDCCartManager.addTo(request);
                Boolean wasSuccessful = (Boolean)outputData.get(ccrz.ccApi.SUCCESS);
                if (wasSuccessful) {
                    Map<String,Object> output = new Map<String,Object>();
                    output.put('cartId', cartId);
                    CCAviPageUtils.buildResponseData(response, true, output);
                }
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        finally{
            ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','addToCart');
           // ccrz.ccLog.close(response);
        }

        return response;
    }

}