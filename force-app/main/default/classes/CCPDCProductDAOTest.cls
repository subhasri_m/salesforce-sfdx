@isTest
public class CCPDCProductDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void getPartsTest() {
        //CCPDCTestUtil util = new CCPDCTestUtil();
        //Map<String, Object> m = util.initData();
        //ccrz__E_Product__c p = (ccrz__E_Product__c)m.get('product');
        ccrz.cc_CallContext.storefront = util.STOREFRONT;

        ccrz__E_Product__c p = Util.getProduct();
        List<ccrz__E_Product__c> items = null;
        Test.startTest();
        items = CCPDCProductDAO.getParts(new List<String> {'product-01'}, ccrz.cc_CallContext.storefront);
        Test.stopTest();

        System.assert(items != null);
        System.assertEquals(1, items.size());
    }
    
    static testmethod void getPartsDspTest() {
        ccrz.cc_CallContext.storefront = util.STOREFRONT;

        ccrz__E_Product__c p = Util.getProduct();
        List<ccrz__E_Product__c> items = null;
        Test.startTest();
        items = CCPDCProductDAO.getPartsDsp(new List<String> {'product-01'}, ccrz.cc_CallContext.storefront);
        Test.stopTest();

        System.assert(items != null);
        System.assertEquals(1, items.size());
    }

    static testmethod void getProductsForPricingBySkuTest() {

        Map<String, ccrz__E_Product__c> items = null;
        Test.startTest();
        items = CCPDCProductDAO.getProductsForPricingBySku(new Set<String> {'product-01'});
        Test.stopTest();

        System.assert(items != null);
        System.assertEquals(1, items.size());
    }

    static testmethod void getProductsForPricingByIdTest() {
        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String, ccrz__E_Product__c> items = null;
        Test.startTest();
        items = CCPDCProductDAO.getProductsForPricingById(new Set<String> {product.Id});
        Test.stopTest();

        System.assert(items != null);
        System.assertEquals(1, items.size());
    }

    static testmethod void searchPartsTest() {

        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        Account account = util.getAccount();
        ccrz__E_Product__c product1 = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        Account_Product_Cross_Reference__c testRef = new Account_Product_Cross_Reference__c(Account_Part__c = 'part-01',Related_to_Account__c = account.Id, Related_to_Product__c = product1.Id);
        insert testRef;
        List<Account_Product_Cross_Reference__c> productsRef = null;
        Test.startTest();
        productsRef = CCPDCProductDAO.searchParts('part-01',String.valueOf(account.Id),ccrz.cc_CallContext.storefront);
        Test.stopTest();

        System.assert(productsRef != null);
        System.assertEquals(1, productsRef.size());
    }

    static testmethod void searchPartsProductCrossReferenceTest() {

        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        ccrz__E_Product__c product1 = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        Product_Cross_Reference__c testRef = new Product_Cross_Reference__c(Interchange_Part_Number__c = 'part-01',Related_to_Product__c = product1.Id);
        insert testRef;
        List<Product_Cross_Reference__c> productsRef = null;
        Test.startTest();
        productsRef = CCPDCProductDAO.searchParts('part-01',ccrz.cc_CallContext.storefront);
        Test.stopTest();

        System.assert(productsRef != null);
        System.assertEquals(1, productsRef.size());
    }

    static testmethod void getProductTypeBySkuTest() {

        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        ccrz__E_Product__c resultProduct = null;
        Test.startTest();
        resultProduct = CCPDCProductDAO.getProductTypeBySku(product.ccrz__SKU__c);
        Test.stopTest();

        System.assert(resultProduct != null);
        System.assertEquals(resultProduct.Id, product.Id);
    }

    static testmethod void getSpecsFromChildrenTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Spec__c ccspec = new ccrz__E_Spec__c(Name='test Spec');
        insert ccspec;
        List<ccrz__E_Spec__c> specs = [SELECT Id FROM ccrz__E_Spec__c WHERE Name = 'test Spec'];
        ccrz__E_Spec__c spec = specs.get(0);


        ccrz__E_Product__c product = products.get(0);
        List<ccrz__E_ProductSpec__c> returnResults = null;
        ccrz__E_ProductSpec__c testProductSpecs = new ccrz__E_ProductSpec__c(ccrz__Product__c = product.Id, ccrz__Spec__c = spec.Id);
        upsert testProductSpecs;

        List<Id> productIds = new List<Id>();
        productIds.add(product.Id);
        List<Id> specIds = new List<Id>();
        specIds.add(spec.Id);

        Test.startTest();
        returnResults = CCPDCProductDAO.getSpecsFromChildren(productIds,specIds);
        Test.stopTest();
        System.assert(returnResults != null);
        System.assertEquals(1, returnResults.size());

    }

    static testmethod void getChildProductsTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        ccrz__E_CompositeProduct__c testComp = new ccrz__E_CompositeProduct__c(ccrz__Composite__c = product.Id, ccrz__Component__c =product.Id );


        product.ccrz__ProductStatus__c = 'Released';
        product.ccrz__StartDate__c= Date.today();
        product.ccrz__EndDate__c= Date.today();
        
        upsert product;
        upsert testComp;
        String prodID =String.valueOf(product.Id);
        List<ccrz__E_CompositeProduct__c> listOfComps =  [SELECT Id, ccrz__Composite__c FROM ccrz__E_CompositeProduct__c WHERE ccrz__Composite__c =: prodID];
        ccrz__E_CompositeProduct__c compsiteP = listOfComps.get(0);

        List<ccrz__E_CompositeProduct__c> returnResults = null;


        Test.startTest();
        returnResults = CCPDCProductDAO.getChildProducts(product.Id);
        Test.stopTest();
        System.debug(compsiteP);
        System.assert(returnResults != null);
        System.assertEquals(1, returnResults.size());

    }

    static testmethod void getProductInventoryForSkusTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<String> skus = new List<String>();
        skus.add('product-01');

        List<ccrz__E_ProductInventoryItem__c> returnResults = null;

        Test.startTest();
        returnResults = CCPDCProductDAO.getProductInventoryForSkus(skus);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    static testmethod void getProductInventoryForSkusNullTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<String> skus = new List<String>();

        List<ccrz__E_ProductInventoryItem__c> returnResults = null;

        Test.startTest();
        returnResults = CCPDCProductDAO.getProductInventoryForSkus(skus);
        Test.stopTest();
        System.assert(returnResults != null);
    }

    static testmethod void getProductThumbnailURIForSkusTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<String> skus = new List<String>();
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        skus.add('product-01');

        ccrz__E_ProductMedia__c newImage = new ccrz__E_ProductMedia__c(
            ccrz__Product__c = product.Id,
            ccrz__EndDate__c = Date.today(),
            ccrz__Sequence__c = 500,
            ccrz__StartDate__c = Date.today(),
            ccrz__ProductMediaSource__c = 'URI',
            ccrz__MediaType__c = 'Product Image Thumbnail'
            );
        insert newImage;

        List<ccrz__E_ProductMedia__c> returnResults = null;

        Test.startTest();
        returnResults = CCPDCProductDAO.getProductThumbnailURIForSkus(skus);
        Test.stopTest();
        System.assert(returnResults != null);
        System.assertEquals(1, returnResults.size());
    }

    static testmethod void getProductThumbnailURIForSkusNullTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<String> skus = new List<String>();

        List<ccrz__E_ProductMedia__c> returnResults = null;

        Test.startTest();
        returnResults = CCPDCProductDAO.getProductThumbnailURIForSkus(skus);
        Test.stopTest();
        System.assert(returnResults != null);
    }

    static testmethod void getRequiredProductsBySkuTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        Set<String> skus = new Set<String>();
        skus.add('product-01');
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        ccrz__E_RelatedProduct__c relatedProd = new ccrz__E_RelatedProduct__c(
            ccrz__RelatedProduct__c = product.Id,
            ccrz__Product__c = product.Id,
            ccrz__RelatedProductType__c = 'Required',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today(),
            ccrz__Sequence__c = 500
            );
        insert relatedProd;

        List<ccrz__E_RelatedProduct__c> returnResults = new List<ccrz__E_RelatedProduct__c>();

        Test.startTest();
        returnResults = CCPDCProductDAO.getRequiredProductsBySku(skus);
        Test.stopTest();
        System.assert(returnResults != null);
        System.assertEquals(1, returnResults.size());
    }

    static testmethod void getRequiredProductsByIdTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        Set<String> ids = new Set<String>();
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        ids.add(String.valueOf(product.Id));

        ccrz__E_RelatedProduct__c relatedProd = new ccrz__E_RelatedProduct__c(
            ccrz__RelatedProduct__c = product.Id,
            ccrz__Product__c = product.Id,
            ccrz__RelatedProductType__c = 'Required',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today(),
            ccrz__Sequence__c = 500
            );
        insert relatedProd;

        List<ccrz__E_RelatedProduct__c> returnResults = new List<ccrz__E_RelatedProduct__c>();

        Test.startTest();
        returnResults = CCPDCProductDAO.getRequiredProductsById(ids);
        Test.stopTest();
        System.assert(returnResults != null);
        System.assertEquals(1, returnResults.size());
    }

    static testmethod void getRelatedProductsFromIdTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        Set<String> ids = new Set<String>();
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        ids.add(String.valueOf(product.Id));

        ccrz__E_RelatedProduct__c relatedProd = new ccrz__E_RelatedProduct__c(
            ccrz__RelatedProduct__c = product.Id,
            ccrz__Product__c = product.Id,
            ccrz__RelatedProductType__c = 'Interchange',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today(),
            ccrz__Sequence__c = 500
            );
        insert relatedProd;

        List<ccrz__E_RelatedProduct__c> returnResults = new List<ccrz__E_RelatedProduct__c>();

        Test.startTest();
        returnResults = CCPDCProductDAO.getRelatedProductsFromId(ids);
        Test.stopTest();
        System.assert(returnResults != null);
        System.assertEquals(1, returnResults.size());
    }
    static testmethod void getRelatedSupercessionProductsFromIdTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        Set<String> ids = new Set<String>();
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        ids.add(String.valueOf(product.Id));

        ccrz__E_RelatedProduct__c relatedProd = new ccrz__E_RelatedProduct__c(
            ccrz__RelatedProduct__c = product.Id,
            ccrz__Product__c = product.Id,
            ccrz__RelatedProductType__c = 'Interchange',
            ccrz__StartDate__c = Date.today(),
            ccrz__EndDate__c = Date.today(),
            ccrz__Sequence__c = 500
            );
        insert relatedProd;

        List<ccrz__E_RelatedProduct__c> returnResults = new List<ccrz__E_RelatedProduct__c>();

        Test.startTest();
        returnResults = CCPDCProductDAO.getRelatedSupercessionProductsFromId(ids);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    static testmethod void getRelatedProductsFromIdNullTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        Set<String> ids = new Set<String>();

        List<ccrz__E_RelatedProduct__c> returnResults = null;

        Test.startTest();
        returnResults = CCPDCProductDAO.getRelatedProductsFromId(ids);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    
    static testmethod void getCrossReferencesForProductTest(){
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        Set<String> ids = new Set<String>();
        List<Product_Cross_Reference__c> returnResults = null;
        ccrz__E_Product__c products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1' limit 1];
        Product_Cross_Reference__c crossProduct2 = new Product_Cross_Reference__c();
        crossProduct2.Related_to_Product__c = products.id;
        crossProduct2.Brand_Name__c = 'testName';
        crossProduct2.Interchange_Part_Number__c = 'sc16001';
        insert crossProduct2;
        
        Test.startTest();
        returnResults = CCPDCProductDAO.getCrossReferencesForProduct(products.ccrz__SKU__c, 'pdc');
        Test.stopTest();
        System.assert(returnResults != null);
    }
    
    static testmethod void getproductfitmentyearTest(){
         List<Product_Fitment__c> returnResults = null;
        ccrz__E_Product__c products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1' limit 1];
        Product_Fitment__c Productfitment2 = new Product_Fitment__c();
        Productfitment2.Related_to_Product__c = products.id;
        Productfitment2 .MakeDesc__c= 'testName';
        Productfitment2 .ModelDesc__c= 'sc16001';
        insert Productfitment2;
        
        Test.startTest();
        returnResults = CCPDCProductDAO.getproductfitmentyear(products.ccrz__SKU__c);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    static testmethod void getproductfitmentmakeTest(){
        List<Product_Fitment__c> returnResults = null;
        ccrz__E_Product__c products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1' limit 1];
        Product_Fitment__c Productfitment2 = new Product_Fitment__c();
        Productfitment2.Related_to_Product__c = products.id;
        Productfitment2 .MakeDesc__c= 'testName';
        Productfitment2 .ModelDesc__c= 'sc16001';
        Productfitment2 .Year__c =2019;
        insert Productfitment2;
        
        Test.startTest();
        returnResults = CCPDCProductDAO.getproductfitmentmake('2019',products.ccrz__SKU__c);
        Test.stopTest();
        System.assert(returnResults != null);
     }
    static testmethod void getproductfitmentmodelTest(){
            List<Product_Fitment__c> returnResults = null;
        ccrz__E_Product__c products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1' limit 1];
        Product_Fitment__c Productfitment2 = new Product_Fitment__c();
        Productfitment2.Related_to_Product__c = products.id;
        Productfitment2 .MakeDesc__c= 'testName';
        Productfitment2 .ModelDesc__c= 'sc16001';
        insert Productfitment2;
        
        Test.startTest();
        returnResults = CCPDCProductDAO.getproductfitmentmodel('2019','testName',products.ccrz__SKU__c);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    static testmethod void getproductfitmentengineTest(){
         List<Product_Fitment__c> returnResults = null;
        ccrz__E_Product__c products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1' limit 1];
        Product_Fitment__c Productfitment2 = new Product_Fitment__c();
        Productfitment2.Related_to_Product__c = products.id;
        Productfitment2 .MakeDesc__c= 'testName';
        Productfitment2 .ModelDesc__c= 'sc16001';
        insert Productfitment2;
        
        Test.startTest();
        returnResults = CCPDCProductDAO.getproductfitmentengine('2019','testName','sc16001',products.ccrz__SKU__c);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    static testmethod void  getProductFitmentMessageTest(){
        List<Product_Fitment__c> returnResults = null;
        ccrz__E_Product__c products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1' limit 1];
        Product_Fitment__c Productfitment2 = new Product_Fitment__c();
        Productfitment2.Related_to_Product__c = products.id;
        Productfitment2 .Make__c= 'testName';
        Productfitment2 .ModelDesc__c= 'sc16001';
        insert Productfitment2;
        
        Test.startTest();
        returnResults = CCPDCProductDAO.getProductFitmentMessage('1998','testmake','testmodel','BMWengine',products.ccrz__SKU__c);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    
    static testmethod void getProductInventoryForSkusAndLocationTest(){
        List<ccrz__E_ProductInventoryItem__c> returnResults = new  List<ccrz__E_ProductInventoryItem__c>();
        ccrz__E_Product__c products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1' limit 1];
        Product_Fitment__c Productfitment2 = new Product_Fitment__c();
        Productfitment2.Related_to_Product__c = products.id;
        Productfitment2 .Make__c= 'testName';
        Productfitment2 .ModelDesc__c= 'sc16001';
        insert Productfitment2;
        
        Test.startTest();
        List<String> lstSKUs = new List<String>();
         List<String> lstLocs = new List<String>();
        lstSKUs.add(products.ccrz__SKU__c);
        lstLocs.add('testLoc1');
        
        returnResults = CCPDCProductDAO.getProductInventoryForSkusAndLocation(lstSKUs,lstLocs);
        Test.stopTest();
        System.assert(returnResults != null);
    }
 

}