public with sharing class fp_bean_Sitemap {
// bean class for the site map
    public String sfid {get;set;}
    public String sku {get;set;}
    public String lastModDate {get;set;}
    public String categoryId {get;set;}
    public String friendlyUrl {get;set;}

    public fp_bean_Sitemap() {

    }

    public fp_bean_Sitemap(String catId) {
        this.categoryId = catId.escapeHtml4();
        this.categoryId = escapeSingleQuote(this.categoryId);
    }

    public fp_bean_Sitemap(String sfid, String sku, DateTime lmd) {
        this.sfid = sfid;
        this.sku = sku.escapeHtml4();
        this.sku = escapeSingleQuote(this.sku);
        this.lastModDate = lmd.format('yyyy-MM-dd');
    }

    //html4 escaping doesnt handle the single quote so I had to handle it manually in the bean
    private static String escapeSingleQuote(final String input)
    {
        String output = input;
        output = output.replace('\'','&apos;'); 
        return output;
    }
}