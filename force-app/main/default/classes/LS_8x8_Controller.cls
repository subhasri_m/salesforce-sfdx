public without Sharing class LS_8x8_Controller{
    public AgentsWrapper agntwrap {get;set;}
    public QueuesWrapper queuewrap {get;set;}
    
    public class AgentsWrapper{
        public List<AgentWrapper> agent {get;set;}    
    } 
    public class AgentWrapper {
        public String   agent_id    {get;set;}  
        public String   agent_media_id    {get;set;}  
        public String   agent_name     {get;set;}
        public String   agent_status   {get;set;}   
        public String   agent_sub_status  {get;set;}
        public String   day_accepted_count     {get;set;}
        public String   day_available_time    {get;set;}  
        public String   day_avg_processing_time    {get;set;} 
        public String   day_break_time    {get;set;} 
        public String   day_login_time     {get;set;} 
        public String   day_offline_time      {get;set;}
        public String   day_processing_time    {get;set;}  
        public String   day_rejected_count   {get;set;}   
        public String   group_id      {get;set;}
        public String   time_in_status     {get;set;}
        public String   day_presented_calls {get; set;}
        
    }
    public class QueueWrapper
    {
        public String agent_count_busy { get; set; }
        public String agent_count_loggedOut { get; set; }
        public String agent_count_onBreak { get; set; }
        public String agent_count_postProcess { get; set; }
        public String agent_count_waitTransact { get; set; }
        public String agent_count_workOffline { get; set; }
        public Integer day_abandoned { get; set; }
        public String day_accepted { get; set; }
        public String day_avg_duration { get; set; }
        public String day_avg_wait_time { get; set; }
        public Integer day_queued { get; set; }
        public String assigned_agent_count { get; set; }
        public String enabled_agent_count { get; set; }
        public String number_in_offered { get; set; }
        public String number_in_progress { get; set; }
        public String queue_id { get; set; }
        public string queue_name { get; set; }
        public String queue_size { get; set; }
        public string queue_type { get; set; }
        
    }
    
    public class QueuesWrapper
    {
        public List<QueueWrapper> queue { get; set; }
    }
    
   
    public PageReference LS_8x8_Refresh(){
         try{
        HttpRequest req1 = new HttpRequest();
        req1.setEndPoint('callout:X8x8_connect/agents');
        req1.setMethod('GET');
        req1.setHeader('Accept', 'application/json');
        
        HttpRequest req2 = new HttpRequest();
        req2.setEndPoint('callout:X8x8_connect/queues');
        req2.setMethod('GET');
        req2.setHeader('Accept', 'application/json');
       
        
        Http http = new Http();
        HTTPResponse res1 = http.send(req1);
        HTTPResponse res2 = http.send(req2);
        
        
        string jsonStr1 =(res1.getBody()).replace('-1,','99999,');
        jsonStr1= jsonStr1.replace('-','_');
        string jsonStr2 =(res2.getBody()).replace('-1,','99999,');
        jsonStr2= jsonStr2.replace('-','_');
               
        
            
            agntwrap = (AgentsWrapper)JSON.deserialize(jsonStr1,AgentsWrapper.class);
            queuewrap = (QueuesWrapper)JSON.deserialize(jsonStr2,QueuesWrapper.class);
           // Filter Queues
            List<QueueWrapper> queuenewlst = new List<QueueWrapper>();
            for (Integer i= 0; i < queuewrap.Queue.size() ; i++ )
            {
                if (queuewrap.Queue[i].queue_id == '130' || queuewrap.Queue[i].queue_id == '131' || queuewrap.Queue[i].queue_id == '530')
                { 
                    queuenewlst.add(queuewrap.Queue[i]);
                 }
            }
            queuewrap.Queue.clear();
            queuewrap.Queue=queuenewlst.clone();
            queuenewlst.clear();
            changeTimeFormatToHHMMSSQueue(queuewrap.Queue);
            
           // Filter Agents
            List<AgentWrapper> agentnewlst = new List<AgentWrapper>();
            for (Integer j= 0; j < agntwrap.Agent.size() ; j++ )
            {
                if (agntwrap.Agent[j].group_id == '128' && agntwrap.Agent[j].agent_name != '8x8 Brian Comer' && agntwrap.Agent[j].agent_name !='Remedyforce System' && agntwrap.Agent[j].agent_name !='Paul PDC' )
                { 
                    agentnewlst.add(agntwrap.Agent[j]);
                 }
            }
            agntwrap.Agent.clear();
            agntwrap.Agent=agentnewlst.clone();
            agentnewlst.clear();
           
            changeTimeFormatToHHMMSS(agntwrap.Agent);
            performStatusMapping(agntwrap.Agent);
                        
            for (agentWrapper agnt : agntwrap.Agent){
                 agnt.day_presented_calls= String.valueOf(Integer.valueOf(agnt.day_accepted_count) + Integer.valueOf(agnt.day_rejected_count));
                }
            
        }
        catch(Exception e){
        
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please refresh the Page and if issue still persist contact Ecom technical support for further assistance !'));
        }
        return null;
    }
    private void changeTimeFormatToHHMMSSQueue(List<QueueWrapper> queuelst){
        for(QueueWrapper queue : queuelst){
           queue.day_avg_wait_time = DateTime.valueOf(Integer.ValueOf(queue.day_avg_wait_time)*1000).formatGMT('HH:mm:ss');
        }  
    }
    
    private void changeTimeFormatToHHMMSS(List<AgentWrapper> agntlst){
        for (agentWrapper agnt : agntlst){
               // format seconds into HH : mm : ss
                agnt.day_processing_time = DateTime.valueOf(Integer.ValueOf(agnt.day_processing_time)*1000).formatGMT('HH:mm:ss');
                agnt.day_available_time= DateTime.valueOf(Integer.ValueOf(agnt.day_available_time)*1000).formatGMT('HH:mm:ss');
                agnt.day_break_time= DateTime.valueOf(Integer.ValueOf(agnt.day_break_time)*1000).formatGMT('HH:mm:ss');
                agnt.day_offline_time= DateTime.valueOf(Integer.ValueOf(agnt.day_offline_time)*1000).formatGMT('HH:mm:ss');
        
    }
    }
    
    private void performStatusMapping(List<AgentWrapper> agntlst){
         for (agentWrapper agnt : agntlst){
            String agentStatus = agnt.agent_status;
            Switch on agentStatus{
                    when '0' {agnt.agent_status = 'Available';}
                    when '1' {agnt.agent_status = 'Transaction Offered';}
                    when '2' {agnt.agent_status = 'Busy';}
                    when '3' {agnt.agent_status = 'Post Processing';}
                    when '4' {agnt.agent_status = 'On Break';}
                    when '5' {agnt.agent_status = 'Work Offline';}
                    when '9' {agnt.agent_status = 'Logged Out';}
                    when else {agnt.agent_status = 'Status Not Available';}
         }
    }
}   
    
}