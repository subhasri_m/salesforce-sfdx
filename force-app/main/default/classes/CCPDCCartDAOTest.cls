@isTest
public class CCPDCCartDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void getCartTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCPDCCartDAO.getCart(cart.ccrz__EncryptedId__c);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }

    static testmethod void getContactInfoInCartTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCPDCCartDAO.getContactInfoInCart(cart.ccrz__EncryptedId__c);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }

    static testmethod void getBackOrderCartFromAccountIdTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__CartType__c = 'BackOrder';
        update cart;
        Account theAccount = util.getAccount();

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCPDCCartDAO.getBackOrderCartFromAccountId(theAccount.Id);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }

    static testmethod void getCartExtraTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }

    static testmethod void getCartOwnerTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCPDCCartDAO.getCartOwner(cart.Id);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }

    static testmethod void getCartItemsForPricingTest(){
        util.initCallContext();
        ccrz__E_CartItem__c cartItem = util.getCartItem();

        Map<Id, ccrz__E_CartItem__c> data = null;

        Test.startTest();
        data = CCPDCCartDAO.getCartItemsForPricing(new Set<Id>{cartItem.Id});
        Test.stopTest();

        System.assert(data != null);
        System.assert(data.containsKey(cartItem.Id));
    }

    static testmethod void getCartItemsTest(){
        util.initCallContext();
        ccrz__E_CartItem__c cartItem = util.getCartItem();

        List<ccrz__E_CartItem__c> data = null;

        Test.startTest();
        data = CCPDCCartDAO.getCartItems(new Set<Id>{cartItem.Id});
        Test.stopTest();

        System.assert(data != null);
        System.assert(data.size() == 1);
    }

    static testmethod void getCartOrderViewTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_Cart__c returnedCart = null;
        
        Test.startTest();
        returnedCart = CCPDCCartDAO.getCartOrderView(String.valueOf(cart.ccrz__EncryptedId__c));
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }

    static testmethod void getCartCouponCodeTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartCoupon__c returnedCoupon = null;
        
        Test.startTest();
        returnedCoupon = CCPDCCartDAO.getCartCouponCode(cart.Id);
        Test.stopTest();
        System.assert(returnedCoupon == null);

    }

    static testmethod void getCartSfidTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_Cart__c returnedCart = null;
        
        Test.startTest();
        returnedCart = CCPDCCartDAO.getCartSfid(String.valueOf(cart.Id));
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }


    static testmethod void getCartForPricingTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_Cart__c returnedCart = null;
        
        Test.startTest();
        returnedCart = CCPDCCartDAO.getCartForPricing(String.valueOf(cart.ccrz__EncryptedId__c));
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);

    }


    static testmethod void getCartCustomTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_Cart__c returnedCart = null;
        
        Test.startTest();
        returnedCart = CCPDCCartDAO.getCartCustom(cart.Id);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);

    }

    static testmethod void getCartItemGroupsForCartTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItemGroup__c newCartItemGroup = new ccrz__E_CartItemGroup__c();
        newCartItemGroup.ccrz__Cart__c = cart.Id;
        insert newCartItemGroup;
        List<ccrz__E_CartItemGroup__c> returnCartItemGroups = null;
        ccrz__E_CartItemGroup__c returnCartItemGroup = null;
        Test.startTest();
        returnCartItemGroups = CCPDCCartDAO.getCartItemGroupsForCart(cart.Id);
        returnCartItemGroup = returnCartItemGroups.get(0);
        Test.stopTest();

        System.assert(returnCartItemGroups != null);
        System.assertEquals(returnCartItemGroup.Id, newCartItemGroup.Id);

    }

    static testmethod void getCartSplitResponseTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Split_Response__c = 'response';
        update cart;

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCPDCCartDAO.getCartSplitResponse(cart.ccrz__EncryptedId__c);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
        System.assertEquals('response', returnedCart.CC_FP_Split_Response__c);
    }

    static testmethod void getCartByIdTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCPDCCartDAO.getCartById(cart.Id);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }
    
    static testmethod void getCartShipToTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCPDCCartDAO.getCartShipTo(cart.ccrz__EncryptedId__c);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }
    
    static testmethod void getFPSkusMediaTest(){
        util.initCallContext();
        ccrz__E_Product__c prod = util.getProduct();
        Set<String> skus = new Set<String>();
        skus.add(prod.ccrz__SKU__c);
        Map<String, ccrz__E_ProductMedia__c> skuMediaMap = new Map<String, ccrz__E_ProductMedia__c>();
        Test.startTest();
        skuMediaMap = CCPDCCartDAO.getFPSkusMedia(skus);
        Test.stopTest();

    }
    
    static testmethod void getZeroPriceCartItemsTest(){
        util.initCallContext();
        List<ccrz__E_CartItem__c> lst = new List<ccrz__E_CartItem__c>();
        ccrz__E_Cart__c cart = util.getCart();
        Test.startTest();
        lst = CCPDCCartDAO.getZeroPriceCartItems(cart.ccrz__EncryptedId__c);
        System.assert(lst != null);
        Test.stopTest();

    }
   
    static testmethod void getCartsFromAccountTest(){
        util.initCallContext();
        Account acc = util.getAccount();
        List<ccrz__E_Cart__c> res = new  List<ccrz__E_Cart__c>();
        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__EffectiveAccountID__c = acc.Id;
        update cart;
        Test.startTest();
        res = CCPDCCartDAO.getCartsFromAccount(cart.ccrz__EffectiveAccountID__c);
        System.assert(res != null);
        Test.stopTest();

    }
    static testmethod void getCartLocationCodeTest(){
        util.initCallContext();
        String res;
        Location__c loc = util.getLocation();
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = loc.Id;
        update cart;
        
        Test.startTest();
        res = CCPDCCartDAO.getCartLocationCode(cart.ccrz__EncryptedId__c);
        System.assert(res != null);
        Test.stopTest();

    }
    //Added By Likhit on 23-09-2020
    static testmethod void getCartItemGroupDetailsForCartTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItemGroup__c newCartItemGroup = new ccrz__E_CartItemGroup__c();
        newCartItemGroup.ccrz__Cart__c = cart.Id;
        insert newCartItemGroup;
        List<ccrz__E_CartItemGroup__c> returnCartItemGroups = null;
        ccrz__E_CartItemGroup__c returnCartItemGroup = null;
        Test.startTest();
        returnCartItemGroups = CCPDCCartDAO.getCartItemGroupDetailsForCart(cart.Id);
        returnCartItemGroup = returnCartItemGroups.get(0);
        Test.stopTest();
        System.assert(returnCartItemGroups != null);
        System.assertEquals(returnCartItemGroup.Id, newCartItemGroup.Id);
	}
    //Added By Likhit on 30-10-2020
    static testmethod void getCartItemsFromSKUTest(){
    	util.initCallContext();
        List<ccrz__E_CartItem__c> lst = new List<ccrz__E_CartItem__c>();
        ccrz__E_Product__c prod = util.getProduct();
        String Sku = prod.ccrz__SKU__c;
        ccrz__E_Cart__c cart = util.getCart();
        Test.startTest();
        lst = CCPDCCartDAO.getCartItemsFromSKU(cart.ccrz__EncryptedId__c,Sku);
        System.assert(lst != null);
        Test.stopTest();
    }
    
    static testmethod void getCartItemGroupDetailsTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__EncryptedId__c = cart.Id;
        update cart;
        ccrz__E_CartItemGroup__c newCartItemGroup = new ccrz__E_CartItemGroup__c();
        newCartItemGroup.ccrz__Cart__c = cart.Id;
        insert newCartItemGroup;
        CCPDCCartDAO.getCartItemGroupDetails(cart.Id);
    }

}