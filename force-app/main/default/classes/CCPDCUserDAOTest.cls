@isTest
public class CCPDCUserDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void queryUsersForAccountsTest(){
        util.initCallContext();
        Account theAccount = util.getAccount();
        User theUser = util.getPortalUser();

        Map<Id, User> data = null;

        Test.startTest();
        data = CCPDCUserDAO.queryUsersForAccounts(new Set<Id> {theAccount.Id});
        Test.stopTest();

        System.assert(data != null);
        System.assert(data.get(theUser.Id) != null);
    }

    static testmethod void queryUserForUserIdTest(){
        util.initCallContext();
        User theUser = util.getPortalUser();

        User data = null;

        Test.startTest();
        data = CCPDCUserDAO.queryUserForUserId(theUser.Id);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(theUser.Id, data.Id);
    }

    static testmethod void queryUserForUserNameTest(){
        util.initCallContext();
        User theUser = util.getPortalUser();

        User data = null;

        Test.startTest();
        data = CCPDCUserDAO.queryUserForUserName(theUser.Username);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(theUser.Username, data.Username);
    }

    static testmethod void queryUserForContactIdTest(){
        util.initCallContext();
        User theUser = util.getPortalUser();
        Contact theContact = util.getContact();

        User data = null;

        Test.startTest();
        data = CCPDCUserDAO.queryUserForContactId(theContact.Id);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(theUser.Id, data.Id);
    }

    static testmethod void queryContactForContactIdTest(){
        util.initCallContext();
        Contact theContact = util.getContact();

        Contact data = null;

        Test.startTest();
        data = CCPDCUserDAO.queryContactForContactId(theContact.Id);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(theContact.Id, data.Id);
    }

    static testmethod void getUsersMapTest(){
        util.initCallContext();
        User theUser = util.getPortalUser();

        Map<Id, User> data = null;

        Test.startTest();
        data = CCPDCUserDAO.getUsersMap(new List<Id> {theUser.Id});
        Test.stopTest();

        System.assert(data != null);
        System.assert(data.get(theUser.Id) != null);
    }
    
     static testmethod void geteffAccountTest(){
        util.initCallContext();
        Account acc =util.getAccount(); 
        Account data = null;
        Test.startTest();
        data = CCPDCUserDAO.geteffAccount(acc.Id);
        Test.stopTest();
        System.assert(data != null);
        System.assert((acc.Id) != null);
    }
    
    static testmethod void getAccOwnerTest(){
        util.initCallContext();
        User theUser = util.getPortalUser();
        User data = null;
        Test.startTest();
        data = CCPDCUserDAO.getAccOwner(theUser.id);
        Test.stopTest();
        System.assert(data != null);
        System.assert((theUser.id) != null);
    }
}