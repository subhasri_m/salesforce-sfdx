public without sharing class CCPDCTestUtil extends CCAviTestUtil {

    public static final String DEFAULT_STOREFRONT = 'DefaultStore';
    public static final String DEFAULT_LOCATION_CODE = 'AVI';
    public static final String ALT_LOCATION_CODE = 'ALT';
    public static final Integer DEFAULT_QUANTITY = 3;

    public override void configureSettings() {
        STOREFRONT = DEFAULT_STOREFRONT;
        COMMUNITY_PROFILE = 'CloudCraze Customer Community User';
        GUEST_USER_PROFILE = 'CloudBurst Customer Community Profile';
        COMMUNITY_NAME = 'CloudBurst Customer Community';
    }

    public override CCAviStorefrontSettings__c createStorefrontSettings(){
        CCAviStorefrontSettings__c settings = new CCAviStorefrontSettings__c(
            Name = STOREFRONT
        );
        return settings;
    } 
    
    public void initCallContext() {
        User portalUser = getPortalUser();
        Account userAccount = getAccount();
        Contact userContact = getContact();
        ccrz__E_Cart__c cart = getCart();
        ccrz.cc_CallContext.storefront = STOREFRONT;
        ccrz.cc_CallContext.currAccount = userAccount;
        ccrz.cc_CallContext.currAccountId = userAccount.Id;
        ccrz.cc_CallContext.currUserId = portalUser.Id;
        ccrz.cc_CallContext.currUser = portalUser;
        ccrz.cc_CallContext.currContact = userContact;
        ccrz.cc_CallContext.currCartId = cart.ccrz__EncryptedId__c;
    }

    public ccrz.cc_RemoteActionContext getRemoteActionContext() {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        User portalUser = getPortalUser();
        ccrz__E_Cart__c cart = getCart();
        ctx.storefront = STOREFRONT;
        ctx.portalUserId = portalUser.Id;
        ctx.currentCartId = cart.ccrz__EncryptedId__c;
        return ctx;
    }


    public override Account createAccount(ccrz__E_AccountGroup__c accountGroup){
        Account act = super.createAccount(accountGroup);
        act.Ship_from_Location__c = DEFAULT_LOCATION_CODE;
        act.ISeries_Customer_Account__c = '2222';
        act.ISeries_Customer_Branch__c = '22';
        return act;
    }

    public override ccrz__E_Product__c createProduct(String name, String sku, String productType, String storefront) {
        ccrz__E_Product__c prod = super.createProduct(name, sku, productType, storefront);
        prod.Part_Number__c = sku;
        prod.Pool_Number__C = '123';
        return prod;
    }

    // create a new cart overload method
    public ccrz__E_Cart__c createNewCart(Account acct, ccrz__E_ContactAddr__c billToAddr, ccrz__E_ContactAddr__c shipToAddr, User portalUser, Contact portalContact, String cartType, String storefront,String encryID){
        String cartName = 'New Shopping Cart';

        ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
            ccrz__Account__c = acct.Id,
            ccrz__ActiveCart__c = false,
            ccrz__BillTo__c = billToAddr.Id,
            ccrz__CartStatus__c = 'Open',
            ccrz__CartType__c = cartType,
            ccrz__Contact__c = portalContact.Id,
            ccrz__Name__c = cartName,
            ccrz__ShipTo__c = shipToAddr.Id,
            ccrz__Storefront__c = storefront,
            ccrz__CurrencyISOCode__c = 'USD',
            ccrz__User__c = portalUser.Id,
            ccrz__CartId__c = 'CartNew' + KEY,
            ccrz__EncryptedId__c = encryID
        );

        return cart;
    }


    public Location__c createLocation(String locationCode){
        Location__c loc = new Location__c();
        loc.Location__c = locationCode;
        loc.Location_Id__c = locationCode;
        loc.Location_Status__c = 'Active' ;
        loc.Location_Type__c ='DC';
        return loc;
    }

    public virtual Location__c getLocation() {
        return [SELECT Id, Name FROM Location__c WHERE Location__c = :DEFAULT_LOCATION_CODE];
    } 

    public ccrz__E_ProductInventoryItem__c createProductInventoryItem(Id productId, String extId, Location__c loc, Integer quantity){

        ccrz__E_ProductInventoryItem__c inv = new ccrz__E_ProductInventoryItem__c();

        inv.Name = extId;
        inv.ccrz__ProductItem__c = productId;
        inv.External_Id__c = extId;
        inv.ccrz__QtyAvailable__c = quantity;
        inv.Location__c = loc.Id;        
        inv.ccrz__InventoryLocationCode__c = loc.Location__c;
        return inv;
    }

    public CC_FP_Contract_Account_Permission_Matrix__c createPermissions(Boolean isAdmin) {
        Account userAccount = getAccount();
        Contact userContact = getContact();
        CC_FP_Contract_Account_Permission_Matrix__c permissions = new CC_FP_Contract_Account_Permission_Matrix__c(
            Account__c = userAccount.Id,
            Allow_Access_To_Accounting_Reports__c = true,
            Allow_Access_to_Sales_Reports__c = true,
            Allow_AR_Payments__c = true,
            Allow_Backorder_Release__c = true,
            Allow_Backorders__c = true,
            Allow_Checkout__c = true,
            Allow_Non_Free_Shipping_Orders__c = true,
            Allow_Override_ShipTo__c = true,
            Can_View_Invoices__c = true,
            Contact__c = userContact.Id,
            Is_Admin__c = isAdmin,
            Maximum_Order_Limit__c = null,
            Pricing_Visibility__c = true             
        );
        return permissions;
    }


    public override Map<String, Object> initData() {
        Map<String, Object> dataMap = super.initData();


        List<Location__c> locList = new List<Location__c>{createLocation(DEFAULT_LOCATION_CODE)};
        locList.add(createLocation(ALT_LOCATION_CODE));
        insert locList;
        dataMap.put('location', locList[0]);
        dataMap.put('altlocation', locList[1]);

        ccrz__E_Product__c prod = (ccrz__E_Product__c)dataMap.get('product');

        List<ccrz__E_ProductInventoryItem__c> invList = new List<ccrz__E_ProductInventoryItem__c>{createProductInventoryItem(prod.Id, 'ext1', locList[0], DEFAULT_QUANTITY)};
        invList.add(createProductInventoryItem(prod.Id, 'ext2', locList[1], DEFAULT_QUANTITY));
        insert invList;
        dataMap.put('inventory', invList[0]);
        dataMap.put('inventoryList', invList);
        User portalUser = getPortalUser();
        Account userAccount = getAccount();
        Contact userContact = getContact();
        ccrz__E_Cart__c cart = createNewCart(userAccount, (ccrz__E_ContactAddr__c)dataMap.get('contactAddress'), (ccrz__E_ContactAddr__c)dataMap.get('contactAddress'), portalUser, userContact, 'Cart', STOREFRONT,'enccart2');
        insert cart;

        dataMap.put('cart2', cart);

        return dataMap;
    }

    public ccrz__E_Cart__c createCartWithSplits(Map<String, Object> dataMap) {
        Account userAccount = (Account) dataMap.get('userAccount');
        Contact userContact = (Contact) dataMap.get('userContact');
        User portalUser = (User) dataMap.get('portalUser');
        Location__c location = (Location__c) dataMap.get('location');
        ccrz__E_ContactAddr__c address = (ccrz__E_ContactAddr__c)dataMap.get('contactAddress');

        ccrz__E_Cart__c cart = createCart(userAccount, address, address, portalUser, userContact, 'Cart', STOREFRONT);
        cart.ccrz__Name__c = 'SplitCart';
        cart.CC_FP_Location__c = location.Id;
        cart.ccrz__CartId__c = 'SplitCart' + KEY;
        cart.ccrz__EncryptedId__c = 'enc1234567890';
        insert cart;

        ccrz__E_CartItemGroup__c group1 = createCartItemGroup(cart, address, 'Shipment-1', 'Carrier', false, 6.00);
        ccrz__E_CartItemGroup__c group2 = createCartItemGroup(cart, address, 'Backorder', null, true, 0);
        insert new List<ccrz__E_CartItemGroup__c>{group1, group2};

        ccrz__E_Product__c product = (ccrz__E_Product__c) dataMap.get('product');
        ccrz__E_CartItem__c cartItem1 = createCartItem(cart, 9.99, 5, product, null);
        cartItem1.ccrz__CartItemGroup__c = group1.Id;
        cartItem1.ccrz__CartItemId__c = 'CartItem1' + KEY;
        ccrz__E_CartItem__c cartItem2 = createCartItem(cart, 9.99, 5, product, null);
        cartItem2.ccrz__CartItemId__c = 'CartItem2' + KEY;
        cartItem2.ccrz__CartItemGroup__c = group1.Id;
        ccrz__E_CartItem__c cartItem3 = createCartItem(cart, 9.99, 10, product, null);
        cartItem3.ccrz__CartItemId__c = 'CartItem3' + KEY;
        cartItem3.ccrz__CartItemGroup__c = group2.Id;
        insert new List<ccrz__E_CartItem__c>{cartItem1, cartItem2, cartItem3};

        return cart;

    }

    public ccrz__E_CartItemGroup__c createCartItemGroup(ccrz__E_Cart__c cart, ccrz__E_ContactAddr__c address, String name, String carrier, Boolean backorder, Decimal shipAmount) {
        ccrz__E_CartItemGroup__c grp = new ccrz__E_CartItemGroup__c(
            ccrz__Cart__c = cart.Id, 
            CC_FP_Is_Backorder__c = backorder,
            ccrz__ShipAmount__c = shipAmount,  
            ccrz__CartItemGroupId__c = name, 
            ccrz__GroupName__c = name, 
            CC_FP_DC_ID__C = DEFAULT_LOCATION_CODE,
            ccrz__RequestDate__c = Date.today(),
            ccrz__ShipMethod__c = carrier,
            ccrz__ShipTo__c = address.Id,
            CC_FP_Subtotal_Amount__c = 0,
            CC_FP_Tax_Amount__c = 0,
            CC_FP_Total_Amount__c = 0
        );
        return grp;
    }

    public ccrz__E_Order__c createOrderWithSplits(Map<String, Object> dataMap, ccrz__E_Cart__c cart) {
        Account userAccount = (Account) dataMap.get('userAccount');
        Contact userContact = (Contact) dataMap.get('userContact');
        User portalUser = (User) dataMap.get('portalUser');
        Location__c location = (Location__c) dataMap.get('location');
        ccrz__E_ContactAddr__c address = (ccrz__E_ContactAddr__c)dataMap.get('contactAddress');

        ccrz__E_Order__c order = createOrder(cart.Id, userAccount.Id, userContact.Id, portalUser.Id, address.Id, address.Id);
        order.ccrz__Name__c = 'SplitOrder';
        order.ccrz__OrderId__c = 'SplitOrder' + KEY;
        order.ccrz__EncryptedId__c = 'onc1234567890';
        insert order;

        ccrz__E_OrderItemGroup__c group1 = createOrderItemGroup(order, address, 'Shipment-1', 'Carrier', 6.00);
        ccrz__E_OrderItemGroup__c group2 = createOrderItemGroup(order, address, 'Backorder', null, 0);
        insert new List<ccrz__E_OrderItemGroup__c>{group1, group2};

        ccrz__E_Product__c product = (ccrz__E_Product__c) dataMap.get('product');
        ccrz__E_CartItem__c cartItem1 = createCartItem(cart, 9.99, 5, product, null);
        ccrz__E_OrderItem__c orderItem1 = createOrderItem(order.Id, 'oi1', 'Major', product.Id, null, null, 9.99, 5, 49.95);
        orderItem1.ccrz__OrderItemGroup__c = group1.Id;
        ccrz__E_OrderItem__c orderItem2 = createOrderItem(order.Id, 'oi2', 'Major', product.Id, null, null, 9.99, 5, 49.95);
        orderItem2.ccrz__OrderItemGroup__c = group1.Id;
        ccrz__E_OrderItem__c orderItem3 = createOrderItem(order.Id, 'oi1', 'Major', product.Id, null, null, 9.99, 10, 99.99);
        orderItem3.ccrz__OrderItemGroup__c = group2.Id;
        insert new List<ccrz__E_OrderItem__c>{orderItem1, orderItem2, orderItem3};

        return order;
    }

    public ccrz__E_OrderItemGroup__c createOrderItemGroup(ccrz__E_Order__c theOrder, ccrz__E_ContactAddr__c address, String name, String carrier, Decimal shipAmount) {
        ccrz__E_ORderItemGroup__c grp = new ccrz__E_OrderItemGroup__c(
            ccrz__Order__c = theOrder.Id, 
            ccrz__ShipAmount__c = shipAmount,  
            ccrz__OrderItemGroupId__c = name, 
            ccrz__GroupName__c = name, 
            ccrz__RequestDate__c = Date.today(),
            ccrz__ShipMethod__c = carrier,
            ccrz__ShipTo__c = address.Id
        );
        return grp;
    }

    public ccrz__E_Order__c getSplitOrder() {
        String theKey = 'SplitOrder' + KEY;

        return [
            SELECT
                Id,
                ccrz__EncryptedId__c
            FROM
                ccrz__E_Order__c
            WHERE
                ccrz__OrderId__c = :theKey
        ];
    }

    public ccrz__E_Cart__c getSplitCart() {
        String theKey = 'SplitCart' + KEY;

        return [
            SELECT
                Id,
                ccrz__EncryptedId__c,
                (SELECT Id FROM ccrz__E_CartItems__r)
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__CartId__c = :theKey
        ];
    }

    public ccrz__E_Product__c createRequiredProduct(ccrz__E_Product__c parent) {

        ccrz__E_Product__c product = createProduct('Product Core', 'product-core', 'Major', STOREFRONT);
        insert product;

        ccrz__E_RelatedProduct__c requiredProduct = new ccrz__E_RelatedProduct__c(
            ccrz__RelatedProduct__c = product.Id,
            ccrz__Product__c = parent.Id,
            ccrz__RelatedProductType__c = 'Required'
        );
        insert requiredProduct;

        return product;
    }


}