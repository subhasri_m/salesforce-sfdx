/**
  * 
    List<Sales_Target__c> stList = [select id from Sales_Target__c limit 10000];
    system.debug('-- get sales target:' + stList.size());
    delete stList;
    // run as many times as needed to clear the old data
    
    Database.executeBatch(new BatchSalesTargetRegen());
    or for test for one record:
    Database.executeBatch(new BatchSalesTargetRegen('a0l4000000Af3Ts'));
    
  *
  **/
global class BatchSalesTargetRegen implements Database.Batchable<sObject>, Database.Stateful {

     global String informaticaUserId = Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
     global Integer totalProcessedNum {get; set;}
     global Integer totalOKNum {get; set;}
     global Integer totalNGNum {get; set;}
     
     public Id oneId {get; set;}
     public BatchSalesTargetRegen() {
     }
     public BatchSalesTargetRegen(Id oneId) {
        this.oneId = oneId;
     }
     
     public Database.QueryLocator start(Database.BatchableContext bc) {
        
        totalProcessedNum = 0;
        totalOKNum = 0;
        totalNGNum = 0;
        
        String msg = 'Batch Sales Target Regen gets started at: ' + Datetime.now();
        Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesTargetRegen',
            Batch_Message__c=msg,
            Type__c = 'INFO'
            );
       //Edited Surabhi, 12/18 
        //insert bl;
        
        // only for the data from last year
        Date sinceDate = Date.newInstance(2015, 1, 1);
        
        String qry = 'select name, Order_Date__c, Account_Owner_ID__c, Sales_Target__c, Sales_Target__r.Start_Date__c, Sales_Target__r.OwnerId from Sales_Order_Summary__c where Account__r.owner.isactive = true and ownerId != :informaticaUserId and Order_Date__c >= :sinceDate ';
        if (!String.isEmpty(oneId)) {
            qry += ' and id = :oneId';
        }
        return Database.getQueryLocator(qry);       
     }
     
     public void execute(Database.BatchableContext bc, List<Sales_Order_Summary__c> sosList) {
     
        Integer processedNum = sosList.size();
        Integer OKNum = 0;
        Integer NGNum = 0;
        
        try {
            
            OKNum = processedNum;
        

            Set<Id> ownerIdSet = new Set<Id>();
            for (Sales_Order_Summary__c sos : sosList) ownerIdSet.add(sos.Account_Owner_ID__c);
            
            SalesOrderSummaryTriggerHandler.runonce = true;
            SalesOrderSummaryTriggerHandler.moveSos(ownerIdSet, sosList);

            String msg = 'Successfully processed: ';
            for(Sales_Order_Summary__c a : sosList) {
                msg += a.id + ',' + a.Name + '; ';
            }

            Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesTargetRegen',
                Batch_Message__c=msg,
                Type__c = 'INFO'
            );
            
            system.debug('-- ok log:' + bl);
            // insert bl;
            
            system.debug('-- insert complete.');
            
        } catch (Exception ex) {
            
            system.debug('-- error log:' + ex);
            
            NGNum = processedNum;
            // Write result to log object
            String msg = 'Error happened : ' + ex.getMessage() + '\n\n';
            msg += ex.getStackTraceString();
            msg += '\n caused by: \n';
            msg += ex.getCause();
            for(Sales_Order_Summary__c a : sosList) {
                msg += a.id + ',' + a.Name + '; ';
            }
            
            Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesTargetRegen',
                Batch_Message__c=msg,
                Type__c = 'ERROR'
            );
            insert bl;
            
        } finally {
            
            system.debug('-- final...:' + totalProcessedNum);
            system.debug('-- final...:' + totalOKNum);
            system.debug('-- final...:' + totalNGNum);
            
            totalProcessedNum += processedNum;
            totalOKNum += OKNum;
            totalNGNum += NGNum;
            
            system.debug('-- final complete.');
            
        }
     }
     
     public void finish(Database.BatchableContext bc) {
        
        
        String msg = 'Record retrieved : ' + totalProcessedNum +
             ', Successfully processed : ' + totalOKNum +
             ', Failed to processed : ' + totalNGNum;
        // Write result to log object
     /*   Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesTargetRegen',
            Batch_Message__c=msg,
            Type__c = 'INFO'
            );
        
        system.debug('-- finish:' + bl);
        
        insert bl;
        */
    
        system.debug('-- finish complete.'); 
     }
      
}