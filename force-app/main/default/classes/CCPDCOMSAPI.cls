/**
 * CCPDCOMSAPI.cls
 *
 * Provides a framework for requesting/refreshing an OAuth access token, executing 
 * API requests to FleetPride's Order Management System (OMS), and committing any updates
 * to Salesforce after all callouts are complete.
 *
 * Usage:
 * 	API consumers should extend CCPDCOMSAbstractAPI and implement the abstract methods.
 *	The controller should instantiate the concrete implementatitons of CCPDCOMSAbstractAPI and
 *	add them to and instance of CCPDCOMSAPI using the queueRequest method. APIs will be executed
 *	in the order they are added to the queue.
 *	The controller should call the executeRequests() method, which will:
 *		Request a new access token or refresh the existing token
 *		Instantiate a new CCAviHttpUtil instance for each API request in the queue, populated
 *		with the 'Authorization: Bearer <token>' header that includes the access token
 *		Call the executeRequest method for the API implementation, passing the CCAviHttpUtil 
 *		instance for each request in the queue. The API implemtations must not perform DML in
 *		the executeRequest method.
 *		Call the commitData method for each API request in the queue. This is where the API
 *		implementations can perform DML to commit data.
 *		Update the CC_PDC_OMS_Settings object with the updated token and related data.
 *
 */
public with sharing class CCPDCOMSAPI {
	private String storefront;

	private List<CCPDCOMSAbstractAPI> omsRequests;
	public CC_PDC_OMS_Settings__c omsSettings {get;private set;}

	public CCPDCOMSAPI(String storefront) {
		this.storefront = storefront;
		omsRequests = new List<CCPDCOMSAbstractAPI>();

        // List<CC_PDC_OMS_Settings__c> listSettings = [SELECT Id, Name, Storefront__c, Auth_URL__c, API_URL__c, Username__c, Password__c, Access_Token__c, Expiry_Date__c, Expiry_Threshold__c, Status_Code__c, Response__c FROM CC_PDC_OMS_Settings__c WHERE Name = :storefront];
        omsSettings = CC_PDC_OMS_Settings__c.getInstance(storefront);
        if(omsSettings == null){
        	OMSException e = new OMSException('CC PDC OMS Settings must be defined.');
        	throw e;
        }
	}

	public void queueRequest(CCPDCOMSAbstractAPI request){
		this.omsRequests.add(request);
	}

	public void executeRequests(){
		loadAccessToken();
		for(CCPDCOMSAbstractAPI req :omsRequests){
		    CCAviHttpUtil httpUtil = new CCAviHttpUtil();
        	httpUtil.addHeader('Authorization', 'Bearer ' + omsSettings.Access_Token__c);
        	req.executeRequest(httpUtil);
 		}

 		// Save the token/OMS Settings
 		update omsSettings;

 		// Save the Request Data
		for(CCPDCOMSAbstractAPI req :omsRequests){
			req.commitData();
		}
	}


	/**
	 * Loads the access token from a custom object. If the token is not available or is expired, 
	 * perform the OAuth requests to retrieve the token
	 *
	 * @return The CCPDC_OMS_Setting__c that contains the OAuth access token
	 */
    private void loadAccessToken() {


        if (omsSettings.Access_Token__c != null && omsSettings.Expiry_Date__c > System.now()) {
            return; // token is in the class omsSettings
        }

        requestAccessToken();
    }

    private void requestAccessToken(){
    	System.debug('requestAccessToken: entry');
        CCAviHttpUtil httpUtil = new CCAviHttpUtil();
        httpUtil.method = CCAviHttpUtil.REQUEST_TYPE.POST;
        httpUtil.requestContentType = CCAviHttpUtil.CONTENT_TYPE.FORM;
        httpUtil.endpoint = omsSettings.Auth_URL__c;
        System.debug('Request endpoint: ' + httpUtil.endpoint);
        String body = 'grant_type=client_credentials&scope=&client_id=' + EncodingUtil.urlEncode(omsSettings.Username__c, 'UTF-8') + '&client_secret=' + EncodingUtil.urlEncode(omsSettings.Password__c, 'UTF-8');
        httpUtil.setBody(body);
        HttpResponse httpResponse = httpUtil.submitRequest();
    	//System.debug('requestAccessToken: response: ' + JSON.serializePretty(httpUtil));
        if (httpUtil.requestErrors) {
            throw new OMSException('Exception requesting token: ' + httpUtil.requestException.getMessage());
        }
        else {
            if (httpResponse.getStatusCode() == 200) {
                AccessToken accessTokenObj = (AccessToken)JSON.deserialize(httpResponse.getBody(), CCPDCOMSAPI.AccessToken.class);
                omsSettings.Access_Token__c = accessTokenObj.access_token;
                omsSettings.Expiry_Date__c = System.now().addSeconds(accessTokenObj.expires_in - omsSettings.Expiry_Threshold__c.intValue());
            }
            else if (httpResponse.getStatusCode() == 401) {
            	throw new OMSException('Exception 401/Unauthorized: Invalid Username or Password');
            }
            else {
            	throw new OMSException('Unknown error requesting access token');
            }
        }
    }

    public class AccessToken {
        public String access_token {get; set;}
        public String token_type {get; set;}
        public Integer expires_in {get; set;}
        public String scope {get; set;}
        public String refresh_token{get; set;}       
        
    }

    public class OMSException extends Exception {
    	public String authError;
    }
}