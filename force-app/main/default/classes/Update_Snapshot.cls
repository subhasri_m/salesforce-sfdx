global class Update_Snapshot implements Database.Batchable<sObject>
{

    String query, value, field;
    
    global Update_Snapshot()
    {
    query = 'SELECT Id, OwnerID, update__c FROM Analytic_Snapshot_Data__c WHERE Date__c = TODAY'; 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       return Database.getQueryLocator(query); 
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        for(sobject s : scope)
            {
                s.put('Update__c', 'Update');
            }
            update scope;
    }
     
    global void finish(Database.BatchableContext BC)
    {
    }
}