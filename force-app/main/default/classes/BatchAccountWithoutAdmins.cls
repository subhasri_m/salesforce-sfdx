global class BatchAccountWithoutAdmins implements database.Batchable<sobject>, schedulable, DataBase.stateful{
    global string finalstr='Accounts with no Admin, AccountNumber, Parent Account, Account Owner\n';
    global Integer count =0;
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new BatchAccountWithoutAdmins());
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        List <CC_FP_Contract_Account_Permission_Matrix__c> adminCAPM = [select Account__c,  Is_Admin__c,Contact__c from CC_FP_Contract_Account_Permission_Matrix__c where Is_Admin__c=true];
        Map <Id, Account> c2AccountsMap = new Map <id, Account>([Select Name, Id from Account where ISeries_Company_Code__c='2']);
        Set<Id> c2AccIds = c2AccountsMap.keySet(); // C2 all account Ids
        set<Id> accwithAdmin = new Set<id>(); // Account having Admins
        set<Id> allAccwithAdmin = new Set<id>(); // All Account having Admins in including Parent Acc
        for (CC_FP_Contract_Account_Permission_Matrix__c CAPM : adminCAPM){
            accwithAdmin.add(CAPM.Account__C);
        }
        
        List<Account> adminAcclst= [Select ParentId,ID, Name from Account where ParentId in : accwithAdmin];
        //  system.debug('Account list with  Admins' + adminAcclst.size());
        for (Account acc : adminAcclst){
            allAccwithAdmin.add(acc.ID);
        }
        allAccwithAdmin.addAll(accwithAdmin);
        // system.debug('All Account Id with  Admin' + allAccwithAdmin.size());
        c2AccIds.removeAll(allAccwithAdmin);
        //   system.debug('Account Ids with No Admin' + c2AccIds.size());  
        count = [select count() from account where id in : c2AccIds];
        return Database.getQueryLocator( [Select  Name, AccountNumber, Parent.Name, Owner.Name from account where id in : c2AccIds ]);
    }
    
    global void execute(Database.BatchableContext BC,List<Account> scope)
    {
        for (Account acc : scope ){
            String recordstr= '"'+acc.Name+'","'+acc.AccountNumber+'","'+acc.Parent.Name+'","'+acc.Owner.Name+'"\n';  
            finalstr += recordstr; 
        }
        
    }
    
    global void finish(Database.BatchableContext BC)
    {
        if (count > 0){
        try{
           // Document> attlst = new List<Document>();
            String[] emaillist;
            if (!test.isRunningTest())
            emaillist = (system.label.AccwithoutAdminEmails).split(';'); 
            else
            emaillist = new String[] {'rahul.bansal@puresoftware.com'};    
            Document doc = new Document (Name = 'list_of_Acc_without_Admins_' + system.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ') + '.csv');
            Folder folder = [select id from Folder where name='Account Without Admins Report' LIMIT 1];
            doc.Description  ='';
            doc.FolderId = folder.Id;
            doc.Body = Blob.valueOf(finalstr);
                                
            insert doc;
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            OrgWideEmailAddress[] owe = [select Id, Address from OrgWideEmailAddress where Address =:System.label.FromEmail];
            String emailbody = 'Hi Team,<br><br> Below is the link to view attachment of "Accounts with no Admins" in PDC. <br>' + URL.getSalesforceBaseUrl().toExternalForm() +'/' + doc.Id;
            emailbody= emailbody + '<br><br>Please navigate to the link and click view file to download and take appropriate actions. <br><br> With Regards,<br> SFDC' ;
            email.setOrgWideEmailAddressId(owe.get(0).Id);
            email.setSubject( 'PDC Report : Accounts with No Admins' );
            email.setToAddresses(emaillist);
            email.setHtmlBody(emailbody);
            messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
           
        }  
        catch(Exception e){
            
            
            system.debug('@@Exception : '+ e.getMessage());
        }
      }
    }
}