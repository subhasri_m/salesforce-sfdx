/*******************************************
Modified 10-22-2015 - CloudSherpas, George Nodalo - Modified controller to display different Dashboard for Sales Directors
*******************************************/

public with sharing class AuthenticatedTableauInterfaceController 
{/*
    private static final String SUPERVISOR = 'Supervisor';
    private static final String WORKBOOK_NAME = 'SalesMaxTSMDashboard_SFDC';
    private static final String VIEW_NAME = 'SalesForceTSMDashboard';
    private static final String VIEW_NAME_SD = 'SalesForceSDDashboard';
  
    public String embeddedURL { get; private set; }
    
    public PageReference doGetURL()
    {
        User user = [Select Salesmax_View_All__c, UserRoleId from User Where Username =: System.userinfo.getusername()];
        Id roleId =  user.UserRoleId;
        String roleName;
        List<UserRole> myRoleList = new List<UserRole>();
        myRoleList = [Select Name FROM UserRole WHERE Id =: roleId LIMIT 1];
        if(myRoleList.size()>0){
            For(UserRole ab : myRoleList){
                
                roleName = ab.Name;

            }

        }
        URL url;
        Map<String,String> params = new Map<String,String>();
        params.put(':tabs', 'no');
        params.put(':toolbar', 'no');
        if(!user.salesmax_view_all__c){
        params.put(SUPERVISOR,UserInfo.getLastName() + '_' + UserInfo.getFirstName());
        }
        
        try
        {
            if(!Test.isRunningTest())
            {
                if(roleName=='Sales Director Central' || roleName=='Sales Director W' || roleName=='Sales Director NE' || roleName=='Sales Director SE'){
                    url = zpf.TableauReportServiceUtil.getEmbeddedReportUrl(WORKBOOK_NAME, VIEW_NAME_SD, params);

                } 
                else{
                    url = zpf.TableauReportServiceUtil.getEmbeddedReportUrl(WORKBOOK_NAME, VIEW_NAME, params);
                }
            }
            else
            {
                url = MockZilliantTableauReportServiceUtil.getEmbeddedReportUrl(WORKBOOK_NAME, VIEW_NAME, params);
            }
        }
        catch(zpf.TableauConnectivityException ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            return null;
        }
        embeddedURL = url.toExternalForm();
        return null;
    }*/
    
}