@isTest
public with sharing class CCPDCShipToControllerHelperTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }
    static testmethod void setDCForAccountTest() {
        util.initCallContext();

		Account account = util.getAccount();		
        
        Test.startTest();
        CCPDCShipToControllerHelper.setDCForAccount(account.Id, 'BB');
        Test.stopTest();
		Account updatedAccount = [SELECT Ship_from_Location__c, Distribution_Center__c FROM Account WHERE Id =: account.Id];
        
        System.assert(updatedAccount != null);
        System.assertEquals(updatedAccount.Ship_from_Location__c, 'BB');
        System.assertEquals(updatedAccount.Distribution_Center__c, 'BB');
    }    
}