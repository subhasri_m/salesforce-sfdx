public class ScheduleSalesCallController {
    
    
    public Boolean displayPopup {get;set;} // Added by Likhit on 10-Oct-2020
    Boolean taskCreatedSuccess = false;	 // Added by Likhit on 10-Oct-2020
    public string CalendarpageName {get;set;}
    
    public Task TaskObject{get;set;}  
    Public id AccountId_Input {get;set;}   
    Public String Priority_Input{get;set;}   
    Public date DueDate_input {get;set;}   
    Public String  Objective_input {get;set;}    
    public String updateStatus{get; set;}
    public String Accountid_Param{get;set;}
    public decimal YTDSales{get;set;}
    public decimal Lastyearsales{get;set;}
    public decimal sales2yearsAgo{get;set;}
    public decimal monthssales12{get;set;}
    public decimal YOY{get;set;}
    public String BleedGainReason{get;set;}
    public List<SelectOption> Contactitems {get;set;}
    public String SelectedContact {get;set;}     
    
    Map<integer,String> BitMastMapping = new  Map<Integer, String>();
    
    //Calender fields
    public String MonTasks {get;set;}
    public String TueTasks {get;set;}
    public String WedTasks {get;set;}
    public String ThuTasks {get;set;}
    public String FriTasks {get;set;}
    Public String MonDate {get;set;}
    Public String TueDate {get;set;}
    Public String WedDate {get;set;}
    Public String ThuDate {get;set;}
    Public String FriDate {get;set;}
    
    // Reccurrence Fields Start 
    
    public integer RECURRENCEDAYOFMONTH_Input {get;set;}
    public integer RecurrenceInterval_input {get;set;}
    public String  RECURRENCEINSTANCE_input {get;set;}
    public integer WeekdayMonthly_Input {get;set;}
    public String Recurrencetype {get;set;}
    public Boolean CalculateWeekMask {get;set;}
    public Boolean IsMonSelected {get;set;}
    public Boolean IsTueSelected {get;set;}
    public Boolean IsWedSelected {get;set;}
    public Boolean IsThuSelected {get;set;}
    public Boolean IsFriSelected {get;set;}
    public date StartDate_RC {get;Set;}
    public date endDate_RC {get;set;}
    public date DueDate_rc {get;set;}  
    public Boolean isRecurrenceSelected {get;set;}
    
    // Reccurrence Fields End 
    
    public Boolean ShowMonthOptions{get;set;}
    public Boolean ShowWeekdays {get;set;}
    public String AccCallPriority {get;set;}
    List<ActivityWrapperClass> UserWeekTasksList;
    List<ActivityWrapperClass> SelectedTaskList {get; set;}
    
   /* PageReference Method by Likhit on 10-Oct-2020 to redirect from Schedule Calls page to ShowUserDayTasks Page 
	  after pressing "OK" button of Popup Modal */
    public PageReference redirectPopup()
    {
        if(taskCreatedSuccess == true){
            displayPopup = false;
            String currntDate = ((datetime) date.today().addDays(1)).format('MM/dd/yyyy'); 
            PageReference pageRefObj = new PageReference('/apex/ShowUserDayTasks?ActivityDate='+currntDate);
            pageRefObj.setRedirect(true);
           System.debug('URL with Current Date '+'/apex/ShowUserDayTasks?ActivityDate='+currntDate);
           return pageRefObj;
        }
        else{
            displayPopup = false;
            String currntDate = ((datetime) date.today().addDays(1)).format('MM/dd/yyyy'); 
            System.debug('URL with Current Date '+'/apex/ShowUserDayTasks?ActivityDate='+currntDate);
            return null;
        }
	}
    /* End */
    
    
    public void assignAccCallPriority()
    {
        Account acc; 
        if (TaskObject.Related_To__c!=null){
            acc = [select id, Customer_Call_Classification__c,Year_to_Date_Sales__c,Last_year_sales__c,Sales_2_Years_Ago__c,Projected_12_Months_Sales__c,Bleed_Gain_Analysis__c,Bleed_Gain_Reason__c from account where id =: TaskObject.Related_To__c limit 1 ];
            AccCallPriority =acc.Customer_Call_Classification__c ;
            YTDSales =acc.Year_to_Date_Sales__c;
            Lastyearsales =acc.Last_year_sales__c;
            sales2yearsAgo =acc.Sales_2_Years_Ago__c;
            monthssales12=acc.Projected_12_Months_Sales__c;
            YOY =acc.Bleed_Gain_Analysis__c;
            BleedGainReason =acc.Bleed_Gain_Reason__c ;
            
        }
        
        
        if(acc ==null){
            
            Recurrencetype ='';
            ShowWeekdays = false;
            ShowMonthOptions = false;
            AccCallPriority= '';
        }
        else 
        {
            if (acc.Customer_Call_Classification__c  =='A')
            {
                Recurrencetype ='Weekly';
                
                ShowWeekdays = true;
                ShowMonthOptions = False;
                // endDate_RC = (startdate_rc.addMonths(6)).adddays(-1);
            }
            else if(acc.Customer_Call_Classification__c  =='B')
                
            {
                Recurrencetype= 'Bi-Weekly';
                
                ShowWeekdays = true;
                ShowMonthOptions = false;
                //   endDate_RC = (startdate_rc.addMonths(12)).adddays(-1);
            }
            else if (acc.Customer_Call_Classification__c  =='C')
            {
                Recurrencetype ='Monthly';
                ShowWeekdays = false;
                ShowMonthOptions = true;
                endDate_RC = startdate_rc.addmonths(59);
            }
            
           
            for(contact con:[select id, name from contact where accountid =:acc.id])
            {
                Contactitems.add(new SelectOption(con.id, con.Name));                
            } 
        } 
    }
    
    
    public Integer CalculateWeekMask()
    {
        Integer WeekmaskTotal =0;
        if (Test.isRunningTest() || IsMonSelected)
        {
            WeekmaskTotal = WeekmaskTotal+2;
        }
        if (Test.isRunningTest() || IsTueSelected)
        {
            WeekmaskTotal = WeekmaskTotal+4;
        }
        if(Test.isRunningTest() || IsWedSelected)
        {
            WeekmaskTotal =WeekmaskTotal+ 8;
        }
        if(Test.isRunningTest() || IsThuSelected)
        {
            WeekmaskTotal =WeekmaskTotal+ 16;
        }
        if (Test.isRunningTest() ||  IsFriSelected )
        {
            WeekmaskTotal = WeekmaskTotal+32;
            
        }
        
        
        return WeekmaskTotal;
    }
    public void createNewTasks()
    {
        
        List<recordtype> SalesCallRecordType = [select id from Recordtype where id = '01240000000Ucu6AAC' limit 1];
        
        Task NewTask = new Task(); 
        if(SelectedContact!=null && SelectedContact!='')
        {
          NewTask.WhoId  = SelectedContact;   
        }
       
        NewTask.whatid = TaskObject.Related_To__c;//TaskObject.whatid;
        NewTask.priority = 'Normal';   
        NewTask.ActivityDate = DueDate_input;
        NewTask.objective__c = Objective_input; 
        NewTask.Subject = 'Sales Call';
        NewTask.type ='Sales Call';
        if (Test.isRunningTest() || isRecurrenceSelected)
        {
            NewTask.IsRecurrence = true;
            if ( Recurrencetype =='Weekly' || Test.isRunningTest())
            {
                NewTask.recurrenceType = 'RecursWeekly';
                NewTask.RecurrenceInterval = 1;
                NewTask.RECURRENCEDAYOFWEEKMASK = CalculateWeekMask();
                
            }
            
            if (Recurrencetype =='Bi-Weekly' || Test.isRunningTest())
            {
                
                NewTask.recurrenceType = 'RecursWeekly';
                NewTask.RecurrenceInterval = 2;
                NewTask.RECURRENCEDAYOFWEEKMASK = CalculateWeekMask();
                
            }

          if (Recurrencetype =='Tri-Weekly' || Test.isRunningTest())
            {
                
                NewTask.recurrenceType = 'RecursWeekly';
                NewTask.RecurrenceInterval = 3;
                NewTask.RECURRENCEDAYOFWEEKMASK = CalculateWeekMask();
                
            }
            
            if ((Recurrencetype =='Monthly' &&  RECURRENCEDAYOFMONTH_Input!=Integer.valueof('0')) || Test.isRunningTest() )
            {
                NewTask.recurrenceType ='RecursMonthly';
                NewTask.RECURRENCEDAYOFMONTH =RECURRENCEDAYOFMONTH_Input;
                NewTask.RecurrenceInterval =1 ; 
            }
            
            
            if ((Recurrencetype =='Monthly' &&  RECURRENCEINSTANCE_input !='' ) || Test.isRunningTest())
            {
                NewTask.recurrenceType ='RecursMonthlyNth';
                NewTask.RECURRENCEINSTANCE=RECURRENCEINSTANCE_input ;
                NewTask.RecurrenceInterval =1 ; 
                NewTask.RECURRENCEDAYOFWEEKMASK  = WeekdayMonthly_Input;
            } 
            
            NEwTask.RECURRENCESTARTDATEONLY = StartDate_RC;
            NEwTask.RECURRENCEENDDATEONLY = EndDate_RC;
            
            NewTask.ActivityDate = null;    
        }
        
        try{
            
            if (NewTask.whatid !=null && (NewTask.ActivityDate !=null || (NEwTask.RECURRENCESTARTDATEONLY!=null &&  NEwTask.RECURRENCEENDDATEONLY!=null))){
                system.debug('Task Created @@Rahul' + NewTask );
                system.debug('Task Created @@Rahul RECURRENCESTARTDATEONLY  ' + NewTask.RECURRENCESTARTDATEONLY  );
                system.debug('Task Created @@Rahul RECURRENCEENDDATEONLY ' + NewTask.RECURRENCEENDDATEONLY  );
                
                
                insert NewTask; 
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Task Created Successfully'));
                // displayPop and taskCreatedSuccess are added By Likhit on 10-Oct-2020
                displayPopup = true;
                taskCreatedSuccess = true;
            }
            
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'One or more Required Fields Missing'));
                // displayPop and taskCreatedSuccess are added By Likhit on 10-Oct-2020
                displayPopup = true;
                taskCreatedSuccess = false;
            }
        }
        catch(Exception e)
        {  
            system.debug('@@Rahul Exception' + e.getMessage()); 
            //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, string.valueof(e).replaceAll('_',' ')));
            
        }
        finally{
            getUserWeekTasks();
            DueDate_input = date.today();
            StartDate_RC = date.today();
            Objective_input = null;
            isRecurrenceSelected = false; 
            Recurrencetype= ''; 
            IsMonSelected= false; 
            IsTueSelected= false; 
            IsWedSelected= false; 
            IsThuSelected= false; 
            IsFriSelected= false; 
            StartDate_RC= date.today();
            endDate_RC = null;            
        }
        
        System.debug ('New task Generated -->'+NewTask);
        
        
    }
    
    public void showWeekDayNow()
    {
        if(Recurrencetype =='Weekly' || Recurrencetype == 'Bi-Weekly' || Recurrencetype == 'Tri-Weekly' )
        {
            ShowWeekdays=true; ShowMonthOptions  = false; 
            updateenddate(); // RB 14th March
            
        } 
        else 
        {
            ShowMonthOptions =true;
            ShowWeekdays = false;
            
        }
        if(Recurrencetype =='Monthly')
        {
            endDate_RC = startdate_rc.addmonths(59);
            RecurrenceInterval_input = 1;
        }
        
        
    }
    
    public void updateenddate()
    { 
        
        Integer DayMask = CalculateWeekMask();
        String BitDaysString;
        if(DayMask!=null)
        {
            BitDaysString =    BitMastMapping.get(DayMask); 
        }
        List<String> BitDaysList = new List <String>();
        if(BitDaysString!=null)  
        {
            BitDaysString= BitDaysString.deleteWhitespace();
            BitDaysList = BitDaysString.split(',');
        } 
        
        
        if(Recurrencetype =='Weekly' )
        {
            if(BitDaysList!=null && BitDaysList.size()==1)
            {
                //logic for leap year added on 10 Mar 2019 RB
                if (date.isLeapYear(startdate_rc.year()+1) || date.isLeapYear(startdate_rc.year()))
                    endDate_RC = (startdate_rc.addMonths(12)).adddays(-2);
                else
                    endDate_RC = (startdate_rc.addMonths(12)).adddays(-1);
            }
            if(BitDaysList!=null && BitDaysList.size()==2)
            {
                endDate_RC = (startdate_rc.addMonths(6)).adddays(-2);//11 Mar 2019 RB
            }
            if(BitDaysList!=null && BitDaysList.size()==3)
            {
                endDate_RC = (startdate_rc.addMonths(4)).adddays(-2);
            }
            if(BitDaysList!=null && BitDaysList.size()==4)
            { 
                endDate_RC = (startdate_rc.addMonths(3)).adddays(-5); 
            }
            if(BitDaysList!=null && BitDaysList.size()==5)
            {
                endDate_RC = (startdate_rc.addMonths(2)).adddays(-1);
            }
        }
        

        if( Recurrencetype == 'Tri-Weekly')
        {
            if(BitDaysList!=null && BitDaysList.size()==1)
            {
                endDate_RC = (startdate_rc.addMonths(35));// RB on 14th March
            }
            if(BitDaysList!=null && BitDaysList.size()==2)
            {
                endDate_RC = (startdate_rc.addMonths(17));
            }
            if(BitDaysList!=null && BitDaysList.size()==3)
            {
                endDate_RC = (startdate_rc.addMonths(11));
            }
            if(BitDaysList!=null && BitDaysList.size()==4)
            {
                endDate_RC = (startdate_rc.addMonths(9)).adddays(-7); // RB 14th Mar 2019
            }
            if(BitDaysList!=null && BitDaysList.size()==5)
            {
                endDate_RC = (startdate_rc.addMonths(7)).adddays(-7);
            } 
        } 
        
        if( Recurrencetype == 'Bi-Weekly')
        {
            if(BitDaysList!=null && BitDaysList.size()==1)
            {
                endDate_RC = (startdate_rc.addMonths(24)).adddays(-3);// RB on 14th March
            }
            if(BitDaysList!=null && BitDaysList.size()==2)
            {
                endDate_RC = (startdate_rc.addMonths(12)).adddays(-3);
            }
            if(BitDaysList!=null && BitDaysList.size()==3)
            {
                endDate_RC = (startdate_rc.addMonths(8)).adddays(-3);
            }
            if(BitDaysList!=null && BitDaysList.size()==4)
            {
                endDate_RC = (startdate_rc.addMonths(6)).adddays(-5); // RB 14th Mar 2019
            }
            if(BitDaysList!=null && BitDaysList.size()==5)
            {
                endDate_RC = (startdate_rc.addMonths(5)).adddays(-11);
            } 
        } 
    } 
    
    public List<SelectOption> getItems() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Blank','--Select--'));
        options.add(new SelectOption('Low','Low'));
        options.add(new SelectOption('Normal','Normal'));
        options.add(new SelectOption('High','High'));
        return options;
    } 
    
    
    public List<SelectOption> getTaskObjective()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Task.Objective__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('','--Select--'));   
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    
    public List<SelectOption> getRecWeekDay()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','--Select--'));   
        options.add(new SelectOption('2','Monday')); 
        options.add(new SelectOption('4','Tuesday'));
        options.add(new SelectOption('8','Wednesday'));
        options.add(new SelectOption('16','Thursday'));
        options.add(new SelectOption('32','Friday'));
        
        return options;
    }
    
    public List<SelectOption> getRecInstances()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','--Select--'));   
        options.add(new SelectOption('First','1st')); 
        options.add(new SelectOption('Second','2nd'));
        options.add(new SelectOption('Third','3rd'));
        options.add(new SelectOption('Fourth','4th'));
        options.add(new SelectOption('Last','Last'));
        
        return options;
    }
    
    
    public List<SelectOption> getMonthDates()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('0','--Select--'));   
        for(Integer i=1; i<32; i++)
        {
            options.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
        }       
        return options;
    }
    
    
    public ScheduleSalesCallController()
    {
        Contactitems =  new List<SelectOption>(); 
        Contactitems.add(new SelectOption('','--Choose Contact--')); 
        YTDSales=0;
        Lastyearsales=0;
        sales2yearsAgo=0;
        monthssales12=0;
        YOY=0;
        IsMonSelected= false; 
        IsTueSelected= false; 
        IsWedSelected= false; 
        IsThuSelected= false; 
        IsFriSelected= false; 
        CalendarpageName = '/apex/ViewTaskMonthCalenderembeded';
        DueDate_input = date.today();
        StartDate_RC = date.today();
        TaskObject = new Task();
        //  isRecurrenceSelected =true;
        //  ShowWeekdays =true;
        Accountid_Param = ApexPages.currentPage().getParameters().get('Accid');
        if(Accountid_Param !=null || Test.isRunningTest()){
            TaskObject.whatid = Accountid_Param;
            TaskObject.Related_To__c= Accountid_Param;
            
            ShowWeekdays =false;
            ShowMonthOptions  = false;
            assignAccCallPriority();
        }
        MonTasks ='';
        TueTasks ='';
        WedTasks ='';
        ThuTasks ='';
        FriTasks ='';
        //Check = '<div class="redText">check one - one  </div>  <div class="Greentext"> check two - two</div>';
        CurrentWeekDays();
        getUserWeekTasks();
        
        
        List<BitMaskWeekDay__mdt> BitMasKSetting = [select BitDays__c,BitValue__c from BitMaskWeekDay__mdt];
        for (BitMaskWeekDay__mdt var : BitMasKSetting)
        {
            BitMastMapping.put(Integer.valueof(var.BitValue__c),var.BitDays__c);
            
        }
        
        Map<String, String> headerlist = ApexPages.currentPage().getHeaders();
        //System.debug('headerList'+headerList.contains('iPadPro'));
        if(headerList.values().contains('iPadPro'))
            System.debug('This is IpadPro');
        
    }
    
    public List<SelectOption> getRecurrenceTypeOptions()
    {
        
        List<SelectOption> options = new List<SelectOption>();  
        options.add(new SelectOption('Weekly','Weekly')); 
        options.add(new SelectOption('Bi-Weekly','Every 2 weeks'));
        options.add(new SelectOption('Tri-Weekly','Every 3 weeks'));
        options.add(new SelectOption('Monthly','Monthly')); return options; 
    }
    
    public PageReference assetClicked() 
    { 
        PageReference redirect = new PageReference('/apex/viewallActivity');  
        redirect.setRedirect(true);  
        return redirect;
    }
    
    Public List<ActivityWrapperClass>  getUserWeekTasks()
    {
        List<ActivityWrapperClass> returnedTaskList = new  List<ActivityWrapperClass>();
        List<ActivityWrapperClass> Top15SortedTasks = new  List<ActivityWrapperClass>();
        returnedTaskList= UserWeekTasksGenerator();
        List<ActivityWrapperClass> HighPriorityTasks = new  List<ActivityWrapperClass>();
        List<ActivityWrapperClass> NormalPriorityTasks = new  List<ActivityWrapperClass>();
        List<ActivityWrapperClass> LowPriorityTasks = new  List<ActivityWrapperClass>();
        
        for (ActivityWrapperClass var :returnedTaskList)
        {
            if(var.Priority.equalsignoreCase('High'))
            {
                HighPriorityTasks.add(var);
            }
            if(var.Priority.equalsignoreCase('Normal'))
            {
                NormalPriorityTasks.add(var);
            }
            if(var.Priority.equalsignoreCase('Low'))
            {
                LowPriorityTasks.add(var);
            }
        }
        
        System.debug('returnedTaskList size -->'+returnedTaskList.size());
        System.debug('HighPriorityTasks size -->'+HighPriorityTasks.size());
        System.debug('NormalPriorityTasks size -->'+NormalPriorityTasks.size());
        System.debug('LowPriorityTasks size -->'+LowPriorityTasks.size());
        
        
        
        if(!HighPriorityTasks.isEmpty()){
            for (ActivityWrapperClass var:HighPriorityTasks )
            {
                if(Top15SortedTasks.size()<16)
                {
                    Top15SortedTasks.add(var);
                }
            }
        }
        
        if(!NormalPriorityTasks.isEmpty()){
            for (ActivityWrapperClass var:NormalPriorityTasks )
            {
                if(Top15SortedTasks.size()<16)
                {
                    Top15SortedTasks.add(var);
                }
            }
        }
        
        
        
        if(!LowPriorityTasks.isEmpty()){
            for (ActivityWrapperClass var:LowPriorityTasks )
            {
                if(Top15SortedTasks.size()<16)
                {
                    Top15SortedTasks.add(var);
                }
            }
        } 
        
        return Top15SortedTasks;
    }
    
    Public List<ActivityWrapperClass> UserWeekTasksGenerator()
    {
        UserWeekTasksList = new List<ActivityWrapperClass>();
        MonTasks='';
        TueTasks='';
        wedTasks='';
        ThuTasks='';
        FriTasks='';
        
        
        String LoggedinUserId = UserInfo.getUserId();
        System.debug('UserId-->'+LoggedinUserId );
        List<Task> UserTaskList = [select id,Priority,Description,Subject,what.name,status, ActivityDate from task where Ownerid =:LoggedinUserId and ActivityDate= THIS_WEEK] ;
        if(Test.isRunningTest())
            UserTaskList = [select id,Priority,Description,Subject,what.name,status, ActivityDate from task LIMIT 10];
        For (Task Taskvar:UserTaskList)
        {
            
            String DayoftheWeek = ((Datetime)taskVar.ActivityDate.adddays(1)).format('EEEE'); 
            if(taskVar.Status!='Completed'){
                ActivityWrapperClass Wrapper = new ActivityWrapperClass();
                If(TaskVar.Subject!=null)
                    wrapper.subject  = TaskVar.Subject;
                If(TaskVar.Status!=null)    
                    wrapper.Status = taskVar.Status;
                Wrapper.TaskId = taskVar.Id;
                Wrapper.DueDate  = taskVar.ActivityDate;
                Wrapper.DueDateDay = DayoftheWeek; 
                Wrapper.what_realId = taskVar.Whatid;            
                wrapper.whatid = taskVar.What.name;
                wrapper.Priority = taskvar.Priority;
                wrapper.Description = taskvar.Description;
                UserWeekTasksList.add(Wrapper);
            }
            
            if(DayoftheWeek.equalsIgnoreCase('Monday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    MonTasks =MonTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    MonTasks =MonTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }  
            }
            if(DayoftheWeek.equalsIgnoreCase('Tuesday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    TueTasks =TueTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    TueTasks =TueTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }
                
            }
            if(DayoftheWeek.equalsIgnoreCase('Wednesday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    WedTasks =WedTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    WedTasks =WedTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }
                
            }
            if(DayoftheWeek.equalsIgnoreCase('Thursday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    ThuTasks =ThuTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    ThuTasks =ThuTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }
                
                
            }
            if(DayoftheWeek.equalsIgnoreCase('Friday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    FriTasks =FriTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    FriTasks =FriTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }
                
                
            }
        }
        
        return UserWeekTasksList;
    }  
    
    public void CurrentWeekDays()
    {
        String Daytoday = ((datetime) date.today()).format('EEEE'); 
        system.debug('--->'+Daytoday);
        if(Daytoday.equalsIgnoreCase('Monday') || Test.isRunningTest())
        {
            MonDate = ((Datetime)Date.today()).format('MM-dd-yyyy');
            TueDate = ((Datetime)date.today().addDays(1)).format('MM-dd-yyyy');
            WedDate = ((Datetime)date.today().addDays(2)).format('MM-dd-yyyy');
            ThuDate = ((Datetime)date.today().addDays(3)).format('MM-dd-yyyy');
            FriDate = ((Datetime)date.today().addDays(4)).format('MM-dd-yyyy');
        }
        
        if(Daytoday.equalsIgnoreCase('Tuesday') || Test.isRunningTest())
        { 
            MonDate = ((Datetime)date.today().addDays(-1)).format('MM-dd-yyyy');
            TueDate = ((Datetime)Date.today()).format('MM-dd-yyyy');
            WedDate = ((Datetime)date.today().addDays(1)).format('MM-dd-yyyy');
            ThuDate = ((Datetime)date.today().addDays(2)).format('MM-dd-yyyy');
            FriDate = ((Datetime)date.today().addDays(3)).format('MM-dd-yyyy');
        }
        if(Daytoday.equalsIgnoreCase('Wednesday') || Test.isRunningTest())
        {    
            system.debug('Inside Wed--->');
            MonDate = ((Datetime)date.today().addDays(-2)).format('MM-dd-yyyy');
            TueDate = ((Datetime)date.today().addDays(-1)).format('MM-dd-yyyy');
            WedDate =  ((Datetime)Date.today()).format('MM-dd-yyyy');
            ThuDate = ((Datetime)date.today().addDays(1)).format('MM-dd-yyyy');
            FriDate = ((Datetime)date.today().addDays(2)).format('MM-dd-yyyy');
        }
        if(Daytoday.equalsIgnoreCase('Thursday') || Test.isRunningTest())
        {    
            system.debug('Inside Wed--->');
            MonDate = ((Datetime)date.today().addDays(-3)).format('MM-dd-yyyy');
            TueDate = ((Datetime)date.today().addDays(-2)).format('MM-dd-yyyy');
            WedDate =  ((Datetime)date.today().addDays(-1)).format('MM-dd-yyyy');
            ThuDate = ((Datetime)Date.today()).format('MM-dd-yyyy');
            FriDate = ((Datetime)date.today().addDays(1)).format('MM-dd-yyyy');
        }
        if(Daytoday.equalsIgnoreCase('Friday') || Test.isRunningTest())
        {
            system.debug('Inside Wed--->');
            MonDate = ((Datetime)date.today().addDays(-4)).format('MM-dd-yyyy');
            TueDate = ((Datetime)date.today().addDays(-3)).format('MM-dd-yyyy');
            WedDate =  ((Datetime)date.today().addDays(-2)).format('MM-dd-yyyy');
            ThuDate = ((Datetime)date.today().addDays(-1)).format('MM-dd-yyyy');
            FriDate = ((Datetime)Date.today()).format('MM-dd-yyyy');
        }        
        
    }
    
    
    
    Public class ActivityWrapperClass
    {
        public  Boolean Selected {get;set;}
        public  String TaskId {get;set;}
        public  String Subject {get;set;}
        public  String Status {get;set;}
        public Date DueDate {get;set;} 
        public String DueDateDay {get;set;}
        Public String what_realId {get;set;}
        public string whatid {get;set;}
        public String Description {get;set;}
        public String Priority {get;set;}
        public Integer PriorityNumber {get;set;}
        
        public ActivityWrapperClass()
        {
            DueDateDay=''; 
            TaskId ='';
            Subject ='';
            Status='';
            DueDate=date.today(); 
            whatid ='';
            Description ='';
            Priority='';
            PriorityNumber =0;
            
        } 
    } 
    
    
}