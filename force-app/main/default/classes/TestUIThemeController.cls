@isTest
public class TestUIThemeController {
    @isTest static void getUITheme_testMethod(){
        String theme = UIThemeController.getUIThemeDescription();
        System.assertEquals(theme, UserInfo.getUiThemeDisplayed()); 
    }
}