@isTest
public class CCPDCPDPInterchangeControllerTest {
	static CCPDCTestUtil util = new CCPDCTestUtil();
	static Map<String, Object> m = new Map<String, Object>();
    ccrz__E_CartItem__c item;
    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            m = util.initData();
        }        
    }
	@isTest static void getRelatedProductsTest() {
		// Implement test code
		List<ccrz__E_Product__c> products = [SELECT Id,ccrz__SKU__c FROM ccrz__E_Product__c WHERE ccrz__SKU__c = 'product-01'];
        System.debug(products);
        ccrz__E_Product__c product1 = products.get(0);
		ccrz__E_Product__c product2 = new ccrz__E_Product__c();
        product2 = products.get(0);
        ccrz__E_RelatedProduct__c relProduct = new ccrz__E_RelatedProduct__c();
        relProduct.ccrz__RelatedProduct__c = product2.Id;
        relProduct.ccrz__Product__c = product1.Id;
        relProduct.ccrz__Enabled__c = true;
        relProduct.ccrz__Sequence__c = 500.00;
        relProduct.ccrz__StartDate__c = Date.today();
        date today = Date.newInstance(2017, 11, 8);
        relProduct.ccrz__EndDate__c = today.addYears(2);
        relProduct.ccrz__RelatedProductType__c = 'Interchange';
        insert relProduct;
        //System.debug(relProduct);
        System.assert(products != null);
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
        
        ctx.queryParams = new Map<String,String>{'sku' => 'product-01'};
        ctx.storefront = 'pdc';
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCPDPInterchangeController.getRelatedProducts(ctx,'product-01');
        Map<String,Object> prodList = (Map<String, Object>)result.data;
        List<Map<String,Object>> returnedData = (List<Map<String,Object>>)prodList.get('interChangeProducts');
        System.debug(returnedData);
        System.assertEquals(String.valueOf(returnedData.get(0).get('SKU')),'product-01');
        System.assert(result.success);
			
	}
    
    	@isTest static void getRelatedProductsTestException() {
            
        List<ccrz__E_Product__c> products = [SELECT Id,ccrz__SKU__c FROM ccrz__E_Product__c WHERE ccrz__SKU__c = 'product-01'];
        ccrz__E_Product__c product1 = products.get(0);
        ccrz__E_Product__c product2 = products.get(0);
        ccrz__E_RelatedProduct__c relProduct = new ccrz__E_RelatedProduct__c();
        relProduct.ccrz__RelatedProduct__c = product2.Id;
        relProduct.ccrz__Product__c = product1.Id;
        relProduct.ccrz__Enabled__c = true;
        relProduct.ccrz__Sequence__c = 500.00;
        relProduct.ccrz__StartDate__c = Date.today();
        date today = Date.newInstance(2017, 11, 8);
        relProduct.ccrz__EndDate__c = today.addYears(2);
        insert relProduct;
        //System.debug(relProduct);
        System.assert(products != null);
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
        
        ctx.queryParams = new Map<String,String>{'sku' => 'product-01'};
        ctx.storefront = 'pdc';
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCPDPInterchangeController.getRelatedProducts(ctx,'product-01');
        }
    
	@isTest static void getCrossReferenceProductsTest() {
		// Implement test code
		List<ccrz__E_Product__c> products = [SELECT Id,ccrz__SKU__c FROM ccrz__E_Product__c WHERE ccrz__SKU__c = 'product-01'];
        System.debug(products);
        ccrz__E_Product__c product1 = products.get(0);
		ccrz__E_Product__c product2 = new ccrz__E_Product__c();
        product2 = products.get(0);
        Product_Cross_Reference__c crossProduct = new Product_Cross_Reference__c();
        crossProduct.Related_to_Product__c = product2.Id;
        crossProduct.Brand_Name__c = 'testName';
        crossProduct.Interchange_Part_Number__c = 'sc1600';
        insert crossProduct;
         Product_Cross_Reference__c crossProduct2 = new Product_Cross_Reference__c();
        crossProduct2.Related_to_Product__c = product2.Id;
        crossProduct2.Brand_Name__c = 'testName';
        crossProduct2.Interchange_Part_Number__c = 'sc16001';
        insert crossProduct2;
        //System.debug(relProduct);
        System.assert(products != null);
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
        ctx.queryParams = new Map<String,String>{'sku' => 'product-01'};
        ctx.storefront = 'parts';
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCPDPInterchangeController.getCrossReferenceProducts(ctx,'product-01');
        Map<String,Object> crossprodList = (Map<String, Object>)result.data;
		System.assert(result.success);
			
	}

}