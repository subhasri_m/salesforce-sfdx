@isTest
public class CCPDCOMSHomeDCAPITest {

    public static final String RESPONSE_STRING = '[{"refName":"illinois-dc","refKey":"b25a9c9964eeff4cccbeb0e24269f80c","displayName":"Illinois-DC","current":true,"keywords":["Illinois-DC"],"tags":[],"auditInfo":{"creationTs":1507170022225,"createUser":"Admin Parts Distributing Co","updateTs":1509637009013,"updateUser":"Praveen Subba Rao"},"uiactions":[],"forced":false,"objRef":false,"doNotAudit":false,"logicallyDeleted":false,"dynAttributes":{},"referenceData":[],"id":"c55d28e9-3689-45c5-bddb-042ab1ca6c0c","address":{"address1":"1501 Remington Blvd","address2":"Suite C","city":"Bolingbrook","state":"IL","zip5":"60490","zip4":"","country":"US","coordinates":{"exact":false,"latitude":41.6515655,"longitude":-88.1388143},"validated":false,"roles":["contact","shipto","billto"]},"fulfillmentCost":0.0,"lineCost":0.0,"shipToCustomer":false,"shipToStore":false,"dropShip":false,"catalogId":"b75bdc50-1ceb-4b04-9c59-f9a65b033aeb","dcNbr":"BB","dc":false,"storeLocation":false,"vendorLocation":false,"allowTransfersFrom":false,"allowTransfersTo":false,"storeNumber":0,"siteType":"DistributionCenter","defaultInventorySource":false,"size":0,"phone":"6785923956","fedExSubscribeErrors":[],"allowManualShipmentCreation":true,"contacts":[],"allowedFulfillmentTypes":{"shipToSite":false,"shipFromSite":false,"pickupAtSite":false,"localDelivery":false,"onSitePurchaseCarryOut":false,"dropShip":false,"eligibleForTransfer":false},"allowedPaymentTypes":{"cash":false,"credit":false,"debitCard":false,"accountCredit":false,"giftCards":false},"allowedSalesTypes":{"saleBasic":false,"salesOrder":false,"layaway":false,"workOrder":false},"addressBooks":{},"parents":[],"fulfillmentServices":[],"timeToAcknowledgeInMin":0,"timeOutReason":"Time Out Rejected","active":true,"dataDomains":["com.pdc"]}]';

    static testmethod void executeRequestsTest() {
        CCPDCOMSAPITest.createOMSSettingsWithToken('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', RESPONSE_STRING));

        List<CCPDCHomeDCResponse> nearestDCList = null;

        Test.startTest();

        CCPDCOMSAPI api = new CCPDCOMSAPI('DefaultStore');
        CC_PDC_OMS_Settings__c settings = api.omsSettings;
        Integer sites = 1;
        CCPDCOMSHomeDCAPI homeDCApi = new CCPDCOMSHomeDCAPI('55901', 1, settings);
        api.queueRequest(homeDCApi);
        api.executeRequests();
        nearestDCList = homeDCApi.nearestDCList;

        Test.stopTest();

        System.assert(nearestDCList != null);
        System.assertEquals(1, nearestDCList.size());
        System.assertEquals('BB', nearestDCList[0].dcNbr);
    }

}