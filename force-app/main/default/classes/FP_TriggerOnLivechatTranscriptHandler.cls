/**
* @description     FP_TriggerOnLivechatTranscriptHandler  Called by FP_TriggerOnLivechatTranscript
* @author          Lalit Arora
* @Company         FP
* @date            29.Nov.2017
*
* HISTORY
* - 29.Nov.2016    Lalit      Created.
*/ 

public class FP_TriggerOnLivechatTranscriptHandler {
    
    List<Live_Agent_Feedback__c> FeedbackList = new List<Live_Agent_Feedback__c>(); 
    List<Live_Agent_Feedback__c> LiveAgentFeedbackupdate = new List<Live_Agent_Feedback__c>();
    
    Public void updateFeedbackRecord(List<LiveChatTranscript> Triggernew)
    {
        set<String> Chatkeys = new set<String>();
        
        for (LiveChatTranscript var : Triggernew)
        {
            Chatkeys.add(var.ChatKey);
        }
        
        FeedbackList = [select id,ChatKeyReference__c,contact__c, account__c,case__c from Live_Agent_Feedback__c where ChatKeyReference__c in :Chatkeys ];
        
        if(!FeedbackList.isEmpty())
        {
            for (LiveChatTranscript LCTvar : Triggernew)
            {
                Live_Agent_Feedback__c LAFupdateRecord = new Live_Agent_Feedback__c();
                
                for (Live_Agent_Feedback__c LAFvar : FeedbackList)
                {
                    if (LCTvar.ChatKey == LAFvar.ChatKeyReference__c)
                    { 
                        LAFupdateRecord.id = LAFvar.id;
                        LAFupdateRecord.Contact__c = LCTvar.ContactId;
                        LAFupdateRecord.Account__c = LCTvar.AccountId;
                        LAFupdateRecord.case__c = LCTvar.caseid;
                        LAFupdateRecord.Live_chat_transcript__c = LCTvar.id;
                        LiveAgentFeedbackupdate.add(LAFupdateRecord);  
                    }
                }                 
            }
        }
        
        If(!LiveAgentFeedbackupdate.isEmpty())
        {
            update LiveAgentFeedbackupdate;
        }
    }
    
}