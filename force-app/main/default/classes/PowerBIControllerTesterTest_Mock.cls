@isTest
global class PowerBIControllerTesterTest_Mock implements httpCalloutMock{
    global HTTPResponse respond(HTTPRequest req) {
        String responseBody = '{'+
            '"expires_on": "1278448832702",'+
            '"access_token": "00Dx0000000BV7z!AR8AQAxo9UfVkh8AlV0Gomt9Czx9LjHnSSpwBMmbRcgKFmxOtvxjTrKW19ye6PE3Ds1eQz3z8jr3W7_VbWmEu4Q8TVGSTHxs",'+
            '"refresh_token": "00Dx0000000BV7z!t9Czx9LjHnSSpwBMmbRcgKFmxOtvxjTrKW19ye6PE3Ds1eQz3z8jr3W7_VbWmEu4Q8TVGSTHxs"'+
        '}';
        HttpResponse response = new HttpResponse();
        response.setStatusCode(200);
        response.setBody(responseBody);
        response.setHeader('Content-Type','application/json');
        return response;
    }
}