public class FP_Batch_CleanupJunkBackorders implements database.Batchable<sObject> {

     public database.QueryLocator start (Database.BatchableContext BC)
    {
        String query = 'select id from ccrz__E_Cart__c where ccrz__CartType__c = \'Backorder\'';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<ccrz__E_Cart__c>scope)
    {
        List<ccrz__E_Cart__c> RecordUpdate = new List<ccrz__E_Cart__c>(); 
        
        for (ccrz__E_Cart__c s : scope){
             ccrz__E_Cart__c record = new ccrz__E_Cart__c();
            record.id = s.id; 
            RecordUpdate.add(record);                  
        }
        
        database.delete(RecordUpdate, false);
        
    }
    
    public void finish(Database.BatchableContext BC)
    {
        
    }
    
}