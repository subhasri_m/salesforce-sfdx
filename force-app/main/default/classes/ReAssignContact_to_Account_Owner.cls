global class ReAssignContact_to_Account_Owner implements Database.Batchable<sObject>
{

    String query, value, field;
    
    global ReAssignContact_to_Account_Owner()
    {
    query = 'SELECT Id, OwnerID, update__c FROM Contact';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       return Database.getQueryLocator(query); 
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        for(sobject s : scope)
            {
                s.put('Update__c', 'Update');
            }
            update scope;
    }
     
    global void finish(Database.BatchableContext BC)
    {
    }
}