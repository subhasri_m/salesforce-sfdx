@isTest
public with sharing class CCFPLocationHelperTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void getLocationForAccountTest() {
        util.initCallContext();

        CCFPLocationHelper.LocationResponse location = null;
        Test.startTest();
        location = CCFPLocationHelper.getLocationForAccount(ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(location != null);
        System.assertEquals(CCPDCTestUtil.DEFAULT_LOCATION_CODE, location.location);
    }
    
    static testmethod void getLocationForId() {
        Location__c location = util.createLocation('RST');
        insert location;

        CCFPLocationHelper.LocationResponse testLocation = null;
        Test.startTest();
        testLocation = CCFPLocationHelper.getLocationForId(location.Id);
        Test.stopTest();

        System.assert(testLocation != null);
        System.assertEquals(location.Id, testLocation.sfid);
    }
}