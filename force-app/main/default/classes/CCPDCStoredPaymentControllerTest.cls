@isTest
public class CCPDCStoredPaymentControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void controllerEditTest() {
        util.initCallContext();
        CCAviCardConnectAPITest.createCardConnectSettings(ccrz.cc_CallContext.storefront);


        ccrz__E_StoredPayment__c payment = CCAviStoredPaymentDAOTest.createStorePayment('100', ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currUserId, ccrz.cc_CallContext.storefront);
        insert payment;

        ApexPages.currentPage().getParameters().put('pid', payment.Id);
        ApexPages.currentPage().getParameters().put('pt', 'cc');
        ApexPages.currentPage().getParameters().put('mode', 'edit');

        CCPDCStoredPaymentController result = null;

        Test.startTest();
        result = new CCPDCStoredPaymentController();
        Test.stopTest();

        System.assert(result != null);
        System.assertEquals(payment.Id, result.storedPaymentId);
        System.assert(result.accountNumber != null);
        System.assert(result.displayName != null);
        System.assert(result.isEnabled);
        System.assertEquals('04', result.expirationMonth);
        System.assertEquals('25', result.expirationYear);
    }

    static testmethod void controllerNewTest() {
        util.initCallContext();
        CCAviCardConnectAPITest.createCardConnectSettings(ccrz.cc_CallContext.storefront);

        ApexPages.currentPage().getParameters().put('pt', 'cc');
        ApexPages.currentPage().getParameters().put('mode', 'new');

        CCPDCStoredPaymentController result = null;

        Test.startTest();
        result = new CCPDCStoredPaymentController();
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.baseURL != null);
        System.assert(result.path != null);
    }

    static testmethod void updateStoredPayment() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        CCAviCardConnectAPITest.createCardConnectSettings(ccrz.cc_CallContext.storefront);

        ccrz__E_StoredPayment__c payment = CCAviStoredPaymentDAOTest.createStorePayment('100', ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currUserId, ccrz.cc_CallContext.storefront);
        insert payment;

        ccrz.cc_RemoteActionResult result = null;

        String data = '{"storedPaymentId":"' + payment.Id + '","expirationMonth":"11","expirationYear":"21","displayName":"test","isEnabled":false}';

        Test.startTest();
        result = CCPDCStoredPaymentController.updateStoredPayment(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    
        ccrz__E_StoredPayment__c updatedPayment = CCAviStoredPaymentDAO.getStoredPayment(payment.Id);
        System.assertEquals(11, updatedPayment.ccrz__ExpMonth__c);
        System.assertEquals(21, updatedPayment.ccrz__ExpYear__c);
        System.assertEquals('test', updatedPayment.ccrz__DisplayName__c);
        System.assert(!updatedPayment.ccrz__Enabled__c);

    }

    static testmethod void createStoredPayment() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        CCAviCardConnectAPITest.createCardConnectSettings(ccrz.cc_CallContext.storefront);
        Test.setMock(HttpCalloutMock.class, new CCAviCardConnectAPITest.CardConnectServiceMock(200, 'OK', CCAviCardConnectAPITest.SUCCESS_RESPONSE));

        ccrz.cc_RemoteActionResult result = null;

        String data = '{"accountNumber":"************1111","token":"9416190845351111","accountType":"cc","expirationMonth":"11","expirationYear":"21","displayName":"test","isEnabled":false}';

        Test.startTest();
        result = CCPDCStoredPaymentController.createStoredPayment(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }
}