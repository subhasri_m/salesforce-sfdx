public class CreatePDFAttachment {
   public static List<String> getContentVersionIds(String recordId, String recordName){
     	List<String> attIds = new List<String>();
        String ids = createAndInsertPdfAsFileToRecord(recordName, recordId);  
        attIds.add(ids);
        return attIds;
    }
    
   public static String createAndInsertPdfAsFileToRecord(String recordName, String recordId ){
        PageReference pdf = Page.RideAlongRenderAsPDF;
        pdf.getParameters().put('id',recordId);
        pdf.setRedirect(true);
        Blob b ;
         if(Test.isRunningTest()){
            b = blob.valueOf('Unit.Test');
        }else{
            b = pdf.getContentAsPDF();
        }
 		ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; 
        conVer.PathOnClient = recordName + '.pdf'; 
        conVer.Title = 'Ride Along '; 
        conVer.VersionData = b; 
        insert conVer;
        //Create ContentDocumentLink
        // First get the content document Id from ContentVersion
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        ContentDocumentLink cDe = new ContentDocumentLink();
        cDe.ContentDocumentId = conDoc;
        cDe.LinkedEntityId = recordId; // you can use objectId,GroupId etc
        cDe.Visibility = 'InternalUsers';
        insert cDe;
       // return contentVersionId
        return string.valueOf(conVer.Id); 
    }
}