@isTest
public class Test_Update_Snapshot{
    static testMethod void Test_Update_Snapshot(){
User user1 = new User(alias = 'ceo', email='admin@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-88888',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTas@testorg.com', profileid = '00e400000013ttZ');
        insert user1;
User user2 = new User(alias = 'ceo2', email='admin2@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-99999',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='admin2@testorg.com', profileid = '00e400000013ttZ');
        insert user2;


Analytic_Snapshot_Data__c asnap = new Analytic_Snapshot_Data__c(Date__c = system.today(), OwnerId = User1.id);
insert asnap;

asnap.update__c = '3-99999';
Update asnap;

Id batchInstanceID = Database.executeBatch(new Update_Snapshot());
 }
}