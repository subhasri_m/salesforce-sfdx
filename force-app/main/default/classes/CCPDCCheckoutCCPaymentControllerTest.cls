@isTest
public class CCPDCCheckoutCCPaymentControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void controllerTest() {
        util.initCallContext();
        CCAviCardConnectAPITest.createCardConnectSettings(ccrz.cc_CallContext.storefront);

        CCPDCCheckoutCCPaymentController result = null;

        Test.startTest();
        result = new CCPDCCheckoutCCPaymentController();
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.baseURL != null);
        System.assert(result.path != null);
    }

    static testmethod void validateCardTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        CCAviCardConnectAPITest.createCardConnectSettings(ccrz.cc_CallContext.storefront);
        Test.setMock(HttpCalloutMock.class, new CCAviCardConnectAPITest.CardConnectServiceMock(200, 'OK', CCAviCardConnectAPITest.SUCCESS_RESPONSE));

        ccrz.cc_RemoteActionResult result = null;

        String data = '{"name":"Todd Mitchell","token":"9416190845351111","expiration":"1221","verificationCode":"222","saveAsStoredPayment":false}';

        Test.startTest();
        result = CCPDCCheckoutCCPaymentController.validateCard(ctx, data);
        CCPDCCheckoutCCPaymentController.isSavedCard(ctx, data);
        CCPDCCheckoutCCPaymentController.isStoredPayment(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

}