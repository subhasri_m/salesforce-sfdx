public with sharing class CCFPVinHelper {

    public static CCVintelligenceAPI.VintelligenceWSResponse getVinDetails(String vin) {
        CCVintelligenceAPI.VintelligenceWSResponse response = null;
        if (vin != null && vin != '') {
            CCVintelligenceAPI.VintelligenceWSRequest request = new CCVintelligenceAPI.VintelligenceWSRequest();
            request.vin = vin;
            response = CCVintelligenceAPI.getVinDetailsWS(request);
        }
        return response;
    }
}