@isTest
public class CCAviCartManagerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void createCartTest() {
        util.initCallContext();

        String data = null;
        Test.startTest();
        data = CCAviCartManager.createCart();
        Test.stopTest();

        System.assert(data != null);
    }

    static testmethod void getActiveCartTest() {
        util.initCallContext();

        Map<String, Object> data = null;
        Test.startTest();
        System.runAs(ccrz.cc_CallContext.currUser){
            CCAviCartManager.createCart();
        }
        data = CCAviCartManager.getActiveCart();
        Test.stopTest();

        System.assert(data != null);
    }

    static testmethod void addToCartTest() {
        util.initCallContext();
        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String, Object> data = null;
        Test.startTest();
        String cartId = CCAviCartManager.getActiveCartId();
        CCAviCartManager.addToCart(cartId, product.Id, product.ccrz__SKU__c, 10);
        data = CCAviCartManager.getCart(cartId);
        Test.stopTest();

        System.assert(data != null);
    }

    static testmethod void addToCartFailTest() {
        util.initCallContext();
        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Exception theException = null;

        Test.startTest();
        String cartId = CCAviCartManager.getActiveCartId();
        try {
            CCAviCartManager.addToCart(cartId, product.Id, 'SKU', 10);
        }
        catch (Exception e) {
            theException = e;
        }
        Test.stopTest();

        System.assert(theException != null);
    }

    static testmethod void removeFromCartTest() {
        util.initCallContext();
        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String, Object> data = null;
        Test.startTest();
        String cartId = CCAviCartManager.getActiveCartId();
        CCAviCartManager.addToCart(cartId, product.Id, product.ccrz__SKU__c, 10);
        data = CCAviCartManager.getCart(cartId);
        List<Map<String, Object>> items = (List<Map<String, Object>>) data.get(CCAviCartManager.CART_ITEMS_KEY);
        String itemId = (String) items[0].get(CCAviCartManager.SFID_KEY);
        CCAviCartManager.removeFromCart(cartId, itemId);
        data = CCAviCartManager.getCart(cartId);
        Test.stopTest();

        System.assert(data != null);
    }

    static testmethod void removeFromCartFailTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c item = util.getCartItem();
        
        Exception theException = null;

        Test.startTest();

        String cartId = CCAviCartManager.getActiveCartId();
        try {
            CCAviCartManager.removeFromCart(item.Id, cart.Id);
        }
        catch (Exception e) {
            theException = e;
        }
        Test.stopTest();

        System.assert(theException != null);
    }

    static testmethod void getCartTest() {
        util.initCallContext();
        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String, Object> data = null;
        Test.startTest();
        String cartId = CCAviCartManager.getActiveCartId();
        CCAviCartManager.addToCart(cartId, product.Id, product.ccrz__SKU__c, 10);
        data = CCAviCartManager.getCart(cartId);
        Test.stopTest();

        System.assert(data != null);
    }

    static testmethod void getXLCartTest() {
        util.initCallContext();
        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String, Object> data = null;
        Test.startTest();
        String cartId = CCAviCartManager.getActiveCartId();
        CCAviCartManager.addToCart(cartId, product.Id, product.ccrz__SKU__c, 10);
        data = CCAviCartManager.getXLCart(cartId);
        Test.stopTest();

        System.assert(data != null);
    }

    static testmethod void cloneCartTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        String data = null;
        Test.startTest();
        data = CCAviCartManager.cloneCart(cart.Id);
        Test.stopTest();

        System.assert(data != null);
    }

}