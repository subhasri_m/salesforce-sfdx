@isTest
public class CCAviTransactionPaymentDAOTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void getStoredPaymentTest() {
        util.initCallContext();

        ccrz__E_Order__c order = util.getOrder();
        order = CCPDCOrderDAO.getOrderDetails(order.Id);

        List<ccrz__E_TransactionPayment__c> data = new List<ccrz__E_TransactionPayment__c> {
            createTransactionPayment(order)
        };
        insert data;

        List<ccrz__E_TransactionPayment__c> payments = null;
        Test.startTest();
        payments = CCAviTransactionPaymentDAO.getTransactionPaymentsForOrder(order.Id);
        Test.stopTest();

        System.assert(payments != null);
        System.assertEquals(1, payments.size());
    }

    public static ccrz__E_TransactionPayment__c createTransactionPayment(ccrz__E_Order__c order) {
        ccrz__E_TransactionPayment__c payment = new ccrz__E_TransactionPayment__c(
            ccrz__Account__c = order.ccrz__Account__c, 
            ccrz__AccountNumber__c = '************1111',
            ccrz__AccountType__c = 'cc',
            ccrz__Amount__c = 0,
            ccrz__CCOrder__c = order.Id,
            ccrz__Contact__c = order.ccrz__Contact__c,
            ccrz__CurrencyISOCode__c = 'USD',
            ccrz__ExpirationMonth__c = 4,
            ccrz__ExpirationYear__c = 25,
            ccrz__RequestAmount__c = 0,
            ccrz__Storefront__c = order.ccrz__Storefront__c,
            ccrz__SubAccountNumber__c = '123',
            ccrz__Token__c = '1234567890',
            ccrz__User__c = order.ccrz__User__c
        );
        return payment;
    }

}