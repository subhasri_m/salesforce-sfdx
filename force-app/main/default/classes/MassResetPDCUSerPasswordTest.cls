@IsTest
public class MassResetPDCUSerPasswordTest {
    
     @testSetup
    public static void generateTestData(){
        User u = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama11.com',
             Username = 'puser000@amamama11.com' + System.currentTimeMillis(),
             CompanyName = 'TEST_SEDE',
             Title = 'title_test',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             Create_Accounts__c = true,
             LocaleSidKey = 'en_US'
        );
        insert u;
    }
      public static testMethod void TestMethodRun(){
        Test.startTest();
          database.executebatch(new MassResetPDCUserPasswords());
           
        Test.stopTest();
    }

}