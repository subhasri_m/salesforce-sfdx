@isTest
public class CCPDCCheckoutOverrideControllerTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = util.STOREFRONT;
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
    }

    static testmethod void checkoutPreprocessTest() {
        util.initCallContext();

        List<String> responses = new List<String>{CCAviBoomiAPITest.TAX_RESPONSE, CCPDCOMSFulfillmentPlanAPITest.OMS_1_RESPONSE};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        cart.CC_FP_Is_Ship_Remainder__c = true;
        cart.CC_FP_Is_Will_Call__c = true;
        update cart;
        
        ccrz.cc_CallContext.currCartId = cart.Id;
        String data = '{"sfid":"' + cart.Id + '"}';

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCCheckoutOverrideController.checkoutPreprocess(ctx, data);
        Test.stopTest();

        System.assert(result != null);
      //  System.assert(result.success);
    }
    
    static testmethod void checkoutPreprocessFPTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        cart.CC_FP_Is_Ship_Remainder__c = true;
        cart.CC_FP_Is_Will_Call__c = true;
        update cart;
        
        ccrz.cc_CallContext.currCartId = cart.Id;
        String data = '{"sfid":"' + cart.Id + '"}';

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCCheckoutOverrideController.checkoutPreprocessFP(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }
    
    static testmethod void getSalesTaxAmountTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
		ccrz.cc_RemoteActionResult result = null;
        
        Test.startTest();
        result = CCPDCCheckoutOverrideController.getSalesTaxAmount(ctx);
        Test.stopTest();

        System.assert(result != null);
    }

}