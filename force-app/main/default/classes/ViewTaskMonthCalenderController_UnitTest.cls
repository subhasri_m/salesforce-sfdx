@isTest
public class ViewTaskMonthCalenderController_UnitTest {
    @testSetup
    public static void testSetup(){
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama11uu.com',
            Username = 'puser000@amamama1177.com' + System.currentTimeMillis(),
            CompanyName = 'TEST_SEDE',
            Title = 'title_test',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Create_Accounts__c = true,
            LocaleSidKey = 'en_US'
        );
        insert u;
        System.runAs(u){
            Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = u.Id,
                                     Customer_Call_Classification__c =  'A');  
            insert acc;
            System.assertNotEquals(null,acc.id)  ;  
            contact con = new contact (lastname = 'checkme',firstname='check',Accountid = acc.id,OwnerId=u.id,Email='test@test.com');    
            insert con;
            Task t = new Task(
                Priority ='High',
                Description='Test',
                Subject = 'Email',
                OwnerId = u.Id,
                WhatId = acc.Id,
                status = 'New',
                ActivityDate = SYSTEM.today()
            );
            insert t;
            Task t1 = new Task(
                Priority ='High',
                Description='Test',
                Subject = 'Email',
                OwnerId = u.Id,
                WhatId = acc.Id,
                status = 'New',
                ActivityDate = SYSTEM.today(),
                Recurring_Task_ID__c = String.valueOf(t.id)
            );
            insert t1;
            
        }
    }
    
    private static testMethod void unitTest_1(){
        ViewTaskMonthCalenderController sch = new ViewTaskMonthCalenderController();
        sch.ScheduleSalesCallClicked();
        sch.redirectToDayView();
    }
}