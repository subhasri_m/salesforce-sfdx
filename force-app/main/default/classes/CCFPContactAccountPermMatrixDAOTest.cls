@isTest
public with sharing class CCFPContactAccountPermMatrixDAOTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void getPermissionsForAccountAndContactTest() {
        util.initCallContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        CC_FP_Contract_Account_Permission_Matrix__c permission = null;
        Test.startTest();
        permission = CCFPContactAccountPermissionMatrixDAO.getPermissionsForAccountAndContact(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id);
        Test.stopTest();

        System.assert(permission != null);
        //System.assertEquals(data[0].Id, permission.Id);
    }

    static testmethod void getPermissionsForAccountTest() {
        util.initCallContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = null;
        Test.startTest();
        permissions = CCFPContactAccountPermissionMatrixDAO.getPermissionsForAccount(ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(permissions != null);
        //System.assert(permissions.size() == 1);
        //System.assertEquals(data[0].Id, permissions[0].Id);
    }

    static testmethod void getAccountsForContactTest() {
        util.initCallContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = null;
        Test.startTest();
        permissions = CCFPContactAccountPermissionMatrixDAO.getAccountsForContact(ccrz.cc_CallContext.currContact.Id);
        Test.stopTest();

        System.assert(permissions != null);
        //System.assert(permissions.size() == 1);
        //System.assertEquals(data[0].Id, permissions[0].Id);
    }
    
    static testmethod void getAdminsForAccountTest() {
        util.initCallContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = null;
        Test.startTest();
        permissions = CCFPContactAccountPermissionMatrixDAO.getAdminsForAccount(ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(permissions != null);
        //System.assert(permissions.size() == 1);
        //System.assertEquals(data[0].Id, permissions[0].Id);
    }

    static testmethod void getPermissionsByIdTest() {
        util.initCallContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        CC_FP_Contract_Account_Permission_Matrix__c permission = null;
        Test.startTest();
        permission = CCFPContactAccountPermissionMatrixDAO.getPermissionsById(data[0].Id);
        Test.stopTest();

        System.assert(permission != null);
        //System.assertEquals(data[0].Id, permission.Id);
    }

    public static CC_FP_Contract_Account_Permission_Matrix__c createPermissions(Id accountId, Id contactId, Boolean isAdmin) {
        CC_FP_Contract_Account_Permission_Matrix__c permissions = new CC_FP_Contract_Account_Permission_Matrix__c(
            Account__c = accountId,
            Allow_Access_To_Accounting_Reports__c = true,
            Allow_Access_to_Sales_Reports__c = true,
            Allow_AR_Payments__c = true,
            Allow_Backorder_Release__c = true,
            Allow_Backorders__c = true,
            Allow_Checkout__c = true,
            Allow_Non_Free_Shipping_Orders__c = true,
            Allow_Override_ShipTo__c = true,
            Can_View_Invoices__c = true,
            Contact__c = contactId,
            Is_Admin__c = isAdmin,
            Maximum_Order_Limit__c = null,
            Pricing_Visibility__c = true             
        );
        return permissions;
    }
  
}