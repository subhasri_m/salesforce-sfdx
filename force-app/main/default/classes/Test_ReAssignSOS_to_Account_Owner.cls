@isTest
public class Test_ReAssignSOS_to_Account_Owner{
    static testMethod void deleteOld_Owner_tasks(){
User user1 = new User(alias = 'ceo', email='admin@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-88888',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTas@testorg.com', profileid = '00e400000013ttZ', Create_Accounts__c = true);
        insert user1;
User user2 = new User(alias = 'ceo2', email='admin2@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-99999',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='admin2@testorg.com', profileid = '00e400000013ttZ');
        insert user2;

    System.runas(user1){
        Account acc = new Account(Name = 'Test Trigger', OwnerId = User1.id);
        insert acc;
        
        Sales_Order_Summary__c sos = new Sales_Order_Summary__c(Account__c= acc.id, OwnerID = User1.id);
        Insert sos;
        
        acc.OwnerId = User2.id;
        Update Acc;
    }

Id batchInstanceID = Database.executeBatch(new ReAssignSOS_to_Account_Owner());
 }
}