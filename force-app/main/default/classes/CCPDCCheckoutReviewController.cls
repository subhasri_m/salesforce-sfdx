global with sharing class CCPDCCheckoutReviewController {
   	@RemoteAction
    global static ccrz.cc_RemoteActionResult updateCart(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        ccrz__E_Cart__c cart;

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            String cartId = ccrz.cc_CallContext.currCartId;

            cart = CCPDCCartDAO.getCartExtra(cartId);
            Map<String, Object> extraData = CCPDCInventoryHelper.createExtraData(cart);

            extraData.put('cartItemList', cart.ccrz__E_CartItems__r);
            List<CCPDCOMSFulFillmentPlanAPI.BackOrderLine> backOrderLines = CCPDCInventoryHelper.createBackOrderLines(cart.ccrz__E_CartItems__r, 0);

            Boolean reserveInventoryFlag = true; // the production value should be 'true'
            Boolean recheckAvailabilityFlag = false;
            cart.Reservation_Id__c = null; // reset reservation id
            cart.CC_FP_Split_Response__c = null; // reset any split
            CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse  fulFilResponse = CCPDCInventoryHelper.checkInventory(
                cart,
                cart.ccrz__E_CartItems__r,
                reserveInventoryFlag,
                recheckAvailabilityFlag,
                false
            );

            response.data = extraData;

            response.success = (!cart.CC_FP_Has_Validation_Message__c);
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
            cart.CC_FP_Validation_Message__c = e.getMessage() + ' => ' + e.getStackTraceString();
        }
        finally {
            if (cart != null) {
                update cart;
                update cart.ccrz__E_CartItems__r;
                CCPDCFulfillmentHelper.updateTax(cart.ccrz__EncryptedId__c);
            }
        }

        return response;
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult updateCartFP(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        ccrz__E_Cart__c cart;

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            String cartId = ccrz.cc_CallContext.currCartId;

            cart = CCPDCCartDAO.getCartExtra(cartId);
            Map<String, Object> extraData = CCPDCInventoryHelper.createExtraData(cart);

            extraData.put('cartItemList', cart.ccrz__E_CartItems__r);
         //   List<CCPDCOMSFulFillmentPlanAPI.BackOrderLine> backOrderLines = CCPDCInventoryHelper.createBackOrderLines(cart.ccrz__E_CartItems__r, 0);

            Boolean reserveInventoryFlag = true; // the production value should be 'true'
            Boolean recheckAvailabilityFlag = false;
            cart.Reservation_Id__c = null; // reset reservation id
            cart.CC_FP_Split_Response__c = null; // reset any split
            response.data = extraData;

            response.success = (!cart.CC_FP_Has_Validation_Message__c);
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
            cart.CC_FP_Validation_Message__c = e.getMessage() + ' => ' + e.getStackTraceString();
        }
        finally {
            if (cart != null) {
                update cart;
                update cart.ccrz__E_CartItems__r;
                CCPDCFulfillmentHelper.updateTax(cart.ccrz__EncryptedId__c);
            }
        }

        return response;
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchCoreCharges(ccrz.cc_RemoteActionContext ctx){           
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);        
        String cartEncId = ccrz.cc_CallContext.currCartId;                
        ccrz__E_Cart__c thecart = CCPDCCartDAO.getCart(cartEncId);
        List<ccrz__E_CartItem__c> cartItems = thecart.ccrz__E_CartItems__r;    
        Map<String, Decimal> coreProductChargesMap = new Map<String, Decimal>();
        Map<String, ccrz__E_CartItem__c> cartItemsMap = new Map<String, ccrz__E_CartItem__c>();        
        Set<Id> cartItemIds = new Set<Id>{ thecart.Id };
        Decimal coreProductCharges = 0;
        
        for(ccrz__E_CartItem__c ci :cartItems){ 
            cartItemIds.add(ci.Id);
            cartItemsMap.put(ci.Id, ci);
        }        

        List<CC_FP_Required_Cart_Item__c> requiredItems = CCPDCRequiredCartItemDAO.getRequiredCartItems(cartItemIds);        
        if (requiredItems != null && requiredItems.size() > 0) {
            
            for(CC_FP_Required_Cart_Item__c rItem : requiredItems) {                
                if(rItem != null){                    
                    if(rItem.CC_Parent_Cart_Item__r != null && rItem.CC_Parent_Cart_Item__r.ccrz__Product__r != null) {
                        //map cartitem product sku to child price    
                        Id ciId = rItem.CC_Parent_Cart_Item__c;
                        ccrz__E_CartItem__c ci = cartItemsMap.get(ciId);                        
                        coreProductCharges = coreProductCharges + (rItem.Price__c * ci.CC_FP_QTY_Shipping__c);

                        String sku = String.valueOf(rItem.CC_Parent_Cart_Item__r.ccrz__Product__r.ccrz__SKU__c);                        
                        coreProductChargesMap.put(sku, rItem.Price__c);
                    }                    
                }                
            }      
        }

        if(coreProductCharges >= 0){
            Map<String,Object> result = new Map<String,Object>();
            result.put('coreProductCharges', coreProductCharges);
            String coreProductChargesMapJSON = JSON.serialize(coreProductChargesMap).unescapeHtml4();
            ccrz.ccLog.log('coreProductChargesMapJSON is ' + coreProductChargesMapJSON);
            result.put('coreProductChargesMap', coreProductChargesMap);
            response.success = true;
            response.data = result;  
        }      

        return response;
    }    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult fetchProductRestrictionsATCheckout(ccrz.cc_RemoteActionContext ctx, String data){           
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);        
        String cartEncId = ccrz.cc_CallContext.currCartId;                
        String accountId = ccrz.cc_CallContext.effAccountId;
        String storefront = ccrz.cc_CallContext.storefront;
        try{
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            String city, state, country, county, zipcode ;

            if (input.get('addressId') != null){
                String addressId = (String) input.get('addressId'); 
                CCAviAddressyAPI.RetrieveResponse detailResponse = CCAviAddressyAPI.retrieveAddress(storefront,  addressId);
                if (detailResponse.success) {
                    city = detailResponse.address.City;
                    state = detailResponse.address.StateCode;
                    zipcode = detailResponse.address.ZipCode.substring(0,5);
                    country = 'US';
                    county = detailResponse.address.County;
                }else{
                   response.data = detailResponse; 
                   return response; 
                } 
            }else{
                city = (String) input.get('city');
                state = (String) input.get('state');
                zipcode = (String) input.get('postalCode');
                country = 'US';
                county = null;
            }
            CCPDCProductRestrictionHelper.ProductRestrictionResult cartProductRestrictions = CCPDCProductRestrictionHelper.checkProductRestrictions(cartEncId, City, State, zipcode, Country,County );
            Map<String,Object> result = new Map<String,Object>();
            //cartProductRestrictionsJSON = JSON.serialize(cartProductRestrictions);    
            result.put('restrictions', cartProductRestrictions);            
            response.data = result;
            response.success = true;
        } catch(Exception e){
            CCAviPageUtils.buildResponseData(response, false,
                                             new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()
                                                 }
                                            );
        } finally{
            ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','fetchCoreCharges');
            ccrz.ccLog.close(response);
        }  
        
        return response;
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult deleteMultipleCartItems(ccrz.cc_RemoteActionContext ctx,  String data){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);        
       	String cartId = ccrz.cc_CallContext.currCartId;
        if (cartId == null) {
                cartId = CCAviCartManager.getActiveCartId();
        }
        try {
           		 List<Object> input = (List<Object>) JSON.deserializeUntyped(data);
            	 List<ccrz.ccApiCart.LineData> cartItems = new List<ccrz.ccApiCart.LineData>();
            	for(Object item : input) {          	   		
               		 ccrz.ccApiCart.LineData cartItem = new ccrz.ccApiCart.LineData();
               		 cartItem.sfid = string.valueOf(item);                
                	 cartItems.add(cartItem);   
                }
            
             	Map<String, Object> request = new Map<String,Object>{
                    ccrz.ccApi.API_VERSION => 6, 
                    ccrz.ccApiCart.CART_ENCID => cartId,
                    ccrz.ccApiCart.LINE_DATA => cartItems
                };
        
                Map<String,Object> outputData = ccrz.ccAPICart.removeFrom(request);
           		response.success  = (Boolean) outputData.get(ccrz.ccAPI.SUCCESS);
    
        } catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
              new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        
        return response;
    }
   
}