@isTest
public class PopNumberOfOSRTrigger_HandlerTest {
    @isTest static void testBatch() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduserdsa@cs.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduserdsa@cs.com');
        insert u;
        User u2 = new User(Alias = 'standt', Email='standarduserasd@testingas.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduserasd@testingas.com', 
                          Title='Outside Sales Representative', ManagerId = u.Id);
        test.startTest();
       	insert u2;
        test.stopTest();
    }	
}