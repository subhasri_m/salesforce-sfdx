@isTest
public class CCPDCSiteAccountRegisterControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

	static testmethod void addAccountRequestDifferentAddressTest() {
		ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
		ctx.storefront = 'pdc';

		String input =
				'{"customerBusinessName":"Pied Piper","typeOfBusiness-options":"corporation","principalOwnersName":"Richard Hendricks ","SsnOrEinNumber":"120938733","resellerTaxID":"123000000","website":"piedpiper.com","businessAddress1":"12345 Madison St","businessAddress2":"Apt:1000","businessCity":"Mountain View","businessState":"CA","businessZipcode":"90291","businessPhoneNumber":"312-232-2222","businessFaxNumber":"312-322-2345","shipAddressBusAddress":"false","businessMailAddress1":"2324 W Sunset Rd","businessMailAddress2":"Apt:1000","mailAddressCity":"San Diego","mailAddressState":"CA","mailAddressZipcode":"90012","detail-type-of-business":"compression algorithm company","years-range":"1 ~ 5 years","accountsPayableSupervisor":"Jared Dunn","supervisorPhoneNumber":"123-456-7890","firstPrincipalName":"Erlich Backman","firstPrincipalRole":"president","firstPrincipalAddress1":"12345 Madison St","firstPrincipalAddress2":"Apt:1000","firstPrincipalCity":"Chicago","firstPrincipalState":"IL","firstPrincipalZipcode":"60606","firstPrincipalPhoneNumber":"300-000-0000","firstPrincipalEmail":"yubo.diao@avionos.com","secondPrincipalName":"Jared Dunn","secondPrincipalRole":"vice-president","secondPrincipalAddress":"12345 Madison St","secondPrincipalAddress2":"Apt:1000","secondPrincipalCity":"Chicago","secondPrincipalState":"IL","secondPrincipalZipcode":"60606","secondPrincipalPhoneNumber":"213-222-2222","secondPrincipalEmail":"diao.yubo@gmail.com","creditOpitions":"false"}';

		ccrz.cc_RemoteActionResult result = null;
		Test.startTest();
		result = CCPDCSiteAccountRegisterController.addAccountRequest(ctx, input);
		Test.stopTest();

		System.assert(result != null);
		System.assert(result.success);
		System.assert(result.data != null);

	}
	static testmethod void addAccountRequestSameAddressTest() {
		ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
		ctx.storefront = 'pdc';

		String input =
				'{"customerBusinessName":"Pied Piper","typeOfBusiness-options":"corporation","principalOwnersName":"Richard Hendricks ","SsnOrEinNumber":"120938733","resellerTaxID":"123000000","website":"piedpiper.com","businessAddress1":"12345 Madison St","businessAddress2":"Apt:1000","businessCity":"Mountain View","businessState":"CA","businessZipcode":"90291","businessPhoneNumber":"312-232-2222","businessFaxNumber":"312-322-2345","shipAddressBusAddress":"true","businessMailAddress1":"2324 W Sunset Rd","businessMailAddress2":"Apt:1000","mailAddressCity":"San Diego","mailAddressState":"CA","mailAddressZipcode":"90012","detail-type-of-business":"compression algorithm company","years-range":"1 ~ 5 years","accountsPayableSupervisor":"Jared Dunn","supervisorPhoneNumber":"123-456-7890","firstPrincipalName":"Erlich Backman","firstPrincipalRole":"president","firstPrincipalAddress1":"12345 Madison St","firstPrincipalAddress2":"Apt:1000","firstPrincipalCity":"Chicago","firstPrincipalState":"IL","firstPrincipalZipcode":"60606","firstPrincipalPhoneNumber":"300-000-0000","firstPrincipalEmail":"yubo.diao@avionos.com","secondPrincipalName":"Jared Dunn","secondPrincipalRole":"vice-president","secondPrincipalAddress":"12345 Madison St","secondPrincipalAddress2":"Apt:1000","secondPrincipalCity":"Chicago","secondPrincipalState":"IL","secondPrincipalZipcode":"60606","secondPrincipalPhoneNumber":"213-222-2222","secondPrincipalEmail":"diao.yubo@gmail.com","creditOpitions":"false"}';

		ccrz.cc_RemoteActionResult result = null;
		Test.startTest();
		result = CCPDCSiteAccountRegisterController.addAccountRequest(ctx, input);
		Test.stopTest();

		System.assert(result != null);
		System.assert(result.success);
		System.assert(result.data != null);

	}
	static testmethod void addAccountRequestCreditTest() {
		ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
		ctx.storefront = 'pdc';

		String input =
				'{"customerBusinessName":"Pied Piper","typeOfBusiness-options":"corporation","principalOwnersName":"Richard Hendricks ","SsnOrEinNumber":"120938733","resellerTaxID":"123000000","website":"piedpiper.com","businessAddress1":"12345 Madison St","businessAddress2":"Apt:1000","businessCity":"Mountain View","businessState":"CA","businessZipcode":"90291","businessPhoneNumber":"312-232-2222","businessFaxNumber":"312-322-2345","shipAddressBusAddress":"true","businessMailAddress1":"2324 W Sunset Rd","businessMailAddress2":"Apt:1000","mailAddressCity":"San Diego","mailAddressState":"CA","mailAddressZipcode":"90012","detail-type-of-business":"compression algorithm company","years-range":"1 ~ 5 years","accountsPayableSupervisor":"Jared Dunn","supervisorPhoneNumber":"123-456-7890","firstPrincipalName":"Erlich Backman","firstPrincipalRole":"president","firstPrincipalAddress1":"12345 Madison St","firstPrincipalAddress2":"Apt:1000","firstPrincipalCity":"Chicago","firstPrincipalState":"IL","firstPrincipalZipcode":"60606","firstPrincipalPhoneNumber":"300-000-0000","firstPrincipalEmail":"yubo.diao@avionos.com","secondPrincipalName":"Jared Dunn","secondPrincipalRole":"vice-president","secondPrincipalAddress":"12345 Madison St","secondPrincipalAddress2":"Apt:1000","secondPrincipalCity":"Chicago","secondPrincipalState":"IL","secondPrincipalZipcode":"60606","secondPrincipalPhoneNumber":"213-222-2222","secondPrincipalEmail":"diao.yubo@gmail.com","creditOpitions":"true","bankName":"Bank","bankAddressline1":"123 Main St","bankAddressline2":"","bankCity":"Chicago","bankState":"IL","bankZipcode":"60606","bankOfficerName":"Joe Bank","bankPhoneNumber":"5555555555","bankerEmail":"bank@bank.com","accountType":"Secret","accountNumber":"123456789","trader1Name":"Joe Trader1","trader1Addressline1":"123 Money St","trader1Addressline2":"","trader1City":"Chicago","trader1State":"IL","trader1Zipcode":"60606","trader1PhoneNumber":"4444444444","trader2Name":"Joe Trader2","trader2Addressline1":"123 Money St","trader2Addressline2":"","trader2City":"Chicago","trader2State":"IL","trader2Zipcode":"60606","trader2PhoneNumber":"4444444444","trader3Name":"Joe Trader3","trader3Addressline1":"123 Money St","trader3Addressline2":"","trader3City":"Chicago","trader3State":"IL","trader3Zipcode":"60606","trader3PhoneNumber":"4444444444","trader4Name":"Joe Trader4","trader4Addressline1":"123 Money St","trader4Addressline2":"","trader4City":"Chicago","trader4State":"IL","trader4Zipcode":"60606","trader4PhoneNumber":"4444444444","creditLimitRequest":"4000.00","purchaseOrder":"true","salesExempt":"true","signedCustomerName":"Need Money","agreeTerms":"true","infoGathering":"true"}';


		ccrz.cc_RemoteActionResult result = null;
		Test.startTest();
		result = CCPDCSiteAccountRegisterController.addAccountRequest(ctx, input);
		Test.stopTest();

		System.assert(result != null);
		System.assert(result.success);
		System.assert(result.data != null);

	}
	static testmethod void addAccountRequestErrorTest(){
		ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
		ctx.storefront = 'pdc';

		String input = '{}';
		ccrz.cc_RemoteActionResult result = null;

		Test.startTest();
		result = CCPDCSiteAccountRegisterController.addAccountRequest(ctx, input);
		Test.stopTest();

		System.assert(result.success == false);
	}
	static testmethod void doUploadAttachmentTest(){
     	Account a = util.getAccount();
		String result = null;

		Test.startTest();
		result = CCPDCSiteAccountRegisterController.doUploadAttachment(a.Id, 'body', 'stuff', null);
		Test.stopTest();

		System.assert(result != null);
	}

}