public with sharing class CCPDCFulfillmentHelper {

    private Static String PARTS_STORE = 'parts';
    
    public static void doSplits(ccrz__E_Cart__c cart, CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse response) {

        if (cart != null && response != null) {

            // Remove old groups
            List<ccrz__E_CartItemGroup__c> groups = CCPDCCartDAO.getCartItemGroupsForCart(cart.Id);
            if (groups != null && !groups.isEmpty()) {
                delete groups;
            }

            groups = new List<ccrz__E_CartItemGroup__c>();
            ccrz__E_CartItemGroup__c backorderGroup = null;
            Map<Integer, ccrz__E_CartItem__c> itemMap = new Map<Integer, ccrz__E_CartItem__c>();

            List<ccrz__E_CartItem__c> items = new List<ccrz__E_CartItem__c>();
            for (ccrz__E_CartItem__c item : cart.ccrz__E_CartItems__r) {
                Integer lineNumber = Integer.valueOf(item.CC_FP_Line_Number__c);
                itemMap.put(lineNumber, item);
            }

            List<CCPDCOMSFulfillmentPlanAPI.Shipment> shipments = response.shipments;
            List<CCPDCOMSFulfillmentPlanAPI.BackOrderLine> backOrderLines = response.backOrderLines;

            Integer count = 1;
            if (shipments != null && !shipments.isEmpty()) {
                for (CCPDCOMSFulfillmentPlanAPI.Shipment shipment : shipments) {
                    String groupId = cart.ccrz__EncryptedId__c + '-' + count;
                    String name = 'Shipment-' + count;
                    count++;
                    ccrz__E_CartItemGroup__c grp = new ccrz__E_CartItemGroup__c(
                        ccrz__Cart__c = cart.Id, 
                        ccrz__ShipAmount__c = shipment.costToCustomer,  
                        ccrz__CartItemGroupId__c = groupId, 
                        ccrz__GroupName__c = name, 
                        CC_FP_DC_ID__C = shipment.dc,
                        ccrz__RequestDate__c = Date.today(),
                        ccrz__ShipMethod__c = shipment.carrier,
                        ccrz__ShipTo__c = cart.ccrz__ShipTo__c,
                        CC_FP_Subtotal_Amount__c = 0,
                        CC_FP_Tax_Amount__c = 0,
                        CC_FP_Total_Amount__c = 0
                    );
                    groups.add(grp);
                }
            }

            if (backOrderLines != null && !backOrderLines.isEmpty()) {
                String groupId = cart.ccrz__EncryptedId__c + '-' + count;
                String name = 'Backorder-' + count;
                backorderGroup = new ccrz__E_CartItemGroup__c(
                    ccrz__Cart__c = cart.Id, 
                    ccrz__ShipAmount__c = 0,  
                    ccrz__CartItemGroupId__c = groupId, 
                    ccrz__GroupName__c = name, 
                    CC_FP_Is_Backorder__c = true,
                    ccrz__RequestDate__c = Date.today(),
                    ccrz__ShipMethod__c = 'backorder',
                    ccrz__ShipTo__c = cart.ccrz__ShipTo__c
                );
                groups.add(backorderGroup);
            }

            if (!groups.isEmpty()) {
                insert groups;
            }
            
            Integer index = 0;
            if (shipments != null) {
                for (CCPDCOMSFulfillmentPlanAPI.Shipment shipment : shipments) {
                    ccrz__E_CartItemGroup__c grp = groups[index];
                    index++;
                    List<CCPDCOMSFulfillmentPlanAPI.Orderline> lines = shipment.lines;
                    Decimal subtotal = 0;
                    for (CCPDCOMSFulfillmentPlanAPI.Orderline line : lines) {
                        ccrz__E_CartItem__c item = itemMap.remove(line.orderLineNo);
                        if (item != null) {
                            if (item.ccrz__Quantity__c > line.quantity) { // Split the Cart Item
                                ccrz__E_CartItem__c newItem = item.clone();
                                newItem.ccrz__Quantity__c = item.ccrz__Quantity__c - line.quantity;
                                newItem.ccrz__Price__c = item.ccrz__Price__c;                                
                                newItem.ccrz__SubAmount__c = newItem.ccrz__Price__c * newItem.ccrz__Quantity__c;
                                itemMap.put(line.orderLineNo, newItem);

                                item.ccrz__Quantity__c = line.quantity;
                                item.ccrz__SubAmount__c = item.ccrz__Price__c * item.ccrz__Quantity__c;
                            }
                            item.ccrz__CartItemGroup__c = grp.Id;
                            items.add(item);
                            subtotal += item.ccrz__SubAmount__c;
                        }
                        else {
                            throw new FulfillmentException('Unable to find Cart Item for orderLineNo: ' + line.orderLineNo);
                        }
                    } 
                    grp.CC_FP_Subtotal_Amount__c = subtotal; 
                    if (grp.ccrz__ShipAmount__c == null) {
                        grp.ccrz__ShipAmount__c = 0;
                    }
                    if (cart.CC_FP_Tax_Rate__c != null && cart.ccrz__TaxExemptFlag__c != true) {
                        //grp.CC_FP_Tax_Amount__c = grp.CC_FP_Subtotal_Amount__c * cart.CC_FP_Tax_Rate__c;
                        grp.CC_FP_Tax_Amount__c = cart.ccrz__TaxAmount__c;
                    }
                    grp.CC_FP_Total_Amount__c = grp.CC_FP_Subtotal_Amount__c + grp.CC_FP_Tax_Amount__c + grp.ccrz__ShipAmount__c; 
                }
            }

            if (backOrderLines != null) {

                Map<String, Integer> backorderMapByPartNo = new Map<String, Integer>();
                for (CCPDCOMSFulfillmentPlanAPI.BackOrderLine backorder : backOrderLines) {
                    backorderMapByPartNo.put(backorder.partNo, backorder.orderlineNo);
                }

                Map<Integer, ccrz__E_CartItem__c> backorderMap = new Map<Integer, ccrz__E_CartItem__c>();
                for (Integer key : itemMap.keySet()) {
                    ccrz__E_CartItem__c item = itemMap.get(key);
                    Integer lineNumber = Integer.valueOf(item.CC_FP_Backorder_Line_Number__c);
                    if (lineNumber == NULL) {
                        lineNumber = backorderMapByPartNo.get(item.ccrz__Product__r.ccrz__Sku__c);
                    }
                    backorderMap.put(lineNumber, item);
                }

                ccrz__E_CartItemGroup__c grp = backorderGroup;
                for (CCPDCOMSFulfillmentPlanAPI.BackOrderLine backorder : backOrderLines) {
                    ccrz__E_CartItem__c item = backorderMap.remove(backorder.orderLineNo);
                    if (item != null) {
                        item.ccrz__CartItemGroup__c = grp.Id;
                        item.ccrz__SubAmount__c = 0;
                        items.add(item);
                    }
                    else {
                        throw new FulfillmentException('Unable to find Cart Item for PartNo: ' + backorder.PartNo);
                    }
                }
            }

            if (!groups.isEmpty()) {
                update groups;
            }
            if (!items.isEmpty()) {
                upsert items;
            }
        }
    }
	
    public static void doSplitsFP(ccrz__E_Cart__c cart) {
		List<String> fulfillmentlst = new List<String>();
        fulfillmentlst.add('PickUp'); // more to be added later
        if (cart != null && fulfillmentlst != null) {

            // Remove old groups
            List<ccrz__E_CartItemGroup__c> groups = CCPDCCartDAO.getCartItemGroupsForCart(cart.Id);
            if (groups != null && !groups.isEmpty()) {
                delete groups;
            }

            groups = new List<ccrz__E_CartItemGroup__c>();
			List<ccrz__E_CartItem__c> items = new List<ccrz__E_CartItem__c>();
            Integer count = 1;
            if (!fulfillmentlst.isEmpty()) {
                for (String fulfillment : fulfillmentlst) {
                    String groupId = cart.ccrz__EncryptedId__c + '-' + count;
                    String name = 'PickUp-' + count;
                    count++;
                    ccrz__E_CartItemGroup__c grp = new ccrz__E_CartItemGroup__c(
                        ccrz__Cart__c = cart.Id, 
                      //  ccrz__ShipAmount__c = shipment.costToCustomer,  
                        ccrz__CartItemGroupId__c = groupId, 
                        ccrz__GroupName__c = name, 
                        CC_FP_DC_ID__C = cart.CC_FP_Location__r.location__c, // Branch Id 
                        ccrz__RequestDate__c = Date.today(),
                        ccrz__ShipMethod__c = fulfillment,
                       // ccrz__ShipTo__c = cart.ccrz__ShipTo__c, // need to update later
                        CC_FP_Subtotal_Amount__c = 0,
                        CC_FP_Tax_Amount__c = 0,
                        CC_FP_Total_Amount__c = 0
                    );
                    groups.add(grp);
                }
            }

            if (!groups.isEmpty()) {
                insert groups;
            }
            
            Integer index = 0;
            if (fulfillmentlst != null) {
                for (String fulfillment : fulfillmentlst) {
                    ccrz__E_CartItemGroup__c grp = groups[index];
                    index++;
                    Decimal subtotal = 0;
                    for (ccrz__E_CartItem__c line : cart.ccrz__E_CartItems__r) {
                       		line.ccrz__CartItemGroup__c = grp.Id;
                            items.add(line);
                            subtotal += line.ccrz__SubAmount__c;
                        	grp.CC_FP_Subtotal_Amount__c = subtotal;
                    }
                    if (cart.CC_FP_Tax_Rate__c != null && cart.ccrz__TaxExemptFlag__c != true) {
                        grp.CC_FP_Tax_Amount__c = cart.ccrz__TaxAmount__c;
                    }
                    grp.CC_FP_Total_Amount__c = grp.CC_FP_Subtotal_Amount__c + grp.CC_FP_Tax_Amount__c; 
                }
            }

            if (!groups.isEmpty()) {
                update groups;
            }
            if (!items.isEmpty()) {
                upsert items;
            }
        }
    }
    
    public static void copyCartToOrderData(String orderId) {

        ccrz__E_Order__c order = CCPDCOrderDAO.getOrderCustom(orderId);
        if (order != null) {
            order.ccrz__OrderId__c = order.ccrz__Storefront__c == 'parts'? order.Name : order.Id;
            ccrz__E_Cart__c cart = CCPDCCartDAO.getCartCustom(order.ccrz__OriginatedCart__c);
            if (cart != null) {
                order.CC_FP_Free_Shipping_Type__c = cart.CC_FP_Free_Shipping_Type__c;
                order.CC_FP_Is_Free_Shipping__c = cart.CC_FP_Is_Free_Shipping__c;
                order.CC_FP_Is_Ship_Remainder__c = cart.CC_FP_Is_Ship_Remainder__c;
                order.CC_FP_Is_Will_Call__c = cart.CC_FP_Is_Will_Call__c;
                order.CC_FP_Location__c = cart.CC_FP_Location__c;
                order.CC_FP_Reservation_Id__c = cart.Reservation_Id__c;
                order.CC_FP_Tax_Rate__c = cart.CC_FP_Tax_Rate__c;
                order.ccrz__PONumber__c = cart.ccrz__PONumber__c;
                order.Order_Comments__c = cart.Order_Comments__c;
                update order;

                Map<String, ccrz__E_CartItemGroup__c> groupMap = new Map<String, ccrz__E_CartItemGroup__c>();
                if (cart.ccrz__E_CartItemGroups__r != null && !cart.ccrz__E_CartItemGroups__r.isEmpty()) {
                    for (ccrz__E_CartItemGroup__c g : cart.ccrz__E_CartItemGroups__r) {
                        groupMap.put(g.ccrz__GroupName__c, g);
                    }

                    if (order.ccrz__E_OrderItemGroups__r != null && !order.ccrz__E_OrderItemGroups__r.isEmpty()) {
                        Integer index = 1;
                        for (ccrz__E_OrderItemGroup__c g : order.ccrz__E_OrderItemGroups__r) {
                            g.ccrz__OrderItemGroupId__c = order.ccrz__Storefront__c == 'parts'? g.Name : g.Id;
                            ccrz__E_CartItemGroup__c c = groupMap.get(g.ccrz__GroupName__c);
                            if (c != null) {
                                g.CC_FP_Is_Backorder__c = c.CC_FP_Is_Backorder__c;
                                g.CC_FP_DC_ID__c = c.CC_FP_DC_ID__c;
                                g.CC_FP_Subtotal_Amount__c = c.CC_FP_Subtotal_Amount__c;
                                g.CC_FP_Tax_Amount__c = c.CC_FP_Tax_Amount__c;
                                g.CC_FP_Total_Amount__c = c.CC_FP_Total_Amount__c;
                            }
                        }
                        update order.ccrz__E_OrderItemGroups__r;
                    }
                }

                Map<String, List<ccrz__E_CartItem__c>> itemMap = new Map<String, List<ccrz__E_CartItem__c>>();
                if (cart.ccrz__E_CartItems__r != null && !cart.ccrz__E_CartItems__r.isEmpty()) {
                    for (ccrz__E_CartItem__c i : cart.ccrz__E_CartItems__r) {
                        List<ccrz__E_CartItem__c> itemList = itemMap.get(i.ccrz__Product__c);
                        if (itemList == null) {
                            itemList = new List<ccrz__E_CartItem__c>();
                            itemMap.put(i.ccrz__Product__c, itemList);
                        }
                        itemList.add(i);
                    }

                    Map<Id, List<CC_FP_Required_Cart_Item__c>> requiredItemMap = new Map<Id, List<CC_FP_Required_Cart_Item__c>>();
                    if (cart.CC_FP_Required_Cart_Items__r != null && !cart.CC_FP_Required_Cart_Items__r.isEmpty()) {
                        for (CC_FP_Required_Cart_Item__c item : cart.CC_FP_Required_Cart_Items__r) {
                            List<CC_FP_Required_Cart_Item__c> requiredList = requiredItemMap.get(item.CC_Parent_Cart_Item__c);
                            if (requiredList == null) {
                                requiredList = new List<CC_FP_Required_Cart_Item__c>();
                                requiredItemMap.put(item.CC_Parent_Cart_Item__c, requiredList);
                            }
                            requiredList.add(item);
                        }
                    }

                    if (order.ccrz__E_OrderItems__r != null && !order.ccrz__E_OrderItems__r.isEmpty()) {
                        List<CC_FP_Required_Order_Item__c> requiredOrderItemList = new List<CC_FP_Required_Order_Item__c>();
                        for (ccrz__E_OrderItem__c i : order.ccrz__E_OrderItems__r) {
                            i.ccrz__OrderItemId__c = order.ccrz__Storefront__c == 'parts' ? i.Name : i.Id;
                            List<ccrz__E_CartItem__c> itemList = itemMap.get(i.ccrz__Product__c);
                            if (itemList != null) {
                                ccrz__E_CartItem__c item = null;
                                Integer index = 0;
                                for (ccrz__E_CartItem__c ci : itemList) {
                                    if (ci.ccrz__Quantity__c == i.ccrz__Quantity__c && 
                                        i.ccrz__OrderItemGroup__r != null &&
                                        ci.ccrz__CartItemGroup__r != null &&
                                        i.ccrz__OrderItemGroup__r.ccrz__GroupName__c == ci.ccrz__CartItemGroup__r.ccrz__GroupName__c) {
                                        item = ci;
                                        break;
                                    }
                                    index++;
                                }
                                if (item != null) {
                                    itemList.remove(index);
                                    i.CC_FP_PONumber__c = item.CC_FP_PONumber__c;
                                    List<CC_FP_Required_Cart_Item__c> requiredList = requiredItemMap.get(item.Id);
                                    if (requiredList != null && !requiredList.isEmpty()) {
                                        for (CC_FP_Required_Cart_Item__c rci : requiredList) {

                                            CC_FP_Required_Order_Item__c ritem = new CC_FP_Required_Order_Item__c(
                                                CC_Order__c = order.Id,
                                                CC_Product__c = rci.CC_Product__c,
                                                CC_Parent_Order_Item__c = i.Id,
                                                Quantity__c = rci.Quantity__c,
                                                Price__c = rci.Price__c,
                                                SubAmount__c = rci.SubAmount__c
                                            );

                                            requiredOrderItemList.add(ritem);
                                        }
                                    }
                                }
                            }
                        }
                        update order.ccrz__E_OrderItems__r;
                        if (!requiredOrderItemList.isEmpty()) {
                            insert requiredOrderItemList;
                        }
                    }
                }

            }
        }
    }

    public static List<ccrz__E_TransactionPayment__c> doAuth(String orderId, List<ccrz__E_TransactionPayment__c> payments) {
        List<ccrz__E_TransactionPayment__c> newPayments = new List<ccrz__E_TransactionPayment__c>();
        if (payments != null && !payments.isEmpty() && payments.size() == 1) {
            ccrz__E_TransactionPayment__c basePayment = payments[0];
            if (basePayment.ccrz__AccountType__c == 'cc' && basePayment.ccrz__Token__c != null) {
                ccrz__E_Order__c order = CCPDCOrderDAO.getOrderCustom(orderId);
                if (order.ccrz__E_OrderItemGroups__r != null && !order.ccrz__E_OrderItemGroups__r.isEmpty()) {
                    for (ccrz__E_OrderItemGroup__c g : order.ccrz__E_OrderItemGroups__r) {
                        if (g.CC_FP_Is_Backorder__c != true) {
                            ccrz__E_TransactionPayment__c payment = copyTransactionPayment(basePayment);
                            String month = String.valueOf(basePayment.ccrz__ExpirationMonth__c);
                            if (month.length() == 1) {
                                month = '0' + month;
                            }
                            String year = String.valueOf(basePayment.ccrz__ExpirationYear__c);
                            if (year.length() == 1) {
                                year = '0' + year;
                            }

                            String expiration = month + year;
                            String amount = String.valueOf(g.CC_FP_Total_Amount__c);
                            CCAviCardConnectAPI.CardConnectResponse authResponse;
                            if(basePayment.ccrz__Storefront__c.equals(PARTS_STORE)){
                            	authResponse = CCAviCardConnectAPI.fpAuthorization(basePayment.ccrz__Storefront__c, basePayment.ccrz__Token__c, expiration, null, null, amount, g.Name, 
                                                                                   order.ccrz__BillTo__r.ccrz__AddressFirstline__c + ' ' + order.ccrz__BillTo__r.ccrz__AddressSecondline__c, order.ccrz__BillTo__r.ccrz__City__c,
                                                                                   order.ccrz__BillTo__r.ccrz__State__c, order.ccrz__BillTo__r.ccrz__Country__c, order.ccrz__BillTo__r.ccrz__PostalCode__c);
                            }else{
                            	authResponse = CCAviCardConnectAPI.authorization(basePayment.ccrz__Storefront__c, basePayment.ccrz__Token__c, expiration, null, null, amount, g.Name);                       
                            }
                            payment.CC_FP_Authorization_Response__c = authResponse.body;
                            payment.ccrz__TransactionTS__c = DateTime.now();
                            payment.CC_Order_Item_Group__c = g.Id;                            
                            payment.ccrz__RequestAmount__c = g.CC_FP_Total_Amount__c;
                            if (authResponse.success == true) {
                                payment.ccrz__Amount__c = Decimal.valueOf(authResponse.data.amount);
                                payment.ccrz__TransactionCode__c = authResponse.data.retref;
                                
                            } else {
                                  payment.ccrz__TransactionCode__c = 'Authorization Failed';  
                                  order.ccrz__OrderStatus__c = 'Failed';
                                  update order;
                            } 
                            newPayments.add(payment);
                        }
                    }
                } 

            }
        }
        return newPayments;
    }
    
    

    public static ccrz__E_TransactionPayment__c copyTransactionPayment(ccrz__E_TransactionPayment__c base) {
        ccrz__E_TransactionPayment__c payment = new ccrz__E_TransactionPayment__c(
            ccrz__Account__c = base.ccrz__Account__c,
            ccrz__AccountNumber__c = base.ccrz__AccountNumber__c,
            ccrz__AccountType__c = base.ccrz__AccountType__c,
            ccrz__Address__c = base.ccrz__Address__c,
            ccrz__BillTo__c = base.ccrz__BillTo__c,
            ccrz__CCOrder__c = base.ccrz__CCOrder__c,
            ccrz__Comments__c = base.ccrz__Comments__c,
            ccrz__Contact__c = base.ccrz__Contact__c,
            ccrz__CurrencyISOCode__c = base.ccrz__CurrencyISOCode__c,
            ccrz__ExpirationMonth__c = base.ccrz__ExpirationMonth__c,
            ccrz__ExpirationYear__c = base.ccrz__ExpirationYear__c,
            ccrz__ParentTransactionPayment__c = base.Id,
            ccrz__PaymentType__c = base.ccrz__PaymentType__c,
            ccrz__SoldTo__c = base.ccrz__SoldTo__c,
            ccrz__Storefront__c = base.ccrz__Storefront__c,
            ccrz__SubAccountNumber__c = base.ccrz__SubAccountNumber__c,
            ccrz__Token__c = base.ccrz__Token__c,
            ccrz__TransactionType__c = 'AUTH',
            ccrz__User__c = base.ccrz__User__c,
            ccrz__StoredPayment__c  = base.ccrz__StoredPayment__c 
        );
        return payment;
    }

    public static void setTaxRate(ccrz__E_Cart__c theCart, Account theAccount) {

        if (theCart != null && theCart.ccrz__ShipTo__r != null && theCart.ccrz__BillTo__r != null && theCart.CC_FP_Location__r != null && theAccount != null) {

            if (theAccount.Tax_Exempt__c == 'Yes') {
                theCart.ccrz__TaxAmount__c = 0;
                theCart.CC_FP_Tax_Rate__c = 0;
                theCart.ccrz__TaxExemptFlag__c = true;
                ccrz.ccLog.log(System.LoggingLevel.INFO,'M:E','setTaxRate:CC_FP_Tax_Rate__c' + '1'); 
            } 
            else {
                Boolean shipInCity = false;
                ccrz.ccLog.log(System.LoggingLevel.INFO,'M:E','setTaxRate:CC_FP_Tax_Rate__c' + '2'); 
                if (theCart.ccrz__ShipTo__r != null && theCart.ccrz__ShipTo__r.CC_FP_Location__c != null) {
                    Location__c shipTo = CCFPLocationDAO.getLocation(theCart.ccrz__ShipTo__r.CC_FP_Location__c);
                    if (shipTo != null && shipTo.In_City__c != null) {
                        shipInCity = shipTo.In_City__c;
                    }
                }
                
                List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest> lstRequests = new List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest>();
                CCAviBoomiAPI.SeriesDefaultTaxRateRequest request = new CCAviBoomiAPI.SeriesDefaultTaxRateRequest();
                
                request.COMPANY = theAccount.Iseries_Company_code__c;
                request.CUSTOMER = theAccount.ISeries_Customer_Account__c;
                request.BRANCH = theAccount.ISeries_Customer_Branch__c;
                request.SHIPTOCITY = theCart.ccrz__ShipTo__r.ccrz__City__c;
                request.SHIPTOSTATE = theCart.ccrz__ShipTo__r.ccrz__State__c;
                request.SHIPTOZIP = theCart.ccrz__ShipTo__r.ccrz__PostalCode__c;
                String county = theCart.ccrz__ShipTo__r.CC_FP_County__c;
                if (county == null) {
                    county = ' ';
                }
                request.SHIPTOCOUNTY = county;
                request.SHIPTOINCITY = shipInCity ? '1' : '0';
                request.SHIPFROMCITY = theCart.CC_FP_Location__r.City__c;
                request.SHIPFROMSTATE = theCart.CC_FP_Location__r.State__c;
                request.SHIPFROMZIP = theCart.CC_FP_Location__r.Zipcode__c;
                county = theCart.CC_FP_Location__r.County__c;
                if (county == null) {
                    county = ' ';
                }                
                request.SHIPFROMCOUNTY = county;
                request.SHIPFROMINCITY = theCart.CC_FP_Location__r.In_City__c ? '1' : '0';

                request.ORDERACCCITY = ' ';
                request.ORDERACCSTATE = ' ';
                request.ORDERACCZIP = ' ';
                request.ORDERACCCOUNTY = ' ';
                request.ORDERACCINCITY = ' ';

                lstRequests.add(request);
                String storefront = ccrz.cc_CallContext.storefront;
                CCAviBoomiAPI.SeriesDefaultTaxRateResponse response = CCAviBoomiAPI.getISeriesDefaultTaxRate(storefront, lstRequests);
                System.debug(JSON.serialize(response));
                ccrz.ccLog.log(System.LoggingLevel.INFO,'M:E','setTaxRate:response' + response); 
                if (response.apiCallSuccess == true) {
                    List<CCAviBoomiAPI.SeriesDefaultTaxRate> taxRates = (List<CCAviBoomiAPI.SeriesDefaultTaxRate>)response.listDefaultTaxRates;
                    if(taxRates != null) {
                        CCAviBoomiAPI.SeriesDefaultTaxRate taxRate = taxRates.get(0);
                        if (taxRate.ErrorMessage == null) {
                            Decimal taxRateNumber = taxRate.DefaultTaxRate;
                            theCart.CC_FP_Tax_Rate__c = taxRateNumber;
                            theCart.ccrz__TaxExemptFlag__c = false;
                            Decimal shipping = 0;
                            if (theCart.ccrz__ShipAmount__c != null) {
                                shipping = theCart.ccrz__ShipAmount__c;
                            }
                            theCart.ccrz__TaxAmount__c = (theCart.ccrz__SubtotalAmount__c + shipping) * taxRateNumber;
                        }
                        else {
                            throw new FulfillmentException(taxRate.ErrorMessage);
                        }
                    } 
                }
            
            }

        }
        else {
            throw new FulfillmentException('Unable to get tax rate.');
        }

    }

    public static void updateTax(String encId) {
        ccrz__E_Cart__c cart = CCPDCCartDAO.getCartSplitResponse(encId);
        if (cart.ccrz__TaxExemptFlag__c != true && cart.CC_FP_Tax_Rate__c != null) {
            Decimal shipping = 0;
            if (cart.ccrz__ShipAmount__c != null) {
                shipping = cart.ccrz__ShipAmount__c;
            }
            cart.ccrz__TaxAmount__c = (cart.ccrz__SubtotalAmount__c + shipping) * cart.CC_FP_Tax_Rate__c;
            update cart;
        }
    }

    public class FulfillmentException extends Exception {}

}