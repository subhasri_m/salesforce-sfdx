// author : Rahul Bansal
// Company : Puresoftware
// Date created : 06 Jun 2019
// Used in VF page to get top 250 products

public without sharing class CCPDCTopProductsController {

public String CRLF {
get {
return '\n';
}
}

public String COMMA {
get {
return String.fromCharArray(new List<Integer> { 44 });
}
}


public list<wrapperProd> wraprodlst { get

{
list<wrapperProd> wraprodlst1 = new list<wrapperProd>();
for (ccrz__E_product__c prod  : [select DSP_Part_Number__c, Salespack__c, ccrz__ShortDescRT__c, FP_Brand_Name__c from ccrz__E_product__c where ccrz__Sequence__c <> null and ccrz__ProductStatus__c = 'Released' order by ccrz__Sequence__c asc limit 250] ){
wrapperProd wrapprod = new wrapperProd ();

wrapprod.dsp = String.ValueOf(prod.DSP_Part_Number__c) ;
wrapprod.qty =Integer.ValueOf(prod.Salespack__c);
wrapprod.sdesc ='"' +String.valueOf(prod.ccrz__ShortDescRT__c) + '"';
wrapprod.brand=String.valueOf(prod.FP_Brand_Name__c);
wraprodlst1.add(wrapprod);
}

return wraprodlst1;
}
}

// Wrapper class to get mandatory fields for product bulk upload
  Public class wrapperProd {
 public string dsp {get;set;}
 public Integer qty {get;set;}
 public string sdesc {get;set;}
 public string brand {get;set;}
   
 }
}