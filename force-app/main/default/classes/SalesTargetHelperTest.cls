@istest
public  class SalesTargetHelperTest {
    
    static testmethod void TestCoverage(){ 
        Sales_Target__c St = new Sales_Target__c(); 
        st.Sales_Sum__c = null;
        st.Sales_Target_Growth_ThisMonth__c = null; 
        st.Sales_Target_Growth_ThisQuarter__c = null;
        st.Sales_Target_Growth_ThisYear__c = null;
        st.YTD_Sales__c =null;    
        st.Sales_Target_Growth_Total_Month_Target__c = null; 
        st.Sales_Target_Growth_Total_Quarter_Target__c = null;
        st.Sales_Target_Growth_Total_Year_Target__c = null; 
        st.GP_Target_Growth_Total_Month_Target__c = null;
        st.GP_Target_Growth_Total_Quarter_Target__c = null; 
        st.GP_Target_Growth_Total_Year_Target__c= null; 
        
        insert St;
    }
}