/**
 * @description     Account Trigger Handler
 * @author          Lawrence Catan
 * @Company         CloudSherpas
 * @date            29.FEB.2016
 *
 * HISTORY
 * - 29.FEB.2016    Lawrence Catan      Created.
*/ 

public class AccountTriggerHandler {

	public static boolean runOnce = false;
	
	public static void syncSalesOrderSummary(Map<Id, Account> oldMap, List<Account> newAccList) {
		
		if (runOnce) return;
		runOnce = true;
		
		Set<Id> ownerIdSet = new Set<Id>();
		
		// exclude the integration records
		String informaticaUserId = Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
		List<Account> ownerChangeList = new List<Account>(); 
		for (Account acc : newAccList) {
			Account oldAcc = oldMap.get(acc.id);
			if (acc.OwnerId != oldAcc.OwnerId && acc.OwnerId != informaticaUserId) {
				ownerChangeList.add(acc);ownerIdSet.add(acc.OwnerId);
				
				
			}
		}
		
		system.debug('-- ownerChangeList:' + ownerChangeList);
		
		if (ownerChangeList.size() > 0) {
			
			List<Sales_Order_Summary__c> sosList = [select Order_Date__c, Account_Owner_ID__c, Sales_Target__c, Sales_Target__r.Start_Date__c, Sales_Target__r.OwnerId from Sales_Order_Summary__c where Account__c in :ownerChangeList and ownerId != :informaticaUserId];
			SalesOrderSummaryTriggerHandler.moveSos(ownerIdSet, sosList);
		}
		
	}

    public void UpdateNumberOfAccounts(Map<Id,Account> oldMap, Map<Id,Account> newMap){
    
    Set<Id> OwnerIdsToUpdate = new Set<Id>();List<User> UsersToUpdate = new List<User>();
    
    //Map<Id,Account> CountAccounts = new Map<Id,Account>();
    Map<String, Integer> mapOfAccounts = new Map<String, Integer>();
    
    System.debug('UpdateNumberOfAccounts called');
    
    for(Account a: newMap.values()){ OwnerIdsToUpdate.add(a.OwnerId);
        //if(oldMap.get(a.id).Ownerid != newMap.get(a.id).Ownerid){
       
        //CountAccounts.put(a.OwnerId, a);
        //}
    }
    //Integer i = CountAccounts.size();
    //CountAccounts = [SELECT ownerid, id, count(ownerid) FROM Account where ownerid in: OwnerIdsToUpdate];
    
    List<AggregateResult> CountOfAccounts = [SELECT OwnerID, Count(Id) OwnerCount From Account where OwnerID In:OwnerIdsToUpdate Group By OwnerID];
    
    System.debug('CountOfAccounts value =' + CountOfAccounts);
    
    for(AggregateResult ag : countOfAccounts) {  mapOfAccounts.put(String.valueOf(ag.get('OwnerId')), (Integer) ag.get('OwnerCount'));//Integer.valueOf('OwnerCount'));
      
    }
    
    System.debug('mapOfAccounts value = ' + mapOfAccounts);
    
    for(User u : [SELECT Id, Number_of_Accounts__c FROM User WHERE Id IN: OwnerIdsToUpdate]) {
        Integer numAcc = mapOfAccounts.containsKey(u.Id)?mapOfAccounts.get(u.Id):0;
        System.debug('numAcc value =' + numAcc);
        u.Number_of_Accounts__c = numAcc; UsersToUpdate.add(u);
       
    }
    
    System.debug('UsersToUpdate value = ' +UsersToUpdate);
    
    if(UsersToUpdate.size() > 0) {update UsersToUpdate;
        
    }

    
    /*for(User u: UsersToUpdate){
        if(UsersToUpdate.contains(u.id)) {
        u.Number_of_Accounts__c = CountOfAccounts.get(u.id);
        }
    }*/
    
    //List<AggregateResult> CountOfAccounts  = [SELECT OwnerID,Count(Id) OwnerCount From Account where OwnerID In:OwnerIdsToUpdate group by OwnerID];
    
   
    }
}