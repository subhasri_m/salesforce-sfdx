@isTest
public class CCFPFulFillMentHelperTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();
    
    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        
        ccrz.cc_util_Reflection.createStorefrontSetting('parts');
        
        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = 'parts';
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
    }
    
    static testmethod void getTaxAmountForGroupNoTest(){
        util.initCallContext();
        
        String result = '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:querySalestaxCCResponse xmlns:wss="http://www.boomi.com/connector/wss"><SalesTaxResponse> <Response> <ShipmentID>a7j1W000000TfHGQA0</ShipmentID> <TaxAmount>42.21</TaxAmount> <ReturnCode>200</ReturnCode> <CodeDescription>Successful </CodeDescription> </Response> <Response> <ShipmentID>a7j1W000000TfHHQA0</ShipmentID> <TaxAmount>20.00</TaxAmount> <ReturnCode>200</ReturnCode> <CodeDescription>Successful </CodeDescription> </Response> <Total> <TotalTaxAmount>62.21</TotalTaxAmount> </Total> </SalesTaxResponse></wss:querySalestaxCCResponse></S:Body></S:Envelope>';
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', result));
        
        
        ccrz__E_Cart__c cart = util.getCart();
        
        
        ccrz__E_CartItem__c ci = util.getCartItem();
        
        Account theAccount = util.getAccount();
        theAccount.Tax_Exempt__c = 'False';
        theAccount.Iseries_Company_code__c = 'cmpCode';
        theAccount.ISeries_Customer_Account__c = 'testAcc';
        theAccount.ISeries_Customer_Branch__c = 'testBranch';
        update theAccount;
        
        
        
        Location__c loc = util.getLocation();
        loc.Location__c = 'DT';
        loc.Address_Line_1__c = 'test';
        loc.Address_Line_2__c = 'test';
        loc.City__c = 'test';
        loc.State__c = 'test';
        loc.Zipcode__c = '75051';
        loc.County__c = 'test';
        update loc;
        
        cart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        cart.CC_FP_Location__r = loc;
        cart.ccrz__Storefront__c = 'parts';
        update cart;
        
        List<ccrz__E_CartItemGroup__c> groups = new  List<ccrz__E_CartItemGroup__c>();// = CCPDCCartDAO.getCartItemGroupsForCart(cart.Id);
        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr; 
        
        System.debug('cart==>'+cart);
        
        ccrz__E_CartItemGroup__c ciGroup = util.createCartItemGroup(cart, addr, 'Shipment-1', 'Carrier', false, 6.00);
        ciGroup.CC_FP_Subtotal_Amount__c = 1000.09;
        insert ciGroup;
        groups.add(ciGroup);
        
        List<ccrz__E_CartItem__c> lstCI = cart.ccrz__E_CartItems__r;
        lstCI[0].ccrz__CartItemGroup__c = ciGroup.Id;
        
        List<ccrz__E_CartItemGroup__c> cartItemGroups =  CCPDCCartDAO.getCartItemGroupDetailsForCart(Cart.Id);
        System.debug('cartItemGroups==>'+cartItemGroups);
        
        
        System.debug('groups==>'+groups);
        ccrz.cc_CallContext.storefront = 'parts';
        
        Test.startTest();
        CCFPFulFillMentHelper.getTaxAmountForGroup(cart, theAccount);
        Test.stopTest();
        
        
    }
    
    static testmethod void getTaxAmountForGroupYesTest(){
        util.initCallContext();
        Location__c loc = util.getLocation();
        loc.Location__c = 'test';
        update loc;
        
        ccrz__E_CartItem__c ci = util.getCartItem();
        System.debug('ci=='+ci);
        
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__r = loc;
        update cart;
        System.debug('cart=='+cart);
        
        
        
        
        Account acc = util.getAccount();
        acc.Tax_Exempt__c = 'Yes';
        update acc;
        
        Test.startTest();
        ccrz.cc_CallContext.storefront = 'parts';
        CCFPFulFillMentHelper.getTaxAmountForGroup(cart, acc);
        Test.stopTest();
        
    }
    
    static testmethod void doSplitsFPTest(){
        util.initCallContext();
  		ccrz__E_Cart__c cart = util.getCart();
        
       cart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        cart.ccrz__E_CartItems__r[0].FulFillMentType__c = 'PickUp';
        update cart;
        System.debug('cart==>'+cart.ccrz__E_CartItems__r);
        
        Test.startTest();
        CCFPFulFillMentHelper.doSplitsFP(cart);
        Test.stopTest();
        
    }
     static testmethod void doSplitsFPShipToTest(){
        util.initCallContext();
  		ccrz__E_Cart__c cart = util.getCart();
        
       cart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        cart.ccrz__E_CartItems__r[0].FulFillMentType__c = 'ShipTo';
        update cart;
        System.debug('cart==>'+cart.ccrz__E_CartItems__r);
        
        Test.startTest();
        CCFPFulFillMentHelper.doSplitsFP(cart);
        Test.stopTest();
        
    }
     static testmethod void doSplitsFPLocDelTest(){
        util.initCallContext();
  		ccrz__E_Cart__c cart = util.getCart();
        
       cart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        cart.ccrz__E_CartItems__r[0].FulFillMentType__c = 'LocalDelivery';
        update cart;
        System.debug('cart==>'+cart.ccrz__E_CartItems__r);
        
        Test.startTest();
        CCFPFulFillMentHelper.doSplitsFP(cart);
        Test.stopTest();
        
    }
    
    static testmethod void copyCartTransactionDataToOrderTest(){
        util.initCallContext();
  		ccrz__E_Order__c order = util.getOrder();
        Test.startTest();
        CCFPFulFillMentHelper.copyCartTransactionDataToOrder(order.Id);
        Test.stopTest();
        
    }
}