global with sharing class CCPDCMyAccountMyBackordersController {

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getBackorderCartItems(ccrz.cc_RemoteActionContext ctx){

        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = String.valueOf(ccrz.cc_CallContext.currContact.Id);
        Boolean isContainBackorder = false;
        List<String> skus = new List<String>();
        Map<String,Map<String,String>> itemDatas = new Map<String,Map<String,String>>();
        Map<String,Map<String,String>> instockItemsDatas = new Map<String,Map<String,String>>();
        Map<String,Map<String,String>> backOrderedItemDatas = new Map<String,Map<String,String>>();
        Map<String,CCPDCInventoryHelper.Inventory> inventories = new Map<String,CCPDCInventoryHelper.Inventory> ();
        List<Map<String, String>> resultData = new List<Map<String, String>>();
        try{
            ccrz__E_Cart__c cart = CCPDCCartDAO.getBackOrderCartFromAccountId(String.valueOf(accountId));
            //List<CC_FP_Contract_Account_Permission_Matrix__c> matrixList = CCFPContactAccountPermissionMatrixDAO.getAccountsForContact(contactId);
            if(cart != null){
                if(!cart.ccrz__E_CartItems__r.isEmpty()){
                    isContainBackorder = true;
                    //itemDatas.put('isContainBackorder',new Map<String, String>{'Status' => 'true'});
                    for (CCRZ__E_CartItem__c item : cart.ccrz__E_CartItems__r){
                        skus.add(item.ccrz__Product__r.ccrz__SKU__c);
                        Map<String, String> currentItemData = new Map<String, String>();
                        String skupo= item.ccrz__Product__r.ccrz__SKU__c+item.PO_Number__c;
                        currentItemData.put('sku',String.valueOf(item.ccrz__Product__r.ccrz__SKU__c));
                        currentItemData.put('productQty',String.valueOf(item.ccrz__Quantity__c));
                        currentItemData.put('productPrice',String.valueOf(item.ccrz__Price__c));
                        currentItemData.put('itemTotal',String.valueOf(item.ccrz__Price__c * item.ccrz__Quantity__c));
                        currentItemData.put('partDesc',String.valueOf(item.ccrz__Product__r.Name));
                        currentItemData.put('backOrderCartId',String.valueOf(cart.Id));
                        currentItemData.put('poNumber',String.valueOf(item.PO_Number__c));
                        //currentItemData.put('pricingVisibility',String.valueOf(matrixList[0].Pricing_Visibility__c));  
                        if(itemDatas.containsKey(skupo)){
                            Map<String, String> selected = itemDatas.get(skupo);
                            String qty = selected.get('productQty');
                            Decimal newqty = Decimal.valueOf(qty)+item.ccrz__Quantity__c;
                            selected.put('productQty',String.valueOf(newqty));
                            itemDatas.put(skupo,selected);

                        }else {
                            itemDatas.put(skupo,currentItemData);   
                        }               

                    }
                    inventories = CCPDCInventoryHelper.getInventory(skus, accountId);

                    if(inventories != null){
                        for(String skupo: itemDatas.keySet()){
                            Map<String,String> itemdata = itemDatas.get(skupo);
                            CCPDCInventoryHelper.Inventory inv = inventories.get(itemdata.get('sku'));
                            //itemdata.put('warehouseQty',String.valueOf(inv.totalQuantity));
                            Integer productQty = Integer.valueOf(itemdata.get('productQty'));
                            ccrz.ccLog.log(' Inventory ', inv);
                            if(inv!=null && productQty <= inv.totalQuantity){
                                itemdata.put('warehouseStatus','In Stock');
                                //itemdata.put('itemTotal',String.valueOf(productPrice*productQty));
                                instockItemsDatas.put(skupo,itemdata);              
                            } else{
                                itemdata.put('warehouseStatus','Backordered');
                                backOrderedItemDatas.put(skupo,itemdata);
                            }
                        }
                        itemDatas.clear();
                        itemDatas.putAll(instockItemsDatas);
                        itemDatas.putAll(backOrderedItemDatas);
                    } else {
                        for(String skupo: itemDatas.keySet()){
                            Map<String,String> itemdata = itemDatas.get(skupo);
                            itemData.put('warehouseStatus','Backordered');
                        }
                    }
                    List<ccrz__E_ProductMedia__c> prodImages = CCPDCProductDAO.getProductThumbnailURIForSkus(skus);
                    Map<String,String> skutoimg = new Map<String,String>();
                    if(prodImages != null){
                        for(ccrz__E_ProductMedia__c pm : prodImages){
                            skutoimg.put(pm.ccrz__Product__r.ccrz__SKU__c, pm.ccrz__URI__c);                
                        }
                    }

                    for(String skupo : itemDatas.keySet()){
                        Map<String,String> itemdata = itemDatas.get(skupo);
                        String imgurl = skutoimg.get(itemdata.get('sku'));
                        itemdata.put('imageURL',imgurl);
                    }


                    //if(prodImages != null){
                    //  for(ccrz__E_ProductMedia__c pm : prodImages){
                    //      for(String skupo: itemDatas.keySet()){
                    //          if(itemDatas.get(skupo).get('sku').equals(pm.ccrz__Product__r.ccrz__SKU__c)){
                    //              Map<String,String> itemdata = itemDatas.get(skupo);
                    //              itemdata.put('imageURL',String.valueOf(pm.ccrz__URI__c));
                    //          }
                    //      }
                            
                    //  }
                    //}
                    //Map<String,String> containBackorder = new Map<String,String>();
                    //containBackorder.put('containsBackOrderItems',String.valueOf(isContainBackorder));
                    //resultData.add(containBackorder);

                    for(String sku: itemDatas.keySet()){
                        resultData.add(itemDatas.get(sku));
                    }

                }      
                CCAviPageUtils.buildResponseData(response,true, new Map<String,Object>{
                        'backorderProducts' => resultData
                    });
            } else{
                CCAviPageUtils.buildResponseData(response,false, new Map<String,Object>{'message' => 'can not find backorderCart','isContainBackorder' => isContainBackorder}
                    );
            }

        } catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
              new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }



        return response;

    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult addBockOrderItemsToCart(ccrz.cc_RemoteActionContext ctx, String backOrderItemData){    
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        String cartId = ccrz.cc_CallContext.currCartId;
        Id accountId = ccrz.cc_CallContext.effAccountId;
        String storefront = ccrz.cc_CallContext.storefront;
 
 

        try{
            ccrz__E_Cart__c backordercart = CCPDCCartDAO.getBackOrderCartFromAccountId(String.valueOf(accountId));
            Account boAccount = CCPDCAccountDAO.getAccount(accountId);

            // Boomi request
            Map<String,Object> input = (Map<String, Object>) JSON.deserializeUntyped(backOrderItemData);
            List<CCRZ__E_CartItem__c> instockItems = new List<CCRZ__E_CartItem__c>();
            List<String> instockProductSkus = new List<String>();
            Map<String,Map<String, String>> convertedData = new Map<String,Map<String, String>>();
            ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(cartId);
            String backOrderCartId = null;

            for(String index: input.keySet()){
                List<Object> cartItem = (List<Object>)input.get(index);
                Map<String,String> currItem = new Map<String,String>();
                currItem.put('sku',String.valueOf(cartItem.get(0)));
                currItem.put('productQty',String.valueOf(cartItem.get(1)));
                currItem.put('productPrice',String.valueOf(cartItem.get(2)));
                currItem.put('itemTotal',String.valueOf(cartItem.get(3)));
                currItem.put('backOrderCartId',String.valueOf(cartItem.get(4)));
                if(cartItem.size() == 6) {
                    currItem.put('poNumber',String.valueOf(cartItem.get(5)));
                }
                
                backOrderCartId = String.valueOf(cartItem.get(4));
                instockProductSkus.add(String.valueOf(cartItem.get(0)));
                convertedData.put(String.valueOf(cartItem.get(0)),currItem);

            }

            List<CCAviBoomiAPI.SeriesBackorderCancellationWSRequest> lstRequests = new List<CCAviBoomiAPI.SeriesBackorderCancellationWSRequest>();
            List<ccrz__E_CartItem__c> instockItemsFrombackorderCart = new List<ccrz__E_CartItem__c>();
            Set<String> cartItemIdsForIS = new Set<String>();
            for (CCRZ__E_CartItem__c item : backordercart.ccrz__E_CartItems__r){
                if(convertedData.containsKey(item.ccrz__Product__r.ccrz__SKU__c)){
                    instockItemsFrombackorderCart.add(item);
                                        
                    //cartItemIdsForIS.add(String.valueOf(item.Id));
                    // use cartItemID and cartID here
                    CCAviBoomiAPI.SeriesBackorderCancellationWSRequest req = new CCAviBoomiAPI.SeriesBackorderCancellationWSRequest();
                    req.TicketLineNumber = item.BO_Line_Number__c;
                    req.TicketNumber = item.BO_ID__c;
                    req.Company = boAccount.Iseries_Company_code__c;
                    lstRequests.add(req);

                }
            }




            //for(String CCRZ__E_CartItem__c : instockItemsFrombackorderCart){
            //  CCAviBoomiAPI.SeriesBackorderCancellationWSRequest req = new CCAviBoomiAPI.SeriesBackorderCancellationWSRequest();
            //  req.TicketLineNumber = cid;
            //  req.TicketNumber = String.valueOf(backOrderCartId);
            //  lstRequests.add(req);
            //}
            //CCAviBoomiAPI.SeriesBackorderCancellationWSResponse lstResponse = CCAviBoomiAPI.getBackorderCancellationWS(storefront,lstRequests);

            

            List<ccrz__E_Product__c> products = CCPDCProductDAO.getParts(instockProductSkus, storefront);
            List<ccrz.ccApiCart.LineData> theNewLines = new List<ccrz.ccApiCart.LineData>();
            Boolean wasSuccessful = false;
            String theCartSFID = String.valueOf(cart.Id);//presumes that the cart id is passed into this code.
            Map<String, Decimal> priceMap = new Map<String, Decimal>();
            for(ccrz__E_Product__c prod: products){
                Map<String, String> currProdData = convertedData.get(String.valueOf(prod.ccrz__SKU__c));
                String skuToAdd = currProdData.get('sku');
                Decimal qtyToAdd = Decimal.valueOf(currProdData.get('productQty'));
                Decimal price = Decimal.valueOf(currProdData.get('productPrice'));
                ccrz.ccApiCart.LineData theNewLine = new ccrz.ccApiCart.LineData();
                theNewLine.sku = skuToAdd;
                theNewLine.quantity = qtyToAdd;
                priceMap.put(skuToAdd, price);          
                theNewLines.add( theNewLine );
            }

                Map<String,Object> addResults = CCPDCCartManager.addTo(new Map<String,Object>{
                        ccrz.ccApi.API_VERSION => 6,
                        ccrz.ccApiCart.CART_ID => theCartSFID,
                        ccrz.ccApiCart.LINE_DATA => theNewLines,
                        ccrz.ccAPI.SIZING => new Map<String, Object>{
                            ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
                                ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_S,
                                ccrz.ccAPI.SZ_REFETCH => TRUE
                            }
                        } 
                    });
                  
                    //General check, since the cart id will be blank if not successful we technically do not need this.               

                // Below is for retrieve new add cart items, set Do not reprice to true
                
                wasSuccessful = (Boolean)addResults.get(ccrz.ccApi.SUCCESS);
                Set<Id> backorderIds = new Set<Id>();
                List<Map<String, Object>> test = (List<Map<String, Object>>)addResults.get(ccrz.ccAPICart.CART_OBJLIST);
                List<Object> cartItems = new List<ccrz__E_CartItem__c>();
                Map<String, Object> data = test[0];
                cartItems = (List<Object>)data.get('ECartItemsS');
                for(Integer i = cartItems.size()-1;i >= cartItems.size() - theNewLines.size();i--){
                    Map<String,Object> q = new Map<String,Object>();
                    q = (Map<String,Object>)cartItems.get(i);
                    Id aId = (Id)q.get('sfid');
                    backorderIds.add(aId);
                    //System.debug('-------------------------------');
                }

            Map<Id,ccrz__E_CartItem__c> updatecartItems = CCPDCCartDAO.getCartItemsForPricing(backorderIds);

            for(ccrz__E_CartItem__c cartItem : updatecartItems.values()){
               ccrz.ccLog.log('@@Rahu4');
                cartItem.CC_FP_DoNotReprice__c = true;
                Decimal price = priceMap.get(cartItem.ccrz__Product__r.ccrz__SKU__c);
                
                if (price != null) {
                    cartItem.ccrz__Price__c = price;
                    cartItem.ccrz__SubAmount__c = cartItem.ccrz__Price__c * cartItem.ccrz__Quantity__c;
                }
                if (cartItem.ccrz__Price__c == 0) {
                    cartItem.CC_FP_DoNotReprice__c = false;
                }
                
                if(convertedData.containsKey(cartItem.ccrz__Product__r.ccrz__SKU__c)) {
                    Map<String,String> a = convertedData.get(cartItem.ccrz__Product__r.ccrz__SKU__c);
                    cartItem.CC_FP_PONumber__c = a.get('poNumber');
                }
            }

            
            cart.CC_FP_Is_Free_Shipping__c = true;
            cart.CC_FP_Free_Shipping_Type__c = 'BACKORDER';


            CCAviBoomiAPI.SeriesBackorderCancellationWSResponse lstResponse = CCAviBoomiAPI.getBackorderCancellationWS(storefront,lstRequests);
            update updatecartItems.values();
            update cart;
            delete instockItemsFrombackorderCart;
            CCAviPageUtils.buildResponseData(response, true, new Map<String, Object>{'boomiResponse'=>lstResponse, 'lstRequests'=>lstRequests});

        } 
        catch (Exception e) {
            System.debug(e);
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
    }

        return response;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult deleteItemFromBackOrderCart(ccrz.cc_RemoteActionContext ctx,String sku){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        Id accountId = ccrz.cc_CallContext.effAccountId;
        List<CCRZ__E_CartItem__c> itemsToBeDelete = new List<CCRZ__E_CartItem__c>();
        String storefront = ccrz.cc_CallContext.storefront;
        List<CCAviBoomiAPI.SeriesBackorderCancellationWSRequest> lstRequests = new List<CCAviBoomiAPI.SeriesBackorderCancellationWSRequest>();
        //String cartItemId = null;
        Account currAcc = CCPDCAccountDAO.getAccount(accountId);

        try{
            Map<String,Map<String, String>> convertedData = new Map<String,Map<String, String>>();
            ccrz__E_Cart__c cart = CCPDCCartDAO.getBackOrderCartFromAccountId(String.valueOf(accountId));
            //String cartId = (String)cart.Id;
            //ccrz.ccApiCart.LineData theNewLine = new ccrz.ccApiCart.LineData();
            for (CCRZ__E_CartItem__c item : cart.ccrz__E_CartItems__r){
                if(sku.equals(String.valueOf(item.ccrz__Product__r.ccrz__SKU__c))){
                    itemsToBeDelete.add(item);
                    //cartItemId = String.valueOf(item.Id);
                    // Using salesforce cartItem ID and cart ID here, need to know boomi number
                    CCAviBoomiAPI.SeriesBackorderCancellationWSRequest req = new CCAviBoomiAPI.SeriesBackorderCancellationWSRequest();
                    req.Company = currAcc.Iseries_Company_code__c;
                    req.TicketLineNumber = String.valueOf(item.BO_Line_Number__c);
                    req.TicketNumber = String.valueOf(item.BO_ID__c);
                    lstRequests.add(req);
                }
            }
            CCAviBoomiAPI.SeriesBackorderCancellationWSResponse lstResponse = CCAviBoomiAPI.getBackorderCancellationWS(storefront,lstRequests);
            //CCAviCartManager.removeFromCart(cartId,cartItemId);
            delete itemsToBeDelete;
            // Boomi call below for delete iSeries.
             //Use CCAviBoomiApi getBackorderCancellationWS



            CCAviPageUtils.buildResponseData(response, true, new Map<String, Object>{'records' => itemsToBeDelete,'boomiResponse' => lstResponse});
        }
        catch(Exception e){
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }


    //@future(callout=true)
    //public static String removeFromCart(Set<String> cartitemIds, Id cartId, String storefront){

    //}


}