@isTest
public class CCPDCRequiredOrderItemDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();

            ccrz__E_Cart__c cart = util.createCartWithSplits(m);
            ccrz__E_Order__c order = util.createOrderWithSplits(m, cart);
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

    }

    static testmethod void getRequiredOrderItemsTest() {
        util.initCallContext();

        ccrz__E_Order__c order = util.getOrder();
        ccrz__E_Product__c product = util.getProduct();
        ccrz__E_Product__c requiredProduct = util.createRequiredProduct(product);
        ccrz__E_OrderItem__c orderItem = CCAviTestUtil.createOrderItem(order.Id, 'test', 'Major', product.Id, null, null, 1.00, 1, 1.00);
        insert orderItem;

        CC_FP_Required_Order_Item__c requiredItem = new CC_FP_Required_Order_Item__c(
            CC_Order__c = order.Id,
            CC_Parent_Order_Item__c = orderItem.Id,
            Quantity__c = 1,
            Price__c = 5.00,
            SubAmount__c = 5.00,
            CC_Product__c = requiredProduct.Id
        );
        insert requiredItem; 

        List<CC_FP_Required_Order_Item__c> results = null;

        Test.startTest();
        results = CCPDCRequiredOrderItemDAO.getRequiredOrderItems(new Set<Id> {orderItem.Id});
        Test.stopTest();

        System.assert(results != null);
        System.assertEquals(1, results.size());

    }
}