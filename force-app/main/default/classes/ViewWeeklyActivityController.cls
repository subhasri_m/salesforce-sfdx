public class ViewWeeklyActivityController {
    
    public Date DateToday{get;set;}  //DateToday Getter-Setter declared By Likhit on 21-09-2020.
    public String updateStatus{get; set;}
    public String Check {get;set;}
    public String MonTasks {get;set;}
    public String TueTasks {get;set;}
    public String WedTasks {get;set;}
    public String ThuTasks {get;set;}
    public String FriTasks {get;set;}
    Public String MonDate {get;set;}
    Public String TueDate {get;set;}
    Public String WedDate {get;set;}
    Public String ThuDate {get;set;}
    Public String FriDate {get;set;}
    public Date PopupDueDate {get;set;}
    public string HiddenTaskId{get;set;}
    public Boolean displayPopup {get;set;}
    Map<integer,String> BitMastMapping = new  Map<Integer, String>();
    
    //Recurrence
    public String  RECURRENCEINSTANCE_input {get;set;}
    public task ParentRecurrencRecord = new task();
    public integer WeekdayMonthly_Input {get;set;}
    public integer RECURRENCEDAYOFMONTH_Input {get;set;}
    public integer RecurrenceInterval_input {get;set;}
    public Boolean IsRecurrenceChanged =false;
    
    public Boolean recurrenceChildTask {get;set;}
    public String RecurrenceParentTaskVar {get;set;}
    
    public Boolean ShowWeekdays{get;set;}
    public Boolean ShowMonthOptions {get;set;}
    public integer daymaskGlobal {get;set;}
    public Boolean IsMonSelected {get;set;}
    public Boolean IsTueSelected {get;set;}
    public Boolean IsWedSelected {get;set;}
    public Boolean IsThuSelected {get;set;}
    public Boolean IsFriSelected {get;set;}
    
    public TaskScheduleWrapper ScheduleWrapper {get;set;} 
    
    List<ActivityWrapperClass> UserWeekTasksList;
    List<ActivityWrapperClass> SelectedTaskList {get; set;}
    
    public List<SelectOption> getItems() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Blank','--Select--'));
    //    options.add(new SelectOption('Not Started','Not Started'));
    //    options.add(new SelectOption('In Progress','In Progress'));
       options.add(new SelectOption('Not Completed','Not Completed'));
  
        options.add(new SelectOption('Completed','Completed'));
        return options;
    }
    
    public List<SelectOption> getRecWeekDay()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','--Select--'));   
        options.add(new SelectOption('2','Monday')); 
        options.add(new SelectOption('4','Tuesday'));
        options.add(new SelectOption('8','Wednesday'));
        options.add(new SelectOption('16','Thursday'));
        options.add(new SelectOption('32','Friday'));
        
        return options;
    }
    
    public List<SelectOption> getRecInstances()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','--Select--'));   
        options.add(new SelectOption('First','1st')); 
        options.add(new SelectOption('Second','2nd'));
        options.add(new SelectOption('Third','3rd'));
        options.add(new SelectOption('Fourth','4th'));
        options.add(new SelectOption('Last','Last'));
        
        return options;
    }
    
    public ViewWeeklyActivityController()
    {
         List<BitMaskWeekDay__mdt> BitMasKSetting = [select BitDays__c,BitValue__c from BitMaskWeekDay__mdt];
        for (BitMaskWeekDay__mdt var : BitMasKSetting)
        {
            BitMastMapping.put(Integer.valueof(var.BitValue__c),var.BitDays__c);
            
        }
         //DateToday Get-set Defination added by Likhit on 21-09-2020.
    	DateToday = Date.today(); 
        MonTasks ='';
        TueTasks ='';
        WedTasks ='';
        ThuTasks ='';
        FriTasks ='';
        //Check = '<div class="redText">check one - one  </div>  <div class="Greentext"> check two - two</div>';
        CurrentWeekDays();
            IsMonSelected =false;
     IsTueSelected =false;
      IsWedSelected =false;
      IsThuSelected =false;
      IsFriSelected =false;
    }
    
    public PageReference assetClicked() 
    { 
        PageReference redirect = new PageReference('/apex/viewallActivity');  
      redirect.setRedirect(true);  
        return redirect;
    }
    

    
    public PageReference ScheduleSalesCallClicked() 
    { 
        PageReference redirect = new PageReference('/apex/scheduleSalesCall');  
        redirect.setRedirect(true);  
        return redirect;
    }
     /* Below Method (ScheduleSalesCallToday) By Likhit on 21-09-2020. */
    
    public void ScheduleSalesCallToday() 
    { 
        String Accountid = ApexPages.currentPage().getParameters().get('Accid');    
        Task NewTask = new Task(); 
        NewTask.whatid = Accountid;
        NewTask.priority = 'Normal';   
        NewTask.ActivityDate = date.today(); 
        NewTask.Subject = 'Sales Call';
       // System.debug('NewTask Data: '+NewTask.whatid+' '+NewTask.priority+' '+NewTask.AccountId+' '+NewTask.ActivityDate+''+NewTask.Subject);        
         try{
                   Insert NewTask;     
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Task Created Successfully'));
                }
                catch(Exception e)
                {   
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                        // Process exception here
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,(e.getDmlMessage(i)))); 
                    }  
                }
    }
    
    
    public PageReference GotoRecurrenceTask() 
    { 
        PageReference redirect = null;
        IF(!TEST.isRunningTest())
          redirect = new PageReference('/'+ScheduleWrapper.ParentRecurrenceID_Rc+'/e?retURL=%2F'+ScheduleWrapper.ParentRecurrenceID_Rc ); 
        ELSE
            redirect = new PageReference('/' );
        redirect.setRedirect(true);  
        return redirect;
    }
    
    
 Public List<ActivityWrapperClass>  getUserWeekTasks()
    {
        List<ActivityWrapperClass> returnedTaskList = new  List<ActivityWrapperClass>();
        List<ActivityWrapperClass> Top15SortedTasks = new  List<ActivityWrapperClass>();
        returnedTaskList= UserWeekTasksGenerator();
        
       /* List<ActivityWrapperClass> HighPriorityTasks = new  List<ActivityWrapperClass>();
        List<ActivityWrapperClass> NormalPriorityTasks = new  List<ActivityWrapperClass>();
        List<ActivityWrapperClass> LowPriorityTasks = new  List<ActivityWrapperClass>();*/
        
        for (ActivityWrapperClass var :returnedTaskList) {
        if(Top15SortedTasks.size()<16)
                {
                    Top15SortedTasks.add(var);
                }
        }
       /* for (ActivityWrapperClass var :returnedTaskList)
        {
            if(var.Priority.equalsignoreCase('High'))
            {
                HighPriorityTasks.add(var);
            }
            if(var.Priority.equalsignoreCase('Normal'))
            {
                NormalPriorityTasks.add(var);
            }
            if(var.Priority.equalsignoreCase('Low'))
            {
                LowPriorityTasks.add(var);
            }
        }
         
        
        
        
        if(!HighPriorityTasks.isEmpty()){
            for (ActivityWrapperClass var:HighPriorityTasks )
            {
                if(Top15SortedTasks.size()<15)
                {
                    Top15SortedTasks.add(var);
                }
            }
        }
        
        if(!NormalPriorityTasks.isEmpty()){
            for (ActivityWrapperClass var:NormalPriorityTasks )
            {
                if(Top15SortedTasks.size()<15)
                {
                    Top15SortedTasks.add(var);
                }
            }
        }
        
        
        
        if(!LowPriorityTasks.isEmpty()){
            for (ActivityWrapperClass var:LowPriorityTasks )
            {
                if(Top15SortedTasks.size()<15)
                {
                    Top15SortedTasks.add(var);
                }
            }
        } 
        */
        return Top15SortedTasks;
    }
    
    public List<SelectOption> getRecurrenceTypeOptions()
    {
        
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Weekly','Weekly')); 
        options.add(new SelectOption('Bi-Weekly','Every 2 weeks'));
        options.add(new SelectOption('Tri-Weekly','Every 3 weeks'));
        options.add(new SelectOption('Monthly','Monthly')); return options; 
    }
    
    
    Public List<ActivityWrapperClass> UserWeekTasksGenerator()
    {
        UserWeekTasksList = new List<ActivityWrapperClass>();
        MonTasks='';
        TueTasks='';
        wedTasks='';
        ThuTasks='';
        FriTasks='';
        
        
        String LoggedinUserId = UserInfo.getUserId();
    
        List<Task> UserTaskList = [select id,Recurring_Task_ID__c ,whoid,who.name,Priority,Description,Subject,what.name,status, ActivityDate from task where Ownerid =:LoggedinUserId and ActivityDate!=null and status !='Completed' and  status !='Not Completed' order by ActivityDate asc limit 20] ;
        IF(Test.isRunningTest())
            UserTaskList = [select id,Recurring_Task_ID__c ,Priority,whoid,who.name,Description,Subject,what.name,status, ActivityDate   from task where  ActivityDate!=null LIMIT 10];
        For (Task Taskvar:UserTaskList)
        {
            
            String DayoftheWeek = ((Datetime)taskVar.ActivityDate.adddays(1)).format('EEEE'); 
            if(taskVar.Status!='Completed'){
                ActivityWrapperClass Wrapper = new ActivityWrapperClass();
                If(TaskVar.Subject!=null)
                    wrapper.subject  = TaskVar.Subject;
                If(TaskVar.Status!=null)    
                    wrapper.Status = taskVar.Status;
                if(taskVar.ActivityDate < Date.Today()) // added 6-Mar-2019 RB. Color Highlighting
                    Wrapper.datecolor =true;    
                Wrapper.TaskId = taskVar.Id;
                Wrapper.DueDate  = taskVar.ActivityDate;
                Wrapper.DueDateDay = DayoftheWeek; 
                if(taskvar.whoid!=null)
                {
                    wrapper.who_realid = taskVar.WhoId;
                    wrapper.whoName = taskVar.who.name;
                }
                   
                Wrapper.what_realId = taskVar.Whatid;            
                wrapper.whatid = taskVar.What.name;
               
                wrapper.Priority = taskvar.Priority;
                wrapper.Description = taskvar.Description;
                
                if (taskvar.Recurring_Task_ID__c !=null)
                {
                    wrapper.RecurrenceParentTaskid = taskvar.Recurring_Task_ID__c;
                    wrapper.recurrenceChildTask  = true;
                }
                UserWeekTasksList.add(Wrapper);
            }
            
            if(DayoftheWeek.equalsIgnoreCase('Monday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    MonTasks =MonTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    MonTasks =MonTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }
                
            }
            if(DayoftheWeek.equalsIgnoreCase('Tuesday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    TueTasks =TueTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    TueTasks =TueTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }
                
            }
            if(DayoftheWeek.equalsIgnoreCase('Wednesday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    WedTasks =WedTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    WedTasks =WedTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }
                
            }
            if(DayoftheWeek.equalsIgnoreCase('Thursday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    ThuTasks =ThuTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    ThuTasks =ThuTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }
                
                
            }
            if(DayoftheWeek.equalsIgnoreCase('Friday'))
            {
                if(taskVar.Status.equalsIgnoreCase('Completed'))
                {
                    FriTasks =FriTasks +'<div class="Greentext">'+taskVar.What.name +  '</div>';
                }
                
                else
                {
                    FriTasks =FriTasks +'<div class="redText">'+taskVar.What.name +  '</div>';
                }
                
                
            }
        }
        
        return UserWeekTasksList;
    }  
    
    public void CurrentWeekDays()
    {
        String Daytoday = ((datetime) date.today()).format('EEEE'); 

        if(Daytoday.equalsIgnoreCase('Monday') || Test.isRunningTest() )
        {
            MonDate = ((Datetime)Date.today()).format('MM-dd-yyyy');
            TueDate = ((Datetime)date.today().addDays(1)).format('MM-dd-yyyy');
            WedDate = ((Datetime)date.today().addDays(2)).format('MM-dd-yyyy');
            ThuDate = ((Datetime)date.today().addDays(3)).format('MM-dd-yyyy');
            FriDate = ((Datetime)date.today().addDays(4)).format('MM-dd-yyyy');
        }
        
        if(Daytoday.equalsIgnoreCase('Tuesday') || Test.isRunningTest())
        { 
            MonDate = ((Datetime)date.today().addDays(-1)).format('MM-dd-yyyy');
            TueDate = ((Datetime)Date.today()).format('MM-dd-yyyy');
            WedDate = ((Datetime)date.today().addDays(1)).format('MM-dd-yyyy');
            ThuDate = ((Datetime)date.today().addDays(2)).format('MM-dd-yyyy');
            FriDate = ((Datetime)date.today().addDays(3)).format('MM-dd-yyyy');
        }
        if(Daytoday.equalsIgnoreCase('Wednesday') || Test.isRunningTest())
        {    
            
            MonDate = ((Datetime)date.today().addDays(-2)).format('MM-dd-yyyy');
            TueDate = ((Datetime)date.today().addDays(-1)).format('MM-dd-yyyy');
            WedDate =  ((Datetime)Date.today()).format('MM-dd-yyyy');
            ThuDate = ((Datetime)date.today().addDays(1)).format('MM-dd-yyyy');
            FriDate = ((Datetime)date.today().addDays(2)).format('MM-dd-yyyy');
        }
        if(Daytoday.equalsIgnoreCase('Thursday') || Test.isRunningTest())
        {    
         
            MonDate = ((Datetime)date.today().addDays(-3)).format('MM-dd-yyyy');
            TueDate = ((Datetime)date.today().addDays(-2)).format('MM-dd-yyyy');
            WedDate =  ((Datetime)date.today().addDays(-1)).format('MM-dd-yyyy');
            ThuDate = ((Datetime)Date.today()).format('MM-dd-yyyy');
            FriDate = ((Datetime)date.today().addDays(1)).format('MM-dd-yyyy');
        }
        if(Daytoday.equalsIgnoreCase('Friday') || Test.isRunningTest())
        {
            
            MonDate = ((Datetime)date.today().addDays(-4)).format('MM-dd-yyyy');
            TueDate = ((Datetime)date.today().addDays(-3)).format('MM-dd-yyyy');
            WedDate =  ((Datetime)date.today().addDays(-2)).format('MM-dd-yyyy');
            ThuDate = ((Datetime)date.today().addDays(-1)).format('MM-dd-yyyy');
            FriDate = ((Datetime)Date.today()).format('MM-dd-yyyy');
        }        
        
    }
    public pagereference updateTableData() {
        //this.accountname=ApexPages.currentPage().getParameters().get('accountname');
        SelectedTaskList=new List<ActivityWrapperClass>();
        for(ActivityWrapperClass item: UserWeekTasksList)
        {
            if(item.selected)
            {
                SelectedTaskList.add(item);
            } 
        }         
        
     
        if(SelectedTaskList.isEmpty())
        {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'No items were selected to process'));}
        
        if(!SelectedTaskList.isEmpty())
        {
            UpdateTaskRecords(SelectedTaskList);
        }
        return null;
    }
    
    @testVisible
    private void UpdateTaskRecords(List<ActivityWrapperClass> SelectedList)
    { 
        
        
        List<Task> UpdateTaskList = new List<Task>();
        for (ActivityWrapperClass itemVar: SelectedList)
        {
            Task TaskRecord = new Task(); 
            TaskRecord.id = itemVar.TaskId;
            TaskRecord.Description= itemVar.Description; // Added By Likhit on 16-Oct-2020
            if(updateStatus !='Blank')
            {
                TaskRecord.Status= updateStatus;
            }
            UpdateTaskList.add(TaskRecord);
        }   
        
        if (!UpdateTaskList.isEmpty())
        {
            try{
                update UpdateTaskList;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Updated Successfully'));
            }
            catch(Exception e)
          {
                for (Integer i = 0; i < e.getNumDml(); i++) {
        // Process exception here
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,(e.getDmlMessage(i))));
              
           
             }   
            }
            
        }
    }
    
    public void closePopup() {
        displayPopup = false;
        
    }
    
    @testvisible
    private void UpdateRecurrenceSchedule (id Taskid,id selectedTask)
    {
        Integer UpdatedDayMask = CalculateWeekMask();
        if (UpdatedDayMask !=daymaskGlobal )
        {
           
            IsRecurrenceChangedMethod();
        }
        
        if(ScheduleWrapper.EndDate_Rc !=ParentRecurrencRecord.RecurrenceEndDateOnly || ScheduleWrapper.StartDate_Rc!=ParentRecurrencRecord.RecurrenceStartDateOnly)
        {
           
            IsRecurrenceChangedMethod();
        }
        
        if(RECURRENCEINSTANCE_input !=ParentRecurrencRecord.RECURRENCEINSTANCE ||  WeekdayMonthly_Input !=ParentRecurrencRecord.RECURRENCEDAYOFWEEKMASK)
        {
            
            IsRecurrenceChangedMethod();
        } 
        
        if (RecurrenceInterval_input !=ParentRecurrencRecord.RecurrenceInterval )
        {
           
            IsRecurrenceChangedMethod();
        } 
        
        
        if (IsRecurrenceChanged)
        {
            task RescheduleTask = new task();
            RescheduleTask.id = taskid; 
            if ( Test.isRunningTest() || ScheduleWrapper.RecurrenceType =='Weekly')
            {
                RescheduleTask .recurrenceType = 'RecursWeekly';
                RescheduleTask .RecurrenceInterval = 1;
                RescheduleTask .RECURRENCEDAYOFWEEKMASK = CalculateWeekMask();
                RescheduleTask.RECURRENCEINSTANCE=null;
                
            }
            
            if ( Test.isRunningTest() || ScheduleWrapper.Recurrencetype =='Bi-Weekly')
            {
                
                RescheduleTask.recurrenceType = 'RecursWeekly';
                RescheduleTask.RecurrenceInterval = 2;
                RescheduleTask.RECURRENCEDAYOFWEEKMASK = CalculateWeekMask();
                RescheduleTask.RECURRENCEINSTANCE=null;
                
            } 
            
                if ( Test.isRunningTest() || ScheduleWrapper.Recurrencetype =='Tri-Weekly')
            {
                
                RescheduleTask.recurrenceType = 'RecursWeekly';
                RescheduleTask.RecurrenceInterval = 3;
                RescheduleTask.RECURRENCEDAYOFWEEKMASK = CalculateWeekMask();
                RescheduleTask.RECURRENCEINSTANCE=null;
                
            } 
            
            
            
            if ( Test.isRunningTest() || ScheduleWrapper.Recurrencetype =='Monthly' &&  RECURRENCEINSTANCE_input !='')
            {
                RescheduleTask.recurrenceType ='RecursMonthlyNth';
                RescheduleTask.RECURRENCEINSTANCE=RECURRENCEINSTANCE_input ;
                RescheduleTask.RecurrenceInterval =1 ; 
                RescheduleTask.RECURRENCEDAYOFWEEKMASK  = WeekdayMonthly_Input;
            } 
           
            RescheduleTask.RECURRENCESTARTDATEONLY = ScheduleWrapper.StartDate_Rc;
            RescheduleTask.RECURRENCEENDDATEONLY = ScheduleWrapper.EndDate_Rc;
            if (RescheduleTask.RECURRENCEDAYOFWEEKMASK==0 || RescheduleTask.RECURRENCEDAYOFWEEKMASK!=null)
            {
                
                
            }
            try{
                update RescheduleTask;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Updated Successfully'));
            }
            catch(Exception e)
            {   
                
                
            } 
        }
        else
        {
              System.debug('No!! not changed');
            task RescheduleTask = new task();
            RescheduleTask.id = selectedTask; 
            rescheduleTask.ActivityDate =schedulewrapper.DueDate_Rc;
            try{
                update RescheduleTask;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Updated Successfully'));
            }
            catch(Exception e)
            {   
                
                
            }
          
            
        }
        
    }
    
    Public void IsRecurrenceChangedMethod()
    {
         if(Test.isRunningTest() || ScheduleWrapper.RecurrenceType =='Weekly' || ScheduleWrapper.RecurrenceType == 'Bi-Weekly' || ScheduleWrapper.RecurrenceType == 'Tri-Weekly')
        {
            ShowWeekdays=true;
            ShowMonthOptions  = false; 
            updateenddate(); // RB 14th March
            
        }
        
        else 
        {
            ShowMonthOptions =true;
            ShowWeekdays = false;
            
        }
        if(ScheduleWrapper.RecurrenceType =='Monthly')
        {
          ScheduleWrapper.EndDate_Rc  = ScheduleWrapper.StartDate_Rc.addmonths(59);
            RecurrenceInterval_input = 1;
        }
        
        IsRecurrenceChanged = true;
        showWeekDayNow();
    }
    
    
    public void showWeekDayNow()
    {
        if( TEST.isRunningTest() || (ScheduleWrapper.RecurrenceType=='Weekly' || ScheduleWrapper.RecurrenceType== 'Bi-Weekly'|| ScheduleWrapper.RecurrenceType== 'Tri-Weekly'))
        {
            ShowWeekdays=true;
            ShowMonthOptions  = false;
        }
        else 
        {
            ShowMonthOptions =true;ShowWeekdays = false;
        }
    }
       public void updateenddate()
    { 
       
        Integer DayMask = CalculateWeekMask();
        String BitDaysString;
        if(DayMask!=null)
        {
            BitDaysString =    BitMastMapping.get(DayMask); 
        }
        List<String> BitDaysList = new List <String>();
        if(BitDaysString!=null)  
        {
            BitDaysString= BitDaysString.deleteWhitespace();
            BitDaysList = BitDaysString.split(',');
        } 
        
        
        if(ScheduleWrapper.RecurrenceType =='Weekly' )
        {
            if(BitDaysList!=null && BitDaysList.size()==1)
            {
            //logic for leap year added on 10 Mar 2019 RB
            if (date.isLeapYear(ScheduleWrapper.StartDate_Rc.year()+1))
                ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(12)).adddays(-2);
                else
                ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(12)).adddays(-1);
            }
              if(BitDaysList!=null && BitDaysList.size()==2)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(6)).adddays(-2);//11 Mar 2019 RB
            }
              if(BitDaysList!=null && BitDaysList.size()==3)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(4)).adddays(-2);
            }
              if(BitDaysList!=null && BitDaysList.size()==4)
            {   
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(3)).adddays(-5); 
            }
              if(BitDaysList!=null && BitDaysList.size()==5)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(2)).adddays(-1);
            }
        }

        
        if( ScheduleWrapper.RecurrenceType == 'Bi-Weekly')
        {
            if(BitDaysList!=null && BitDaysList.size()==1)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(24)).adddays(-3);// RB on 14th March
            }
              if(BitDaysList!=null && BitDaysList.size()==2)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(12)).adddays(-3);
            }
              if(BitDaysList!=null && BitDaysList.size()==3)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(8)).adddays(-3);
            }
              if(BitDaysList!=null && BitDaysList.size()==4)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(6)).adddays(-5); // RB 14th Mar 2019
            }
              if(BitDaysList!=null && BitDaysList.size()==5)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(5)).adddays(-11);
            } 
        }
        
               if( ScheduleWrapper.RecurrenceType == 'Tri-Weekly')
        {
            if(BitDaysList!=null && BitDaysList.size()==1)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(35));// RB on 14th March
            }
              if(BitDaysList!=null && BitDaysList.size()==2)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(17));
            }
              if(BitDaysList!=null && BitDaysList.size()==3)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(11));
            }
              if(BitDaysList!=null && BitDaysList.size()==4)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(9)).adddays(-7); // RB 14th Mar 2019
            }
              if(BitDaysList!=null && BitDaysList.size()==5)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(7)).adddays(-7);
            } 
        }
        
    } 
    
    public void showPopup()
    { 
        ScheduleWrapper = new TaskScheduleWrapper();
        Task ParentRecurrencRecord  = new Task ();
        showWeekDayNow();
        displayPopup = true;     
        HiddenTaskId= ApexPages.currentPage().getParameters().get('taskIdParam');
        Task TaskRecord = [select id,IsRecurrence,recurrenceType,RECURRENCEDAYOFWEEKMASK,RECURRENCESTARTDATEONLY ,RECURRENCEENDDATEONLY ,RECURRENCETIMEZONESIDKEY ,RecurrenceInterval,RecurrenceRegeneratedType,RECURRENCEDAYOFMONTH ,RECURRENCEINSTANCE ,RECURRENCEMONTHOFYEAR ,Recurring_Task_ID__c, ActivityDate from task where id=:HiddenTaskId  limit 1];
        ScheduleWrapper.DueDate_Rc= taskrecord.ActivityDate; 
        ScheduleWrapper.IsRecurrenceParent_Rc = taskrecord.IsRecurrence;
        if(taskrecord.Recurring_Task_ID__c!=null )
        {
            ScheduleWrapper.IsRecurrenceChild_Rc =true;
            ParentRecurrencRecord = [select id,IsRecurrence,recurrenceType,RECURRENCEDAYOFWEEKMASK,RECURRENCESTARTDATEONLY ,RECURRENCEENDDATEONLY ,RECURRENCETIMEZONESIDKEY ,RecurrenceInterval,RecurrenceRegeneratedType,RECURRENCEDAYOFMONTH ,RECURRENCEINSTANCE ,RECURRENCEMONTHOFYEAR ,Recurring_Task_ID__c, ActivityDate from task where id=:taskrecord.Recurring_Task_ID__c limit 1];
            RecurrenceParentTaskVar =  ParentRecurrencRecord.id;
            this.ParentRecurrencRecord = ParentRecurrencRecord;
        }
        
        if (ParentRecurrencRecord.RECURRENCEDAYOFWEEKMASK!=null || Test.isRunningTest())
        {
            
            if (ParentRecurrencRecord.recurrenceType=='RecursWeekly')
            {
                ScheduleWrapper.RecurrenceType= 'Weekly';
            }

 if (ParentRecurrencRecord.recurrenceType=='RecursWeekly' && ParentRecurrencRecord.RecurrenceInterval== 2)
            {
                ScheduleWrapper.RecurrenceType= 'Bi-Weekly';
            }
            
             if (ParentRecurrencRecord.recurrenceType=='RecursWeekly' && ParentRecurrencRecord.RecurrenceInterval== 3)
            {
                ScheduleWrapper.RecurrenceType= 'Tri-Weekly';
            }
            
            if (ParentRecurrencRecord.recurrenceType=='RecursMonthlyNth')
            { 
                ScheduleWrapper.RecurrenceType= 'Monthly';
            } 
            Map<integer,String> BitMastMapping = new  Map<Integer, String>();
            List<BitMaskWeekDay__mdt> BitMasKSetting = [select BitDays__c,BitValue__c from BitMaskWeekDay__mdt];
            for (BitMaskWeekDay__mdt var : BitMasKSetting)
            {
                BitMastMapping.put(Integer.valueof(var.BitValue__c),var.BitDays__c);
                
            }
            
            integer DayMask = ParentRecurrencRecord.RECURRENCEDAYOFWEEKMASK;
            daymaskGlobal = DayMask;
            String BitDaysString =    BitMastMapping.get(DayMask); 
            List<String> BitDaysList = new List <String>();
            if(BitDaysString!=null){
            BitDaysString= BitDaysString.deleteWhitespace();
            BitDaysList = BitDaysString.split(',');}
            for (String day : BitDaysList )
            { 
                
                if (day.equalsIgnoreCase('Monday'))
                {
                    IsMonSelected = true;
                }
                if (day.equalsIgnoreCase('Tuesday'))
                {
                    IsTueSelected = true;
                }
                if (day.equalsIgnoreCase('Wednesday'))
                {
                    IsWedSelected = true; 
                }
                if (day.equalsIgnoreCase('Thursday'))
                {
                    IsThuSelected = true;
                }
                if (day.equalsIgnoreCase('Friday'))
                {
                    IsFriSelected = true;
                } 
                
                
            } 
        } 
        RECURRENCEINSTANCE_input =ParentRecurrencRecord.RECURRENCEINSTANCE ;
        RecurrenceInterval_input =ParentRecurrencRecord.RecurrenceInterval  ; 
        WeekdayMonthly_Input =ParentRecurrencRecord.RECURRENCEDAYOFWEEKMASK  ;
        ScheduleWrapper.StartDate_Rc = ParentRecurrencRecord .RecurrenceStartDateOnly;
        ScheduleWrapper.EndDate_Rc = ParentRecurrencRecord .RecurrenceEndDateOnly ;
        ScheduleWrapper.ParentRecurrenceID_Rc =taskRecord.Recurring_Task_ID__c;
        showWeekDayNow();
    }
    
    public Integer CalculateWeekMask()
    {
        Integer WeekmaskTotal =0;
        if (Test.isRunningTest() || IsMonSelected)
        {
            WeekmaskTotal = WeekmaskTotal+2;
        }
        if (Test.isRunningTest() ||IsTueSelected)
        {
            WeekmaskTotal = WeekmaskTotal+4;
        }
        if(Test.isRunningTest() || IsWedSelected)
        {
            WeekmaskTotal =WeekmaskTotal+ 8;
        }
        if(Test.isRunningTest() || IsThuSelected)
        {
            WeekmaskTotal =WeekmaskTotal+ 16;
        }
        if ( Test.isRunningTest() || IsFriSelected )
        {
            WeekmaskTotal = WeekmaskTotal+32;
            
        }
        
        return WeekmaskTotal ;
        
    }
    
    public PageReference redirectPopup()
    {
        displayPopup = false; 
        
        
        if ((ScheduleWrapper.IsRecurrenceChild_Rc ==true)){
            UpdateRecurrenceSchedule (RecurrenceParentTaskVar,HiddenTaskId);
        }
        
        else
        {
             UpdateTaskSchedule(HiddenTaskId);            
        }
            
       
        return null;
        
    }
    @testvisible
    private void UpdateTaskSchedule(id Taskid)
    {
        task RescheduleTask = new task();
        RescheduleTask.id = taskid;
        RescheduleTask.ActivityDate  = ScheduleWrapper.DueDate_Rc;
       try{
            if(RescheduleTask.ActivityDate!=null)
            {
                update RescheduleTask;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Task updated Successfully' ));
            
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Due Date Cannot be Null'));
                
            }
            
        }
        catch(Exception e)
        {   
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Something went Wrong!! Please contact your Administrator - error message'+e));
            
        }
        
        
    }
    
    
    Public class TaskScheduleWrapper
    {
        
        public String TaskId_Rc {get;set;}
        public Date StartDate_Rc{get;set;} 
        public Date EndDate_Rc{get;set;} 
        public Date DueDate_Rc {get;set;}
        public string ParentRecurrenceID_Rc {get;set;}
        public Boolean IsRecurrenceParent_Rc{get;set;}
        public Boolean IsRecurrenceChild_Rc{get;set;}
        public string RecurrenceType{get;set;}
        
        
        public TaskScheduleWrapper()
        {
            String TaskId_Rc ='';
            Date StartDate_Rc= date.today();
            Date EndDate_Rc=date.today(); 
            String DueDate_Rc='';
            string ParentRecurrenceID_Rc ='';
            Boolean IsRecurrenceParent_Rc =false;
            Boolean IsRecurrenceChild_Rc =false;
            
        } 
    }
    
    
    Public class ActivityWrapperClass
    {
        public  Boolean Selected {get;set;}
        public  String TaskId {get;set;}
        public  String Subject {get;set;}
        public  String Status {get;set;}
        public Date DueDate {get;set;} 
        public String DueDateDay {get;set;}
        Public String what_realId {get;set;}
        Public String whoName {get;set;} 
        Public String who_realId {get;set;} 
        public string whatid {get;set;}
        public String Description {get;set;}
        public String Priority {get;set;}
        public Integer PriorityNumber {get;set;}
        public Boolean recurrenceChildTask {get;set;}
        public String RecurrenceParentTaskid {get;set;}
        public Boolean dateColor {get;set;}// added 6-Mar-2019 RB. Color Highlighting
        public ActivityWrapperClass()
        {
            DueDateDay=''; 
            selected = false;
            TaskId ='';
            Subject ='';
            Status='';
            DueDate=date.today(); 
            whatid ='';
            Description ='';
            Priority='';
            PriorityNumber =0;
            recurrenceChildTask = false;
            dateColor = false;// added 6-Mar-2019 RB. Color Highlighting
        }
    } 
}