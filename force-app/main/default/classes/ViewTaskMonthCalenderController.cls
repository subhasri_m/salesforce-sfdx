public class ViewTaskMonthCalenderController { 
    
    public integer DaysinMonth{get;set;}
    Public String CheckImage {get;set;}
    Public String TableData {get;set;}
    Public String MonthYear {get;Set;}
    Map<Datetime,List<Task>> MonthDaysMap = new Map<DateTime, List<Task>>();
    Map<Task,Account> TaskAccMap = new Map<Task,Account>();
    List<Task> A_Recurrence = new List<Task>();
    List<Task> B_Recurrence = new List<Task>();
    List<Task> C_Recurrence = new List<Task>();
    List<Task> A_NonRecurrence = new List<Task>();
    List<Task> B_NonRecurrence = new List<Task>();
    List<Task> C_NonRecurrence = new List<Task>(); 
    Map<Datetime,List<List<Task>>> MapDateLists =  new Map<Datetime,List<List<Task>>>(); 
    date CalendermonthDate = Date.today();
    
    public ViewTaskMonthCalenderController()
    {
        getTasks(CalendermonthDate);
        getMonthDays(CalendermonthDate);
        CheckImage = '<table><tr><td> <img id="theImage" src="https://fleetpride--uat--c.cs35.content.force.com/servlet/servlet.ImageServer?id=0151k000000DXs5&oid=00D1k000000Cn4N&lastMod=1538672109000" width="25" height="25" alt="Description of image here"/></td></tr></table>';
        System.debug ('Image-->'+checkimage);
    }
    
      public PageReference ScheduleSalesCallClicked() 
    { 
        PageReference redirect = new PageReference('/apex/scheduleSalesCall');  
        redirect.setRedirect(true);  
        return redirect;
    }
    
     public  void PreviousMonthClicked() 
    { 
        CalendermonthDate = CalendermonthDate.addMonths(-1);
        getTasks(CalendermonthDate);
          getMonthDays(CalendermonthDate);
    }

     public void NextMonthClicked() 
    { 
          CalendermonthDate = CalendermonthDate.addMonths(+1);
          getTasks(CalendermonthDate);
          getMonthDays(CalendermonthDate);
    }    
    
     public PageReference redirectToDayView () 
    { 
        PageReference redirect = new PageReference('/apex/ShowUserDayTasks');  
        redirect.setRedirect(true);  
        return redirect;
    }
    
    Public void getMonthDays(Date MonthForDate)
    {
        System.debug('date'+MonthForDate);
        DaysinMonth  = Date.daysInMonth(MonthForDate.year(), MonthForDate.month());
        Integer value = DaysinMonth;
        List<date> Monthdates = new List<Date>();
        MonthYear =  ((datetime)MonthForDate).format('MMMMM') + ' '+MonthForDate.year();
        Map<Datetime,String> MonthDays = new Map<DateTime, String>();
        
        
        for (integer i=1; i<=value; i++ )
        {
            Datetime DateVar = (Datetime)date.parse(MonthForDate.month()+'/'+i+'/'+MonthForDate.Year());
            string dayVar=   DateVar.adddays(1).format('EEEE'); 
            // TableData  =TableData  + '<tr><td>'+ Date.today().month()+'/'+i+'/'+Date.today().Year()+ '</td><td>'+ dayVar+'</td></tr>';
            MonthDays.put (DateVar,dayVar);
        }
        TableData  = '<tr style="Font-size:15px;font-weight:bold "><td align="center">Monday</td><td align="center">Tuesday</td><td align="center">Wednesday</td><td align="center">Thursday</td><td align="center">Friday</td></tr>';
        for (Datetime DateValue: MonthDays.keySet()){
            
            if (DateValue.adddays(1).day()==1)
            {
                if( MonthDays.get(DateValue) == 'Tuesday')
                {
                    TableData   = TableData  +' <td></td>';
                }
                else if (MonthDays.get(DateValue) == 'Wednesday')
                {
                    TableData   = TableData  +' <td></td><td></td>';
                }
                
                else if (MonthDays.get(DateValue) == 'Thursday')
                {
                    TableData   = TableData  +' <td></td><td></td><td></td>';
                }
                
                else if (MonthDays.get(DateValue) == 'Friday')
                {
                    TableData   = TableData  +' <td></td><td></td><td></td><td></td>';
                }
            }
            
            if (MonthDays.get(DateValue)<>'Saturday' && MonthDays.get(DateValue)<>'Sunday'){
                if(MonthDays.get(DateValue) =='Monday')
                {
                    TableData  = TableData  +'</tr><tr>';
                }
               
                if ((MonthDaysMap.get(DateValue))<>null){
                    TableData  =TableData  + '<td align="Right"  bgcolor="#f2f2f2"  ><div style = "color:black;font-size:16px;padding-right:10px"><b>'+ DateValue.adddays(1).Month()+'-'+DateValue.adddays(1).day()+'-'+DateValue.adddays(1).year() + '</b></div>'+' <div style="padding-right:10px;font-weight:bold; font-size:16px;color:black">Total Calls :<b> <a href="/apex/ShowUserDayTasks?ActivityDate=' +DateValue.adddays(1).month()+'/'+DateValue.adddays(1).Day()+'/'+DateValue.adddays(1).Year()+'" target="_blank">'+(MonthDaysMap.get(DateValue)).size()+'</a></b></div>';
                }
                else
                {
                    TableData  =TableData  + '<td align="Right" bgcolor="#f2f2f2"  ><div style = "color:black;font-size:16px;padding-right:10px"><b>'+ DateValue.adddays(1).Month()+'-'+DateValue.adddays(1).day()+'-'+DateValue.adddays(1).year() +  '</b></div>'+' <div style ="padding-right:10px;font-weight:bold;font-size:16px;color: black">Total Calls : <b>0</b></div>';
                }
                
                TableData = TableData+'<table bgcolor="white" Border="0" style = " margin:10px; border-collapse: collapse;"><tr> <td Align="center" Style ="Padding:5px" >'+
                    '<img id="theImage" src="/resource/1556680194000/Category" height="25" width="25"></td><td Align="center"  Style ="Padding:5px" >'+ 
                    '<img id="theImage" style="padding-top:1px" src="/resource/1548738231000/RecurrenceIcon" width="25" height="25" ></td><td Align="center" Style ="Padding:5px" >'+ 
                   '<img id="theImage" src="/resource/1556530456000/NonRecurrence" width="32" height="28" ></td></tr>';
                
                Integer A_rec_size =0;
                Integer B_rec_size =0;
                Integer C_rec_size =0;
                Integer A_Nonrec_size =0;
                Integer B_Nonrec_size =0;
                Integer C_Nonrec_size =0;
                
                if(MapDateLists.get(DateValue) <>null)
                {
                    A_rec_size = MapDateLists.get(DateValue)[0].size();
                }
                if(MapDateLists.get(DateValue) <>null)
                {
                    B_rec_size = MapDateLists.get(DateValue)[1].size();
                }
                if(MapDateLists.get(DateValue) <>null)
                {
                    C_rec_size = MapDateLists.get(DateValue)[2].size();
                }
                if(MapDateLists.get(DateValue) <>null)
                {
                    A_Nonrec_size = MapDateLists.get(DateValue)[3].size();
                }
                if(MapDateLists.get(DateValue) <>null)
                {
                    B_Nonrec_size = MapDateLists.get(DateValue)[4].size();
                }
                if(MapDateLists.get(DateValue) <>null)
                {
                    C_Nonrec_size = MapDateLists.get(DateValue)[5].size();
                } 
                
                TableData = TableData +'<tr><td Align="center"><b>A</b></td><td align="center">'+A_rec_size +'</td><td align="center">'+A_Nonrec_size+'</td></tr><tr><td Align="center"><b>B</b></td><td align="center">'+B_rec_size+'</td><td align="center">'+B_Nonrec_size+'</td></tr><tr><td Align="center"><b>C</b></td><td align="center">'+C_rec_size+'</td><td align="center">'+C_Nonrec_size+'</td></tr></table> </td>';
            }
        }
        TableData = tabledata + '</tr>';
    }
    
    public static String formatDate(Date d) {
    return d.year() + '-' + d.month() + '-' + d.day();
}
    
    public void getTasks(Date MonthForDate)
    {
        String LoggedinUserId = UserInfo.getUserId();
        Datetime TempDate = MonthForDate.toStartOfMonth();
        Date myDate = date.newinstance(TempDate.year(), TempDate.month(),1);  
        Date lastDayOfMonth = Date.newInstance(TempDate.year(), TempDate.month(),  Date.daysInMonth(TempDate.year(), TempDate.month()));
        DaysinMonth  = Date.daysInMonth(MonthForDate.year(), MonthForDate.month()); 
        System.debug('first day of month'+myDate);
        System.debug('Last day of month'+lastDayOfMonth);
        
        List<task> DayTaskList;
        Set<String> AccountWhatIDSet = new Set<String>(); 
        List<Task> TasksList = [select id,Recurring_Task_ID__c,whatid, Priority,Description,Subject,what.name,status, ActivityDate from task where Ownerid =:LoggedinUserId and ( ActivityDate>=LAST_N_DAYS:50 and ActivityDate<=NEXT_N_DAYS:50 ) and whatid<>null ALL ROWS] ;
        if(Test.isRunningTest())
            TasksList = [select id,Recurring_Task_ID__c,whatid, Priority,Description,Subject,what.name,status, ActivityDate from task LIMIT 10];
        for(Task var: TasksList)
        {
            if(var.whatid!=null)
            AccountWhatIDSet.add(var.whatid);
        }
        List<Account> AccList = [select id,Customer_Call_Classification__c from account where id in :AccountWhatIDSet];
        
        for(Task var: TasksList)
        {
            for (Account acc :AccList)
            {
                if (var.WhatId == acc.id)
                {
                    TaskAccMap.put(var, acc);
                } 
            }
        }  
        
      
        for (integer i=1 ; i<=DaysinMonth; i++){
            DayTaskList =new List<Task>(); 
            List<List<Task>> TaskLists;
            TaskLists = new List<List<Task>>();
            A_Recurrence = new List<Task>();
            B_Recurrence= new List<Task>();
            C_Recurrence= new List<Task>();
            A_NonRecurrence = new List<Task>();
            B_NonRecurrence = new List<Task>();
            C_NonRecurrence = new List<Task>();
            for (Task taskvar : TasksList )
            {                 
                if (TempDate ==taskvar.ActivityDate )  
                {
                    DayTaskList.add(taskvar); 
                }   
            } 
            if(DayTaskList.size()<>0){
             MonthDaysMap.put(TempDate,DayTaskList);
                for (Task taskvar : DayTaskList ){ 
                    
                    if (taskvar.Recurring_Task_ID__c!=null)
                    {
                    
                        if(TaskAccMap.containsKey(taskvar))
                        {
                            if(TaskAccMap.get(taskvar).Customer_Call_Classification__c=='A')
                            {
                                A_Recurrence.add(taskvar);
                                
                            }
                            if(TaskAccMap.get(taskvar).Customer_Call_Classification__c=='B')
                            {
                                
                                B_Recurrence.add(taskvar); 
                                
                            }
                            if(TaskAccMap.get(taskvar).Customer_Call_Classification__c=='C')
                            { 
                                C_Recurrence.add(taskvar); 
                            }
                        }
                    }
                    if (taskvar.Recurring_Task_ID__c ==null)
                    {
                        if(TaskAccMap.containsKey(taskvar))
                        {
                            if( TaskAccMap.get(taskvar).Customer_Call_Classification__c=='A')
                            {
                                A_NonRecurrence.add(taskvar);
                            }
                            if(TaskAccMap.get(taskvar).Customer_Call_Classification__c=='B')
                            {
                                B_NonRecurrence.add(taskvar);
                            }
                            if(TaskAccMap.get(taskvar).Customer_Call_Classification__c=='C')
                            {
                                C_NonRecurrence.add(taskvar);
                            }
                        }
                    }
                    
                    TaskLists.add(A_Recurrence);
                    TaskLists.add(B_Recurrence);
                    TaskLists.add(C_Recurrence);
                    TaskLists.add(A_NonRecurrence);
                    TaskLists.add(B_NonRecurrence);
                    TaskLists.add(C_NonRecurrence);
                  
                    
                }
                MapDateLists.put(TempDate, TaskLists);
            }
            
           
            TempDate= TempDate.addDays(1); 
        }
      
        
    }
    
    
}