@isTest
public class FP_FetchCurrentContactUserIdTest {
	@isTest
    static void getContactUserIdTest(){
        Account accObj = new Account(Name='Test Name',Type='Customer');
        insert accObj;
        
        Contact conObj = new Contact();
        conObj.FirstName = 'First Name';
        conObj.LastName = accObj.Name;
        conObj.AccountId = accObj.Id;
        conObj.Email = 'test@gmail.com';
        insert conObj;
       
        Test.startTest();
        	FP_FetchCurrentContactUserId.getContactUserId(conObj.Id);
        Test.stopTest();  
    }
}