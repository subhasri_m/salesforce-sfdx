global with sharing class CCPDCQuickWishlistController {
    @RemoteAction
    global static ccrz.cc_RemoteActionResult addBulk(ccrz.cc_RemoteActionContext ctx, String data, Boolean isWillCall, Boolean isShipRemainder, String localDC) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            // reusing data structure from CCPDCQuickOrderHelper.QuickOrderItem as CCPDCQuickWishlistHelper.QuickOrderItemWL
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Id contactId = ccrz.cc_CallContext.currContact.Id;
            List<CCPDCQuickWishlistHelper.QuickOrderItemWL> input = (List<CCPDCQuickWishlistHelper.QuickOrderItemWL>) JSON.deserialize(data, List<CCPDCQuickWishlistHelper.QuickOrderItemWL>.class);
            List<CCPDCQuickWishlistHelper.QuickOrderItemWL> items = CCPDCQuickWishlistHelper.getQuickOrderItemsWL(input);
            CCPDCUserPermissionsHelper.UserPermissions permissions = CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);
            List<CCPDCQuickWishlistHelper.QuickOrderItemWL> newItems = new List<CCPDCQuickWishlistHelper.QuickOrderItemWL>();

            Map<String,Object> output = new Map<String,Object>();
            output.put('items', items);
            List<ccrz__E_PageLabel__c> pgLbls =  [ Select Name, ccrz__PageLabelId__c, ccrz__ValueRT__c, ccrz__Value__c FROM ccrz__E_PageLabel__c WHERE Name like 'CC_PDC_Restrict%' ];

            String CC_PDC_Restrict_BackOrderModal_Line1 = '', CC_PDC_Restrict_BackOrderModal_Line2 = '', CC_PDC_Restrict_WillCall_NoPerm = '', CC_PDC_Restrict_BackOrderModal_Wc_Ship = '';

            for (ccrz__E_PageLabel__c lbl : pgLbls){
                if(lbl.Name == 'CC_PDC_Restrict_BackOrderModal_Line1' && lbl.ccrz__Value__c != null){
                    CC_PDC_Restrict_BackOrderModal_Line1 = lbl.ccrz__Value__c;
                } else if(lbl.Name == 'CC_PDC_Restrict_BackOrderModal_Line2' && lbl.ccrz__Value__c != null){
                    CC_PDC_Restrict_BackOrderModal_Line2 = lbl.ccrz__Value__c;
                } else if(lbl.Name ==  'CC_PDC_Restrict_WillCall_NoPerm' && lbl.ccrz__Value__c != null){
                    CC_PDC_Restrict_WillCall_NoPerm = lbl.ccrz__Value__c;
        } else if(lbl.Name ==  'CC_PDC_Restrict_BackOrderModal_Wc_Ship' && lbl.ccrz__Value__c != null){
          CC_PDC_Restrict_BackOrderModal_Wc_Ship = lbl.ccrz__Value__c;
        }               
            }

            System.debug('CCPDCQuickWishlistController items ' + items);
            if (CCPDCQuickWishlistHelper.validate(items)) {
                List<String> skulist = new List<String>();
                for(CCPDCQuickWishlistHelper.QuickOrderItemWL ci : items){
                    skulist.add(ci.sku);
                }

                Map<String, CCPDCInventoryHelper.Inventory> productInventory = CCPDCInventoryHelper.getInventory(skulist, accountId);
                output.put('inv', productInventory);
                String cartId = null;
                for(CCPDCQuickWishlistHelper.QuickOrderItemWL ci : items) {
                    Decimal qtyInput = ci.quantity;
                    String sku = ci.sku;
                    CCPDCInventoryHelper.Inventory inv = productInventory.get(sku);
                    Decimal ATS = (inv != null) && (inv.totalQuantity != null) ? inv.totalQuantity : 0 ;
                    Decimal localDcQty = 0;
                    String name = ci.name != null ? ci.name : '';
                    if( (inv != null) && (inv.primaryQuantity != null)
                            && (inv.primaryLoc != null) && (inv.primaryLoc == localDC)) {
                        localDcQty = inv.primaryQuantity;
                    }

                    if(isWillCall) {
                        if(permissions.allowWillCall) {
                            if(qtyInput >= localDcQty){
                                // clone the item and adjust the correct values -> only add whats available in inv.primaryQuantity 
                                CCPDCQuickWishlistHelper.QuickOrderItemWL newItem = ci.clone();
                                newItem.quantity = (Integer) localDcQty;
                                newItems.add(newItem);
                            }
                            else {
                                newItems.add(ci); // satisfy all qtyInput
                            }
                        }
                        else {
                            // will call is not allowed, build message to send back error
                            response.success = false;
                            response.messages.add(CCAviPageUtils.createMessage(ccrz.cc_bean_Message.MessageSeverity.INFO, null, CC_PDC_Restrict_WillCall_NoPerm, ''));
                            return response;
                        }
                    }
                    else {
                        // this is shipping or will call ship later, backorders are allowed here                        
                        if(qtyInput > ATS ) {
                            // backorders exist here, check for permission
                            if(permissions.allowBackorders) {
                // TODO CHECK IF IS WILL CALL OR SHIP REMAINDER
                /* Don't think this will get called so commenting it out
                if(isShipRemainder) {
                    // satisfy all qtyInput, let user know there will be a backorder
                    Decimal newCiShippingQty = inv != null && inv.totalQuantity != null ? inv.totalQuantity : 0;
                    Decimal newCiBackorderQuantity = qtyInput - newCiShippingQty;
                    Decimal willCallPickupQty = 0, shipQty = 0;
                    if(inv != null) {
                      if(inv.primaryQuantity != null) {
                        willCallPickupQty = inv.primaryQuantity;
                        shipQty = newCiShippingQty - willCallPickupQty;
                      }
                    }                    
                    String m1 = CC_PDC_Restrict_BackOrderModal_Wc_Ship.replace('{willCallPickupQty}', String.valueOf(willCallPickupQty)).replace('{shipQty}', String.valueOf(shipQty)).replace('{boQty}', String.valueOf(newCiBackorderQuantity));
                    String m2 = '';
                    response.messages.add(CCAviPageUtils.createMessage(ccrz.cc_bean_Message.MessageSeverity.INFO, null, m1, ''));
                    response.messages.add(CCAviPageUtils.createMessage(ccrz.cc_bean_Message.MessageSeverity.INFO, null, m2, ''));
                    newItems.add(ci);
                } else{
                */
                    // satisfy all qtyInput, let user know there will be a backorder
                    Decimal diff = qtyInput - ATS;
                    String m1 = CC_PDC_Restrict_BackOrderModal_Line1.replace('{userQty}', String.valueOf(qtyInput)).replace('{prodName}', sku);
                    String m2 = CC_PDC_Restrict_BackOrderModal_Line2.replace('{invQty}', String.valueOf(ATS)).replace('{diff}', String.valueOf(diff));
                    response.messages.add(CCAviPageUtils.createMessage(ccrz.cc_bean_Message.MessageSeverity.INFO, null, m1, ''));
                    response.messages.add(CCAviPageUtils.createMessage(ccrz.cc_bean_Message.MessageSeverity.INFO, null, m2, ''));
                    newItems.add(ci);
                //}
                            } else {
                                // only add ATS
                                CCPDCQuickWishlistHelper.QuickOrderItemWL newItem = ci.clone();
                                newItem.quantity = (Integer) ATS;
                                newItems.add(newItem);
                            }
                        }
                        else {
                            newItems.add(ci); // qtyInput < ATS, satisfy all qtyInput
                        }
                    }

                } // end of for each

                // finally, add updated items to cart
                if(newItems != null && newItems.size() > 0) {
                    cartId = CCPDCQuickWishlistHelper.addToCart(newItems);
                    System.debug('CCPDCQuickWishlistController newItems ' + newItems);
                    output.put('newItems', newItems);
                }

                if (cartId != null) {
                    output.put('cartId', cartId);
                    CCAviPageUtils.buildResponseData(response, true, output);
                }
                else {
                    CCAviPageUtils.buildResponseData(response, false, output);
                }
            }
            else {
                CCAviPageUtils.buildResponseData(response, false, output);
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                    new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }
}