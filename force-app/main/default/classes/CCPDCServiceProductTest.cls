@isTest
public class CCPDCServiceProductTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }

        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.SERVICE_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccServiceProduct' => 'c.CCPDCServiceProduct'
                }
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void fetchTest() {
        util.initCallContext();
        List<Product_Cross_Reference__c> lstInputPdt = new List<Product_Cross_Reference__c>();

        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String,Object> response = null;
        
		ccrz__E_Product__c product1 = product;
		ccrz__E_Product__c product2 = new ccrz__E_Product__c();
        product2 = product;
        Product_Cross_Reference__c crossProduct = new Product_Cross_Reference__c();
        crossProduct.Related_to_Product__c = product2.Id;
        crossProduct.Brand_Name__c = 'testName';
        crossProduct.Interchange_Part_Number__c = 'sc1600';
        insert crossProduct;
        Product_Cross_Reference__c crossProduct2 = new Product_Cross_Reference__c();
        crossProduct2.Related_to_Product__c = product2.Id;
        crossProduct2.Brand_Name__c = 'testName';
        crossProduct2.Interchange_Part_Number__c = 'sc16001';
        insert crossProduct2;
        Product_Cross_Reference__c crossProduct3 = new Product_Cross_Reference__c();
        crossProduct3.Related_to_Product__c = product2.Id;
        crossProduct3.Brand_Name__c = '';
        crossProduct3.Interchange_Part_Number__c = 'sc1600';
        insert crossProduct3;
        
        lstInputPdt.add(crossProduct2);
        lstInputPdt.add(crossProduct3);
        
        List<Map<String, Object>> lstmap = new List<Map<String, Object>>();
        Map<String, Object> mapStrObj  = new Map<String, Object>();
        mapStrObj.put('productList',lstInputPdt);
        lstmap.add(mapStrObj);
        
        
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccAPIProduct.PRODUCTLIST => lstmap
        };
        
        Test.startTest();
        CCPDCServiceProduct svcPdt = new CCPDCServiceProduct();
        response = svcPdt.fetch(request);
        Test.stopTest();

        System.assert(response != null);
    }

    static testmethod void searchTest() {
        util.initCallContext();
		ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccService.SEARCHSTRING => 'searchString',
                ccrz.ccService.SEARCHRESULTS => '{searchString}'
        };
		// Setup data of related product for code coverage             
		ccrz__E_RelatedProduct__c relProduct = new ccrz__E_RelatedProduct__c();
        relProduct.ccrz__Product__c = product.id;
        relProduct.ccrz__RelatedProductType__c = 'Supercession';
        relproduct.ccrz__RelatedProduct__c = product.Id;
        insert relProduct;
        
        Test.startTest();
        response = ccrz.ccAPIProduct.search(request);
        CCPDCServiceProduct svcPdt = new CCPDCServiceProduct();
        svcPdt.extendSearch('Product',request);
        Test.stopTest();

        System.assert(response != null);
    }

	static testmethod void extendSearchTest() {
        util.initCallContext();
		ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccService.SEARCHSTRING => 'searchString',
                ccrz.ccService.SEARCHRESULTS => 'searchString'
        };
		// Setup data of related product for code coverage     
		product.ccrz__ProductStatus__c = 'Not Orderable';     
        update product;
		ccrz__E_RelatedProduct__c relProduct = new ccrz__E_RelatedProduct__c();
        relProduct.ccrz__Product__c = product.id;
        //relProduct.ccrz__Product__r.ccrz__ProductStatus__c = 'Not Orderable';
        relProduct.ccrz__RelatedProductType__c = 'Supercession';
        relproduct.ccrz__RelatedProduct__c = product.Id;
        insert relProduct;
        
        Test.startTest();
        CCPDCServiceProduct svcPdt = new CCPDCServiceProduct();
        ccrz.ccAPIProduct.search(request);
        response = svcPdt.extendSearch('Product',request);
        Test.stopTest();

        System.assert(response != null);
    }	
    
    static testmethod void getFieldsMapTest() {
        util.initCallContext();
		ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccService.SEARCHSTRING => 'Product'
        };
		// Setup data of related product for code coverage             
		ccrz__E_RelatedProduct__c relProduct = new ccrz__E_RelatedProduct__c();
        relProduct.ccrz__Product__c = product.id;
        relProduct.ccrz__RelatedProductType__c = 'Supercession';
        relproduct.ccrz__RelatedProduct__c = product.Id;
        insert relProduct;
        
        CCPDCServiceProduct srvcpdt = new CCPDCServiceProduct();
        Test.startTest();
        response = srvcpdt.getFieldsMap(request);
        Test.stopTest();

        System.assert(response != null);
    }
    
    static testmethod void getSubQueryMapTest() {
        util.initCallContext();
		ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccService.SEARCHSTRING => 'Product'
        };
		// Setup data of related product for code coverage             
		ccrz__E_RelatedProduct__c relProduct = new ccrz__E_RelatedProduct__c();
        relProduct.ccrz__Product__c = product.id;
        relProduct.ccrz__RelatedProductType__c = 'Supercession';
        relproduct.ccrz__RelatedProduct__c = product.Id;
        insert relProduct;
        
        CCPDCServiceProduct srvcpdt = new CCPDCServiceProduct();
        Test.startTest();
        response = srvcpdt.getSubQueryMap(request);
        //srvcpdt.fetch(request);
        Test.stopTest();

        System.assert(response != null);
    }
    
    static testmethod void formatCrossReferencesTest() {
        util.initCallContext();
       List<Product_Cross_Reference__c> response = null;
        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        List<Product_Cross_Reference__c> lstCrsRef = new List<Product_Cross_Reference__c>();
        Product_Cross_Reference__c crossProduct = new Product_Cross_Reference__c();
        crossProduct.Related_to_Product__c = product.Id;
        crossProduct.Brand_Name__c = 'testName';
        crossProduct.Interchange_Part_Number__c = 'sc1600';
        insert crossProduct;
        
        Product_Cross_Reference__c crossProduct2 = new Product_Cross_Reference__c();
        crossProduct2.Related_to_Product__c = product.Id;
        crossProduct2.Brand_Name__c = 'testName';
        crossProduct2.Interchange_Part_Number__c = 'sc1600';
        insert crossProduct2;
        lstCrsRef.add(crossProduct);
        lstCrsRef.add(crossProduct2);
        
        
        Test.startTest();
        response = CCPDCServiceProduct.formatCrossReferences(lstCrsRef);
        Test.stopTest();

        System.assert(response != null);
    }
    
    static testmethod void formatCrossReferencesTest2() {
        util.initCallContext();
       List<Product_Cross_Reference__c> response = null;
        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        List<Product_Cross_Reference__c> lstCrsRef = new List<Product_Cross_Reference__c>();
        Product_Cross_Reference__c crossProduct = new Product_Cross_Reference__c();
        crossProduct.Related_to_Product__c = product.Id;
        crossProduct.Interchange_Part_Number__c = 'sc1600';
        insert crossProduct;
        lstCrsRef.add(crossProduct);
        
        
        Test.startTest();
        response = CCPDCServiceProduct.formatCrossReferences(lstCrsRef);
        Test.stopTest();

        System.assert(response != null);
    }

}