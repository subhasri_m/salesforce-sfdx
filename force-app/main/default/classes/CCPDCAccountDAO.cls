public without sharing class CCPDCAccountDAO {
    /**
*
* General query for all accounts the current user can access.
*
*/
    public static Map<Id, Account> queryAccessibleAccounts(){
        Map<Id,Account> mainAccounts = new Map<Id,Account>([
            SELECT
            Id
            , Name
            , ParentId
            FROM
            Account
        ]);
        
        Map<Id,Account> childAccounts = new Map<Id,Account>([
            SELECT
            Id
            , Name
            , ParentId
            FROM
            Account
            WHERE
            ParentId in :mainAccounts.keySet()
        ]);
        
        mainAccounts.putAll(childAccounts);
        return mainAccounts;
    } 
    
    /**
*
* General query for all accounts the current user can access.
*
*/
    public static Map<Id, Account> queryAccessibleAccountsWithAddress(){
        Map<Id,Account> mainAccounts = new Map<Id,Account>([
            SELECT
            Id
            , Name
            , ParentId
            , ShippingStreet
            , ShippingCity
            , ShippingState
            , ShippingPostalCode
            FROM
            Account
        ]);
        
        Map<Id,Account> childAccounts = new Map<Id,Account>([
            SELECT
            Id
            , Name
            , ParentId
            , ShippingStreet
            , ShippingCity
            , ShippingState
            , ShippingPostalCode
            FROM
            Account
            WHERE
            ParentId in :mainAccounts.keySet()
        ]);
        
        mainAccounts.putAll(childAccounts);
        return mainAccounts;
    }
    
    public static Account getAccountPermissions(Id accountId) {
        Account accountPermissions = [
            SELECT 
            Id, 
            Name,
            ParentId,
            Parent.ParentId,
            Parent.Parent.ParentId,
            Account_Status__c,
            Tax_Exempt__c,
            OK_to_Backorder__c,
            Will_Call_Allowed__c,
            Requires_PO__c,
            Ship_from_Location__c,
            RecordType.DeveloperName
            FROM 
            Account
            WHERE
            Id = :accountId
        ];
        return accountPermissions;
    }
    
    public static List<Account> getAccountHierarchy(Id accountId) {
        List<Account> accounts = [
            SELECT 
            Id, 
            Name,
            AccountNumber,
            BillingStreet,
            BillingCity,
            BillingState,
            BillingPostalCode,
            BillingCountry,
            ShippingStreet,
            ShippingCity,
            ShippingState,
            ShippingPostalCode,
            ShippingCountry,
            ccrz__E_AccountGroup__c
            FROM 
            Account
            WHERE
            Id = :accountId OR ParentId = :accountId OR Parent.ParentId = :accountId OR Parent.Parent.ParentId = :accountId
        ];
        return accounts;
    }
    
    public static List<Account> getAccountFreeShipping(Id accountId) {
        List<Account> accounts = [
            SELECT
            Id,
            LTL_Freight_Threshold__c,
            Parcel_Freight_Threshold__c,
            Parcel_Weight_Limit__c
            FROM Account WHERE ID =: accountID
        ];
        return accounts;
    }
    
    public static Account getAccount(Id accountId) {
        Account acc = [
            SELECT 
            Id, 
            Name,
            Iseries_Company_code__c,
            ISeries_Customer_Account__c,
            ISeries_Customer_Branch__c,
            Ship_from_Location__c,
            Tax_Exempt__c,
            RecordType.DeveloperName,
            National_Account_Group__c,
            CC_FP_Preferred_Ship_From_Location__c,
            Local_Delivery_Allowed__c
            FROM 
            Account
            WHERE
            Id = :accountId
        ];
        return acc;
    }
    
    // warning, this has recursive queries, will only check 4 levels
    public static Account getTopLevelAccount(Id accountId) {
        Account topLevelAccount = null;
        Integer count = 4;
        Id currentId = accountId;
        while ((topLevelAccount == null) || count != 0) {
            Account a = [Select Id, ParentId From Account where Id =: currentId limit 1];
            if (a.ParentID != null) {
                currentId = a.ParentID;
            }
            else {
                topLevelAccount = a;
            }
            count--;
        }
        return topLevelAccount;
    }

    // method to get all accounts having same NAMgroup number
    // parentId != null // Picking all accounts except Header account
    // It is for Company 1 only
	public static List<Account> getAccountsFromGroup(String NAMgroup, string headerAccid){
        List<Account> acclst = new List<Account>();
        acclst = [Select Id, Name, OwnerId from Account where National_Account_Group__c =:NAMgroup and Iseries_company_code__c = '1' and id !=:headerAccid and parentId != null limit 5000];
        return acclst;
    }   
}