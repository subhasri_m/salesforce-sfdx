global class BatchStatusEmailNotify implements schedulable {
    
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new Batch_UpdateMonthlyCallIndicator());
    } 
    
    public List<Crontrigger> CronTriggerList = new List<Crontrigger>(); 
    public List<AsyncApexJob> AsyncApexJobList = new List<AsyncApexJob>(); 
    public List<CronTriggerWrapper> CronTriggerRecords  { get; set; }
    Map<String, Batch_Tracker__mdt> Batch_TrackerMap = new Map<String, Batch_Tracker__mdt>(); 
    Set<String> BatchNamesSet = new Set<String>();  
    Set<String> ActiveBatches = new Set<String>();
    Map<String, String> UseridNameMap = new Map<String, String>();
    Integer i = 1;
    
    public BatchStatusEmailNotify ()
    {
        FillBatchNames();
        LoadDataSendEmail(); 
    }
    
    public void UserData (set<String> UserIDs)
    { 
        List <User> userlist = [select id, name from user where id in : UserIDs];
        for(User users: userlist)  {UseridNameMap.put(users.id, users.name);}
    }
    
    public void FillBatchNames()
    {
        List<Batch_Tracker__mdt> ListMdt = [select id, DeveloperName, Batch_Description__c, Business_Unit__c from Batch_Tracker__mdt];
        for (Batch_Tracker__mdt md : ListMdt)
        {
            Batch_TrackerMap.put(md.DeveloperName,md); 
        }
        
        for (String obj : Batch_TrackerMap.keySet())
        {BatchNamesSet.add(obj);}
    }
    
    public void LoadDataSendEmail()
    {
        CronTriggerList = [select CronJobDetail.Name, NextFireTime,PreviousFireTime,State,StartTime,Ownerid,TimeZoneSidKey from CronTrigger where State in ('Waiting','Acquired','Executing') or NextFireTime != NULL];
        CronTriggerRecords = new List<CronTriggerWrapper>();
        AsyncApexJobList = [SELECT ApexClass.name, Status FROM AsyncApexJob where status in ('processing', 'Preparing','Holding')];
        set<String> UserIds = new set<String>();  
        Set<Crontrigger>   CronTriggerSet = new Set<Crontrigger>();
        Set<AsyncApexJob> AsyncApexJobSet = new Set<AsyncApexJob>(); 
        set<String> ApexClassNameSet = new set<String>();
        for (AsyncApexJob sc: AsyncApexJobList)
        {
            if(!ApexClassNameSet.contains(sc.apexclass.name))
            {
                AsyncApexJobSet.add(sc);
                ApexClassNameSet.add(sc.apexclass.name);
            }
        } 
        CronTriggerSet.addAll(CronTriggerList);
        for (Crontrigger Obj : CronTriggerList) { UserIds.add(obj.Ownerid);}
        UserData(UserIds);
        
        for (String BatchSet: Batch_TrackerMap.keySet())
        {  
            CronTriggerWrapper wrapper = new CronTriggerWrapper();
            for (Crontrigger Obj : CronTriggerSet)
            {   
                if ((obj.CronJobDetail.Name.deleteWhitespace()).contains(BatchSet.deleteWhitespace())){
                    
                    ActiveBatches.add(BatchSet);
                    wrapper.JobName = obj.CronJobDetail.Name;
                    wrapper.NextFireTime  = obj.NextFireTime.format('MM/dd/yyyy HH:mm:ss','America/Mexico_City');
                    if (obj.PreviousFireTime!=null){wrapper.previousFireTime =obj.PreviousFireTime.format('MM/dd/yyyy HH:mm:ss','America/Mexico_City');}
                    wrapper.isRunning = true;
                    wrapper.Status  = obj.State;
                    wrapper.Owner =UseridNameMap.get(obj.Ownerid);
                    wrapper.StartTime = obj.StartTime.format('MM/dd/yyyy HH:mm:ss','America/Mexico_City');
                    wrapper.Description = Batch_TrackerMap.get(BatchSet).Batch_Description__c;
                    wrapper.BU = Batch_TrackerMap.get(BatchSet).Business_Unit__c;
                    CronTriggerRecords.add(wrapper);
                } 
            } 
            
            for (AsyncApexJob apexjob: AsyncApexJobSet)
            {
                if ((apexjob.ApexClass.name.deleteWhitespace()).contains(BatchSet.deleteWhitespace())){
                    
                    ActiveBatches.add(BatchSet);
                    wrapper.JobName = apexjob.ApexClass.name;
                    wrapper.NextFireTime  = '';
                    wrapper.isRunning = true;
                    wrapper.Status  = apexjob.status;
                    wrapper.Owner ='';
                    wrapper.StartTime ='';
                    wrapper.Description = Batch_TrackerMap.get(BatchSet).Batch_Description__c;
                    wrapper.BU = Batch_TrackerMap.get(BatchSet).Business_Unit__c;
                    CronTriggerRecords.add(wrapper);
                } 
                
            }
        }
        
        for (String AllBatch: BatchNamesSet) 
        {
            if (!ActiveBatches.Contains(AllBatch))
            {
                CronTriggerWrapper wrapper = new CronTriggerWrapper();
                wrapper.JobName = AllBatch;
                wrapper.Isnotrunning = true;
                wrapper.Description = Batch_TrackerMap.get(AllBatch).Batch_Description__c;
                wrapper.BU = Batch_TrackerMap.get(AllBatch).Business_Unit__c; 
                CronTriggerRecords.add(wrapper);
                
            }
        } 
        List<CronTriggerWrapper> CronTriggerRecordsSet = new List<CronTriggerWrapper>();
        List<string> JobSet = new List<String> (); 
        for(CronTriggerWrapper wrapper : CronTriggerRecords)
        {
            if(!JobSet.contains(wrapper.JobName))
            {
                JobSet.add(wrapper.JobName);
                CronTriggerRecordsSet.add(wrapper);
            }
        } 
        
        CronTriggerRecords.clear();
        CronTriggerRecords.addAll(CronTriggerRecordsSet);
        String MailBody = '<table border="1"> <tr><td>Batch Name</td><td>Description</td><td>BU</td><td>Next Schedule Time</td><td>Last Run Time</td><td>Status</td><td>IsRunning</td><td>isNotRunning</td></tr>';
        String Temp ='';
        for(CronTriggerWrapper wrapper :CronTriggerRecords)
        {
            temp =temp+ '<tr><td>'+wrapper.JobName+'</td><td>'+wrapper.Description+'</td><td>'+wrapper.BU+'</td><td>'+wrapper.NextFireTime+'</td><td>'+wrapper.previousFireTime+'</td><td>'+wrapper.Status+'</td><td>'+wrapper.isRunning+'</td></tr>';
        }
        Mailbody = Mailbody+temp+'</table>';
        
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage(); 
        message.toAddresses = (system.label.BatchStatusEmail).split(';'); 
        message.subject = 'Production Batches Status '+Date.today().format(); 
        message.setHtmlBody(Mailbody); 
        message.setOrgWideEmailAddressId('0D2400000004CwU');
        Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                         + results[0].errors[0].message);
        } 
    } 
    
    private class CronTriggerWrapper    
    { 
        Public String JobName {get; set;}
        Public String Description {get; set;}
        Public String BU {get; set;}
        public string NextFireTime {get; set;}
        public String previousFireTime {get; set;}
        public String Status {get; set;}
        public String Owner {get; set;}
        Public Boolean isRunning {get; set;}
        Public Boolean isNotRunning {get; set;}
        public String StartTime {get; set;}
        
        public  CronTriggerWrapper()
        {
            isRunning = false;
            isNotRunning  = false;
            JobName ='';
            Description ='';
            BU ='';
            NextFireTime ='';
            previousFireTime = '';
            Status ='';
            Owner ='';
            isRunning =false;
            isNotRunning =false;
            StartTime ='';
            JobName ='';
            Description ='';
            BU ='';
            NextFireTime ='';
            previousFireTime = '';
            Status ='';
            Owner ='';
        } 
    } 
}