@isTest
public class CCFPAddressBookDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void getAddressBooksForAccountTest() {
        util.initCallContext();

        List<ccrz__E_AccountAddressBook__c> items = null;

        Test.startTest();
        items = CCFPAddressBookDAO.getAddressBooksForAccount(ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(items != null);
        System.assertEquals(2, items.size());
    }

    static testmethod void getAddressBooksForAccountShippingTest() {
        util.initCallContext();

        List<ccrz__E_AccountAddressBook__c> items = null;

        Test.startTest();
        items = CCFPAddressBookDAO.getAddressBooksForAccountShipping(ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(items != null);
        System.assertEquals(1, items.size());
    }

}