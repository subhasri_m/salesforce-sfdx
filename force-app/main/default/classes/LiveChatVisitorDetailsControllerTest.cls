/**
 * @description     LiveChatVisitorDetailsController testclass
 * @author          Lalit
 * @Company         FP
 * @date            14-12-2017
 *
 * HISTORY
 * - 14-12-2017    Lalit      Created.
*/ 

@istest
public class LiveChatVisitorDetailsControllerTest {

    
    testmethod static void test1()
    {
        
         Profile saProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User saUser = new User(Alias = 'newUser', Email='test123@email.com', 
                               EmailEncodingKey='UTF-8',
                               LastName='Testing',
                               LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',
                               ProfileId = saProfile.Id, 
                               TimeZoneSidKey='America/Los_Angeles',
                               UserName='test123123@email.com',
                               Create_Accounts__c = true,
                               Salesman_Number__c = '1-1234',
                               Region__c= 'Central');   
        insert saUser;
        
        
        
        Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = saUser.Id);  
        insert acc;
        System.assertNotEquals(null,acc.id)  ;  
        contact con = new contact (lastname = 'checkme',firstname='check',Accountid = acc.id,OwnerId=saUser.id,Email='test@test.com');    
		insert con;
System.assertNotEquals(null,con.id)    ;   
        Test.setCurrentPageReference(new PageReference('Page.LiveChatVisitorDetails')); 
		System.currentPageReference().getParameters().put('Vistorid',con.id  );
        System.currentPageReference().getParameters().put('EffAccid', acc.id  );
        LiveChatVisitorDetailsController controller = new LiveChatVisitorDetailsController();
        controller.SaveDetails();
        
    }
}