public with sharing class CCFPFulFillMentHelper {
    
    public static void getTaxAmountForGroup(ccrz__E_Cart__c theCart, Account theAccount) {
        if (theCart != null &&  theCart.CC_FP_Location__r != null && theAccount != null) {
            
            if (theAccount.Tax_Exempt__c == 'Yes') {
                theCart.ccrz__TaxAmount__c = 0;
                theCart.CC_FP_Tax_Rate__c = 0;
                theCart.ccrz__TaxExemptFlag__c = true;
            } 
            else {		
                
                List<CCAviBoomiAPI.FleetPrideIseriesTaxRequest> requestList = new List<CCAviBoomiAPI.FleetPrideIseriesTaxRequest>();
                Map<Id, List<ccrz__E_CartItem__c>> groupToCartItemMap = new Map<Id, List<ccrz__E_CartItem__c>>();
                System.debug('theCart==>'+theCart);
                System.debug('theCart.ccrz__E_CartItems__r==>'+theCart.ccrz__E_CartItems__r);
                for(ccrz__E_CartItem__c cartItem :theCart.ccrz__E_CartItems__r){
                    List<ccrz__E_CartItem__c> ciList = groupToCartItemMap.get(cartItem.ccrz__CartItemGroup__c);
                    if(ciList == null){
                        ciList = new List<ccrz__E_CartItem__c>();
                        groupToCartItemMap.put(cartItem.ccrz__CartItemGroup__c, ciList);
                    }
                    ciList.add(cartItem);
                }
                List<ccrz__E_CartItemGroup__c> cartItemGroups =  CCPDCCartDAO.getCartItemGroupDetailsForCart(theCart.Id);
                for(ccrz__E_CartItemGroup__c ciGroup : cartItemGroups) {                                             
                    List<ccrz__E_CartItem__c> ciList = groupToCartItemMap.get(ciGroup.Id);      
                    CCAviBoomiAPI.FleetPrideIseriesTaxRequest request = new CCAviBoomiAPI.FleetPrideIseriesTaxRequest();
                    CCAviBoomiAPI.FleetPrideIseriesTaxHeader headerRequest = new CCAviBoomiAPI.FleetPrideIseriesTaxHeader();
                    //header request
                    headerRequest.ShipmentID = ciGroup.Id;               
                    headerRequest.SellingCompany = theAccount.Iseries_Company_code__c;
                    headerRequest.CustomerNumber = theAccount.ISeries_Customer_Account__c;
                    headerRequest.CustomerBranch = theAccount.ISeries_Customer_Branch__c;
                    headerRequest.SellingLocation = theCart.CC_FP_Location__r.Location__c;
                    headerRequest.SalesDate = String.valueOf(Date.today().format());
                    headerRequest.ShipToAddr1 = ciGroup.ccrz__ShipMethod__c == 'PickUp' ? theCart.CC_FP_Location__r.Address_Line_1__c : theCart.ccrz__ShipTo__r.ccrz__AddressFirstline__c ;
                    headerRequest.ShipToAddr2 = ciGroup.ccrz__ShipMethod__c == 'PickUp' ? theCart.CC_FP_Location__r.Address_Line_2__c: theCart.ccrz__ShipTo__r.ccrz__AddressSecondline__c ;
                    headerRequest.ShipToCity = ciGroup.ccrz__ShipMethod__c == 'PickUp' ? theCart.CC_FP_Location__r.City__c: theCart.ccrz__ShipTo__r.ccrz__City__c ;
                    headerRequest.ShipToState = ciGroup.ccrz__ShipMethod__c == 'PickUp' ? theCart.CC_FP_Location__r.State__c: theCart.ccrz__ShipTo__r.ccrz__State__c ;
                    headerRequest.ShipToZip = ciGroup.ccrz__ShipMethod__c == 'PickUp' ? theCart.CC_FP_Location__r.Zipcode__c.subString(0,5): theCart.ccrz__ShipTo__r.ccrz__PostalCode__c.subString(0,5);
                    headerRequest.ShipToCountry = 'US';
                    String county = theCart.CC_FP_Location__r.County__c;
                    if (county == null) {
                        county = ' ';
                    }
                    headerRequest.ShipToCounty = county;
                    if (ciGroup.ccrz__ShipMethod__c == 'PickUp'){
                       headerRequest.DeliverMethod='W'; 
                    } else if(ciGroup.ccrz__ShipMethod__c == 'LocalDelivery'){
                       headerRequest.DeliverMethod='D'; 
                    } else if (ciGroup.ccrz__ShipMethod__c == 'ShipTo'){
                       headerRequest.DeliverMethod='S'; 
                    }
                    //headerRequest.DeliverMethod = ciGroup.ccrz__ShipMethod__c == 'PickUp' ? 'W' : ''; //willcall
                    headerRequest.TicketTotal = String.valueOf(ciGroup.CC_FP_Subtotal_Amount__c);
                    
                    //detail requests
                    List<CCAviBoomiAPI.FleetPrideIseriesTaxDetail> detailRequests = new List<CCAviBoomiAPI.FleetPrideIseriesTaxDetail>();                     
                    for(ccrz__E_CartItem__c ci : ciList) {
                        CCAviBoomiAPI.FleetPrideIseriesTaxDetail detailRequest = new CCAviBoomiAPI.FleetPrideIseriesTaxDetail();
                        detailRequest.LineID = ci.Id;
                        detailRequest.LineType = 'I';
                        detailRequest.Pool = ci.ccrz__Product__r.Pool_Number__c;
                        detailRequest.PartNumber = ci.ccrz__Product__r.Part_Number__c;
                        detailRequest.QtyOrdered = String.valueOf(ci.ccrz__Quantity__c);
                        detailRequest.QtyShipped = String.valueOf(ci.CC_FP_QTY_Shipping__c);
                        detailRequest.SellingPrice = String.valueOf(ci.ccrz__Price__c);
                        detailRequests.add(detailRequest);
                    }
                    request.TaxHeaderRequest = headerRequest;
                    request.TaxDetailRequestList = detailRequests;
                    requestList.add(request);
                }  
                String storefront = ccrz.cc_CallContext.storefront;
                System.debug('storefront==>'+storefront+'  Request List==>'+requestList);
                CCAviBoomiAPI.FleetPrideIseriesTaxResponseWrapper response = CCAviBoomiAPI.getFleetPrideIseriesTaxRate(storefront, requestList);
                List<ccrz__E_CartItemGroup__c> groupsToUpdate = new List<ccrz__E_CartItemGroup__c>();
                if (response.apiCallSuccess == true) {
                    List<CCAviBoomiAPI.FleetPrideIseriesTaxResponse> taxResponseList = (List<CCAviBoomiAPI.FleetPrideIseriesTaxResponse>)response.ResponseList;
                    if(taxResponseList != null) {
                        for(CCAviBoomiAPI.FleetPrideIseriesTaxResponse taxResponse: taxResponseList)
                        {
                            for(ccrz__E_CartItemGroup__c ciGroup : cartItemGroups) {
                                if(ciGroup.Id == taxResponse.ShipmentID) {
                                    if(taxResponse.ReturnCode != '200') {
                                        throw new SalesTaxException('Error getting tax information:' + taxResponse.CodeDescription);  
                                    } else {
                                        ciGroup.CC_FP_Tax_Amount__c = taxResponse.TaxAmount;
                                    }
                                }
                            }
                        }
                        for(ccrz__E_CartItemGroup__c ciGroup : cartItemGroups) {    
                            ciGroup.CC_FP_Total_Amount__c = ciGroup.CC_FP_Total_Amount__c + ciGroup.CC_FP_Tax_Amount__c;        
                        }
                        update cartItemGroups;
						theCart.ccrz__TaxAmount__c = response.TotalTaxAmount;
                    } 
                } else {
                    throw new SalesTaxException(response.message);
                }
                
            }
        }
        else {
            throw new SalesTaxException('Unable to get tax rate.');
        }
        
    }
    
    public static void doSplitsFP(ccrz__E_Cart__c cart) {
        Map<String, Integer> fftypeGroupIndexMap = new Map<String, Integer>();
        List<ccrz__E_CartItem__c> pickUpCartItems = new List<ccrz__E_CartItem__c>();
        List<ccrz__E_CartItem__c> LDCartItems = new List<ccrz__E_CartItem__c>();
        List<ccrz__E_CartItem__c> ShipToCartItems = new List<ccrz__E_CartItem__c>();
        // Remove old groups
        List<ccrz__E_CartItemGroup__c> groups = CCPDCCartDAO.getCartItemGroupsForCart(cart.Id);
        if (groups != null && !groups.isEmpty()) {
            delete groups;
        }
        groups = new List<ccrz__E_CartItemGroup__c>();
        Set<String> fulfillmentlst = new Set<String>();
        for (ccrz__E_CartItem__c cartItem : cart.ccrz__E_CartItems__r){
            fulfillmentlst.add(cartItem.FulFillMentType__c);
            if (cartItem.FulFillMentType__c == 'PickUp'){
                pickUpCartItems.add(cartItem); 
            }else if (cartItem.FulFillMentType__c == 'LocalDelivery'){
                LDCartItems.add(cartItem);  
            }else{
                ShipToCartItems.add(cartItem);  
            }
            
        } 
        List<ccrz__E_CartItem__c> items = new List<ccrz__E_CartItem__c>();
        Integer count = 1;
        for (String fftype : fulfillmentlst){
          ccrz__E_CartItemGroup__c grp = new ccrz__E_CartItemGroup__c();
                grp.ccrz__Cart__c = cart.Id; 
                //  ccrz__ShipAmount__c = shipment.costToCustomer,  
                grp.ccrz__CartItemGroupId__c = cart.ccrz__EncryptedId__c + '-' + count;
                grp.ccrz__GroupName__c =  fftype; 
                grp.CC_FP_DC_ID__C = cart.CC_FP_Location__r.location__c; // Branch Id 
                grp.ccrz__RequestDate__c = Date.today();
                grp.ccrz__ShipMethod__c = fftype; // critical
                grp.ccrz__ShipTo__c = cart.ccrz__ShipTo__c;
                grp.CC_FP_Subtotal_Amount__c = 0;
                grp.CC_FP_Tax_Amount__c = 0;
                grp.CC_FP_Total_Amount__c = 0;
                groups.add(grp); 
            	count++;
        }
        if (!groups.isEmpty()) {
            insert groups;
        }
        Integer index = 0;
        for (ccrz__E_CartItemGroup__c grp :groups){
            fftypeGroupIndexMap.put(grp.ccrz__ShipMethod__c,index);
            index++;
        }

        for (String fftype : fulfillmentlst){
            Decimal subtotal = 0;
            Integer fftypeIndex = fftypeGroupIndexMap.get(fftype);
            if (fftype == 'PickUp'){
            for (ccrz__E_CartItem__c line :pickUpCartItems){
                line.ccrz__CartItemGroup__c = groups[fftypeIndex].id;
                items.add(line);
                subtotal += line.ccrz__SubAmount__c;
                groups[fftypeIndex].CC_FP_Subtotal_Amount__c = subtotal;  
            }
                groups[fftypeIndex].CC_FP_Total_Amount__c = groups[fftypeIndex].CC_FP_Subtotal_Amount__c;
            }
            if (fftype == 'LocalDelivery'){
            for (ccrz__E_CartItem__c line :LDCartItems){
                line.ccrz__CartItemGroup__c = groups[fftypeIndex].id;
                items.add(line);
                subtotal += line.ccrz__SubAmount__c;
                groups[fftypeIndex].CC_FP_Subtotal_Amount__c = subtotal;  
            }
                groups[fftypeIndex].CC_FP_Total_Amount__c = groups[fftypeIndex].CC_FP_Subtotal_Amount__c;
            }
            if (fftype == 'ShipTo'){
            for (ccrz__E_CartItem__c line :ShipToCartItems){
                line.ccrz__CartItemGroup__c = groups[fftypeIndex].id;
                items.add(line);
                subtotal += line.ccrz__SubAmount__c;
                groups[fftypeIndex].CC_FP_Subtotal_Amount__c = subtotal;  
            }
                groups[fftypeIndex].CC_FP_Total_Amount__c = groups[fftypeIndex].CC_FP_Subtotal_Amount__c;
            }
        }
        
        if (!groups.isEmpty()) {
            update groups;
        }
        if (!items.isEmpty()) {
            upsert items;
        }                
    }
    
    public static void copyCartTransactionDataToOrder(String orderId) {
        system.debug('Inside Copy Transaction orderTransPayments @@Rahul');
        ccrz__E_Order__c order = CCPDCOrderDAO.getOrderCustom(orderId);
        ccrz__E_Cart__c cart = CCPDCCartDAO.getCartCustom(order.ccrz__OriginatedCart__c);
        List<ccrz__E_TransactionPayment__c> orderTransPayments = new List<ccrz__E_TransactionPayment__c>();
        List<ccrz__E_TransactionPayment__c> cartTransPayments = CCAviTransactionPaymentDAO.getTransactionPaymentsForCarts(order.ccrz__OriginatedCart__c);
        Map<String, String> groupMap = new Map<String, String>();
        for (ccrz__E_OrderItemGroup__c g : order.ccrz__E_OrderItemGroups__r) {
            groupMap.put(g.ccrz__GroupName__c, g.id);
        }
        for (ccrz__E_TransactionPayment__c cartTransPayment : cartTransPayments){
            ccrz__E_TransactionPayment__c tp = cartTransPayment.clone(false, true, false, false);
            tp.ccrz__CCOrder__c = orderId;
            tp.CC_Cart_Item_Group__c = null;
            tp.CC_Cart__c = null;
            tp.CC_Order_Item_Group__c = groupMap.get(cartTransPayment.CC_Cart_Item_GroupName__c);
            orderTransPayments.add(tp);
        }
        system.debug('Inside Copy Transaction orderTransPayments @@Rahul--'+ orderTransPayments);
       insert  orderTransPayments;
    }
    
    public class SalesTaxException extends Exception {}
}