/**
 * @description     FP_TriggerOnLivechatTranscripttest
 * @author          Lalit
 * @Company         FP
 * @date            12-12-2017
 *
 * HISTORY
 * - 12-12-2017    Lalit      Created.
*/ 


@istest
public class FP_TriggerOnLivechatTranscripttest {

    testmethod static void test1()
    {
        
       Profile saProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User saUser = new User(Alias = 'newUser', Email='test123@email.com', 
                               EmailEncodingKey='UTF-8',
                               LastName='Testing',
                               LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',
                               ProfileId = saProfile.Id, 
                               TimeZoneSidKey='America/Los_Angeles',
                               UserName='test123123@email.com',
                               Create_Accounts__c = true,
                               Salesman_Number__c = '1-1234',
                               Region__c= 'Central');   
        insert saUser;
        
        
        
        Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = saUser.Id);  
        insert acc;
        
        contact con = new contact (lastname = 'checkme',firstname='check',Accountid = acc.id,OwnerId=saUser.id,Email='test@test.com');    
		insert con;
       
        
        Live_Agent_Feedback__c feedback = new Live_Agent_Feedback__c(); 
        feedback.ChatKeyReference__c ='123';
         insert feedback;
         System.assertNotEquals(null,feedback.id);
        
        
        LiveChatVisitor visitor = new LiveChatVisitor();
        insert visitor;
		LiveChatTranscript Transcript = new LiveChatTranscript();
        Transcript.chatkey = '123';
        Transcript.Contactid =con.id;
        Transcript.Accountid =acc.id;
        transcript.LiveChatVisitorid = visitor.Id;
         
        insert Transcript;
        
        System.assertNotEquals(null,Transcript.id);
        
        
        
    }
}