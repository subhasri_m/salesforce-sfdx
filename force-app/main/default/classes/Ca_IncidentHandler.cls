public without sharing class Ca_IncidentHandler {
    /**
    * ca_IncidentHandler - Polulate CI,Primary Owner,Secondary owner, PicklistParentCategory based on Selected Category.
    * Created by CloudAction
    * @author: Furkhan Bage
    * @version: 1.0
*/
     Public void method1()
    {
        integer var=1;
    }
    
    /*

  public static boolean runOnceIncHandler = false;
  

    public static void UpdateCategoriesOnIncident  (List<BMCServiceDesk__Incident__c> IncRecNewSet)
    {
        String UserAdminId = System.label.BMCUserAdminId; 
        
        for (BMCServiceDesk__Incident__c BMCIncident: IncRecNewSet)
        {
            
            BMCIncident.BMCServiceDesk__FKCategory__c = UserAdminId;
            BMCIncident.Parent_Category__c ='Administration';
            if(BMCIncident.BMCServiceDesk__shortDescription__c!=Null && (BMCIncident.BMCServiceDesk__shortDescription__c.containsIgnoreCase('User Add')))
            { BMCIncident.Category_Action__c ='Add';}
            if(BMCIncident.BMCServiceDesk__shortDescription__c!=Null && (BMCIncident.BMCServiceDesk__shortDescription__c.containsIgnoreCase('User Modified'))){ BMCIncident.Category_Action__c ='Modify';}
            if(BMCIncident.BMCServiceDesk__shortDescription__c!=Null && (BMCIncident.BMCServiceDesk__shortDescription__c.containsIgnoreCase('User delete') )) { BMCIncident.Category_Action__c ='Delete';}
       
           
            if(BMCIncident.FP_Describe_your_Issue__c!=null && BMCIncident.BMCServiceDesk__incidentResolution__c==null)
            {
                BMCIncident.BMCServiceDesk__incidentResolution__c = BMCIncident.FP_Describe_your_Issue__c.stripHtmlTags();
            } 
        }
    }  
   
  Public static void IncidentHandler(List<BMCServiceDesk__Incident__c> IncRecNewSet,
                                     Map<Id,BMCServiceDesk__Incident__c>IncRecOldMap){
  
  System.debug('IncRecNewSet++++++++'+IncRecNewSet);
   //if (runOnceIncHandler ) return;
   // runOnceIncHandler  = true;
    system.debug('Category on Inc  formula name ');
                                         
   system.debug('Category on Inc  formula name BMCServiceDesk__FKCategory__r.Name '+IncRecNewSet[0].BMCServiceDesk__FKCategory__r.Name); 
    
  /*BMCServiceDesk__BMC_BaseElement__c[] relatedCI = [SELECT ID,BMCServiceDesk__Parent_Service__c,Ca_Primary_Owner__c,Ca_Secondary_Owner__c 
                                                    FROM BMCServiceDesk__BMC_BaseElement__c 
                                                    WHERE Name =: IncRecNewSet[0].BMCServiceDesk__Category_ID__c ];
     System.debug('#### IncRecNewSet[0].BMCServiceDesk__FKCategory__c '+IncRecNewSet[0].BMCServiceDesk__FKCategory__c);     
     BMCServiceDesk__Category__c[] catName=[SELECT Name,BMCServiceDesk__parentCategory_Id__c FROM BMCServiceDesk__Category__c 
                                        where Id=:IncRecNewSet[0].BMCServiceDesk__FKCategory__c]; 
       
      System.debug('####  catName[0].name '+catName[0].name);
     string categoryName=catName[0].name;  
     string categoryParentName=catName[0].BMCServiceDesk__parentCategory_Id__c;
     BMCServiceDesk__BMC_BaseElement__c[] relatedCI = [SELECT ID,BMCServiceDesk__Parent_Service__c,Ca_Primary_Owner__c,Ca_Secondary_Owner__c 
                                                    FROM BMCServiceDesk__BMC_BaseElement__c 
                                                    WHERE Name =: categoryName];                                    
                                         
                                         
  BMCServiceDesk__BMC_BaseElement__c[] relatedService = [SELECT id,Name 
                                                         FROM BMCServiceDesk__BMC_BaseElement__c 
                                                         WHERE Name =: categoryParentName ];
  
  BMCServiceDesk__Category__c[] catRec=[SELECT Name,BMCServiceDesk__parentCategory_Id__c,Business_Service__c,Business_Service__r.Name,Ownerid  
                                        FROM BMCServiceDesk__Category__c 
                                        where Name=:categoryName];
  //User current_user=[SELECT id FROM User WHERE Id= :UserInfo.getUserId()] ;
  
  System.debug('relatedCI++++++++'+relatedCI);
  System.debug('relatedService++++++++'+relatedService);
      
   //set CI , primary owner and secondary owner
   if(relatedCI.size()==1)
   {
      IncRecNewSet[0].BMCServiceDesk__FKBMC_BaseElement__c=relatedCI[0].id;IncRecNewSet[0].Ca_CI_Primary_Owner__c=relatedCI[0].Ca_Primary_Owner__c;
      IncRecNewSet[0].Ca_CI_Secondary_Owner__c=relatedCI[0].Ca_Secondary_Owner__c;
      System.debug('inside relatedCI values++++++++'+IncRecNewSet);
   }else if(relatedCI.size()==0)
   {
      IncRecNewSet[0].BMCServiceDesk__FKBMC_BaseElement__c=null; 
      IncRecNewSet[0].Ca_CI_Primary_Owner__c=null;
      IncRecNewSet[0].Ca_CI_Secondary_Owner__c=null;
      
   }
   //set Related Service
   if(relatedService.size()==1){
       IncRecNewSet[0].BMCServiceDesk__FKBusinessService__c=relatedService[0].id;IncRecNewSet[0].Ca_ServiceName__c=relatedService[0].Name;
       System.debug('inside related service values++++++++'+IncRecNewSet);
   }
   else if(relatedService.size()==0){
       IncRecNewSet[0].BMCServiceDesk__FKBusinessService__c=null;
       IncRecNewSet[0].Ca_ServiceName__c=null;
       System.debug('Is it coming inside this too related service values++++++++'+IncRecNewSet);
   }
               
   //set PicklistParentCategory and OwnerId
   if(catRec.size()==1){ 
    IncRecNewSet[0].Parent_Category__c=catRec[0].BMCServiceDesk__parentCategory_Id__c;
   }
   else if(catRec.size()==0){IncRecNewSet[0].Parent_Category__c=null;}
      
  }
*/

}