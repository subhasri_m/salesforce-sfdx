public with sharing class CCPDCOMSHomeDCAPI extends CCPDCOMSAbstractAPI {
	public static final String ZIP_CODE_QUERY_PARM = 'zipcode';
	// The zipcode for which to find the closest Distribution Centers
	public String zipCode {get;set;}
	// The number of distribution centers to find
	public Integer sites {get;set;}

	public CCPDCHomeDCResponse nearestDC {get; private set;}
	public List<CCPDCHomeDCResponse> nearestDCList {get; private set;}

	CCAviHttpUtil httpUtil;
	CC_PDC_OMS_Settings__c omsSettings;

	public CCPDCOMSHomeDCAPI(String zipCode, Integer sites, CC_PDC_OMS_Settings__c omsSettings) {
		this.zipCode = zipCode;
		this.sites = sites;
		this.omsSettings = omsSettings;
		
	}

	// An instance of HttpUtil with the OAuth token will be provided. No DML should be performed
	// in this method.
	public override void executeRequest(CCAviHttpUtil util){
		this.httpUtil = util;

		getNearestDCs();	
	}

	// Perform any DML operations. This method is called after all CCPDCOMSIFC intefaces
	// executeRequest methods have been called.
	public override void commitData(){
		// nothing to commit
	}

	private void getNearestDCs() {
        httpUtil.method = CCAviHttpUtil.REQUEST_TYPE.GET;
        httpUtil.endpoint = omsSettings.Nearest_DC_URL__c;
        httpUtil.addQueryParameter(ZIP_CODE_QUERY_PARM, zipCode);
        HttpResponse httpRes = httpUtil.submitRequest();
        System.debug('requestDCs response: ' + httpRes.getBody());
        if (httpUtil.requestErrors) {
            nearestDC.message  = 'Exception occured';
            throw new CCPDCOMSAPI.OMSException('Nearest DC exception: ' + httpUtil.requestException.getMessage());
        }
        else {
            if (httpRes.getStatusCode() == 200) {
            	System.debug('NearestDC Response: ' + httpRes.getBody());
            	nearestDCList = CCPDCHomeDCResponse.parse(httpRes.getBody());
            	nearestDC = nearestDCList[0];
                nearestDC.success = true;
            }
            else {
            	throw new CCPDCOMSAPI.OMSException('Unknown error requesting access token: ' + httpRes.getStatus() + ' => ' + httpRes.getStatusCode());
            }
        }

	}
}