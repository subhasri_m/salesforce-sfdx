@isTest
public class CommonContextDataPrepareTest {

    public static testMethod void prepareData() {
    	
    	Id thisOwnerId = UserInfo.getUserId();
    	
    	User thisUser = [select id from User where id =: thisOwnerId];
    	thisUser.Growth_Rate_Q1__c = 10;
    	thisUser.Growth_Rate_Q2__c = 10;
    	thisUser.Growth_Rate_Q3__c = 10;
    	thisUser.Growth_Rate_Q4__c = 10;
    	update thisUser;
    	
        Account acc = new Account(Name = 'Test Acc', OwnerId = thisOwnerId, AccountNumber = '1-1221-1');
        insert acc;
        
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        
        Date month1 = Date.newInstance(Date.today().year() - 1, 1, 1);
        Date month2 = Date.newInstance(Date.today().year() - 1, 2, 1);
        Date month3 = Date.newInstance(Date.today().year() - 1, 3, 1);
        
        Date month11 = Date.newInstance(Date.today().year(), 1, 2);
        Date month22 = Date.newInstance(Date.today().year(), 2, 2);
        Date month33 = Date.newInstance(Date.today().year(), 3, 2);
        
        Date thisMonth = Date.newInstance(Date.today().year(), Date.today().month(), 1);
        Date thisMonthLastYear = Date.newInstance(Date.today().year() - 1, Date.today().month(), 1);
        
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 100, Order_Date__c = thisMonth, Account__c = acc.Id, ownerid=thisOwnerId);
        soList.add(so);
        
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 200, Order_Date__c = thisMonthLastYear, Account__c = acc.Id, ownerid=thisOwnerId);
        soList.add(so2);
        
        soList.add(new Sales_Order_Summary__c(Sales__c = 200, Order_Date__c = month1, Account__c = acc.Id, ownerid=thisOwnerId));
        soList.add(new Sales_Order_Summary__c(Sales__c = 200, Order_Date__c = month2, Account__c = acc.Id, ownerid=thisOwnerId));
        soList.add(new Sales_Order_Summary__c(Sales__c = 200, Order_Date__c = month3, Account__c = acc.Id, ownerid=thisOwnerId));
        
        insert soList;
        
        List<Working_Days__c> wd = new List<Working_Days__c>();
        wd.add(new Working_Days__c(Date__c = month1, Is_Working_Day__c = true));
        wd.add(new Working_Days__c(Date__c = month2, Is_Working_Day__c = true));
        wd.add(new Working_Days__c(Date__c = month3, Is_Working_Day__c = true));
        
        wd.add(new Working_Days__c(Date__c = month11, Is_Working_Day__c = true));
        wd.add(new Working_Days__c(Date__c = month22, Is_Working_Day__c = true));
        wd.add(new Working_Days__c(Date__c = month33, Is_Working_Day__c = true));
        
        wd.add(new Working_Days__c(Date__c = thisMonth, Is_Working_Day__c = true));
        wd.add(new Working_Days__c(Date__c = thisMonthLastYear, Is_Working_Day__c = true)); 
        
        insert wd;
    }
}