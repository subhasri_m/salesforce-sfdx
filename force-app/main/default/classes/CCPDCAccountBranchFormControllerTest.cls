@isTest
public class CCPDCAccountBranchFormControllerTest {
	
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

    }

    static testmethod void testaddAccountRequest(){
    	util.initCallContext();

    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
		ctx.storefront = 'pdc';
    	String input = '{"branchId":"SNH-48","branchName":"Turkey-branch","addressLine1":"12345 Madison St","addressLine2":"Apt:1000","city":"Mountain View","state":"CA","postalCode":"90291","primaryContactName":"Jared Dunn","primaryContactPhoneNumber":"123-456-7890","primaryContactEmailAddress":"yubo.diao232@avionos.com","secondaryContactName":"Jared Dunn","secondaryContactPhoneNumber":"213-222-2222","secondaryContactEmailAddress":"diao.yubo212@gmail.com"}';
    	ccrz.cc_RemoteActionResult result = null;
		Test.startTest();
		result = CCPDCAccountBranchFormController.addAccountRequest(ctx, input);
		Test.stopTest();
		System.assert(result.success);
		Map<String,Object> data = (Map<String, Object>)result.data;
		Map<String,Object> inputData = (Map<String, Object>)data.get('theinput');

		String branchId = String.valueOf(inputData.get('branchId'));
		System.assertEquals(branchId,'SNH-48');

    }

    static testmethod void testaddAccountRequestException(){

    	ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
    	String input = '{"branchId":"SNH-48","addressLine1":"12345 Madison St","addressLine2":"Apt:1000","city":"Mountain View","state":"CA","postalCode":"90291","primaryContactName":"Jared Dunn","primaryContactPhoneNumber":"123-456-7890","primaryContactEmailAddress":"yubo.diao232@avionos.com","secondaryContactName":"Jared Dunn","secondaryContactPhoneNumber":"213-222-2222","secondaryContactEmailAddress":"diao.yubo212@gmail.com"}';
    	ccrz.cc_RemoteActionResult result = null;
		Test.startTest();
		result = CCPDCAccountBranchFormController.addAccountRequest(ctx, input);
		Test.stopTest();

		System.assert(!result.success);
		
    }
	
}