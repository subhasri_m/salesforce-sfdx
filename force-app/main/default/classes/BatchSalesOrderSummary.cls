/*
Date sinceDate = Date.newInstance(Date.today().year(), 1, 1);
Date toDate = Date.newInstance(Date.today().year(), 12, 31);
Database.executeBatch(new BatchSalesOrderSummary('a0m18000000XmJV', sinceDate, toDate), 50);
        
*/
global class BatchSalesOrderSummary implements Database.Batchable<sObject>, Database.Stateful {

     global Date startDate;
     global Date endDate;
             
     global String informaticaUserId = Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
     global Integer totalProcessedNum {get; set;}
     global Integer totalOKNum {get; set;}
     global Integer totalNGNum {get; set;}

     global Id oneId {get; set;}
     global BatchSalesOrderSummary(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;

     }
     global BatchSalesOrderSummary(Id oneId, Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
        
        this.oneId = oneId;
     }
     
    global Database.QueryLocator start(Database.BatchableContext bc) {

        totalProcessedNum = 0;
        totalOKNum = 0;
        totalNGNum = 0;     
        
       /* String msg = 'Batch Sales Target Calculation gets started at: ' + Datetime.now();
        Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesOrderSummary',
            Batch_Message__c=msg,
            Type__c = 'INFO'
            );
        
        insert bl;
        */
        
        String qry = 'SELECT OwnerId, Start_Date__c, Name';
        qry += ', Gross_Profit_Sum__c';
        qry += ', Sales_Sum__c';
        qry += ', GP_Target_Growth_ThisMonth__c, GP_Target_Growth_ThisQuarter__c, GP_Target_Growth_ThisYear__c';
        qry += ', Sales_Target_Growth_ThisMonth__c, Sales_Target_Growth_ThisQuarter__c, Sales_Target_Growth_ThisYear__c';
        
        qry += ' from Sales_Target__c';
        qry += '  where ownerid != :informaticaUserId AND Start_Date__c  >= :startDate AND Start_Date__c  <= :endDate';
        
        if (!String.isEmpty(oneId)) {
            qry += ' and id = :oneId';
        }
        
        return Database.getQueryLocator(qry);
    }
    
    global void execute(Database.BatchableContext bc, List<Sales_Target__c> stList) {
        
        Integer processedNum = stList.size();
        Integer OKNum = 0;
        Integer NGNum = 0;
        
        try { /*
            
            OKNum = processedNum;
        
            // get the data last year for comparison
            String qry = 'SELECT OwnerId, Start_Date__c,Gross_Profit_Sum__c, Sales_Sum__c, GP_Target_Growth_ThisMonth__c, GP_Target_Growth_ThisQuarter__c, GP_Target_Growth_ThisYear__c, Sales_Target_Growth_ThisMonth__c, Sales_Target_Growth_ThisQuarter__c, Sales_Target_Growth_ThisYear__c from Sales_Target__c where ';
            Boolean firstFlag = true;
            
            for(Sales_Target__c st : stList) {
                if (!firstFlag) qry += ' or '; 
                Date dt = CommonUtil.getTheSameDayLastYear(st.Start_Date__c);
                DateTime dtTime = DateTime.newInstance(dt.year(), dt.month(), dt.day());
                
                qry += '(OwnerId = \'' + st.OwnerId + '\' and Start_Date__c= ' + dtTime.format('yyyy-MM-dd') + ')';
                firstFlag = false;
            }
            system.debug('-- stLastYear qry:' + qry);
            
            List<Sales_Target__c> stListLastYear = Database.Query(qry);
            system.debug('-- stLastYear before:' + stListLastYear);
             
            AggregateResult[] groupedResults = [SELECT Sales_Target__c, SUM(Gross_Profit__c), SUM(Sales__c) FROM Sales_Order_Summary__c where ownerid != :informaticaUserId AND Sales_Target__c in :stListLastYear GROUP BY Sales_Target__c];
            
            for (AggregateResult ar : groupedResults) {
                String stIdStr = (String)ar.get('Sales_Target__c');
                
                for(Sales_Target__c stLastYear : stListLastYear) {
                    if (stLastYear.id != stIdStr) continue; 
                    stLastYear.Gross_Profit_Sum__c = ar.get('expr0') == null ? 0 : (Decimal)ar.get('expr0');
                    stLastYear.Sales_Sum__c = ar.get('expr1') == null ? 0 : (Decimal)ar.get('expr1'); 
                }
            }
            
            system.debug('-- stListLastYear:' + stListLastYear);
            
            // process the data this year
            Set<Id> ownerIdSet = new Set<Id>();
            for(Sales_Target__c st : stList) ownerIdSet.add(st.OwnerId);
            
            List<User> ownerList = [select Growth_Rate_Q1__c, Growth_Rate_Q2__c, Growth_Rate_Q3__c, Growth_Rate_Q4__c from User where id in :ownerIdSet];
            system.debug('-- ownerList:' + ownerList);
            
            AggregateResult[] groupedResultsThis = [SELECT Sales_Target__c, SUM(Gross_Profit__c), SUM(Sales__c) FROM Sales_Order_Summary__c where ownerid != :informaticaUserId AND Sales_Target__c in :stList GROUP BY Sales_Target__c];
            
            for (AggregateResult ar : groupedResultsThis) {
                String stIdStr = (String)ar.get('Sales_Target__c');
                
                for(Sales_Target__c st : stList) {
                    if (st.id != stIdStr) continue; 
                    st.Gross_Profit_Sum__c = ar.get('expr0') == null ? 0 : (Decimal)ar.get('expr0');
                    st.Sales_Sum__c = ar.get('expr1') == null ? 0 : (Decimal)ar.get('expr1'); 
                }
            }
            
            system.debug('-- stList:' + stList);
                        
            // compare with the data laster year and set the target
            for(Sales_Target__c st : stList) {
                
                // get the percentage of growth of each quater
                Decimal percentageGrowth = 0;
                
                for(User u : ownerList) {
                    if (u.id != st.ownerId) continue;
                    
                    Integer quarterNum = CommonUtil.getFiscalYearPeriodNumber(st.Start_Date__c, 'Quarter');
                    if (quarterNum == null) break;
                    
                    percentageGrowth = (Decimal)u.get('Growth_Rate_Q' + quarterNum + '__c');
                    
                }
                
                if (percentageGrowth == null) percentageGrowth = Fleet_Pride_Common__c.getOrgDefaults().Default_Sales_Growth_Rate__c;
                
                st.Growth_Percentage__c = percentageGrowth;
                
                // as it's percentage
                percentageGrowth = percentageGrowth / 100;
                
                // last year's data for the same month
                Date dt = CommonUtil.getTheSameDayLastYear(st.Start_Date__c);
                
                for(Sales_Target__c stLastYear : stListLastYear) {
                    if (dt != stLastYear.Start_Date__c || stLastYear.OwnerId != st.OwnerId) continue;
                    // GP Total
                    st.GP_Target_Growth_Total_Month_Target__c = (stLastYear.Gross_Profit_Sum__c * CommonUtil.getHowManyWorkingDaysThisMonth(st.Start_Date__c) / CommonUtil.getHowManyWorkingDaysLastMonth(st.Start_Date__c)) * (1 + percentageGrowth);
                    st.GP_Target_Growth_Total_Quarter_Target__c = (stLastYear.Gross_Profit_Sum__c * CommonUtil.getHowManyWorkingDaysThisQuarter(st.Start_Date__c) / CommonUtil.getHowManyWorkingDaysLastQuarter(st.Start_Date__c)) * (1 + percentageGrowth);
                    st.GP_Target_Growth_Total_Year_Target__c = (stLastYear.Gross_Profit_Sum__c * CommonUtil.getHowManyWorkingDaysThisYear(st.Start_Date__c) / CommonUtil.getHowManyWorkingDaysLastYear(st.Start_Date__c)) * (1 + percentageGrowth);

                    // Sales Total
                    st.Sales_Target_Growth_Total_Month_Target__c = (stLastYear.Sales_Sum__c * CommonUtil.getHowManyWorkingDaysThisMonth(st.Start_Date__c) / CommonUtil.getHowManyWorkingDaysLastMonth(st.Start_Date__c)) * (1 + percentageGrowth);
                    st.Sales_Target_Growth_Total_Quarter_Target__c = (stLastYear.Sales_Sum__c * CommonUtil.getHowManyWorkingDaysThisQuarter(st.Start_Date__c) / CommonUtil.getHowManyWorkingDaysLastQuarter(st.Start_Date__c)) * (1 + percentageGrowth);
                    st.Sales_Target_Growth_Total_Year_Target__c = (stLastYear.Sales_Sum__c * CommonUtil.getHowManyWorkingDaysThisYear(st.Start_Date__c) / CommonUtil.getHowManyWorkingDaysLastYear(st.Start_Date__c)) * (1 + percentageGrowth);
                    
                    
                    // only calculate for this month
                    Date dayStartInThisMonth = Date.newInstance(Date.today().year(), Date.today().month(), 1);
                    Date dayEndInThisMonth = dayStartInThisMonth.addMonths(1).addDays(-1);
                    
                    // only calculate for this month
                    if (st.Start_Date__c >= dayStartInThisMonth && st.Start_Date__c <= dayEndInThisMonth) {
                        st.GP_Target_Growth_ThisMonth__c = (stLastYear.Gross_Profit_Sum__c * CommonUtil.getHowManyWorkingDaysThisMonthUpToToday() / CommonUtil.getHowManyWorkingDaysLastMonth(st.Start_Date__c)) * (1 + percentageGrowth);
                        st.Sales_Target_Growth_ThisMonth__c = (stLastYear.Sales_Sum__c * CommonUtil.getHowManyWorkingDaysThisMonthUpToToday() / CommonUtil.getHowManyWorkingDaysLastMonth(st.Start_Date__c)) * (1 + percentageGrowth);
                    }
                    
                    
                    // only calculate for this quarter
                    Period pd = CommonUtil.getFiscalYearPeriod(Date.today(), 'Quarter');
                    Date dayStartInThisQuarter = pd.startDate;
                    Date dayEndInThisQuarter = pd.endDate;
                    
                    // only calculate for this quarter
                    if (st.Start_Date__c >= dayStartInThisQuarter && st.Start_Date__c <= dayEndInThisQuarter) {
                        st.GP_Target_Growth_ThisQuarter__c = (stLastYear.Gross_Profit_Sum__c * CommonUtil.getHowManyWorkingDaysThisQuarterUpToToday() / CommonUtil.getHowManyWorkingDaysLastQuarter(st.Start_Date__c)) * (1 + percentageGrowth);
                        st.Sales_Target_Growth_ThisQuarter__c = (stLastYear.Sales_Sum__c * CommonUtil.getHowManyWorkingDaysThisQuarterUpToToday() / CommonUtil.getHowManyWorkingDaysLastQuarter(st.Start_Date__c)) * (1 + percentageGrowth);
                    }
                    
                    // only calculate for this year
                    pd = CommonUtil.getFiscalYearPeriod(Date.today(), 'Year');
                    Date dayStartInThisYear = pd.startDate;
                    Date dayEndInThisYear = pd.endDate;
                    
                    // only calculate for this year
                    if (st.Start_Date__c >= dayStartInThisYear && st.Start_Date__c <= dayEndInThisYear) {
                        st.GP_Target_Growth_ThisYear__c = (stLastYear.Gross_Profit_Sum__c * CommonUtil.getHowManyWorkingDaysThisYearUpToToday() / CommonUtil.getHowManyWorkingDaysLastYear(st.Start_Date__c)) * (1 + percentageGrowth);
                        st.Sales_Target_Growth_ThisYear__c = (stLastYear.Sales_Sum__c * CommonUtil.getHowManyWorkingDaysThisYearUpToToday() / CommonUtil.getHowManyWorkingDaysLastYear(st.Start_Date__c)) * (1 + percentageGrowth);
                    }
                    
                }
            }
            
            update stList;
    
      */  } catch (Exception ex) { /*
            
            system.debug('-- error log:' + ex);
            
            NGNum = processedNum;
            // Write result to log object
            String msg = 'Error happened : ' + ex.getMessage() + '\n\n';
            msg += ex.getStackTraceString();
            msg += '\n caused by: \n';
            msg += ex.getCause();
            for(Sales_Target__c a : stList) {
                msg += a.id + ',' + a.Name + '; ';
            }
            
            Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesOrderSummary',
                Batch_Message__c=msg,
                Type__c = 'ERROR'
            );
            insert bl;
            
      */  } finally {
            
            system.debug('-- final2...:' + totalProcessedNum);
            system.debug('-- final2...:' + totalOKNum);
            system.debug('-- final2...:' + totalNGNum);
            
            system.debug('-- final2...:' + processedNum);
            system.debug('-- final2...:' + OKNum);
            system.debug('-- final2...:' + NGNum);
            
        /*    totalProcessedNum += processedNum;
            totalOKNum += OKNum;
            totalNGNum += NGNum;
          */  
            system.debug('-- final complete.');
            
        }
     }
     
     global void finish(Database.BatchableContext bc) {
        
        String msg = 'Record retrieved : ' + totalProcessedNum +
             ', Successfully processed : ' + totalOKNum +
             ', Failed to processed : ' + totalNGNum;
        // Write result to log object
      /*  Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesOrderSummary',
            Batch_Message__c=msg,
            Type__c = 'INFO'
            );
        
        system.debug('-- finish:' + bl);
        
        insert bl;
        */
    
        system.debug('-- finish complete.'); 
     }
}