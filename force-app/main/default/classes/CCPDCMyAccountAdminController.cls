global with sharing class CCPDCMyAccountAdminController {

	global static final String ACTION_DELETE = 'delete';

	@RemoteAction
	global static ccrz.cc_RemoteActionResult affectUser(ccrz.cc_RemoteActionContext ctx, String action, String matrixId){
		ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
		String debugMessage = 'none';
		try {
			if (ACTION_DELETE.equals(action)) {
				debugMessage = ACTION_DELETE;
				CC_FP_Contract_Account_Permission_Matrix__c permission = CCFPContactAccountPermissionMatrixDAO.getPermissionsById(matrixId);
				Id contactId = permission.Contact__c;
				CCPDCMyAccountAdminHelper.deletePermissions(permission);
				List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = CCFPContactAccountPermissionMatrixDAO.getAccountsForContact(contactId);
				if (permissions.isEmpty()) {
					activateUser_future(permission.Contact__c, false);
					debugMessage = 'Contact has been set to inactive.';
				}
			}

			// Return the result
     		CCAviPageUtils.buildResponseData(response, true, 
     			new Map<String,Object>{
     				'success' => true,
     				'debugMessage' => debugMessage
     			}
        	);
     	}
     	catch(Exception e){
	     	CCAviPageUtils.buildResponseData(response, true, 
     			new Map<String,Object>{'success' => false, 'debugMessage' => debugMessage, 'message' => e.getMessage(), 'cause' => e.getCause(), 'trace' => e.getStackTraceString()}
        	);	
     	}		
     	return response;
	}

	public static Boolean getBoolean(Map<String, Object> data, String fieldName){
		String val = (String)data.get(fieldName);
		if(val == null){
			return false;
		}
		if('on'.equalsIgnoreCase(val)){
			return true;
		}
		return Boolean.valueOf(val);
	}

	public static CC_FP_Contract_Account_Permission_Matrix__c populatePermissionMatrix(Map<String,Object> data, Id accountId){
		CC_FP_Contract_Account_Permission_Matrix__c perms = new CC_FP_Contract_Account_Permission_Matrix__c(Account__c = accountId);
		perms.Allow_AR_Payments__c 					= getBoolean(data, 'arPayments');
		perms.Allow_Access_To_Accounting_Reports__c = getBoolean(data, 'accountingReports');
		perms.Allow_Access_to_Sales_Reports__c 		= getBoolean(data, 'salesReports');
		perms.Allow_Backorder_Release__c 			= getBoolean(data, 'backorderRelease');
		perms.Allow_Backorders__c 					= getBoolean(data, 'backorders');
		perms.Allow_Checkout__c 					= getBoolean(data, 'allowCheckout');
		perms.Allow_Non_Free_Shipping_Orders__c		= getBoolean(data, 'allowNonFreeShipping');
		perms.Allow_Override_ShipTo__c 				= getBoolean(data, 'overrideShipTo');
		perms.Is_Admin__c 							= getBoolean(data, 'isAdmin');
		perms.Pricing_Visibility__c 				= getBoolean(data, 'canSeePricing');
		perms.Can_View_Invoices__c 				    = getBoolean(data, 'canViewInvoices');

		try {
			String strMaxOrderLimit = (String)data.get('maximumOrderLimit');
			perms.Maximum_Order_Limit__c = (strMaxOrderLimit == null || strMaxOrderLimit == '')?null:Decimal.valueOf(strMaxOrderLimit);
		}
		catch(Exception e){
			// leave the max order limit amount unset
			//perms.Maximum_Order_Limit__c = -1;
		}

		String permissionId = (String)data.get('permissionId');
		if(permissionId != null && permissionId.length() > 0){
			perms.Id = permissionId;
		}
		
		return perms;
	}

	@RemoteAction
	global static ccrz.cc_RemoteActionResult addAccountPermission(ccrz.cc_RemoteActionContext ctx, String data){
		ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
		Boolean success = true;
		Id accountId;
		Id contactId;
		try{
			Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
			String accountIdstr = String.valueOf(input.get('add-permission-select'));
			accountId = Id.valueOf(accountIdstr);
			String contactIdstr = String.valueOf(input.get('contactId'));
			contactId = Id.valueOf(contactIdstr);
			CC_FP_Contract_Account_Permission_Matrix__c perm = populatePermissionMatrix(input,accountId);
			perm.Contact__c = contactId;
			CCPDCMyAccountAdminHelper.insertPermissions(perm);
	 		CCAviPageUtils.buildResponseData(response, true, 
	 			new Map<String,Object>{'perm' => perm}
	    	);
		}
		catch(Exception e){
	     	CCAviPageUtils.buildResponseData(response, false, 
                new Map<String,Object>{'success' => false,'message' => e.getMessage(),'cause' => e.getCause(),'trace' => e.getStackTraceString(), 'error' => 'Exception'}
        	);	
     	}

		return response;
	}

	@RemoteAction
	global static ccrz.cc_RemoteActionResult addOrUpdateUser(ccrz.cc_RemoteActionContext ctx, Boolean isUpdate, String data) {
		ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
		Boolean success = true;
		String errorCode;
		Id accountId;
		String storefront = ccrz.cc_CallContext.storefront;
		CC_Avi_Settings__mdt aviSettings = [
			SELECT
				Storefront__c
				, Default_Domain__c
			FROM
				CC_Avi_Settings__mdt
			WHERE
				Storefront__c = :storefront
			LIMIT
				1
		];
		String defaultDomain = '';
		if(aviSettings != null && CC_Avi_Settings__mdt.Default_Domain__c != null){
			defaultDomain = aviSettings.Default_Domain__c;
		}

		Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
		
		if(!isUpdate){
		  accountId = (String)input.get('aviliable-accounts');
		  if(accountId == null) {
		  	accountId = ccrz.cc_CallContext.effAccountId;
		  }
		}

		String contactId = (String)input.get('contactId');
		String emailAddress = (String)input.get('emailAddress');
		String firstName = (String)input.get('firstName');
		String lastName = (String)input.get('lastName');
		String phoneNumber = (String)input.get('phoneNumber');
		CC_FP_Contract_Account_Permission_Matrix__c perms = populatePermissionMatrix(input, accountId);

		String username = (String)input.get('username');
		if(username.indexOfIgnoreCase('@') < 0){
			username += defaultDomain;
		}


		try {
			// Fetch the accounts the current user has access to
			// If the supplied account Id is not in the list return an error (unauthorized)
			//Map<Id, Account> actMap = CCPDCAccountDAO.queryAccessibleAccounts();
			User currentUser = CCPDCUserDAO.queryUserForUserId(UserInfo.getUserId());
			User existingUser = null;

			// Fetch the contacts and make sure the supplied username is not in the list of contacts
			// otherwise return an error (user exists)
			if(!isUpdate){
				existingUser = CCPDCUserDAO.queryUserForUserName(username);
				if (existingUser != null) {
					if (existingUser.ContactId != null) {
						CC_FP_Contract_Account_Permission_Matrix__c permission = CCFPContactAccountPermissionMatrixDAO.getPermissionsForAccountAndContact(accountId, existingUser.ContactId);
						if (permission != null) {
							// Contact already has a permission matrix
							success = false;
							errorCode = 'Username_Already_Exists';
						}
						if (accountId != existingUser.Contact.AccountId) {
							Account parentAccount = CCPDCAccountDAO.getTopLevelAccount(accountId);
							Account userParentAccount = CCPDCAccountDAO.getTopLevelAccount(existingUser.Contact.AccountId);
							if ((parentAccount == null) || (userParentAccount == null) || (parentAccount.Id != userParentAccount.Id)) {
								// User exists but is not in same account hierarchy
								success = false;
								errorCode = 'Username_Invalid_Account';
							}
						}
					}
					else {
						// User has no Contact
						success = false;
						errorCode = 'Username_Has_No_Contact';
					}

					if (!success) {
	     				CCAviPageUtils.buildResponseData(response, success, 
     						new Map<String,Object>{
                                'success' => false,
     							'error' => errorCode,
     							'inputData' => input,
     							'perms' => perms
     						}
        				);	
        				return response;						
					}

					if (!existingUser.IsActive) {
						activateUser_future(existingUser.ContactId, true);
					}
				}
			}

			Contact newContact = new Contact(
				FirstName  						= (String)input.get('firstName'),
				LastName 						= (String)input.get('lastName'),
				Email 							= (String)input.get('emailAddress'),
				Phone 							= (String)input.get('phoneNumber')
			);
			if(isUpdate){
				newContact.Id = contactId;
			}
			else if (existingUser != null) {
				newContact.Id = existingUser.ContactId;
			}
			else {
				newContact.AccountId = accountId;
			}

			// Insert the contact record
			// insert newContact; // need a helper for this - without sharing
			if(isUpdate){
				CCPDCMyAccountAdminHelper.updateContact(newContact);
				CCPDCMyAccountAdminHelper.updatePermissions(perms);
			}
			else if (existingUser != null) {
				CCPDCMyAccountAdminHelper.updateContact(newContact);
				perms.Contact__c = existingUser.ContactId;
				CCPDCMyAccountAdminHelper.insertPermissions(perms);
			}
			else {
				CCPDCMyAccountAdminHelper.insertContact(newContact);
				perms.Contact__c = newContact.Id;
				CCPDCMyAccountAdminHelper.insertPermissions(perms);
			}

			// Create a user record that points to the new contact record (turn off notification or no?)
			System.debug('THE PROFILE ID: ' + currentUser.ProfileId);

			User newUser;
			if(isUpdate){
				newUser = CCPDCUserDAO.queryUserForContactId(contactId);
				newUser.FirstName         = firstName;
				newUser.LastName          = lastName;
				newUser.Email             = emailAddress;
				newUser.Username          = username;
				newUser.Phone 			  = phoneNumber;
			}
			
			if(isUpdate){
				updateUser_future(newUser.Id, firstName, lastName, emailAddress, phoneNumber, username);
			}
			else if (existingUser == null) {
				insertUser_future(newContact.Id, firstName, lastName, emailAddress, phoneNumber, username);
			}


			// Return the result
			if(!success){
	     		CCAviPageUtils.buildResponseData(response, false, 
     				new Map<String,Object>{'success' => success,'error' => 'Unable_To_Create_Or_Update_User','inputData' => input,'perms' => perms}
        		);			
			}
			else {
     			CCAviPageUtils.buildResponseData(response, true, 
     				new Map<String,Object>{'success' => success,'inputData' => input,'perms' => perms}
        		);
     		}
     	}
     	catch(Exception e){
	     	CCAviPageUtils.buildResponseData(response, true, 
                new Map<String,Object>{'success' => false,'message' => e.getMessage(),'cause' => e.getCause(),'trace' => e.getStackTraceString(),'inputData' => input,'perms' => perms, 'error' => 'Exception'}
        	);	
     	}		
     	return response;
	}

	@future
	@TestVisible
	private static void insertUser_future(String contactId, String firstName, String lastName, String emailAddress, String phoneNumber, String username) {
		User currentUser = CCPDCUserDAO.queryUserForUserId(UserInfo.getUserId());
		User newUser = new User();
		// newUser.Alias             = emailAddress.substring(0,8);
		newUser.Alias             = username.substring(0,8);
		newUser.CommunityNickname = username;
		newUser.LocaleSidKey      = currentUser.LocaleSidKey;
		newUser.TimeZoneSidKey    = currentUser.TimeZoneSidKey;
		CCPDCMyAccountAdminHelper.assignProfileToUser(currentUser.ProfileId, newUser);
		newUser.LanguageLocaleKey = currentUser.LanguageLocaleKey;
		newUser.EmailEncodingKey  = currentUser.EmailEncodingKey;
		newUser.ContactId         = contactId;
		newUser.FirstName         = firstName;
		newUser.LastName          = lastName;
		newUser.Email             = emailAddress;
		newUser.Username          = username;
		newUser.Phone 			  = phoneNumber;

		CCPDCMyAccountAdminHelper.insertUser(newUser);
	}

	@future
	private static void updateUser_future(String userId, String firstName, String lastName, String emailAddress, String phoneNumber, String theUsername){
		User newUser = new User(
			Id 			= userId,
			FirstName   = firstName,
			LastName    = lastName,
			Email       = emailAddress,
			Username    = theUsername,
			Phone 		= phoneNumber
		);
		CCPDCMyAccountAdminHelper.updateUser(newUser);

	}

	@future
	private static void activateUser_future(String contactId, Boolean isActive) {
		User u = CCPDCUserDAO.queryUserForContactId(contactId);
		if(u != null) {
			u.IsActive = isActive;
			CCPDCMyAccountAdminHelper.updateUser(u);
		}
	}

/*
	@future
	private static void updateUser_future(String userId, boolean isActive) {
		User newUser = new User(
			Id 			= userId,
			IsActive    = isActive
		);
		CCPDCMyAccountAdminHelper.updateUser(newUser);
	}

	@future
	private static void updateContact_future(String contactId, boolean isPortalUser) {
		Contact newContact = new Contact(
			Id 						= contactId,
			ccrz__IsPortalUser__c 	= isPortalUser
		);
		CCPDCMyAccountAdminHelper.updateContact(newContact);
	}

	@future
	private static void deleteContact_future(String contactId) {
		Contact newContact = new Contact(
			Id 						= contactId
		);
		CCPDCMyAccountAdminHelper.deleteContact(newContact);
	}		
*/

	@RemoteAction
	global static ccrz.cc_RemoteActionResult retrieveAccountAndUserData(ccrz.cc_RemoteActionContext ctx){
		ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
		try {
			// Query permission matrix for current effective account
			Boolean parentAccount = false;
			Id selectedActId = ccrz.cc_CallContext.effAccountId;
			Set<Id> accountIds = new Set<Id>();
			List<CCPDCMyAccountAdminHelper.CCContact> contactList = CCPDCMyAccountAdminHelper.fetchContactList(selectedActId);
			for(CCPDCMyAccountAdminHelper.CCContact contact:contactList){
				accountIds.add(contact.accountId);
			}
			if(accountIds.size()>1){
				parentAccount = true;
			}

			List<Account> accounts = [SELECT Id, Name FROM Account WHERE ParentId =:selectedActId OR Id=:selectedActId];

			// Return the CCContacts
     		CCAviPageUtils.buildResponseData(response, true, 
     			new Map<String,Object>{'success' => true,'contactData' => contactList,'parentAccount' => parentAccount,'accountsVisiable'=>accountIds,'allAccounts'=>accounts}
        	);
		}
     	catch(Exception e){
	     		CCAviPageUtils.buildResponseData(response, true, 
     			new Map<String,Object>{'success' => false,'message' => e.getMessage(),'cause' => e.getCause(),'trace' => e.getStackTraceString()}
        	);	
     	}		

     	return response;
     	
	}
}