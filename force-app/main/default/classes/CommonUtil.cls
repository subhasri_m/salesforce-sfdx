/*
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,1,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,2,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,3,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,4,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,5,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,6,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,7,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,8,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,9,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,10,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,11,1)));
system.debug(CommonUtil.getHowManyWorkingDaysThisMonth(Date.newInstance(2016,12,1)));



*/
public class CommonUtil {
	
	private static List<Period> fiscalYearPeriod = null;
	
	private static List<Working_Days__c> workingDaysListFromLastYear = null;
	
	private static List<UserRole> userRoleList = null;
	
	public static List<UserRole> getUserRoleList() {
		if (userRoleList == null) {
			userRoleList = [SELECT ParentRoleId, DeveloperName, Name FROM UserRole]; 
		}
		return userRoleList;
	}
	
	public static UserRole getRole(Id roleId) {
		for(UserRole userR : getUserRoleList()) {
			if (userR.Id == roleId) return userR;
		}
		return null;
	}
	
	public static void getChildrenRole(Set<UserRole> userRoleSet, Id parentRoleId) {
		Set<UserRole> childrenRoleSet = new Set<UserRole>(); 
		for(UserRole userR : getUserRoleList()) {
			if (userR.ParentRoleId == parentRoleId) childrenRoleSet.add(userR);
		}
		if (childrenRoleSet.size() == 0) return;
		
		userRoleSet.addAll(childrenRoleSet);
		
		for(UserRole childrenRole: childrenRoleSet) getChildrenRole(userRoleSet, childrenRole.id);
	}

	public static void getParentRole(Set<UserRole> parentUserRoleSet,  UserRole thisUserRole) {
		if (thisUserRole == null || String.isEmpty(thisUserRole.ParentRoleId)) return;
		
		UserRole parentRole = null; 
		for(UserRole userR : getUserRoleList()) {
			if (userR.Id == thisUserRole.ParentRoleId) {
				parentRole = userR;
				break;
			}
		}
		if (parentRole == null) return;
		
		parentUserRoleSet.add(parentRole);
		
		getParentRole(parentUserRoleSet, parentRole);
	}
	
	public static List<User> getUserSetInOneTeamUnderLeader(Id leaderUserId) {
		
		User u = [SELECT UserRoleId FROM User WHERE Id = :leaderUserId];
		Id roleId = u.UserRoleId;
		
		Set<UserRole> userRoleSet = new Set<UserRole>();
		
		getChildrenRole(userRoleSet, roleId);
		
		List<User> ulist = [select name, UserRoleId from User where UserRoleId in :userRoleSet or id = :leaderUserId];
		
		return ulist;
	}
	
	public static Integer getFiscalYearPeriodNumber(Date d, String periodType) {
		for(Period pd : getFiscalYearPeriod()) {
			if (d > = pd.startDate && d <= pd.endDate && pd.type == periodType) return pd.Number;
		}
		return null;
	}
	
	public static Period getFiscalYearPeriod(Date d, String periodType) {
		for(Period pd : getFiscalYearPeriod()) {
			if (d > = pd.startDate && d <= pd.endDate && pd.type == periodType) return pd;
		}
		return null;
	}
	
	
	public static List<Period> getFiscalYearPeriod() {
		
		if (fiscalYearPeriod == null) {
			// retrieve 2 years
			Date fromDate = Date.newInstance(Date.today().year() - 2, 1, 1);
			fiscalYearPeriod = [Select type, StartDate, EndDate, Number From Period Where StartDate >= :fromDate];
		}
		
		return fiscalYearPeriod;
	}
	
/*	public static List<Working_Days__c> getWorkingDaysListFromLastYear() {
		
		if (workingDaysListFromLastYear == null) {
			Date fromDate = Date.newInstance(Date.today().year() - 2, 1, 1);
			workingDaysListFromLastYear = [select Date__c, Is_Working_Day__c from Working_Days__c where Date__c >= :fromDate];
		}
		
		return workingDaysListFromLastYear;
	}

	
	public static Integer getHowManyWorkingDays(Date startDate, Date endDate) {
		Integer num = 0;
		
		for(Working_Days__c w : getWorkingDaysListFromLastYear()) {
		    if(w.Date__c >= startDate && w.Date__c <= endDate){
			    if (w.Is_Working_Day__c) num++;
		    }
		}
		
		return num;
	}
	
		
	public static Integer getHowManyWorkingDaysThisMonthUpToToday() {
		Date sinceDate = Date.newInstance(Date.today().year(), Date.today().month(), 1);
		Integer num = getHowManyWorkingDays(sinceDate, Date.today());
		// calculate to today
		if (num > 0) num--; 
		
		return num;
	}
	
	public static Integer getHowManyWorkingDaysThisQuarterUpToToday() {
		
		Period pd = getFiscalYearPeriod(Date.today(), 'Quarter');
		
		Date sinceDate = pd.startDate;
		
		Integer num = getHowManyWorkingDays(sinceDate, Date.today());
		// calculate to today
		if (num > 0) num--; 
		
		return num;
	}
	
	public static Integer getHowManyWorkingDaysThisYearUpToToday() {
		
		Period pd = getFiscalYearPeriod(Date.today(), 'Year');
		
		Date sinceDate = pd.startDate;
		
		Integer num = getHowManyWorkingDays(sinceDate, Date.today());
		// calculate to today
		if (num > 0) num--; 
		
		return num;
	}
	
	public static Integer getHowManyWorkingDaysThisMonth(Date startDate) {
		Period pd = getFiscalYearPeriod(startDate, 'Month');
		return getHowManyWorkingDays(pd.startDate, pd.endDate);
	}
	
	public static Integer getHowManyWorkingDaysThisQuarter(Date startDate) {
		Period pd = getFiscalYearPeriod(startDate, 'Quarter');
		return getHowManyWorkingDays(pd.startDate, pd.endDate);
	}
	
	public static Integer getHowManyWorkingDaysThisYear(Date startDate) {
		Period pd = getFiscalYearPeriod(startDate, 'Year');
		return getHowManyWorkingDays(pd.startDate, pd.endDate);
	}
	
	public static Integer getHowManyWorkingDaysLastMonth(Date startDate) {
		
		startDate = getTheSameDayLastYear(startDate);
		
		Period pd = getFiscalYearPeriod(startDate, 'Month');
		return getHowManyWorkingDays(pd.startDate, pd.endDate);
	}
	
	public static Integer getHowManyWorkingDaysLastQuarter(Date startDate) {
		
		startDate = getTheSameDayLastYear(startDate);
		
		Period pd = getFiscalYearPeriod(startDate, 'Quarter');
		return getHowManyWorkingDays(pd.startDate, pd.endDate);
	}
	
	public static Integer getHowManyWorkingDaysLastYear(Date startDate) {
		
		startDate = getTheSameDayLastYear(startDate);
		
		Period pd = getFiscalYearPeriod(startDate, 'Year');
		return getHowManyWorkingDays(pd.startDate, pd.endDate);
	}
*/

	public static Date getTheSameDayLastYear(Date startDate) {
		return startDate.addYears(-1);
	}
	
	public static Date getTheSameDayNextYear(Date startDate) {
		return startDate.addYears(1);
	}
	
	public static String convertToPercent(Decimal d, Integer scale) {
		return String.valueOf(((d * 10000).round(roundingMode.HALF_UP) / 100.0).setScale(scale)) + '%';
	}
}