global class BatchAccountSOSched implements Schedulable {
    global void execute(SchedulableContext sc) {
        
        Integer month = System.today().month();
        Database.executeBatch(new BatchAccountSO(month), 100);
    }
}