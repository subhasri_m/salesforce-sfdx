@isTest
public class CCPDCLogicProductPricingTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Map<String, Object> m;
        System.runAs(thisUser){
            m = util.initData();
        }

        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.SERVICE_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccServiceProduct' => 'c.CCPDCServiceProduct'
                }
            },
            ccrz.ccApiTestData.LOGIC_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccLogicProductPricing' => 'c.CCPDCLogicProductPricing'
                }
            }
        };

        ccrz__E_PriceList__c priceListtest = (ccrz__E_PriceList__c) m.get('priceList');

        priceListtest.Name = 'Enterprise';
        update priceListtest;
        ccrz.ccApiTestData.setupData(testData);


        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = util.STOREFRONT;
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
    }

    static testmethod void processTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>B99</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO><POOL>123</POOL></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccApiProduct.PARAM_INCLUDE_PRICING => true,
            ccrz.ccAPIProduct.PRODUCTIDLIST => new List<Id>{product.Id}
        };
            
            List<Object> newLines = new List<Object>();
        ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
        newLine.sku = 'product-01';
        newLine.quantity = 1;
        newLines.add(newLine);
        newLine = new ccrz.ccApiCart.LineData();
        newLine.sku = 'product-02';
        newLine.quantity = 1;
        newLines.add(newLine);

        CC_FP_Contract_Account_Permission_Matrix__c perMtx = util.createPermissions(true);
        perMtx.Contact__c = ccrz.cc_CallContext.currContact.Id;
        insert perMtx;
        
        Test.startTest();
        
        response = ccrz.ccAPIProduct.fetch(request);
        Test.stopTest();

        System.assert(response != null);
    }
    
     static testmethod void processTest2() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>B99</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO><POOL>123</POOL></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccApiProduct.PARAM_INCLUDE_PRICING => true,
            ccrz.ccAPIProduct.PRODUCTIDLIST => new List<Id>{product.Id}
        };
            

        
        
        Test.startTest();
        CC_FP_Contract_Account_Permission_Matrix__c perMtx = util.createPermissions(true);
        perMtx.Contact__c = ccrz.cc_CallContext.currContact.Id;
        insert perMtx;
         
        response = ccrz.ccAPIProduct.fetch(request);
        Test.stopTest();

        System.assert(response != null);
    }

    static testmethod void process10000Test() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>B99</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccApiProduct.PARAM_INCLUDE_PRICING => true,
            ccrz.ccAPIProduct.PRODUCTIDLIST => new List<Id>{product.Id}
        };

        Test.startTest();
        CC_FP_Contract_Account_Permission_Matrix__c perMtx = util.createPermissions(false);
        perMtx.Contact__c = ccrz.cc_CallContext.currContact.Id;
        insert perMtx;
        CCPDCLogicProductPricing obj = new CCPDCLogicProductPricing();
        response = obj.process(request);
        Test.stopTest();

        System.assert(response != null);
    }

    static testmethod void processPartsTest() {
        util.initCallContext();

        System.debug( 'contactID in Test '+ ccrz.cc_CallContext.currContact);
        CC_FP_Contract_Account_Permission_Matrix__c perMtx = util.createPermissions(false);
        perMtx.Contact__c = ccrz.cc_CallContext.currContact.Id;
        ccrz__E_PriceList__c priceListtest = [SELECT Name, Id FROM ccrz__E_PriceList__c WHERE Name = 'Enterprise'];
		priceListtest.Name = 'General';
        update priceListtest;

        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>B99</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccApiProduct.PARAM_INCLUDE_PRICING => true,
            ccrz.ccAPIProduct.PRODUCTIDLIST => new List<Id>{product.Id}
        };

        Test.startTest();
        ccrz.cc_CallContext.storefront = 'parts';
        ccrz.cc_CallContext.currPageName = 'Products';
         insert perMtx;
        System.debug('perMtx==>'+perMtx);
        CCPDCLogicProductPricing obj = new CCPDCLogicProductPricing();
        response = obj.process(request);
        Test.stopTest();

        System.assert(response != null);
    }
    
    static testmethod void process10000Test2() {
        util.initCallContext();

        System.debug( 'contactID in Test '+ ccrz.cc_CallContext.currContact);
        CC_FP_Contract_Account_Permission_Matrix__c perMtx = util.createPermissions(true);
        perMtx.Contact__c = ccrz.cc_CallContext.currContact.Id;
        
       


        ccrz__E_PriceList__c priceListtest = [SELECT Name, Id FROM ccrz__E_PriceList__c WHERE Name = 'Enterprise'];

        priceListtest.Name = 'General';
        update priceListtest;

        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>B99</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        ccrz__E_Product__c product = util.getProduct();

        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccApiProduct.PARAM_INCLUDE_PRICING => true,
            ccrz.ccAPIProduct.PRODUCTIDLIST => new List<Id>{product.Id}
        };

        Test.startTest();
        ccrz.cc_CallContext.currPageName = 'Products';
         insert perMtx;
        System.debug('perMtx==>'+perMtx);
        CCPDCLogicProductPricing obj = new CCPDCLogicProductPricing();
        response = obj.process(request);
        Test.stopTest();

        System.assert(response != null);
    }

   static testmethod void process10000Test21() {
        util.initCallContext();
		ccrz.cc_CallContext.Storefront = 'parts';
    	ccrz.cc_CallContext.isGuest = true;
        ccrz__E_Product__c product = util.getProduct();

        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccApiProduct.PARAM_INCLUDE_PRICING => true,
            ccrz.ccAPIProduct.PRODUCTIDLIST => new List<Id>{product.Id}
        };

        Test.startTest();
        ccrz.cc_CallContext.currPageName = 'Products';
        CCPDCLogicProductPricing obj = new CCPDCLogicProductPricing();
        response = obj.process(request);
        Test.stopTest();

        System.assert(response != null);
    }    
    static testmethod void dummyMethodTest(){
        Test.startTest();
		CCPDCLogicProductPricing.dummyMethod();
        Test.stopTest();
    }

}