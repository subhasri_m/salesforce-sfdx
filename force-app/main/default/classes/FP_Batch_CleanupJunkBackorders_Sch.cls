Global class  FP_Batch_CleanupJunkBackorders_Sch implements schedulable {
     
     global void execute(SchedulableContext SC){
        Database.executeBatch(new FP_Batch_CleanupJunkBackorders());
    } 
}