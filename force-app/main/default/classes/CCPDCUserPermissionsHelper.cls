public with sharing class CCPDCUserPermissionsHelper {
    public static UserPermissions getUserPermissions(Id accountId, Id contactId) {
        UserPermissions permissions = new UserPermissions();
        Account accountPermissions = CCPDCAccountDAO.getAccountPermissions(accountId);
        CC_FP_Contract_Account_Permission_Matrix__c matrix = null;
        if (accountPermissions != null) {
            matrix = CCFPContactAccountPermissionMatrixDAO.getPermissionsForAccountAndContact(accountPermissions.Id, contactId);
            if (matrix == null && accountPermissions.ParentId != null) {
                matrix = CCFPContactAccountPermissionMatrixDAO.getPermissionsForAccountAndContact(accountPermissions.ParentId, contactId);
                if (matrix == null && accountPermissions.Parent.ParentId != null) {
                    matrix = CCFPContactAccountPermissionMatrixDAO.getPermissionsForAccountAndContact(accountPermissions.Parent.ParentId, contactId);
                    if (matrix == null && accountPermissions.Parent.Parent.ParentId != null) {
                        matrix = CCFPContactAccountPermissionMatrixDAO.getPermissionsForAccountAndContact(accountPermissions.Parent.Parent.ParentId, contactId);
                    }
                }
            }
            if (matrix != null) {
                permissions.updatePermissions(matrix, accountPermissions);
            }
        }
        return permissions;
    }

    public class UserPermissions {

        public Boolean onHold {get; set;}
        public Boolean allowCreditCard {get; set;}
        public Boolean allowInvoice {get; set;}
        public Boolean taxExempt {get; set;}
        public Boolean allowWillCall {get; set;}
        public Boolean requiresPO {get; set;}

        public Boolean allowAccessToAccountingReports {get; set;}
        public Boolean allowAccessToSalesReports {get; set;}
        public Boolean allowARPayments {get; set;}
        public Boolean allowBackorderRelease {get; set;}
        public Boolean allowBackorders {get; set;}
        public Boolean allowCheckout {get; set;}
        public Boolean allowNonFreeShippingOrders {get; set;}
        public Boolean allowOverrideShipTo {get; set;}
        public Boolean allowCreateBranchRequest {get; set;}
        public Boolean isAdmin {get; set;}
        public Boolean isPricingVisible {get; set;}
        public Decimal maximumOrderLimit {get; set;}
        public String distributionCenter {get; set;}
        public Boolean canViewInvoices {get; set;}
        public String distributionCenterCity {get; set;}

        public UserPermissions() {
            this.onHold = false;
            this.allowCreditCard = false;
            this.allowInvoice = false;
            this.taxExempt = false;
            this.allowBackorders = false;
            this.allowWillCall = false;
            this.requiresPO = false;
            this.allowAccessToAccountingReports = false;
            this.allowAccessToSalesReports = false;
            this.allowARPayments = false;
            this.allowBackorderRelease = false;
            this.allowCheckout = false;
            this.allowNonFreeShippingOrders = false;
            this.allowOverrideShipTo = false;
            this.allowCreateBranchRequest = false;
            this.isAdmin = false;
            this.maximumOrderLimit = null;
            this.isPricingVisible = false;
            this.canViewInvoices = false;
        }

        public void updatePermissions(CC_FP_Contract_Account_Permission_Matrix__c matrix, Account accountPermissions) {
            this.onHold = false;
            this.allowCreditCard = false;
            this.allowInvoice = false;
            if (accountPermissions.Account_Status__c == 'ON HOLD') {
                this.onHold = true;
            }
            else {
                this.allowCreditCard = true;
                if (accountPermissions.Account_Status__c == 'DIRECT PAY/BANK TRANSFER' ||  accountPermissions.Account_Status__c == 'INTER COMPANY ACCOUNT' ||  accountPermissions.Account_Status__c == 'OPEN' ||  accountPermissions.Account_Status__c == 'PROBATION' || accountPermissions.Account_Status__c == 'SLOW - COD') {
                    this.allowInvoice = true;
                }
            }
            this.taxExempt = (accountPermissions.Tax_Exempt__c == 'Yes');
            this.allowBackorders = (accountPermissions.OK_to_Backorder__c == 'Yes' && matrix.Allow_Backorders__c == true);
            this.allowWillCall = (accountPermissions.Will_Call_Allowed__c == true);
            this.requiresPO = (accountPermissions.Requires_PO__c == 'Yes');
            this.allowAccessToAccountingReports = (matrix.Allow_Access_To_Accounting_Reports__c == true);
            this.allowAccessToSalesReports = (matrix.Allow_Access_to_Sales_Reports__c == true);
            this.allowARPayments = (matrix.Allow_AR_Payments__c == true);
            this.allowBackorderRelease = (matrix.Allow_Backorder_Release__c == true);
            this.allowCheckout = (matrix.Allow_Checkout__c == true);
            this.allowNonFreeShippingOrders = (matrix.Allow_Non_Free_Shipping_Orders__c == true);
            this.allowOverrideShipTo = (matrix.Allow_Override_ShipTo__c == true);
            this.isAdmin = (matrix.Is_Admin__c == true);
            this.maximumOrderLimit = matrix.Maximum_Order_Limit__c;
            this.isPricingVisible = (matrix.Pricing_Visibility__c == true);
            this.distributionCenter = accountPermissions.Ship_from_Location__c;
            this.canViewInvoices = (matrix.Can_View_Invoices__c == true);
            
            
            Location__c loc = CCFPLocationDAO.getLocationForCode(accountPermissions.Ship_from_Location__c);
            if(loc != null && loc.City__c != null) {
                this.distributionCenterCity = loc.City__c;
            } else {
                this.distributionCenterCity = accountPermissions.Ship_from_Location__c;
            }            

            // Get the CC Avi Settings
            // If the current record type developer name is in the list of names
            // allowed to create a branch request, set allowCreateBranchRequest to true
            String storefront = ccrz.cc_CallContext.storefront;
            CC_Avi_Settings__mdt settings = CCAviUtil.getCCAviSettingsMetaData(storefront);
            if(settings.Branch_Request_Record_Types__c.contains(accountPermissions.RecordType.DeveloperName)){
                this.allowCreateBranchRequest = true;
            }
        }

    }
}