@isTest
public class CCPDCCartActionTotalsControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void testCCPDCCartActionTotalsController(){
        CCPDCCartActionTotalsController controller = new CCPDCCartActionTotalsController();

    }

    static testmethod void testSubmitForApprovalFail(){
        util.initCallContext();
        CC_FP_Contract_Account_Permission_Matrix__c apermissionMatrix= new CC_FP_Contract_Account_Permission_Matrix__c();
        apermissionMatrix.Is_Admin__c = true;
        Account account = util.getAccount();
        Contact contact = util.getContact();
        apermissionMatrix.Account__c = account.Id;
        apermissionMatrix.Contact__c = contact.Id;
        insert apermissionMatrix;


        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;

        String cartId = util.getCart().ccrz__EncryptedId__c;
        result = CCPDCCartActionTotalsController.submitForApproval(ctx,cartId, 'SHDDBDJD');

        System.assert(!result.success);

    }

    static testmethod void testSubmitForApproval(){
        util.initCallContext();

        Account account = util.getAccount();
        Contact contact = util.createContact(account,'testcontact1@email.com');
        insert contact;

        CC_FP_Contract_Account_Permission_Matrix__c apermissionMatrix= new CC_FP_Contract_Account_Permission_Matrix__c();
        apermissionMatrix.Is_Admin__c = true;
        apermissionMatrix.Account__c = account.Id;
        apermissionMatrix.Contact__c = contact.Id;
        insert apermissionMatrix;


        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;

        String cartId = util.getCart().ccrz__EncryptedId__c;
        result = CCPDCCartActionTotalsController.submitForApproval(ctx,cartId, 'SHDDBDJDSSS');

        //System.assert(result.success);

    }

    static testmethod void testSubmitForApprovalAddress(){

        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        Account account = util.getAccount();
        Contact contact = util.getContact();
        ctx.effAccountId = String.valueOf(account.Id);
        ccrz.cc_CallContext.currContact = contact;
        //Contact updatecontact = [SELECT Id, Email FROM Contact WHERE Id =: contact.Id];
        //updatecontact.Email = 'test@gmail.com';
        //update updatecontact;

        CC_FP_Contract_Account_Permission_Matrix__c apermissionMatrix= new CC_FP_Contract_Account_Permission_Matrix__c();
        apermissionMatrix.Is_Admin__c = false;
        apermissionMatrix.Account__c = account.Id;
        apermissionMatrix.Contact__c = contact.Id;
        insert apermissionMatrix;

        ccrz.cc_RemoteActionResult result = null;

        Account paccount = util.createAccount(null);
        paccount.Name = 'test-account-p';
        insert paccount;
        Account newpaccount = [SELECT Id FROM Account WHERE Name = 'test-account-p'];
        account.ParentId = newpaccount.Id;
        update account;

        Contact newcontact = util.createContact(account,'testcontact2@email.com');
        insert newcontact;

        CC_FP_Contract_Account_Permission_Matrix__c apermissionMatrix2= new CC_FP_Contract_Account_Permission_Matrix__c();
        apermissionMatrix2.Is_Admin__c = true;
        apermissionMatrix2.Account__c = newpaccount.Id;
        apermissionMatrix2.Contact__c = newcontact.Id;
        insert apermissionMatrix2;

        System.debug(apermissionMatrix);
        System.debug(apermissionMatrix2);
        System.debug(account);
        System.debug(ctx);
        Test.startTest();
        String cartId = util.getCart().ccrz__EncryptedId__c;
        result = CCPDCCartActionTotalsController.submitForApproval(ctx,cartId,'SHDDBDJDGDHE');
        Test.stopTest();

        System.debug(result);

        //System.assert(!result.success);

    }


    static testmethod void testSubmitForApprovalExceptions(){
        //util.initCallContext();

        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ctx.storefront = 'pdc';
        ccrz.cc_RemoteActionResult result = null;

        String cartId = util.getCart().ccrz__EncryptedId__c;
        result = CCPDCCartActionTotalsController.submitForApproval(ctx,cartId, 'SHDDBDJDDAE');

        System.assert(!result.success);


    }



}