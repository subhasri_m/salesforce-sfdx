@isTest
public class CCPDCFulfillmentHelperTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();

            ccrz__E_Cart__c cart = util.createCartWithSplits(m);
            ccrz__E_Order__c order = util.createOrderWithSplits(m, cart);
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = util.STOREFRONT;
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
    }

    static testmethod void doSplitsTest() {
        util.initCallContext();

        String result = '{"orderId":"0ebff7e0-d1b8-4719-8cea-9c4ea6c19180","result":"Success","reservationId":"000000000560","homeDC":"AT","recheckOccurred":false,"shipments":[{"dc":"AT","carrier":"SAIA","expedited":false,"costToFP":128.82,"costToCustomer":0.0,"mode":"Less-Than-Truckload","transitTime":2,"lines":[{"partNo":"product-01","orderLineNo":"1","quantity":1}]}],"backOrderLines":[]}';
        CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse response = (CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse)JSON.deserialize(result,  CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse.class);

        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c item = util.getCartItem();
        item.CC_FP_Line_Number__c = 1;
        update item;
  
        Test.startTest();
        cart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        CCPDCFulfillmentHelper.doSplits(cart, response);
        Test.stopTest();

        List<ccrz__E_CartItemGroup__c> groups = CCPDCCartDAO.getCartItemGroupsForCart(cart.Id);
        System.assert(groups != null);
        System.assertEquals(1, groups.size());

    }
	static testmethod void doSplitsFPTest() {
        util.initCallContext();
		ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c item = util.getCartItem();
        item.CC_FP_Line_Number__c = 1;
        update item;
  
        Test.startTest();
        cart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        CCPDCFulfillmentHelper.doSplitsFP(cart);
        Test.stopTest();

        List<ccrz__E_CartItemGroup__c> groups = CCPDCCartDAO.getCartItemGroupsForCart(cart.Id);
        System.assert(groups != null);
        System.assertEquals(1, groups.size());

    }
    static testmethod void doSplitsBackorderTest() {
        util.initCallContext();

        String result = '{"orderId":"0ebff7e0-d1b8-4719-8cea-9c4ea6c19180","result":"Success","reservationId":"000000000560","homeDC":"AT","recheckOccurred":false,"shipments":[{"dc":"AT","carrier":"SAIA","expedited":false,"costToFP":128.82,"costToCustomer":0.0,"mode":"Less-Than-Truckload","transitTime":2,"lines":[{"partNo":"product-01","orderLineNo":"1","quantity":5}]}],"backOrderLines":[{"partNo":"product-01","quantity":5}]}';
        CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse response = (CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse)JSON.deserialize(result,  CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse.class);

        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c item = util.getCartItem();
        item.CC_FP_Line_Number__c = 1;
        item.ccrz__Quantity__c = 10;
        update item;
  
        Test.startTest();
        cart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        try{
            CCPDCFulfillmentHelper.doSplits(cart, response);
        }catch(System.Exception e){
        }
        Test.stopTest();

        List<ccrz__E_CartItemGroup__c> groups = CCPDCCartDAO.getCartItemGroupsForCart(cart.Id);
        System.assert(groups != null);
        System.assertEquals(2, groups.size());

    }
    
    static testmethod void copyCartToOrderDataTest() {
        util.initCallContext();

        ccrz__E_Order__c order = util.getSplitOrder();
  
        Test.startTest();
        CCPDCFulfillmentHelper.copyCartToOrderData(order.Id);
        Test.stopTest();

        ccrz__E_Order__c updatedOrder = CCPDCOrderDAO.getOrderCustom(order.Id);
        System.assert(updatedOrder != null);

    }

    static testmethod void doAuthTest() {
        util.initCallContext();

        ccrz__E_Order__c order = util.getSplitOrder();
        order = CCPDCOrderDAO.getOrderDetails(order.Id);
        List<ccrz__E_TransactionPayment__c> data = new List<ccrz__E_TransactionPayment__c> {
            CCAviTransactionPaymentDAOTest.createTransactionPayment(order)
        };
        insert data;
  
        Test.startTest();
        List<ccrz__E_TransactionPayment__c> payments = CCAviTransactionPaymentDAO.getTransactionPaymentsForOrder(order.Id);
        CCPDCFulfillmentHelper.doAuth(order.Id, payments);
        Test.stopTest();

        ccrz__E_Order__c updatedOrder = CCPDCOrderDAO.getOrderCustom(order.Id);
        System.assert(updatedOrder != null);

    }

    static testmethod void setTaxRateTest() {
        util.initCallContext();

        String result = '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesDefaultTaxRateResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesDefaultTaxRateResponseList><ISeriesDefaultTaxRate><DefaultTaxRate>0.062500</DefaultTaxRate></ISeriesDefaultTaxRate></wss:ISeriesDefaultTaxRateResponseList></wss:getISeriesDefaultTaxRateResponse></S:Body></S:Envelope>';
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', result));

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        update cart;
        Account theAccount = util.getAccount();

        Test.startTest();
        cart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        theAccount = CCPDCAccountDAO.getAccount(theAccount.Id);
        CCPDCFulfillmentHelper.setTaxRate(cart, theAccount);
        Test.stopTest();

        List<ccrz__E_CartItemGroup__c> groups = CCPDCCartDAO.getCartItemGroupsForCart(cart.Id);
        System.assert(cart.CC_FP_Tax_Rate__c != null);
        System.assertEquals(0.062500, cart.CC_FP_Tax_Rate__c);

    }

    static testmethod void updateTaxTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__TaxExemptFlag__c = false;
        cart.CC_FP_Tax_Rate__c = 0.062500;
        cart.ccrz__ShipAmount__c = 5.00;
        update cart;

        Test.startTest();
        CCPDCFulfillmentHelper.updateTax(cart.ccrz__EncryptedId__c);
        Test.stopTest();

        ccrz__E_Cart__c updateCart = CCPDCCartDAO.getCartSplitResponse(cart.ccrz__EncryptedId__c);
        System.assert(updateCart.ccrz__TaxAmount__c != null);
        System.assertEquals(0.936875, updateCart.ccrz__TaxAmount__c);

    }


}