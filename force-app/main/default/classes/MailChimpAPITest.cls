@isTest
public class MailChimpAPITest {
    
    public static final String RESPONSE_STRING = '{"type":"http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/","title":"Member Exists","status":400,"detail":"ana.santellana@fleetpride.com is already a list member. Use PUT to insert or update list members.","instance":"dfb497d9-7211-4a92-a2ea-e4f73d142268"}';    
    static testmethod void fleetPrideSubscribeTest() {
        HttpResponse  response= null;
        MailChimpAuthDetail__c authDetail = new MailChimpAuthDetail__c();
        authDetail.APIKey__c = '1d7d9267f9308b7ab44bbb4e6eecc30c-us7';
        authDetail.ListId__c = '58b230415c';
        authDetail.Name = 'parts';
        authDetail.URLEndPoint__c = 'https://us7.api.mailchimp.com/3.0/';
        authDetail.UserName__c = 'ana.santellana@fleetpride.com';
        Test.setMock(HttpCalloutMock.class, new MAILCHIMPMock(200, 'OK', RESPONSE_STRING));
       
        insert authDetail;
        
        Test.startTest();
        response = MailChimpAPI.fleetPrideSubscribe('ana.santellana@fleetpride.com');        
        Test.stopTest();
    }
    
    public class MAILCHIMPMock implements HttpCalloutMock {
        public Integer code {get; set;}
        public String status {get; set;}
        public List<String> body {get; set;}
        
        public MAILCHIMPMock(Integer code, String status, String body) {
            this.code = code;
            this.status = status;
            this.body = new List<String>{body};
                }
        
        public HTTPResponse respond(HTTPRequest request) {
            HttpResponse response = new HttpResponse();
            if (this.body != null && !this.body.isEmpty()) {
                response.setBody(this.body[0]);
                this.body.remove(0);
            }
            response.setStatusCode(this.code);
            response.setStatus(this.status);
            return response;
        }
    }

}