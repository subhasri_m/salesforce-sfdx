global class CCPDCLogicCartAddTo extends ccrz.ccLogicCartAddTo {

    global override Map<String, Object> process(Map<String, Object> inputData) {

        ccrz.ccLog.log('******************CCPDCLogicCartAddTo process***************************');
        Map<String, Decimal> priceMap = CCPDCCallContext.priceMap;
        if (priceMap == null) {
            CCPDCCallContext.skipPricing = true;
        }

        Map<String, Object> outputData = super.process(inputData);
        ccrz.ccLog.log('****************** end CCPDCLogicCartAddTo process***************************');
        return outputData;        
    }

    global override Map<String, Object> addMajorLineItems(Map<String, Object> inputData) {
        ccrz.ccLog.log('addMajorLineItems inputData: ' + inputData);
        Map<String, Object> outputData = super.addMajorLineItems(inputData);
        ccrz.ccLog.log('addMajorLineItems outputData: ' + outputData);
        ccrz__E_CartItem__c cartItem = (ccrz__E_CartItem__c) outputData.get('currItem');
        System.debug('#### cartItem '+cartItem);
        // Set timestamp on cart item when added to cart.
        
        cartItem.CC_FP_Price_Timestamp__c = Datetime.now();
        // for FP Storefront only
        String fulfillmentType = String.ValueOf(ccrz.cc_CallContext.currPageParameters.get('deliveryOption'));
        if(fulfillmentType != null && fulfillmentType.length() > 0	) {
        	cartItem.FulFillMentType__c = fulfillmentType;
        }
        update cartItem;
		return outputData;
    }
    
    //CloudCraze test code - JLowenthal
    private Map<String, Decimal> originalExtPrices;
    
    global override Map<String, Object> addCurrentLineDataToCart(Map<String, Object> inputData) {
   
        if(originalExtPrices == null)
            originalExtPrices = new Map<String, Decimal>();
        List<Object> workingLines = (List<Object>) inputData.get(ccrz.ccApiCart.WORKING_LINE_DATAS);
        for (Object currentLine : workingLines) {
            Map<String, Object> line = (Map<String, Object>) currentLine;
            originalExtPrices.put((String)line.get(ccrz.ccApiCart.LINE_DATA_SKU), (Decimal)line.get(ccrz.ccApiCart.LINE_DATA_PRICE));
            line.put(ccrz.ccApiCart.LINE_DATA_PRICE, null);

        }

        super.addCurrentLineDataToCart(inputData);
        Map<String, Object> prodMap = (Map<String, Object>) inputData.get('ProductMap');
        for(String key : prodMap.keySet()){
            Map<String, Object> prodDetail = (Map<String,Object>)prodMap.get(key);
            for(String key2 : prodDetail.keySet()){
            	originalExtPrices.put((String)prodDetail.get('sfid'), originalExtPrices.get((String)prodDetail.get('SKU')));
            }
        }
        List<ccrz__E_CartItem__c> cartItemObjs = (List<ccrz__E_CartItem__c>)inputData.get(ccrz.ccApiCart.CARTITEMS_TO_UPSERT);

        for(ccrz__E_CartItem__c cartItemObj : cartItemObjs){
            if(originalExtPrices.get(cartItemObj.ccrz__Product__c) != null){
                cartItemObj.ccrz__Price__c = (Decimal)originalExtPrices.get(cartItemObj.ccrz__Product__c);
                cartItemObj.ccrz__OriginalItemPrice__c = cartItemObj.ccrz__Price__c;
				cartItemObj.ccrz__Subamount__c = cartItemObj.ccrz__Price__c * cartItemObj.ccrz__Quantity__c;
                if(ccrz.cc_CallContext.storefront != 'parts'){
                    cartItemObj.ccrz__PricingType__c = 'external';   
                }
                
            }
        }

        upsert cartItemObjs;
        return inputData;
    }
}