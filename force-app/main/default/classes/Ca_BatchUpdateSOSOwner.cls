global class Ca_BatchUpdateSOSOwner implements Schedulable {
   global void execute(SchedulableContext sc) {
      Id batchInstanceID = Database.executeBatch(new Ca_UpdateSOSOwner(), 50);
   }
}