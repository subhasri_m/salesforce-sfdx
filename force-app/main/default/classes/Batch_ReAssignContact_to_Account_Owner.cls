global class Batch_ReAssignContact_to_Account_Owner implements Schedulable {
   global void execute(SchedulableContext sc) {
      Id batchInstanceID = Database.executeBatch(new ReAssignContact_to_Account_Owner());
   }
}