/* 
@Author : Rahul Bansal
@Created Date : 15-Mar-2019
@Company : PureSoftware
 
*/

public class TriggerOnCartItemHelpler {

public static void UpdatePricingType(List<ccrz__E_CartItem__c> cartItems){

for (ccrz__E_CartItem__c cartItem :cartItems ){
 if (cartItem.CC_FP_DoNotReprice__c!=true)
 cartItem.ccrz__PricingType__c = 'auto';
 else
 cartItem.ccrz__PricingType__c = 'external';
}

}

/* 
@Author : Rahul Bansal
@Created Date : 15-Mar-2019
@Company : PureSoftware
@Description : This function is updating the parent cart's checkbox  if there is atleast one cart_item in the cart. */


public static void UpdatefirstCartItemCheck(List<ccrz__E_CartItem__c> cartItems){
List<ccrz__E_CartItem__c> singleCartItemlst = new List<ccrz__E_CartItem__c>();
Set<ID> cartIdsToUpdate = new Set<ID>();
set<ID> cartIds= new set<ID>();    
List<ccrz__E_Cart__c> carttoupdatelst = new List<ccrz__E_Cart__c>(); 

for (ccrz__E_CartItem__c cartItem :cartItems ){
 cartIds.add(cartItem.ccrz__Cart__c);
}

    
    if (cartIds != null ){
for (AggregateResult result : [SELECT ccrz__Cart__c, count(id) FROM ccrz__E_CartItem__c where ccrz__Cart__c in : cartIds GROUP BY ccrz__Cart__c HAVING count(Id) >= 1]){
 cartIdsToUpdate.add((Id)(result.get('ccrz__Cart__c')));
}

} 

    
if (!cartIdsToUpdate.IsEmpty() || cartIdsToUpdate!=null ){
for (ccrz__E_Cart__c carttoupdate : [select Id , CartItemCheck__c from ccrz__E_Cart__c where id in : cartIdsToUpdate]){
carttoupdate.CartItemCheck__c= True;
carttoupdatelst.add(carttoupdate);
}
}
     
try {
if (!carttoupdatelst.IsEmpty() || carttoupdatelst!=null)
    update carttoupdatelst ;
}
catch (Exception e)
{
System.debug('Exception : ' + e.getMessage());
}

}
/*
@Author : Rahul Bansal
@Created Date : 20-Mar-2019
@Company : PureSoftware
@Description : This function is updating the parent cart's checkbox  if there is zero cart_item in the cart after deletion. */

public static void DeletefirstCartItemCheck(List<ccrz__E_CartItem__c> cartItems){
set<ID> cartIds= new set<ID>();
set<ID> cartIdsToremove= new set<ID>();
List<ccrz__E_Cart__c> carttoupdatelst = new List<ccrz__E_Cart__c>();
    for (ccrz__E_CartItem__c cartItem :cartItems ){
     cartIds.add(cartItem.ccrz__Cart__c);
}

if (cartIds != null ){
 for (ccrz__E_CartItem__c  cartitm : [SELECT ccrz__Cart__c, id, Name FROM ccrz__E_CartItem__c where ccrz__Cart__c in : cartIds]){
 cartIdsToremove.add(cartitm.ccrz__Cart__c);
 }
}

cartIds.removeAll(cartIdsToremove);

if (!cartIds.IsEmpty() || cartIds!=null ){
for (ccrz__E_Cart__c carttoupdate : [select Id , CartItemCheck__c from ccrz__E_Cart__c where id in : cartIds]){
carttoupdate.CartItemCheck__c= false;
carttoupdatelst.add(carttoupdate);
}
}
   
try {
if (!carttoupdatelst.IsEmpty() || carttoupdatelst!=null)
    update carttoupdatelst ;
}
catch (Exception e)
{
System.debug('Exception : ' + e.getMessage());
}

}

}