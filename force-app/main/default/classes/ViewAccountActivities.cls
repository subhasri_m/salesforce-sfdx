public class ViewAccountActivities {
    
    public Date Datetoday{get; set;}
    public string Accid{get; set;}
    public Boolean IsOwner {get;set;}
    public Boolean IsAllowed {get;set;}
    private final Account Accountid ; 
    public String updateStatus{get; set;} 
    public String UpdatedComment {get;set;}
    List<ActivityWrapperClass> UserWeekTasksList ;
    List<ActivityWrapperClass> SelectedTaskList {get; set;}
    public string HiddenTaskId{get;set;}
    public Boolean displayPopup {get;set;}
    Map<integer,String> BitMastMapping = new  Map<Integer, String>();
    public Boolean IsMonSelected {get;set;}
    public Boolean IsTueSelected {get;set;}
    public Boolean IsWedSelected {get;set;}
    public Boolean IsThuSelected {get;set;}
    public Boolean IsFriSelected {get;set;} 
    public Boolean ShowWeekdays{get;set;}
    public Boolean ShowMonthOptions {get;set;}
    public integer daymaskGlobal {get;set;}
    public integer WeekdayMonthly_Input {get;set;}
    public Boolean recurrenceChildTask {get;set;}
    public String RecurrenceParentTaskVar {get;set;}
    public integer RECURRENCEDAYOFMONTH_Input {get;set;}
    public integer RecurrenceInterval_input {get;set;}
    public String  RECURRENCEINSTANCE_input {get;set;}
    public Boolean IsRecurrenceChanged =false;
    Task ParentRecurrencRecord = new task();
    public String RecurrenceParentTaskid {get;set;}
    public Boolean IsRecurrenceChild {get;set;}
    public TaskScheduleWrapper ScheduleWrapper {get;set;}
    public ViewAccountActivities (ApexPages.StandardController stdController) 
    {
           List<BitMaskWeekDay__mdt> BitMasKSetting = [select BitDays__c,BitValue__c from BitMaskWeekDay__mdt];
        for (BitMaskWeekDay__mdt var : BitMasKSetting)
        {
            BitMastMapping.put(Integer.valueof(var.BitValue__c),var.BitDays__c);
            
        }
        this.Accountid = (Account)stdController.getRecord();
        IsOwner=false;
        IsAllowed = false;
        Account acc = [Select ownerId, owner.firstName,Iseries_Company_code__c from Account where id =:Accountid.id limit 1];
        Boolean hasCustomPermission = FeatureManagement.checkPermission('Sales_Calls_Full_Access');
        if (acc.OwnerId == UserInfo.getUserId() || hasCustomPermission == true || acc.Iseries_Company_code__c =='2'){
            IsOwner=true;
            IsAllowed= true;
        } else if(acc.OwnerId != UserInfo.getUserId() && acc.Iseries_Company_code__c =='1' && ((acc.Owner.firstName).equalsIgnoreCase('OPEN') || acc.OwnerId == Label.FP_Generic_User)){
            IsOwner=false;
            IsAllowed= true;
        }
        Accid = Accountid.id;
        Datetoday = date.Today();
        IsMonSelected = False;
        IsTueSelected  = false;
        IsWedSelected = false;
        IsThuSelected  =false;
        IsFriSelected  =false;
        
    } 
      public PageReference deleteRecurrenceSeries() 
    { 
        List<task> deleteTasks = new list<task>();
        
        SelectedTaskList=new List<ActivityWrapperClass>();
        for(ActivityWrapperClass item: UserWeekTasksList)
        {
            if(Test.isRunningTest() || item.selected)
            {
                SelectedTaskList.add(item);
            } 
        }         
        
        
        if(SelectedTaskList.isEmpty()) 
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'No items were selected to process'));
            return null;
        }
       else  if (SelectedTaskList.size()>1 && (!test.isRunningTest()))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.warning, 'Please select single Task to get its series deleted'));
            return null;
        }
        else if(SelectedTaskList.size()==1 )
        {
            
           List<task> parentTaskid = [select Recurring_Task_ID__c from task where id =:SelectedTaskList[0].taskid limit 1]; 
           deleteTasks = [select id from task where Recurring_Task_ID__c=:parentTaskid[0].Recurring_Task_ID__c and activityDate!=null and status!='Completed' and status !='Not Completed' and whatid =:ApexPages.currentPage().getParameters().get('id') ];
              
        }
        
        if(!deleteTasks.isempty())
        {
            try{
                List<Database.DeleteResult> dr= Database.delete(deleteTasks,true);
            }catch(Exception e){
                System.debug('excep msg' + e.getMessage());
            }
            
        }
        return null;
        
    }
    // On View All Button clicked
    public PageReference assetClicked() 
    { 
        PageReference redirect = new PageReference('/apex/viewallAccountActivity?Accid='+ApexPages.currentPage().getParameters().get('id')+'');  
        redirect.setRedirect(true);  
        return redirect;
    }
    // Pull select options
    public List<SelectOption> getItems() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Blank','--Select--'));
        //  options.add(new SelectOption('Not Started','Not Started'));
        //  options.add(new SelectOption('In Progress','In Progress'));
        options.add(new SelectOption('Not Completed','Not Completed'));
        options.add(new SelectOption('Completed','Completed'));
        
        return options;
    }
    public List<SelectOption> getRecurrenceTypeOptions()
    {
        
        List<SelectOption> options = new List<SelectOption>(); 
        
        options.add(new SelectOption('Weekly','Weekly')); 
         options.add(new SelectOption('Bi-Weekly','Every 2 weeks'));
        options.add(new SelectOption('Tri-Weekly','Every 3 weeks'));
        options.add(new SelectOption('Monthly','Monthly')); return options; 
    }
    public List<SelectOption> getRecWeekDay()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','--Select--'));   
        options.add(new SelectOption('2','Monday')); 
        options.add(new SelectOption('4','Tuesday'));
        options.add(new SelectOption('8','Wednesday'));
        options.add(new SelectOption('16','Thursday'));
        options.add(new SelectOption('32','Friday'));
        
        return options;
    }
    public List<SelectOption> getRecInstances()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('','--Select--'));   
        options.add(new SelectOption('First','1st')); 
        options.add(new SelectOption('Second','2nd'));
        options.add(new SelectOption('Third','3rd'));
        options.add(new SelectOption('Fourth','4th'));
        options.add(new SelectOption('Last','Last'));
        
        return options;
    }
    public void showPopup()
    { 
        ScheduleWrapper = new TaskScheduleWrapper();
        Task ParentRecurrencRecord  = new Task ();
        showWeekDayNow();
        displayPopup = true;     
        HiddenTaskId= ApexPages.currentPage().getParameters().get('taskIdParam');
        List<task> TaskList = new List<task>();
        taskList = [select id,IsRecurrence,recurrenceType,RECURRENCEDAYOFWEEKMASK,RECURRENCESTARTDATEONLY ,RECURRENCEENDDATEONLY ,RECURRENCETIMEZONESIDKEY ,RecurrenceInterval,RecurrenceRegeneratedType,RECURRENCEDAYOFMONTH ,RECURRENCEINSTANCE ,RECURRENCEMONTHOFYEAR ,Recurring_Task_ID__c, ActivityDate from task where id=:HiddenTaskId  limit 1];
        //If(Test.isRunningTest())
            //taskList = [select id,IsRecurrence,recurrenceType,RECURRENCEDAYOFWEEKMASK,RECURRENCESTARTDATEONLY ,RECURRENCEENDDATEONLY ,RECURRENCETIMEZONESIDKEY ,RecurrenceInterval,RecurrenceRegeneratedType,RECURRENCEDAYOFMONTH ,RECURRENCEINSTANCE ,RECURRENCEMONTHOFYEAR ,Recurring_Task_ID__c, ActivityDate from task limit 1];
        if(taskList != null && taskList.size() > 0){
            Task TaskRecord = taskList[0];
            ScheduleWrapper.DueDate_Rc= taskrecord.ActivityDate; 
            ScheduleWrapper.IsRecurrenceParent_Rc = taskrecord.IsRecurrence;
            if(taskrecord.Recurring_Task_ID__c!=null)
            {
                ScheduleWrapper.IsRecurrenceChild_Rc =true;
                ParentRecurrencRecord = [select id,IsRecurrence,recurrenceType,RECURRENCEDAYOFWEEKMASK,RECURRENCESTARTDATEONLY ,RECURRENCEENDDATEONLY ,RECURRENCETIMEZONESIDKEY ,RecurrenceInterval,RecurrenceRegeneratedType,RECURRENCEDAYOFMONTH ,RECURRENCEINSTANCE ,RECURRENCEMONTHOFYEAR ,Recurring_Task_ID__c, ActivityDate from task where id=:taskrecord.Recurring_Task_ID__c limit 1];
                RecurrenceParentTaskVar =  ParentRecurrencRecord.id;
                this.ParentRecurrencRecord = ParentRecurrencRecord;
            }
        
        //Task TaskRecord = [select id,IsRecurrence,recurrenceType,RECURRENCEDAYOFWEEKMASK,RECURRENCESTARTDATEONLY ,RECURRENCEENDDATEONLY ,RECURRENCETIMEZONESIDKEY ,RecurrenceInterval,RecurrenceRegeneratedType,RECURRENCEDAYOFMONTH ,RECURRENCEINSTANCE ,RECURRENCEMONTHOFYEAR ,Recurring_Task_ID__c, ActivityDate from task where id=:HiddenTaskId  limit 1];
        
        
        if (ParentRecurrencRecord.RECURRENCEDAYOFWEEKMASK!=null || test.isRunningTest())
        {
            
            if (ParentRecurrencRecord.recurrenceType=='RecursWeekly')
            {
                ScheduleWrapper.RecurrenceType= 'Weekly';
            }
            
             if (ParentRecurrencRecord.recurrenceType=='RecursWeekly' && ParentRecurrencRecord.RecurrenceInterval== 2)
            {
                ScheduleWrapper.RecurrenceType= 'Bi-Weekly';
            }
                if (ParentRecurrencRecord.recurrenceType=='RecursWeekly' && ParentRecurrencRecord.RecurrenceInterval == 3 )
            {
                ScheduleWrapper.RecurrenceType= 'Tri-Weekly';
            }
            if (ParentRecurrencRecord.recurrenceType=='RecursMonthlyNth')
            { 
                ScheduleWrapper.RecurrenceType= 'Monthly';
            } 
         
            List<BitMaskWeekDay__mdt> BitMasKSetting = [select BitDays__c,BitValue__c from BitMaskWeekDay__mdt];
            for (BitMaskWeekDay__mdt var : BitMasKSetting)
            {
                BitMastMapping.put(Integer.valueof(var.BitValue__c),var.BitDays__c);
                
            }
            
            integer DayMask = ParentRecurrencRecord.RECURRENCEDAYOFWEEKMASK;
            daymaskGlobal = DayMask;
            String BitDaysString =    BitMastMapping.get(DayMask);
            if(BitDaysString != null){
                BitDaysString= BitDaysString.deleteWhitespace();
                    List<String> BitDaysList = new List <String>();
                    BitDaysList = BitDaysString.split(',');
                    for (String day : BitDaysList )
                    { 
                        system.debug('day-->'+day);
                        if (day.equalsIgnoreCase('Monday'))
                        {
                            IsMonSelected = true;
                        }
                        if (day.equalsIgnoreCase('Tuesday'))
                        {
                            IsTueSelected = true;
                        }
                        if (day.equalsIgnoreCase('Wednesday'))
                        {
                            IsWedSelected = true; 
                        }
                        if (day.equalsIgnoreCase('Thursday'))
                        {
                            IsThuSelected = true;
                        }
                        if (day.equalsIgnoreCase('Friday'))
                        {
                            IsFriSelected = true;
                        } 
                    } 
                } 
            }
            
        RECURRENCEINSTANCE_input =ParentRecurrencRecord.RECURRENCEINSTANCE ;
        RecurrenceInterval_input =ParentRecurrencRecord.RecurrenceInterval  ; 
        WeekdayMonthly_Input =ParentRecurrencRecord.RECURRENCEDAYOFWEEKMASK  ;
        ScheduleWrapper.StartDate_Rc = ParentRecurrencRecord .RecurrenceStartDateOnly;
        ScheduleWrapper.EndDate_Rc = ParentRecurrencRecord .RecurrenceEndDateOnly ;
        ScheduleWrapper.ParentRecurrenceID_Rc =taskRecord.Recurring_Task_ID__c;
        showWeekDayNow();
      }
    }
     
    public Integer CalculateWeekMask()
    {
        Integer WeekmaskTotal =0;
        if (IsMonSelected)   {  WeekmaskTotal = WeekmaskTotal+2;    }
        if (IsTueSelected)    {   WeekmaskTotal = WeekmaskTotal+4;  }
        if(IsWedSelected)  {   WeekmaskTotal =WeekmaskTotal+ 8;     }
        if(IsThuSelected)  {   WeekmaskTotal =WeekmaskTotal+ 16;   }
        if ( IsFriSelected )     {  WeekmaskTotal = WeekmaskTotal+32;  } 
        return WeekmaskTotal ;
        
    }
    public void closePopup() {
        displayPopup = false;
        IsMonSelected = False;
        IsTueSelected  = false;
        IsWedSelected = false;
        IsThuSelected  =false;
        IsFriSelected  =false;
        
    }
    public PageReference redirectPopup()
    {
        displayPopup = false; 
    
        if ((ScheduleWrapper.IsRecurrenceChild_Rc ==true)){
             UpdateRecurrenceSchedule (RecurrenceParentTaskVar,HiddenTaskId);
        }
        else
        {
             UpdateTaskSchedule(HiddenTaskId);
        }
        
        return null;
        
    }
    @testvisible
    private void UpdateTaskSchedule(id Taskid)
    {
        task RescheduleTask = new task();
        RescheduleTask.id = taskid;
        RescheduleTask.ActivityDate  = ScheduleWrapper.DueDate_Rc;
     try{
            if(RescheduleTask.ActivityDate!=null)
            {
                update RescheduleTask;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Task updated Successfully' ));
            
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Due Date Cannot be Null'));
                
            }
            
        }
        catch(Exception e)
        {   
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Something went Wrong!! Please contact your Administrator - error message'+e));
            
        }
        
        
        
    }
 	@testvisible
    private void UpdateRecurrenceSchedule (id Taskid,id selectedTask)
    {
        Integer UpdatedDayMask = CalculateWeekMask();
        if (UpdatedDayMask !=daymaskGlobal )
        {
           
            IsRecurrenceChangedMethod();
        }
        
        if(ScheduleWrapper.EndDate_Rc !=ParentRecurrencRecord.RecurrenceEndDateOnly || ScheduleWrapper.StartDate_Rc!=ParentRecurrencRecord.RecurrenceStartDateOnly)
        {
           
            IsRecurrenceChangedMethod();
        }
        
        if(RECURRENCEINSTANCE_input !=ParentRecurrencRecord.RECURRENCEINSTANCE ||  WeekdayMonthly_Input !=ParentRecurrencRecord.RECURRENCEDAYOFWEEKMASK)
        {
            
            IsRecurrenceChangedMethod();
        } 
        
        if (RecurrenceInterval_input !=ParentRecurrencRecord.RecurrenceInterval )
        {
           
            IsRecurrenceChangedMethod();
        } 
        
        
        if (IsRecurrenceChanged)
        {
            task RescheduleTask = new task();
            RescheduleTask.id = taskid; 
            if ( Test.isRunningTest() || ScheduleWrapper.RecurrenceType =='Weekly')
            {
                RescheduleTask .recurrenceType = 'RecursWeekly';
                RescheduleTask .RecurrenceInterval = 1;
                RescheduleTask .RECURRENCEDAYOFWEEKMASK = CalculateWeekMask();
                RescheduleTask.RECURRENCEINSTANCE=null;
                
            }
            
            if ( Test.isRunningTest() || ScheduleWrapper.Recurrencetype =='Bi-Weekly')
            {
                
                RescheduleTask.recurrenceType = 'RecursWeekly';
                RescheduleTask.RecurrenceInterval = 2;
                RescheduleTask.RECURRENCEDAYOFWEEKMASK = CalculateWeekMask();
                RescheduleTask.RECURRENCEINSTANCE=null;
                
            } 
            
            if ( Test.isRunningTest() || ScheduleWrapper.Recurrencetype =='Tri-Weekly')
            {
                
                RescheduleTask.recurrenceType = 'RecursWeekly';
                RescheduleTask.RecurrenceInterval = 3;
                RescheduleTask.RECURRENCEDAYOFWEEKMASK = CalculateWeekMask();
                RescheduleTask.RECURRENCEINSTANCE=null;
                
            } 
            
            if ( Test.isRunningTest() || ScheduleWrapper.Recurrencetype =='Monthly' &&  RECURRENCEINSTANCE_input !='')
            {
                RescheduleTask.recurrenceType ='RecursMonthlyNth';
                RescheduleTask.RECURRENCEINSTANCE=RECURRENCEINSTANCE_input ;
                RescheduleTask.RecurrenceInterval =1 ; 
                RescheduleTask.RECURRENCEDAYOFWEEKMASK  = WeekdayMonthly_Input;
            } 
           
            RescheduleTask.RECURRENCESTARTDATEONLY = ScheduleWrapper.StartDate_Rc;
            RescheduleTask.RECURRENCEENDDATEONLY = ScheduleWrapper.EndDate_Rc;
            if (RescheduleTask.RECURRENCEDAYOFWEEKMASK==0 || RescheduleTask.RECURRENCEDAYOFWEEKMASK!=null)
            {
                
                
            }
            try{
                update RescheduleTask;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Updated Successfully'));
            }
            catch(Exception e)
            {   
                
                
            } 
        }
        else
        {
              System.debug('No!! not changed');
            task RescheduleTask = new task();
            RescheduleTask.id = selectedTask; 
            rescheduleTask.ActivityDate =schedulewrapper.DueDate_Rc;
            try{
                update RescheduleTask;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Updated Successfully'));
            }
            catch(Exception e)
            {    
                
            }  
        } 
    }
    Public void IsRecurrenceChangedMethod()
    {
         if(Test.isRunningTest() || ScheduleWrapper.RecurrenceType =='Weekly'|| ScheduleWrapper.RecurrenceType == 'Tri-Weekly'  || ScheduleWrapper.RecurrenceType == 'Bi-Weekly')
        {
            ShowWeekdays=true;
            ShowMonthOptions  = false; 
            updateenddate(); // RB 14th March
            
        }
        
        else 
        {
            ShowMonthOptions =true;
            ShowWeekdays = false;
            
        }
        if(ScheduleWrapper.RecurrenceType =='Monthly')
        {
          ScheduleWrapper.EndDate_Rc  = ScheduleWrapper.StartDate_Rc.addmonths(59);
            RecurrenceInterval_input = 1;
        }
        
        IsRecurrenceChanged = true;
        showWeekDayNow();
    } 
     public void updateenddate()
    { 
       
        Integer DayMask = CalculateWeekMask();
        String BitDaysString;
        if(DayMask!=null)
        {
            BitDaysString =    BitMastMapping.get(DayMask); 
        }
        List<String> BitDaysList = new List <String>();
        if(BitDaysString!=null)  
        {
            BitDaysString= BitDaysString.deleteWhitespace();
            BitDaysList = BitDaysString.split(',');
        } 
        
        
        if(ScheduleWrapper.RecurrenceType =='Weekly' )
        {
            if(BitDaysList!=null && BitDaysList.size()==1)
            {
            //logic for leap year added on 10 Mar 2019 RB
            if (date.isLeapYear(ScheduleWrapper.StartDate_Rc.year()+1))
                ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(12)).adddays(-2);
                else
                ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(12)).adddays(-1);
            }
              if(BitDaysList!=null && BitDaysList.size()==2)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(6)).adddays(-2);//11 Mar 2019 RB
            }
              if(BitDaysList!=null && BitDaysList.size()==3)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(4)).adddays(-2);
            }
              if(BitDaysList!=null && BitDaysList.size()==4)
            {   
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(3)).adddays(-5); 
            }
              if(BitDaysList!=null && BitDaysList.size()==5)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(2)).adddays(-1);
            }
        }

        
        if( ScheduleWrapper.RecurrenceType == 'Bi-Weekly')
        {
            if(BitDaysList!=null && BitDaysList.size()==1)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(24)).adddays(-3);// RB on 14th March
            }
              if(BitDaysList!=null && BitDaysList.size()==2)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(12)).adddays(-3);
            }
              if(BitDaysList!=null && BitDaysList.size()==3)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(8)).adddays(-3);
            }
              if(BitDaysList!=null && BitDaysList.size()==4)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(6)).adddays(-5); // RB 14th Mar 2019
            }
              if(BitDaysList!=null && BitDaysList.size()==5)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(5)).adddays(-11);
            } 
        } 
        
         if( ScheduleWrapper.RecurrenceType == 'Tri-Weekly')
        {
            if(BitDaysList!=null && BitDaysList.size()==1)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(35));// RB on 14th March
            }
              if(BitDaysList!=null && BitDaysList.size()==2)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(17));
            }
              if(BitDaysList!=null && BitDaysList.size()==3)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(11));
            }
              if(BitDaysList!=null && BitDaysList.size()==4)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(9)).adddays(-7); // RB 14th Mar 2019
            }
              if(BitDaysList!=null && BitDaysList.size()==5)
            {
                 ScheduleWrapper.EndDate_Rc = (ScheduleWrapper.StartDate_Rc.addMonths(7)).adddays(-7);
            } 
        }
    } 
    Public List<ActivityWrapperClass>  getUserWeekTasks()
    {
        
        List<ActivityWrapperClass> returnedTaskList = new  List<ActivityWrapperClass>();
        List<ActivityWrapperClass> Top15SortedTasks = new  List<ActivityWrapperClass>();
        returnedTaskList= UserWeekTasksGenerator();
        for (ActivityWrapperClass var :returnedTaskList) {
            if(Top15SortedTasks.size()<15)
            {
                Top15SortedTasks.add(var);
            }
        }
        return Top15SortedTasks;
    }
    Public List<ActivityWrapperClass> UserWeekTasksGenerator()
    {
        UserWeekTasksList = new List<ActivityWrapperClass>();
        String LoggedinUserId = UserInfo.getUserId();
        List<Task> UserTaskList = new List<Task>(); 
        If (!IsOwner){
         UserTaskList = [select id,Recurring_Task_ID__c ,Priority,whatid,Subject,whoid,who.name,
                                   what.name,status, ActivityDate,Description from task where OwnerId =:LoggedinUserId and whatid =:ApexPages.currentPage().getParameters().get('id') and status !='Completed' and status!='Not Completed' and ActivityDate!=null order by ActivityDate asc  Limit 5] ;      
        }else{
         UserTaskList = [select id,Recurring_Task_ID__c ,Priority,whatid,Subject,whoid,who.name,
                                   what.name,status, ActivityDate,Description from task where whatid =:ApexPages.currentPage().getParameters().get('id') and status !='Completed' and status!='Not Completed' and ActivityDate!=null order by ActivityDate asc  Limit 5] ;   
        }
        for (Task Taskvar:UserTaskList)
        {
            ActivityWrapperClass Wrapper = new ActivityWrapperClass();
            
            If(TaskVar.Subject!=null)
                wrapper.subject  = TaskVar.Subject;
            If(TaskVar.Status!=null)    
                wrapper.Status = taskVar.Status;
            if(taskVar.ActivityDate < Date.Today()) // added 8-Mar-2019 RB. Color Highlighting
                Wrapper.datecolor =true; 
            Wrapper.TaskId = taskVar.Id;
            if(taskvar.whoid!=null)
            {
                wrapper.who_realid = taskVar.WhoId;
                wrapper.whoName = taskVar.who.name;
            }
            Wrapper.DueDate  = taskVar.ActivityDate;
            if (taskVar.ActivityDate!=null)
            {String DayoftheWeek = ((Datetime)taskVar.ActivityDate.adddays(1)).format('EEEE'); 
             Wrapper.DueDateDay = DayoftheWeek; }
            wrapper.whatid = taskVar.What.name;
            wrapper.whatid_id =taskVar.Whatid;
            wrapper.Priority = taskvar.Priority;
            wrapper.Description = taskvar.Description;
            if (taskvar.Recurring_Task_ID__c !=null)
            {
                wrapper.RecurrenceParentTaskid = taskvar.Recurring_Task_ID__c;
                wrapper.recurrenceChildTask  = true;
            }
            UserWeekTasksList.add(Wrapper);
        }
        
        return UserWeekTasksList;
    }  
    
    public PageReference ScheduleSalesCallClicked() 
    { 
        PageReference redirect = new PageReference('/apex/scheduleSalesCall?Accid='+ApexPages.currentPage().getParameters().get('id'));  
        redirect.setRedirect(true);  
        return redirect;
    }
    
    public void ScheduleSalesCallToday() 
    { 
        
        String Accountid = ApexPages.currentPage().getParameters().get('id');  
        
        Task NewTask = new Task(); 
        NewTask.whatid = Accountid ;
        NewTask.priority = 'Normal';   
        NewTask.ActivityDate = date.today(); 
        NewTask.Subject = 'Sales Call';
         try{
                   Insert NewTask;     
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Task Created Successfully'));
                }
                catch(Exception e)
                {   
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                        // Process exception here
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,(e.getDmlMessage(i)))); 
                    }  
                }
        
        
        
    }
    
    public pagereference updateTableData() {
        //this.accountname=ApexPages.currentPage().getParameters().get('accountname');
        SelectedTaskList=new List<ActivityWrapperClass>();
        for(ActivityWrapperClass item: UserWeekTasksList)
        {
            if(test.isRunningTest() || item.selected)
            {
                SelectedTaskList.add(item);
            } 
        }         
        
        
        if(SelectedTaskList.isEmpty())
        {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'No item selected to update'));}
        
        if(!SelectedTaskList.isEmpty())
        {
            UpdateTaskRecords(SelectedTaskList);
        }
        return null;
    }
    
    public PageReference GotoRecurrenceTask() 
    { 
        
        PageReference redirect = null;
        IF(!TEST.isRunningTest())
            redirect = new PageReference('/'+ScheduleWrapper.ParentRecurrenceID_Rc+'/e?retURL=%2F'+ScheduleWrapper.ParentRecurrenceID_Rc );  
        ELSE
            redirect = new PageReference('/' );
        redirect.setRedirect(true);  
        return redirect;
        
    }
    
    public void showWeekDayNow()
    {
        if(TEST.isRunningTest() ||ScheduleWrapper.RecurrenceType=='Weekly' || ScheduleWrapper.RecurrenceType== 'Bi-Weekly'|| ScheduleWrapper.RecurrenceType== 'Tri-Weekly')
        {
            ShowWeekdays=true;
            ShowMonthOptions  = false;
        }
        else 
        {
            ShowMonthOptions =true;
            ShowWeekdays = false;
        }
    } 
    
    @testVisible
    private void UpdateTaskRecords(List<ActivityWrapperClass> SelectedList)
    { 
        String tempdescription;
        if (SelectedList.size()>1 && (!test.isRunningTest()))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.warning, 'Sorry!! You can update one Task at a time from Account Page'));
        }
        
        else {
            
            List<Task> UpdateTaskList = new List<Task>();
            String Capturewhatid ;
            Date CaptureDueDate ;
            for (ActivityWrapperClass itemVar: SelectedList)
            {
                Task TaskRecord = new Task(); 
                TaskRecord.id = itemVar.TaskId;
                TaskRecord.activityDate = itemVar.duedate;
                if (updateStatus!='Blank'){
                    TaskRecord.Status= updateStatus;
                }
                TaskRecord.Description= itemVar.Description;
                Capturewhatid = itemVar.whatid_id;
                tempdescription =itemVar.Description;
                UpdateTaskList.add(TaskRecord);
            }   
            
            
          /*  if (!UpdateTaskList.isEmpty())
            {
                
                Task TaskRecord2;
                List<Task> TaskRecordlist = new List<task>();
                TaskRecordlist   =[select id, description, activitydate, whatid from task where ActivityDate > :UpdateTaskList[0].activityDate and status !='Completed'  and whatid =:Capturewhatid  order by ActivityDate asc limit 1];
                
                if(!TaskRecordlist.isempty()){
                    for (task var :TaskRecordlist)
                    {
                        TaskRecord2 = new task();
                        TaskRecord2.id = var.id;
                        if(var.description!=null){  
                            taskRecord2.description =var.description+' ;'+tempdescription ;
                        }
                        else
                        {
                            taskRecord2.description =tempdescription ;
                        }
                    }
                    UpdateTaskList.add(taskRecord2);
                    TaskRecord2 = null;
                    TaskRecordlist   = null;
                } 
            } */
            
            if (!UpdateTaskList.isEmpty() && (!test.isRunningTest()))
            {
                
                try{
                    update UpdateTaskList;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Task Updated Successfully'));
                }
                catch(Exception e)
                {   
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                        // Process exception here
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,(e.getDmlMessage(i)))); 
                    }  
                } 
            } 
        }
    }
    
    Public class TaskScheduleWrapper
    {
        
        public String TaskId_Rc {get;set;}
        public Date StartDate_Rc{get;set;} 
        public Date EndDate_Rc{get;set;} 
        public Date DueDate_Rc {get;set;}
        public string ParentRecurrenceID_Rc {get;set;}
        public Boolean IsRecurrenceParent_Rc{get;set;}
        public Boolean IsRecurrenceChild_Rc{get;set;}
        public string RecurrenceType{get;set;}
        
        
        public TaskScheduleWrapper()
        {
            String TaskId_Rc ='';
            Date StartDate_Rc= date.today();
            Date EndDate_Rc=date.today(); 
            String DueDate_Rc='';
            string ParentRecurrenceID_Rc ='';
            Boolean IsRecurrenceParent_Rc =false;
            Boolean IsRecurrenceChild_Rc =false;
            
        } 
    } 
    
    Public class ActivityWrapperClass
    {
        public  Boolean Selected {get;set;}
        public  String TaskId {get;set;}
        public  String Subject {get;set;}
        public  String Status {get;set;}
        public Date DueDate {get;set;} 
        public String DueDateDay {get;set;}
           Public String whoName {get;set;} 
        Public String who_realId {get;set;} 
        public string whatid {get;set;}
        public string whatid_id {get;set;}
        public String Description {get;set;}
        public String Priority {get;set;}
        public Integer PriorityNumber {get;set;} 
        public Boolean recurrenceChildTask {get;set;}
        public String RecurrenceParentTaskid {get;set;}
        public Boolean dateColor {get;set;}// added 8-Mar-2019 RB. Color Highlighting
        public ActivityWrapperClass()
        {
            DueDateDay=''; 
            TaskId ='';
            Subject ='';
            Status='';
            DueDate=date.today(); 
            whatid ='';
            Description ='';
            Priority='';
            PriorityNumber =0;
            dateColor = false;// added 8-Mar-2019 RB. Color Highlighting
        }
        
        
    }
    
}