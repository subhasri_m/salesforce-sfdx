@IsTest
public class testDeleteOverdueTasks {

    static testmethod void insertTask()  {
    
    Task t = new Task();
    Task t2 = new Task();
    
    t.status='Not Started';
    t.Objective__c = 'Housekeeping';
    t.Priority = 'High';
    t.Description = 'Description';
    t.activitydate = system.today();
    
    insert t;
    
    t2.status='Not Started';
    t2.Objective__c = 'Housekeeping';
    t2.Priority = 'High';
    t2.Description = 'Description';
    t2.activitydate = system.today()-15;
    
    insert t2;
    
    t.Description= 'Visit';
Update t;
Id batchInstanceID = Database.executeBatch(new DeleteOverdueTasks());
Delete t;
    }}