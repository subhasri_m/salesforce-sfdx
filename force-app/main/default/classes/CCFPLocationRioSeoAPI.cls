public class CCFPLocationRioSeoAPI{
    
    public class Location {
        public String fid;
        public String location_name;
        public String address_1;
        public String address_2;
        public String city;
        public String region;
        public String post_code;
        public String country;
        public String local_phone;
        public String lat;
        public String lng;
        public String distance;
        public Boolean isSuccess {get; set;} 
        public String message {get; private set;}
        public String servedFromDC {get; private set;} // to get served from DC
        public hoursdata primaryHours;
        public Specialties specialties;
        public String hoursOfopenMsg;
        public Boolean isClosedToday = False;
        public String county;
        public String state;
        // Below Location variable are set from SF UI
        public Boolean pickUpON;
        public Boolean localDeliveryON;
        public Boolean shippingON;    
    }
    public class Specialties {
        public Integer Parts;
    }
    public class hoursdata {
        public String label;
        public String name;
        public days days;
    }
    
    public class operationHour {
        public String open; //07:30
        public String close;    //17:00
    }
    
    public class days {
        public String SundayStr;
        public String MondayStr;
        public String TuesdayStr;
        public String WednesdayStr;
        public String ThursdayStr;
        public String FridayStr;
        public String SaturdayStr;
        public List<operationHour> Monday;
        public List<operationHour> Tuesday;
        public List<operationHour> Wednesday;
        public List<operationHour> Thursday;
        public List<operationHour> Friday;
        public List<operationHour> Saturday;
        public List<operationHour> Sunday;
    }
    public class LocationWrapper{
        public List<Location> loclist {get; set;}
        public Boolean isSuccess {get; set;} 
        public String message {get; private set;}
        public LocationWrapper(){
            loclist  = new List<Location>();
            isSuccess = true;
            message = null;
        }
    }
    
    public static LocationWrapper getBranchLocations(String ipAddress, String zipCode, Boolean prfrdSameAsShpfrm){
        String baseURL =  System.Label.RioSEOURL;//'https://api.branches.fleetpride.com/';
        String restAPIURL;
        if(!String.isEmpty(zipCode)){
            restAPIURL = baseURL + 'api/locations?addr=' + zipCode + '&specialties_11=1&rnum=10'; // Final RIO SEO EndPoint  bring only Fleetpride branches  
        }else if(!String.isEmpty(ipAddress)){
            restAPIURL = baseURL + 'api/locations?ip=' + ipAddress + '&specialties_11=1&rnum=10';   
        }
        
        HttpRequest httpRequest = new HttpRequest();  
        httpRequest.setMethod('GET');   
        httpRequest.setEndpoint(restAPIURL); 
        LocationWrapper response = new LocationWrapper();
        try {  
            Http http = new Http();   
            HttpResponse RawHttpResponse = http.send(httpRequest); 
            System.debug('RawHttpResponse==>'+RawHttpResponse);
            string httpResponseBody =(RawHttpResponse.getBody()).replace('hours:primary','primaryHours');
            
            httpResponseBody = httpResponseBody.replace('"Sunday":"closed"','"SundayStr":"closed"');
            httpResponseBody = httpResponseBody.replace('"Monday":"closed"','"MondayStr":"closed"');
            httpResponseBody = httpResponseBody.replace('"Tuesday":"closed"','"TuesdayStr":"closed"');
            httpResponseBody = httpResponseBody.replace('"Wednesday":"closed"','"WednesdayStr":"closed"');
            httpResponseBody = httpResponseBody.replace('"Thursday":"closed"','"ThursdayStr":"closed"');
            httpResponseBody = httpResponseBody.replace('"Friday":"closed"','"FridayStr":"closed"');
            httpResponseBody = httpResponseBody.replace('"Saturday":"closed"','"SaturdayStr":"closed"');
            
            if (RawHttpResponse.getStatusCode() == 200 ) {                
                List<Location> locList  = (List<Location>) System.JSON.deserialize(httpResponseBody, List<Location>.class); 
                List<Location__c> lstLoc = new  List<Location__c>();
                Map<String,Location__c> mapLocFidVsLocation = new Map<String,Location__c>();
                Set<String> setLocFid = new Set<String>();
                for(Location loc : locList){
                    setLocFid.add(loc.fid);
                }
                System.debug('setLocFid==>'+setLocFid);
                
                lstLoc = CCFPLocationDAO.getServedFromDC(setLocFid);
                for(Location__c loc: lstLoc){
                    mapLocFidVsLocation.put(loc.Location__c, loc);
                }
                System.debug('mapLocFidVsLocation==>'+ mapLocFidVsLocation);
                //==update source location
                System.debug('before locList==>'+locList);
                for(Integer i=0; i < locList.size(); i++){
                    if(mapLocFidVsLocation.get(locList[i].fid) != null){
                        locList[i].pickUpON = mapLocFidVsLocation.get(locList[i].fid).PickUp_ON__c;
                        if (prfrdSameAsShpfrm){ // setting localDelivery only when preferedloc and shipfromloc are same
                           locList[i].localDeliveryON = mapLocFidVsLocation.get(locList[i].fid).LocalDelivery_ON__c; 
                        }else{
                            locList[i].localDeliveryON = false;
                        }
                        locList[i].shippingON = mapLocFidVsLocation.get(locList[i].fid).Shipping_ON__c;
                        locList[i].servedFromDC = mapLocFidVsLocation.get(locList[i].fid).SourceLoc__c;
                        locList[i].county = mapLocFidVsLocation.get(locList[i].fid).County__c;
                        locList[i].state = mapLocFidVsLocation.get(locList[i].fid).State__c;
                    }
                    else{
                        locList.remove(i);
                        continue;
                    }
                }
                
                System.debug('After locList==>'+locList);
                
                response.loclist = locList;
                
            } else {
                response.isSuccess = false;
                response.message = 'Exception occured --> Response code: ' + String.ValueOf(RawHttpResponse.getStatusCode()) +', Response body: '+ RawHttpResponse.getBody();
            }
        } catch(Exception e) {  
            response.isSuccess= false;
            response.message = e.getMessage();
        }
        return response;
    }
    
    public static Location getNearestFleetPrideBranch(String ipAddress, String zipCode,String fid, Boolean prfrdSameAsShpfrm ){
        LocationWrapper locWrapper;
        Location nearestloc = new Location();        
        locWrapper =  getBranchLocations(ipAddress, zipCode, prfrdSameAsShpfrm); // 
        if(locWrapper.isSuccess) { 
            //--changes made on 17/09/2020 : put check of fid in returned nearest brances, only for loggedin user
            if(UserInfo.getUserType() != 'Guest'){
                Boolean isFidMatched = False;
                for(Integer i=0; i < locWrapper.locList.size(); i++){
                    if(locWrapper.locList[i].fid == fid){
                        nearestloc = locWrapper.locList[i];
                        isFidMatched = True;
                        break;
                    }
                }
                if(!isFidMatched){
                    nearestloc = locWrapper.locList[0];
                }
            }
            else{
                nearestloc = locWrapper.locList[0];
            }
            return nearestloc;
        }
        else {
            nearestloc.isSuccess = false;
            nearestloc.message = locWrapper.message;
            return nearestloc;
        }         
        
    }    
    
}