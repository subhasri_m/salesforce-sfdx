public without sharing  class CCPDCRequiredOrderItemDAO {
    public static List<CC_FP_Required_Order_Item__c> getRequiredOrderItems(Set<Id> theIds) {
        List<CC_FP_Required_Order_Item__c> items = [
            SELECT 
                Id,
            	Name,
                CC_Parent_Order_Item__c,
            	CC_Parent_Order_Item__r.Name,
                CC_Parent_Order_Item__r.ccrz__Product__r.ccrz__SKU__c, 
                Quantity__c,
                Price__c,
                SubAmount__c,
                CC_Product__c, 
                CC_Product__r.Name, 
                CC_Product__r.ccrz__SKU__c,
                CC_Product__r.Part_Number__c,
                CC_Product__r.Pool_Number__c
            FROM 
                CC_FP_Required_Order_Item__c
            WHERE
                CC_Parent_Order_Item__c IN :theIds
        ];

        return items;
    }
}