global without sharing class CCFPServiceOrder extends ccrz.ccServiceOrder {
    global virtual override Map<String,Object> getFieldsMap(Map<String, Object> inputData) {
        inputData = super.getFieldsMap(inputData);
        String objectFields = (String)inputData.get(ccrz.ccService.OBJECTFIELDS);
        objectFields += ',Order_Item_Count__c' ;
        return new Map<String, Object> {ccrz.ccService.OBJECTFIELDS => objectFields};
    }
    global virtual override Map<String,Object> getSubQueryMap (Map<String,Object> inputData) {
        Map<String,Object> outputData = super.getSubQueryMap(inputData);
        outputData.put('E_OrderItemGroups__r', '(Select Id, Name,  ccrz__Order__c, ccrz__GroupName__c, ccrz__Note__c, ccrz__OrderItemGroupId__c, ccrz__RequestDate__c, ccrz__ShipAmount__c, ccrz__ShipMethod__c, ccrz__ShipTo__c, CC_FP_DC_ID__c, CC_FP_Is_Backorder__c, CC_FP_Subtotal_Amount__c, CC_FP_Tax_Amount__c, CC_FP_Total_Amount__c, Shipped_Date__c, Status__c, ccrz_OrderItemGroupIdCustom__c, Invoice_Number__c  from E_OrderItemGroups__r)');
        return outputData; 
    }
    global virtual override Map<String,Object> getOrderByMap (Map<String,Object> inputData) {
        Map<String,Object> outputData = super.getOrderByMap(inputData);
        String defaultOrderByStr = '';
        Map<String,String> pageParams = ccrz.cc_CallContext.currPageParameters;
        ccrz.cclog.log('getting Curr Page Params:'+ pageParams);
        if(outputData.containsKey('byOrderNumber')) {
        	defaultOrderByStr = (String) outputData.get('byOrderNumber');
        }
        if(String.isNotEmpty(defaultOrderByStr)) {
            if(pageParams.containsKey('sortBy'))
            outputData.put('byOrderNumber', pageParams.get('sortBy')); 
        } 
        return outputData; 
    }
}