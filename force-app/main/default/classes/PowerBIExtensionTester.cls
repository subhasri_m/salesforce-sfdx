public class PowerBIExtensionTester extends PowerBIControllerTester {

    private final Account objAccount;

    /**
    * Generic constructor
    */
    public PowerBIExtensionTester (ApexPages.StandardController stdController) {       
        this.objAccount = (Account)stdController.getRecord();
                
    }
}