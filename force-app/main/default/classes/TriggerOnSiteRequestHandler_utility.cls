public class TriggerOnSiteRequestHandler_utility {

    public void InsertCaseOnVisitorQuery(List<SiteRequestHandler__c> Triggernew) 
    {
        List<case> CaseListInsert = new List<case>(); 
        for (SiteRequestHandler__c obj: Triggernew )
        {
            case newcase = new case();
            newcase.ContactId = obj.CustomerName__c;
            newcase.AccountId = obj.AccountID__C ;
            newcase.Description= obj.Complaints_Query__c;
            newcase.OwnerId = obj.AccountOwner__c;
            if(obj.AccountName__C!=null){
            newcase.Subject = 'Offline Request From '+ obj.AccountName__C;
            }
            else
            {
            newcase.Subject = 'Offline Request from Guest';
            }
            CaseListInsert.add(newcase);
        }
        
        insert CaseListInsert;
    }
    
}