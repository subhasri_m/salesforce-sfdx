public with sharing class CCFPContactAddressDAO {
    public static ccrz__E_ContactAddr__c getAddress(Id sfid) {
        ccrz__E_ContactAddr__c address = null;
        List<ccrz__E_ContactAddr__c> addresses = [
            SELECT 
                Id, 
                Name,
                ccrz__AddressFirstline__c,
                ccrz__AddressSecondline__c,
                ccrz__AddressThirdline__c,
                ccrz__AddrReadOnly__c,
                ccrz__City__c,
                ccrz__CompanyName__c,
                ccrz__ContactAddrId__c,
                ccrz__Country__c,
                ccrz__CountryISOCode__c,
                CC_FP_County__c,
                ccrz__DaytimePhone__c,
                ccrz__Email__c,
                ccrz__FirstName__c,
                CC_FP_Has_Lift_Gate__c,
                ccrz__HomePhone__c,
                ccrz__LastName__c,
                CC_FP_Location__c,
                ccrz__MailStop__c,
                ccrz__MiddleName__c,
                ccrz__Partner_Id__c,
                ccrz__PostalCode__c,
                ccrz__ShippingComments__c,
                ccrz__State__c,
                ccrz__StateISOCode__c,
                CC_FP_Type__c
            FROM 
                ccrz__E_ContactAddr__c
            WHERE
                Id = :sfid
        ];
        if (addresses != null && !addresses.isEmpty()) {
            address = addresses[0];
        }
        return address;
    }
}