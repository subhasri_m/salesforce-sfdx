@isTest
public class CCFPOrderPickUpDeliveryControllerTest {  
	static CCPDCTestUtil util = new CCPDCTestUtil();
    @isTest
    static void testPickUpDeliveryCtrl() {
        CCFPOrderPickUpDeliveryController ctrl = new CCFPOrderPickUpDeliveryController();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        ccrz__E_Order__c order;
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();

            ccrz__E_Cart__c cart = util.createCartWithSplits(m);
            cart.CC_FP_Is_Will_Call__c = true;
            update cart;
            order = util.createOrderWithSplits(m, cart);
        }
        ctrl.orderID = order.Id;
        ctrl.getStorefrontURL();
        ctrl.getmockOrder();
        ctrl.getisShortShip();
        ctrl.getorderItems();
        ctrl.getorderItemGroups();
    }
}