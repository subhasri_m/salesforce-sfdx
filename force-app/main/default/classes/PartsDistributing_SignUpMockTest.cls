@isTest
global class PartsDistributing_SignUpMockTest implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest httpReq){
        String responseString = '{'+
                '"title": "Invalid Resource",'+
                '"status": 400,'+
                '"detail": "Your merge fields were invalid."'+
            '}';
        HttpResponse httpRes = new HttpResponse();
        httpres.setStatusCode(400);
        httpRes.setBody(responseString);
        return httpRes;
    }
}