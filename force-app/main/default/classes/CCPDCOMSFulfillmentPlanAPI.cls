public with sharing class CCPDCOMSFulfillmentPlanAPI extends CCPDCOMSAbstractAPI {
	public FulfillmentPlanResponse fulfillmentResponse {get;set;}
	public FulfillmentPlanRequest fulfillmentRequest {get;set;}
    public String fulfillmentResponseBody {get; set;}
    private Boolean isFulfillmentPlanRequest = true;

    public CancelReservationRequest cancelRequest {get;set;}
    public CancelReservationResponse cancelResponse {get;set;}
    public String cancelResponseBody {get; set;}


	public CCAviHttpUtil httpUtil {get; private set;}
	CC_PDC_OMS_Settings__c omsSettings;

	public CCPDCOMSFulfillmentPlanAPI(CC_PDC_OMS_Settings__c omsSettings, FulfillmentPlanRequest fulFilRequest) {
		this.omsSettings = omsSettings;
		this.fulfillmentRequest = fulFilRequest;
        isFulfillmentPlanRequest = true;
	}

    public CCPDCOMSFulfillmentPlanAPI(CC_PDC_OMS_Settings__c omsSettings, CancelReservationRequest cancelRequest){
        this.omsSettings = omsSettings;
        this.cancelRequest = cancelRequest;
        isFulfillmentPlanRequest = false;
    }

	// An instance of HttpUtil with the OAuth token will be provided. No DML should be performed
	// in this method.
	public override void executeRequest(CCAviHttpUtil util){
		this.httpUtil = util;

		// execute the FulfillmentPlan request
        if(isFulfillmentPlanRequest){
		  fulfillmentResponse = getOptimizedFulfillmentPlan();
        }
        else {
            cancelResponse = executeCancelRequest();
        }
		System.debug('Fulfillment response: ' + fulfillmentResponse);
		//System.debug('Fulfillment response: ' + JSON.serializePretty(fulfillmentResponse));

	}

	// Perform any DML operations. This method is called after all CCPDCOMSIFC intefaces
	// executeRequest methods have been called.
	public override void commitData(){
		// no data needs to be stored for this request
	}

    private FulfillmentPlanResponse getOptimizedFulfillmentPlan() {
        //FulfillmentPlanResponse response = new FulfillmentPlanResponse();

        httpUtil.method = CCAviHttpUtil.REQUEST_TYPE.POST;
        httpUtil.requestContentType = CCAviHttpUtil.CONTENT_TYPE.JSON;
        // httpRequest.endpoint = listSettings[0].API_URL__c + 'getOptimizedFulfillmentPlan';
        httpUtil.endpoint = omsSettings.Fulfillment_Plan_URL__c;
        String body = JSON.serialize(fulfillmentRequest);
        body = body.remove('"reservationId":null,');
        httpUtil.setBody(body);
        System.debug('FulfillPlan endpoint: ' + httpUtil.endpoint);
        System.debug('FulfillPlan body: ' + body);
        // Set timeout
        httpUtil.timeout = Integer.valueOf(omsSettings.TimeOut_InMillsec__c);
        HttpResponse httpRes = httpUtil.submitRequest();
        
        if (httpUtil.requestErrors) {
            FulfillmentPlanResponse res = new FulfillmentPlanResponse();
            res.message = httpUtil.requestException.getMessage();
            res.success = false;
            return res;
        }
        else {
            if (httpRes.getStatusCode() == 200) {
                System.debug('FulfillPlan body: ' + httpRes.getBody());
                fulfillmentResponseBody = httpRes.getBody();
                fulfillmentResponse = (FulfillmentPlanResponse)JSON.deserialize(httpRes.getBody(),  FulfillmentPlanResponse.class);
                fulfillmentResponse.success = true;
            }
            else {
                FulfillmentPlanResponse res = new FulfillmentPlanResponse();
                res.message = httpRes.getStatusCode() + httpRes.getBody();
                res.success = false;
                return res;
            }
        }
        
        
        return fulfillmentResponse;
    }   


    private CancelReservationResponse executeCancelRequest() {
        //FulfillmentPlanResponse response = new FulfillmentPlanResponse();

        httpUtil.method = CCAviHttpUtil.REQUEST_TYPE.POST;
        httpUtil.requestContentType = CCAviHttpUtil.CONTENT_TYPE.JSON;
        // httpRequest.endpoint = listSettings[0].API_URL__c + 'getOptimizedFulfillmentPlan';
        httpUtil.endpoint = omsSettings.Cancel_Fulfillment_Request_URL__c;
        String body = JSON.serialize(cancelRequest);
        body = body.remove('"reservationId":null,');
        httpUtil.setBody(body);
        System.debug('FulfillPlan endpoint: ' + httpUtil.endpoint);
        System.debug('FulfillPlan body: ' + body);
        // Set timeout
        httpUtil.timeout = Integer.valueOf(omsSettings.TimeOut_InMillsec__c);
        HttpResponse httpRes = httpUtil.submitRequest();
        
        if (httpUtil.requestErrors) {
            CancelReservationResponse res = new CancelReservationResponse();
            res.message = httpUtil.requestException.getMessage();
            res.success = false;
            return res;
        }
        else {
            if (httpRes.getStatusCode() == 200) {
                System.debug('Cancel reservation body: ' + httpRes.getBody());
                cancelResponseBody = httpRes.getBody();
            	cancelResponse = (CancelReservationResponse)JSON.deserialize(httpRes.getBody(),  CancelReservationResponse.class);
                cancelResponse.success = true;
            }
            else {
                CancelReservationResponse res = new CancelReservationResponse();
                res.message = httpRes.getStatusCode() + httpRes.getBody();
                res.success = false;
                return res;
            }
        }
        
        
        return cancelResponse;
    }	

    public class OrderLine {
        public String partNo {get; set;}
        public Integer orderLineNo {get; set;}
        public Integer quantity {get; set;}

        public transient Boolean expeditedFlag = false;
        //private String expedited {get { return expeditedFlag?'Y':'N'; }}
        private Boolean expedited {get { return expeditedFlag?true:false; }}

    }
    public class BackOrderLine {
        public String partNo {get; set;}
        public Integer quantity {get; set;}
        public Integer orderLineNo {get; set;}
    }
    public class ShipToAddress {
        public String zipcode {get; set;}

        public transient Boolean residentialFlag = false;
        //private String residential {get { return residentialFlag?'Y':'N'; }}
        private Boolean residential {get { return residentialFlag?true:false; }}

        public transient Boolean liftgateFlag  = false;
        //private String liftgate {get { return liftgateFlag?'Y':'N'; }}
        private Boolean liftgate {get { return liftgateFlag?true:false; }}

        public String homeDC {get; set;}
    }

    public class CancelReservationRequest {
        public String orderId {get; set;}
        public String reservationId {get; set;}
    }

    public class CancelReservationResponse {
        public Boolean success {get;set;}
        public String message {get;set;}
        public String result {get; set;}
        public Boolean recheckOccurred {get; set;}
        public List<Shipment> shipments {get; set;}
        public List<BackOrderLine> backOrderLines {get;set;}
    }
    
    public class FulfillmentPlanRequest {
        public String orderId {get; set;}

        public transient Boolean reserveInventoryFlag  = false;
        //private String reserveInventory {get { return reserveInventoryFlag?'Y':'N'; }}
        private Boolean reserveInventory {get { return reserveInventoryFlag?true:false; }}

        public String reservationId {get; set;}

        public transient Boolean recheckAvailabilityFlag  = false;
        //private String recheckAvailability {get { return recheckAvailabilityFlag?'Y':'N'; }}
        private Boolean recheckAvailability {get { return recheckAvailabilityFlag?true:false; }}

        public transient Boolean freeFreightEligibilityFlag  = false;
        //private String freeFreightEligibility {get { return freeFreightEligibilityFlag?'Y':'N'; }}
        private Boolean freeFreightEligibility {get { return freeFreightEligibilityFlag?true:false; }}

        public transient Boolean willCallFlag  = false;
        //private String willCall {get { return willCallFlag?'Y':'N'; }}
        private Boolean willCall {get { return willCallFlag?true:false; }}

        public transient Boolean shipUnavailableWillCallFlag  = false;
        //private String shipUnavailableWillCall {get { return shipUnavailableWillCallFlag?'Y':'N'; }}
        private Boolean shipUnavailableWillCall {get { return shipUnavailableWillCallFlag?true:false; }}

        public List<OrderLine> orderLines = new List<OrderLine>();
        public List<BackOrderLine> backOrderLines  = new List<BackOrderLine>();

        public String customerNo {get; set;}

        public String channel {get;set;}
        public Decimal orderTotal {get;set;}

        public ShipToAddress shipToAddress  = new ShipToAddress();
    }
    
    // Response Objects
    public class Shipment {
        public String dc;
        public String carrier;
        public String mode;
		public String service;
		public Boolean expedited;
		public Double costToFP;
		public Double costToCustomer;
		public Integer transitTime;
		public List<Orderline> lines;
	}

    public class FulfillmentPlanResponse {
        public String message {get; set;}
        public Boolean success {get; set;}
        public String failureReason {get;set;}
        public String result {get;set;}

		public String orderId;
		public String reservationId;
		public Boolean recheckOccurred;
		public String homeDC;
		public List<CCPDCOMSFulfillmentPlanAPI.Shipment> shipments;
		public List<BackOrderLine> backOrderLines;
    }
}