public class PowerBIControllerExtension extends PowerBISilentOAuthController {

    private final Account objAccount;

    /**
    * Generic constructor
    */
    public PowerBIControllerExtension (ApexPages.StandardController stdController) {       
        this.objAccount = (Account)stdController.getRecord();
                
    }
}