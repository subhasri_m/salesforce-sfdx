@isTest
public class CCPDCMyAccountMyOrdersControllerTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id,Create_Accounts__c,Name FROM User WHERE Create_Accounts__c = true AND 
                                Profile.Name='System Administrator' AND isActive=true Limit 1];
        System.debug('#### thisUser '+thisUser);
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void reorderTest() {
        util.initCallContext();
        ccrz__E_Order__c order = util.getOrder();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyOrdersController.reorder(ctx, order.Id);
        Test.stopTest();

        System.assert(result != null);
    //    System.assert(result.success);
        System.assert(result.data != null);
    }
}