public with sharing class CCVintelligenceAPI {

  public class VintelligenceWSResponse {
      public String message {get; set;}
      public Boolean success {get; set;}
      public VinInfo vinResponse {get; set;}

      public VintelligenceWSResponse() {
          success = false;
          vinResponse = new VinInfo();
      }
  }

  public class VinInfo {
      public String vin {get; set;} // <ACES_YEAR_ID>
      public String year {get; set;} // <ACES_YEAR_ID>
      public String make {get; set;} // <ACES_MAKE_NAME>
      public String manufacturer {get; set;} // <ENG_MFG_DESC>
      public String model {get; set;} // <ACES_MODEL_NAME>
      public String engineLiters {get; set;} // <ACES_LITERS>
      public String engineModel {get; set;} // <ACES_ENGINE_DESIGNATION_NAME>
      public String engine {get; set;} // <ACES_MAKE_NAME> <ACES_ENGINE_DESIGNATION_NAME> <ACES_LITERS>L.
      public String errorBytes {get; set;}
      public String correctedVin {get; set;}
      public String returnCode {get; set;}
  }

  public class VintelligenceWSRequest {
      public String vin {get; set;}
  }

  public static CCVintelligenceAPI.VintelligenceWSResponse getVinDetailsWS(VintelligenceWSRequest vinRequest) {
      System.debug('Start VintelligenceAPI getVinDetailsWS: '  + System.now());
      VintelligenceWSResponse response = new VintelligenceWSResponse();

      try {
          if (vinRequest == null) {
              response.success = false;
              response.message  = 'VIN is empty';
              return response;
          }

          // read CCVintelligenceSettings config data from settings
          CCVintelligenceSettings__c settings =  CCVintelligenceSettings__c.getOrgDefaults();
          if (settings == null) {
              response.success = false;
              response.message  = 'Configuration not found';
              return response;
          }


          Blob authBlob = Blob.valueOf(settings.Username__c + ':' + settings.Password__c);
          String encodedAuthString = EncodingUtil.base64Encode(authBlob);

          HttpRequest httpRequest = new HttpRequest();
          HttpResponse httpResponse = null;
          httpRequest.setMethod('POST');
          httpRequest.setHeader('Content-Type', 'text/xml;charset=UTF-8');
          httpRequest.setHeader('Authorization', 'Basic ' + encodedAuthString);
          httpRequest.setHeader('SOAPAction', '"decodeVin"');

          String body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.vindecoder.polk.com/">';
          body += '<soapenv:Header/>';
          body += '<soapenv:Body>';
          body += '<web:decodeVin>';

          //Manually form the xml
          body += '<VinRequest>';
          body += '<vin>' + vinRequest.vin + '</vin>';
          body += '</VinRequest>';
          body += '</web:decodeVin>';
          body += '</soapenv:Body>';
          body += '</soapenv:Envelope>';

          httpRequest.setBody(body);
          httpRequest.setEndpoint(settings.End_Point__c);
          httpRequest.setTimeout(120000);

          Http objHttp = new Http();
          httpResponse = objHttp.send(httpRequest);

          if (httpResponse.getStatusCode() == 200) {
              Dom.Document doc = httpResponse.getBodyDocument();
              Dom.XMLNode soapEnvelope = doc.getRootElement();
              Dom.XMLNode soapBody = soapEnvelope.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/');
              Dom.XMLNode decodeVinResponse = soapBody.getChildElement('decodeVinResponse', 'http://webservice.vindecoder.polk.com/');
              Dom.XMLNode vinResponse = decodeVinResponse.getChildElement('VinResponse', null);
              Dom.XMLNode[] vinChildren = vinResponse.getChildElements();
              VinInfo vinInfo = new VinInfo();

                vinInfo.vin = vinResponse.getChildElement('vin', null).getText();

              for (Dom.XMLNode node : vinChildren) {
                //   if (node.getName() == 'vin') {
                //       vinInfo.vin = node.getText();
                //   }
                   if (node.getName() == 'returnCode') {
                      vinInfo.returnCode = node.getText();
                  }
                  else if (node.getName() == 'correctedVin') {
                      vinInfo.correctedVin = node.getText();
                  }
                  else if (node.getName() == 'errorBytes') {
                      vinInfo.errorBytes = node.getText();
                  }
                  else if (node.getAttributeValue('name', null) == 'ACES_YEAR_ID') {
                      vinInfo.year = node.getText();
                  }
                  else if (node.getAttributeValue('name', null) == 'ACES_MAKE_NAME') {
                      vinInfo.make = node.getText();
                  }
                  else if (node.getAttributeValue('name', null) == 'ENG_MFG_DESC') {
                      vinInfo.manufacturer = node.getText();
                  }
                  else if (node.getAttributeValue('name', null) == 'ACES_MODEL_NAME') {
                      vinInfo.model = node.getText();
                  }
                  else if( node.getAttributeValue('name', null) == 'ACES_LITERS') {
                      vinInfo.engineLiters = node.getText();
                  }
                  else if( node.getAttributeValue('name', null) == 'ACES_ENGINE_DESIGNATION_NAME') {
                      vinInfo.engineModel = node.getText();
                  }
                }

              vinInfo.engine = vinInfo.manufacturer + ' ' + vinInfo.engineModel + ' ' + vinInfo.engineLiters + 'L.';

              response.vinResponse = vinInfo;
              response.success = true;
          }
          else if (httpResponse.getStatusCode() == 401) {
              response.message = '401: Unauthorized Access';
          }
          else {
              response.message = 'Unknown error. Status Code: '+ httpResponse.getStatusCode() + '; Response:' + httpResponse.getBody();
          }

      }
      catch (Exception e) {
          System.debug(System.LoggingLevel.ERROR, e);
          response.success = false;
          response.message = e.getMessage();// + e.getStackTraceString();
      }

      System.debug('End VintelligenceAPI getVinDetailsWS: '  + System.now());
      return response;
  }

}