@isTest
public class CCAviProductDaoTest {
  static CCPDCTestUtil util = new CCPDCTestUtil();
  @testSetup
  static void testSetup(){
    User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    System.runAs(thisUser){
        Map<String, Object> m = util.initData();
    }

  }

  static testmethod void getProductsTest(){
    List<ccrz__E_Product__c> items = null;
    ccrz__E_Product__c testproduct = util.createProduct('testProduct','abc123456');
    insert testproduct;
    Test.startTest();
    items = CCAviProductDao.getProducts(new List<ID> {testproduct.Id});
    Test.stopTest();

    System.assert(items != null);
    System.assertEquals(1, items.size());
  }
  static testmethod void getProductsWeightTest(){
    Map<String,Decimal> weights = new Map<String,Decimal>();
    ccrz__E_Product__c testproduct2 = util.createProduct('testProduct2','abc1234567');
    insert testproduct2;
    Test.startTest();
    weights = CCAviProductDao.getProductsWeight(new List<ID> {testproduct2.Id});
    Test.stopTest();
    
  }
}