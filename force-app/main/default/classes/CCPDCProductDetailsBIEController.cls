public with sharing class CCPDCProductDetailsBIEController {
    public String productType {get; set; }
    public String storeFront {get;set;}

    public CCPDCProductDetailsBIEController() {
        String sku = ApexPages.currentPage().getParameters().get('sku');
        String accountId = ccrz.cc_CallContext.effAccountId;
        storeFront = ccrz.cc_CallContext.storefront;
        if (sku != null) {
            ccrz__E_Product__c product = CCPDCProductDAO.getProductTypeBySku(sku);

            if (product != null) {
                this.productType = product.ccrz__ProductType__c;
            } else {
                this.productType = 'NotOrderableProduct';
            }
        }
    }

}