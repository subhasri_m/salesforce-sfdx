@isTest
public class BatchAccountWithoutAdminsTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();
    
    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    } 
    
    
    public static testMethod void updateUserTest(){
        
        CC_FP_Contract_Account_Permission_Matrix__c capm = util.createPermissions(true);
        insert capm;
        Account acc1 = util.getAccount();
        acc1.Iseries_Company_code__c='2';
		update acc1;        
        ccrz__E_AccountGroup__c userAccountGroup1 =util.createAccountGroup('accountGroup2', null);
        Account acc2 = util.createAccount(userAccountGroup1);
        acc2.Iseries_Company_code__c='2';
        insert acc2;
        contact contemp = util.getContact();
        contemp.email='rahul.bansal@puresoftware.com';
        update contemp;
        Test.startTest();
        database.executebatch(new BatchAccountWithoutAdmins());
        Test.stopTest();
       
    }
    
    
}