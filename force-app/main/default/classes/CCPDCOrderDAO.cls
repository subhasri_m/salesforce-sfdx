public with sharing class CCPDCOrderDAO {
    public static ccrz__E_Order__c getOrderOwner(String theId) {

        ccrz__E_Order__c order = null;

        List<ccrz__E_Order__c> orderList = [
            SELECT
                Id,
                Name,
                OwnerId,
                ccrz__Account__c,
                ccrz__Contact__c,
                ccrz__User__c,
                ccrz__ShipTo__c,
                ccrz__ShipTo__r.Id,
                ccrz__ShipTo__r.OwnerId,
                ccrz__BillTo__c,
                ccrz__BillTo__r.Id,
                ccrz__BillTo__r.OwnerId,
                ccrz__OriginatedCart__c,
                ccrz__OriginatedCart__r.OwnerId,
                ccrz__OriginatedCart__r.ccrz__Account__c,
                ccrz__OriginatedCart__r.ccrz__Contact__c,
                ccrz__OriginatedCart__r.ccrz__User__c,
                ccrz__OriginatedCart__r.ccrz__ShipTo__c,
                ccrz__OriginatedCart__r.ccrz__ShipTo__r.Id,
                ccrz__OriginatedCart__r.ccrz__ShipTo__r.OwnerId,
                ccrz__OriginatedCart__r.ccrz__BillTo__c,
                ccrz__OriginatedCart__r.ccrz__BillTo__r.Id,
                ccrz__OriginatedCart__r.ccrz__BillTo__r.OwnerId,
                (SELECT
                    Id, 
                    CC_FP_OMS_ReservID__c,
                    CC_FP_Price_Ovr_Date__c,
                    CC_FP_Price_Ovr_UsrID__c,
                    ccrz__Comments__c,
                    ccrz__OrderItemGroup__c,
                    CC_FP_PONumber__c,
                    ccrz__Price__c,
                    ccrz__Quantity__c,
                    ccrz__Product_Name__c,
                    ccrz__ItemTotal__c,
                    ccrz__Product__r.Id,
                    ccrz__Product__r.ccrz__Sku__c,
                    ccrz__Product__r.Part_Number__c,
                    ccrz__Product__r.Pool_Number__c
                FROM
                    ccrz__E_OrderItems__r
                )
            FROM
                ccrz__E_Order__c
            WHERE
                Id  = :theId
        ];

        if (!orderList.isEmpty()) {
            order = orderList[0];
        }

        return order;
    }

    /* Order */
    /* Account */
    /* Ship To */
    /* Order Items */
    /* Products within order items */
    /* Order Item Groups with Ship To information */
    public static ccrz__E_Order__c getOrderDetails(Id orderId){
        ccrz__E_Order__c theOrder = [
            SELECT
                Id,
                Name,
                ccrz__OrderDate__c,
                CC_FP_Location__r.Id,
                CC_FP_Location__r.Location__c,
                CreatedDate,
                CreatedById,
                CreatedBy.Username,
                ccrz__User__c,
                ccrz__User__r.FederationIdentifier,
                CreatedBy.FederationIdentifier,
                ccrz__Contact__c,
                ccrz__Contact__r.Name,
                ccrz__Note__c,
                ccrz__ShipMethod__c,
                ccrz__BuyerCompanyName__c,
                ccrz__PaymentMethod__c,
                ccrz__BillTo__c,
                ccrz__ShipTo__c,
                ccrz__Storefront__c,
                ccrz__SubtotalAmount__c,
                ccrz__TaxAmount__c,
                ccrz__TotalAmount__c,
                ccrz__OrderStatus__c,
                ccrz__OriginatedCart__r.Reservation_Id__c,
                ccrz__Account__c,
                ccrz__Account__r.Id,
                ccrz__Account__r.Iseries_Company_code__c,
                ccrz__Account__r.ISeries_Customer_Account__c,
                ccrz__Account__r.ISeries_Customer_Branch__c,
                ccrz__Account__r.AccountNumber,
                ccrz__Account__r.CC_PDC_Cust_Branch__c,
                ccrz__ShipTo__r.Id,
                ccrz__ShipTo__r.Name,
                ccrz__ShipTo__r.ccrz__Country__c,
                ccrz__ShipTo__r.CC_FP_County__c,
                ccrz__ShipTo__r.ccrz__AddressFirstline__c,
                ccrz__ShipTo__r.ccrz__AddressSecondline__c,
                ccrz__ShipTo__r.ccrz__City__c,
                ccrz__ShipTo__r.ccrz__PostalCode__c,
                ccrz__ShipTo__r.ccrz__State__c,
            	ccrz__BillTo__r.ccrz__Country__c,
                ccrz__BillTo__r.CC_FP_County__c,
                ccrz__BillTo__r.ccrz__AddressFirstline__c,
                ccrz__BillTo__r.ccrz__AddressSecondline__c,
                ccrz__BillTo__r.ccrz__City__c,
                ccrz__BillTo__r.ccrz__PostalCode__c,
                ccrz__BillTo__r.ccrz__State__c,
                CC_FP_Is_Ship_Remainder__c,
                CC_FP_Is_Will_Call__c,                
                ccrz__PONumber__c,
            	Order_Comments__c,
                ccrz__Contact__r.Phone,
                (SELECT
                    Id,
                    Name,
                    CC_FP_OMS_ReservID__c,
                    CC_FP_Price_Ovr_Date__c,
                    CC_FP_Price_Ovr_UsrID__c,
                    ccrz__Comments__c,
                    ccrz__OrderItemGroup__c,
                    CC_FP_PONumber__c,
                    ccrz__Price__c,
                    ccrz__Quantity__c,
                    ccrz__Product_Name__c,
                    ccrz__ItemTotal__c,
                    ccrz__Product__r.Id,
                    ccrz__Product__r.ccrz__Sku__c,
                    ccrz__Product__r.Part_Number__c,
                    ccrz__Product__r.Pool_Number__c,
                    ccrz__Product__r.FP_Brand_Name__c
                FROM
                    ccrz__E_OrderItems__r
                ),
                (SELECT
                    Id,
                    Name,
                    ccrz__GroupName__c,
                    ccrz__Note__c,
                    ccrz__OrderItemGroupId__c,
                    ccrz__RequestDate__c,
                    ccrz__ShipAmount__c,
                    ccrz__ShipMethod__c,
                    CC_FP_Is_Backorder__c,
                    CC_FP_DC_ID__c,
                    Status__c,
                    Invoice_Number__c,
                    ccrz__ShipTo__r.Id,
                    ccrz__ShipTo__r.Name,
                    ccrz__ShipTo__r.ccrz__Country__c,
                    ccrz__ShipTo__r.CC_FP_County__c,
                    ccrz__ShipTo__r.ccrz__AddressFirstline__c,
                    ccrz__ShipTo__r.ccrz__AddressSecondline__c,
                    ccrz__ShipTo__r.ccrz__City__c,
                    ccrz__ShipTo__r.ccrz__PostalCode__c,
                    ccrz__ShipTo__r.ccrz__State__c,
                    CC_FP_Total_Amount__c,
                    CC_FP_Tax_Amount__c,
                    CC_FP_Subtotal_Amount__c
                FROM
                    ccrz__E_OrderItemGroups__r
                )
            FROM
                ccrz__E_Order__c
            WHERE
                Id = :orderId
        ];
        return theOrder;        
    }

    public static ccrz__E_Order__c getOrderCustom(String theId) {

        ccrz__E_Order__c order = null;

        List<ccrz__E_Order__c> orderList = [
            SELECT
                Id,
                Name,
                ccrz__EncryptedId__c,
                ccrz__OriginatedCart__c,
                ccrz__PONumber__c,
                ccrz__OrderStatus__c,
                CC_FP_Free_Shipping_Type__c,
                CC_FP_Is_Free_Shipping__c,
                CC_FP_Is_Ship_Remainder__c,
                CC_FP_Is_Will_Call__c,
                CC_FP_Location__c,
            	ccrz__Storefront__c,
                CC_FP_Reservation_Id__c,
            	Order_Comments__c,
                CC_FP_Tax_Rate__c,
            	ccrz__BillTo__r.ccrz__Country__c,
                ccrz__BillTo__r.CC_FP_County__c,
                ccrz__BillTo__r.ccrz__AddressFirstline__c,
                ccrz__BillTo__r.ccrz__AddressSecondline__c,
                ccrz__BillTo__r.ccrz__City__c,
                ccrz__BillTo__r.ccrz__PostalCode__c,
                ccrz__BillTo__r.ccrz__State__c,
                (SELECT 
                    Id,
                    Name,
                    ccrz__Quantity__c,
                    ccrz__Product__c, 
                    CC_FP_PONumber__c,
                    ccrz__OrderItemGroup__c,
                    ccrz__OrderItemGroup__r.ccrz__GroupName__c
                FROM ccrz__E_OrderItems__r),
                (SELECT 
                    Id,
                    Name,
                    ccrz__GroupName__c,
                    CC_FP_Is_Backorder__c,
                    CC_FP_DC_ID__c,
                    CC_FP_Subtotal_Amount__c,
                    CC_FP_Tax_Amount__c,
                    CC_FP_Total_Amount__c
                FROM ccrz__E_OrderItemGroups__r)
            FROM
                ccrz__E_Order__c
            WHERE
                Id = :theId
        ];
        
        if (!orderList.isEmpty()) {
            order = orderList[0];
        }
        return order;
    }
}