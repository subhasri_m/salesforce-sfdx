/**
 * CCPDCInventoryHelper
 *
 * Description:
 * This helper queries for available inventory and currently supports the Product Detail
 * Page and the Cart page. This helper may be updated in the future to integrate with
 * a real-time inventory management system, or to decrement inventory after an order is
 * placed.
 *
 * Inventory is queried from the CC Product Inventory Item object (ccrz__E_ProductInventoryItem__c),
 * using the Product Item.Sku field from the product (ccrz__ProductItem__r.ccrz__Sku__c). All
 * inventory locations are retrieved, but the specific account's location code is determined by the 
 * Account.Ship_from_Location__c field TODO - verify this is the right field
 *
 *
 */
public with sharing class CCPDCInventoryHelper {

    /**
     * Gets the JSON representation for the product inventory for the specified 
     * SKUS and encrypted Account Id
     *
     * @return A JSON representation of the Map of sku => CCPDCInventoryHelper.Inventory objects. The map may be empty
     * but wll not be null.
     *
     */
    public static String getInventoryJSON(List<String> skus, Id accountId){
        String inventoryJson;

        Map<String, Inventory> invMap = getInventory(skus, accountId);
        inventoryJson = JSON.serialize(invMap);

        return inventoryJson;
    }

    public static String getInventoryJSONByCart(List<String> skus, Id accountId){
        String inventoryJson;

        Map<String, Inventory> invMap = getInventoryByCart(skus, accountId);
        inventoryJson = JSON.serialize(invMap);

        return inventoryJson;
    }    
    public static Map<String, Inventory> getInventoryByCart(List<String> skus, Id accountId){

        Map<String, Inventory> invMap = new Map<String, Inventory>();
        // Query the product to get the Ship from Location
        String primaryLocation = '';
        try {
            Account a = [
                SELECT
                    Ship_from_Location__c
                    , Distribution_Center__c
                FROM
                    Account
                WHERE
                    Id = :accountId
                LIMIT
                    1
            ];
            // TODO - verify Distribution_Center is the right field
            primaryLocation = a.Ship_from_Location__c;
        }
        catch(Exception e){
            // there will be no primary location for inventory
        }
        //ccrz.ccLog.log('CCPDCInventoryHelper primaryLocation'+primaryLocation); 
        String encCartId = ccrz.cc_CallContext.currCartId;
        if (encCartId != null) {
            ccrz__E_Cart__c cart = [SELECT Id, CC_FP_Location__c, ccrz__ShipTo__r.CC_FP_Location__r.Location__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: encCartId];
            if(cart != null && cart.ccrz__ShipTo__r.CC_FP_Location__r.Location__c != null) {            
                primaryLocation = cart.ccrz__ShipTo__r.CC_FP_Location__r.Location__c;            
            }        

            // Query the Product Inventory Location records
            List<ccrz__E_ProductInventoryItem__c> piiList = CCPDCProductDAO.getProductInventoryForSkus(skus);

            // Create Inventory objects
            // sku => Inventory
            Decimal qty = 0;

            Location__c loc = CCFPLocationDAO.getLocationForCode(primaryLocation);
            for(ccrz__E_ProductInventoryItem__c pii :piiList){
                // TODO - not sure which location field should be used
                // Inventory inv = createInventory(primaryLocation, pii.ccrz__ProductItem__c, pii.ccrz__ProductItem__r.ccrz__Sku__c, pii.ccrz__QtyAvailable__c, pii.Location__r.Location__c);
                // TODO : resolve null values                        
                if(pii != null) {
                    if(pii.ccrz__QtyAvailable__c != null) {
                        qty = pii.ccrz__QtyAvailable__c;
                    }
                    Inventory inv = createInventory(primaryLocation, loc, pii.ccrz__ProductItem__c, pii.ccrz__ProductItem__r.ccrz__Sku__c, qty, pii.ccrz__InventoryLocationCode__c, pii.Location__r.City__c, pii.Location__r.State__c);
                    // Inventory inv = createInventory(locId, pii.ccrz__ProductItem__c, pii.ccrz__ProductItem__r.ccrz__Sku__c, pii.ccrz__QtyAvailable__c, pii.ccrz__InventoryLocationCode__c);
                    // Add Inventory objects
                    invMap.put(pii.ccrz__ProductItem__r.ccrz__Sku__c, inv);
                }
            }
        }
        //ccrz.ccLog.log('CCPDCInventoryHelper primaryLocation 2'+primaryLocation); 

        return invMap;
    }    
	
    
     public static Map<String, Inventory> getFPInventoryByCart(List<String> skus, Id accountId, string branchLoc , string dcLoc){

        Map<String, Inventory> invMap = new Map<String, Inventory>();
		string primaryLocation = branchLoc;
        //String encCartId = ccrz.cc_CallContext.currCartId;
        //if (encCartId != null) {
            //ccrz__E_Cart__c cart = [SELECT Id, CC_FP_Location__c, CC_FP_Location__r.Location__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: encCartId];
            //if(cart != null && cart.CC_FP_Location__c != null) {            
               // primaryLocation = cart.CC_FP_Location__c;            
            //}        
			List<string> locations = new List<string>();
            locations.add(primaryLocation);         	 
         	if(String.isNotBlank(dcLoc)) {
               locations.add(dcLoc);
         	}
            
            // Query the Product Inventory Location records
            List<ccrz__E_ProductInventoryItem__c> piiList = CCPDCProductDAO.getProductInventoryForSkusAndLocation(skus, locations); 
            Decimal qty = 0;

            Location__c loc = CCFPLocationDAO.getLocationForCode(primaryLocation);
             for (string sku : skus ) {
               		Inventory inv = createInventory(primaryLocation, loc, '',sku, qty, loc.Location__c, loc.City__c, loc.State__c);
                    invMap.put(sku, inv);
           	}
            
            for(ccrz__E_ProductInventoryItem__c pii :piiList){                      
                if(pii != null) {
                    if(pii.ccrz__QtyAvailable__c != null) {
                        qty = pii.ccrz__QtyAvailable__c;
                    }
                    Inventory inv = createInventory(primaryLocation, loc, pii.ccrz__ProductItem__c, pii.ccrz__ProductItem__r.ccrz__Sku__c, qty, pii.ccrz__InventoryLocationCode__c, pii.Location__r.City__c, pii.Location__r.State__c);
                    invMap.put(pii.ccrz__ProductItem__r.ccrz__Sku__c, inv);
                } 
            }
           
               
                   
       // }
         return invMap;
    }    

    
    /**
     * Gets the product inventory for the specified SKUS and account Id
     *
     * @return A Map of sku => CCPDCInventoryHelper.Inventory objects. The map may be empty
     * but wll not be null. 'primaryLocation' will be populated with the Inventory record that
     * matches the account's Ship from Location, if available.
     *
     */
    public static Map<String, Inventory> getInventory(List<String> skus, Id accountId){
      /*  if(Test.isRunningTest()){
            Map<String,Inventory> invmap = new Map<String,Inventory>();
            Inventory inv = new Inventory('GAGFKSJA',skus[0],5);
            invmap.put(skus[0],inv );
            return invmap;
        }*/
        // Query the product to get the Ship from Location
        String primaryLocation = '';
        try {
            Account a = [
                SELECT
                    Ship_from_Location__c
                    , Distribution_Center__c
                FROM
                    Account
                WHERE
                    Id = :accountId
                LIMIT
                    1
            ];
            // TODO - verify Distribution_Center is the right field
            primaryLocation = a.Ship_from_Location__c;
        }
        catch(Exception e){
            // there will be no primary location for inventory
        }
        String encCartId = ccrz.cc_CallContext.currCartId;

        // Query the Product Inventory Location records
        List<ccrz__E_ProductInventoryItem__c> piiList = CCPDCProductDAO.getProductInventoryForSkus(skus);

        // Create Inventory objects
        // sku => Inventory
        Map<String, Inventory> invMap = new Map<String, Inventory>();
        Decimal qty = 0;
        Location__c loc = CCFPLocationDAO.getLocationForCode(primaryLocation);
        for(ccrz__E_ProductInventoryItem__c pii :piiList){
            // TODO - not sure which location field should be used
            // Inventory inv = createInventory(primaryLocation, pii.ccrz__ProductItem__c, pii.ccrz__ProductItem__r.ccrz__Sku__c, pii.ccrz__QtyAvailable__c, pii.Location__r.Location__c);
            // TODO : resolve null values                        
            if(pii != null) {
                if(pii.ccrz__QtyAvailable__c != null) {
                    qty = pii.ccrz__QtyAvailable__c;
                }
                Inventory inv = createInventory(primaryLocation, loc, pii.ccrz__ProductItem__c, pii.ccrz__ProductItem__r.ccrz__Sku__c, qty, pii.ccrz__InventoryLocationCode__c, pii.Location__r.City__c, pii.Location__r.State__c);
                // Inventory inv = createInventory(locId, pii.ccrz__ProductItem__c, pii.ccrz__ProductItem__r.ccrz__Sku__c, pii.ccrz__QtyAvailable__c, pii.ccrz__InventoryLocationCode__c);
                // Add Inventory objects
                invMap.put(pii.ccrz__ProductItem__r.ccrz__Sku__c, inv);
            }
        }

        return invMap;
    }


    public static Map<String, Inventory> invMap = new Map<String, Inventory>();
    public static Inventory createInventory(String primaryLocation, Location__c loc, String sfid, String sku, Decimal quantity, String locationCode, String LocationCity, String locationState){

        Inventory inv = invMap.get(sku);
        if(inv == null){
            inv = new Inventory(sfid, sku, quantity.intValue());
            inv.primaryLoc = primaryLocation;
            invMap.put(sku, inv);

            ccrz.ccLog.log('CCPDCInventoryHelper createInventory'); 


            if(loc != null && loc.City__c != null) {
                inv.primaryLocCity = loc.City__c;
            } else {
                inv.primaryLocCity  = primaryLocation;
            }             
        }
        else {
            inv.totalQuantity += quantity.intValue();
        }
        InventoryLocation il = new InventoryLocation(locationCode, quantity.intValue(), locationCity, locationState);
        if(primaryLocation == locationCode){
            inv.primaryLocation = il;
            inv.primaryQuantity = quantity.intValue();
        }
        else {
            inv.otherLocations.add(il);
            inv.otherQuantity += quantity.intValue();
        }
        return inv;
    }

    public class Inventory {
        public String primaryLoc;
        public String sfid;
        public String sku;
        public Integer primaryQuantity;
        public Integer totalQuantity;
        public Integer otherQuantity;
        public String primaryLocCity;

        public InventoryLocation primaryLocation;
        public List<InventoryLocation> otherLocations = new List<InventoryLocation>();

        private Inventory(String sfid, String sku, Integer quantity){
            this.sfid = sfid;
            this.sku = sku;
            this.totalQuantity = quantity;

            this.primaryQuantity = 0;
            this.otherQuantity = 0;
        }

    }
    public class InventoryLocation {
        public String locationCode;
        public Integer quantity;
        public String LocationCity;
        public String LocationState;

        public InventoryLocation(String locationCode, Integer quantity, String LocationCity, String LocationState){
            this.locationCode = locationCode;
            this.quantity = quantity;
            this.LocationCity = LocationCity;
            this.LocationState = LocationState;
        }
    }
    
        // handle creating structures inside the method instead of passing so much
    //public static CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse checkInventory(ccrz__E_Cart__c cart, List<ccrz__E_CartItem__c> cartItemList, Boolean reserveInventory, Boolean recheckAvailability, Boolean cancelReservation){
    public static CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse checkInventory(ccrz__E_Cart__c cart, List<ccrz__E_CartItem__c> cartItemList, Boolean reserveInventory, Boolean recheckAvailability, Boolean cancelReservation){
        ccrz.ccLog.log('CCPDCInventoryHelper CCPDCOMSFulfillmentPlanAPI');

        Boolean willCall = cart.CC_FP_Is_Will_Call__c;
        Boolean freeFreight = cart.CC_FP_Is_Free_Shipping__c;
        String customerNumber = cart.ccrz__Contact__r.CUSTNO__c;
        Decimal orderTotal = cart.ccrz__TotalAmount__c;
        String locationCode = cart.CC_FP_Location__r.Location__c;
        //String reservationId = cart.Reservation_Id__c;
        String storefront = cart.ccrz__Storefront__c;
        String curCartId = ccrz.cc_CallContext.currCartId;
        List<CCPDCOMSFulFillmentPlanAPI.OrderLine> orderLines = createOrderLines(cartItemList);
        List<CCPDCOMSFulFillmentPlanAPI.BackOrderLine> backOrderLines = createBackOrderLines(cartItemList, orderLines.size());

        CCPDCOMSAPI api = new CCPDCOMSAPI(storefront);
        CC_PDC_OMS_Settings__c settings = api.omsSettings;

        cart.CC_FP_Has_Validation_Message__c = false;
        cart.CC_FP_Validation_Message__c = '';

        // build cancellation request if cancelReservation==true
        // create the cancellation APIf
        // queue the cancellation API using api.queueRequest(cancellation api)

        CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanRequest fulFilReq = new CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanRequest();
        fulFilReq.orderId = curCartId;
        fulFilReq.reservationId = cart.Reservation_Id__c;
        fulFilReq.recheckAvailabilityFlag = recheckAvailability;
        fulFilReq.reserveInventoryFlag = reserveInventory;
        fulFilReq.freeFreightEligibilityFlag = freeFreight;
        fulFilReq.willCallFlag  = willCall;
        fulFilReq.shipUnavailableWillCallFlag = (willCall && cart.CC_FP_Is_Ship_Remainder__c);
        fulFilReq.customerNo = cart.ccrz__Account__r.AccountNumber;//customerNumber;
        fulFilReq.shipToAddress = new CCPDCOMSFulFillmentPlanAPI.ShipToAddress();
        fulFilReq.shipToAddress.zipcode = cart.ccrz__ShipTo__r.ccrz__PostalCode__c;//'75202';
        fulFilReq.shipToAddress.residentialFlag = ('Residential'.equalsIgnoreCase(cart.ccrz__ShipTo__r.CC_FP_Type__c)?true:false);
        fulFilReq.shipToAddress.liftgateFlag = cart.ccrz__ShipTo__r.CC_FP_Has_Lift_Gate__c;
        fulFilReq.shipToAddress.homeDC = cart.CC_FP_Location__r.Location__c;

   
        fulFilReq.channel = 'ECommerce';
        fulFilReq.orderTotal = orderTotal;

        fulFilReq.OrderLines = orderLines;
        fulFilReq.backOrderLines = backOrderLines; //new List<CCPDCOMSFulFillmentPlanAPI.BackOrderLine>();

        System.debug('Request: ' + JSON.serializePretty(fulFilReq));

        // Cancel the existing reservation if requested
        if(cancelReservation && cart.Reservation_Id__c != null){
            CCPDCOMSFulfillmentPlanAPI.CancelReservationRequest cancelRequest = new CCPDCOMSFulfillmentPlanAPI.CancelReservationRequest();
            cancelRequest.orderId = curCartId;
            cancelRequest.reservationId =  cart.Reservation_Id__c;
            CCPDCOMSFulfillmentPlanAPI cancelApi = new CCPDCOMSFulfillmentPlanAPI(settings, cancelRequest);
            api.queueRequest(cancelApi);

            fulFilReq.reservationId = null;
            cart.Reservation_Id__c = null;
        }

        CCPDCOMSFulfillmentPlanAPI fulFillApi = new CCPDCOMSFulfillmentPlanAPI(settings, fulFilReq);

        api.queueRequest(fulFillApi);


        api.executeRequests();
        CCAviHttpUtil httpUtil = fulFillApi.httpUtil;
        cart.CC_FP_Inventory_Request__c = httpUtil.request.getBody();
        if (httpUtil.response != null) {
            cart.CC_FP_Inventory_Response__c = httpUtil.response.getBody();
        }
        if(httpUtil.requestErrors){
            // exception
            cart.CC_FP_Has_Validation_Message__c = true;
            cart.CC_FP_Validation_Message__c = httpUtil.requestException.getMessage();
            CCPDCExceptionService.sendExceptionEmail('CCPDCInventoryHelper', 'checkInventory', httpUtil.requestException.getMessage());
        }
        else if(httpUtil.response != null && httpUtil.response.getStatusCode() != 200){
            // HTTP Status != 200/OK
            cart.CC_FP_Has_Validation_Message__c = true;
            cart.CC_FP_Validation_Message__c = String.valueOf(httpUtil.response.getStatusCode()) + '/' +  httpUtil.response.getStatus();// + '**' + httpUtil.response.getBody();
            String exceptionMessage = String.valueOf(httpUtil.response.getStatusCode()) + '/' +  httpUtil.response.getStatus();
            CCPDCExceptionService.sendExceptionEmail('CCPDCInventoryHelper', 'checkInventory', exceptionMessage);
        }
        else if(fulFillApi.fulfillmentResponse == null){
            cart.CC_FP_Has_Validation_Message__c = true;
            cart.CC_FP_Validation_Message__c =  String.valueOf(httpUtil.response.getStatusCode()) + '/' + httpUtil.response.getStatus() + '**' + httpUtil.response.getBody();
            CCPDCExceptionService.sendExceptionEmail('CCPDCInventoryHelper', 'checkInventory', cart.CC_FP_Validation_Message__c);
        }
        else if(!fulFillApi.fulfillmentResponse.success){
            // Fullfillment Status != success
            cart.CC_FP_Has_Validation_Message__c = true;
            // cart.CC_FP_Validation_Message__c = fulFillApi.fulfillmentResponse.message + '**' + fulFillApi.fulfillmentResponse + '**' + httpUtil.response.getBody();
            // cart.CC_FP_Validation_Message__c = fulFillApi.fulfillmentResponse.failureReason;
            cart.CC_FP_Validation_Message__c = fulFillApi.fulfillmentResponse.message;
            CCPDCExceptionService.sendExceptionEmail('CCPDCInventoryHelper', 'checkInventory', fulFillApi.fulfillmentResponse.message);
        }
        else if (fulFillApi.fulfillmentResponse.failureReason != null && fulFillApi.fulfillmentResponse.result == 'Failure') {
            cart.CC_FP_Has_Validation_Message__c = true;
            cart.CC_FP_Validation_Message__c = fulFillApi.fulfillmentResponse.failureReason;
            CCPDCExceptionService.sendExceptionEmail('CCPDCInventoryHelper', 'checkInventory', fulFillApi.fulfillmentResponse.failureReason);
        }
        else {
            cart.CC_FP_Validation_Message__c = null;//'success';
            // success
            if (fulFillApi.fulfillmentResponse.reservationId != null) {
                cart.Reservation_Id__c = fulFillApi.fulfillmentResponse.reservationId;
                cart.CC_FP_Split_Response__c = fulFillApi.fulfillmentResponseBody;
            }
            List<Decimal> shippingSubtotals = new List<Decimal>();
            Decimal shippingCost = 0;
            // Map of line item number => shipment quantity
            Map<Integer, Integer> lineItemToShipmentQuantity = new Map<Integer, Integer>();
            // if there are no shipment lines and no backorder lines, don't re-process the data
            if(fulFillApi.fulfillmentResponse.shipments.size() > 0 || fulFillApi.fulfillmentResponse.backOrderLines.size() > 0){
                for(CCPDCOMSFulfillmentPlanAPI.Shipment shipment :fulFillApi.fulfillmentResponse.shipments){
                    if(shipment.costToCustomer != null){
                        shippingCost += shipment.costToCustomer;
                    }

                    Integer qty = 0;
                    // Add up all the quantities for the line numbers in the shipment order lines
                    for(CCPDCOMSFulfillmentPlanAPI.OrderLine ol :shipment.lines){
                        qty = lineItemToShipmentQuantity.get(ol.orderLineNo);
                        if(qty == null){
                            qty = 0;
                        }
                        qty += ol.quantity;
                        lineItemToShipmentQuantity.put(ol.orderLineNo,qty);
                    }

                }
                cart.ccrz__ShipAmount__c = shippingCost;

                // For each request cart item,
                // get the cart item quantity
                // get the backorder quantity
                // get the requested quantity: cart item quantiy - backorder quantity
                // get the difference between shipment quantity
                // and requested quantity: (cart item quantity - backorder quantity) - shipment quantity
                // Set the backorder quantity to backorder quantity + (diff of request-shipment)
                for(Integer i = 0; i < cartItemList.size(); i++){
                    // Get the cart item  and order item
                    ccrz__E_CartItem__c ci = cartItemList[i];                    
                    System.debug('request cart item  start' + ci);   
                    ccrz.ccLog.log('request cart item  start' + ci);   
                    CCPDCOMSFulfillmentPlanAPI.OrderLine ol = orderLines[i];
                    // Get the inventory
                    Integer shipQty = lineItemToShipmentQuantity.get(ol.orderLineNo);
                    ci.CC_FP_QTY_Shipping__c = (shipQty != null) ? shipQty : 0; // todo 
                    // If there are no shipment lines, the back order quantity is the
                    // same as the requested quantity
                    if(shipQty == null || shipQty == 0){
                        ci.CC_FP_QTY_Backorder__c = ci.ccrz__Quantity__c;
                    }
                    // Otherwise, the backorder quantity is the cart item quantity
                    // minus the shipment quantity. If the cart item quantity and
                    // the ship quantity are equal, backorder quantity will be zero
                    else {
                        ci.CC_FP_QTY_Backorder__c = ci.ccrz__Quantity__c - shipQty;

                    }

                    if(ci.CC_FP_QTY_Shipping__c != null || ci.CC_FP_QTY_Shipping__c != 0) {
                        // calculate price for shipping items only
                        ci.ccrz__SubAmount__c = ci.ccrz__Price__c * ci.CC_FP_QTY_Shipping__c ;                        
                    }
                    ci.CC_FP_Item_Order_Subtotal__c = ci.ccrz__SubAmount__c;
                    System.debug('request cart item  end' + ci);   
                    ccrz.ccLog.log('request cart item  end' + ci);   
                }

            }

            if(fulFillApi.fulfillmentResponse.shipments.isEmpty() && !fulFillApi.fulfillmentResponse.backOrderLines.isEmpty() && cart.CC_FP_Is_Will_Call__c && !cart.CC_FP_Is_Ship_Remainder__c) {
                cart.CC_FP_Has_Validation_Message__c = true;
                cart.CC_FP_Validation_Message__c = getPageLabel('CC_PDC_WillCall_Zero_Quantity_Error');
            }

        }
        System.debug('Fulfillment response: ' + fulFillApi.fulfillmentResponse);
        update cart;
        return fulFillApi.fulfillmentResponse;
    }

    public static List<CCPDCOMSFulFillmentPlanAPI.OrderLine> createOrderLines(List<ccrz__E_CartItem__c> cartItemList){
        List<CCPDCOMSFulFillmentPlanAPI.OrderLine> orderLines = new List<CCPDCOMSFulFillmentPlanAPI.OrderLine>();
        Integer lineNum = 1;
        for(ccrz__E_CartItem__c cartItem :cartItemList){
            CCPDCOMSFulFillmentPlanAPI.OrderLine orderLine = new CCPDCOMSFulFillmentPlanAPI.OrderLine();
            // orderLine.partNo = cartItem.ccrz__Product__r.Part_Number__c; //'BAL-100';
            orderLine.partNo = cartItem.ccrz__Product__r.ccrz__Sku__c; //MT - changed per Laxma 11/15/2017
            orderLine.orderLineNo = lineNum; //1;
            ccrz.ccLog.log('CCPDCInventoryHelper createOrderLines 1' + cartItem);  
            ccrz.ccLog.log('CCPDCInventoryHelper createOrderLines orderLine.orderLineNo' + orderLine.orderLineNo);
            cartItem.CC_FP_Line_Number__c = lineNum;  
            /*if(cartItem.CC_FP_Line_Number__c == null){
                cartItem.CC_FP_Line_Number__c = lineNum;
                ccrz.ccLog.log('CCPDCInventoryHelper createOrderLines 2' + lineNum);   
            }*/
            if(cartItem.CC_FP_Line_Number__c != null){
                orderLine.orderLineNo = (Integer)cartItem.CC_FP_Line_Number__c;
            }
            Integer backorderQuantity = (cartItem.CC_FP_QTY_Backorder__c > 0?(cartItem.CC_FP_QTY_Backorder__c).intValue():0);
            Integer quantity = (cartItem.ccrz__Quantity__c.intValue() - backorderQuantity);//5;
            orderLine.quantity = quantity;
            orderLine.expeditedFlag = false; // TODO - is there an expedited flag?
            orderLines.add(orderLine);

            lineNum++;
        }
        return orderLines;
    }

    public static List<CCPDCOMSFulFillmentPlanAPI.BackOrderLine> createBackOrderLines(List<ccrz__E_CartItem__c> cartItemList, Integer size){
        List<CCPDCOMSFulFillmentPlanAPI.BackOrderLine> orderLines = new List<CCPDCOMSFulFillmentPlanAPI.BackOrderLine>();
        Integer lineNum = size+1;
        for(ccrz__E_CartItem__c cartItem :cartItemList){
            cartItem.CC_FP_Backorder_Line_Number__c = null;
            if(cartItem.CC_FP_QTY_Backorder__c > 0){

                CCPDCOMSFulFillmentPlanAPI.BackOrderLine orderLine = new CCPDCOMSFulFillmentPlanAPI.BackOrderLine();
                // orderLine.partNo = cartItem.ccrz__Product__r.Part_Number__c; //'BAL-100';
                orderLine.partNo = cartItem.ccrz__Product__r.ccrz__Sku__c; //MT - changed per Laxma 11/15/2017
                orderLine.quantity = cartItem.CC_FP_QTY_Backorder__c.intValue();//5;
                orderLine.orderLineNo = lineNum; 
                cartItem.CC_FP_Backorder_Line_Number__c = lineNum;
                orderLines.add(orderLine);
                lineNum++;
            }
        }
        return orderLines;
    }

    public static Map<String, Object> createExtraData(ccrz__E_Cart__c cart) {
        Map<String, Object> dataMap = new Map<String, Object>();
        dataMap.put('poNumber', cart.ccrz__PONumber__c);
        Map<String, Object> extraMap = new Map<String, Object>();
        dataMap.put('extra', extraMap);
        for (ccrz__E_CartItem__c item : cart.ccrz__E_CartItems__r) {
            Map<String, Object> itemMap = new Map<String, Object>();
            itemMap.put('CCFPPONumber', item.CC_FP_PONumber__c);
            extraMap.put(item.Id, itemMap);

            Map<String, String> mapProductData = new Map<String, String>();
            mapProductData.put('FPBrandName', item.ccrz__Product__r.FP_Brand_Name__c);
            mapProductData.put('partNumber', item.ccrz__Product__r.Part_Number__c);
            if(item.ccrz__Product__r.DSP_Part_Number__c != null){
                mapProductData.put('DSPPartNumber', item.ccrz__Product__r.DSP_Part_Number__c);
            }
            extraMap.put(item.ccrz__Product__r.Id, mapProductData);
        }
        return dataMap;
    }

    public static String getPageLabel(String name) {
        String value = name;
        List<ccrz__E_PageLabel__c> labels =  [Select Name, ccrz__PageLabelId__c, ccrz__ValueRT__c, ccrz__Value__c FROM ccrz__E_PageLabel__c WHERE Name = :name ];
        if (labels != null && !labels.isEmpty()) {
            ccrz__E_PageLabel__c label = labels[0];
            if (label.ccrz__ValueRT__c != null) {
                value = label.ccrz__ValueRT__c;
            }
            else if (label.ccrz__Value__c != null) {
                value = label.ccrz__Value__c;
            }
        }
        return value;
    }
 
}