global class InsertPriceList  implements Database.Batchable<sObject>  { 
     
      
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        
         String query = 'select id from ccrz__E_Product__c';   
             
        System.debug('Query--> '+query );
        return Database.getQueryLocator(query);
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<ccrz__E_Product__c>scope){
        
        List<ccrz__E_Product__c> ProductUpdateList  = new  List<ccrz__E_Product__c>();
        List<ccrz__E_PriceListItem__c> ProductPriceList  = new List<ccrz__E_PriceListItem__c>();
        
         
        for (ccrz__E_Product__c Products : Scope)
        {
            ccrz__E_PriceListItem__c record = new ccrz__E_PriceListItem__c();
            record.ccrz__Pricelist__c = 'a831W0000008OyQ';
                record.ccrz__StartDate__c = Date.today();
            record.ccrz__EndDate__c = Date.valueOf(System.label.CloseDateOpp);
                record.ccrz__Price__c =0;
                record.ccrz__Product__c = Products.id;
            ProductPriceList.add(record);
        }
         
        insert ProductPriceList;
    }
    
    global void finish(Database.BatchableContext BC){
        
        
    }
}