global with sharing class CCFPCheckoutCCPaymentController {
    public String baseURL {get; set;}
    public String path {get; set;}

    global CCFPCheckoutCCPaymentController() {

        String store = ccrz.cc_CallContext.storefront;
        CCAviCardConnectSettings__c settings = CCAviCardConnectSettings__c.getInstance(store);
        this.baseURL = settings.Base_URL__c;
        this.path = settings.Hosted_Tokenizer_Path__c;
    }


    @RemoteAction
    global static ccrz.cc_RemoteActionResult isStoredPayment(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);

            String storefront = ccrz.cc_CallContext.storefront;
            String token = (String)input.get('token');
            Id accountId = ccrz.cc_CallContext.effAccountId;
            CCAviCardConnectAPI.CardConnectResponse authResponse = CCAviCardConnectAPI.fpZeroAuthorization(storefront, (String)input.get('token'), (String)input.get('expiration'), (String)input.get('verificationCode'), 
                                                                                                           (String)input.get('name'), (String)input.get('address'), (String)input.get('city'), (String)input.get('region'), 
                                                                                                           		(String)input.get('country'), (String)input.get('postal'));
            List<ccrz__E_StoredPayment__c> findCard = [SELECT Id FROM ccrz__E_StoredPayment__c WHERE ccrz__Token__c  =:token AND ccrz__EffectiveAccountID__c=:accountId AND CreatedById =:UserInfo.getUserId()];


            if (authResponse.success && (findCard == null || findCard.isEmpty())) {
                CCAviPageUtils.buildResponseData(response, true, new Map<String,Object>{'input' => authResponse}
                );

            } else {

                if(findCard!=null){
                    if(authResponse != null &&  authResponse.message != null && authResponse.message == 'Non-numeric CVV') { authResponse.message = 'CC_PDC_CC_CVVInvalid';
                        CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'input' => authResponse, 'message' => 'CC_PDC_CC_CVVInvalid'}
                        );
                    } else {

                        CCAviPageUtils.buildResponseData(response, false,  new Map<String,Object>{'input' => authResponse, 'message' => 'Card Already Exists'}
                        );
                    }
                }
                else {
                    CCAviPageUtils.buildResponseData(response, true, new Map<String,Object>{'input' => authResponse}
                    );

                }

            }


            //response.success = authResponse.success;
            //Map<String,Object> result = new Map<String,Object>();
            //result.put('input', authResponse);
            //response.data = result;


        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                    new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }
  	@RemoteAction
    global static ccrz.cc_RemoteActionResult validateCC(ccrz.cc_RemoteActionContext ctx, String data, string address) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        ccrz__E_TransactionPayment__c basePayment = new ccrz__E_TransactionPayment__c() ;
        List<ccrz__E_TransactionPayment__c> transPayments = new List<ccrz__E_TransactionPayment__c>();
        Boolean basePayValid = false;
        String tokenId = '';
        String CVV = null;
        String expiration = '';
        String expMonth;
        String expYear;
        String Name = '';
        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            Map<String, Object> inputaddr = (Map<String, Object>) JSON.deserializeUntyped(address);
            if (input.get('storePaymentId') !=null){
                ccrz__E_StoredPayment__c storePayment = CCAviStoredPaymentDAO.getStoredPayment((String)input.get('storePaymentId'));
                if (storePayment != null) {
                    Name = storePayment.ccrz__DisplayName__c;
                    expMonth = String.valueOf(storePayment.ccrz__ExpMonth__c);
                    if (expMonth != null && expMonth.length() == 1) {
                        expMonth = '0' + expMonth;
                    }
                    expYear = String.valueOf(storePayment.ccrz__ExpYear__c);
                    expiration = expMonth + expYear;
                    tokenId =  storePayment.ccrz__Token__c;
                    CVV = (String)input.get('verificationCodeSP');
                    input.put('token',tokenId);
                    input.put('expiration',expiration);
                    // need to add line for CVV
                }
            }else{
                tokenId =  (String)input.get('token');
                CVV = (String)input.get('verificationCode');
                expiration = (String)input.get('expiration');
                Name = (String)input.get('name');     
                    
            }
           
			String encryptedCartId = ccrz.cc_CallContext.currCartId;
			String storefront = ccrz.cc_CallContext.storefront;
            Map<String,Object> result = new Map<String,Object>();
            ccrz__E_Cart__c cart = CCPDCCartDAO.getCartItemGroupDetails(encryptedCartId);
            basePayment = getBasePayment(input);
            ccrz.ccLog.log(system.LoggingLevel.info,'M:X BasePayment -->',basePayment);
			CCAviCardConnectAPI.CardConnectResponse authResponse = CCAviCardConnectAPI.fpZeroAuthorization(storefront,tokenId,expiration ,CVV,
                                                                                                           Name, (String)inputaddr.get('address') + ' ' +  (String)inputaddr.get('address2'), (String)inputaddr.get('city'), (String)inputaddr.get('region'), 
                                                                                                           		(String)inputaddr.get('country'),  ((String)inputaddr.get('postal')).substring(0,5));
			
            response.success = authResponse.success;
            basePayment.ccrz__Amount__c = cart.ccrz__TotalAmount__c;
            basePayment.CC_Cart__c = cart.id;
            if(authResponse != null &&  authResponse.message != null && authResponse.message == 'Non-numeric CVV') {
                authResponse.message = 'CC_PDC_CC_CVVInvalid';
            }
            result.put('input', authResponse);
            response.data = result;
            ccrz.ccLog.log(system.LoggingLevel.info,'M:X BasePayment Response --> ',authResponse);
            if (authResponse.success){
                basePayValid = true;
                for(ccrz__E_CartItemGroup__c g : cart.ccrz__E_CartItemGroups__r){
                    ccrz__E_TransactionPayment__c payment = basePayment.clone(false, true, false, false);
                    String amount = String.valueOf(g.CC_FP_Total_Amount__c);
                    CCAviCardConnectAPI.CardConnectResponse authResponseSeq = CCAviCardConnectAPI.fpAuthorization(storefront, tokenId , expiration, null, null, amount, g.Name, 
                                                                                                                  (String)inputaddr.get('address') + ' ' +  (String)inputaddr.get('address2'), (String)inputaddr.get('city'), (String)inputaddr.get('region'), 
                                                                                                                  (String)inputaddr.get('country'), ((String)inputaddr.get('postal')).substring(0,5));
					

                    response.success = authResponseSeq.success;
                    result.put('input', authResponseSeq);
            		response.data = result;
                    payment.CC_FP_Authorization_Response__c = authResponseSeq.body;
                    payment.ccrz__TransactionTS__c = DateTime.now();
                    payment.CC_Cart_Item_Group__c = g.Id;
                    payment.CC_Cart_Item_GroupName__c = g.ccrz__GroupName__c;
                    payment.ccrz__RequestAmount__c = g.CC_FP_Total_Amount__c;
                    ccrz.ccLog.log(system.LoggingLevel.info,'M:X AuthPayment Response --> ', authResponseSeq);
                    if (authResponseSeq.success == true) {
                        payment.ccrz__Amount__c = Decimal.valueOf(authResponseSeq.data.amount);
                        payment.ccrz__TransactionCode__c = authResponseSeq.data.retref;
                        
                    } else {
                        ccrz.ccLog.log('M:X AuthPayment failure else block -->');
                        payment.ccrz__TransactionCode__c = 'Authorization Failed';
                        transPayments.add(payment);
                        return response ;
                    } 
                    transPayments.add(payment);
                }
            }
            else{
                return response;
            }
        } 
        
        catch (Exception e) {
             ccrz.ccLog.log(system.LoggingLevel.info,'M:X Exception handling Message --> ', e.getMessage());
             CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            
			);
        }
        finally{
            ccrz.ccLog.log(system.LoggingLevel.info,'M:X FInally Block BasePayment--> ', basePayment);
            ccrz.ccLog.log(system.LoggingLevel.info,'M:X FInally Block transPayments --> ', transPayments);
            if (basePayValid){
                insert basePayment;
                if (!transPayments.isEmpty()){
                    for (ccrz__E_TransactionPayment__c tp : transPayments){
                        tp.ccrz__ParentTransactionPayment__c = basePayment.id;
                    }
                    insert transPayments;
                }
            }
        }
        system.debug('Last response --> ' + response);
        return response;
    }
    
    public static ccrz__E_TransactionPayment__c getBasePayment(Map <String, Object> input){
         ccrz__E_TransactionPayment__c basePayment = new ccrz__E_TransactionPayment__c();
            basePayment.ccrz__Account__c = ccrz.cc_CallContext.currAccountId;
            basePayment.ccrz__AccountNumber__c = '************' +  ((String)input.get('token')).substring(12);
            basePayment.ccrz__AccountType__c = 'cc';
            basePayment.ccrz__BillTo__c = ccrz.cc_CallContext.currAccountId;
            basePayment.ccrz__Comments__c = (String)input.get('name') != null ? (String)input.get('name') : '' ;
            basePayment.ccrz__Contact__c = ccrz.cc_CallContext.currContact.Id;
            basePayment.ccrz__CurrencyISOCode__c = ccrz.cc_CallContext.userCurrency;
            basePayment.ccrz__ExpirationMonth__c = Integer.valueOf(((String)input.get('expiration')).substring(0,2));
            basePayment.ccrz__ExpirationYear__c = Integer.valueOf(((String)input.get('expiration')).substring(2));
            basePayment.ccrz__SoldTo__c = ccrz.cc_CallContext.currAccountId;
            basePayment.ccrz__Storefront__c = ccrz.cc_CallContext.storefront;
            basePayment.ccrz__SubAccountNumber__c = (String)input.get('poNumber') != null ? (String)input.get('poNumber') : '';
            basePayment.ccrz__Token__c = (String)input.get('token');
            basePayment.ccrz__TransactionType__c = 'AUTH';
       		basePayment.ccrz__RequestAmount__c = 0;
            basePayment.ccrz__User__c = ccrz.cc_CallContext.currUserId;
        return basePayment;
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult validateCard(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);




            String storefront = ccrz.cc_CallContext.storefront;

            CCAviCardConnectAPI.CardConnectResponse authResponse = CCAviCardConnectAPI.fpZeroAuthorization(storefront, (String)input.get('token'), (String)input.get('expiration'), (String)input.get('verificationCode'),
                                                                                                           (String)input.get('name'), (String)input.get('address') + ' ' +  (String)input.get('address2'), (String)input.get('city'), (String)input.get('region'), 
                                                                                                           		(String)input.get('country'), (String)input.get('postal'));

            response.success = authResponse.success;
            Map<String,Object> result = new Map<String,Object>();

            if(authResponse != null &&  authResponse.message != null && authResponse.message == 'Non-numeric CVV') { authResponse.message = 'CC_PDC_CC_CVVInvalid';
            }
            result.put('input', authResponse);
            response.data = result;
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }

    @RemoteAction
    global static boolean isSavedCard(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);

        String storefront = ccrz.cc_CallContext.storefront;
        String token = (String) input.get('token');
        Id accountId = ccrz.cc_CallContext.effAccountId;
        List<ccrz__E_StoredPayment__c> findCard = [SELECT Id FROM ccrz__E_StoredPayment__c WHERE ccrz__Token__c = :token AND ccrz__EffectiveAccountID__c = :accountId AND CreatedById = :UserInfo.getUserId()];

        if (findCard == null || findCard.isEmpty()) {
            return true; } else { return false;
        }

    }
    
    
    /**
	 * saveAddress
	 * Updates a contact address record, and creates/updates the item group record
	 * If the addressJSON parameter contains an ID - the contact address is updated
	 * If the addressJSON parameter DOES NOT contain an ID - a new contact address is created, and it is associated with the correct item group
	 *
	 */
	
	@RemoteAction
	global static ccrz.cc_RemoteActionResult saveAddress(ccrz.cc_RemoteActionContext ctx, String addressJSON, String cartSfid){

		ccrz.cc_RemoteActionResult result = ccrz.cc_CallContext.init(ctx);
		result.success = false;
		Boolean isNewAddr = false;
		Savepoint sp = Database.setSavepoint();
		try{
			Map<String,Object> addressForm = (Map<String, Object>)JSON.deserializeUntyped(addressJSON);
			Map<String, Object> updatedAddress = (Map<String,Object>)addressForm.get('billTo');
			String addressId = (String)updatedAddress.get('sfid');

			if(String.isBlank(addressId)){
				isNewAddr = true;
				updatedAddress.remove('sfid');
			}

			Map<String, Object> addrInput = new Map<String, Object>{
				ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
				ccrz.ccApiAddress.ADDRESSLIST => new List<Map<String,Object>>{updatedAddress}
			};

			//handle discrepancies in the field names
			if(updatedAddress.containsKey('stateCode')){
				updatedAddress.put('stateISOCode', updatedAddress.remove('stateCode'));
				updatedAddress.remove('state');
			}else{
				updatedAddress.remove('state');
			}

			//Creating new contact address record
			if(isNewAddr){
				Map<String,Object> res = ccrz.ccApiAddress.createAddress(addrInput);
				List<String> addressIds = (List<String>)res.get(ccrz.ccApiAddress.ADDRESSIDLIST);
				addressId = addressIds.get(0);
				Map<String, Object> cartData = new Map<String, Object> {
					'sfid' => cartSfid,
					'billTo' => addressId
				};

				Map<String, Object> cartResult = ccrz.ccApiCart.revise(new Map<String, Object> {
					ccrz.ccAPI.API_VERSION => ccrz.ccAPI.CURRENT_VERSION,
					ccrz.ccAPICart.CART_OBJLIST => new List<Map<String, Object>>{cartData}
				});
			}else{ //otherwise updating contact address record
				Map<String,Object> res = ccrz.ccApiAddress.revise(addrInput);
			}

			result.data = addressId;
			result.success = true;
		}catch(Exception e){
			Database.rollBack(sp);
			ccrz.ccLog.log(System.LoggingLevel.ERROR,'ERR', e);
			ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
			msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
			msg.classToAppend = 'messagingSection-Error';
			msg.labelId = 'LLI_Checkout_Exception';
			msg.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
			result.messages.add(msg);
		}finally{
			ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X', 'saveAddress');
			ccrz.ccLog.close(result);
		}
		return result;
	}

    
    /* fetchBillTo
	 * fetches the address books for the current account
	 * if there is a default billing address, it creates MSO based on the billing address
	 *
	public static Map<String,Object> fetchBillTo(){
		ccrz.ccLog.log(LoggingLevel.INFO,'M:E','fetchBillTo');
		Map<String,Object> billAddress = new Map<String,Object>();
		String accountId = ccrz.cc_CallContext.isGuest ? ccrz.cc_CallContext.currAccountId : ccrz.cc_CallContext.effAccountId;
		List<Object> addressBookList = new List<Object>();
		List<Object> addressList = new List<Object>();
		Map<String,Object> inputData = (new Map<String,Object>{
			ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccAPIAddressBook.ACCOUNTID => accountId,
			ccrz.ccAPIAddressBook.TYPE => 'Billing',
			ccrz.ccAPIAddressBook.ISDEFAULT => true,
			ccrz.ccApi.SIZING => new Map<String, Object>{
				ccrz.ccAPIAddressBook.ENTITYNAME => new Map<String, Object>{
					ccrz.ccAPI.SZ_DATA=>ccrz.ccAPI.SZ_S
				},
				ccrz.ccAPIAddress.ENTITYNAME => new Map<String, Object>{
					ccrz.ccAPI.SZ_DATA=>ccrz.ccAPI.SZ_XL
				}
			}
		});

		try{
			Map<String, Object> outputData = ccrz.ccAPIAddressBook.fetch(inputData);
			addressBookList = (List<Object>)outputData.get(ccrz.ccApiAddressBook.ADDRESSBOOKS);
			addressList = (List<Object>)outputData.get(ccrz.ccAPIAddress.ADDRESSLIST);
			if(!ccrz.ccUtil.isEmpty(addressList)){
				billAddress = (Map<String,Object>)addressList[0];
				billAddress.remove('sfid');
			}
		}catch(Exception e){
			ccrz.ccLog.log(LoggingLevel.ERROR,'ERR', e);
			//silence exception, we'll just return an empty map
		}
		ccrz.ccLog.log(LoggingLevel.INFO,'M:X','fetchBillTo');
		return billAddress;
	}*/
}