global without sharing class CCPDCEffectiveAccountHook extends ccrz.cc_hk_EffectiveAccount {

    global override Map<String,Object> fetchAccounts(Map<String,Object> inputData) {
        String accountID = (String) inputData.get(PARAM_ACCOUNT_ID);
        String userID = (String) inputData.get(PARAM_USER_ID);
        try {
            List<Account> accountList = CCPDCAccountDAO.getAccountHierarchy(accountID);
            List<ccrz.cc_bean_EffectiveAccount> effAcctBeans = new List<ccrz.cc_bean_EffectiveAccount>();
            Set<Id> accountSet = new Set<Id>();
 
            for (Account acct: accountList) {
                accountSet.add(acct.Id);
                ccrz.cc_bean_EffectiveAccount acctBean = new ccrz.cc_bean_EffectiveAccount(acct);
                effAcctBeans.add(acctBean);
            }

            User u = CCPDCUserDAO.queryUserForUserId(userID);
            if (u != null && u.ContactId != null) {
                List<CC_FP_Contract_Account_Permission_Matrix__c> matrix = CCFPContactAccountPermissionMatrixDAO.getAccountsForContact(u.ContactId);
                for (CC_FP_Contract_Account_Permission_Matrix__c p : matrix) {
                    if (!accountSet.contains(p.Account__r.Id)) {
                        accountSet.add(p.Account__r.Id);
                        ccrz.cc_bean_EffectiveAccount acctBean = new ccrz.cc_bean_EffectiveAccount(p.Account__r);
                        effAcctBeans.add(acctBean);
                    }
                }
            }
 
            inputData.put(PARAM_EFFACCOUNT_LIST, effAcctBeans);
        }
        catch (Exception e) {
            System.debug(LoggingLevel.ERROR, e.getMessage());
            System.debug(LoggingLevel.ERROR, e.getStackTraceString());
        }
 
        return inputData;
    }
}