global class TopFiveComptTaskAccountUtility implements database.Batchable<sobject>,  DataBase.stateful{
      
    global Database.QueryLocator start(Database.BatchableContext BC){
    return Database.getQueryLocator([Select Id, name from account ] );
    }
    
    global void execute(Database.BatchableContext BC,List<Account> scope)
    {
        
        Set<Id> effAccIds = new set<Id>();
        for (Account acc : scope){
            effAccIds.add(acc.id);
        } 
       list<Account> acclst = [Select Name,Id,(Select Id, Subject, LatestFiveCompltActivity__c  from tasks where status = 'Completed' order by close_date__c desc limit 5) from account where Id in : effAccIds]; 
        List<Task> taskToUpdate = new List<Task>(); 
        for (Account acc : acclst){
          List<Task> tsklst = acc.tasks;
             for (Task t : tsklst) {
                t.LatestFiveCompltActivity__c= true;
                 taskToUpdate.add(t); 
              } 
         }
       database.update(taskToUpdate);
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
        
      }
    
}