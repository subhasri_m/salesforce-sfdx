@isTest
public class CCPDCServiceCartTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }

        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.SERVICE_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccServiceCart' => 'c.CCPDCServiceCart'
                }
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void fetchTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        Location__c loc = util.getLocation();
        cart.CC_FP_Location__c = loc.Id;
        update cart;

        Map<String,Object> response = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccAPICart.CART_ENCID => 'enc123'
        };

        Test.startTest();
        response = ccrz.ccAPICart.fetch(request);
        Test.stopTest();

        System.assert(response != null);
    }

}