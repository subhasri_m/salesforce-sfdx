@isTest
public class CCAviCardConnectAPITest {

    static testmethod void zeroAuthorizationTest() {
        createCardConnectSettings('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new CardConnectServiceMock(200, 'OK', SUCCESS_RESPONSE));

        CCAviCardConnectAPI.CardConnectResponse response = null;

        Test.startTest();
        response = CCAviCardConnectAPI.zeroAuthorization('DefaultStore', '9416190845351111', '1222', '123', 'Joe Credit');
        Test.stopTest();

        System.assert(response != null);
        System.assert(response.data != null);
        System.assertEquals(200, response.responseCode);
        System.assertEquals('A', response.data.respstat);
    }

    static testmethod void zeroAuthorizationFailTest() {
        createCardConnectSettings('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new CardConnectServiceMock(405, 'OK', SUCCESS_RESPONSE));

        CCAviCardConnectAPI.CardConnectResponse response = null;

        Test.startTest();
        response = CCAviCardConnectAPI.zeroAuthorization('DefaultStore', '9416190845351111', '1222', '123', 'Joe Credit');
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals(405, response.responseCode);
    }
    
    static testmethod void fpZeroAuthorizationTest() {
        createCardConnectSettings('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new CardConnectServiceMock(200, 'OK', SUCCESS_RESPONSE));

        CCAviCardConnectAPI.CardConnectResponse response = null;

        Test.startTest();
        response = CCAviCardConnectAPI.fpZeroAuthorization('DefaultStore', '9416190845351111', '1222', '123', 'Joe Credit', 'TestAddress', 'TestCity', 'TestRegion', 'TestCountry', 'TestPostal');
        Test.stopTest();

        System.assert(response != null);
        System.assert(response.data != null);
        System.assertEquals(200, response.responseCode);
        System.assertEquals('A', response.data.respstat);
    }

    static testmethod void fpZeroAuthorizationFailTest() {
        createCardConnectSettings('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new CardConnectServiceMock(405, 'OK', SUCCESS_RESPONSE));

        CCAviCardConnectAPI.CardConnectResponse response = null;

        Test.startTest();
        response = CCAviCardConnectAPI.fpZeroAuthorization('DefaultStore', '9416190845351111', '1222', '123', 'Joe Credit', 'TestAddress', 'TestCity', 'TestRegion', 'TestCountry', 'TestPostal');
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals(405, response.responseCode);
    }


    static testmethod void coverageTest() {
        String requestString = null;
        String responseString = null;

        Test.startTest();
        CCAviCardConnectAPI.AuthorizationRequest request = new CCAviCardConnectAPI.AuthorizationRequest();
        request.merchid = 'test';
        request.accttype = 'test';
        request.account = 'test';
        request.expiry = 'test';
        request.amount = 'test';
        request.currency_x = 'test';
        request.name = 'test';
        request.address = 'test';
        request.city = 'test';
        request.region = 'test';
        request.country = 'test';
        request.phone = 'test';
        request.postal = 'test';
        request.email = 'test';
        request.ecomind = 'test';
        request.cvv2 = 'test';
        request.orderid = 'test';
        request.track = 'test';
        request.bankaba = 'test';
        request.tokenize = 'test';
        request.termid = 'test';
        request.capture = 'test';
        request.authcode = 'test';
        request.signature = 'test';
        request.taxexempt = 'test';    
        requestString = request.serialize();

        CCAviCardConnectAPI.AuthorizationResponse response = new CCAviCardConnectAPI.AuthorizationResponse();
        response.respstat = 'test'; 
        response.retref = 'test'; 
        response.account = 'test'; 
        response.token = 'test'; 
        response.amount = 'test'; 
        response.merchid = 'test'; 
        response.respcode = 'test'; 
        response.resptext = 'test'; 
        response.respproc = 'test'; 
        response.avsresp = 'test'; 
        response.cvvresp = 'test'; 
        response.authcode = 'test'; 
        response.signature = 'test'; 
        response.commcard = 'test'; 
        response.emv = 'test'; 
        responseString = response.serialize();
        Test.stopTest();

        System.assert(requestString != null);
        System.assert(responseString != null);

    }

    public static void createCardConnectSettings(String storefront) {
        CCAviCardConnectSettings__c settings = new CCAviCardConnectSettings__c();
        settings.Name = storefront;
        settings.Auth_Path__c = '/cardconnect/rest/auth';
        settings.Base_URL__c = 'https://fleetpride.cardconnect.com';
        settings.Hosted_Tokenizer_Path__c = '/itoke/ajax-tokenizer.html';
        settings.Merchant_Id__c = '123';  
        settings.Named_Credential_Name__c = 'CardConnect_URL';  
        insert settings;
    }

    public class CardConnectServiceMock implements HttpCalloutMock {

        public Integer code {get; set;}
        public String status {get; set;}
        public List<String> body {get; set;}

        public CardConnectServiceMock(Integer code, String status, String body) {
            this.code = code;
            this.status = status;
            this.body = new List<String> {body};
        }

        public HTTPResponse respond(HTTPRequest request) {
            HttpResponse response = new HttpResponse();
            if (!this.body.isEmpty()) {
                response.setBody(this.body[0]);
                this.body.remove(0);
            }
            response.setStatusCode(this.code);
            response.setStatus(this.status);
            return response;
        }
    }

    public static final String SUCCESS_RESPONSE = '{"amount":"0.00","resptext":"Approval","commcard":"0A ","cvvresp":"M","avsresp":" ","respcode":"00","merchid":"008021479826","token":"9416190845351111","authcode":"PPS011","respproc":"VPS","retref":"255006161602","respstat":"A","account":"9416190845351111"}';
}