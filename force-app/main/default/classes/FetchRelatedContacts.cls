public class FetchRelatedContacts {
    @AuraEnabled
    public static List<Contact> getContacts(String recordId){
        List<Contact> conList = new List<Contact>();
        conList = [SELECT Id, Name, Email, Account.Id FROM Contact WHERE AccountId = :recordId];
        System.debug('ss==>'+conList);
        return conList;
    }
}