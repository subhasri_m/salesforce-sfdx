global without sharing class CCPDCShipToControllerHelper {
    public static void setDCForAccount(Id accountId, String dc) {
        Account a = [ SELECT Ship_from_Location__c, Distribution_Center__c FROM Account WHERE Id = :accountId LIMIT 1 ];                            
        a.Ship_from_Location__c = dc == null ? null : dc;
        a.Distribution_Center__c = dc == null ? null : dc;
        update a;
    }
}