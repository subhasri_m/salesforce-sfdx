@isTest
private class CCFPBoomiOrderPlacementAPITest {
	
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();

            ccrz__E_Cart__c cart = util.createCartWithSplits(m);
            ccrz__E_Order__c order = util.createOrderWithSplits(m, cart);
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = util.STOREFRONT;
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
        CCAviCardConnectAPITest.createCardConnectSettings(util.STOREFRONT);
    }
	
    static testmethod void placeOrderTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getCreateOrderCCResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:CreateOrderCCResponseList><OrderStatus><Status>Success</Status><Description>Order Creation Competed</Description></OrderStatus></wss:CreateOrderCCResponseList></wss:getCreateOrderCCResponse></S:Body></S:Envelope>'));

        ccrz__E_Order__c order = util.getSplitOrder();
        order = CCPDCOrderDAO.getOrderDetails(order.Id);
        List<ccrz__E_TransactionPayment__c> data = new List<ccrz__E_TransactionPayment__c> {
            CCAviTransactionPaymentDAOTest.createTransactionPayment(order)
        };
        insert data;
        CCPDCFulfillmentHelper.copyCartToOrderData(order.Id);
  
        Test.startTest();
        CCFPBoomiOrderPlacementAPI.placeOrder(order.Id, data);
        Test.stopTest();

        ccrz__E_Order__c updatedOrder = CCPDCOrderDAO.getOrderCustom(order.Id);
        System.assertEquals('In Process', updatedOrder.ccrz__OrderStatus__c);

    }

    static testmethod void placeOrderRequiredTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getCreateOrderCCResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:CreateOrderCCResponseList><OrderStatus><Status>Success</Status><Description>Order Creation Competed</Description></OrderStatus></wss:CreateOrderCCResponseList></wss:getCreateOrderCCResponse></S:Body></S:Envelope>'));

        ccrz__E_Order__c order = util.getSplitOrder();
        ccrz__E_Cart__c cart = util.getSplitCart();
        ccrz__E_Product__c product = util.getProduct();
        order = CCPDCOrderDAO.getOrderDetails(order.Id);
        List<ccrz__E_TransactionPayment__c> data = new List<ccrz__E_TransactionPayment__c> {
            CCAviTransactionPaymentDAOTest.createTransactionPayment(order)
        };
        insert data;

        ccrz__E_Product__c requiredProduct = util.createRequiredProduct(product);
        CC_FP_Required_Cart_Item__c requiredItem = new CC_FP_Required_Cart_Item__c(
            CC_Cart__c = cart.Id,
            CC_Parent_Cart_Item__c = cart.ccrz__E_CartItems__r[0].Id,
            Quantity__c = 1,
            Price__c = 5.00,
            SubAmount__c = 5.00,
            CC_Product__c = requiredProduct.Id
        );

        insert requiredItem; 

        CCPDCFulfillmentHelper.copyCartToOrderData(order.Id);
  
        Test.startTest();
        CCFPBoomiOrderPlacementAPI.placeOrder(order.Id, data);
        Test.stopTest();

        ccrz__E_Order__c updatedOrder = CCPDCOrderDAO.getOrderCustom(order.Id);
        System.assertEquals('In Process', updatedOrder.ccrz__OrderStatus__c);

    }
}