public with sharing class CCPDCOrderConfirmationController {
	public String orderItemMapJSON {get; set;}
	public String ordrItmGrpMapJSON {get; set;} 
	public String dcLocationsMapJSON {get; set;} 
	public String ffType {get; set;} 
	public String thisDcJSON {get; set;} 
    public String orderExtra {get; set;}
	public Decimal coreProductCharges {get; set;} 
	
	public CCPDCOrderConfirmationController() {							
		String orderEncryptedId = ApexPages.currentPage().getParameters().get('o');			
		List<ccrz__E_Order__c> orders = [ SELECT Id, ccrz__OriginatedCart__c, CC_FP_Is_Will_Call__c, Order_Comments__c,createdby.email, ccrz__Account__r.AccountNumber FROM ccrz__E_Order__c WHERE ccrz__EncryptedId__c =: orderEncryptedId ]; 		
		orderExtra = JSON.serialize(orders[0]);
        
        ccrz__E_Order__c o = null;		
		
		Map<String,  List<ccrz__E_OrderItem__c> > orderGroupsMap = new Map<String, List<ccrz__E_OrderItem__c> >();
		Map<Id, List<ccrz__E_OrderItem__c> > ordrItmGrpToItms = new Map<Id, List<ccrz__E_OrderItem__c> >();
		Map<Id, ccrz__E_OrderItemGroup__c > ordrItmGrpMap = new Map<Id, ccrz__E_OrderItemGroup__c >();
		Map<Id, Location__c> dcLocationsMap = new Map<Id, Location__c >();
	    Boolean isWillCall = false;    
			
		if(orders != null && orders.size() > 0) {						
			o = CCPDCOrderDAO.getOrderDetails(orders[0].Id);		
			if(o.ccrz__E_OrderItems__r != null && o.ccrz__E_OrderItems__r.size() > 0) {
				if ((o.CC_FP_Is_Will_Call__c != null && o.CC_FP_Is_Will_Call__c) && (o.CC_FP_Is_Ship_Remainder__c != null && !o.CC_FP_Is_Ship_Remainder__c) ){
					// CC_FP_Is_Ship_Remainder__c
					isWillCall = true;
				}				
				List<ccrz__E_OrderItem__c> orderItems = o.ccrz__E_OrderItems__r;
				for(ccrz__E_OrderItem__c oi : orderItems) {													
					if(!ordrItmGrpToItms.containsKey(oi.ccrz__OrderItemGroup__c)) { // doesnt exist yet, add it 						 
						List<ccrz__E_OrderItem__c> oiList = new List<ccrz__E_OrderItem__c> { oi };
						ordrItmGrpToItms.put(oi.ccrz__OrderItemGroup__c, oiList);
					} else {
						List<ccrz__E_OrderItem__c> oiList= ordrItmGrpToItms.get(oi.ccrz__OrderItemGroup__c);
						oiList.add(oi);
						ordrItmGrpToItms.put(oi.ccrz__OrderItemGroup__c, oiList);
					}					
				}
			}			
					
			if(o.ccrz__E_OrderItemGroups__r != null && o.ccrz__E_OrderItemGroups__r.size() > 0) {
				List<ccrz__E_OrderItemGroup__c> orderItemGroups = o.ccrz__E_OrderItemGroups__r;
				for(ccrz__E_OrderItemGroup__c oig: orderItemGroups) {
					
					if(ordrItmGrpToItms.containsKey(oig.Id)) {																				
						if(oig.CC_FP_DC_ID__c != null) {
							Location__c loc =  CCFPLocationDAO.getLocationForCode(oig.CC_FP_DC_ID__c);
							dcLocationsMap.put(oig.Id, loc);
						}
						// TODO check if will call and perms is allowed
            			Id accountId = ccrz.cc_CallContext.effAccountId;
            			Id contactId = ccrz.cc_CallContext.currContact.Id;
            			CCPDCUserPermissionsHelper.UserPermissions perms = CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);				
            			Boolean allowBO = perms.allowBackorders;						
						String orderItemGroupName = oig.ccrz__GroupName__c;											
						if( (orderItemGroupName.startsWithIgnoreCase('Backorder') && isWillCall) || (orderItemGroupName.startsWithIgnoreCase('Backorder') && !allowBO)) {
							System.debug('Cart is will call and no Backorder will be displayed');

						} else {
							orderGroupsMap.put(oig.ccrz__GroupName__c, ordrItmGrpToItms.get(oig.Id));
						}						
						ordrItmGrpMap.put(oig.Id, oig);												
					}																																				
				}
			}						
			if(o.CC_FP_Is_Will_Call__c && o.CC_FP_Is_Ship_Remainder__c) {
				ffType = 'CC_FP_Is_Ship_Remainder__c';

			} else if (o.CC_FP_Is_Will_Call__c && !o.CC_FP_Is_Ship_Remainder__c) {
				ffType = 'CC_FP_Is_Will_Call__c';
			} else {
				ffType = 'none';
			} 			

			if(ffType != null) {
				Id accountId = ccrz.cc_CallContext.effAccountId;
				// TODO : where is will call address coming from? or use CC_FP_Location__c from Order
				// o.CC_FP_Location__r.Location__c
				//CCFPLocationHelper.LocationResponse loc = CCFPLocationHelper.getLocationForAccount(accountId);
				Id orderLocId = o.CC_FP_Location__c;
				CCFPLocationHelper.LocationResponse loc = CCFPLocationHelper.getLocationForId(orderLocId);				
				thisDcJSON = JSON.serialize(loc);				
			}			
			dcLocationsMapJSON = JSON.serialize(dcLocationsMap);
			orderItemMapJSON = JSON.serialize(orderGroupsMap);		
			ordrItmGrpMapJSON = JSON.serialize(ordrItmGrpMap);	
			
			calculateCoreCharges(orderGroupsMap, orders[0].Id);
		} 					
	}

	private void calculateCoreCharges(Map<String,  List<ccrz__E_OrderItem__c> > orderGroupsMap, Id orderId){		
		coreProductCharges = 0;
		Map<String, ccrz__E_OrderItem__c> orderItemMap = new Map<String, ccrz__E_OrderItem__c>();    
		Set<Id> orderItemIds = new Set<Id>{orderId};
		
            if (ccrz.cc_CallContext.Storefront == 'parts'){
                for (String key : orderGroupsMap.keySet()){ 
                    List<ccrz__E_OrderItem__c> orderItems = orderGroupsMap.get(key);
                    for(ccrz__E_OrderItem__c oi : orderItems) {					
                        orderItemMap.put(oi.Id, oi);
                        orderItemIds.add(oi.Id);
                      }
                }                                       
             } else{
                for (String key : orderGroupsMap.keySet()){ 
                    if(key.startsWithIgnoreCase('Ship')) {
                        List<ccrz__E_OrderItem__c> orderItems = orderGroupsMap.get(key);
                        for(ccrz__E_OrderItem__c oi : orderItems) {					
                            orderItemMap.put(oi.Id, oi);
                            orderItemIds.add(oi.Id);
                        } 
                     }
                }                                       
            }  
				
		if(orderItemIds != null && orderItemIds.size() > 0) {
			List<CC_FP_Required_Order_Item__c> requiredOrderItems = CCPDCRequiredOrderItemDAO.getRequiredOrderItems(orderItemIds);
			                      
	        if (requiredOrderItems != null && requiredOrderItems.size() > 0) {            
	            for(CC_FP_Required_Order_Item__c rItem : requiredOrderItems) {
	            	// check if sku is in the map of ordered items
	                Id oId = rItem.CC_Parent_Order_Item__c;
	                ccrz__E_OrderItem__c oi = orderItemMap.get(oId);              	
	            	coreProductCharges = coreProductCharges + (rItem.Price__c * oi.ccrz__Quantity__c);                                
	            }      
	        }
		}		

	}


}