global with sharing class CCPDCServiceCart extends ccrz.ccServiceCart {
    global override Map<String, Object> getFieldsMap(Map<String, Object> inputData) {
        inputData = super.getFieldsMap(inputData);
        String objectFields = (String)inputData.get(ccrz.ccService.OBJECTFIELDS);
        objectFields += 
            ',CC_FP_Is_Ship_Remainder__c'
            + ',CC_FP_Is_Will_Call__c'
            + ',CC_FP_Location__c'
            + ',CC_FP_Validation_Message__c';
        return new Map<String, Object> {ccrz.ccService.OBJECTFIELDS => objectFields};

    }

    global override Map<String,Object> fetch(Map<String,Object> inputData){
        ccrz.ccLog.log(System.LoggingLevel.INFO, '*******************CCPDCServiceCartM:E', 'fetch');
        ccrz.ccLog.log(System.LoggingLevel.INFO, 'CCPDCServiceCartM:E', inputData);
        //if (inputData.get(ccrz.ccAPICart.BYOWNER) != null){
        //    inputData.remove(ccrz.ccAPICart.BYOWNER);
        //    inputData.put(ccrz.ccAPICart.BYEFFECTIVEACCOUNT, ccrz.cc_CallContext.effAccountId);
        //}
        ccrz.ccLog.log(System.LoggingLevel.INFO, 'CCPDCServiceCartM:E', inputData);
        Map<String,Object> data = super.fetch(inputData);
        ccrz.ccLog.log('super.fetch(inputData) >>>>>>  ' + data);
        ccrz.ccLog.log(System.LoggingLevel.INFO, 'CCPDCServiceCartM:X', 'fetch');
        return data;
    }

    global override Map<String, Object> getFilterMap(Map<String, Object> inputData) {

        Map<String, Object> localizedFilterMap = super.getFilterMap(inputData);

        if(inputData.get(ccrz.ccAPICart.BYEFFECTIVEACCOUNT) != null){
            localizedFilterMap.put(ccrz.ccApiCart.BYEFFECTIVEACCOUNT, ' AND EffectiveAccountID__c = \'' + ccrz.cc_CallContext.effAccountId + '\'');
        }

        return localizedFilterMap;
    }

    // Override and return a WO sharing DAO.
    global override Map<String, Object> initSvcDAO(Map<String, Object> inputData){
        return new Map<String, Object>{ccrz.ccService.SVCDAO => new ccrz.ccServiceDAOWO(inputData)};
    }

}