@isTest
public class LS_8x8_ControllerTest {

    @isTest static void  LS_8x8_Refreshcheck(){
        LS_8x8_Controller lscontrl = new LS_8x8_Controller();
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());
        PageReference apexPage1 = lscontrl.LS_8x8_Refresh();
       
             
    } 
   
    public class MockHttpResponseGenerator1  implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        
        if (req.getEndpoint().endsWith('agents')){
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"agent":[{"active-customer-chat-count":-1,"agent-guid":"fleetpride01-ag5IpPDWpgTyWATrVOYw7wfg-2edce869-9d84-4bcb-a9c7-fbda4d54eedd","agent-id":"ag5IpPDWpgTyWATrVOYw7wfg","agent-media-id":"120","agent-name":"Lizeth Aceituno","agent-status":12,"agent-sub-status":"active","assigned-skill-count":35,"day-accepted-count":2,"day-available-time":12,"day-avg-processing-time":500,"day-break-time":0,"day-login-time":30,"day-offline-time":0,"day-processing-time":0,"day-rejected-count":0,"day-wrap-up-time":0,"enabled-skill-count":6,"group-id":128,"idle-start-time":-1,"last-login-time":1564580384,"longest-active-customer-chat-duration":-1,"max-concurrent-customer-chat":1,"phone-line1-status":-1,"phone-line1-status-time":0,"phone-line2-status":-1,"phone-line2-status-time":0,"status-code-item-id":1,"status-code-item-short-code":"","status-code-list-id":-1,"thirty-min-accepted-count":0,"thirty-min-available-time":0,"thirty-min-avg-handling-time":0,"thirty-min-break-time":0,"thirty-min-offline-time":0,"thirty-min-processing-time":0,"thirty-min-rejected-count":0,"thirty-min-wrap-up-time":0,"time-in-status":141493}]}');
        res.setStatusCode(200);
        return res;  
            
        }else if (req.getEndpoint().endsWith('queues')){
		res.setHeader('Content-Type', 'application/json');
        res.setBody('{"queue":[{"agent-count-busy":10,"agent-count-loggedOut":11,"agent-count-onBreak":5,"agent-count-postProcess":0,"agent-count-waitTransact":5,"agent-count-workOffline":5,"day-abandoned":1,"day-accepted":0,"day-avg-duration":18,"day-avg-wait-time":400,"day-queued":"20","day-sla-activity":0,"assigned-agent-count":32,"enabled-agent-count":1,"longest-wait-time":0,"media-type":"phone","number-in-offered":0,"number-in-progress":0,"queue-id":130,"queue-name":"Tyler_S VM","queue-size":20,"queue-type":"inbound_phone","sla-activity":0,"sla-target":0,"thirty-min-abandoned":0,"thirty-min-accepted":0,"thirty-min-avg-duration":0,"thirty-min-avg-wait-time":0,"thirty-min-longest-wait-time":0,"thirty-min-queued":0,"thirty-min-sla-activity":0}]}');
        res.setStatusCode(200);
        return res;  
        }
        
       return null; 
        
    }
    }   
        
}