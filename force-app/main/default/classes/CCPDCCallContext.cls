global with sharing class CCPDCCallContext {
    global static Boolean skipPricing = false;

    global static Map<String, Decimal> priceMap = null;
    global static Map<String, Decimal> updatePriceMap = new Map<String, Decimal>();
}