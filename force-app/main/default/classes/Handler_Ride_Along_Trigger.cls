/********************************************************************************************************
* @author         PureSoftware
* @description    Handler_Ride_Along_Trigger. 

* @date           2019-10-16
*********************************************************************************************************

* Modification Log:  
* ------------------------------------------------------------------------------------------------------
* Developer                Date            Modification ID             Description 
* ------------------------------------------------------------------------------------------------------
* Rahul Bansal             2019-10-16                                    Initial version
* Rahul Bansal             2020-03-19                                    Email handler for new Ride Along Form
*/

public without sharing class Handler_Ride_Along_Trigger {
    
    public static void shareRecordWithManager(List<Ride_Along__c> RAlst)
    {
        List<Ride_Along__Share> RAshares= new List<Ride_Along__Share>();
        List<Ride_Along__c> RAtoConsider = new List<Ride_Along__c>();
        for (Ride_Along__c RA: RAlst){
            if (RA.Manager__c != null)
                RAtoConsider.add(RA);
        }
        if (!RAtoConsider.isEmpty()){
            for (Ride_Along__c RAs : RAtoConsider ){
                Ride_Along__Share RAShare = new Ride_Along__Share();
                RAShare.AccessLevel = 'Edit';
                RAShare.ParentId = RAs.Id;
                RAShare.RowCause = Schema.Ride_Along__Share.RowCause.Share_with_Manager__c;
                RAShare.UserOrGroupId = RAs.ManagerID__c;
                RAshares.add(RAShare); 
            }
            Database.SaveResult[] lsr = Database.insert(RAshares,false);
        }
    }
    @AuraEnabled
    public static string processEmail(String recordId){
        String sMessage = '';
        Contact tempContact;
        //String Subject = 'Test from Ride Along';
        EmailTemplate tpl = (EmailTemplate)[select Id FROM EmailTemplate WHERE Name = 'Ride Along Acknowledgment Template' limit 1];
        Ride_Along__c RA = [Select Team_Member__c, Team_Member__r.email, Team_Member__r.firstName,Team_Member__r.lastName, ManagerID__c, Acknowledgement_Sent_Date__c from Ride_Along__c where Id=:recordId limit 1];
        try{
            if(RA.Team_Member__c!=null) {
                Account acc = [Select Id from Account where ownerId =:RA.Team_Member__r.Id limit 1];
                tempContact = new Contact(email = RA.Team_Member__r.email, firstName = 'Dummy', lastName = 'Dummy', AccountId=acc.id);
                insert tempContact;// temp contact
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String [] ToAddresses = new List<String>();
                String [] CcAddresses = new List<String>();
                String [] BccAddresses = new List<String>();
                //setting To address
                ToAddresses.add(RA.Team_Member__c);
                mail.setToAddresses(ToAddresses);
                //setting Cc address
                if(RA.ManagerID__c!=null){
                    CcAddresses.add(RA.ManagerID__c);
                    mail.setCcAddresses(CcAddresses);
                    User RAManager = [Select Name, ManagerId from User where id=:RA.ManagerID__c limit 1 ];
                    //setting Bcc address
                    if(RAManager.ManagerID!=null){
                        BccAddresses.add(RAManager.ManagerID);
                        mail.setBccAddresses(BccAddresses);
                    }
                }
                mail.setTemplateId(tpl.id);
                mail.setTargetObjectId(tempContact.id);
                mail.setwhatId(RA.Id);
                mail.setSaveAsActivity(false);
                Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                sMessage='******************Mail Sent Successfully******************';
                RA.Acknowledgement_Sent_Date__c= DateTime.now();
                update RA;
                delete tempContact; // delete tempcontact
            } 
            else{
                sMessage='Error : Please assign a valid Team member';   
            }
        }
        catch(Exception ex){
            if(tempContact!=null)
                delete tempContact;
            sMessage=ex.getLineNumber()+'-'+ex.getCause()+'-'+ex.getMessage()+'-'+ex.getStackTraceString();
        } 
        return sMessage;
    }
    
    @Future(callout=true)
    public static void processEmailWithAttach(String recordId){
        String sMessage = '';
        Contact tempContact;
        //String Subject = 'Test from Ride Along';
        EmailTemplate tpl = (EmailTemplate)[select Id FROM EmailTemplate WHERE Name = 'Ride Along Completion Template' limit 1];
        Ride_Along__c RA = [Select Name,Team_Member__c,Sales_Call_Complete__c, Team_Member__r.email, Team_Member__r.firstName,Team_Member__r.lastName, ManagerID__c, Acknowledgement_Sent_Date__c from Ride_Along__c where Id=:recordId limit 1];
        if(RA.ManagerID__c!=null && RA.Sales_Call_Complete__c) {
            try{
                Account acc;
                if(Test.isRunningTest()){
                    acc = [Select Id from Account where ownerId =:RA.Team_Member__r.Id limit 1]; 
                }
                else{
                    acc = [Select Id from Account where ownerId =:UserInfo.getUserId() limit 1]; 
                }
                tempContact = new Contact(email = RA.Team_Member__r.email, firstName = 'Dummy', lastName = 'Dummy', AccountId=acc.id);
                insert tempContact;// temp contact
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String [] ToAddresses = new List<String>();
                String [] CcAddresses = new List<String>();
                //setting To address
                ToAddresses.add(RA.ManagerID__c);
                mail.setToAddresses(ToAddresses);
                //setting Cc address RVP
                User RAManager = [Select Name, ManagerId from User where id=:RA.ManagerID__c limit 1 ];
                if(RAManager.ManagerID!=null){
                    CcAddresses.add(RAManager.ManagerID);
                    mail.setCcAddresses(CcAddresses);
                }
                mail.setTemplateId(tpl.id);
                mail.setTargetObjectId(tempContact.id);
                mail.setwhatId(RA.Id);
                mail.setSaveAsActivity(false);
                // create record pdf and attach it as file to the record.
                List<String> attIds = CreatePDFAttachment.getContentVersionIds(String.valueOf(RA.Id), RA.Name); // get attachment ids
                mail.setEntityAttachments(attIds);
                Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                sMessage='******************Mail Sent Successfully******************';
                delete tempContact; // delete tempcontact
            } 
            catch(Exception ex){
                if(tempContact!=null)
                    delete tempContact;
                sMessage=ex.getLineNumber()+'-'+ex.getCause()+'-'+ex.getMessage()+'-'+ex.getStackTraceString();
            } 
        }  
        else{
            sMessage='Manager field is Blank';   
        }
        
    }
    
}