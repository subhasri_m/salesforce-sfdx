global class FP_Batch_ProductSequence_Scheduler implements System.Schedulable {
    global void execute(SchedulableContext SC){
        Database.executeBatch(new FP_Batch_DefaultProductSequence());
    }
}