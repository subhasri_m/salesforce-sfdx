@isTest
private class Batch_deleteTasksWithoutAccTest {
    
    static testmethod void test() {
        
        User user1 = new User(alias = 'ceo', email='admin@testorg.com',
                              emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                              Salesman_Number__c = '3-88888',
                              localesidkey='en_US',
                              timezonesidkey='America/Los_Angeles', username='adminTas@testorg.com', profileid = '00e400000013ttZ', Create_Accounts__c = true);
        insert user1;
         
        
        task t1 = new task();
        t1.activitydate = date.today();
        t1.Type = 'Sales Call';
        
        insert t1;  
        
        test.startTest();
        database.executeBatch(new Batch_DeleteTasksWithoutAccounts());          
        test.stopTest();
    }
    
       
    static testmethod void test1() {
        
        User user1 = new User(alias = 'ceo', email='admin@testorg.com',
                              emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                              Salesman_Number__c = '3-88888',
                              localesidkey='en_US',
                              timezonesidkey='America/Los_Angeles', username='adminTas@testorg.com', profileid = '00e400000013ttZ', Create_Accounts__c = true);
        insert user1;
        
        Account acc;
        System.runas(user1){
            acc = new Account(Name = 'Test Trigger', OwnerId = User1.id, AccountNumber = '1-100-0');
            insert acc;
        } 
       
        contact c1 = new contact();
        c1.LastName ='test1';
        c1.AccountId= acc.id;
        c1.Email = 'test@gmail.com';
        c1.FirstName = 'test12';
        insert c1;
        system.Debug('check contact'+c1.id);
        
        task t2 = new task();
        t2.activitydate = date.today();
        t2.whoid = c1.Id;
        t2.Status = 'Not Completed';
        t2.Type = 'Sales Call'; 
        insert t2;
      
        
        test.startTest();
        database.executeBatch(new Batch_DeleteTasksWithoutAccounts());          
        test.stopTest();
    }

    
}