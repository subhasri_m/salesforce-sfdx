@isTest
public class CCFPLogicUserRegisterTest {
    static User thisUser;
    static CCPDCTestUtil util = new CCPDCTestUtil();
    
    @testSetup
    static void testSetup() {        
        thisUser = [SELECT Id,AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        thisUser.Email = 'test@gmail.com';
        update thisUser;
        
        System.debug('thisUser==>'+thisUser.AccountId);
        Account acc = new Account(Id=thisUser.AccountId);
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }
    @IsTest
    private static void validateUserTest() {
        util.initCallContext();
        User theUser = util.getPortalUser();

        map<String,String> usernameToRecordMap = new Map<String,String>();
        usernameToRecordMap.put('username',theUser.Username);
        Map<String, Object> inputData = new Map<String, Object>();
        inputData.put(ccrz.ccApiUser.USER_DATA_MAP, usernameToRecordMap);
        inputData.put('usrRegisterJson', '{"accountNumber":"","billingZipCode":"30040","firstName":"sridhar","lastName":"g","jobTitle":"","username":"test@testacct50.com","primaryPhone":"2149084232","companyName":"TestB2B","billingAddress.address1":"5252 Shiloh","billingAddress.address2":"","billingAddress.city":"Cumming","billingAddress.stateCode":"GA","billingAddress.postalCode":"30040","language":"en_US","currencyCode":"USD","timeZone":"America/Chicago","accountNumberFlag":false,"taxExemptFlag":false,"shippingAddress.address1":"5252 Shiloh","shippingAddress.address2":"","shippingAddress.city":"Cumming","shippingAddress.stateCode":"GA","shippingAddress.postalCode":"30040","billingAddress.countryCode":"US","shippingAddress.countryCode":"US"}');
        test.startTest();
        CCFPLogicUserRegister instance = new CCFPLogicUserRegister();
        Map<String, Object> result = instance.validateUser(inputData);
        test.stopTest();
        system.assertNotEquals(null,result);
    }
    
   @IsTest
    private static void createAccountTest() {
        util.initCallContext();
        User theUser = util.getPortalUser();
        theUser.Email = 'test@gmail.com';
        //theUser.AccountId = acc.Id;
        update theUser;
        Contact theContact = util.getContact();
        
        map<String,String> usernameToRecordMap = new Map<String,String>();
        usernameToRecordMap.put('username','test@gmail.com');
        Map<String, Object> inputData = new Map<String, Object>();
		inputData.put(ccrz.ccApiUser.USER_DATA_MAP, usernameToRecordMap);
        inputData.put('usrRegisterJson', '{"accountNumber":"","billingZipCode":"30040","firstName":"sridhar","lastName":"g","jobTitle":"","username":"test@testacct50.com","primaryPhone":"2149084232","companyName":"TestB2B","billingAddress.address1":"5252 Shiloh","billingAddress.address2":"","billingAddress.city":"Cumming","billingAddress.stateCode":"GA","billingAddress.postalCode":"30040","language":"en_US","currencyCode":"USD","timeZone":"America/Chicago","accountNumberFlag":false,"taxExemptFlag":false,"shippingAddress.address1":"5252 Shiloh","shippingAddress.address2":"","shippingAddress.city":"Cumming","shippingAddress.stateCode":"GA","shippingAddress.postalCode":"30040","billingAddress.countryCode":"US","shippingAddress.countryCode":"US"}');
        test.startTest();
        CCFPLogicUserRegister instance = new CCFPLogicUserRegister();
        Map<String, Object> result = instance.createAccount(inputData);
        test.stopTest();
    }
    @IsTest
    private static void createExternalUserTest() {
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Contact cont = new Contact();
        cont.FirstName='Test';
        cont.LastName='Test';
        cont.Accountid= testAccount.id;
        cont.Email = 'cont@gmail.com';
        insert cont;
        
        Profile profile = [SELECT Id FROM Profile WHERE Name='CloudCraze Customer Community User FleetPride'];
        User user = new User(Alias = 'standt', Email='signup@gmail.com',
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = profile.Id, IsActive = true,
                TimeZoneSidKey='America/Los_Angeles', UserName='signup@gmail.com');
        user.ContactId = cont.Id;
        insert user;
        
        Map<String, Object> inputData2 = new Map<String, Object>();
		inputData2.put(ccrz.ccApiUser.USER_ID, user.id);
        inputData2.put('usrRegisterJson', '{"accountNumber":"","billingZipCode":"30040","firstName":"sridhar","lastName":"g","jobTitle":"","username":"test@testacct50.com","primaryPhone":"2149084232","companyName":"TestB2B","billingAddress.address1":"5252 Shiloh","billingAddress.address2":"","billingAddress.city":"Cumming","billingAddress.stateCode":"GA","billingAddress.postalCode":"30040","language":"en_US","currencyCode":"USD","timeZone":"America/Chicago","accountNumberFlag":false,"taxExemptFlag":false,"shippingAddress.address1":"5252 Shiloh","shippingAddress.address2":"","shippingAddress.city":"Cumming","shippingAddress.stateCode":"GA","shippingAddress.postalCode":"30040","billingAddress.countryCode":"US","shippingAddress.countryCode":"US"}');
        test.startTest();
        CCFPLogicUserRegister instance = new CCFPLogicUserRegister();
        Map<String, Object> result = instance.createExternalUser(inputData2);
        test.stopTest();
        system.assertEquals(user.id,result.get('usrId'));
    } 

}