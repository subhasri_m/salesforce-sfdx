global class FP_Batch_DefaultAccountGroup implements Database.Batchable<sObject> {
    List<ccrz__E_AccountGroup__c> accgroupCo1 = [Select id from ccrz__E_AccountGroup__c where Name = 'FP-AllAccounts'];
    String AccountGroupIdCo1 = accgroupCo1[0].id;  
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String Query = 'select id,ccrz__E_AccountGroup__c,Iseries_Company_code__c from Account where ccrz__E_AccountGroup__c = null';
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
       
        List <Account> Acctupdate = new List<Account>();
        For(Account Acc : scope){    
            if(Acc.Iseries_Company_code__c == '1'){
                Account Acct = new Account(); 
                Acct.id = Acc.id;
                Acct.ccrz__E_AccountGroup__c = AccountGroupIdCo1;
                Acctupdate.add(Acct);
            }else if(Acc.Iseries_Company_code__c == '2'){
                Account Acct = new Account(); 
                Acct.id = Acc.id;
                Acct.ccrz__E_AccountGroup__c = System.label.AccountGroupID;
                Acctupdate.add(Acct);
                
            }
            
        }
        update Acctupdate;
    }
    global void finish(Database.BatchableContext BC){
        System.debug(System.LoggingLevel.DEBUG, 'Finish Executed');
    }
 

}