public class PopNumberOfOSRTrigger_Handler {
    public static void populateNoOSRField(List<User> userList){
        Decimal num = 0;
        List<User> uList = new List<User>();
        List<User> uList2 = new List<User>();
        uList = [SELECT Id, Name, Title, Manager.Id FROM User WHERE Title = 'Outside Sales Representative' AND isActive = true];
        System.debug('@@@@' + uList.size());
        for(User u: userList){
            System.debug('@@@@' + num);
            for(User u2: uList){
                if(u2.Manager.Id == u.Id){
                    System.debug('@@@@' + num);
                    num = num + 1;
                    System.debug('@@@@' + num);
                }
            }   
            System.debug('@@@@' + num);
            u.Number_of_OSR__c = num;
        }

    }
}