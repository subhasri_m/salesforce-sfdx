@isTest
public class CCPDCPricingHelperTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = util.STOREFRONT;
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
    }

    static testmethod void priceProductListTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        List<Map<String,Object>> prodList = new List<Map<String,Object>>();

        Map<String,Object> prod1 = new Map<String,Object>{
            'SKU' => 'product-01',
            'partNumber' => 'product-01',
            'poolNumber' => '123'
        };
        Map<String,Object> prod2 = new Map<String,Object>{
            'SKU' => 'product-02',
            'partNumber' => 'product-02',
            'poolNumber' => '123'
        };
        prodList.add(prod1);
        prodList.add(prod2);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        Map<String, Decimal> response2 = null;

        Test.startTest();
        response = CCPDCPricingHelper.price(prodList);
        response2 = CCPDCPricingHelper.createPriceMap(response);
        Test.stopTest();

        System.assert(response != null);
        System.assert(response2 != null);
        System.assertEquals(2, response2.size());
    }

    static testmethod void priceProductListAnonymousTest() {
        /* This is a temporary method.
         * To delete once the customer login has been implemented
         */
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE /><QPRICE>0.000</QPRICE><QPRCCATID /><CUSTNO>999999</CUSTNO><CUSTBR>0</CUSTBR><LOCATE>DA</LOCATE><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL /><PLEXCPT /><LLVL /><MULTPLR>0.0000</MULTPLR><PRLOOP /><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE /><QPRICE>0.000</QPRICE><QPRCCATID /><CUSTNO>999999</CUSTNO><CUSTBR>0</CUSTBR><LOCATE>DA</LOCATE><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        List<Map<String,Object>> prodList = new List<Map<String,Object>>();
        String location = 'DA';

        Map<String,Object> prod1 = new Map<String,Object>{
            'SKU' => 'product-01',
            'partNumber' => 'product-01',
            'poolNumber' => '123'
        };
        Map<String,Object> prod2 = new Map<String,Object>{
            'SKU' => 'product-02',
            'partNumber' => 'product-02',
            'poolNumber' => '123'
        };
        prodList.add(prod1);
        prodList.add(prod2);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        Map<String, Decimal> response2 = null;

        Test.startTest();
        response = CCPDCPricingHelper.anonymousPrice(prodList, location);
        // response2 = CCPDCPricingHelper.createPriceMap(response);
        Test.stopTest();

        System.assert(response != null);
        // System.assert(response2 != null);
        // System.assertEquals(2, response2.size());
    }
    static testmethod void priceForSpecificLocationTest() {
        /* This is a temporary method.
         * To delete once the customer login has been implemented
         */
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE /><QPRICE>0.000</QPRICE><QPRCCATID /><CUSTNO>999999</CUSTNO><CUSTBR>0</CUSTBR><LOCATE>DA</LOCATE><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL /><PLEXCPT /><LLVL /><MULTPLR>0.0000</MULTPLR><PRLOOP /><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE /><QPRICE>0.000</QPRICE><QPRCCATID /><CUSTNO>999999</CUSTNO><CUSTBR>0</CUSTBR><LOCATE>DA</LOCATE><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        List<Map<String,Object>> prodList = new List<Map<String,Object>>();
        String location = 'DA';

        Map<String,Object> prod1 = new Map<String,Object>{
            'SKU' => 'product-01',
            'partNumber' => 'product-01',
            'poolNumber' => '123'
        };
        Map<String,Object> prod2 = new Map<String,Object>{
            'SKU' => 'product-02',
            'partNumber' => 'product-02',
            'poolNumber' => '123'
        };
        prodList.add(prod1);
        prodList.add(prod2);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        Map<String, Decimal> response2 = null;

        Test.startTest();
        response = CCPDCPricingHelper.priceForSpecificLocation(prodList, location);
        // response2 = CCPDCPricingHelper.createPriceMap(response);
        Test.stopTest();

        System.assert(response != null);
        // System.assert(response2 != null);
        // System.assertEquals(2, response2.size());
    }

    static testmethod void priceLineDataTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        List<Object> newLines = new List<Object>();
        ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
        newLine.sku = 'product-01';
        newLine.quantity = 1;
        newLines.add(newLine);
        newLine = new ccrz.ccApiCart.LineData();
        newLine.sku = 'product-02';
        newLine.quantity = 1;
        newLines.add(newLine);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;

        Test.startTest();
        response = CCPDCPricingHelper.price(newLines);
        Test.stopTest();

        System.assert(response != null);
    }

    static testmethod void priceLineDataIdTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));
        
        ccrz__E_Product__c product1 = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];

        List<Object> newLines = new List<Object>();
        ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
        newLine.sfid = product1.Id;
        newLine.quantity = 1;
        newLines.add(newLine);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;

        Test.startTest();
        response = CCPDCPricingHelper.price(newLines);
        Test.stopTest();

        System.assert(response != null);
    }

    static testmethod void priceLineDataMapTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        List<Object> newLines = new List<Object>();
        Map<String, Object> newLine = new Map<String, Object>();
        newLine.put('sku', 'product-01');
        newLine.put('quantity', 1);
        newLines.add(newLine);
        newLine = new Map<String, Object>();
        newLine.put('sku', 'product-02');
        newLine.put('quantity', 1);
        newLines.add(newLine);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;

        Test.startTest();
        response = CCPDCPricingHelper.price(newLines);
        Test.stopTest();

        System.assert(response != null);
    }

    static testmethod void priceCartItemsTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        ccrz__E_Product__c product1 = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product2 = util.createProduct('Product 3', 'product-03', 'Major', util.STOREFRONT);
        insert product2;


        List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c>();
        ccrz__E_CartItem__c item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product1.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 10;
        cartItems.add(item);
        item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product2.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 10;
        cartItems.add(item);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;

        Test.startTest();
        response = CCPDCPricingHelper.price(cartItems);
        Test.stopTest();

        System.assert(response != null);
    }

    static testmethod void getQuantityMapTest() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        ccrz__E_Product__c product1 = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product2 = util.createProduct('Product 3', 'product-03', 'Major', util.STOREFRONT);
        insert product2;

        List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c>();
        ccrz__E_CartItem__c item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product1.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 10;
        cartItems.add(item);
        item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product2.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 10;
        cartItems.add(item);
        item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product1.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 4;
        cartItems.add(item);
        item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product2.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 5;
        cartItems.add(item);

        Map<String, Map<String, Decimal>> response = null;

        Test.startTest();
        response = CCPDCPricingHelper.getQuantityMap(cartItems);
        Test.stopTest();

        System.assert(response != null);
        Map<String, Decimal> quantitySkuMap = response.get('sku');
        Map<String, Decimal> quantityIdMap = response.get('sfid');
        System.assert(quantitySkuMap != null);
        System.assert(quantitySkuMap.isEmpty());
        System.assert(quantityIdMap != null);
        System.assert(!quantityIdMap.isEmpty());
        System.assertEquals(2, quantityIdMap.size());
        System.assertEquals(14, quantityIdMap.get(product1.Id));
        System.assertEquals(15, quantityIdMap.get(product2.Id));
    }
    static testmethod void priceProductList2Test() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        List<Map<String,Object>> prodList = new List<Map<String,Object>>();

        Map<String,Object> prod1 = new Map<String,Object>{
            'SKU' => 'product-01',
            'partNumber' => 'product-01',
            'poolNumber' => '123'
        };
        Map<String,Object> prod2 = new Map<String,Object>{
            'SKU' => 'product-02',
            'partNumber' => 'product-02',
            'poolNumber' => '123'
        };
        prodList.add(prod1);
        prodList.add(prod2);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        Map<String, Decimal> response2 = null;

        Test.startTest();
         CCPDCPricingHelper.priceProductList(prodList);

        Test.stopTest();



    }

    static testmethod void priceProductList3Test() {
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        List<Map<String,Object>> prodList = new List<Map<String,Object>>();

        Map<String,Object> prod1 = new Map<String,Object>{
            'SKU' => 'product-01',
            'partNumber' => 'product-01',
            'poolNumber' => '123'
        };
        Map<String,Object> prod2 = new Map<String,Object>{
            'SKU' => 'product-02',
            'partNumber' => 'product-02',
            'poolNumber' => '123'
        };
        prodList.add(prod1);
        prodList.add(prod2);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        Map<String, Decimal> response2 = null;

        Test.startTest();
         CCPDCPricingHelper.priceProductList(prodList);

        Test.stopTest();



    }

    static testmethod void priceWithContTest() {
        util.initCallContext();

        List<Object> newLines = new List<Object>();
        Map<String, Object> newLine = new Map<String, Object>();
        newLine.put('sku', 'product-01');
        newLine.put('quantity', 1);
        newLines.add(newLine);
        newLine = new Map<String, Object>();
        newLine.put('sku', 'product-02');
        newLine.put('quantity', 1);
        newLines.add(newLine);

        HttpRequest request;

        Test.startTest();
            request = CCPDCPricingHelper.priceWithCont(newLines);
        Test.stopTest();

        System.assert(request != null);

    }

    static testmethod void priceWithCont2Test() {
        util.initCallContext();

        ccrz__E_Product__c product1 = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product2 = util.createProduct('Product 3', 'product-03', 'Major', util.STOREFRONT);
        insert product2;

        List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c>();
        ccrz__E_CartItem__c item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product1.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 10;
        cartItems.add(item);
        item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product2.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 10;
        cartItems.add(item);
        item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product1.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 4;
        cartItems.add(item);
        item = new ccrz__E_CartItem__c();
        item.ccrz__Product__c = product2.Id;
        item.ccrz__Price__c = 9.99;
        item.ccrz__Quantity__c = 5;
        cartItems.add(item);


        HttpRequest request;

        Test.startTest();
            request = CCPDCPricingHelper.priceWithCont(cartItems);
        Test.stopTest();

        System.assert(request != null);

    }

}