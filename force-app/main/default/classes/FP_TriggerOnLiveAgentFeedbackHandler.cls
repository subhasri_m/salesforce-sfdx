/**
* @description     FP_TriggerOnLiveAgentFeedbackHandler Called by FP_TriggerOnLiveAgentFeedback
* @author          Lalit Arora
* @Company         FP
* @date            29.Nov.2017
*
* HISTORY
* - 29.Nov.2016    Lalit      Created.
*/ 


public class FP_TriggerOnLiveAgentFeedbackHandler {
    set<String> ChatKeys = new set<String>();
    List<LiveChatTranscript> LiveChatTranscriptList = new List<LiveChatTranscript>();  
    map<String,LiveChatTranscript> ChatKeyTranscriptMapping = new map<string, LiveChatTranscript>();
    List<Live_Agent_Feedback__c> LiveChatTranscriptupdateList = new List<Live_Agent_Feedback__c>();  
    
    public void UpdateChatInfo(List<Live_Agent_Feedback__c> Triggernew)    
    {    
        for (Live_Agent_Feedback__c var : triggernew)
        {
            chatkeys.add(var.ChatKeyReference__c); 
        }
        LiveChatTranscriptList =[select id,chatkey,Contactid,Accountid,Caseid from LiveChatTranscript where chatkey in :chatkeys];
        
        if(!LiveChatTranscriptList.isEmpty()){ 
            for (LiveChatTranscript LATvar: LiveChatTranscriptList)
            {
                ChatKeyTranscriptMapping.put(LATvar.ChatKey, LATvar);
            } 
        } 
        
        for (Live_Agent_Feedback__c var : triggernew)
        {
            Live_Agent_Feedback__c LAFVar = new Live_Agent_Feedback__c();
            LAFvar.Id = var.id;
            
            if (ChatKeyTranscriptMapping.get(var.ChatKeyReference__c) !=null){
                if ( ChatKeyTranscriptMapping.get(var.ChatKeyReference__c).id !=null)
                {
                    LAFvar.Live_chat_transcript__c = ChatKeyTranscriptMapping.get(var.ChatKeyReference__c).id;
                }
                
                if (ChatKeyTranscriptMapping.get(var.ChatKeyReference__c).contactid !=null)
                {
                    LAFvar.Contact__c = ChatKeyTranscriptMapping.get(var.ChatKeyReference__c).contactid;
                }
                if (ChatKeyTranscriptMapping.get(var.ChatKeyReference__c).Accountid !=null)
                {
                    LAFvar.Account__c = ChatKeyTranscriptMapping.get(var.ChatKeyReference__c).Accountid;
                }
                if (ChatKeyTranscriptMapping.get(var.ChatKeyReference__c).caseid !=null)
                {
                    LAFvar.case__c = ChatKeyTranscriptMapping.get(var.ChatKeyReference__c).caseid;
                }
            }
            LiveChatTranscriptupdateList.add(LAFvar);
            
        }
        
        if (!LiveChatTranscriptupdateList.isEmpty())
        {
            update LiveChatTranscriptupdateList;
        }
        
    }
}