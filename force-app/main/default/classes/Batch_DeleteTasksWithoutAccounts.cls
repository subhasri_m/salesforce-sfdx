Global class Batch_DeleteTasksWithoutAccounts  implements database.Batchable<sobject>, schedulable{
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new Batch_DeleteTasksWithoutAccounts());
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {    
        
        String type = 'Sales Call'; 
        String query;
        if(test.isRunningTest())
        {
   query = 'Select id,owner.name,subject,whatid,whoid,who.type from task';
                 
        }
        else
        {
       query  = 'Select id,owner.name,subject,whatid,whoid,who.type from task where type = :type and ActivityDate!=null and whatid = null and status not in (\'Completed\', \'Not Completed\') and IsRecurrence != True';
     
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<Task> scope)
    { 
        List<Task> TaskListWithContacts = new List<Task>();
        List<Task> TaskListWithoutContacts = new List<Task>();
        List<Task> TaskDeleteList = new List<Task>();
        List<Task> TaskUpdateList = new List<Task>();
        Map<id,id> ContactAccountOwnerIdMap = new Map<id,id>();
        Map<id,id> ContactAccountMap = new Map<id,id>();
        for(Task Ta: Scope)
        {
            System.debug('tasks'+ta.Id);
        }
        for (Task Ta: Scope)
        { 
            if(ta.whoid==null)
            {
                TaskListWithoutContacts.add(ta);
            }
            
            else
            {
                if(ta.who.type =='Contact')
                TaskListWithContacts.add(ta);
            }
        } 
        Set<id> contactIds = new Set<id>();
        for(task tasks:TaskListWithContacts)
        {
            
            contactIds.add(tasks.whoid); 
        }
        
        for(contact contacts :[select id,ownerid,accountid,account.ownerid from contact where id in :contactIds  ])
        {
            
            ContactAccountOwnerIdMap.put(contacts.id,contacts.account.ownerid);
             ContactAccountMap.put(contacts.id,contacts.accountid);
        }
        
        for(task tasks:TaskListWithContacts)
        { 
            if(tasks.OwnerId != ContactAccountOwnerIdMap.get(tasks.WhoId))
            {
                TaskDeleteList.add(tasks);
            }
            
            else  
            {
                task t = new task();
                t.id = tasks.id;
                t.WhatId = ContactAccountMap.get(tasks.WhoId);
                TaskUpdateList.add(tasks);
            }
            
            
        }
        
        Delete TaskListWithoutContacts;
        Delete TaskDeleteList;
        Update TaskUpdateList;
   }
    
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
    
}