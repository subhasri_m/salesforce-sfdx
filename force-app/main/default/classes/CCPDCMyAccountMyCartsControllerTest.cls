@isTest
public class CCPDCMyAccountMyCartsControllerTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.SERVICE_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccServiceCart' => 'c.CCPDCServiceCart'
                }
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void getCartsTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__EffectiveAccountID__c = ccrz.cc_CallContext.currAccountId;
        update cart;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyCartsController.getCarts(ctx);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void changeCartOwnerTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz.cc_CallContext.currCartId = cart.Id;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyCartsController.changeCartOwner(ctx, cart.Id);
        Test.stopTest();

        System.assert(result != null);
       // System.assert(result.success);
    }

    static testmethod void cloneCartTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz.cc_CallContext.currCartId = cart.Id;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyCartsController.cloneCart(ctx, cart.Id);
        Test.stopTest();

        System.assert(result != null);
      //  System.assert(result.success);
        System.assert(result.data != null);
    }

    static testmethod void setNewCartValuesTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz.cc_CallContext.currCartId = cart.Id;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyCartsController.setNewCartValues(ctx, String.valueOf(cart.Id),String.valueOf(cart.ccrz__EncryptedId__c));
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
        System.assert(result.data != null);
    }

    static testmethod void deleteCartValueTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz.cc_CallContext.currCartId = cart.Id;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
         result = CCPDCMyAccountMyCartsController.deleteCart(ctx, String.valueOf(cart.Id));
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
        System.assert(result.data != null);



    }

}