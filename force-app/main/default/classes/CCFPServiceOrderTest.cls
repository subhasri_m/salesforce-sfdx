@isTest
public class CCFPServiceOrderTest {
    static testmethod void getSubQueryMapTest() {
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccService.SEARCHSTRING => 'Order'
        };
       Test.startTest();
        CCFPServiceOrder obj = new CCFPServiceOrder();
        obj.getSubQueryMap(request);
        test.stopTest();
    }
    
    static testmethod void getFieldsMapTest() {
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccService.SEARCHSTRING => 'Order'
        };
       Test.startTest();
        CCFPServiceOrder obj = new CCFPServiceOrder();
        obj.getFieldsMap(request);
        test.stopTest();
    }
    
    static testmethod void getOrderByMapTest() {
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 5, 
            ccrz.ccService.SEARCHSTRING => 'Order'
        };
       Test.startTest();
        CCFPServiceOrder obj = new CCFPServiceOrder();
        obj.getOrderByMap(request);
        test.stopTest();
    }

}