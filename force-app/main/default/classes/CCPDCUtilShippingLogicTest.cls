@isTest
public class CCPDCUtilShippingLogicTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void currentAccountFreeFeightValues(){

      Account account = util.getAccount();

      Map<String,Decimal> accountfreevalue = CCPDCUtilShippingLogic.currentAccountFreeFeightValues(account.Id);
      System.assert(accountfreevalue != null);
      System.debug(accountfreevalue);
    }

    static testmethod void currentQualifyFeightValue(){
      Decimal currentqualifyValue = CCPDCUtilShippingLogic.currentQualifyFeightValue('enc123');
      Decimal currentqualifyValueZero = CCPDCUtilShippingLogic.currentQualifyFeightValue(null);
      System.debug(currentqualifyValue);
      System.assertEquals(0,currentqualifyValueZero);
    }
    static testmethod void currentParcelShippingWeight(){
      Decimal currentqualifyWeightValue = CCPDCUtilShippingLogic.currentParcelShippingWeight('enc123');
      System.debug(currentqualifyWeightValue);
      System.assertEquals(0,currentqualifyWeightValue);
    }

}