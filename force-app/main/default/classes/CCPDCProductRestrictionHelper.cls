/** ------------------------------------------------------------------------------------------------------
 * @Description - Used to check product shipment  restriction
 *
 * @Author      
 * @Date        Sep 2017
 *-----------------------------------------------------------------------------------------------------*/
public without sharing class CCPDCProductRestrictionHelper {

    private static String restrictionAreaTypeCountry = 'Country';
    private static String restrictionAreaTypeZipCode = 'ZipCode';
    private static String restrictionAreaTypeState = 'State';
    private static String restrictionAreaTypeCity = 'City';
    private static String restrictionAreaTypeCounty = 'County';


    private static String restrictionTypeUnRestricted = 'Un-Restricted';

    /**
     * @Description - Check if cart products are restricted to the shipping address.
     *
     * @Author  
     * @Date    Sep 2017
     *
     * @param   String 
     * @param   String  
     * @param   String 
     * @param   String 
     * @param   String 
     * @param   String 
     * @return  ProductRestrictionResult 
     **/
    public static ProductRestrictionResult checkProductRestrictions(String cartId, String shippingCity, String shippingState, String shippingZip, String shippingCountry, String shippingCounty) {
        ProductRestrictionResult  result = new ProductRestrictionResult();
       	ccrz__E_Cart__c thecart = CCAviCartDAO.getCartForQuote(cartId);
        List<ccrz__E_CartItem__c> lstCartItems = thecart.ccrz__E_CartItems__r;
        ccrz__E_Cart__c cartShipToAdrr = CCPDCCartDAO.getCartShipTo(cartId);
        //ccrz__E_Cart__c cartLoc = CCPDCCartDAO.getCartOrderView(cartId);
        Set<Id> setProducts = new Set<Id>();
        for(ccrz__E_CartItem__c cartItem :lstCartItems){
            setProducts.add(cartItem.ccrz__Product__c);
        }
        Date today = System.today();
        // get product restrictions for the products in the cart
        //List<CC_PDC_Product_Restriction__c> lstProductRestrictions = [SELECT Id, Product__c, Country__c, Zip__c, State__c, City__c, County__c, Details__c FROM CC_PDC_Product_Restriction__c WHERE Product__c IN :setProducts ORDER BY Product__c];
        List<Product_Restriction__c> lstProductRestrictions = [SELECT Id, Related_to_Product__c, Related_to_Product__r.Name,Related_to_Product__r.Part_Number__c, Restriction_Status__c, Effective_Date__c, Restrcition_Type__c, Restriction_Area_Type__c, Restriction_Country__c, Restriction_ZipCode__c, Restriction_State__c, Restriction_City__c, Restriction_County__c, Restriction_Reason__c FROM Product_Restriction__c WHERE Restriction_Status__c = true AND (Effective_Date__c = null OR Effective_Date__c <= :today) AND Related_to_Product__c IN :setProducts ORDER BY Related_to_Product__c];

        // create map of product restrictions for each product
        Map<Id, List<Product_Restriction__c>> mapRestrictions = new Map<Id, List<Product_Restriction__c>>();
        for(Product_Restriction__c productRestriction : lstProductRestrictions) {
            List<Product_Restriction__c> lstCurrentProductsRestrictions = mapRestrictions.get(productRestriction.Related_to_Product__c);
            if (lstCurrentProductsRestrictions == null) {
                lstCurrentProductsRestrictions = new List<Product_Restriction__c>();
                mapRestrictions.put(productRestriction.Related_to_Product__c, lstCurrentProductsRestrictions);
            }
            lstCurrentProductsRestrictions.add(productRestriction);
        }

        //check if restrictions apply for each product.
        for(ccrz__E_CartItem__c cartItem :lstCartItems) {
            // get the restrictions for this product from the map.
            List<Product_Restriction__c> lstCurrentProductsRestrictions = mapRestrictions.get(cartItem.ccrz__Product__c);
            if (lstCurrentProductsRestrictions == null) {
                continue;
            }
            if((cartItem.FulFillMentType__c == 'ShipTo' || cartItem.FulFillMentType__c == 'LocalDelivery') && ccrz.cc_CallContext.currPageName != 'ccrz__CheckoutNew'){
               	shippingCity = cartShipToAdrr.ccrz__ShipTo__r.ccrz__City__c; 
                shippingState = cartShipToAdrr.ccrz__ShipTo__r.ccrz__State__c;
                shippingZip = cartShipToAdrr.ccrz__ShipTo__r.ccrz__PostalCode__c;
                shippingCountry = cartShipToAdrr.ccrz__ShipTo__r.ccrz__Country__c;
                shippingCounty = cartShipToAdrr.ccrz__ShipTo__r.CC_FP_County__c; 
            } else if (cartItem.FulFillMentType__c == 'PickUp' && ccrz.cc_CallContext.currPageName == 'ccrz__CheckoutNew'){
               continue;   
            }

            Boolean found = false;
            //check if country is  restricted.
            for (Product_Restriction__c productRestriction : lstCurrentProductsRestrictions) {
                if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeCountry && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted
                    && productRestriction.Restriction_Country__c == shippingCountry) {
                    Boolean isUnrestricted = false;
                    // country is restricted, see if they are unrestricted in state, zip, city, county
                    for (Product_Restriction__c productRestrictionInner : lstCurrentProductsRestrictions) {

                        // country is restricted, see if they are unrestricted in state
                        if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeState && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                            && productRestrictionInner.Restriction_Country__c == shippingCountry && productRestrictionInner.Restriction_State__c == shippingState) {
                            isUnrestricted = true;
                            break;
                        }
                        // country is restricted, see if they are unrestricted in zip
                        else 
                        if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeZipCode && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                            && productRestrictionInner.Restriction_Country__c == shippingCountry && productRestrictionInner.Restriction_ZipCode__c == shippingZip) {
                            isUnrestricted = true;
                            break;
                        }
                        // country is restricted, see if they are unrestricted in city
                        else 
                        if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeCity && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                            && productRestrictionInner.Restriction_Country__c == shippingCountry && productRestrictionInner.Restriction_State__c == shippingState && productRestrictionInner.Restriction_City__c == shippingCity) {
                            isUnrestricted = true;
                            break;
                        }
                        // country is restricted, see if they are unrestricted in county
                        else 
                        if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeCounty && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                            && productRestrictionInner.Restriction_Country__c == shippingCountry && productRestrictionInner.Restriction_State__c == shippingState && productRestrictionInner.Restriction_County__c == shippingCounty) {
                            isUnrestricted = true;
                            break;
                        }
                    }

                    // this product is restricted
                    if (!isUnrestricted) {
                        Restriction restriction = new Restriction();
                        restriction.productId = productRestriction.Related_to_Product__c;
                        
                        //String productName = productRestriction.Related_to_Product__r != null && productRestriction.Related_to_Product__r.Name != null ? productRestriction.Related_to_Product__r.Name : ''; 
                        String productName = productRestriction.Related_to_Product__r != null && productRestriction.Related_to_Product__r.Name != null ? productRestriction.Related_to_Product__r.Name + ' ' + productRestriction.Related_to_Product__r.Part_Number__c : '';
                        System.debug('#### productRestriction '+productRestriction);
                        if(productName != null) {
                            restriction.message = productName + ' is restricted from being sold to your selected address.';// + productRestriction.Restriction_Country__c;
                           // restriction.message = productName;
                        } else {
                            restriction.message = 'Product is restricted from being sold to your selected address.';// + productRestriction.Restriction_Country__c;
                        }                       
                        result.restrictions.add(restriction);
                        result.restricted = true;
                        found = true;
                        break;
                    }
                }
            }

            if (found) {
                continue;
            }

            //check if state is  restricted.
            for (Product_Restriction__c productRestriction : lstCurrentProductsRestrictions) {
                if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeState && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted
                    && productRestriction.Restriction_State__c == shippingState) {

                    Boolean isUnrestricted = false;
                    // state is restricted, see if they are unrestricted in city, county
                    for (Product_Restriction__c productRestrictionInner : lstCurrentProductsRestrictions) {

                        // state is restricted, see if they are unrestricted in city
                        if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeCity && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                            && productRestrictionInner.Restriction_Country__c == shippingCountry && productRestrictionInner.Restriction_State__c == shippingState && productRestrictionInner.Restriction_City__c == shippingCity) {
                            isUnrestricted = true;
                            break;
                        }
                        // state is restricted, see if they are unrestricted in county
                        else 
                        if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeCounty && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                            && productRestrictionInner.Restriction_Country__c == shippingCountry && productRestrictionInner.Restriction_State__c == shippingState && productRestrictionInner.Restriction_County__c == shippingCounty) {
                            isUnrestricted = true;
                            break;
                        }
                    }

                    // this product is restricted
                    if (!isUnrestricted) {
                        Restriction restriction = new Restriction();
                        restriction.productId = productRestriction.Related_to_Product__c;

                        String productName = productRestriction.Related_to_Product__r != null && productRestriction.Related_to_Product__r.Name != null ? productRestriction.Related_to_Product__r.Name + ' ' + productRestriction.Related_to_Product__r.Part_Number__c : '';

                        if(productName != null) {
                            restriction.message = productName+' is restricted from being sold to your selected address.';// + productRestriction.Restriction_Country__c;
                        } else {
                           restriction.message = 'Product is restricted from being sold to your selected address.';// + productRestriction.Restriction_State__c;
                        }                          
                        result.restrictions.add(restriction);
                        result.restricted = true;
                        found = true;
                        break;
                    }
                }
            }

            if (found) {
                continue;
            }

            //check if zip is  restricted.
            for (Product_Restriction__c productRestriction : lstCurrentProductsRestrictions) {
                if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeZipCode && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted
                    && productRestriction.Restriction_Country__c == shippingCountry && productRestriction.Restriction_ZipCode__c == shippingZip) {

                    Restriction restriction = new Restriction();
                    restriction.productId = productRestriction.Related_to_Product__c;
                    
                    String productName = productRestriction.Related_to_Product__r != null && productRestriction.Related_to_Product__r.Name != null ? productRestriction.Related_to_Product__r.Name + ' ' + productRestriction.Related_to_Product__r.Part_Number__c : '';
                    if(productName != null) {
                        restriction.message = productName+' is restricted from being sold to your selected address.'; //+ productRestriction.Restriction_Country__c;
                    } else {
                       restriction.message = 'Product is restricted from being sold to your selected address.';// + productRestriction.Restriction_State__c;
                    }                      
                    //restriction.message = 'Product is restricted in zip: ' + productRestriction.Restriction_ZipCode__c;
                    result.restrictions.add(restriction);
                    result.restricted = true;
                    found = true;
                    break;
                }
            }

            if (found) {
                continue;
            }
            
            //check if city is  restricted.
            for (Product_Restriction__c productRestriction : lstCurrentProductsRestrictions) {
                if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeCity && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted
                    && productRestriction.Restriction_Country__c == shippingCountry && productRestriction.Restriction_State__c == shippingState && productRestriction.Restriction_City__c == shippingCity) {

                    Restriction restriction = new Restriction();
                    restriction.productId = productRestriction.Related_to_Product__c;
                    String productName = productRestriction.Related_to_Product__r != null && productRestriction.Related_to_Product__r.Name != null ? productRestriction.Related_to_Product__r.Name + ' ' + productRestriction.Related_to_Product__r.Part_Number__c : '';
                    if(productName != null) {
                        restriction.message = productName+' is restricted from being sold to your selected address.';// + productRestriction.Restriction_Country__c;
                    } else {
                       restriction.message = 'Product is restricted from being sold to your selected address.';// + productRestriction.Restriction_State__c;
                    }                       
                    //restriction.message = 'Product is restricted in city: ' + productRestriction.Restriction_City__c;
                    result.restrictions.add(restriction);
                    result.restricted = true;
                    found = true;
                    break;
                }
            }

            if (found) {
                continue;
            }

            //check if county is  restricted.
            for (Product_Restriction__c productRestriction : lstCurrentProductsRestrictions) {
                if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeCounty && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted
                    && productRestriction.Restriction_Country__c == shippingCountry && productRestriction.Restriction_State__c == shippingState && productRestriction.Restriction_County__c == shippingCounty) {

                    Restriction restriction = new Restriction();
                    restriction.productId = productRestriction.Related_to_Product__c;
                    String productName = productRestriction.Related_to_Product__r != null && productRestriction.Related_to_Product__r.Name != null ? productRestriction.Related_to_Product__r.Name + ' ' + productRestriction.Related_to_Product__r.Part_Number__c : '';
                    if(productName != null) {
                        restriction.message = productName+' is restricted from being sold to your selected address.';// + productRestriction.Restriction_Country__c;
                    } else {
                       restriction.message = 'Product is restricted from being sold to your selected address.';// + productRestriction.Restriction_State__c;
                    }                      
                    //restriction.message = 'Product is restricted in county: ' + productRestriction.Restriction_County__c;
                    result.restrictions.add(restriction);
                    result.restricted = true;
                    found = true;
                    break;
                }
            }
        }
        return result;    
    }
	

    /**
     * @Description - Get Product Restrictions for a product.
     *
     * @Author  
     * @Date    Sep 2017
     *
     * @param   String
     * @return  SingleProductRestrictionResult 
     **/
    public static SingleProductRestrictionResult getProductRestriction(String productId) {

        Date today = System.today();
        // get product restrictions for the product
        //List<CC_PDC_Product_Restriction__c> lstCurrentProductsRestrictions = [SELECT Id, Product__c, Country__c, Zip__c, State__c, City__c, County__c, Details__c FROM CC_PDC_Product_Restriction__c WHERE Product__c = :productId];
        List<Product_Restriction__c> lstCurrentProductsRestrictions =[SELECT Id, Related_to_Product__c,Related_to_Product__r.Part_Number__c, Restriction_Status__c, Effective_Date__c, Restrcition_Type__c, Restriction_Area_Type__c, Restriction_Country__c, Restriction_ZipCode__c, Restriction_State__c, Restriction_City__c, Restriction_County__c, Restriction_Reason__c FROM Product_Restriction__c WHERE Restriction_Status__c = true AND (Effective_Date__c = null OR Effective_Date__c <= :today) AND Related_to_Product__c =  :productId];
        return parseProductRestriction(lstCurrentProductsRestrictions);
    }

    /**
     * @Description - Get Product Restrictions for a list of products.
     *
     * @Author  
     * @Date    Oct 2017
     *
     * @param   List<String>
     * @return  Map<Id, List<SingleProductRestrictionResult>> 
     **/
    public static Map<Id, SingleProductRestrictionResult> getProductsRestrictionMap(List<String> listProductIds) {
        Date today = System.today();
		
        Set<String> setProductIds = new Set<String>(listProductIds);

        List<Product_Restriction__c> lstProductRestrictions = [SELECT Id, Related_to_Product__c, Restriction_Status__c, Effective_Date__c, Restrcition_Type__c, Restriction_Area_Type__c, Restriction_Country__c, Restriction_ZipCode__c, Restriction_State__c, Restriction_City__c, Restriction_County__c, Restriction_Reason__c FROM Product_Restriction__c WHERE Restriction_Status__c = true AND (Effective_Date__c = null OR Effective_Date__c <= :today) AND Related_to_Product__c IN :setProductIds ORDER BY Related_to_Product__c];

        // create map of product restrictions for each product
        Map<Id, List<Product_Restriction__c>> mapRestrictions = new Map<Id, List<Product_Restriction__c>>();
        for(Product_Restriction__c productRestriction : lstProductRestrictions) {
            List<Product_Restriction__c> lstCurrentProductsRestrictions = mapRestrictions.get(productRestriction.Related_to_Product__c);
            if (lstCurrentProductsRestrictions == null) {
                lstCurrentProductsRestrictions = new List<Product_Restriction__c>();
                mapRestrictions.put(productRestriction.Related_to_Product__c, lstCurrentProductsRestrictions);
            }
            lstCurrentProductsRestrictions.add(productRestriction);
        }

        Map<Id, SingleProductRestrictionResult> returnMap = new Map<Id, SingleProductRestrictionResult>();

        for (String productId : setProductIds) {
            List<Product_Restriction__c> lstCurrentProductsRestrictions = mapRestrictions.get(productId);
            if (lstCurrentProductsRestrictions == null) {
                returnMap.put(productId, new SingleProductRestrictionResult());
            }
            else {
                returnMap.put(productId, parseProductRestriction(lstCurrentProductsRestrictions));
            }
        }

        return returnMap;
    }

    private static SingleProductRestrictionResult parseProductRestriction(List<Product_Restriction__c> lstCurrentProductsRestrictions) {

        SingleProductRestrictionResult result = new SingleProductRestrictionResult();

        for (Product_Restriction__c productRestriction : lstCurrentProductsRestrictions) {

            // Country is restricted
            if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeCountry && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted) {

                RestrictedAddress rAddress = new RestrictedAddress();
                rAddress.country = productRestriction.Restriction_Country__c;
                result.countryRestrictions.add(rAddress);
                result.hasRestrictions = true;


                // country is restricted, see if they are unrestricted in state, zip, city, county
                for (Product_Restriction__c productRestrictionInner : lstCurrentProductsRestrictions) {

                    // country is restricted, see if they are unrestricted in state
                    if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeState && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                        && productRestrictionInner.Restriction_Country__c == productRestriction.Restriction_Country__c) {
                        rAddress.exceptionStates.add(productRestrictionInner.Restriction_State__c);
                    }
                    // country is restricted, see if they are unrestricted in zip
                    else 
                    if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeZipCode && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                        && productRestrictionInner.Restriction_Country__c == productRestriction.Restriction_Country__c) {
                        rAddress.exceptionZips.add(productRestrictionInner.Restriction_ZipCode__c);
                    }
                    // country is restricted, see if they are unrestricted in city
                    else 
                    if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeCity && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                        && productRestrictionInner.Restriction_Country__c == productRestriction.Restriction_Country__c) {
                        CityCounty cc = new CityCounty();
                        cc.state = productRestrictionInner.Restriction_State__c;
                        cc.cityOrCounty = productRestrictionInner.Restriction_City__c;
                        rAddress.exceptionCities.add(cc);
                    }
                    // country is restricted, see if they are unrestricted in county
                    else 
                    if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeCounty && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                        && productRestrictionInner.Restriction_Country__c == productRestriction.Restriction_Country__c) {
                        CityCounty cc = new CityCounty();
                        cc.state = productRestrictionInner.Restriction_State__c;
                        cc.cityOrCounty = productRestrictionInner.Restriction_County__c;
                        rAddress.exceptionCounties.add(cc);
                    }
                }
            }
            // State is restricted
            else if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeState && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted) {

                RestrictedAddress rAddress = new RestrictedAddress();
                rAddress.country = productRestriction.Restriction_Country__c;
                rAddress.state = productRestriction.Restriction_State__c;
                result.stateRestrictions.add(rAddress);
                result.hasRestrictions = true;

                // state is restricted, see if they are unrestricted in city or county
                for (Product_Restriction__c productRestrictionInner : lstCurrentProductsRestrictions) {

                    // state is restricted, see if they are unrestricted in city
                    if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeCity && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                        && productRestrictionInner.Restriction_Country__c == productRestriction.Restriction_Country__c && productRestrictionInner.Restriction_State__c == productRestriction.Restriction_State__c) {
                        CityCounty cc = new CityCounty();
                        cc.state = productRestrictionInner.Restriction_State__c;
                        cc.cityOrCounty = productRestrictionInner.Restriction_City__c;
                        rAddress.exceptionCities.add(cc);
                    }
                    // state is restricted, see if they are unrestricted in county
                    else 
                    if (productRestrictionInner.Restriction_Area_Type__c == restrictionAreaTypeCounty && productRestrictionInner.Restrcition_Type__c == restrictionTypeUnRestricted
                        && productRestrictionInner.Restriction_Country__c == productRestriction.Restriction_Country__c && productRestrictionInner.Restriction_State__c == productRestriction.Restriction_State__c) {
                        CityCounty cc = new CityCounty();
                        cc.state = productRestrictionInner.Restriction_State__c;
                        cc.cityOrCounty = productRestrictionInner.Restriction_County__c;
                        rAddress.exceptionCounties.add(cc);
                    }
                }
            }
            // Zip is restricted
            else if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeZipCode && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted) {
            
                RestrictedAddress rAddress = new RestrictedAddress();
                rAddress.country = productRestriction.Restriction_Country__c;
                rAddress.zip = productRestriction.Restriction_ZipCode__c;
                result.zipRestrictions.add(rAddress);
                result.hasRestrictions = true;
            }
            // City is restricted
            else if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeCity && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted) {
            
                RestrictedAddress rAddress = new RestrictedAddress();
                rAddress.country = productRestriction.Restriction_Country__c;
                rAddress.state = productRestriction.Restriction_State__c;
                rAddress.city = productRestriction.Restriction_City__c;
                result.cityRestrictions.add(rAddress);
                result.hasRestrictions = true;
            }
            // County is restricted
            else if (productRestriction.Restriction_Area_Type__c == restrictionAreaTypeCounty && productRestriction.Restrcition_Type__c != restrictionTypeUnRestricted) {
            
                RestrictedAddress rAddress = new RestrictedAddress();
                rAddress.country = productRestriction.Restriction_Country__c;
                rAddress.state = productRestriction.Restriction_State__c;
                rAddress.county = productRestriction.Restriction_County__c;
                result.countyRestrictions.add(rAddress);
                result.hasRestrictions = true;
            }
        }
        return result;
    }
    
    public class Restriction {
        public String productId {get; set;}
        public String message {get; set;}
    }
    public class ProductRestrictionResult {
        // will be true if any of the product is  restricted t the shipping address.
        public Boolean restricted {get; set;}
        public List<Restriction> restrictions{get; set;}

        public ProductRestrictionResult() {
            restricted = false;
            restrictions = new List<Restriction>();
        }
    }

    public class CityCounty {
        public String state {get; set;}
        public String cityOrCounty {get; set;}
    }
    public class RestrictedAddress {
        public String country {get; set;}
        public String zip {get; set;}
        public String state {get; set;}
        public String city {get; set;}
        public String county {get; set;}

        public List<String> exceptionStates {get; set;}
        public List<String> exceptionZips {get; set;}
        public List<CityCounty> exceptionCities {get; set;}
        public List<CityCounty> exceptionCounties {get; set;}

        public RestrictedAddress() {
            exceptionStates = new List<String>();
            exceptionZips = new List<String>();
            exceptionCities = new List<CityCounty>();
            exceptionCounties = new List<CityCounty>();
        }
    }
    public class SingleProductRestrictionResult {
        public Boolean hasRestrictions {get; set;}
        public List<RestrictedAddress> countryRestrictions{get; set;}
        public List<RestrictedAddress> stateRestrictions{get; set;}
        public List<RestrictedAddress> zipRestrictions{get; set;}
        public List<RestrictedAddress> cityRestrictions{get; set;}
        public List<RestrictedAddress> countyRestrictions{get; set;}

        public SingleProductRestrictionResult() {
            hasRestrictions = false;
            countryRestrictions = new List<RestrictedAddress>();
            stateRestrictions = new List<RestrictedAddress>();
            zipRestrictions = new List<RestrictedAddress>();
            cityRestrictions = new List<RestrictedAddress>();
            countyRestrictions = new List<RestrictedAddress>();
        }
    }
}