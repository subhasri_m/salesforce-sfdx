public class FP_FetchCurrentContactUserId {
@AuraEnabled
    public static String getContactUserId(String contactId){
        List<User> currntConUserId = new List<User>();
        currntConUserId = [SELECT Id, name FROM user WHERE contact.id =: contactId limit 1];
        //System.debug('ss==>'+currntConUserId);
        if(currntConUserId.size() > 0){
            return currntConUserId[0].Id;
        }else{
            return Null;
        }
        
    }
}