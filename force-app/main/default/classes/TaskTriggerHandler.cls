/*
Description: Task Trigger Handler class for Account Object
Author: Alvin Claridad
History:
--------------------------------------------------------------------------
04.29.2015          Alvin Claridad(Cloud Sherpas)          Created
06.19.2015          Sid Pajutrao(Cloud Sherpas)            Created - DepopulateAccountRecussrenceEndDate
*/

public class TaskTriggerHandler{

 public Static Boolean IsRecursive = false;
//Added by Lalit
public static void populateRelatedAccountInfo(List<Task> newTaskList)
{
    List<id> relatedContactId = new List<id>(); 
    Map<id,id> ContactAccountMap = new Map<id,id>();
    for(task t : newTaskList)
    {
        if(t.WhoId!=null && t.WhatId==null && t.Type=='Sales Call')     
        { 
            relatedContactId.add(t.WhoId);     
        } 
    }
    
    for(contact c : [select id, account.id from contact where id in:relatedContactId])
    {
        if(c.accountid!=null)      
        {  
            ContactAccountMap.put(c.id,c.account.id); 
        }
    }
    
    for(task t : newTaskList)
    {
        if( ContactAccountMap.get(t.WhoId)!=null)
        {
            t.WhatId = ContactAccountMap.get(t.WhoId);
        }
    }
}

//Added by Lalit
public static void updateDaysSinceLastSalesCall(List<Task> newTaskList,Map<id,Task> TriggerOldMap)
{
    List<id> relatedAccountIds = new List<id>();
    Map<id, List<Task>> AccountTasksMap = new Map<id,List<Task>>();
    Map<id,Account> accountMap ;
    List<Account> AccountToUpdate = new List<Account>();
    set<id> UniqueIDs = new Set<Id>();
    List<id> Accids = new List<id>();
    
    for(task t : newTaskList)
    {
        Accids.add(t.whatid);
    }
    accountMap = new Map<ID, Account>([select id,Last_Completed_Task_Date__c from Account where id in :Accids ]);
    
    for(task t : newTaskList)
    {
        Account acc = new Account();
        if (t.Whatid!=null && String.valueOf(t.Whatid).substring(0,3)=='001'){
            if(t.Status=='Completed' || test.isRunningTest())
            { 
                if(accountMap.get(t.whatid).Last_Completed_Task_Date__c!=null){
                    if(accountMap.get(t.whatid).Last_Completed_Task_Date__c<t.ActivityDate)
                    {
                        acc.id = t.WhatId;
                        acc.Last_Completed_Task_Date__c = t.ActivityDate;
                        if(!UniqueIDs.contains(t.whatid)){ 
                            AccountToUpdate.add(acc);    
                            UniqueIDs.add(acc.id);
                        }
                    }
                }
                else
                { 
                    acc.id = t.WhatId; 
                    acc.Last_Completed_Task_Date__c = t.ActivityDate;
                    if(!UniqueIDs.contains(t.whatid)){
                        AccountToUpdate.add(acc); 
                        UniqueIDs.add(acc.id);
                    }
                }
            }  
        }
    }  
    
    
    if(!AccountToUpdate.isEmpty()){
        update AccountToUpdate;        
    }
    
}
//Added by Lalit
public static void CheckMonthlyCallonUpdate(List<Task> newTaskList,Map<id,Task> TriggerOldMap)
{        
    List<id> relatedAccountIds = new List<id>();
    Map<id, List<Task>> AccountTasksMap = new Map<id,List<Task>>();
    List<Account> AccountToUpdate = new List<Account>();
    for(task t : newTaskList)
    {
        if (t.Whatid!=null && String.valueOf(t.Whatid).substring(0,3)=='001'){
            if(t.ActivityDate !=TriggerOldMap.get(t.id).ActivityDate || test.isRunningTest())
            {
                relatedAccountIds.add(t.WhatId);
            }  
        }
    }  
    
    for(task ta:[select whatid,what.name,id,type from task where activitydate = THIS_MONTH and Type='sales call' and RecurrenceActivityId!=null and  whatid in: relatedAccountIds])
    {
        if(!AccountTasksMap.ContainsKey(ta.WhatId))   { 
            AccountTasksMap.put(ta.WhatId,new List<Task>());    }
        AccountTasksMap.get(ta.WhatId).add(ta);
    } 
    
    
    for(id Accountid :AccountTasksMap.keyset())
    {
        if(Accountid!=null){
            Account acc = new Account();
            acc.id = Accountid;
            if(AccountTasksMap.get(Accountid).size()>0)    {   
                acc.Monthly_Call_Indicator__c = true;   }
            else          {    
                acc.Monthly_Call_Indicator__c = false;  }
            AccountToUpdate.add(acc);
        }
    }
    if(!AccountToUpdate.isEmpty()){ 
        update AccountToUpdate;      }
}




//Added by Lalit
public static void CheckMonthlyCallonDelete(List<Task> deletedTaskList)
{
    set<id> relatedAccountIds = new set<id>();
    Map<id, List<Task>> AccountTasksMap = new Map<id,List<Task>>();
    List<Account> AccountToUpdate = new List<Account>();
    for(task t : deletedTaskList)
    { 
        if(t.WhatId!=null &&   String.valueOf(t.Whatid).substring(0,3)=='001'){                relatedAccountIds.add(t.WhatId);            }
    }  
    
    if(test.isRunningTest())
    {
        for(task ta:[select whatid,what.name,id,type from task])
        {
            if(!AccountTasksMap.ContainsKey(ta.WhatId)){AccountTasksMap.put(ta.WhatId,new List<Task>());}
            AccountTasksMap.get(ta.WhatId).add(ta);
        }
    }
    else
    {
        for(task ta:[select whatid,what.name,id,type from task where activitydate = THIS_MONTH and Type='sales call' and RecurrenceActivityId!=null and  whatid in: relatedAccountIds])
        {
            if(!AccountTasksMap.ContainsKey(ta.WhatId)){AccountTasksMap.put(ta.WhatId,new List<Task>());}
            AccountTasksMap.get(ta.WhatId).add(ta);
        }
    }
    
    for(id Accountid :AccountTasksMap.keyset())
    {
        if(Accountid!=null){
            Account acc = new Account();
            acc.id = Accountid;
            if(AccountTasksMap.get(Accountid).size()>0)    {     
                acc.Monthly_Call_Indicator__c = true;       }
            else 
            {   
                acc.Monthly_Call_Indicator__c = false;  
            }
            
            AccountToUpdate.add(acc);
        }
    }
    if(!AccountToUpdate.isEmpty()){ 
        update AccountToUpdate;        }
    
} 

//Added by Lalit
public static void CheckMonthlyCallonInsert(List<Task> newTaskList)
{
    List<id> relatedAccountIds = new List<id>();
    Map<id, List<Task>> AccountTasksMap = new Map<id,List<Task>>();
    List<Account> AccountToUpdate = new List<Account>();
    for(task t : newTaskList)
    {  
        if (t.Whatid!=null && String.valueOf(t.Whatid).substring(0,3)=='001')   {
            relatedAccountIds.add(t.WhatId);          }
    }  
    
    System.debug('relatedAccountIds'+relatedAccountIds);
    
    
    for(task ta:[select whatid,what.name,id,type from task where activitydate = THIS_MONTH and Type='sales call' and RecurrenceActivityId!=null and  whatid in: relatedAccountIds])
    {
        if(!AccountTasksMap.ContainsKey(ta.WhatId))            {  
            AccountTasksMap.put(ta.WhatId,new List<Task>());           }
        AccountTasksMap.get(ta.WhatId).add(ta);
    } 
    
    System.debug('AccountTasksMap'+AccountTasksMap);
    
    
    for(id Accountid :AccountTasksMap.keyset())
    {
        Account acc = new Account();
        acc.id = Accountid;
        if(AccountTasksMap.get(Accountid).size()>0)            {  
            acc.Monthly_Call_Indicator__c = true;            }
        else    {
            acc.Monthly_Call_Indicator__c = false;   }           AccountToUpdate.add(acc);        }
    if(!AccountToUpdate.isEmpty()){
        update AccountToUpdate;
    }
}


public static void PopulateEndOfRecurrence(Task[] TriggerNew,Map<Id, Task> TriggerOldMap){
    List<Task> tasks = new List<Task>();
    List<Account> updateAccounts = new List<Account>();
    
    for(Task t: TriggerNew){
        if(t.IsRecurrence == true && t.Type == 'Sales Call'){ 
            tasks.add(t);}
    }
    
    for(Task t:[SELECT AccountId,IsRecurrence,RecurrenceActivityId,RecurrenceEndDateOnly,RecurrenceType FROM Task WHERE RecurrenceActivityId IN:tasks AND AccountId != NULL]){
        Id i = t.AccountId;
        Account newAccount = new Account(Id = i);
        System.Debug('Account to be Modified'+ newAccount );
        if(newAccount.Recurrence_End_Date__c != t.RecurrenceEndDateOnly){
            newAccount.Recurrence_End_Date__c = t.RecurrenceEndDateOnly;
            updateAccounts.add(newAccount);
        }
    }
    
    if(!updateAccounts.isEmpty()){update updateAccounts; }
}

public static void DepopulateAccountRecussrenceEndDate(Map<Id, Task> TriggerOld){
    Map<Id, Task> AccountToTaskMap = new Map<Id, Task>();
    Set<Id> AccountIdOfTask = new Set<Id>();
    for(Task t : TriggerOld.values()){
        AccountToTaskMap.put(t.AccountId, t);
        AccountIdOfTask.add(t.AccountId);
    }  
    
    List<Account> AccountList = [SELECT Id, Recurrence_End_Date__c FROM Account WHERE Id IN: AccountIdOfTask];
    for(Account a : AccountList){
        if(AccountToTaskMap.containsKey(a.Id)){   a.Recurrence_End_Date__c = null;   }
    }
    update AccountList;
}

public static void populateNumberOfSalesCall(List<Task> taskList){
    //set that getting all the manager id of task
    Set<Id> oId = new Set<Id>();
    for(Task tsk: taskList){
        oId.add(tsk.Manager_Id__c);
    }
    if(!(System.isBatch()))
    {
    populateNumberOfSalesCall_future(oId);
    }
}


@future
public static void populateNumberOfSalesCall_future(Set<Id> manId){ 
    System.debug('@@@@@@@@@@@@@ populateNumberOfSalesCall');
    Decimal num;
    List<Task> tList = new List<Task>();
    Map<Id,Id> ownerToManager= new Map<Id, Id>();
    Map<Id, Integer> managerToCount = new Map<Id, Integer>();
    Date startOfMonth = Date.newInstance(2015,7,6);
    Date endOfMonth = Date.newInstance(2015,7,10);
    //tList = [SELECT Id, Owner.Id FROM Task WHERE Sales_Rep_Type__c = 'OSR' AND Status = 'Completed' AND RecordType.Name = 'Sales Call' AND CreatedDate = THIS_WEEK]
    for(User u : [SELECT Id, ManagerId FROM User WHERE ManagerId IN: manId]){
        if(u.ManagerId!=null){
            ownerToManager.put(u.Id, u.ManagerId); 
            if(!managerToCount.containsKey(u.ManagerId)){  managerToCount.put(u.ManagerId, 0);   }
        }
    }
    List<user> uList = new List<User>();
    for(AggregateResult ag : [SELECT count(Id) ct, OwnerId 
                              FROM Task 
                              WHERE Sales_Rep_Type__c = 'OSR' 
                              AND Status = 'Completed' 
                              AND RecordType.Name = 'Sales Call'
                              AND ActivityDate = THIS_WEEK
                              AND OwnerId IN :ownerToManager.keyset()
                              GROUP BY OwnerId]){    
                                  Id managerId = ownerToManager.get(String.valueOf(ag.get('OwnerId')));
                                  Integer total = managerToCount.get(managerId) + Integer.valueOf(ag.get('ct'));
                                  managerToCount.put(managerId , total);
                                  //System.debug('@@@@@@@@@@@@@' + u.Id + ' ' + u.Number_of_Sales_Call_of_OSR__c);      
                              }
    for(Id i:managerToCount.keySet()){
        System.debug('@@@@@@@@@@@@@' + i + ' ' +managerToCount.get(i));
        User u = new User(Id = i, Number_of_Sales_Call_of_OSR__c = managerToCount.get(i), Date_of_last_Sales_Call__c = System.today()); 
        uList.add(u);
    }
    System.debug('@@@@@@@@@@@@@' + uList.size());
    try{
        update uList;            
    } catch(Exception e){
        
    }
}

public  static void testmethodcoverage_Temp_TobeRemove()
{
    
}
public static void updateLatestFiveCompltActivitiesFlagOnUpdate(List<Task> newTaskList, Map<id, Task> TriggerOldMap){
    IsRecursive = true;
    List<Task> effTasklst1 = new List<Task>(); // enable True flag
    List<Task> effTasklst2 = new list<Task>();  // enable false flag 
    Set<id> effAccountIds = new Set<id>(); // accounts whose flag need to set
    Map<string, string> effAccountIdsmap = new Map<string, string>();
    Map<string, Integer> mapOfAccountsTask = new Map<string, Integer>();
    Set<id> effAccountIds2 = new Set<id>();// accounts whose flag need reset
    List<Task> prevtaskToupdate = new List<Task>(); 
    List<Task> prevtaskToupdate2 = new List<Task>(); 
    for (Task tsk: newTaskList) {
        Task oldTask = TriggerOldMap.get(tsk.ID);
        if(tsk.status == 'Completed'&& oldTask.Status != 'Completed') {
            tsk.LatestFiveCompltActivity__c=true; // flag may be checked for more than 5 task
            effTasklst1.add(tsk);  
        }
        else if (tsk.status != 'Completed'&& oldTask.Status == 'Completed' && tsk.LatestFiveCompltActivity__c == true){
            tsk.LatestFiveCompltActivity__c=false;
            effTasklst2.add(tsk);
        }
    }
    if (!effTasklst1.isEmpty()) {
        for(task t : effTasklst1)
        {
            if (t.Whatid!=null && String.valueOf(t.Whatid).substring(0,3)=='001'){
                // tsk.LatestFiveCompltActivity__c=true;
                effAccountIds.add(t.WhatId);
                effAccountIdsmap.put(t.id,t.WhatId);
            }  
        }
    }
    
    
    if (!effAccountIds.IsEmpty()) {
        List<AggregateResult> countTaskPerAcc = [SELECT whatId, Count(Id) TaskCount From Task where id In:effAccountIdsmap.keySet() and whatId in : effAccountIdsmap.Values() Group By whatId];
        for(AggregateResult ag : countTaskPerAcc) { 
            mapOfAccountsTask.put(String.valueOf(ag.get('whatId')), (Integer) ag.get('TaskCount'));//
            
        }
        List<Account> accWithTasklst = [Select Name, (Select id,LatestFiveCompltActivity__c from tasks where status='Completed' and LatestFiveCompltActivity__c =true order by close_date__c asc ) from account where id in : effAccountIds ];
        
        for (Account acc : accWithTasklst){
            if (acc.tasks.size() + mapOfAccountsTask.get(String.valueOf(acc.id))  > 5) {
                //system.debug('@@Rahul acc Size'+ acc.tasks.size() );
                List<Task> tupdatelst = acc.tasks;
                Integer loopSize = mapOfAccountsTask.get(String.valueOf(acc.id));
               //  system.debug('@@Rahul LoopSize ' + loopSize);
                if (loopSize > (tupdatelst.size()) )
                    loopSize = tupdatelst.size(); // to uncheck all old task
                
                for (integer i = 0 ; i < loopSize; i++ ) {
                    tupdatelst[i].LatestFiveCompltActivity__c = false;
                    prevtaskToupdate.add(tupdatelst[i]) ;
                }
                
            }
        }
            try { update prevtaskToupdate; }
          catch (Exception e){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        string[] to = new string[] {'rahul.bansal@puresoftware.com'};
        email.setToAddresses(to);
        email.setSubject('TaskTriggerHandler Exception');
        email.setHtmlBody(e.getMessage());
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        } 
    } 
    
    if (!effTasklst2.isEmpty()) {
        for(task t : effTasklst2)
        {
            if (t.Whatid!=null && String.valueOf(t.Whatid).substring(0,3)=='001'){
                
                effAccountIds2.add(t.WhatId);
                
            }  
        }
    }
    if (!effAccountIds2.IsEmpty()) {
        
        List<Account> accWithTasklst2 = [Select Name, (Select id,LatestFiveCompltActivity__c from tasks where status='Completed' and LatestFiveCompltActivity__c =false order by close_date__c desc limit 1) from account where id in : effAccountIds2 ];
        for (Account acc : accWithTasklst2){
            if (acc.tasks.size() > 0) {
                List<Task> tupdatelst = acc.tasks;
                
                for (integer i = 0 ; i<tupdatelst.size(); i++ ) {
                    tupdatelst[i].LatestFiveCompltActivity__c = true;
                    prevtaskToupdate2.add(tupdatelst[i]) ;
                }
                
            }
        }
        update prevtaskToupdate2; 
    } 
}

public static void updateLatestFiveCompltActivitiesFlagOnInsert(List<Task> newTaskList){
    System.debug('My Name is Rahul Insert');
    List<Task> effTasklst1 = new list<Task>();
    Map<Integer, string> effAccountIdsmap = new Map<Integer, string>();
    Map<string, Integer> mapOfAccountsTask = new Map<string, Integer>();
    Set<id> effAccountIds = new Set<id>();
    Integer count = 1;
    
    List<Task> prevtaskToupdate = new List<Task>(); 
    
    
    for (Task tsk: newTaskList) {
        if(tsk.status == 'Completed') {
            tsk.LatestFiveCompltActivity__c=true;
            effTasklst1.add(tsk);  
        }
    }
    if (!effTasklst1.isEmpty()) {
        for(task t : effTasklst1)
        {
            if (t.Whatid!=null && String.valueOf(t.Whatid).substring(0,3)=='001'){
                effAccountIds.add(t.WhatId);
                effAccountIdsmap.put(count,t.WhatId);
                count++;
            }  
        }
    }
    if (!effAccountIds.IsEmpty()) {
        
        for(Integer intr : effAccountIdsmap.keySet()){ 
            
            if(!mapOfAccountsTask.containsKey(effAccountIdsmap.get(intr)))
        { 
            mapOfAccountsTask.put(effAccountIdsmap.get(intr),1);
        } 
         else 
         { 
             mapOfAccountsTask.put(effAccountIdsmap.get(intr),mapOfAccountsTask.get(effAccountIdsmap.get(intr))+1);
         } 
        }  
        
        List<Account> accWithTasklst = [Select Name, (Select id,LatestFiveCompltActivity__c from tasks where status='Completed' and LatestFiveCompltActivity__c =true order by close_date__c asc) from account where id in : effAccountIds ];
       // System.debug('@Rahul '+ accWithTasklst);
        
        if (test.isRunningTest()){
        accWithTasklst = [Select Name, (Select id,LatestFiveCompltActivity__c from tasks ) from account where id in : effAccountIds ];
        }
        for (Account acc : accWithTasklst){
            IsRecursive=true;
            //System.debug('@@RAhul value ' + (acc.tasks.size() + mapOfAccountsTask.get(String.valueOf(acc.id))) );
            if ((acc.tasks.size() + mapOfAccountsTask.get(String.valueOf(acc.id)))  > 5) {
                List<Task> tupdatelst = acc.tasks;
                Integer loopSize = mapOfAccountsTask.get(String.valueOf(acc.id));
                if (loopSize > (tupdatelst.size()) )
                    loopSize = tupdatelst.size(); // to uncheck all old task
                
                for (integer i = 0 ; i < loopSize; i++ ) {
                   //  System.debug('@Rahul loopSize 2'+ loopSize);
                    tupdatelst[i].LatestFiveCompltActivity__c = false;
                    prevtaskToupdate.add(tupdatelst[i]) ;
                }
                
            }
        }
        
       try { update prevtaskToupdate; }
          catch (Exception e){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        string[] to = new string[] {'rahul.bansal@puresoftware.com'};
        email.setToAddresses(to);
        email.setSubject('TaskTriggerHandler Exception');
        email.setHtmlBody(e.getMessage());
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        } 
        
        
         }
       
    
}
}