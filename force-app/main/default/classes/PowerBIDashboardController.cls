public class PowerBIDashboardController {
    
    @AuraEnabled
    public static List<Boolean> getCstmPmsns(){
        List<Boolean> lstCstmPmsn = new List<Boolean>();
        
        Boolean Critical_C6_Dashboard_BDM = FeatureManagement.checkPermission('Critical_C6_Dashboard_BDM');
        lstCstmPmsn.add(Critical_C6_Dashboard_BDM);
        Boolean PBI_PDC_C6Dashboard = FeatureManagement.checkPermission('PBI_PDC_C6Dashboard');
        lstCstmPmsn.add(PBI_PDC_C6Dashboard);
        Boolean Critical_6_Dashboard = FeatureManagement.checkPermission('Critical_6_Dashboard');
        lstCstmPmsn.add(Critical_6_Dashboard);
        Boolean C6_Dashboard_NAM = FeatureManagement.checkPermission('C6_Dashboard_NAM');
        lstCstmPmsn.add(C6_Dashboard_NAM);
        Boolean C6_Dashboard_C2 = FeatureManagement.checkPermission('C6_Dashboard_C2');
        lstCstmPmsn.add(C6_Dashboard_C2);
        Boolean Adoption_Win_Rates = FeatureManagement.checkPermission('Adoption_Win_Rates');
        lstCstmPmsn.add(Adoption_Win_Rates);
        Boolean Focus_5_Components = FeatureManagement.checkPermission('Focus_5_Components');
        lstCstmPmsn.add(Focus_5_Components);
        Boolean Monthly_Sales_Call_Calendar = FeatureManagement.checkPermission('Monthly_Sales_Call_Calendar');
        lstCstmPmsn.add(Monthly_Sales_Call_Calendar);
        Boolean My_Week_Tasks = FeatureManagement.checkPermission('My_Week_Tasks');
        lstCstmPmsn.add(My_Week_Tasks);
        Boolean Dashbord_Snapshot = FeatureManagement.checkPermission('Dashbord_Snapshot');
        lstCstmPmsn.add(Dashbord_Snapshot);
        Boolean Calender = FeatureManagement.checkPermission('Calender');
        lstCstmPmsn.add(Calender);
        Boolean My_Task = FeatureManagement.checkPermission('My_Task');
        lstCstmPmsn.add(My_Task);
        
        return lstCstmPmsn;
    }
}