@istest
class CCPDCReportingControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();
    
    
    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }
    
    
    testmethod static void test1()
    {
        util.initCallContext();
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 01, 01);
        
        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> 
        {
            createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
            };
        insert data;
        
        
        CCPDCReportingController obj = new CCPDCReportingController();
        obj.AccountingReportsAllowed = True;
        obj.SalesReportsAllowed = true;
        obj.matrixID = '123';
    } 
    
    public static CC_FP_Contract_Account_Permission_Matrix__c createPermissions(Id accountId, Id contactId, Boolean isAdmin) {
        CC_FP_Contract_Account_Permission_Matrix__c permissions = new CC_FP_Contract_Account_Permission_Matrix__c(
            Account__c = accountId,
            Allow_Access_To_Accounting_Reports__c = true,
            Allow_Access_to_Sales_Reports__c = true,
            Allow_AR_Payments__c = true,
            Allow_Backorder_Release__c = true,
            Allow_Backorders__c = true,
            Allow_Checkout__c = true,
            Allow_Non_Free_Shipping_Orders__c = true,
            Allow_Override_ShipTo__c = true,
            Can_View_Invoices__c = true,
            Contact__c = contactId,
            Is_Admin__c = isAdmin,
            Maximum_Order_Limit__c = null,
            Pricing_Visibility__c = true             
        );
        return permissions;
    }
}