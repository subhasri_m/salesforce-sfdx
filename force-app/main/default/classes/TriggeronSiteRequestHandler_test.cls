@istest
public class TriggeronSiteRequestHandler_test {

    
    testmethod static void test1() 
    {
        
         Profile saProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User saUser = new User(Alias = 'newUser', Email='test123@email.com', 
                               EmailEncodingKey='UTF-8',
                               LastName='Testing',
                               LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',
                               ProfileId = saProfile.Id, 
                               TimeZoneSidKey='America/Los_Angeles',
                               UserName='test123123@email.com',
                               Create_Accounts__c = true,
                               Salesman_Number__c = '1-1234',
                               Region__c= 'Central');   
        insert saUser;
        
        
        
        Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = saUser.Id);  
        insert acc;
        System.assertNotEquals(null,acc.id)  ;  
        contact con = new contact (lastname = 'checkme',firstname='check',Accountid = acc.id,OwnerId=saUser.id,Email='test@test.com');    
    insert con;
    System.assertNotEquals(null,con.id);
        
        
        SiteRequestHandler__c  obj = new SiteRequestHandler__c();
        obj.CustomerName__c = con.id;
            obj.AccountID__C = acc.id;
            obj.Complaints_Query__c = 'check'; 
            obj.AccountOwner__c = acc.OwnerId;
            obj.AccountName__C = acc.name;
        insert obj;
        
    }
}