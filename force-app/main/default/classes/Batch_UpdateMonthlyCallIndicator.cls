global class Batch_UpdateMonthlyCallIndicator implements database.Batchable<sobject>, schedulable{
    
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new Batch_UpdateMonthlyCallIndicator());
    } 
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query;
        if(test.isRunningTest())
        { 
            query = 'select id,Monthly_Call_Indicator__c,recordtype.name from account where Iseries_Company_code__c =\'1\' or Iseries_Company_code__c =\'2\'';
        }
        else
        {   
            query = System.label.UpdateMonthyCallIndicatorQuery;  
        }
        System.debug('Query--> '+query );
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<Account>scope)
    {
        Map<id,integer> accountMonthlyTaskCount = new Map<id,integer>();
        Set<Id> AllAccountId = new Set<Id>();
        List<Account> AccountUpdateList = new List<Account>();
        for (Account acc: Scope)
        { 
            AllAccountId.add(acc.id);
        }
        
        List<task> thisMonthTaskList = [select whatid,what.name,id,type from task where whatid in:AllAccountId and activitydate = THIS_MONTH and Type='sales call' and RecurrenceActivityId!=null];
        
        for (id AccId:AllAccountId)
        {
            accountMonthlyTaskCount.put(Accid,0);
            for (task ta :thisMonthTaskList)
            {
                if(accountMonthlyTaskCount.containsKey(ta.whatid))
                {
                    Integer currentCount = accountMonthlyTaskCount.get(ta.whatid);
                    accountMonthlyTaskCount.put(ta.whatid,currentCount+1);
                } 
            }
        } 
        
        for(id mapAccId : accountMonthlyTaskCount.Keyset())
        {
            Account acc = new Account();
            acc.id = mapAccId; 
            if(accountMonthlyTaskCount.get(mapAccId)>0)
            {
                acc.Monthly_Call_Indicator__c = true; 
            }
            else
            {
                acc.Monthly_Call_Indicator__c = false;
            }
            AccountUpdateList.add(acc);
        } 
        update AccountUpdateList; 
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
}