global class CCPDCLogicProductPricing extends ccrz.ccLogicProductPricing {
    
    global override Map<String, Object> process(Map<String, Object> inputData) {
        ccrz.ccLog.log('******************CCAviLogicProductPricing process***************************');
        ccrz.ccLog.log(System.LoggingLevel.DEBUG, 'M:E getFPProductInventories ', ccrz.cc_CallContext.currPageParameters);
        if(ccrz.cc_CallContext.storefront == 'parts' && ccrz.cc_CallContext.isGuest) {
            Map<String, Object> outputData = super.process(inputData);
            return outputData;
        }
        if (ccrz.cc_CallContext.currPageName == 'Products' || ccrz.cc_CallContext.currPageName == 'ccrz__Cart' )
        { CCPDCCallContext.skipPricing = true;
        }// CHG0030315 : ccrz.cc_CallContext.currPageName == 'ccrz__CheckoutNew'
        if(ccrz.cc_CallContext.storefront == 'parts' && ccrz.cc_CallContext.currPageName == 'ProductDetails') {
          CCPDCCallContext.skipPricing = true; // skip pricing on FP PDP page as we are doing a pricing call from the controller 
        }
        String accId = ccrz.cc_CallContext.effAccountId;

        Map<String, Object> outputData = super.process(inputData);
        outputData = price(outputData,accId);
      
        ccrz.ccLog.log('****************** end CCAviLogicProductPricing process***************************');
        return outputData;
    }
    
    private Map<String, Object> price(Map<String, Object> inputData,String accId) {
        List<Map<String,Object>> prodList = (List<Map<String,Object>>)inputData.get(ccrz.ccApiProduct.PRODUCTLIST);
        List<Map<String,Object>> boomiProd = new List<Map<String,Object>>();
        List<String> updatePriceItemId= new List<String>();
        Map<String,Map<String, Decimal>> localPriceMap = new Map<String,Map<String, Decimal>>();
        
        CCAviBoomiSettings__c settings =  CCAviBoomiSettings__c.getInstance((String) inputData.get('ccrz__Storefront__c'));
        Boolean boomiAllProducts = settings.Enable_Boomi_On_All__c;
        
        Id contactId = String.valueOf(ccrz.cc_CallContext.currContact.Id);
        List<CC_FP_Contract_Account_Permission_Matrix__c> matrixList = CCFPContactAccountPermissionMatrixDAO.getAccountsForContact(contactId);
        System.debug('contactId ' + contactId);
        System.debug('prodList ' +prodList);
        
        if (prodList != null && !prodList.isEmpty() && !CCPDCCallContext.skipPricing && ccrz.cc_CallContext.currPageKey != 'ccrz__Products') {
            Map<String, Decimal> priceMap = new Map<String, Decimal>();
            List<String> productIds = new List<String>();
            for (Map<String,Object> obj: prodList) {
                productIds.add(String.valueOf(obj.get('sfid')));
                
            }
            
            // Code that filter out the Enterprise price that is not take into account of as local price
            Account acc = [SELECT Id, ccrz__E_AccountGroup__c FROM Account WHERE Id =:accId];
            String accGrpId = String.valueOf(acc.ccrz__E_AccountGroup__c);
            List<String> priceListId = new List<String>();
            Map<String,Set<String>> pricelistItemsChecker = new Map<String,Set<String>>();
            List<ccrz__E_AccountGroupPriceList__c> accGrpPriceList = [SELECT Id, ccrz__Pricelist__c FROM ccrz__E_AccountGroupPriceList__c WHERE ccrz__AccountGroup__c =:accGrpId];
            for(ccrz__E_AccountGroupPriceList__c accGrpPrice : accGrpPriceList){
                priceListId.add(String.valueOf(accGrpPrice.ccrz__Pricelist__c ));    
            }
            
            List<ccrz__E_PriceListItem__c> pricelistItems = [SELECT Id,ccrz__Product__c,ccrz__Pricelist__r.name,ccrz__Price__c, ccrz__Product__r.Part_Number__c, ccrz__Product__r.Pool_Number__c FROM ccrz__E_PriceListItem__c WHERE ccrz__Product__c IN: productIds AND ccrz__Pricelist__c IN:priceListId];
            //ccrz.ccLog.log('CCPDCLogicCartPrice pricelistItem:23432423 ' + pricelistItems);
            
            for(ccrz__E_PriceListItem__c pricelistItem : pricelistItems){
                if(!pricelistItemsChecker.isEmpty() && pricelistItemsChecker.containsKey(pricelistItem.ccrz__Product__c)){   
                    Set<String> plnames = pricelistItemsChecker.get(pricelistItem.ccrz__Product__c); 
                    plnames.add(pricelistItem.ccrz__Pricelist__r.name);
                    
                }else{
                    Set<String> plnames =new Set<String>();
                    plnames.add(pricelistItem.ccrz__Pricelist__r.name);
                    pricelistItemsChecker.put(pricelistItem.ccrz__Product__c,plnames);
                }
                
                String prKey = String.valueOf(pricelistItem.ccrz__Product__r.Pool_Number__c) + String.valueOf(pricelistItem.ccrz__Product__r.Part_Number__c);
                if(pricelistItem.ccrz__Pricelist__r.name == 'Enterprise'){
                    if(!localPriceMap.isEmpty() && localPriceMap.containsKey(prKey)){
                        (localPriceMap.get(prKey)).put('Enterprise',pricelistItem.ccrz__Price__c);
                    }
                    else if(pricelistItem.ccrz__Price__c > 0){
                        localPriceMap.put(prKey,new Map<String,Decimal>{'Enterprise' => pricelistItem.ccrz__Price__c});
                    }
                } else {
                    if(!localPriceMap.isEmpty() && localPriceMap.containsKey(prKey)){
                        Map<String, Decimal> currentProdPricing = localPriceMap.get(prKey); 
                        if(currentProdPricing.containsKey('BestPrice') && currentProdPricing.get('BestPrice') > pricelistItem.ccrz__Price__c){ 
                            currentProdPricing.put('BestPrice',pricelistItem.ccrz__Price__c);
                            
                        }else if(!currentProdPricing.containsKey('BestPrice')){  
                            currentProdPricing.put('BestPrice',pricelistItem.ccrz__Price__c);
                        }
                        
                    }else{
                        localPriceMap.put(prKey,new Map<String,Decimal>{
                            'BestPrice'=>pricelistItem.ccrz__Price__c
                                }
                                         );
                    }
                }
            }
            
            for(Map<String,Object> prod : prodList){
                Set<String> plnames = pricelistItemsChecker.get(String.valueOf(prod.get('sfid')));
                List<String> newplnames = new List<String>(plnames);
                if((newplnames.size() == 1 && newplnames[0] == 'Enterprise')||boomiAllProducts){
                    boomiProd.add(prod);
                }
            }
            
            CCAviBoomiAPI.SeriesPricingWSResponse response; 
            //ccrz.ccLog.log('#### boomiProd '+boomiProd);
            if(boomiProd!=null && !boomiProd.isEmpty()){
                response = CCPDCPricingHelper.price(boomiProd);
                System.debug('response==>'+response);
                if (response != null && response.success && response.listPricing != null) {
                    priceMap = CCPDCPricingHelper.createPriceMap(response);
                }
            }
            for (Map<String,Object> obj: prodList) {
                String partNumber = (String) obj.get('partNumber');
                String poolNumber = (String) obj.get('poolNumber');
                String key = poolNumber + partNumber;
                if (priceMap != null && !priceMap.isEmpty()) {
                    
                    if (partNumber != null && poolNumber != null) {
                        Decimal price = priceMap.get(key);
                        if (price != null && price != 10000 && response.success && !matrixList.isEmpty()){

                            //if(ccrz.cc_CallContext.storefront == 'pdc') {
                            obj.put('basePrice', obj.get('price'));
                            //}
                            obj.put('price', price);
                            //ccrz.ccLog.log('####  obj  '+obj);
                            obj.put('CCFPPricingFailed', false);
                            if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                            updatePriceItemId.add((String)obj.get('sfid'));
                        }
                        else {
                            if(!matrixList.isEmpty()){
                                obj.put('CCFPPricingFailed', true);
                                if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);    
                                
                                ccrz.ccLog.log('#### price condition 2');
                                if(!localPriceMap.isEmpty() && localPriceMap.containsKey(key)){
                                    Map<String,Decimal> localprice = localPriceMap.get(key);
                                    if(localprice.containsKey('BestPrice')){ 
                                        obj.put('price', localprice.get('BestPrice'));
                                        obj.put('CCFPPricingFailed', false);
                                        if(matrixList!=null && matrixList.size() > 0) obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                                    }else if(localprice.containsKey('Enterprise')){
                                        obj.put('price', localprice.get('Enterprise'));
                                        obj.put('CCFPPricingFailed', true); 
                                        if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                                    }else {
                                        obj.put('price', 10000); obj.put('CCFPPricingFailed', true);
                                        if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                                    }
                                }
                            }else {
                                obj.put('CCFPPricingFailed', true); 
                                if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',false); 
                            }
                        }
                    }
                    else {
                        ccrz.ccLog.log('#### price condition 3');
                        obj.put('price', 10000); obj.put('CCFPPricingFailed', true);
                        if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                    }
                    
                    
                }else if(!localPriceMap.isEmpty() && localPriceMap.containsKey(key)){
                    Map<String,Decimal> localprice = localPriceMap.get(key);
                    if(localprice.containsKey('BestPrice')){
                        obj.put('price', localprice.get('BestPrice')); 
                        obj.put('CCFPPricingFailed', false); 
                        if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                    }else if(localprice.containsKey('Enterprise')){
                        obj.put('price', localprice.get('Enterprise'));
                        obj.put('CCFPPricingFailed', true); 
                        if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                    }else {
                        obj.put('price', 10000); obj.put('CCFPPricingFailed', true);
                        if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                    }
                    
                }
                else {
                    ccrz.ccLog.log('#### price 4');
                    
                    obj.put('CCFPPricingFailed', true);
                    if(matrixList!=null && matrixList.size() > 0)obj.put('CC_FPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                }
            }
        }
        ccrz.ccLog.log('inputData pricing:'+prodList);
        ccrz.ccLog.log(' inputData ', inputData);
        return inputData;
    }
    
    public static void dummyMethod(){
        ccrz.ccLog.log('dummyMethod');
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod');
          ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
        ccrz.ccLog.log('dummyMethod'); 
    }
    
}