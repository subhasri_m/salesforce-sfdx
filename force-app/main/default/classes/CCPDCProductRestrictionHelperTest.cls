@isTest
public class CCPDCProductRestrictionHelperTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
             Map<String, Object> m = util.initData();
        }
    }
    static testmethod void testCountryRestriction() {
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        
        Product_Restriction__c productRestriction;

        // restricted in Canada
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restriction_Area_Type__c = 'Country',
            Restriction_Country__c = 'CA'
        );
        insert productRestriction;

        // restricted in US
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restriction_Area_Type__c = 'Country',
            Restriction_Country__c = 'US'
        );
        insert productRestriction;

        // restricted in US, un-restricted in state IL
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restrcition_Type__c = 'Un-Restricted', 
            Restriction_Area_Type__c = 'State',
            Restriction_Country__c = 'US',
            Restriction_State__c = 'IL'
        );
        insert productRestriction;

        // restricted in US, un-restricted in zip 20904
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restrcition_Type__c = 'Un-Restricted', 
            Restriction_Area_Type__c = 'ZipCode',
            Restriction_Country__c = 'US',
            Restriction_ZipCode__c = '20904'
        );
        insert productRestriction;

        // restricted in US, un-restricted in city Baltimore
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restrcition_Type__c = 'Un-Restricted', 
            Restriction_Area_Type__c = 'City',
            Restriction_Country__c = 'US',
            Restriction_State__c = 'MD',
            Restriction_City__c = 'Baltimore'
        );
        insert productRestriction;

        // restricted in US, un-restricted in county Bronx
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restrcition_Type__c = 'Un-Restricted', 
            Restriction_Area_Type__c = 'County',
            Restriction_Country__c = 'US',
            Restriction_State__c = 'NY',
            Restriction_County__c = 'Bronx'
        );
        insert productRestriction;

        Test.startTest();
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult1 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Ottawa', 'ON', 'K1A 0A2', 'CA', null);
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult2 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Gaithersburg', 'MD', '20877', 'US', 'Montgomery');
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult3 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Chicago', 'IL', '30000', 'US', null);
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult4 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Silver Spring', 'MD', '20904', 'US', 'Silver Spring');
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult5 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Baltimore', 'MD', '2000', 'US', null);
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult6 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'New York', 'NY', '10453', 'US', 'Bronx');

        CCPDCProductRestrictionHelper.SingleProductRestrictionResult productRestrictionResult = CCPDCProductRestrictionHelper.getProductRestriction(cartItem.ccrz__Product__c);
      
        Test.stopTest();
        System.assert(restrictionResult1.restricted);
        System.assert(restrictionResult2.restricted);
        System.assert(!restrictionResult3.restricted);
        System.assert(!restrictionResult4.restricted);
        System.assert(!restrictionResult5.restricted);
        System.assert(!restrictionResult6.restricted);

        System.assert(productRestrictionResult.hasRestrictions);
        System.assertEquals(2, productRestrictionResult.countryRestrictions.size());
        System.assertEquals(0, productRestrictionResult.stateRestrictions.size());
        System.assertEquals(0, productRestrictionResult.zipRestrictions.size());
        System.assertEquals(0, productRestrictionResult.countyRestrictions.size());
        System.assertEquals(0, productRestrictionResult.cityRestrictions.size());
    }

    static testmethod void testStateRestriction() {
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        
        Product_Restriction__c productRestriction;

        // restricted in state MD
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restriction_Area_Type__c = 'State',
            Restriction_Country__c = 'US',
            Restriction_State__c = 'MD'
        );
        insert productRestriction;

        // restricted in US, un-restricted in city Baltimore
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restrcition_Type__c = 'Un-Restricted', 
            Restriction_Area_Type__c = 'City',
            Restriction_Country__c = 'US',
            Restriction_State__c = 'MD',
            Restriction_City__c = 'Baltimore'
        );
        insert productRestriction;

        // restricted in US, un-restricted in county Montgomery
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restrcition_Type__c = 'Un-Restricted', 
            Restriction_Area_Type__c = 'County',
            Restriction_Country__c = 'US',
            Restriction_State__c = 'MD',
            Restriction_County__c = 'Montgomery'
        );
        insert productRestriction;

        Test.startTest();
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult1 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Silver Spring', 'MD', '20904', 'US', 'Silver Spring');
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult2 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Baltimore', 'MD', '2000', 'US', null);
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult3 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Gaithersburg', 'MD', '20877', 'US', 'Montgomery');

        CCPDCProductRestrictionHelper.SingleProductRestrictionResult productRestrictionResult = CCPDCProductRestrictionHelper.getProductRestriction(cartItem.ccrz__Product__c);
      
        Test.stopTest();
        System.assert(restrictionResult1.restricted);
        System.assert(!restrictionResult2.restricted);
        System.assert(!restrictionResult3.restricted);

        System.assert(productRestrictionResult.hasRestrictions);
        System.assertEquals(0, productRestrictionResult.countryRestrictions.size());
        System.assertEquals(1, productRestrictionResult.stateRestrictions.size());
        System.assertEquals(0, productRestrictionResult.zipRestrictions.size());
        System.assertEquals(0, productRestrictionResult.countyRestrictions.size());
        System.assertEquals(0, productRestrictionResult.cityRestrictions.size());
    }

    static testmethod void testZipRestriction() {
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        
        Product_Restriction__c productRestriction;

        // restricted in US, un-restricted in zip 20904
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true,  
            Restriction_Area_Type__c = 'ZipCode',
            Restriction_Country__c = 'US',
            Restriction_ZipCode__c = '20904'
        );
        insert productRestriction;

        Test.startTest();
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult1 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Silver Spring', 'MD', '20904', 'US', 'Silver Spring');
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult2 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Gaithersburg', 'MD', '20877', 'US', 'Montgomery');

        CCPDCProductRestrictionHelper.SingleProductRestrictionResult productRestrictionResult = CCPDCProductRestrictionHelper.getProductRestriction(cartItem.ccrz__Product__c);
      
        Test.stopTest();
        System.assert(restrictionResult1.restricted);
        System.assert(!restrictionResult2.restricted);

        System.assert(productRestrictionResult.hasRestrictions);
        System.assertEquals(0, productRestrictionResult.countryRestrictions.size());
        System.assertEquals(0, productRestrictionResult.stateRestrictions.size());
        System.assertEquals(1, productRestrictionResult.zipRestrictions.size());
        System.assertEquals(0, productRestrictionResult.countyRestrictions.size());
        System.assertEquals(0, productRestrictionResult.cityRestrictions.size());
    }

    static testmethod void testCityRestriction() {
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        
        Product_Restriction__c productRestriction;


        // restricted in city Baltimore
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restriction_Area_Type__c = 'City',
            Restriction_Country__c = 'US',
            Restriction_State__c = 'MD',
            Restriction_City__c = 'Baltimore'
        );
        insert productRestriction;

        Test.startTest();
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult1 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Baltimore', 'MD', '2000', 'US', null);
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult2 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Silver Spring', 'MD', '20904', 'US', 'Silver Spring');

        CCPDCProductRestrictionHelper.SingleProductRestrictionResult productRestrictionResult = CCPDCProductRestrictionHelper.getProductRestriction(cartItem.ccrz__Product__c);
      
        Test.stopTest();
        System.assert(restrictionResult1.restricted);
        System.assert(!restrictionResult2.restricted);

        System.assert(productRestrictionResult.hasRestrictions);
        System.assertEquals(0, productRestrictionResult.countryRestrictions.size());
        System.assertEquals(0, productRestrictionResult.stateRestrictions.size());
        System.assertEquals(0, productRestrictionResult.zipRestrictions.size());
        System.assertEquals(0, productRestrictionResult.countyRestrictions.size());
        System.assertEquals(1, productRestrictionResult.cityRestrictions.size());
    }
    
    static testmethod void testCountyRestriction() {
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        
        Product_Restriction__c productRestriction;

        // restricted in county Montgomery
        productRestriction = new Product_Restriction__c(
            Related_to_Product__c = cartItem.ccrz__Product__c, 
            Restriction_Status__c = true, 
            Restriction_Area_Type__c = 'County',
            Restriction_Country__c = 'US',
            Restriction_State__c = 'MD',
            Restriction_County__c = 'Montgomery'
        );
        insert productRestriction;

        Test.startTest();
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult1 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Gaithersburg', 'MD', '20877', 'US', 'Montgomery');
        CCPDCProductRestrictionHelper.ProductRestrictionResult restrictionResult2 = CCPDCProductRestrictionHelper.checkProductRestrictions(cart.ccrz__EncryptedId__c, 'Silver Spring', 'MD', '20904', 'US', 'Silver Spring');

        CCPDCProductRestrictionHelper.SingleProductRestrictionResult productRestrictionResult = CCPDCProductRestrictionHelper.getProductRestriction(cartItem.ccrz__Product__c);
        Map<Id, CCPDCProductRestrictionHelper.SingleProductRestrictionResult> mapProductRestrictionResult = CCPDCProductRestrictionHelper.getProductsRestrictionMap(new String[]{cartItem.ccrz__Product__c});
      
        Test.stopTest();
        System.assert(restrictionResult1.restricted);
        System.assert(!restrictionResult2.restricted);

        System.assert(productRestrictionResult.hasRestrictions);
        System.assertEquals(0, productRestrictionResult.countryRestrictions.size());
        System.assertEquals(0, productRestrictionResult.stateRestrictions.size());
        System.assertEquals(0, productRestrictionResult.zipRestrictions.size());
        System.assertEquals(1, productRestrictionResult.countyRestrictions.size());
        System.assertEquals(0, productRestrictionResult.cityRestrictions.size());
    }
    
    static testmethod void testNoRestriction() {
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c cartItem = util.getCartItem();

        Test.startTest();
        Map<Id, CCPDCProductRestrictionHelper.SingleProductRestrictionResult> mapProductRestrictionResult = CCPDCProductRestrictionHelper.getProductsRestrictionMap(new String[]{cartItem.ccrz__Product__c});
      
        Test.stopTest();

        System.assertEquals(1, mapProductRestrictionResult.size());
        CCPDCProductRestrictionHelper.SingleProductRestrictionResult productRestrictionResult = mapProductRestrictionResult.values()[0];
        System.assert(!productRestrictionResult.hasRestrictions);
        System.assertEquals(0, productRestrictionResult.countryRestrictions.size());
        System.assertEquals(0, productRestrictionResult.stateRestrictions.size());
        System.assertEquals(0, productRestrictionResult.zipRestrictions.size());
        System.assertEquals(0, productRestrictionResult.countyRestrictions.size());
        System.assertEquals(0, productRestrictionResult.cityRestrictions.size());
    }
    
}