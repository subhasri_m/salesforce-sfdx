public with sharing class CCPDCCartManager {

    public static Map<String,Object> addTo(Map<String, Object> inputData) {
            Map<String,Object> outputData = null;
            Map<String, Decimal> priceMap = null;
            Map<String, Decimal> requiredPricingMap = null;

            List<Object> data = (List<Object>) inputData.get(ccrz.ccApiCart.LINE_DATA);
            if (data != null && !data.isEmpty()) {

                Map<String, Map<String, Decimal>> quantityMap = CCPDCPricingHelper.getQuantityMap(data);
                requiredPricingMap = getRequiredProducts(quantityMap);
                if(requiredPricingMap != null && !requiredPricingMap.isEmpty()){
                    List<Object> newLines = new List<ccrz.ccApiCart.LineData>();
                    for(String key: requiredPricingMap.keySet()){
                        ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
                        newLine.sku = key;
                        newLine.quantity = requiredPricingMap.get(key);
                        newLines.add(newLine);
                    }
                    CCAviBoomiAPI.SeriesPricingWSResponse response = CCPDCPricingHelper.price(newLines);
                    ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','CCPDCCartManager:response'+response);
                    if (response != null && response.success && response.listPricing != null) {
                        priceMap = CCPDCPricingHelper.createPriceMap(response);
                        //CCPDCCallContext.priceMap = priceMap;
                    }
                }

                //CCAviBoomiAPI.SeriesPricingWSResponse response = CCPDCPricingHelper.price(data, quantityMap);
                //if (response != null && response.success && response.listPricing != null) {
                //    priceMap = CCPDCPricingHelper.createPriceMap(response);
                //    //CCPDCCallContext.priceMap = priceMap;
                //}
            }
            ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','CCPDCCartManager:addTo'+requiredPricingMap);
        	outputData = ccrz.ccAPICart.addTo(inputData);
            Boolean isSuccess = (Boolean) outputData.get(ccrz.ccAPI.SUCCESS);
            if (isSuccess && requiredPricingMap != null && !requiredPricingMap.isEmpty()) {
                String cartId = (String)outputData.get(ccrz.ccApiCart.CART_ENCID);
                updateRequiredProducts(cartId, priceMap);
            }
            return outputData;
    }

    public static Map<String,Object> addTo(Map<String, Object> inputData, Boolean doPriceCall) {
        Map<String,Object> outputData = null;
        Map<String, Decimal> priceMap = null;
        Map<String, Decimal> requiredPricingMap = null;

        List<Object> data = (List<Object>) inputData.get(ccrz.ccApiCart.LINE_DATA);
        if (data != null && !data.isEmpty() && doPriceCall) {

            Map<String, Map<String, Decimal>> quantityMap = CCPDCPricingHelper.getQuantityMap(data);
            requiredPricingMap = getRequiredProducts(quantityMap);

            CCAviBoomiAPI.SeriesPricingWSResponse response = CCPDCPricingHelper.price(data, quantityMap);
            if (response != null && response.success && response.listPricing != null) {
                priceMap = CCPDCPricingHelper.createPriceMap(response);
                //CCPDCCallContext.priceMap = priceMap;
            }
        }
  
        outputData = ccrz.ccAPICart.addTo(inputData);
        Boolean isSuccess = (Boolean) outputData.get(ccrz.ccAPI.SUCCESS);
        if (isSuccess && requiredPricingMap != null && !requiredPricingMap.isEmpty()) {
            String cartId = (String)outputData.get(ccrz.ccApiCart.CART_ENCID);
            if(doPriceCall){
                updateRequiredProducts(cartId, requiredPricingMap);
            }
        }
        return outputData;
    }

    public static void updateRequiredProducts(String cartId, Map<String, Decimal> priceMap) {
        //ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','CCPDCCartManager:requiredItemMap'+priceMap);
        ccrz__E_Cart__c cart = CCPDCCartDAO.getCartForPricing(cartId);
        List<CC_FP_Required_Cart_Item__c> requiredItems = new List<CC_FP_Required_Cart_Item__c>();
        List<ccrz__E_CartItem__c> updatedItems = new List<ccrz__E_CartItem__c>();
        if (cart != null && cart.ccrz__E_CartItems__r != null && !cart.ccrz__E_CartItems__r.isEmpty()) {
            Set<String> productIds = new Set<String>();
            for (ccrz__E_CartItem__c item : cart.ccrz__E_CartItems__r) {
                productIds.add(item.ccrz__Product__c);
            }
            List<ccrz__E_RelatedProduct__c> required = CCPDCProductDAO.getRequiredProductsById(productIds);
            if (required != null && !required.isEmpty()) {
                Map<String, Map<String, ccrz__E_RelatedProduct__c>> requiredMap = new Map<String, Map<String, ccrz__E_RelatedProduct__c>>();
                for (ccrz__E_RelatedProduct__c r : required) {
                    Map<String, ccrz__E_RelatedProduct__c> relatedMap = requiredMap.get(r.ccrz__Product__r.ccrz__SKU__c);
                    if (relatedMap == null) {
                        relatedMap = new Map<String, ccrz__E_RelatedProduct__c>();
                        requiredMap.put(r.ccrz__Product__r.ccrz__SKU__c, relatedMap);
                    }
                    relatedMap.put(r.ccrz__RelatedProduct__r.ccrz__SKU__c, r);
                }
                Map<Id, Map<String, CC_FP_Required_Cart_Item__c>> requiredItemMap = new Map<Id, Map<String, CC_FP_Required_Cart_Item__c>>();
                if (cart.CC_FP_Required_Cart_Items__r != null && !cart.CC_FP_Required_Cart_Items__r.isEmpty()) {
                    for (CC_FP_Required_Cart_Item__c item : cart.CC_FP_Required_Cart_Items__r) {
                        Map<String, CC_FP_Required_Cart_Item__c> requiredSkuMap = requiredItemMap.get(item.CC_Parent_Cart_Item__c);
                        if (requiredSkuMap == null) {
                            requiredSkuMap = new Map<String, CC_FP_Required_Cart_Item__c>();
                            requiredItemMap.put(item.CC_Parent_Cart_Item__c, requiredSkuMap);
                        }
                        requiredSkuMap.put(item.CC_Product__r.ccrz__SKU__c, item);
                    }
                }

     
                for (ccrz__E_CartItem__c item : cart.ccrz__E_CartItems__r) {
                    Map<String, ccrz__E_RelatedProduct__c> prodList = requiredMap.get(item.ccrz__Product__r.ccrz__SKU__c);
                    Decimal basePrice = 0;
                    //if(item.ccrz__Price__c > item.ccrz__OriginalItemPrice__c && item.ccrz__OriginalItemPrice__c != 0)
                    //    basePrice = item.ccrz__OriginalItemPrice__c;
                    //else 
                        basePrice = item.ccrz__Price__c;
                    Decimal originalPrice = item.ccrz__OriginalItemPrice__c;
                    Decimal totalPrice = basePrice;
                    ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','CCPDCCartManager:basePrice'+basePrice);
                    ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','CCPDCCartManager:originalPrice'+originalPrice);
                    if (prodList != null && !prodList.isEmpty()) {
                        Map<String, CC_FP_Required_Cart_Item__c> requiredList = requiredItemMap.get(item.Id);
                        for (ccrz__E_RelatedProduct__c p : prodList.values()) {
                            CC_FP_Required_Cart_Item__c ritem  = null;
                            if (requiredList != null) {
                                ritem = requiredList.get(p.ccrz__RelatedProduct__r.ccrz__SKU__c);
                            }
                            if (ritem == null) {

                                ritem = new CC_FP_Required_Cart_Item__c(
                                    CC_Cart__c = item.ccrz__Cart__c,
                                    CC_Product__c = p.ccrz__RelatedProduct__c,
                                    CC_Parent_Cart_Item__c = item.Id,
                                    Quantity__c = item.ccrz__Quantity__c
                                    //Price__c = 99.9
                                );
                            }
                            else {
                                ritem.Quantity__c = item.ccrz__Quantity__c;
                            }
                            if (priceMap != null) {
                                
                                
                                String key = p.ccrz__RelatedProduct__r.Pool_Number__c + p.ccrz__RelatedProduct__r.Part_Number__c;
                                Decimal price = priceMap.get(key);
                                if (price != null) {
                                    ritem.Price__c = price;
                                    if (ritem.Quantity__c != null) {
                                        ritem.SubAmount__c = ritem.Price__c * ritem.Quantity__c;
                                    }
                                }
                                if (ritem.SubAmount__c != null) {
                                    ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','CCPDCCartManager:ritem.Price__c'+ritem.Price__c);
                                    if(originalPrice + ritem.Price__c != basePrice)
                                    totalPrice += ritem.Price__c;
                                }
                            }
                            requiredItems.add(ritem);

                        }
                        item.ccrz__Price__c = totalPrice;
                        ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','CCPDCCartManager:totalPrice2'+totalPrice);
                        item.CC_FP_Service_Price__c = basePrice;
                        item.ccrz__SubAmount__c = item.ccrz__Price__c * item.ccrz__Quantity__c;
                        updatedItems.add(item);
                    }
                }
            }
        }
        if (requiredItems != null && !requiredItems.isEmpty()) {
            upsert requiredItems;
            update updatedItems;
        }
    }

    public static Map<String, Decimal> getRequiredProducts(Map<String, Map<String, Decimal>> quantityMap) {
        Map<String, Decimal> requiredMap = new Map<String, Decimal>();

        Map<String, Decimal> quantitySkuMap = quantityMap.get('sku');

        if (quantitySkuMap != null && !quantitySkuMap.isEmpty()) {
            List<ccrz__E_RelatedProduct__c> required = CCPDCProductDAO.getRequiredProductsBySku(quantitySkuMap.keySet());
            if (required != null && !required.isEmpty()) {
                for (ccrz__E_RelatedProduct__c r : required) {
                    Decimal quantity = quantitySkuMap.get(r.ccrz__Product__r.ccrz__SKU__c);
                    if (quantity == null) {
                        quantity = 1;
                    }
                    requiredMap.put(r.ccrz__RelatedProduct__r.ccrz__SKU__c, quantity);
                }
            }
            quantitySkuMap.putAll(requiredMap);
        }

        return requiredMap;
    }

}