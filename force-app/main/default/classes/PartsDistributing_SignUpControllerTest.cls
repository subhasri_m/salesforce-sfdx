@isTest
public class PartsDistributing_SignUpControllerTest {
    @testSetup
    public static void generateTestData(){
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama11.com',
            Username = 'puser000@amamama11.com' + System.currentTimeMillis(),
            CompanyName = 'TEST_SEDE',
            Title = 'title_test',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Create_Accounts__c = true,
            LocaleSidKey = 'en_US'
        );
        insert u;
        System.runAs(u){
            
        }
    }
    
    /*public static testMethod void PDCSignUpWithoutParameters(){
        //Test.startTest();
	        Test.setCurrentPage(Page.PartsDistributing_SignUp);	
            Test.StartTest();
        	
        		Test.setMock(HttpCalloutMock.class, new PartsDistributing_SignUpMockTest());
                PartsDistributing_SignUpController controller =  new PartsDistributing_SignUpMockTest();
                controller.doSubscribe();
            Test.StopTest();
        //Test.stopTest();
    }*/
    public static testMethod void PDCSignUpWithParameters(){
        Test.startTest();
        	Test.setCurrentPage(Page.PartsDistributing_SignUp);
        	Test.setMock(HttpCalloutMock.class, new PartsDistributing_SignUpMockTest());
        	PageReference pageRef = Page.PartsDistributing_SignUp;
        	pageRef.getParameters().put('isSuccess','true'); 
            pageRef.getParameters().put('Details','Your merge fields were invalid.');
        	PartsDistributing_SignUpController controller =  new PartsDistributing_SignUpController();
            controller.doSubscribe();
        Test.stopTest();
    }
}