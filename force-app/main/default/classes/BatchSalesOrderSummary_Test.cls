@isTest
private class BatchSalesOrderSummary_Test {

    static testmethod void test() {
        
       // CommonContextDataPrepareTest.prepareData();

        Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        System.schedule('BatchSalesOrderSummaryScheduler', CRON_EXP, new BatchSalesOrderSummaryScheduler());
        
        Period pd = CommonUtil.getFiscalYearPeriod(CommonUtil.getTheSameDayLastYear(Date.today()), 'Year');
        Date sinceDate = pd.startDate; 
        Date toDate = pd.endDate;
        
        Database.executeBatch(new BatchSalesTargetSyncWithLastYear(sinceDate, toDate), 50);
        
        
        //Account acc = [select id from Account limit 1];
         User user2sos = new User(alias = 'ceo', email='admin2sos@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-888880',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTas2sos@testorg.com', profileid = '00e400000013ttZ', Create_Accounts__c = true);
        user2sos.Growth_Rate_Q1__c = 10;
        user2sos.Growth_Rate_Q2__c = 10;
        user2sos.Growth_Rate_Q3__c = 10;
        user2sos.Growth_Rate_Q4__c = 10;
        insert user2sos;
        
        User user1sos = new User(alias = 'ceo', email='admisosn@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-888881',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTassos@testorg.com', profileid = '00e400000013ttZ', Create_Accounts__c = true);
        user1sos.Growth_Rate_Q1__c = 10;
        user1sos.Growth_Rate_Q2__c = 10;
        user1sos.Growth_Rate_Q3__c = 10;
        user1sos.Growth_Rate_Q4__c = 10;
        insert user1sos;
        
        
        system.runas(user2sos){
        CommonContextDataPrepareTest.prepareData();
        Account acc = [select id from Account limit 1];
        acc.ownerid = user1sos.id;
        
        update acc;
        
        Database.executeBatch(new BatchSalesTargetRegen());
        
        Sales_Order_Summary__c sos = [select id from Sales_Order_Summary__c limit 1];
        Database.executeBatch(new BatchSalesTargetRegen(sos.id));
        
        Database.executeBatch(new BatchSalesTargetSyncWithLastYear(sinceDate, toDate), 50);
        
         }
        Test.stopTest();
       
        // System.assertEquals(i, 0);

    }

}