public class CCPDCHeaderBIEController {

    public String HolidayMessage{get;set;}
    public boolean displayPopup {get; set;}
    public boolean displayBanner {get; set;}
 	Public Static boolean MessageDisplayed = false;
     public String storeFront {get;set;}
	public CCPDCHeaderBIEController()
    {
        storeFront = ccrz.cc_CallContext.storefront;
        if (ccrz.cc_CallContext.storefront != Label.FP_StoreName){
         displayBanner = Boolean.valueOf(Label.CCPDCHeaderBanner); //pdc covid banner   
        }
        
        List<PDC_Holiday__c> HolidayList = [select id,name,Closure_Reason__c,Description__c,Notes__c ,Area__c from PDC_Holiday__c where PDC_Closed_Date__c =today limit 1];
        if(!HolidayList.isEmpty() && MessageDisplayed==false && ccrz.cc_CallContext.storefront != Label.FP_StoreName)
        {
            HolidayMessage=returnNotificationMessage(HolidayList[0].Closure_Reason__c,HolidayList[0].Description__c,HolidayList[0].Area__C); 
            MessageDisplayed = true;
        }
    }
    
     private string returnNotificationMessage(String ClosureReason,String description, string Area)
    {
        String Message;
        
        if(ClosureReason=='Holiday')
        {
           message='Thank you for visiting partsdistributing.com! PDC’s Main Office is closed on '+date.today().format()+' for '+description+'. Any questions or concerns can be directed to pdcsales@partsdistributing.com';

        }
        
        else if(ClosureReason=='Inclement Weather')
        {
            message = 'Thank you for visiting partsdistributing.com! Due to inclement weather, PDC’s Main Office is closed on '+date.today().format()+'.  While we do not anticipate any delays at our warehouse to process your order, there may be delays in communication if you attempt to contact your Account Manager.'
+'Any questions or concerns can be directed to pdcsales@partsdistributing.com';
        }
        
        else if(ClosureReason=='PDC Closed')
        {
            message='Thank you for visiting partsdistributing.com! PDC’s Main Office will be closed on '+date.today().format()+' for '+ description+'.Any questions or concerns can be directed to pdcsales@partsdistributing.com'; 
        }       
 		
        else if(ClosureReason=='Warehouse Closed')
        {
            if(Area==null)
            {
             Message = 'Thank you for visiting partsdistributing.com! Please be advised that warehouses are closed on '+date.today().format()+' for Inventory. This closure may cause a shipping delay as well as affect transit times.'+  
                       'Any questions or concerns can be directed to pdcsales@partsdistributing.com or by contacting your Account Manager.';   
            }
            
            else
            {
            Message = 'Thank you for visiting partsdistributing.com! Please be advised that '+Area.replace(';', ' / ')+' warehouse(s) is closed on '+date.today().format()+' for Inventory. This closure may cause a shipping delay as well as affect transit times.'+  
                   'Any questions or concerns can be directed to pdcsales@partsdistributing.com or by contacting your Account Manager.';
            }
            
        }
        
         else if(ClosureReason=='PDC/Warehouse Closed')
        {
            
            if(Area==null)
            {
            Message= 'Thank you for visiting partsdistributing.com! Please be advised that our PDC Main Office and  warehouse(s) are closed on '+date.today().format()+' due to inclement weather. This closure may cause a shipping delay as well as affect transit times.'+
                'Any questions or concerns can be directed to pdcsales@partsdistributing.com';
            }
            else
            {
                 Message= 'Thank you for visiting partsdistributing.com! Please be advised that our PDC Main Office and  '+Area.replace(';', ' / ')+' warehouse(s) is closed on '+date.today().format()+' due to inclement weather. This closure may cause a shipping delay as well as affect transit times.'+
                'Any questions or concerns can be directed to pdcsales@partsdistributing.com';
            }
            
        }
        
        return Message;
    }
    
    
}