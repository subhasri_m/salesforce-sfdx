global with sharing class CCPDCAccountBranchFormController {
	//public String emptyVar {get;set;}

	//public CCPDCAccountBranchFormController() {
		
	//}

	@RemoteAction
	global static ccrz.cc_RemoteActionResult addAccountRequest(ccrz.cc_RemoteActionContext ctx, String data){
		ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
		try{
			String accountId = ccrz.cc_CallContext.effAccountId;
			Id recordTypeId = CCAviUtil.getRecordTypeIdByNameAndSObjectType('Branch_Request', 'CC_PDC_Account_Request__c');

			Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);

			CC_PDC_Account_Request__c accountRequest = new CC_PDC_Account_Request__c();
			accountRequest.RecordTypeId = recordTypeId;
			accountRequest.Parent_Account__c = accountId;
			accountRequest.Branch_Id__c = (String)input.get('branchId');
			// accountRequest.Name = (String)input.get('branchName');
			accountRequest.Business_Name__c = (String)input.get('branchName');
			accountRequest.Business_Address__c = ((String)input.get('addressLine1')).trim() + ' ' + ((String)input.get('addressLine2')).trim();
			accountRequest.BusinessCity__c = (String)input.get('city');
			accountRequest.BusinessState__c = (String)input.get('state');
			accountRequest.BusinessZipCode__c = (String)input.get('postalCode');
			accountRequest.First_Principal_Name__c = (String)input.get('primaryContactName');
			accountRequest.First_Principal_Phone_Number__c = (String)input.get('primaryContactPhoneNumber');
			accountRequest.First_Principal_Email__c = (String)input.get('primaryContactEmailAddress');
			accountRequest.Second_Principal_Name__c = (String)input.get('secondaryContactName');
			accountRequest.Second_Principal_Phone_Number__c = (String)input.get('secondaryContactPhoneNumber');
			accountRequest.Second_Principal_Email__c = (String)input.get('secondaryContactEmailAddress');

			insert accountRequest;

			CCAviPageUtils.buildResponseData(
				response, 
				true, 
				new Map<String, Object>{
					// 'branchDetails' => details
					'branchDetails' => 'test',
					'inputData' => data,
					'theinput' => input
				}
			);
		}
		catch(Exception e){
			String error = e.getMessage();
			String errMessage;
			if(error.contains('email')){
				errMessage = 'Invalid email address';
			} else if(error.contains('phone')){
				errMessage = 'Invalid Phone Number';
			} else if(error.contains('zipcode')){
				errMessage = 'Invalid Zipcode';
			}

			CCAviPageUtils.buildResponseData(response, false,
				new Map<String,Object>{
					'errorMessage' => errMessage,
					'error' => e.getMessage(),
					'cause' => e.getCause(),
					'lineno' => e.getLineNumber(),
					'stack' => e.getStackTraceString(),
					'data' => data
				}
			);
		}
		return response;
	}


	public class BranchDetails {
		String branchId;
		String branchName;
		String addressLine1;
		String addressLine2;
		String city;
		String state;
		String postalCode;
		String primaryContactName;
		String primaryContactPhoneNumber;
		String primaryContactEmailAddress;
		String secondaryContactName;
		String secondaryContactPhoneNumber;
		String secondaryContactEmailAddress;

	}
}