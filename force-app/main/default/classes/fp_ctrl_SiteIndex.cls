public with sharing class fp_ctrl_SiteIndex {
    public String declarationXml { get { return '<?xml version="1.0" encoding="UTF-8"?>\n'; } }
    public List<fp_bean_SiteMap> cats {get;set;}
    public final String HTTPS = 'https://';
    
    public String baseURL {
        get {
            if (null == baseURL) {
                // Ensure the URL starts with https and does not have a trailing slash
                baseURL = ((String) ccrz.cc_CallContext.storeFrontSettings.get('Site_Secure_Domain__c')).removeEnd('/');
                baseURL = !baseURL.startsWithIgnoreCase(HTTPS) ? HTTPS + baseURL : baseURL;
            }
            return baseURL;
        }
        private set;
    }
    
    public fp_ctrl_SiteIndex(){
        try{
            this.cats = (List<fp_bean_SiteMap>) retrieveCategoriesByProducts(); //Get our entitled products for the sitemap based on this category
        }catch(Exception e){
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
        }
    }
    
    //Gets the Categories that have entitled products for this account
    @TestVisible private static List<fp_bean_SiteMap> retrieveCategoriesByProducts(){
        String storefrontName = ccrz.cc_CallContext.storefront;         
        
        //Get the sku's that we are entitled to based on what is found in the price lists
       // Map<String, String> entitledSkus = entitledProductsByAccountGroup(storefrontName);  
        
        List<String> statuses = new List<String> { 'Released', 'Not Orderable' };
            String invalidType = 'Coupon';        
        
        //Query uses the previously found sku's from the pricelists to then check all the SKU's in the category and make sure they are listed, if not they are not included for the sitemap
        List<AggregateResult> categoryList = [SELECT ccrz__Category__c
                                                       FROM ccrz__E_ProductCategory__c p
                                                       WHERE p.ccrz__Product__r.ccrz__Storefront__c INCLUDES (:storefrontName)
                                                       AND p.ccrz__Product__r.ccrz__ProductStatus__c IN :statuses
                                                       AND p.ccrz__Product__r.ccrz__ProductType__c != :invalidType
                                                       AND p.ccrz__Product__r.ccrz__StartDate__c <=  TODAY 
                                                       AND p.ccrz__Product__r.ccrz__EndDate__c >=  TODAY
                                                       group by  p.ccrz__Category__c LIMIT 50000
                                                      ];
         List<String> resultList = new  List<String>();
         for(AggregateResult ag : categoryList) {  
             resultList.add(String.valueOf(ag.get('ccrz__Category__c')));
    		}
      
        Map<String, fp_bean_SiteMap> uniqueCategories = new Map<String, fp_bean_SiteMap>();
        for (String category: resultList){
            if(uniqueCategories.containsKey(category)){
                continue;
            }else{
                uniqueCategories.put(category,new fp_bean_SiteMap(category));
            }
        }       
        
        return uniqueCategories.values();
    }
    
    @TestVisible private static Map<Id, ccrz__E_PriceList__c> getAllowedPricelists(String storefrontName, Id accountGroupId){
        //Gets the price lists that are allowed for this account group  
        Map<Id,ccrz__E_PriceList__c> allowedPricelists = new Map<Id,ccrz__E_PriceList__c>([
            SELECT Id, Name
            FROM ccrz__E_PriceList__c 
            WHERE 
            Id IN (SELECT ccrz__PriceList__c FROM ccrz__E_AccountGroupPriceList__c WHERE ccrz__AccountGroup__c = :accountGroupId) AND
            ccrz__Storefront__c INCLUDES (:storefrontName) AND
            ccrz__StartDate__c <=  TODAY AND
            ccrz__EndDate__c >=  TODAY
        ]);         
        return allowedPricelists;
    }
    
    @TestVisible private static Map<String, String> entitledProductsByAccountGroup(String storefront){
        //Gets the entitled product SKU's for this specific account group
        Map<String, String> retValues = new Map<String, String>();
        try{
            String storefrontName = storefront;            
            ccrz__E_AccountGroup__c accountGroup = ccrz.cc_CallContext.currAccountGroup;
            Id accountGroupId = accountGroup.id;            
            //first determine the correct pricelists to use (it matches the storefront name and account group)
            Map<Id, ccrz__E_PriceList__c> allowedPricelists = getAllowedPricelists(storefrontName, accountGroupId);         
            for(ccrz__E_PriceListItem__c pli:[
                SELECT Id, ccrz__Product__r.ccrz__SKU__c 
                FROM ccrz__E_PriceListItem__c
                WHERE
                ccrz__Pricelist__c IN :allowedPricelists.values() AND
                (ccrz__Product__r.ccrz__ProductStatus__c = 'Released' OR ccrz__Product__r.ccrz__ProductStatus__c = 'Not Orderable' )AND
                ccrz__Product__r.ccrz__ProductType__c != 'Coupon' AND
                ccrz__StartDate__c <=  TODAY AND
                ccrz__EndDate__c >=  TODAY
                LIMIT :50000
            ]){
                //add each match to the return value set by the sku for quick order
                if(retValues.containsKey(pli.ccrz__Product__r.ccrz__SKU__c)){
                    continue;
                }else{
                    retValues.put(pli.ccrz__Product__r.ccrz__SKU__c, pli.ccrz__Product__r.ccrz__SKU__c);
                }
            } 
        }catch(Exception e){
            System.debug(System.LoggingLevel.Error, e.getMessage());
            System.debug(System.LoggingLevel.Error, e.getStackTraceString());
        }
        return retValues;
    }
}