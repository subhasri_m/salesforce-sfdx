global class CCFPUserUtil {
    
    @future
    public static void updateUser (String userData ){
        User usr = (User) JSON.deserialize(userData, User.Class);
        update usr;
    }
    
    @future
    public static void deleteCAPM(String contactId){
        System.debug('Inside CAPM Del');
        List<CC_FP_Contract_Account_Permission_Matrix__c> camplst = [Select id from CC_FP_Contract_Account_Permission_Matrix__c where Contact__c =: contactId ];
        System.debug('CAPM Del -->' + camplst);
        delete camplst;
    }
    
    
    public static void createFPBasicCAPM(String contactId, String accId ){
        CC_FP_Contract_Account_Permission_Matrix__c b_capm = new CC_FP_Contract_Account_Permission_Matrix__c(
            Contact__c = contactId,
            Account__c = accId,
            Allow_Override_ShipTo__c = false,
            Allow_AR_Payments__c = false,
            Allow_Access_to_Sales_Reports__c = false,
            Allow_Access_To_Accounting_Reports__c = false,
            Allow_Backorders__c = false,
            Allow_Non_Free_Shipping_Orders__c = false,
            Allow_Backorder_Release__c = false,
            Can_View_Invoices__c = false,
            Is_Admin__c = false,
            Power_Deals_Visibility__c = false,
            Pricing_Visibility__c = true,
            Allow_Checkout__c = true);
        insert b_capm;
    }
    
    @future
    public static void upsertUser(String contactId, Boolean userActive){
        try{
            Contact con = [select id,name,email,lastname,firstname,AccountId from contact where contact.id =: contactId limit 1];
            Account accObj = [Select id, name ,Iseries_Company_code__c from Account where Account.id =: con.AccountId limit 1];        
            List<User> userlst = [SELECT Id, name, isActive,IsPortalEnabled FROM user WHERE Contact.id =: contactId];
            
            if(userActive && userlst.isEmpty() && accObj.Iseries_Company_code__c == '1'){
                Profile profObj = [SELECT id,name FROM Profile where name ='CloudCraze Customer Community User FleetPride' limit 1];
                User newUser = new User(contactId=contactId, 
                                        username=con.Email, 
                                        firstname=con.FirstName,
                                        lastname=con.LastName, email=con.Email,
                                        communityNickname = con.LastName + '_'+ string.valueof((Math.random() * 100)),
                                        isActive = true,
                                        alias = string.valueof(con.FirstName.substring(0,1) + con.LastName.substring(0,1)), 
                                        profileid = profObj.Id, 
                                        emailencodingkey='UTF-8',
                                        languagelocalekey='en_US', 
                                        localesidkey='en_US', 
                                        timezonesidkey='America/Los_Angeles');
                insert newUser;
                CCFPUserUtil.createFPBasicCAPM(contactId,accObj.id);
            }
            else if (!userActive && !userlst.isEmpty() && accObj.Iseries_Company_code__c == '1'){
                userlst[0].isActive = userActive;
                userlst[0].IsPortalEnabled =userActive;
                update userlst;
            }
        }
        catch(Exception e){
            system.debug('Error -->' + e.getMessage());
        }
        
    }
}