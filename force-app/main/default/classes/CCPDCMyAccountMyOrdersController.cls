global with sharing class CCPDCMyAccountMyOrdersController {
    @RemoteAction
    global static ccrz.cc_RemoteActionResult reorder(ccrz.cc_RemoteActionContext ctx, String orderId) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        response.success = false;

        ccrz.ccLog.log('newIdCART CART CART @@@@@@@@@@@@@:  ');
        String cartEncId = ccrz.cc_CallContext.currCartId;
        try {
            if (orderId != null) {
                ccrz__E_Order__c order = CCPDCOrderDAO.getOrderOwner(orderId);
                Id oldOwner = order.OwnerId;
                Id oldAccount = order.ccrz__Account__c;
                Id oldContact = order.ccrz__Contact__c;

                Id newOwner = ccrz.cc_CallContext.currUserId;
                Id newAccount = ccrz.cc_CallContext.currAccountId;
                Id newContact = ccrz.cc_CallContext.currContact.Id;
                IF (Test.isRunningTest()) {
                    CCPDCChangeOwnerHelper.changeOrderOwnerAll(orderId, [
                            SELECT Id
                            FROM User
                            WHERE Create_Accounts__c = true AND
                            Profile.Name = 'System Administrator' AND isActive = true
                            Limit 1
                    ].Id, newContact, newAccount);
                } else {
                    CCPDCChangeOwnerHelper.changeOrderOwnerAll(orderId, newOwner, newContact, newAccount);
                }

                String newId = CCAviOrderManager.reorder(orderId);
                ccrz.ccLog.log(System.LoggingLevel.INFO, 'newIdCART CART CART @@@@@@@@@@@@@:', newId);
                ccrz.ccLog.log(System.LoggingLevel.INFO, 'newIdCART CART CART @@@@@@@@@@@@@:', newId);
                ccrz.ccLog.log(System.LoggingLevel.DEBUG, 'newIdCART CART CART @@@@@@@@@@@@@:', newId);

                IF (Test.isRunningTest()) {
                    CCPDCChangeOwnerHelper.changeOrderOwnerAll(orderId, [
                            SELECT Id
                            FROM User
                            WHERE Create_Accounts__c = true AND
                            Profile.Name = 'System Administrator' AND isActive = true
                            Limit 1
                    ].Id, oldContact, oldAccount);
                } else {
                    CCPDCChangeOwnerHelper.changeOrderOwnerAll(orderId, oldOwner, oldContact, oldAccount);
                }

                if (newId != null) {

                    ccrz__E_Cart__c anewcart = CCPDCCartDAO.getCart(newId);
                    ccrz.ccLog.log(System.LoggingLevel.INFO, 'newIdCART CART CART a newcart:', anewcart);
                    ccrz__E_ContactAddr__c oldshipto = CCFPContactAddressDAO.getAddress(order.ccrz__ShipTo__c);
                    ccrz__E_ContactAddr__c oldbillto = CCFPContactAddressDAO.getAddress(order.ccrz__BillTo__c);


                    ccrz__E_ContactAddr__c newshipto = oldshipto.clone();
                    ccrz__E_ContactAddr__c newbillto = oldbillto.clone();
                    newshipto.ccrz__FirstName__c = 'firstname';
                    newshipto.ccrz__LastName__c = 'lastname';
                    insert newshipto;
                    insert newbillto;

                    List<ccrz__E_CartItem__c> cartitems = anewcart.ccrz__E_CartItems__r;


                    anewcart.ccrz__BillTo__c = newbillto.Id;
                    anewcart.ccrz__ShipTo__c = newshipto.Id;


                    delete cartitems;
                    update anewcart;

                    Map<String, Decimal> itemMap = new Map<String, Decimal>();

                    if (order != null && order.ccrz__E_OrderItems__r != null && order.ccrz__E_OrderItems__r.size() > 0) {


                        for (ccrz__E_OrderItem__c oi : order.ccrz__E_OrderItems__r) {
                            String sku = oi.ccrz__Product__r.ccrz__Sku__c;
                            Decimal quantity = itemMap.get(sku);
                            if (quantity == null) {
                                quantity = oi.ccrz__Quantity__c;
                            } else {
                                quantity += oi.ccrz__Quantity__c;
                            }
                            itemMap.put(sku, quantity);
                        }
                    }
                    ////INSERT OLD CART
                    if (cartEncId != null) {
                        ccrz__E_Cart__c thecart = CCPDCCartDAO.getCart(cartEncId);
                        if (thecart != null) {
                            List<ccrz__E_CartItem__c> oldCartItems = thecart.ccrz__E_CartItems__r;

                            List<String> skulist = new List<String>();
                            for (ccrz__E_CartItem__c ci : oldCartItems) {
                                String sku = ci.ccrz__Product__r.ccrz__SKU__c;
                                Decimal quantity = itemMap.get(sku);
                                if (quantity == null) {
                                    quantity = ci.ccrz__Quantity__c;
                                } else {
                                    quantity += ci.ccrz__Quantity__c;
                                }
                                itemMap.put(sku, quantity);
                            }
                            delete oldCartItems;
                        }


                        update thecart;
                    }



                    if (!itemMap.isEmpty()) {
                        List<ccrz.ccApiCart.LineData> newLines = new List<ccrz.ccApiCart.LineData>();
                        for (String key : itemMap.keySet()) {
                            ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
                            newLine.sku = key;
                            newLine.quantity = itemMap.get(key);
                            newLines.add(newLine);
                        }

//                        if (anewcart.Id != null) {
//                            Map<String, Object> request = new Map<String,Object>{
//                                ccrz.ccApi.API_VERSION => 6,
//                                ccrz.ccApiCart.CART_ENCID => anewcart.Id,
//                                ccrz.ccApiCart.LINE_DATA => newLines
//                            };
//
//                            Map<String, Object> outputData = CCPDCCartManager.addTo(request);
//                            Boolean wasSuccessful = (Boolean)outputData.get(ccrz.ccApi.SUCCESS);
//                            if (wasSuccessful) {
//                                response.success = true;
//                            }
//                        }

                        if (cartEncId != null) {
                            Map<String, Object> request = new Map<String, Object>{
                                    ccrz.ccApi.API_VERSION => 6,
                                    ccrz.ccApiCart.CART_ENCID => cartEncId,
                                    ccrz.ccApiCart.LINE_DATA => newLines
                            };

                            Map<String, Object> outputData = CCPDCCartManager.addTo(request);
                            Boolean wasSuccessful = (Boolean) outputData.get(ccrz.ccApi.SUCCESS);
                            if (wasSuccessful) {
                                response.success = true;
                            }
                        }


                        response.data = new Map<String, Object>{
                                'encId' => cartEncId
                        };
                        response.success = true;
                    }
                }
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }finally{
            ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','reorder');
            ccrz.ccLog.close(response);
        }
        return response;
    }

//    @RemoteAction
//    global static ccrz.cc_RemoteActionResult reorder(ccrz.cc_RemoteActionContext ctx, String orderId) {
//        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
//        response.success = false;
//
//
//        ccrz.ccLog.log('NEW REORDER:  ');
//        try {
//            if (orderId != null) {
//
//                ccrz.ccLog.log(System.LoggingLevel.DEBUG, 'D:something', orderId );
//                ccrz.ccLog.log('newIdCART CART CART @@@@@@@@@@@@@:  '+orderId);
//
//                ccrz__E_Order__c order = CCPDCOrderDAO.getOrderDetails(orderId);
//                Map<String, Decimal> itemMap = new Map<String, Decimal>();
//                if (order != null && order.ccrz__E_OrderItems__r != null && order.ccrz__E_OrderItems__r.size() > 0) {
//                    for(ccrz__E_OrderItem__c oi : order.ccrz__E_OrderItems__r) {
//                        String sku = oi.ccrz__Product__r.ccrz__Sku__c;
//                        Decimal quantity = itemMap.get(sku);
//                        if (quantity == null) {
//                            quantity = oi.ccrz__Quantity__c;
//                        }
//                        else {
//                            quantity += oi.ccrz__Quantity__c;
//                        }
//                        itemMap.put(sku, quantity);
//                    }
//                }
//
//                String cartId = CCAviCartManager.createCart();
//                if (!itemMap.isEmpty()) {
//                    List<ccrz.ccApiCart.LineData> newLines = new List<ccrz.ccApiCart.LineData>();
//                    for (String key : itemMap.keySet()) {
//                        ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
//                        newLine.sku = key;
//                        newLine.quantity = itemMap.get(key);
//                        newLines.add(newLine);
//                    }
//
//                    if (cartId != null) {
//                        Map<String, Object> request = new Map<String,Object>{
//                            ccrz.ccApi.API_VERSION => 6,
//                            ccrz.ccApiCart.CART_ENCID => cartId,
//                            ccrz.ccApiCart.LINE_DATA => newLines
//                        };
//
//                        Map<String, Object> outputData = CCPDCCartManager.addTo(request);
//                        Boolean wasSuccessful = (Boolean)outputData.get(ccrz.ccApi.SUCCESS);
//                        if (wasSuccessful) {
//                            response.success = true;
//                        }
//                    }
//                }
//                else {
//                    response.success = true;
//                }
//
//
//                if (cartId != null) {
//                    ccrz__E_Cart__c cart = CCPDCCartDAO.getCartShipTo(cartId);
//                    cart.CC_FP_Location__c = order.CC_FP_Location__c;
//                    if (cart.ccrz__BillTo__c == null && order.ccrz__BillTo__c != null) {
//                        ccrz__E_ContactAddr__c billTo = CCFPContactAddressDAO.getAddress(order.ccrz__BillTo__c);
//                        ccrz__E_ContactAddr__c newBillTo = billTo.clone();
//                        cart.ccrz__BillTo__c = newBillTo.Id;
//                    }
//                    if (cart.ccrz__ShipTo__c == null && order.ccrz__ShipTo__c != null) {
//                        ccrz__E_ContactAddr__c shipTo = CCFPContactAddressDAO.getAddress(order.ccrz__ShipTo__c);
//                        ccrz__E_ContactAddr__c newShipTo = shipTo.clone();
//                        cart.ccrz__ShipTo__c = newShipTo.Id;
//                    }
//                    update cart;
//                    response.data = new Map<String,Object>{'encId' => cartId};
//                }
//
//            }
//        }
//        catch (Exception e) {
//            ccrz.ccLog.log('NEW REORDER:  ');
//            CCAviPageUtils.buildResponseData(response, false,
//                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
//            );
//        }
//        return response;
//    }

}