@isTest
public class CCFPUserUtilTest {
	    static User thisUser;
    static CCPDCTestUtil util = new CCPDCTestUtil();
    
    @testSetup
    static void testSetup() {        
        thisUser = [SELECT Id,AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        thisUser.Email = 'test@gmail.com';
        update thisUser;
        
        System.debug('thisUser==>'+thisUser.AccountId);
        Account acc = new Account(Id=thisUser.AccountId);
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
        
        
    }
    @IsTest
    public static void updateUserTest() {
        util.initCallContext();
        User theUser = util.getPortalUser();
        theUser.IsActive = false;
        CCFPUserUtil.updateUser(JSON.serialize(theUser));
        
        
        Account acct = new Account(Name='Test Account',Iseries_Company_code__c = '1');
        insert acct;
        Contact contObj = new Contact(AccountId=acct.Id,
                                      firstname='Test First Name',
                                      lastname='Test Last Name',
                                      email='email@testemail.com');
        insert contObj;
        Test.startTest();
        CCFPUserUtil.upsertUser(contObj.Id,true);
        CCFPUserUtil.deleteCAPM(contObj.Id);
        Test.stopTest();
        
    }
    
}