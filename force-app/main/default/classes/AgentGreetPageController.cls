/**
 * @description     AgentGreetPage page Controller 
 * @author          Lalit
 * @Company         FP
 * @date            14-12-2017
 *
 * HISTORY
 * - 14-12-2017    Lalit      Created.
*/ 

public class AgentGreetPageController {
    
   public  String chatKey {get; set;}
   public String FeedbackComment {get; set;}
   Public String OverallExperience {get; set;}
   public String QuestionAnswered {get; set;}
   public String RecommendProduct {get; set;}
   public String FeedbackRating {get; set;}
   List<Live_Agent_Feedback__c> LAFList  = new List<Live_Agent_Feedback__c>();
    
    public AgentGreetPageController()
    {
        chatkey =   ApexPages.currentPage().getParameters().get('ChatKey');
      System.debug('--->'+ chatKey);
    }
    public pagereference SaveFeedback()
    {
    
        Live_Agent_Feedback__c LAF = new Live_Agent_Feedback__c();
        LAF.Feedback_Comment__c = FeedbackComment;
        LAF.Feedback_Rating__c = FeedbackRating;
        LAF.ChatKeyReference__c  =chatkey;
        LAF.Overall_Experience__c = OverallExperience ;
        LAF.Recommend_Product__c = RecommendProduct;
        LAF.Question_Answered__c = QuestionAnswered;
        LAFList.add(LAF);
        Insert LAFList;
        
         PageReference pr = new PageReference('/apex/LiveAgentThankyou?CloseVisible=true&ReturnVisible=false');
        pr.getParameters().put('key','value');
        pr.setRedirect(true); // If you want a redirect. Do not set anything if you want a forward.
        return pr;
    
    }

}