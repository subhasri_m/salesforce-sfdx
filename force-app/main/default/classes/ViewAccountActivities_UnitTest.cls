@isTest
public class ViewAccountActivities_UnitTest {

     @testSetup
    public static void testSetup(){
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama11uu.com',
            Username = 'puser000@amamama1177.com' + System.currentTimeMillis(),
            CompanyName = 'TEST_SEDE',
            Title = 'title_test',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Create_Accounts__c = true,
            LocaleSidKey = 'en_US'
        );
        insert u;
        System.runAs(u){
            Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = u.Id,
                                      Customer_Call_Classification__c =  'A');  
            insert acc;
            System.assertNotEquals(null,acc.id)  ;  
            contact con = new contact (lastname = 'checkme',firstname='check',Accountid = acc.id,OwnerId=u.id,Email='test@test.com');    
            insert con;
            Event newEvent = new Event();
            newEvent.OwnerId = u.Id;
            newEvent.Subject ='Test';
            newEvent.WhatId = acc.Id;
            newEvent.IsRecurrence = true;
            newEvent.RecurrenceStartDateTime = System.today();
            newEvent.RecurrenceEndDateOnly = System.today()+30;
            newEvent.RecurrenceType = 'RecursDaily';
            newEvent.RecurrenceInterval = 1; // This means that the event will wait 1 day before recurring again
            newEvent.IsAllDayEvent =true;
            newEvent.DurationInMinutes =1440;
            newEvent.ActivityDate = System.today();
            insert newEvent;
            Task t = new Task(
                Priority ='High',
                Description='Test',
                Subject = 'Email',
                OwnerId = u.Id,
                WhatId = acc.Id,
                status = 'New',
                ActivityDate = SYSTEM.today()
            );
            insert t;
            Task t1 = new Task(
                Priority ='High',
                Description='Test',
                Subject = 'Email',
                OwnerId = u.Id,
                WhatId = acc.Id,
                status = 'New',
                ActivityDate = SYSTEM.today(),
                
                Recurring_Task_ID__c = T.id
            );
            insert t1;
            Task t2 = new Task(
                Priority ='High',
                Description='Test',
                Subject = 'Call',
                OwnerId = u.Id,
                WhatId = acc.Id,
                status = 'New',
                ActivityDate = SYSTEM.today(),
                
                Recurring_Task_ID__c = T1.id
            );
            insert t2;
            
              Task newEvent1 = new Task();
            newEvent1.OwnerId = u.Id;
            newEvent1.Subject ='Test22222';
            newEvent1.WhatId = acc.Id;
            newEvent1.IsRecurrence = true;
            newEvent1.RECURRENCESTARTDATEONLY = System.today();
            newEvent1.RecurrenceEndDateOnly = System.today()+5;
         
            newEvent1.RecurrenceType = 'RecursDaily';
            newEvent1.RecurrenceInterval = 1; // This means that the event will wait 1 day before recurring again 
            insert newEvent1;
            SYSTEM.debug('newEvent1'+newEvent1);
             
        }
    }
    
    private static testMethod void unitTest_1(){
        TEST.setCurrentPage(Page.ViewAccountActivities);
        ApexPages.currentPage().getParameters().put('taskIdParam',[Select Id From Task Limit 1].Id);
        Account acc = [select id from account limit 1];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc); 
        ViewAccountActivities sch = new ViewAccountActivities(sc);
       
        ViewAccountActivities.TaskScheduleWrapper wrap = new ViewAccountActivities.TaskScheduleWrapper(); //ActivityWrapperClass
        ViewAccountActivities.ActivityWrapperClass wrap1 = new ViewAccountActivities.ActivityWrapperClass();
        wrap.TaskId_Rc = [Select Id From Task Limit 1].Id;
        wrap.StartDate_Rc = Date.today();
        wrap.EndDate_Rc = Date.today().addMonths(2);
        wrap.DueDate_Rc =  Date.today().addMonths(1);
        wrap.ParentRecurrenceID_Rc = [Select Id From Task Limit 1].Id;
        wrap.IsRecurrenceParent_Rc = true;
        wrap.IsRecurrenceChild_Rc = true;
        wrap.RecurrenceType = 'Weekly';
        sch.ScheduleWrapper = wrap;
        sch.getItems();
        sch.getRecWeekDay();
        sch.getRecInstances();
        sch.assetClicked();
        sch.ScheduleSalesCallClicked();
        sch.GotoRecurrenceTask();
        sch.getUserWeekTasks();
        sch.getRecurrenceTypeOptions();
        sch.UserWeekTasksGenerator(); 
        sch.updateTableData();
        sch.IsRecurrenceChangedMethod();
        sch.deleteRecurrenceSeries();
        sch.showWeekDayNow();
        sch.showPopup();
        sch.closePopup();
             sch.UpdateRecurrenceSchedule([Select Id From Task LIMIT 1].Id,[Select Id From Task where isRecurrence =false LIMIT 1].Id);
        sch.CalculateWeekMask();
        sch.redirectPopup(); 
     
    }
    
    private static testMethod void unitTest_2(){
        TEST.setCurrentPage(Page.ViewAccountActivities);
        ApexPages.currentPage().getParameters().put('taskIdParam',[Select Id From Task where Recurring_Task_ID__c!=null limit 1].Id);
       ApexPages.currentPage().getParameters().put('id',[Select Id From Account].Id);
       
        
        Account acc = [select id from account limit 1];
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc); 
        ViewAccountActivities sch = new ViewAccountActivities(sc);
         sch.HiddenTaskId = [Select Id From Task   limit 1].Id;
        ViewAccountActivities.TaskScheduleWrapper wrap = new ViewAccountActivities.TaskScheduleWrapper(); //ActivityWrapperClass
        ViewAccountActivities.ActivityWrapperClass wrap1 = new ViewAccountActivities.ActivityWrapperClass();
        wrap.TaskId_Rc = [Select Id From Task Limit 1].Id;
        wrap.StartDate_Rc = Date.today();
        wrap.EndDate_Rc = Date.today().addMonths(2);
        wrap.DueDate_Rc =  Date.today().addMonths(1);
        wrap.ParentRecurrenceID_Rc = [Select Id From Task Limit 1].Id;
        wrap.IsRecurrenceParent_Rc = true;
        wrap.IsRecurrenceChild_Rc = true;
           wrap.RecurrenceType = 'Weekly';
        sch.ScheduleWrapper = wrap;
        sch.getItems();
        sch.getRecWeekDay();
        sch.getRecInstances();
        sch.assetClicked();
        sch.ScheduleSalesCallClicked();
        sch.GotoRecurrenceTask();
        sch.getUserWeekTasks();
        sch.getRecurrenceTypeOptions();
        sch.UserWeekTasksGenerator(); 
        sch.updateTableData();
        sch.IsRecurrenceChangedMethod();
        sch.showWeekDayNow();
        sch.showPopup();
        sch.closePopup();
           sch.UpdateRecurrenceSchedule([Select Id From Task LIMIT 1].Id,[Select Id From Task where isRecurrence =false LIMIT 1].Id);
        sch.CalculateWeekMask();
        sch.redirectPopup(); 
        sch.ScheduleSalesCallToday();
     
    }
    
    
}