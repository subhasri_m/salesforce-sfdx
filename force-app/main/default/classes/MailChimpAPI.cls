public without sharing class MailChimpAPI {
	
    public static HttpResponse fleetPrideSubscribe(string email){
        
        MailChimpAuthDetail__c mailChimpAuthDetail = MailChimpAuthDetail__c.getValues('parts');
        
         final String MailChimpUsername = mailChimpAuthDetail.UserName__c; //'ana.santellana@fleetpride.com';
         final String MailChimpAPIKey = mailChimpAuthDetail.APIKey__c; //'1d7d9267f9308b7ab44bbb4e6eecc30c-us7';
         final String MailChimpURLEndPoint =mailChimpAuthDetail.URLEndPoint__c; // 'https://us7.api.mailchimp.com/3.0/';
         final String MailChimpListId = mailChimpAuthDetail.ListId__c;//'58b230415c';
        
        
        String endPoint = MailChimpURLEndPoint+'lists/'+MailChimpListId+'/members/';
        String blobString =  MailChimpUsername+':'+MailChimpAPIKey;
        String encodedValue = EncodingUtil.base64Encode(blob.valueOf(blobString));

        
        HttpRequest httpReq = new HttpRequest();
        HttpResponse httpRes = new HttpResponse();
        httpReq.setEndpoint(endPoint);
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type','application/json');
        httpReq.setHeader('Authorization','Basic '+encodedValue);
        
        String jsonString = '{'+
            '"email_address": "'+email+'",'+
            '"status": "subscribed",'+
            '"merge_fields": {'+
            '"MMERGE5": ""'+
            '}'+
            '}';
        httpReq.setBody(jsonString);        
        HttpRes = (new http()).send(httpReq);
        return HttpRes;
        
    }
    
}