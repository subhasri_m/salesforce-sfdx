@isTest
public class CCPDCAccountDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void queryAccessibleAccountsTest(){
        util.initCallContext();
        Account theAccount = util.getAccount();

        Map<Id, Account> data = null;

        Test.startTest();
        data = CCPDCAccountDAO.queryAccessibleAccounts();
        Test.stopTest();

        System.assert(data != null);
        System.assert(data.get(theAccount.Id) != null);
    }

    static testmethod void queryAccessibleAccountsWithAddressTest(){
        util.initCallContext();
        Account theAccount = util.getAccount();

        Map<Id, Account> data = null;

        Test.startTest();
        data = CCPDCAccountDAO.queryAccessibleAccountsWithAddress();
        Test.stopTest();

        System.assert(data != null);
        System.assert(data.get(theAccount.Id) != null);
    }

    static testmethod void getAccountPermissionsTest(){
        util.initCallContext();
        Account theAccount = util.getAccount();

        Account returnedAccount = null;

        Test.startTest();
        returnedAccount = CCPDCAccountDAO.getAccountPermissions(theAccount.Id);
        Test.stopTest();

        System.assert(returnedAccount != null);
        System.assertEquals(theAccount.Id, returnedAccount.Id);
    }

    static testmethod void getAccountHierarchyTest(){
        util.initCallContext();
        Account theAccount = util.getAccount();

        List<Account> returnedAccounts = null;

        Test.startTest();
        returnedAccounts = CCPDCAccountDAO.getAccountHierarchy(theAccount.Id);
        Test.stopTest();

        System.assert(returnedAccounts != null);
        System.assertEquals(1, returnedAccounts.size());
    }

    static testmethod void getAccountFreeShippingTest(){
        util.initCallContext();
        Account theAccount = util.getAccount();

        List<Account> returnedAccounts = null;

        Test.startTest();
        returnedAccounts = CCPDCAccountDAO.getAccountFreeShipping(theAccount.Id);
        Test.stopTest();

        System.assert(returnedAccounts != null);
        System.assertEquals(1, returnedAccounts.size());
    }

    static testmethod void getAccountTest(){
        util.initCallContext();
        Account theAccount = util.getAccount();

        Account returnedAccount = null;

        Test.startTest();
        returnedAccount = CCPDCAccountDAO.getAccount(theAccount.Id);
        Test.stopTest();

        System.assert(returnedAccount != null);
        System.assertEquals(theAccount.Id, returnedAccount.Id);
    }

    static testmethod void getTopLevelAccountTest(){
        util.initCallContext();
        Account theAccount = util.getAccount();

        Account returnedAccount = null;

        Test.startTest();
        returnedAccount = CCPDCAccountDAO.getTopLevelAccount(theAccount.Id);
        Test.stopTest();

        System.assert(returnedAccount != null);
        System.assertEquals(theAccount.Id, returnedAccount.Id);
    }

}