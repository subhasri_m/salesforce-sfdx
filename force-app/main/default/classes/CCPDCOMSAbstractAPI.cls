public abstract class CCPDCOMSAbstractAPI {
		
	// An instance of HttpUtil with the OAuth token will be provided. No DML should be performed
	// in this method.
	public abstract void executeRequest(CCAviHttpUtil util);

	// Perform any DML operations. This method is called after all CCPDCOMSIFC intefaces
	// executeRequest methods have been called.
	public abstract void commitData();

}