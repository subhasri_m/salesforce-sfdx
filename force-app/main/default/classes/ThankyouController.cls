public class ThankyouController {
    
    Public String ReturnVisible{get;set;}
    public String CloseVisible {get; set;}

    Public ThankyouController ()
    {
    ReturnVisible = ApexPages.currentPage().getParameters().get('ReturnVisible');
    CloseVisible = ApexPages.currentPage().getParameters().get('CloseVisible');
    
    }
     

    public pagereference ReturnToPDC()
    {
        String URL= System.label.PDC_URL;
        pagereference pr = new pagereference(URL);
         pr.setRedirect(true);
        return pr;
   
    }
    
}