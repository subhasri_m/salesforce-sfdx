@isTest
public class CCPDCProductQuantityRuleTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }    

    static testmethod void ruleTest() {
        util.initCallContext();

        ccrz__E_Product__c p = util.getProduct();

        List<String> listSKUs = new List<String>{p.ccrz__SKU__c};
        Map<String,Map<String,Object>> outputData;
        
        Test.startTest();
        CCPDCProductQuantityRule rule = new CCPDCProductQuantityRule();
        outputData = rule.getRules(listSKUs);
        Test.stopTest();


        System.assertEquals(1,outputData.size());
    }
}