/* Written By Likhit Verma */
/* Returns the current theme that is actually being used. */ 
public with sharing class UIThemeController {
   @AuraEnabled
    public static String getUIThemeDescription() {
        String theme = UserInfo.getUiThemeDisplayed();
        System.debug('Theme type: '+theme);
        return theme;
    }
}