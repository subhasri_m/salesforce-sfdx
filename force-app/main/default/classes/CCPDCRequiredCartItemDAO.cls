public with sharing class CCPDCRequiredCartItemDAO {
    public static List<CC_FP_Required_Cart_Item__c> getRequiredCartItems(Set<Id> theIds) {
        List<CC_FP_Required_Cart_Item__c> items = [
            SELECT 
                Id,
                CC_Cart__c,
                CC_Parent_Cart_Item__c,
                CC_Parent_Cart_Item__r.ccrz__Product__r.ccrz__SKU__c,
                Quantity__c,
                Price__c,
                SubAmount__c,
                CC_Product__c, 
                CC_Product__r.Name, 
                CC_Product__r.ccrz__SKU__c,
                CC_Product__r.Part_Number__c,
                CC_Product__r.Pool_Number__c
            FROM 
                CC_FP_Required_Cart_Item__c
            WHERE
                CC_Parent_Cart_Item__c IN :theIds
        ];

        return items;
    }
}