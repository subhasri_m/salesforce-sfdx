/**
* @description     Number of Accounts Batch 
* @author          Lawrence Catan
* @Company         CloudSherpas
* @date            29.FEB.2016
*
* - Updates the Number of Accounts field in Users based on the Accounts they own
* HISTORY
* - 29.FEB.2016    Lawrence Catan      Created.
*/ 

global class NumberofAccountsBatch implements Database.Batchable<sObject>, schedulable
{
    List<BatchNumberAccountsOnUserSetting__c> QuerySetting = BatchNumberAccountsOnUserSetting__c.getAll().values();
    public String query;    
    
     global void execute(SchedulableContext sc) {
    Database.executeBatch(new NumberofAccountsBatch());
    }
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(test.isRunningTest())
        {
        query = 'SELECT Id,OwnerID, Name FROM Account' ;
        }else
        {
        query = QuerySetting[0].AccountFilterQuery__c ;
        }System.debug('query for account'+query);
        return Database.getQueryLocator(query);
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject>scope){
        // Logic to be Executed batch wise      
        Set<Id> OwnerIdsToUpdate = new Set<Id>();
        Map<String, Integer> mapOfAccounts = new Map<String, Integer>();
        List<User> UsersToUpdate = new List<User>();
        
        for (sobject s : scope){
            Account a = (Account)s;
            OwnerIdsToUpdate.add(a.OwnerID);                  
        }
        
        System.debug('OwnerIdsToUpdate value = '+ OwnerIdsToUpdate);
        
        //List<AggregateResult> CountOfAccounts = [SELECT OwnerID, Count(Id) OwnerCount From Account where OwnerID In:OwnerIdsToUpdate Group By OwnerID];
        
        for(AggregateResult ag : [SELECT OwnerID, Count(Id) OwnerCount From Account where OwnerID In:OwnerIdsToUpdate Group By OwnerID]) {
            mapOfAccounts.put(String.valueOf(ag.get('OwnerId')), (Integer) ag.get('OwnerCount'));//Integer.valueOf('OwnerCount'));
        }
        
        for(User u : [SELECT Id, Number_of_Accounts__c FROM User WHERE Id IN: OwnerIdsToUpdate]) {
            Integer numAcc = mapOfAccounts.containsKey(u.Id)?mapOfAccounts.get(u.Id):0;
            System.debug('numAcc value =' + numAcc);
            if (u.Number_of_Accounts__c!=numAcc)
            {
            u.Number_of_Accounts__c = numAcc;
            UsersToUpdate.add(u);
            }
        }
        
        System.debug('UsersToUpdate value = ' +UsersToUpdate);
        
        if(UsersToUpdate.size() > 0) {
            update UsersToUpdate;
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
        NumberofAccountsBatch batchSch=new NumberofAccountsBatch();
         
        Integer SettingHours =  QuerySetting[0].Schedule_HoursDifference__c.intValue(); 
        Integer LastRunTimeHour ; 
        BatchNumberAccountsOnUserSetting__c updateCustomSetting = new BatchNumberAccountsOnUserSetting__c();
        
        if (QuerySetting[0].LastRunTimeHour__c!=null)
        {
         LastRunTimeHour =  QuerySetting[0].LastRunTimeHour__c.intValue();
        }
        String sch ; 
        integer nextHours= LastRunTimeHour+SettingHours;
        Datetime Cron = Datetime.now();   
        
        if (nextHours >21 && nextHours<=24)
        {
            if (cron.format('EEE').equalsIgnoreCase('Fri')){
            Cron = cron.addDays(3);
            }
            else
            {
                Cron = cron.addDays(1);
            }
            sch='0 0 9 ' +cron.day()+' '+cron.month()+' ? '+cron.year();
            updateCustomSetting.LastRunTimeHour__c = 9;
        }
     
        else if (LastRunTimeHour!=null && nextHours>=9 && nextHours<=21 )
        {
        	sch='0 0 ' + (LastRunTimeHour+SettingHours)+' '+cron.day()+' '+cron.month()+' ? '+cron.year();
            updateCustomSetting.LastRunTimeHour__c = LastRunTimeHour+SettingHours;
        } 
         
        updateCustomSetting.id = QuerySetting[0].id; 
        update updateCustomSetting; 
        system.debug('Cron -->' + sch); 
        if(!test.isRunningTest())
        {System.schedule('NumberofAccountsBatch'+sch, sch , batchSch);}
    }
}