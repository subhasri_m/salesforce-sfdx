global without sharing class CCFPLogicUserRegister extends ccrz.ccLogicUserRegister {
	global override virtual Map<String, Object> validateUser (Map<String, Object> inputData) {
        inputData = super.validateUser(inputData);
        String accountNumber = '';
        String billingZipCode = '';
        String streetAddress = '';
        String streetAddress2 = '';
        String city = '';
        String state = '';
        String country = 'US';
        String postalCode = '';
        String streetNumber ='';
        Boolean accountNumberFlag = false;
        String iSeriesCustomerAccount = '';
        String iSeriesCustomerBranch = '0';
        String formData = (String) inputData.get('usrRegisterJson');
        String addressValidationMessage = '';
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(formData);
        
        if(result.containsKey('accountNumber')) {
            accountNumber = (String) result.get('accountNumber');
            if(String.isNotBlank(accountNumber)) {
                if(accountNumber.contains('-')) {
                    String[] iSeriesInfo = accountNumber.split('-');
                    iSeriesCustomerAccount = String.valueOf(Integer.valueOf(iSeriesInfo[0]));
                    iSeriesCustomerBranch = String.valueOf(Integer.valueOf(iSeriesInfo[1]));
                } else {
                    iSeriesCustomerAccount = String.valueOf(Integer.valueOf(accountNumber));
                }
            }
        }
        if(result.containsKey('billingZipCode')) {
            billingZipCode = (String) result.get('billingZipCode');
        }
        if(result.containsKey('accountNumberFlag')) {
            accountNumberFlag = (Boolean) result.get('accountNumberFlag');
        }
        if(result.containsKey('billingAddress.address1')) {
            streetAddress = (String) result.get('billingAddress.address1');
            if(String.isNotBlank(streetAddress)) {
                String[] address = streetAddress.trim().split(' ');
                streetNumber = address[0];
            }
        }
        
       if(result.containsKey('billingAddress.address2')) {
            streetAddress2 = (String) result.get('billingAddress.address2');
        }
        
        if(result.containsKey('billingAddress.city')) {
            city = (String) result.get('billingAddress.city');
        }
        
         if(result.containsKey('billingAddress.stateCode')) {
            state = (String) result.get('billingAddress.stateCode');
        }
        
        if(result.containsKey('billingAddress.postalCode')) {
            postalCode = (String) result.get('billingAddress.postalCode');
        }
     
        
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','createAccount accountNumber:' + accountNumber );
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','createAccount billingZipCode:' + billingZipCode );
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','createAccount accountNumberFlag:' + accountNumberFlag );
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','createAccount billingAddress1:' + streetAddress );
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','createAccount billingPostalCode:' + postalCode );
        if(accountNumberFlag == false && (String.isNotBlank(billingZipCode) || String.isNotblank(iSeriesCustomerAccount))) {
            List<Account> acctList = [Select Id, Name from Account where Iseries_Company_code__c = '1' and ISeries_Customer_Account__c = :iSeriesCustomerAccount and ISeries_Customer_Branch__c = :iSeriesCustomerBranch and BillingPostalCode like :billingZipCode + '%'];
            if(acctList.size() == 0) {
                ccrz.cc_bean_Message message = new ccrz.cc_bean_Message();
                message.labelId  = 'SiteRegistration_AccountNumberError';
                message.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
                message.classToAppend = 'error_messages_section';
                message.severity = ccrz.cc_bean_Message.MessageSeverity.ERROR;
    
                inputData.put(ccrz.ccApi.MESSAGES, new List<ccrz.cc_bean_Message>{
                        message
                });
            }
        } 
        
        return inputData;
    }
    
	global override virtual Map<String, Object> createExternalUser (Map<String, Object> inputData) {
        String OldformData = (String) inputData.get('usrRegisterJson');
        Map<String, Object> Oldresult = (Map<String, Object>)JSON.deserializeUntyped(OldformData);
        String accountNumber = '';
        String billingZipCode = '';
        String jobTitle = '';
        Boolean accountNumberFlag = false;
        if(Oldresult.containsKey('accountNumberFlag')) {
            accountNumberFlag = (Boolean) Oldresult.get('accountNumberFlag');
        }
        User usr = (User) inputData.get('user');
        if(accountNumberFlag) {
            usr.Email = usr.Email + '.invalid';
        }
        inputData = super.createExternalUser(inputData);
		User userObject = (User) inputData.get('user');
        if (accountNumberFlag){
            userObject.id = (id)inputData.get('usrId');
            userObject.IsActive = false;
            CCFPUserUtil.updateUser(JSON.serialize(userObject));
        }
        //ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','CreatedExternalUser outputData:' + inputData );
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','CreatedExternalUser userObject:' + userObject );
        String formData = (String) inputData.get('usrRegisterJson');
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(formData);
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','CreatedExternalUser formData:' + result );        
        if(result.containsKey('jobTitle')) {
            jobTitle = (String) result.get('jobTitle');
        }
        if(result.containsKey('accountNumber')) {
            accountNumber = (String) result.get('accountNumber');
        }
        if(result.containsKey('billingZipCode')) {
            billingZipCode = (String) result.get('billingZipCode');
        }
        if(result.containsKey('accountNumberFlag')) {
            accountNumberFlag = (Boolean) result.get('accountNumberFlag');
        }
        if(Test.isRunningTest()) {
           userObject = [Select Id, Email, ContactId from User where UserName = 'signup@gmail.com'] ; 
        }
        Contact con = [Select AccountId, Title,User_Entered_Account_Number__c,User_Entered_Zip_Code__c from Contact where Id=:userObject.contactId];
        if(String.isNotBlank(jobTitle) || String.isNotBlank(accountNumber) || String.isNotBlank(billingZipCode)) {
        	con.Title = jobTitle;
            con.User_Entered_Account_Number__c = accountNumber;
            con.User_Entered_Zip_Code__c = billingZipCode;
        	update con;
        }
		
        if(!accountNumberFlag){
            CC_FP_Contract_Account_Permission_Matrix__c contactPermissionMatrix = new CC_FP_Contract_Account_Permission_Matrix__c();
            contactPermissionMatrix.Contact__c = userObject.contactId;
            contactPermissionMatrix.Account__c = con.AccountId;
            contactPermissionMatrix.Allow_Override_ShipTo__c = false;
            contactPermissionMatrix.Allow_AR_Payments__c = false;
            contactPermissionMatrix.Allow_Access_to_Sales_Reports__c = false;
            contactPermissionMatrix.Allow_Access_To_Accounting_Reports__c = false;
            contactPermissionMatrix.Allow_Backorders__c = false;
            contactPermissionMatrix.Allow_Non_Free_Shipping_Orders__c = false;
            contactPermissionMatrix.Allow_Backorder_Release__c = false;
            contactPermissionMatrix.Can_View_Invoices__c = false;
            contactPermissionMatrix.Power_Deals_Visibility__c = false;
            if(accountNumberFlag == true) {
                contactPermissionMatrix.Allow_Checkout__c = false;
                contactPermissionMatrix.Pricing_Visibility__c = false;
            } else if (accountNumberFlag == false && (String.isNotBlank(accountNumber) || String.isNotBlank(billingZipCode)) ) {
                contactPermissionMatrix.Allow_Checkout__c = true;
                contactPermissionMatrix.Pricing_Visibility__c = true;
            } else if (accountNumberFlag == false && String.isBlank(accountNumber) && String.isBlank(billingZipCode)) {
                contactPermissionMatrix.Allow_Checkout__c = true;
                contactPermissionMatrix.Pricing_Visibility__c = true;            
            } 
            insert contactPermissionMatrix;
        }
		return inputData;
	} 
	global override virtual Map<String, Object> createAccount (Map<String, Object> inputData) {        
        String accountNumber = '';
        String billingZipCode = '';
        Boolean accountNumberFlag = false;
        String iSeriesCustomerAccount = '';
        String iSeriesCustomerBranch = '0';
        String formData = (String) inputData.get('usrRegisterJson');
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(formData);
        
        if(result.containsKey('accountNumber')) {
            accountNumber = (String) result.get('accountNumber');
            if(String.isNotBlank(accountNumber)) {
                if(accountNumber.contains('-')) {
                    String[] iSeriesInfo = accountNumber.split('-');
                    iSeriesCustomerAccount = String.valueOf(Integer.valueOf(iSeriesInfo[0]));
                    iSeriesCustomerBranch = String.valueOf(Integer.valueOf(iSeriesInfo[1]));
                } else {
                    iSeriesCustomerAccount = String.valueOf(Integer.valueOf(accountNumber));
                }
            }
        }

        if(result.containsKey('billingZipCode')) {
            billingZipCode = (String) result.get('billingZipCode');
        }
        if(result.containsKey('accountNumberFlag')) {
            accountNumberFlag = (Boolean) result.get('accountNumberFlag');
        }
        
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','createAccount accountNumber:' + accountNumber );
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','createAccount billingZipCode:' + billingZipCode );
        ccrz.cclog.log(System.LoggingLevel.INFO,'M:X','createAccount accountNumberFlag:' + accountNumberFlag );
        Account acct;
        if(accountNumberFlag == true) {
            List<Account> acctList = [Select Id, Name from Account where AccountNumber = '99999999-999' ];
            if(acctList.size() > 0) {
                acct = acctList[0];
            }                
        } else if(accountNumberFlag == false && (String.isNotBlank(billingZipCode) || String.isNotblank(iSeriesCustomerAccount))) {
            List<Account> acctList = [Select Id, Name from Account where Iseries_Company_code__c = '1' and ISeries_Customer_Account__c = :iSeriesCustomerAccount and ISeries_Customer_Branch__c = :iSeriesCustomerBranch and BillingPostalCode like :billingZipCode + '%'];
            if(acctList.size() > 0) {
                acct = acctList[0];
            }
        }
        if(acct != null) {
            inputData.put(ccrz.ccApiAccount.ACCOUNT_NEW, acct);
        } else {
            inputData = super.createAccount(inputData);  
        }
		return inputData;
	}

}