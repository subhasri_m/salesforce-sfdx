@isTest
private class CCAviHttpUtilTest {

    private static void testEmptyConstructor(){
        CCAviHttpUtil util = new CCAviHttpUtil();
        Integer statusCode = util.statusCode;
        String status = util.status;
        
        System.assertEquals(util.requestContentType, null);
        System.assertEquals(util.endpoint, null);
        System.assertEquals(util.body, null);
        System.assertEquals(util.method, CCAviHttpUtil.REQUEST_TYPE.GET);
        System.assertEquals(statusCode, null);
        System.assertEquals(status, null);
    } 

    @isTest
    private static void testException(){
        CCAviHttpUtil util = new CCAviHttpUtil();
        CCAviHttpUtilTestMock testMock = new CCAviHttpUtilTestMock();
        testMock.throwException = true;
        Test.setMock(HttpCalloutMock.class, testMock);
        Boolean threwException = false;
        Test.startTest();
        util.submitRequest();
        Test.stopTest();
        System.assert(util.requestErrors);
    }

    @isTest
    private static void testFullConstructor(){
        Test.setMock(HttpCalloutMock.class, new CCAviHttpUtilTestMock());
        Test.startTest();
        CCAviHttpUtil util = new CCAviHttpUtil('http://avionos.com', CCAviHttpUtil.CONTENT_TYPE.JSON, '{ "userName":"test@avionos.com”, "password": "password", "firstName":"firstName","lastName":"lastName","email":"email@avionos.com"}');
        util.addCookie('AuthToken', 'MyAuth');
        util.addQueryParameter('isOrganization', 'true');
        util.setBody('test');
        util.addQueryParameter('param1', 'value1');
        util.addQueryParameter('param2', 'value2');
        util.submitRequest();
        Test.stopTest();
        System.assertEquals(200, util.statusCode);
        System.assertEquals('OK', util.status);
    }

    @isTest
    private static void testCookie(){
        PageReference pageRef = new PageReference('/');
        Cookie c = new Cookie('testkey', 'testvalue', null, -1, false);
        pageRef.setCookies(new LIST<Cookie>{c});

        Test.setCurrentPage(pageRef);
        Test.setMock(HttpCalloutMock.class, new CCAviHttpUtilTestMock());
        Test.startTest();
        CCAviHttpUtil util = new CCAviHttpUtil('http://avionos.com', CCAviHttpUtil.CONTENT_TYPE.JSON, '{ "userName":"test@avionos.com”, "password": "password", "firstName":"firstName","lastName":"lastName","email":"email@avionos.com"}');
        util.addCookie('AuthToken', 'MyAuth');
        util.addQueryParameter('isOrganization', 'true');
        util.setBody('test');
        util.addQueryParameter('param1', 'value1');
        util.addQueryParameter('param2', 'value2');
        util.submitRequest();
        Test.stopTest();
        System.assertEquals(200, util.statusCode);
        System.assertEquals('OK', util.status);
        System.assertEquals('testvalue', util.getCookieValue('testkey'));
    }

    @isTest
    private static void testResetState(){
        Test.setMock(HttpCalloutMock.class, new CCAviHttpUtilTestMock());
        Test.startTest();
        CCAviHttpUtil util = new CCAviHttpUtil('http://avionos.com', CCAviHttpUtil.CONTENT_TYPE.JSON, '{ "userName":"test@avionos.com”, "password": "password", "firstName":"firstName","lastName":"lastName","email":"email@avionos.com"}');
        util.addCookie('AuthToken', 'MyAuth');
        util.addQueryParameter('isOrganization', 'true');
        util.setBody('test');
        util.addQueryParameter('param1', 'value1');
        util.addQueryParameter('param2', 'value2');
        util.submitRequest();
        util.resetState();
        Test.stopTest();

        System.assertEquals(util.requestContentType, null);
        System.assertEquals(util.endpoint, null);
        System.assertEquals(util.body, null);
        System.assertEquals(util.method, null);
        System.assertEquals(util.statusCode, null);
        System.assertEquals(util.status, null);

    }

    public class CCAviHttpUtilTestMock implements HttpCalloutMock {
        public CCAviHttpUtilTestMock() {
            throwException = false;
        }
    
        public HTTPResponse respond(HttpRequest request){
            if(throwException){
                throw new CCAviHttpUtil.CCAviHttpUtilsException('Test Exception');
            }
            HttpResponse response = new HttpResponse();
            response.setStatusCode(200);
            response.setStatus('OK');
            response.setHeader('set-cookie','testkey=testvalue');
            return response;
        }
    
        public Boolean throwException {get; set;}
    }   
}