global class Batch_ReAssignAccount_Owner implements Schedulable {
   global void execute(SchedulableContext sc) {
      Id batchInstanceID = Database.executeBatch(new ReAssignAccount_Owner());
   }
}