@isTest(seealldata=true)
public class    TriggerOnCartItemTest {
    
     private static testMethod void unitTest_1()
     {
     
         List<ccrz__E_CartItem__c> cartitems = [select id,ccrz__Price__c from ccrz__E_CartItem__c where ccrz__Price__c!=0 order by createddate desc limit 1];
         
         for(ccrz__E_CartItem__c CI: cartitems)
         {
             CI.ccrz__Price__c = 0;
         }
         
         update cartitems;
         
     }

}