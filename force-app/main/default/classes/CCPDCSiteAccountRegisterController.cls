global with sharing class CCPDCSiteAccountRegisterController {
  @RemoteAction
  global static ccrz.cc_RemoteActionResult addAccountRequest(ccrz.cc_RemoteActionContext ctx, String data){
    ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
    try{
      Map<String,Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
      CC_PDC_Account_Request__c account_request = new CC_PDC_Account_Request__c();
      // fields go below
      account_request.Accounts_Payable_Supervisor__c = (String)input.get('accountsPayableSupervisor');
      account_request.Accounts_Payable_Supervisor_Phone__c = (String)input.get('supervisorPhoneNumber');
      account_request.Business_Address__c = ((String)input.get('businessAddress1')).trim() + ' ' + ((String)input.get('businessAddress2')).trim();
      account_request.BusinessCity__c = (String)input.get('businessCity');
      account_request.Business_Credit_App_Requested__c = Boolean.valueOf(input.get('creditOpitions'));
      account_request.Business_Fax_Number__c = (String)input.get('businessFaxNumber');
      account_request.Business_Name__c = (String)input.get('Business Name'); //customerBusinessName
      account_request.Business_Phone_Number__c = (String)input.get('businessPhoneNumber');
      account_request.BusinessState__c = (String)input.get('businessState');
      account_request.BusinessZipCode__c = (String)input.get('businessZipcode');
      account_request.First_Principal_Address__c = ((String)input.get('firstPrincipalAddress1')).trim() + ' '+ ((String)input.get('firstPrincipalAddress2')).trim();
      account_request.First_Principal_City__c = (String)input.get('firstPrincipalCity');
      account_request.First_Principal_Email__c = (String)input.get('firstPrincipalEmail');
      account_request.First_Principal_Name__c = (String)input.get('firstPrincipalName');
      account_request.First_Principal_Phone_Number__c = (String)input.get('firstPrincipalPhoneNumber');
      account_request.First_Principal_Role__c = (String)input.get('firstPrincipalRole');
      account_request.First_Principal_State__c = (String)input.get('firstPrincipalState');
      account_request.First_Principal_ZipCode__c = (String)input.get('firstPrincipalZipcode');


     account_request.Principal_Owners_Name__c = (String)input.get('Principal Owners Name');//principalOwnersName
     account_request.Reseller_Tax_ID__c = (String)input.get('resellerTaxID');

      account_request.Second_Principal_Address__c = (String)input.get('secondPrincipalAddress');
      account_request.Second_Principal_City__c = (String)input.get('secondPrincipalCity');
      account_request.Second_Principal_Email__c = (String)input.get('secondPrincipalEmail');
      account_request.Second_Principal_Name__c = (String)input.get('secondPrincipalName');
      account_request.Second_Principal_Phone_Number__c = (String)input.get('secondPrincipalPhoneNumber');
      account_request.Second_Principal_Role__c = (String)input.get('secondPrincipalRole');
      account_request.Second_Principal_State__c = (String)input.get('secondPrincipalState');
      account_request.Second_Principal_ZipCode__c = (String)input.get('secondPrincipalZipcode');
      account_request.Shipping_Address_is_Business_Address__c = Boolean.valueOf(input.get('shipAddressBusAddress'));
     if(Boolean.valueOf(input.get('shipAddressBusAddress'))){
        account_request.Mailing_Address__c = ((String)input.get('businessAddress1')).trim() + ' ' + ((String)input.get('businessAddress2'));
        account_request.MailingCity__c = (String)input.get('businessCity');
        account_request.MailingState__c = (String)input.get('businessState');
        account_request.MailingZipCode__c = (String)input.get('businessZipcode');
     } else {
        account_request.Mailing_Address__c = ((String)input.get('businessMailAddress1')).trim() + ' ' + ((String)input.get('businessMailAddress2'));
        account_request.MailingCity__c = (String)input.get('mailAddressCity');
        account_request.MailingState__c = (String)input.get('mailAddressState');
        account_request.MailingZipCode__c = (String)input.get('mailAddressZipcode');
     }
      account_request.SSN_EIN__c = (String)input.get('SsnOrEinNumber');
      account_request.Type_of_Business__c = (String)input.get('typeOfBusiness-options');
      account_request.Type_of_Business_Detail__c = (String)input.get('detail-type-of-business');
      account_request.Website__c = (String)input.get('website');
      account_request.Years_of_Operation__c = (String)input.get('years-range');

      if(Boolean.valueOf(input.get('creditOpitions'))){
          account_request.Bank_Name__c = (String)input.get('bankName');
          account_request.Bank_Address__c = ((String)input.get('bankAddressline1')).trim() + ' ' + ((String)input.get('bankAddressline2')).trim();
          account_request.Bank_City__c = (String)input.get('bankCity');
          account_request.Bank_State__c = (String)input.get('bankState');
          account_request.Bank_ZipCode__c = (String)input.get('bankZipcode');
          account_request.Bank_Officer_Name__c = (String)input.get('bankOfficerName');
          account_request.Bank_Officer_Phone__c = (String)input.get('bankPhoneNumber');
          account_request.Bank_Officer_Email__c = (String)input.get('bankerEmail');
          account_request.Bank_Account_Type__c = (String)input.get('accountType');
          account_request.Bank_Account_Number__c = (String)input.get('accountNumber');
          account_request.Trader_1_Name__c   = (String)input.get('trader1Name');
          account_request.Trader_1_Address__c = ((String)input.get('trader1Addressline1')).trim() + ' ' + ((String)input.get('trader1Addressline2'));
          account_request.Trader_1_City__c   = (String)input.get('trader1City');
          account_request.Trader_1_State__c  = (String)input.get('trader1State');
          account_request.Trader_1_Zipcode__c  = (String)input.get('trader1Zipcode');
          account_request.Trader_1_Phone__c  = (String)input.get('trader1PhoneNumber');
          account_request.Trader_2_Name__c   = (String)input.get('trader2Name');
          account_request.Trader_2_Address__c = ((String)input.get('trader2Addressline1')).trim() + ' ' + ((String)input.get('trader2Addressline2'));
          account_request.Trader_2_City__c   = (String)input.get('trader2City');
          account_request.Trader_2_State__c  = (String)input.get('trader2State');
          account_request.Trader_2_Zipcode__c  = (String)input.get('trader2Zipcode');
          account_request.Trader_2_Phone__c  = (String)input.get('trader2PhoneNumber');
          account_request.Trader_3_Name__c   = (String)input.get('trader3Name');
          account_request.Trader_3_Address__c  = ((String)input.get('trader3Addressline1')).trim() + ' ' + ((String)input.get('trader3Addressline2'));
          account_request.Trader_3_City__c   = (String)input.get('trader3City');
          account_request.Trader_3_State__c  = (String)input.get('trader3State');
          account_request.Trader_3_Zipcode__c  = (String)input.get('trader3Zipcode');
          account_request.Trader_3_Phone__c  = (String)input.get('trader3PhoneNumber');

          account_request.Trader_4_Name__c   = (String)input.get('trader4Name');
          account_request.Trader_4_Address__c  = ((String)input.get('trader4Addressline1')).trim() + ' ' + ((String)input.get('trader4Addressline2'));
          account_request.Trader_4_City__c   = (String)input.get('trader4City');
          account_request.Trader_4_State__c  = (String)input.get('trader4State');
          account_request.Trader_4_Zipcode__c  = (String)input.get('trader4Zipcode');
          account_request.Trader_4_Phone__c  = (String)input.get('trader4PhoneNumber');
          account_request.Credit_Request__c  = Decimal.valueOf((String)input.get('creditLimitRequest')).setScale(2,RoundingMode.HALF_UP);
          account_request.Require_Purchase_Order__c  = Boolean.valueOf(input.get('purchaseOrder'));
          account_request.Sales_Tax_Exempt__c  = Boolean.valueOf(input.get('salesExempt'));
          account_request.Sign_Date__c   = date.today();
          account_request.Customer_Sign_Name__c  = (String)input.get('signedCustomerName');

          account_request.Agreement_Checkbox__c  = Boolean.valueOf(input.get('agreeTerms'));
          account_request.Authorize_PDC_for_Bank_Info__c  = Boolean.valueOf(input.get('infoGathering'));
      }

      
      upsert account_request;
      CCAviPageUtils.buildResponseData(response, true, new Map<String, Object>{'record' => account_request});
    }
    catch (Exception e) {
      CCAviPageUtils.buildResponseData(response, false,
              new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
      );
    }
    return response;

  }


  @RemoteAction
    global static  String doUploadAttachment(String acctReqId, String attachmentBody, String attachmentName, String attachmentId) {
        Attachment att = getAttachment(attachmentId);
        
        String newBody = '';
        if(att.Body != null) {
            newBody = EncodingUtil.base64Encode(att.Body);
        }
        newBody += attachmentBody;
        att.Body = EncodingUtil.base64Decode(newBody);
        if(attachmentId == null) {
            att.Name = attachmentName;
            att.parentId = acctReqId;
        }
        upsert att;
        return att.Id;
    }

    private static Attachment getAttachment(String attId) {
        list<Attachment> attachments = [SELECT Id, Body
                                        FROM Attachment 
                                        WHERE Id =: attId];
        if(attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }


}