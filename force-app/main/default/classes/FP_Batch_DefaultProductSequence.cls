global class FP_Batch_DefaultProductSequence implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('Select Id, Name, ccrz__Sequence__c From ccrz__E_Product__c Where ccrz__Sequence__c= null');
    }
    global void execute(Database.BatchableContext BC, List<ccrz__E_Product__c> scope){
        For(ccrz__E_Product__c Prod : scope){
            prod.ccrz__Sequence__c = 99999;
        }
        update scope;
    }
    global void finish(Database.BatchableContext BC){
        System.debug(System.LoggingLevel.DEBUG, 'Finish Executed');
    }
}