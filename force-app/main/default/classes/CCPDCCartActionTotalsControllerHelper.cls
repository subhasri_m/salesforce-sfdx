global without sharing class CCPDCCartActionTotalsControllerHelper {
    public static List<CC_FP_Contract_Account_Permission_Matrix__c> getAdminsForAccount(Id accountId) {
        //String accountId = ccrz.cc_CallContext.effAccountId;
        Set<Id> accountIdsSet = new Set<Id>();
        For(Account acc : [Select Id, ParentId From Account Where Id=:accountId]){
            IF(acc.ParentId <> null){
                accountIdsSet.add(acc.ParentId);
            }
        }
        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = [
            SELECT 
                Id, 
                Name,
                Account__c,
                Allow_Checkout__c,
                Contact__c,
                Contact__r.Email,                                
                Is_Admin__c
            FROM 
                CC_FP_Contract_Account_Permission_Matrix__c
            WHERE
                (Account__c = :accountId OR Account__c = :accountIdsSet) AND (Is_Admin__c = TRUE)
        ];
        return permissions;
    }
}