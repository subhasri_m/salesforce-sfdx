@isTest
public class CCPDCMyAccountMyWishlistControllerTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.SERVICE_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccServiceCart' => 'c.CCPDCServiceCart'
                }
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void getWishlistsTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__EffectiveAccountID__c = ccrz.cc_CallContext.currAccountId;
        cart.ccrz__CartType__c = 'WishList';
        update cart;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyWishlistController.getWishlists(ctx);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void changeCartOwnerTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz.cc_CallContext.currCartId = cart.Id;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyWishlistController.changeCartOwner(ctx, cart.Id);
        Test.stopTest();

        System.assert(result != null);
      //  System.assert(result.success);
    }

    static testmethod void cloneCartTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz.cc_CallContext.currCartId = cart.Id;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyWishlistController.cloneCart(ctx, cart.Id);
        Test.stopTest();

        System.assert(result != null);
       // System.assert(result.success);
        System.assert(result.data != null);
    }

    static testmethod void editWishlistTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz.cc_CallContext.currCartId = cart.Id;
        String data = '{"name":"Test name","note":"Test note","isActive":true}';

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyWishlistController.editWishlist(ctx, cart.Id, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
        ccrz__E_Cart__c updatedCart = [SELECT Id, ccrz__EncryptedId__c, ccrz__Name__c, ccrz__Note__c, ccrz__ActiveCart__c FROM ccrz__E_Cart__c WHERE Id = :cart.Id];
        System.assertEquals('Test name', updatedCart.ccrz__Name__c);
        System.assert(updatedCart.ccrz__ActiveCart__c);
    }
    
    static testmethod void addAllItemsToCartTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz.cc_CallContext.currCartId = cart.Id;       
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ctx.currentCartId = 'enc123';
        ccrz.cc_RemoteActionResult result = null;
        
        Test.startTest();
        result = CCPDCMyAccountMyWishlistController.addAllItemsToCart(ctx, cart.Id);
        Test.stopTest();
        System.assert(result != null);
       // System.assert(result.success);
        System.assert(result.data != null);

    }

    static testmethod void addToCartTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyWishlistController.addToCart(ctx, 'product-01', 1);
        Test.stopTest();

        System.assert(result != null);
      //  System.assert(result.success);
    }

}