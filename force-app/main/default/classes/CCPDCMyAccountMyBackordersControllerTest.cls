@isTest
public class CCPDCMyAccountMyBackordersControllerTest {
	static CCPDCTestUtil util = new CCPDCTestUtil();
	static Map<String, Object> m = new Map<String, Object>();
    ccrz__E_CartItem__c item;
    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            m = util.initData();
        }        
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

	@isTest static void testGetBackorderCartItems() {
        ccrz__E_CartItem__c item = [SELECT Id,ccrz__Quantity__c FROM ccrz__E_CartItem__c];
        item.ccrz__Quantity__c = 50;
        update item;
        
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
		ccrz.cc_RemoteActionResult result = null;
		ccrz__E_Cart__c cart = util.getCart();
		cart.ccrz__CartType__c = 'BackOrder';
		update cart;
		result = CCPDCMyAccountMyBackordersController.getBackorderCartItems(ctx);
		Map<String,Object> backcartitem = (Map<String, Object>)result.data;
        List<Map<String,String>> returnedData = (List<Map<String,String>>)backcartitem.get('backorderProducts');
		System.assertEquals(returnedData.get(0).get('warehouseStatus'),'Backordered');
		System.assert(result != null);
        System.assert(result.success);
  
	}
    
    @isTest static void testGetBackorderItemInstock(){
        ccrz__E_CartItem__c item = [SELECT Id,ccrz__Quantity__c FROM ccrz__E_CartItem__c];
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
		ccrz.cc_RemoteActionResult result = null;
		ccrz__E_Cart__c cart = util.getCart();
		cart.ccrz__CartType__c = 'BackOrder';
		update cart;
       	result = CCPDCMyAccountMyBackordersController.getBackorderCartItems(ctx);
		Map<String,Object> backcartitem = (Map<String, Object>)result.data;
        List<Map<String,String>> returnedData = (List<Map<String,String>>)backcartitem.get('backorderProducts');

		System.assertEquals(returnedData.get(0).get('warehouseStatus'),'In Stock');
		System.assert(result != null);
        System.assert(result.success);
    }

    @isTest static void testGetBackorderItemExceptions(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
		ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        List<ccrz__E_ProductInventoryItem__c> invList = [SELECT Id FROM ccrz__E_ProductInventoryItem__c WHERE Name = 'ext1' OR Name ='ext2'];
        delete invList;

        List<ccrz__E_Product__c> products = [SELECT Id FROM ccrz__E_Product__c];
        update products;

        result = CCPDCMyAccountMyBackordersController.getBackorderCartItems(ctx);
        Test.stopTest();
        ccrz__E_Cart__c cart = util.getCart();
        delete cart;
        
        result = CCPDCMyAccountMyBackordersController.getBackorderCartItems(ctx);
		System.assert(!result.success);
		
    }

	@isTest static void testAddAllItemToCartException(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
		ccrz.cc_RemoteActionResult result = null;
		String input = '{"1":["product-01","3","22.00","66.00"],"2":["product-02","1","149.00","149.00"]}';
        result = CCPDCMyAccountMyBackordersController.addBockOrderItemsToCart(ctx,input);
        System.assert(!result.success);      
	}
	
	@isTest static void testAddAllItemToCart(){
		util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
		ccrz.cc_RemoteActionResult result = null;
        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__CartType__c = 'BackOrder';
        cart.ccrz__ActiveCart__c = false;
        update cart;
		String input = '{"1":["product-01","3","22.00","66.00","'+cart.Id+'"]}';
        String cartId = CCAviCartManager.getActiveCartId();
        ctx.currentCartId = cartId;
        Test.startTest();
        result = CCPDCMyAccountMyBackordersController.addBockOrderItemsToCart(ctx,input);
        System.assert(result.success); 
        Test.stopTest();
	}
    
    @isTest static void testDeleteItemToCart(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
		ccrz.cc_RemoteActionResult result = null;
        ccrz__E_Cart__c cart = util.getCart();
        result = CCPDCMyAccountMyBackordersController.deleteItemFromBackOrderCart(ctx,'product-01');
        System.assert(!result.success);
        
        cart.ccrz__CartType__c = 'BackOrder';
		update cart;
        result = CCPDCMyAccountMyBackordersController.deleteItemFromBackOrderCart(ctx,'product-01');
        System.assert(result.success);
    }

	
}