public with sharing class CCPDCPricingHelper {   
    
     public static CCAviBoomiAPI.SeriesPricingWSResponse getPrice(String sku, Integer quantity) {
         
          ccrz__E_Product__c p = [SELECT  Id, Name, Pool_Number__c, ccrz__SKU__c,Part_Number__c, Salespack__c FROM ccrz__E_Product__c WHERE ccrz__SKU__c =: sku LIMIT 1];
          if(p != null) {
                    List<Map<String,Object>> productList = new List<Map<String,Object>>();
                     Map<String, Object> prodMap = new Map<String, Object>();
                     prodMap.put('SKU',sku);
                     prodMap.put('poolNumber', p.Pool_Number__c);
                     prodMap.put('partNumber',p.Part_Number__c);
                     prodMap.put('quantity',quantity);
                     productList.add(prodMap);
                     return price(productList);
                }
           return null;
     }
	 public static CCAviBoomiAPI.SeriesPricingWSResponse anonymousPrice(List<Map<String,Object>> prodList, String location) {
        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        if (prodList != null && !prodList.isEmpty()) {

            List<CCAviBoomiAPI.SeriesPricingWSRequest> requests = new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
            for (Map<String,Object> obj: prodList) {
                CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                request.AccountNumber = '999999';
                request.CustomerBranch = '0';
                request.Location = location;
                request.PoolNumber = (String) obj.get('poolNumber');
                request.PartNumber = (String) obj.get('partNumber');
                Object quantity = obj.get('quantity');
                if(quantity == null) {
                    request.Quantity = '1';
                } else {
                    request.Quantity = String.valueOf(quantity);
                }
                request.Price = '0';
                request.SPCIND = ' ';
                if(ccrz.cc_CallContext.storefront == 'pdc') {
                    request.COMPY = '2';
                } else {
                    request.COMPY = '1';
                }
                requests.add(request);
            }

            response = CCAviBoomiAPI.getISeriesPricingWS(ccrz.cc_CallContext.storefront, requests);
        }
        return response;
    }
     public static CCAviBoomiAPI.SeriesPricingWSResponse priceForSpecificLocation(List<Map<String,Object>> prodList, String location) {
        Long beginOfSearch = DateTime.now().getTime();
        ccrz.ccLog.log('pricing-service CCPDCPricingHelper begin of price1: ' + beginOfSearch);          
        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        if (prodList != null && !prodList.isEmpty()) {

            List<CCAviBoomiAPI.SeriesPricingWSRequest> requests = new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Account effAccount = CCPDCAccountDAO.getAccount(accountId);
            if (effAccount != null && effAccount.ISeries_Customer_Account__c != null && effAccount.ISeries_Customer_Branch__c != null) {
                for (Map<String,Object> obj: prodList) {
                    String sku = (String) obj.get('SKU');
                    CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                    request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                    request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                    request.Location = location;
                    request.PoolNumber = (String) obj.get('poolNumber');
                    request.PartNumber = (String) obj.get('partNumber');  
                    Object quantity = obj.get('quantity');
                    if(quantity == null) {
                        request.Quantity = '1';
                    } else {
                       request.Quantity = String.valueOf(quantity); 
                    }                    
                    request.Price = '0';
                    request.SPCIND = ' ';
                    if(ccrz.cc_CallContext.storefront == 'pdc') {
                        request.COMPY = '2';
                    } else {
                        request.COMPY = '1';
                    }
					requests.add(request);
                }

                ccrz.ccLog.log(JSON.serializePretty(requests));
                ccrz.ccLog.log('$$$$$$$$$$$$$$$$$$$$$$$ Pricing Call1 $$$$$$$$$$$$$$$$$$$$$$$$$');      
                response = CCAviBoomiAPI.getISeriesPricingWS(ccrz.cc_CallContext.storefront, requests);
                ccrz.ccLog.log(JSON.serializePretty(response));
            }
        }
        Long endOfSearch = DateTime.now().getTime();
        //ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price1: ' + endOfSearch);
        ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price1 diff: ' + (endOfSearch - beginOfSearch));             
        return response;
    }
     public static CCAviBoomiAPI.SeriesPricingWSResponse price(List<Map<String,Object>> prodList) {
        Long beginOfSearch = DateTime.now().getTime();
        ccrz.ccLog.log('pricing-service CCPDCPricingHelper begin of price1: ' + beginOfSearch);          
        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        if (prodList != null && !prodList.isEmpty()) {
			List<CCAviBoomiAPI.SeriesPricingWSRequest> requests = new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Account effAccount = CCPDCAccountDAO.getAccount(accountId);
            if (effAccount != null && effAccount.ISeries_Customer_Account__c != null && effAccount.ISeries_Customer_Branch__c != null && effAccount.Ship_from_Location__c != null) {
                for (Map<String,Object> obj: prodList) {
                    String sku = (String) obj.get('SKU');
                    CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                    request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                    request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                    request.Location = effAccount.Ship_from_Location__c;
                    request.PoolNumber = (String) obj.get('poolNumber');
                    request.PartNumber = (String) obj.get('partNumber');  
                    Object quantity = obj.get('quantity');
                    if(quantity == null) {
                        request.Quantity = '1';
                    } else {
                       request.Quantity = String.valueOf(quantity); 
                    }                    
                    request.Price = '0';
                    request.SPCIND = ' ';
                    if(ccrz.cc_CallContext.storefront == 'pdc') {
                        request.COMPY = '2';
                    } else {
                        request.Location = CCPDCCartDAO.getCartLocationCode(ccrz.cc_CallContext.currCartId);
                        request.COMPY = '1';
                    }
					requests.add(request);
                }

                ccrz.ccLog.log(JSON.serializePretty(requests));
                ccrz.ccLog.log('$$$$$$$$$$$$$$$$$$$$$$$ Pricing Call1 $$$$$$$$$$$$$$$$$$$$$$$$$');      
                response = CCAviBoomiAPI.getISeriesPricingWS(ccrz.cc_CallContext.storefront, requests);
                ccrz.ccLog.log(JSON.serializePretty(response));
            }
        }
        Long endOfSearch = DateTime.now().getTime();
        //ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price1: ' + endOfSearch);
        ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price1 diff: ' + (endOfSearch - beginOfSearch));             
        return response;
    }
	 public static CCAviBoomiAPI.SeriesPricingWSResponse price(List<Object> data) {
        return price(data, null);
    }
	 public static CCAviBoomiAPI.SeriesPricingWSResponse price(List<Object> data, Map<String, Map<String, Decimal>> qMap) {
        Long beginOfSearch = DateTime.now().getTime();
        ccrz.ccLog.log('pricing-service CCPDCPricingHelper begin of price2: ' + beginOfSearch);           
        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        if (data != null && !data.isEmpty()) {

            List<CCAviBoomiAPI.SeriesPricingWSRequest> requests = new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Account effAccount = CCPDCAccountDAO.getAccount(accountId);
            if (effAccount != null && effAccount.ISeries_Customer_Account__c != null && effAccount.ISeries_Customer_Branch__c != null && effAccount.Ship_from_Location__c != null) {
                Map<String, Map<String, Decimal>> quantityMap = qMap;
                if (qMap == null) {
                    quantityMap = getQuantityMap(data);
                }
                Map<String, Decimal> quantitySkuMap = quantityMap.get('sku');
                Map<String, Decimal> quantityIdMap = quantityMap.get('sfid');

                if (quantitySkuMap != null && !quantitySkuMap.isEmpty()) {
                    Map<String, ccrz__E_Product__c> productMap = CCPDCProductDAO.getProductsForPricingBySku(quantitySkuMap.keySet());
                    for (String k : quantitySkuMap.keySet()) {
                        ccrz__E_Product__c product = productMap.get(k);
                        if (product != null) {
                            CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                            request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                            request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                            request.Location = effAccount.Ship_from_Location__c;
                            request.PoolNumber = product.Pool_Number__c;
                            request.PartNumber = product.Part_Number__c;
                            request.Quantity = String.valueOf(quantitySkuMap.get(k));
                            request.Price = '0';
                            request.SPCIND = ' ';
                            if(ccrz.cc_CallContext.storefront == 'pdc') {
                               request.COMPY = '2';
                            } else {
                               request.COMPY = '1';
                            }
                            requests.add(request);
                        }
                    }
                }

                if (quantityIdMap != null && !quantityIdMap.isEmpty()) {
                    Map<String, ccrz__E_Product__c> productMap = CCPDCProductDAO.getProductsForPricingById(quantityIdMap.keySet());
                    for (String k : quantityIdMap.keySet()) {
                        ccrz__E_Product__c product = productMap.get(k);
                        if (product != null) {
                            CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                            request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                            request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                            request.Location = effAccount.Ship_from_Location__c;
                            request.PoolNumber = product.Pool_Number__c;
                            request.PartNumber = product.Part_Number__c;
                            request.Quantity = String.valueOf(quantityIdMap.get(k));
                            request.Price = '0';
                            request.SPCIND = ' ';
                            if(ccrz.cc_CallContext.storefront == 'pdc') {
                               request.COMPY = '2';
                            } else {
                               request.COMPY = '1';
                            }
                            requests.add(request);
                        }
                    }
                }

                ccrz.ccLog.log(JSON.serializePretty(requests));
                ccrz.ccLog.log('$$$$$$$$$$$$$$$$$$$$$$$ Pricing Call2 $$$$$$$$$$$$$$$$$$$$$$$$$');      
                response = CCAviBoomiAPI.getISeriesPricingWS(ccrz.cc_CallContext.storefront, requests);
                ccrz.ccLog.log(JSON.serializePretty(response));
                Long endOfSearch = DateTime.now().getTime();
                //ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price2: ' + endOfSearch);
                ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price2 diff: ' + (endOfSearch - beginOfSearch));                 
            }
        }
        return response;
    }
	
     public static CCAviBoomiAPI.SeriesPricingWSResponse price(List<ccrz__E_CartItem__c> cartItems) {
        return price(cartItems, null);
    }

    public static CCAviBoomiAPI.SeriesPricingWSResponse price(List<ccrz__E_CartItem__c> cartItems, Map<String, Map<String, Decimal>> qMap) {
        Long beginOfSearch = DateTime.now().getTime();
        ccrz.ccLog.log('pricing-service CCPDCPricingHelper begin of price3: ' + beginOfSearch);         
        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        if (cartItems != null && !cartItems.isEmpty()) {

            List<CCAviBoomiAPI.SeriesPricingWSRequest> requests = new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Account effAccount = CCPDCAccountDAO.getAccount(accountId);
            if (effAccount != null && effAccount.ISeries_Customer_Account__c != null && effAccount.ISeries_Customer_Branch__c != null && effAccount.Ship_from_Location__c != null) {
                Map<String, Map<String, Decimal>> quantityMap = qMap;
                if (qMap == null) {
                    quantityMap = getQuantityMap(cartItems);
                }
                Map<String, Decimal> quantityIdMap = quantityMap.get('sfid');

                if (quantityIdMap != null && !quantityIdMap.isEmpty()) {
                    Map<String, ccrz__E_Product__c> productMap = CCPDCProductDAO.getProductsForPricingById(quantityIdMap.keySet());
                    for (String k : quantityIdMap.keySet()) {
                        ccrz__E_Product__c product = productMap.get(k);
                        if (product != null) {
                            CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                            request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                            request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                            request.Location = effAccount.Ship_from_Location__c;
                            request.PoolNumber = product.Pool_Number__c;
                            request.PartNumber = product.Part_Number__c;
                            request.Quantity = String.valueOf(quantityIdMap.get(k));
                            request.Price = '0';
                            request.SPCIND = ' ';
                            if(ccrz.cc_CallContext.storefront == 'pdc') {
                               request.COMPY = '2';
                            } else {
                               request.COMPY = '1';
                               request.Location = CCPDCCartDAO.getCartLocationCode(ccrz.cc_CallContext.currCartId);
                            }
                            requests.add(request);
                        }
                    }
                }

                ccrz.ccLog.log(JSON.serializePretty(requests));
                ccrz.ccLog.log('$$$$$$$$$$$$$$$$$$$$$$$ Pricing Call3 $$$$$$$$$$$$$$$$$$$$$$$$$');      
                response = CCAviBoomiAPI.getISeriesPricingWS(ccrz.cc_CallContext.storefront, requests);
                ccrz.ccLog.log(JSON.serializePretty(response));
            }
        }
                Long endOfSearch = DateTime.now().getTime();
                //ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price3: ' + endOfSearch);
                ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price3 diff: ' + (endOfSearch - beginOfSearch));         
        return response;
    }
	 
     public static CCAviBoomiAPI.SeriesPricingWSResponse priceCoreProducts(List<ccrz__E_RelatedProduct__c> relatedProducts) {
        Long beginOfSearch = DateTime.now().getTime();
        ccrz.ccLog.log('pricing-service CCPDCPricingHelper begin of price3: ' + beginOfSearch);         
        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        if (relatedProducts != null && !relatedProducts.isEmpty()) {
            List<CCAviBoomiAPI.SeriesPricingWSRequest> requests = new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Account effAccount = CCPDCAccountDAO.getAccount(accountId);
            if (effAccount != null && effAccount.ISeries_Customer_Account__c != null && effAccount.ISeries_Customer_Branch__c != null && effAccount.Ship_from_Location__c != null) {
                    for (ccrz__E_RelatedProduct__c r  : relatedProducts) {
                        if (r.ccrz__Product__c != null) {
                            CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                            request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                            request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                            request.Location = effAccount.Ship_from_Location__c;
                            request.PoolNumber = r.ccrz__RelatedProduct__r.Pool_Number__c;
                            request.PartNumber = r.ccrz__RelatedProduct__r.Part_Number__c;
                            request.Quantity = '1';
                            request.Price = '0';
                            request.SPCIND = ' ';
                            if(ccrz.cc_CallContext.storefront == 'pdc') {
                               request.COMPY = '2';
                            } else {
                               request.COMPY = '1';
                               request.Location = CCPDCCartDAO.getCartLocationCode(ccrz.cc_CallContext.currCartId);
                            }
                            requests.add(request);
                        }
                    }
                ccrz.ccLog.log(JSON.serializePretty(requests));
                ccrz.ccLog.log('$$$$$$$$$$$$$$$$$$$$$$$ Pricing Call3 $$$$$$$$$$$$$$$$$$$$$$$$$');      
                response = CCAviBoomiAPI.getISeriesPricingWS(ccrz.cc_CallContext.storefront, requests);
                ccrz.ccLog.log(JSON.serializePretty(response));
            }
        }
        
         Long endOfSearch = DateTime.now().getTime();
         //ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price3: ' + endOfSearch);
         ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price3 diff: ' + (endOfSearch - beginOfSearch));         
        return response;
    }
    
    public static Map<String, Map<String, Decimal>> getQuantityMap(List<Object> lineData) {
        Map<String, Map<String, Decimal>> quantityMap = new Map<String, Map<String, Decimal>>();
        Map<String, Decimal> quantitySkuMap = new Map<String, Decimal>();
        quantityMap.put('sku', quantitySkuMap);
        Map<String, Decimal> quantityIdMap = new Map<String, Decimal>();
        quantityMap.put('sfid', quantityIdMap);
        if (lineData != null) {
            for (Object o : lineData) {
                if (o instanceof ccrz.ccApiCart.LineData) {
                    ccrz.ccApiCart.LineData line = (ccrz.ccApiCart.LineData) o;
                    if (line.sku != null) {
                        quantitySkuMap.put(line.sku, line.quantity);
                    }
                    else if (line.sfid != null) {
                        quantityIdMap.put(line.sfid, line.quantity);
                    }
                }
                else {
                    Map<String, Object> line = (Map<String, Object>) o;
                    String sku = (String) line.get('sku');
                    String sfid = (String) line.get('sfid');
                    if (sku != null) {
                        quantitySkuMap.put(sku, (Decimal) line.get('quantity'));
                    }
                    else if (sfid != null) {
                        quantitySkuMap.put(sfid, (Decimal) line.get('quantity'));
                    }
                }
            }
        }
        return quantityMap;
    }

    public static Map<String, Map<String, Decimal>> getQuantityMap(List<ccrz__E_CartItem__c> cartItems) {
        Map<String, Map<String, Decimal>> quantityMap = new Map<String, Map<String, Decimal>>();
        Map<String, Decimal> quantitySkuMap = new Map<String, Decimal>();
        quantityMap.put('sku', quantitySkuMap);
        Map<String, Decimal> quantityIdMap = new Map<String, Decimal>();
        quantityMap.put('sfid', quantityIdMap);
        if (cartItems != null && !cartItems.isEmpty()) {
            for (ccrz__E_CartItem__c item : cartItems) {
                Decimal quantity = quantityIdMap.get(item.ccrz__Product__c);
                if (quantity == null) {
                    quantity = 0;
                }
                quantity += item.ccrz__Quantity__c;
                quantityIdMap.put(item.ccrz__Product__c, quantity);
           }
        }
        return quantityMap;
    }

    public static Map<String, Decimal> createPriceMap(CCAviBoomiAPI.SeriesPricingWSResponse response) {
        Map<String, Decimal> priceMap = new Map<String, Decimal>();
        if (response != null && response.success && response.listPricing != null) {
            for (CCAviBoomiAPI.Pricing p : response.listPricing) {
                if (p.ErrorMessage == null && p.PRICE != null) {
                    String key = p.PoolNumber + p.PartNumber;
                    priceMap.put(key, p.PRICE);
                }
            }
        }
        return priceMap;
    }

    public static void priceProductList(List<Map<String,Object>> prodList) {
        CCAviBoomiAPI.SeriesPricingWSResponse response = CCPDCPricingHelper.price(prodList);
        if (response != null && response.success && response.listPricing != null) {
            Map<String, Decimal> priceMap = CCPDCPricingHelper.createPriceMap(response);

            if (priceMap != null) {
                for (Map<String,Object> obj: prodList) {
                    String partNumber = (String) obj.get('partNumber');
                    String poolNumber = (String) obj.get('poolNumber');
                    if (partNumber != null && poolNumber != null) {
                        String key = poolNumber + partNumber;
                        Decimal price = priceMap.get(key);
                        if (price != null) {
                            ccrz.ccLog.log(price);
                            obj.put('price', price);
                        }
                    }
                }
            }
        }

    }

	//jlowenthal@salesforce.com BEGIN refactor for continuations
    public static HttpRequest priceWithCont(List<Map<String,Object>> prodList) {
        HttpRequest httpRequest = new HttpRequest();
        if (prodList != null && !prodList.isEmpty()) {

            List<CCAviBoomiAPI.SeriesPricingWSRequest> requests = new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Account effAccount = CCPDCAccountDAO.getAccount(accountId);
            if (effAccount != null && effAccount.ISeries_Customer_Account__c != null && effAccount.ISeries_Customer_Branch__c != null && effAccount.Ship_from_Location__c != null) {
                for (Map<String,Object> obj: prodList) {
                    String sku = (String) obj.get('SKU');
                    CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                    request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                    request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                    request.Location = effAccount.Ship_from_Location__c;
                    request.PoolNumber = (String) obj.get('poolNumber');
                    request.PartNumber = (String) obj.get('partNumber');
                    request.Quantity = '1';
                    request.Price = '0';
                    request.SPCIND = ' ';
                    if(ccrz.cc_CallContext.storefront == 'pdc') {
                        request.COMPY = '2';
                    } else {
                        request.COMPY = '1';
                    }
                    requests.add(request);
                }

                ccrz.ccLog.log(JSON.serializePretty(requests));
                ccrz.ccLog.log('$$$$$$$$$$$$$$$$$$$$$$$ Pricing Call1 $$$$$$$$$$$$$$$$$$$$$$$$$');      
                httpRequest = CCAviBoomiAPI.getISeriesPricingWSWithCont(ccrz.cc_CallContext.storefront, requests);
            }
        }
            
        return httpRequest;
    }

    public static HttpRequest priceWithCont(List<Object> data) {
        return priceWithCont(data, null);
    }

    public static HttpRequest priceWithCont(List<Object> data, Map<String, Map<String, Decimal>> qMap) {
        HttpRequest httpRequest = new HttpRequest();
        
        if (data != null && !data.isEmpty()) {
            List<CCAviBoomiAPI.SeriesPricingWSRequest> requests = new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Account effAccount = CCPDCAccountDAO.getAccount(accountId);
            if (effAccount != null && effAccount.ISeries_Customer_Account__c != null && effAccount.ISeries_Customer_Branch__c != null && effAccount.Ship_from_Location__c != null) {
                Map<String, Map<String, Decimal>> quantityMap = qMap;
                if (qMap == null) {
                    quantityMap = getQuantityMap(data);
                }
                Map<String, Decimal> quantitySkuMap = quantityMap.get('sku');
                Map<String, Decimal> quantityIdMap = quantityMap.get('sfid');

                if (quantitySkuMap != null && !quantitySkuMap.isEmpty()) {
                    Map<String, ccrz__E_Product__c> productMap = CCPDCProductDAO.getProductsForPricingBySku(quantitySkuMap.keySet());
                    for (String k : quantitySkuMap.keySet()) {
                        ccrz__E_Product__c product = productMap.get(k);
                        if (product != null) {
                            CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                            request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                            request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                            request.Location = effAccount.Ship_from_Location__c;
                            request.PoolNumber = product.Pool_Number__c;
                            request.PartNumber = product.Part_Number__c;
                            request.Quantity = String.valueOf(quantitySkuMap.get(k));
                            request.Price = '0';
                            request.SPCIND = ' ';
                            if(ccrz.cc_CallContext.storefront == 'pdc') {
                               request.COMPY = '2';
                            } else {
                               request.COMPY = '1';
                            }
                            requests.add(request);
                        }
                    }
                }

                if (quantityIdMap != null && !quantityIdMap.isEmpty()) {
                    Map<String, ccrz__E_Product__c> productMap = CCPDCProductDAO.getProductsForPricingById(quantityIdMap.keySet());
                    for (String k : quantityIdMap.keySet()) {
                        ccrz__E_Product__c product = productMap.get(k);
                        if (product != null) {
                            CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                            request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                            request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                            request.Location = effAccount.Ship_from_Location__c;
                            request.PoolNumber = product.Pool_Number__c;
                            request.PartNumber = product.Part_Number__c;
                            request.Quantity = String.valueOf(quantityIdMap.get(k));
                            request.Price = '0';
                            request.SPCIND = ' ';
                            if(ccrz.cc_CallContext.storefront == 'pdc') {
                               request.COMPY = '2';
                            } else {
                               request.COMPY = '1';
                            }
                            requests.add(request);
                        }
                    }
                }

                ccrz.ccLog.log(JSON.serializePretty(requests));
                ccrz.ccLog.log('$$$$$$$$$$$$$$$$$$$$$$$ Pricing Call2 $$$$$$$$$$$$$$$$$$$$$$$$$');      
                httpRequest = CCAviBoomiAPI.getISeriesPricingWSWithCont(ccrz.cc_CallContext.storefront, requests);
            }
        }
        return httpRequest;
    }

    public static HttpRequest priceWithCont(List<ccrz__E_CartItem__c> cartItems) {
        return priceWithCont(cartItems, null);
    }

    public static HttpRequest priceWithCont(List<ccrz__E_CartItem__c> cartItems, Map<String, Map<String, Decimal>> qMap) {
        HttpRequest httpRequest = new HttpRequest();

        Long beginOfSearch = DateTime.now().getTime();
        ccrz.ccLog.log('pricing-service CCPDCPricingHelper begin of price3: ' + beginOfSearch);         
        if (cartItems != null && !cartItems.isEmpty()) {

            List<CCAviBoomiAPI.SeriesPricingWSRequest> requests = new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Account effAccount = CCPDCAccountDAO.getAccount(accountId);
            if (effAccount != null && effAccount.ISeries_Customer_Account__c != null && effAccount.ISeries_Customer_Branch__c != null && effAccount.Ship_from_Location__c != null) {
                Map<String, Map<String, Decimal>> quantityMap = qMap;
                if (qMap == null) {
                    quantityMap = getQuantityMap(cartItems);
                }
                Map<String, Decimal> quantityIdMap = quantityMap.get('sfid');

                if (quantityIdMap != null && !quantityIdMap.isEmpty()) {
                    Map<String, ccrz__E_Product__c> productMap = CCPDCProductDAO.getProductsForPricingById(quantityIdMap.keySet());
                    for (String k : quantityIdMap.keySet()) {
                        ccrz__E_Product__c product = productMap.get(k);
                        if (product != null) {
                            CCAviBoomiAPI.SeriesPricingWSRequest request = new CCAviBoomiAPI.SeriesPricingWSRequest();
                            request.AccountNumber = effAccount.ISeries_Customer_Account__c;
                            request.CustomerBranch = effAccount.ISeries_Customer_Branch__c;
                            request.Location = effAccount.Ship_from_Location__c;
                            request.PoolNumber = product.Pool_Number__c;
                            request.PartNumber = product.Part_Number__c;
                            request.Quantity = String.valueOf(quantityIdMap.get(k));
                            request.Price = '0';
                            request.SPCIND = ' ';
                            if(ccrz.cc_CallContext.storefront == 'pdc') {
                               request.COMPY = '2';
                            } else {
                               request.COMPY = '1';
                            }
                            requests.add(request);
                        }
                    }
                }

                ccrz.ccLog.log(JSON.serializePretty(requests));
                ccrz.ccLog.log('$$$$$$$$$$$$$$$$$$$$$$$ Pricing Call3 $$$$$$$$$$$$$$$$$$$$$$$$$');      
                httpRequest = CCAviBoomiAPI.getISeriesPricingWSWithCont(ccrz.cc_CallContext.storefront, requests);
            }
        }
                Long endOfSearch = DateTime.now().getTime();
                //ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price3: ' + endOfSearch);
                ccrz.ccLog.log('pricing-service CCPDCPricingHelper end of price3 diff: ' + (endOfSearch - beginOfSearch));         
        return httpRequest;
    }
    
    public static CCAviBoomiAPI.SeriesPricingWSResponse priceWithContCallback(Object state) {
    	return CCAviBoomiAPI.getISeriesPricingWSContCallback(state);
    }

}