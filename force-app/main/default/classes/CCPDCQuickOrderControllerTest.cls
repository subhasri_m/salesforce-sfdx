@isTest
public class CCPDCQuickOrderControllerTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void addBulkTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c,DSP_Part_Number__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        product.DSP_Part_Number__c = 'product-01';
        update product;        

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickOrderController.addBulk(ctx, CCPDCQuickOrderHelperTest.TEST_JSON_REQUEST_2);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void addBulkContTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c,DSP_Part_Number__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        product.DSP_Part_Number__c = 'product-01';
        update product;        

        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation)CCPDCQuickOrderController.addBulkCont(ctx, CCPDCQuickOrderHelperTest.TEST_JSON_REQUEST_2);
        if (conti != NULL) {
            // Verify that the continuation has the proper requests
            Map<String, HttpRequest> requests = conti.getRequests();
            system.assert(requests.size() == 1);
            //system.assert(requests.get((String)CCPDCProductDetailSimpleController.stateMap.get('RequestLabel')) != null);
            
            // Perform mock callout 
            // (i.e. skip the callout and call the callback method)
            HttpResponse response = new HttpResponse();
            response.setBody('Mock response body');   
            // Set the fake response for the continuation     
            //Test.setContinuationResponse((String)CCPDCProductDetailSimpleController.stateMap.get('RequestLabel'), response);
            // Invoke callback method
            Object result = Test.invokeContinuationMethod(new CCPDCQuickOrderController(), conti);
        }
    }

    static testmethod void addBulkErrorTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickOrderController.addBulk(ctx, CCPDCQuickOrderHelperTest.TEST_JSON_REQUEST);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
    }

    static testmethod void validateTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c,DSP_Part_Number__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        product.DSP_Part_Number__c = 'product-01';
        update product;
        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickOrderController.validate(ctx, CCPDCQuickOrderHelperTest.TEST_JSON_REQUEST_2);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void validateErrorTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCQuickOrderController.validate(ctx, CCPDCQuickOrderHelperTest.TEST_JSON_REQUEST);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
    }

}