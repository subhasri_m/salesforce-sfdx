@isTest
public class CCPDCMyAccountNavControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
             Map<String, Object> m = util.initData();
             Account a = util.getAccount();
             a.Account_Status__c = 'OPEN';
             update a;
        }

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            util.createPermissions(true)
        };
        insert data;

        CCAviBillTrustSettings__c settings = new CCAviBillTrustSettings__c();
        settings.Name = 'DefaultStore';
        settings.URL_For_eInvoiceConnect__c = 'https://fleetpride.billtrust.com';
        settings.PB__c = 'N';
        settings.EN__c = 'Y'; 
        settings.EC__c = 'Y'; 
        settings.ClientGUID__c = '1234'; 
        settings.ETYPE__c = '0';  
        insert settings;
    }
    static testmethod void testConstructor() {
        util.initCallContext();

        CCPDCMyAccountNavController controller = null;

        Test.startTest();
        controller = new CCPDCMyAccountNavController();
        Test.stopTest();

        System.assert(controller.billConnectURL != null);
    }
}