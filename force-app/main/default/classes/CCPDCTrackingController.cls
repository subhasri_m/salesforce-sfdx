global with sharing class CCPDCTrackingController {
    public string Carrier {get;set;}
    public string TrackingNumber {get;set;}
    public String URL {get;set;}
    public Boolean URLok {get;set;}
    
    public CCPDCTrackingController() {
        URLoK = false;
    }
    public PageReference getCarrierURL()
    {
        try{
            if(Apexpages.Currentpage().getparameters().get('trackingNumber')!=null && Apexpages.Currentpage().getparameters().get('carriercode')!=null) {
                TrackingNumber = Apexpages.Currentpage().getparameters().get('trackingNumber');
                Carrier = (Apexpages.Currentpage().getparameters().get('carriercode')).toUpperCase();
                
            } else {
                TrackingNumber = 'DUMMY';
                Carrier = 'DUMMY';
            }
            Switch on Carrier {
                when 'UPS' {
                    URLoK = true;
                    URL= 'https://www.ups.com/track?loc=en_US&tracknum='
                        + TrackingNumber;
                    
                }
                when 'AACT'{
                    URLoK = true;
                    URL= 'http://www.aaacooper.com/Transit/ProTrackResults.aspx?ProNum='
                        + TrackingNumber
                        + '&AllAccounts=true';
                    
                }
                when 'RNLO'{
                    URLoK = true;
                    URL= 'https://m.rlcarriers.com/ShipmentTracing/?rstn='
                        + TrackingNumber;
                    
                }
                when 'CNWY'{
                    URLoK = true;
                    URL= 'https://track.aftership.com/con-way/'
                        + TrackingNumber;
                    
                }
                when 'FEDEX'{
                    URLoK = true;
                    URL='https://www.fedex.com/apps/fedextrack/index.html?tracknumbers='
                        + TrackingNumber;
                    
                }
                when 'CENF' {
                    URLoK = true;
                    URL = 'http://www.centralfreight.com/website/mf/mfInquiry.aspx?inqmode=PRO&pro='
                        + TrackingNumber;
                    
                }
                when 'FXFE'{
                    URLoK = true;
                    URL='https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber='
                        + TrackingNumber;
                    
                }
                when 'LTLG'{
                    URLoK = true;
                    URL= 'https://my.yrc.com/tools/track/shipments?referenceNumber='
                        + TrackingNumber
                        + '&referenceNumberType=PRO';
                    
                }
                when 'SEFL'{
                    URLoK = true;
                    URL= 'https://www.sefl.com/webconnect/tracing?Type=PN&RefNum1='
                        + TrackingNumber;
                }
                when 'SAIA'{
                    URLoK = true;
                    URL = 'https://www.saia.com/tracing/ajaxprostatusbypro.aspx?nh=N&Pro='
                        + TrackingNumber;
                    
                    
                }
                when 'DHRN'{
                    URLoK = true;
                    URL='https://www.dohrn.com/tools/track-by-pro-number?wbtn=PRO&prolist='
                        + TrackingNumber;
                    
                }
                when 'SPEDE'{
                    URLoK = true;
                    URL = 'http://speedeedelivery.com/track-a-shipment/?v=detail&barcode='
                        + TrackingNumber ;
                }
                
                when else {
                    URLok=false;
                    
                }
            }
        }
        catch (Exception e){
            URLok=false;  
        }
        
        return null;
    }
}