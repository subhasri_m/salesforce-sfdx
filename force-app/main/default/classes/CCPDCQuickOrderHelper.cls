public with sharing class CCPDCQuickOrderHelper {

    public static List<QuickOrderItem> getQuickOrderItems(List<QuickOrderItem> items) {
        List<String> parts = new List<String>();
        List<QuickOrderItem> validItems = new List<QuickOrderItem>();
        Map<String, QuickOrderItem> partMap = new Map<String, QuickOrderItem>();
        for (QuickOrderItem item : items) {
            if (!String.isBlank(item.partNumber)) {
                item.partNumber = item.partNumber.trim();
                item.isConflict = false;
                System.debug('CCPDCQuickOrderHelper: '+item.selected);
                if(item.selected !=null && item.selected != ''){
                    parts.add(item.selected);
                    
                }else {
                    parts.add(item.partNumber);
                }

                partMap.put(item.partNumber, item);
                validItems.add(item);
            }
        }
        List<ccrz__E_Product__c> products = CCPDCProductDAO.getPartsDsp(parts, ccrz.cc_CallContext.storefront);
        for (ccrz__E_Product__c p : products) {
            for (QuickOrderItem item : validItems) {
                if (item.isMatch(p)) {
                    if (item.record == null) {
                        if (item.isConflict == null) {
                            item.isConflict = false;
                        }
                        if (item.isConflict) {
                            item.conflicts.add(p);
                        }
                        else {
                            item.record = p;
                            item.sku = p.ccrz__SKU__c;
                        }
                    }
                    else {
                        if (item.conflicts == null) {
                            item.conflicts = new List<ccrz__E_Product__c>();
                            item.conflicts.add(item.record);
                        }
                        item.conflicts.add(p);
                        if (item.selected != null) {
                            if (item.selected == p.ccrz__SKU__c) {
                                item.isConflict = false;
                                item.record = p;
                                item.sku = p.ccrz__SKU__c;
                            }
                            else if (item.selected != item.sku) {
                                item.isConflict = true;
                                item.record = null;
                                item.sku = null;
                            }
                        }
                        else {
                            item.isConflict = true;
                            item.record = null;
                            item.sku = null;
                        }
                    }

                    if(p.Salespack__c != null){
                        item.salesPackqty = Integer.valueOf(p.Salespack__c);
                    } else {
                        item.salesPackqty = 1;
                    }
                    if(p.ccrz__ProductStatus__c != null && p.ccrz__ProductStatus__c != 'Released'){
                        item.isConflict = true;
                    }
                    break;
                }
            }
        }
        return validItems;
    }

    public static Boolean validate(List<QuickOrderItem> items) {
        Boolean isValid = true;
        for (QuickOrderItem item : items) {
            if(item.salesPackqty == 0){
               item.salesPackqty = 1; 
            }
            if (item.sku == null || item.quantity == null || item.isConflict == null || item.isConflict || math.mod(Integer.valueOf(item.quantity),item.salesPackqty)!= 0) {
                item.isInvalid = true;
                isValid = false;
            }
        }
        return isValid;
    }

    public static String addToCart(List<QuickOrderItem> items) {
        String cartEncId = null;
        Boolean wasSuccessful = true;

        List<ccrz.ccApiCart.LineData> newLines = new List<ccrz.ccApiCart.LineData>();
        for (QuickOrderItem item : items) {
            if (item.sku == null || item.quantity == null) {
                wasSuccessful = false;
            }
            else {
                ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
                newLine.sku = item.sku;
                newLine.quantity = item.quantity;
                if(item.boomiPrice != null){
                   newLine.price = item.boomiPrice;
                }
                newLines.add(newLine);
            }
        }

        if (wasSuccessful) {                    
            String cartId = CCAviCartManager.getActiveCartId();
            if (cartId != null) {
                Map<String, Object> request = new Map<String,Object>{
                    ccrz.ccApi.API_VERSION => 6, 
                    ccrz.ccApiCart.CART_ENCID => cartId,
                    ccrz.ccApiCart.LINE_DATA => newLines
                };
        
                Map<String, Object> outputData = CCPDCCartManager.addTo(request);
                wasSuccessful = (Boolean)outputData.get(ccrz.ccApi.SUCCESS);
                if (wasSuccessful) {
                    cartEncId = cartId;
                }
            }
        }

        return cartEncId;
    }

    public static List<QuickOrderItem> parseQuickOrderItem(String json) {
        List<QuickOrderItem> items = (List<QuickOrderItem>) System.JSON.deserialize(json, List<QuickOrderItem>.class);
        return items;
    }

    public static List<QuickOrderItem> parseQuickOrderRequest(String requestString) {
        List<QuickOrderItem> items = new List<QuickOrderItem>();
        List<String> lines = requestString.split('\n');
        for (String s : lines) {
            List<String> values = s.split(',');
            QuickOrderItem item = new QuickOrderItem();
            item.partNumber = values[0];
            item.quantity = Integer.valueOf(values[1]);
            items.add(item);
        }
        return items;
    }

    public static String createQuickOrderRequest(List<QuickOrderItem> items) {
        String request = '';
        for (QuickOrderItem item : items) {
            request += item.partNumber + ',' + item.quantity + '\n';
        }
        return request;
    }

    public class QuickOrderItem {
        public String sku {get; set;}
        public Integer quantity {get; set;}
        public String partNumber {get; set;}
        public ccrz__E_Product__c record {get; set;}
        public Boolean isConflict {get; set;}
        public Boolean isInvalid {get; set;}
        public String selected {get; set;}
        public List<ccrz__E_Product__c> conflicts {get; set;}
        public Integer salesPackqty {get; set;}
        public Decimal boomiPrice {get; set;}

        public QuickOrderItem() {
            this.isConflict = false;
            this.selected = '';
        }

        public Boolean isMatch(ccrz__E_Product__c product) {
            Boolean isMatch = false;
            //if (product.ccrz__SKU__c == partNumber || product.Name == partNumber || product.Part_Number__c == partNumber) {
            if (product.DSP_Part_Number__c == partNumber || product.ccrz__SKU__c == partNumber || product.Name == partNumber || product.Part_Number__c == partNumber) {
                isMatch = true;
            }
            return isMatch;
        }
    }
}