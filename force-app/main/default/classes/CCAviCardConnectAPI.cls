public with sharing class CCAviCardConnectAPI {

    public static CardConnectResponse zeroAuthorization(String storefront, String account, String experation, String cvv2, String name) {
        return authorization(storefront, account, experation, cvv2, name, '0', null);
    }

    public static CardConnectResponse fpZeroAuthorization(String storefront, String account, String experation, String cvv2, String name, String address, String city, String region, String country, String postal) {
        return fpAuthorization(storefront, account, experation, cvv2, name, '0', null, address, city, region, country, postal);
    }
    
    public static CardConnectResponse fpAuthorization(String storefront, String account, String experation, String cvv2, String name, String amount, String orderId, String address, String city, String region, String country, String postal) {
		String encryptedCartID = ccrz.cc_CallContext.currCartId;
        CardConnectResponse response = new CardConnectResponse();
		AuthorizationRequest request = new AuthorizationRequest();
        try {
            CCAviCardConnectSettings__c settings =  CCAviCardConnectSettings__c.getInstance(storefront);
             
            CCAviHttpUtil httpRequest = new CCAviHttpUtil();
            httpRequest.method = CCAviHttpUtil.REQUEST_TYPE.PUT;
            httpRequest.requestContentType = CCAviHttpUtil.CONTENT_TYPE.JSON;
            httpRequest.endpoint = 'callout:' + settings.Named_Credential_Name__c + settings.AUTH_PATH__c;
			request.merchid = settings.Merchant_Id__c;
            request.account = account;
            request.expiry = experation;
            request.amount = amount;
            request.currency_x = 'USD';
            request.address = address;
            request.city = city;
            request.region = region;
            request.country = country;
            request.postal = postal; 
            if (cvv2 != null) {
                request.cvv2 = cvv2;
            }
            if (name != null) {
                request.name = name;
            }
            if (orderId != null) {
                request.orderid = orderId;
            }

            String body = request.serialize();
            body = body.replace('"currency_x":', '"currency":');
            httpRequest.body = request.serialize();
            HttpResponse httpResponse = httpRequest.submitRequest();
			
            if (httpResponse.getStatusCode() == 200) {
                String responseBody =  httpResponse.getBody(); 
                System.debug(responseBody);
                if (String.isNotBlank(responseBody)) {
                    response.data = (AuthorizationResponse) JSON.deserialize(responseBody, AuthorizationResponse.class);
                    response.success = (response.data.respstat == 'A');
                    response.message = response.data.resptext;
                }
                response.responseCode  = httpResponse.getStatusCode();
                response.body = httpResponse.getBody();              
            }
            else {
                response.success = false;
                response.responseCode  = httpResponse.getStatusCode();
                response.body = httpResponse.getBody();
            }
        }
        catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, e);
            response.success = false;
            response.message = e.getMessage();
        }
      /* 
        if (!Test.isRunningTest()){
		ccrz__E_Cart__c currCart = CCPDCCartDAO.getCart(encryptedCartID);
      	currCart.CC_Cardconnect_Response__c= '"Success" : '+ String.valueOf(response.success)  +',  "Message" : ' + response.message +',  "ResponseCode" : ' +  String.valueOf(response.responseCode);
            update currCart;} */
        return response;
    }
    
    
    
    public static CardConnectResponse authorization(String storefront, String account, String experation, String cvv2, String name, String amount, String orderId) {
		String encryptedCartID = ccrz.cc_CallContext.currCartId;
        CardConnectResponse response = new CardConnectResponse();
		AuthorizationRequest request = new AuthorizationRequest();
        try {
            CCAviCardConnectSettings__c settings =  CCAviCardConnectSettings__c.getInstance(storefront);
             
            CCAviHttpUtil httpRequest = new CCAviHttpUtil();
            httpRequest.method = CCAviHttpUtil.REQUEST_TYPE.PUT;
            httpRequest.requestContentType = CCAviHttpUtil.CONTENT_TYPE.JSON;
            httpRequest.endpoint = 'callout:' + settings.Named_Credential_Name__c + settings.AUTH_PATH__c;
			request.merchid = settings.Merchant_Id__c;
            request.account = account;
            request.expiry = experation;
            request.amount = amount;
            request.currency_x = 'USD';
            if (cvv2 != null) {
                request.cvv2 = cvv2;
            }
            if (name != null) {
                request.name = name;
            }
            if (orderId != null) {
                request.orderid = orderId;
            }

            String body = request.serialize();
            body = body.replace('"currency_x":', '"currency":');
            httpRequest.body = request.serialize();
            HttpResponse httpResponse = httpRequest.submitRequest();
			
            if (httpResponse.getStatusCode() == 200) {
                String responseBody =  httpResponse.getBody(); 
                System.debug(responseBody);
                if (String.isNotBlank(responseBody)) {
                    response.data = (AuthorizationResponse) JSON.deserialize(responseBody, AuthorizationResponse.class);
                    response.success = (response.data.respstat == 'A');
                    response.message = response.data.resptext;
                }
                response.responseCode  = httpResponse.getStatusCode();
                response.body = httpResponse.getBody();
            }
            else {
                response.success = false;
                response.responseCode  = httpResponse.getStatusCode();
                response.body = httpResponse.getBody();
            }
        }
        catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, e);
            response.success = false;
            response.message = e.getMessage();
        }
      /* 
        if (!Test.isRunningTest()){
		ccrz__E_Cart__c currCart = CCPDCCartDAO.getCart(encryptedCartID);
      	currCart.CC_Cardconnect_Response__c= '"Success" : '+ String.valueOf(response.success)  +',  "Message" : ' + response.message +',  "ResponseCode" : ' +  String.valueOf(response.responseCode);
            update currCart;} */
        return response;
    }
		
    public class CardConnectResponse {
        public String message {get; set;}
        public Boolean success {get; set;}
        public Integer responseCode {get; set;}
        public String body {get; set;}

        public AuthorizationResponse data {get; set;}

    }

    public class AuthorizationRequest {
        public String merchid {get; set;} // Merchant Id, required for all requests
        public String accttype {get; set;} // One of PPAL, PAID, GIFT, PDEBIT, otherwise not required.
        public String account {get; set;} // Clear or Tokenized card number for payment card, Bank account number for ECheck, Masked in response
        public String expiry {get; set;} // Card Expiration in either MMYY or YYYYMMDD format, not required for ECHK
        public String amount {get; set;} // Amount with decimal or without decimal in currency minor units; i.e. USD Pennies, EUR Cents; Positive, Negative or Zero
        public String currency_x {get; set;} // USD for US dollars, CAD for Canadian Dollars, CAD Canadian Dollars, etc. Currency of merchant settlement.
        public String name {get; set;} // Account name; CCard optional, ECheck required
        public String address {get; set;} // Account street address, required for AVS
        public String city {get; set;} // Account city
        public String region {get; set;} // US State, Mexican State, Canadian Province
        public String country {get; set;} // Account country; 2 char country code, defaults to "US"
        public String phone {get; set;} // Account phone
        public String postal {get; set;} // Account postal code, defaults to "99999". If country is "US", must be 5 or 9 digits, otherwise any alphanumeric string is accepted.
        public String email {get; set;} // E-Mail address of the account holder
        public String ecomind {get; set;} // telephone or mail, R recurring, E ecommerce/Internet
        public String cvv2 {get; set;} // CVV2/CVC/CID value
        public String orderid {get; set;} // Source system order number
        public String track {get; set;} // Unencrypted Track1 OR unencrypted Track2 OR encrypted swipe data; containing Track1 and/or Track2; data.
        public String bankaba {get; set;} // Bank routing ABA number, Check only
        public String tokenize {get; set;} // Optional, Y to return an account token in token response field
        public String termid {get; set;} // Terminal Device ID
        public String capture {get; set;} // Optional, Y to capture the transaction for settlement if approved
        public String authcode {get; set;} // Authorization Code from original Authorization VoiceAuth
        public String signature {get; set;} // JSON escaped, Base64 encoded, Gzipped, BMP of signature data via Ingenico ISC250. Returned if authorization uses a token which has associated signature data or track with embedded signature data.
        public String taxexempt {get; set;} // If taxexempt is set to No for the transaction and a tax is not passed, default configuration data is used. If taxexempt is set to Yes, the default tax rate is used.        

        public String serialize() {
            return JSON.serialize(this);
        }
    }

    public class AuthorizationResponse {
        public String respstat {get; set;} // Status; A - Approved, B - Retry, C - Declined
        public String retref {get; set;} // Retrieval Reference Number; Unique identifying transaction number, used as merchant reference number.
        public String account {get; set;} // Account Number  19  Copied from request, masked
        public String token {get; set;} // (if requested) Token; A token that replaces the card number in capture and settlement requests if requested
        public String amount {get; set;} // Amount; Authorized amount. Same as the request amount for most approvals. The amount remaining on the card for prepaid/gift cards if partial authorization is enabled. Not relevant for declines.
        public String merchid {get; set;} // Merchant id; Copied from request
        public String respcode {get; set;} // Response Code; Alpha-numeric response code. See the tables in the PROCESSOR SPECIFIC responses. PPS – 2 Numeric, FNOR – 2 Alphanumeric
        public String resptext {get; set;} // Response text; Text description of response
        public String respproc {get; set;} // Response Processor; PPS - CardConnect internal response, FNOR - First Data North
        public String avsresp {get; set;} // AVS response code; Alpha-numeric AVS response. See the tables in the PROCESSOR SPECIFIC responses.
        public String cvvresp {get; set;} // CVV response code; See the tables in the PROCESSOR SPECIFIC RESPONSES section.
        public String authcode {get; set;} // Authorization code; Authorization Code from the Issuer
        public String signature {get; set;} // Signature Bitmap; JSON escaped, Base64 encoded, Gzipped, BMP of signature data via Ingenico ISC250. Returned if authorization used a token which had associated signature data or track with embedded signature data.
        public String commcard {get; set;} // Commercial Card Flag; Y if a Corporate or Purchase Card
        public String emv {get; set;} // Cryptogram - Authorization Response Cryptogram ARPC. This is returned only when EMV data is present within the Track Parameter.        
        
        public String serialize() {
            return JSON.serialize(this);
        }
    }

}