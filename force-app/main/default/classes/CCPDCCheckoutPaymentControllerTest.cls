@isTest
public class CCPDCCheckoutPaymentControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
    }

    static testmethod void updateCartTest() {
        util.initCallContext();

        List<String> responses = new List<String>{CCPDCOMSFulfillmentPlanAPITest.OMS_3_RESPONSE};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        cart.CC_FP_Split_Response__c = '{"orderId":"0ebff7e0-d1b8-4719-8cea-9c4ea6c19180","result":"Success","reservationId":"000000000560","homeDC":"AT","recheckOccurred":false,"shipments":[{"dc":"AT","carrier":"SAIA","expedited":false,"costToFP":128.82,"costToCustomer":0.0,"mode":"Less-Than-Truckload","transitTime":2,"lines":[{"partNo":"product-01","orderLineNo":"1","quantity":1}]}],"backOrderLines":[]}';
        cart.CC_FP_Tax_Rate__c = .06;
        update cart;
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.CC_FP_Line_Number__c = 1;
        update cartItem;
        String data = '{"sfid":"' + cart.Id + '","poNumber":"1234","cartItems":[{"itemID":"' + cartItem.Id + '","CCFPPONumber":"4321"}]}';

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCCheckoutPaymentController.updateCart(ctx, data);
        Test.stopTest();

        System.assert(result != null);
      //  System.assert(result.success);
        ccrz__E_Cart__c theCart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        System.assertEquals('1234', theCart.ccrz__PONumber__c);
        System.assertEquals('4321', theCart.ccrz__E_CartItems__r[0].CC_FP_PONumber__c);
    }
     static testmethod void updateCartFPTest() {
        util.initCallContext();

        List<String> responses = new List<String>{CCPDCOMSFulfillmentPlanAPITest.OMS_3_RESPONSE};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        cart.CC_FP_Split_Response__c = '{"orderId":"0ebff7e0-d1b8-4719-8cea-9c4ea6c19180","result":"Success","reservationId":"000000000560","homeDC":"AT","recheckOccurred":false,"shipments":[{"dc":"AT","carrier":"SAIA","expedited":false,"costToFP":128.82,"costToCustomer":0.0,"mode":"Less-Than-Truckload","transitTime":2,"lines":[{"partNo":"product-01","orderLineNo":"1","quantity":1}]}],"backOrderLines":[]}';
        cart.CC_FP_Tax_Rate__c = .06;
        update cart;
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.CC_FP_Line_Number__c = 1;
        update cartItem;
        String data = '{"sfid":"' + cart.Id + '","poNumber":"1234","cartItems":[{"itemID":"' + cartItem.Id + '","CCFPPONumber":"4321"}]}';

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCCheckoutPaymentController.updateCartFP(ctx, data);
        Test.stopTest();

        System.assert(result != null);
      //  System.assert(result.success);
        ccrz__E_Cart__c theCart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        System.assertEquals('1234', theCart.ccrz__PONumber__c);
        System.assertEquals('4321', theCart.ccrz__E_CartItems__r[0].CC_FP_PONumber__c);
    }


}