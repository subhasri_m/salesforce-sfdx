global class FP_BatchUpdateBlockedProduct  implements Database.Batchable<sObject>,schedulable  { 
     
  global void execute(SchedulableContext sc) {
    Database.executeBatch(new FP_BatchUpdateBlockedProduct(),100);
    }    
    
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('I have started');
         String query ='';   
        If(Test.isRunningTest()){
            query = 'Select id from ccrz__E_Product__c Limit 1000'; 
        }else{
            query = 'Select id from ccrz__E_Product__c where LastModifiedDate < Last_N_Days:3';  
        }    
        System.debug('Query--> '+query );
        return Database.getQueryLocator(query);
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<ccrz__E_Product__c>scope){
        
        List<ccrz__E_Product__c> ProductUpdateList  = new  List<ccrz__E_Product__c>();
        List<ccrz__E_Product__c> ProductDeletionList  = new  List<ccrz__E_Product__c>();
        List<ccrz__E_PriceListItem__c> PLIDeleteList = new List <ccrz__E_PriceListItem__c>();
        List<Product_Cross_Reference__c> PCRdeleteList = new List <Product_Cross_Reference__c>();
        List<ccrz__E_ProductInventoryItem__c> PIIdeleteList = new List <ccrz__E_ProductInventoryItem__c>();
        List<ccrz__E_ProductInventoryItem__c> PIIBlockeddeleteList = new List <ccrz__E_ProductInventoryItem__c>();
        
        Set<id> BlockedProductsIdsSet  = new Set<id>();
        Set<id> ProductDeletionIdsSet  = new  Set<id>(); 
        Set<id> InactiveProductIds = new Set<Id>(); 
        
        for (ccrz__E_Product__c Products : Scope)
        {
            InactiveProductIds.add(Products.id);
        }
        
 
        
        List<ccrz__E_CartItem__c> relatedCartItems = new List<ccrz__E_CartItem__c>();
        relatedCartItems = [Select id,ccrz__Product__c from ccrz__E_CartItem__c where ccrz__Product__c in :InactiveProductIds  ];//and ccrz__Cart__r.ccrz__ActiveCart__c=true
        
        For (ccrz__E_CartItem__c BlockedProducts : relatedCartItems )
        {
            BlockedProductsIdsSet.add(BlockedProducts.ccrz__Product__c);
        }
        
        For (id InactiveProId : InactiveProductIds)
        {
            If (!BlockedProductsIdsSet.Contains(InactiveProId))
            {
                ProductDeletionIdsSet.add(InactiveProId);
            }
            
        }
        
        
        PLIDeleteList = [select id,ccrz__Product__c from ccrz__E_PriceListItem__c where ccrz__Product__c in : ProductDeletionIdsSet];
        PCRdeleteList = [select id,Related_to_Product__c from Product_Cross_Reference__c where Related_to_Product__c in : ProductDeletionIdsSet ];
        PIIdeleteList = [select id,ccrz__ProductItem__c from ccrz__E_ProductInventoryItem__c where ccrz__ProductItem__c in : ProductDeletionIdsSet ];
        for (id DeleteProId : ProductDeletionIdsSet )
        {
            ccrz__E_Product__c DeleteProd = new ccrz__E_Product__c();
            DeleteProd.id = DeleteProId;            
            ProductDeletionList.add(DeleteProd);
        }
        
        
        for (id ProIds : BlockedProductsIdsSet )
        {
            ccrz__E_Product__c BlockedProd = new ccrz__E_Product__c();
            BlockedProd.id = ProIds;
            BlockedProd.ccrz__ProductStatus__c = 'Blocked';
            ProductUpdateList.add(BlockedProd);
        } 
        
        
         PIIBlockeddeleteList = [select id,ccrz__ProductItem__c from ccrz__E_ProductInventoryItem__c where ccrz__ProductItem__c in : BlockedProductsIdsSet ];
        
        If(!PLIDeleteList.Isempty()){  Delete PLIDeleteList; }
        
        If(!PCRdeleteList.Isempty()) {  Delete PCRdeleteList;}        
        
        If(!PIIdeleteList.Isempty()) {  Delete PIIdeleteList;}         
        
        
        If(!ProductUpdateList.isEmpty())
        {
            Update ProductUpdateList;
                If(!PIIBlockeddeleteList.Isempty()) {  Delete PIIBlockeddeleteList;}    
        }
        
        
        
        If(!ProductDeletionList.Isempty())
        {
            Delete ProductDeletionList;
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        
        
    }
}