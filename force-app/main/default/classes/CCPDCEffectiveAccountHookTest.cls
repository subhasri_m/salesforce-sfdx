@isTest
public class CCPDCEffectiveAccountHookTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void fetchAccountsTest() {
        util.initCallContext();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        CC_FP_Contract_Account_Permission_Matrix__c perms2 = util.createPermissions(false);
        Account a = new Account(Name = 'Extra');
        insert a;
        perms2.Account__c = a.Id;
        insert perms2;

        Map<String,Object> input = new Map<String,Object>();
        input.put(ccrz.cc_hk_EffectiveAccount.PARAM_ACCOUNT_ID, ccrz.cc_CallContext.currAccountId);
        input.put(ccrz.cc_hk_EffectiveAccount.PARAM_USER_ID, ccrz.cc_CallContext.currUserId);

        Map<String,Object> data = null;

        Test.startTest();
        CCPDCEffectiveAccountHook hook = new CCPDCEffectiveAccountHook();
        data = hook.fetchAccounts(input);
        Test.stopTest();

        System.assert(data != null);
    }

}