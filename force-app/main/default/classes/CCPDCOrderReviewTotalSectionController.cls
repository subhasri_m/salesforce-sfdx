global with sharing class CCPDCOrderReviewTotalSectionController {

    @RemoteAction
    global static ccrz.cc_RemoteActionResult setTaxRate(ccrz.cc_RemoteActionContext ctx) {
    	ccrz.cc_RemoteActionResult remoteresponse = CCAviPageUtils.remoteInit(ctx);
    	String storefront = ccrz.cc_CallContext.storefront;
		String cartEncId = ccrz.cc_CallContext.currCartId;
		String accountId = ccrz.cc_CallContext.effAccountId;

        try {
			ccrz__E_Cart__c thecart = CCPDCCartDAO.getCartOrderView(cartEncId);


			Account account = CCPDCAccountDAO.getAccount(accountId);
			// If Tax_Exempt__c, tax amount = 0; else use tax service from boomi

			if((Boolean)thecart.ccrz__TaxExemptFlag__c){
			    thecart.ccrz__TaxAmount__c = 0;
			} else {
			    String shipToLocationId = String.valueOf(thecart.ccrz__ShipTo__r.CC_FP_Location__c);
			    String billToLocationId = String.valueOf(thecart.ccrz__BillTo__r.CC_FP_Location__c);
			    
			    List<Location__c> shipToLocationObject = [SELECT Id,In_City__c FROM Location__c WHERE Id =:shipToLocationId];
			    Location__c shipTo = shipToLocationObject.get(0);
			    Boolean shipInCity = (Boolean)shipTo.In_City__c;
			    
			    List<Location__c> billToLocationObject = [SELECT Id,In_City__c FROM Location__c WHERE Id =:billToLocationId];
			    Location__c billTo = billToLocationObject.get(0);
			    Boolean billInCity = (Boolean)billTo.In_City__c;

			    
			    
			    List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest> lstRequests = new List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest>();
			    CCAviBoomiAPI.SeriesDefaultTaxRateRequest request;
			    request = new CCAviBoomiAPI.SeriesDefaultTaxRateRequest();
			    
			    request.COMPANY = String.valueOf(account.Iseries_Company_code__c);
			    request.CUSTOMER = String.valueOf(account.ISeries_Customer_Account__c);
			    request.BRANCH = String.valueOf(account.ISeries_Customer_Branch__c);
			    request.SHIPTOCITY = String.valueOf(thecart.ccrz__ShipTo__r.ccrz__City__c);
			    request.SHIPTOSTATE = String.valueOf(thecart.ccrz__ShipTo__r.ccrz__State__c);
			    request.SHIPTOZIP = String.valueOf(thecart.ccrz__ShipTo__r.ccrz__PostalCode__c);
			    request.SHIPTOCOUNTY = String.valueOf(thecart.ccrz__ShipTo__r.CC_FP_County__c);
			    request.SHIPTOINCITY = shipInCity ? '1' : '0';
			    request.SHIPFROMCITY = String.valueOf(thecart.CC_FP_Location__r.City__c);
			    request.SHIPFROMSTATE = String.valueOf(thecart.CC_FP_Location__r.State__c);
			    request.SHIPFROMZIP = String.valueOf(thecart.CC_FP_Location__r.Zipcode__c);
			    request.SHIPFROMCOUNTY = String.valueOf(thecart.CC_FP_Location__r.County__c);
		    	request.SHIPFROMINCITY = (Boolean)thecart.CC_FP_Location__r.In_City__c ? '1' : '0';

			    request.ORDERACCCITY = String.valueOf(thecart.ccrz__BillTo__r.ccrz__City__c);
			    request.ORDERACCSTATE = String.valueOf(thecart.ccrz__BillTo__r.ccrz__State__c);
			    request.ORDERACCZIP = String.valueOf(thecart.ccrz__BillTo__r.ccrz__PostalCode__c);
			    request.ORDERACCCOUNTY = String.valueOf(thecart.ccrz__BillTo__r.CC_FP_County__c);
			    request.ORDERACCINCITY = billInCity ? '1' : '0';

			    lstRequests.add(request);
			    CCAviBoomiAPI.SeriesDefaultTaxRateResponse response = CCAviBoomiAPI.getISeriesDefaultTaxRate(storefront, lstRequests);
			    System.debug(JSON.serialize(response));
			    if((Boolean)response.apiCallSuccess){
			    	List<CCAviBoomiAPI.SeriesDefaultTaxRate> taxRates = (List<CCAviBoomiAPI.SeriesDefaultTaxRate>)response.listDefaultTaxRates;
				    if(taxRates != null){
				    	CCAviBoomiAPI.SeriesDefaultTaxRate taxRate = taxRates.get(0);
				    	if(taxRate.ErrorMessage == null){
				    		Decimal taxRateNumber = taxRate.DefaultTaxRate;
					    	thecart.ccrz__TaxAmount__c = ((Decimal)thecart.ccrz__SubtotalAmount__c) * taxRateNumber;
					    	update thecart;
					    	CCAviPageUtils.buildResponseData(remoteresponse, true, new Map<String, Object>{'record' => thecart});   
				    	} else {
				    		CCAviPageUtils.buildResponseData(remoteresponse, false, new Map<String, Object>{'error' => taxRate.ErrorMessage}); 
				    	}

				    } 
			    }
			     
		    
			}

		}

		catch(Exception e){
            CCAviPageUtils.buildResponseData(remoteresponse, false,
                new Map<String,Object>{
                    'error' => e.getMessage(),
                    'cause' => e.getCause(),
                    'lineno' => e.getLineNumber(),
                    'stack' => e.getStackTraceString()
                }
            );
        }
		return remoteresponse;
	}
}