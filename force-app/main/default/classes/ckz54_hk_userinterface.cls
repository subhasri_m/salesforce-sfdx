global class ckz54_hk_userinterface extends ccrz.cc_hk_UserInterface {
    global virtual class v1 extends ccrz.cc_hk_UserInterface.v004{
        global override String ccrzIncludes(){
            String returnValue = super.ccrzIncludes();
            String cookieOverride = '<script type="text/javascript">'+
                'jQuery(function($) {'+
                'CCRZ.setCookieWithPath = function(c_name, value, expiredays, path) {'+               
                'var exdate = new Date();'+
                'if(c_name == "apex__effacc") {expiredays = -1 ;}'  + 
                'console.log("CCRZ Setting Cookies: " + c_name + " Path: " +path + " Expire in: " + expiredays);'+
                'exdate.setDate(exdate.getDate() + expiredays);'+              
                'document.cookie = c_name + "=" + window.escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString())+";path="+path;'+
                '};'+
                'CCRZ.setCookie = function(c_name, value, expiredays) {'+
                'CCRZ.setCookieWithPath(c_name, value, expiredays,"/")'+
                '};'+
                '});'+
                '</script>';
            return returnValue+cookieOverride;
        }
    }
}