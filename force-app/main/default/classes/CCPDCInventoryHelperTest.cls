@isTest
private class CCPDCInventoryHelperTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();

        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
    }
	
	@isTest static void test_EmptySkuAndInvalidEncryptedAccountId() {
		Id accountId;
		List<String> skuList = new List<String>();

		String invJson = CCPDCInventoryHelper.getInventoryJSON(skuList, accountId);

		Map<String, CCPDCInventoryHelper.Inventory> controlMap = new Map<String, CCPDCInventoryHelper.Inventory>();
		String controlJson = JSON.serialize(controlMap);

		System.assertEquals(controlJson, invJson);
	}
	
	/**
	 * Create an Account, Location, Product, Product Inventory Item that points to the
	 * Location that matches the Ship from Location on the Account.
	 *
	 * Query the product inventory items for the created product
	 *
	 * Verify there is one Product Inventory Item returned
	 * Verify the Location__r.Location__c on the Product Inventory Item matches the 
	 * ship from location on the account
	 * Verify the quantity on the Product Inventory Item matches CCPDCTestUtil.DEFAULT_QUANTITY
	 *
	 */
	@isTest static void test_SingleInventoryForAccountIdDAO() {
		Account a = util.getAccount();
		ccrz__E_Product__c p = util.getProduct();

        List<ccrz__E_ProductInventoryItem__c> inventory = [SELECT Id, ccrz__QtyAvailable__c FROM ccrz__E_ProductInventoryItem__c WHERE ccrz__ProductItem__c = :p.Id AND ccrz__InventoryLocationCode__c = :CCPDCTestUtil.DEFAULT_LOCATION_CODE];
		ccrz__E_ProductInventoryItem__c pii = inventory[0];
        
		List<ccrz__E_ProductInventoryItem__c> piiList = CCPDCProductDAO.getProductInventoryForSkus(new List<String>{p.ccrz__Sku__c});
		System.assertEquals(2, piiList.size());
		System.assertEquals(pii.ccrz__QtyAvailable__c, piiList[0].ccrz__QtyAvailable__c);
	}

	@isTest static void test_SingleInventoryForAccountId() {
		Account a = util.getAccount();
		ccrz__E_Product__c p = util.getProduct();
        List<ccrz__E_ProductInventoryItem__c> inventory = [SELECT Id, ccrz__QtyAvailable__c FROM ccrz__E_ProductInventoryItem__c WHERE ccrz__ProductItem__c = :p.Id AND ccrz__InventoryLocationCode__c = :CCPDCTestUtil.DEFAULT_LOCATION_CODE];
		ccrz__E_ProductInventoryItem__c pii = inventory[0];

		Map<String,  CCPDCInventoryHelper.Inventory> invMap = CCPDCInventoryHelper.getInventory(new List<String>{p.ccrz__Sku__c}, a.Id);
		String prodSku = p.ccrz__Sku__c;
		CCPDCInventoryHelper.Inventory invResult = (CCPDCInventoryHelper.Inventory)invMap.get(prodSku);

		System.assertNotEquals(null, invResult);
	//	System.assertNotEquals(null, invResult.primaryLocation);
		System.assertEquals(pii.ccrz__QtyAvailable__c, invResult.primaryLocation.quantity);
		System.assertEquals(1, invResult.otherLocations.size());
		// total quantity
		System.assertEquals(CCPDCTestUtil.DEFAULT_QUANTITY*2, invResult.totalQuantity);
	}

    static testmethod void checkInventoryTest() {
        util.initCallContext();

        List<String> responses = new List<String>{CCPDCOMSFulfillmentPlanAPITest.OMS_2_RESPONSE};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        update cart;
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.CC_FP_Line_Number__c = 1;
        cartItem.ccrz__Quantity__c = 20;
        update cartItem;

        CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse fulFilResponse = null;

        Test.startTest();

        ccrz__E_Cart__c extraCart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        Boolean reserveInventoryFlag = true; // production value is 'true'
        Boolean recheckAvailabilityFlag = true;
        fulFilResponse = CCPDCInventoryHelper.checkInventory(
            extraCart,
            extraCart.ccrz__E_CartItems__r,
            reserveInventoryFlag,
            recheckAvailabilityFlag,
            false
        );

        Test.stopTest();

        System.assert(fulFilResponse != null);
        System.assert(fulFilResponse.result != null);
        System.assertEquals('Success', fulFilResponse.result);
    }

    static testmethod void createExtraDataTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        cart.ccrz__PONumber__c = '123';
        update cart;
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.CC_FP_Line_Number__c = 1;
        cartItem.ccrz__Quantity__c = 20;
        update cartItem;

        Map<String, Object> extraData = null;

        Test.startTest();

        ccrz__E_Cart__c extraCart = CCPDCCartDAO.getCartExtra(cart.ccrz__EncryptedId__c);
        extraData = CCPDCInventoryHelper.createExtraData(extraCart);
        Test.stopTest();

        System.assert(extraData != null);
        System.assertEquals('123', (String)extraData.get('poNumber'));
    }

    @isTest static void getInventoryJSONTest() {
        util.initCallContext();
        Account a = util.getAccount();
        ccrz__E_Product__c p = util.getProduct();
        List<String> skuList = new List<String>{p.ccrz__Sku__c};

        String invJson = CCPDCInventoryHelper.getInventoryJSON(skuList, a.Id);

        System.assert(invJson != null);
    }

    @isTest static void getInventoryJSONByCartTest() {
        util.initCallContext();
        Account a = util.getAccount();
        ccrz__E_Product__c p = util.getProduct();
        List<String> skuList = new List<String>{p.ccrz__Sku__c};

        String invJson = CCPDCInventoryHelper.getInventoryJSONByCart(skuList, a.Id);

        System.assert(invJson != null);
    }

    @isTest static void getPageLabelTest() {
        ccrz__E_PageLabel__c label = new ccrz__E_PageLabel__c(Name = 'pdc_test', ccrz__Value__c = 'test');
        insert label;

        String text = CCPDCInventoryHelper.getPageLabel('pdc_test');

        System.assertEquals('test', text);
    }
    
    @isTest static void getFPInventoryByCartTest() {
        util.initCallContext();
        Location__c loc = util.getLocation();
        loc.Location__c  = 'ROCH';
        update loc;
       ccrz__E_Product__c p = Util.getProduct();
         List<String> lstSKUs = new List<String>();
         lstSKUs.add(p.ccrz__SKU__c);
        Account a = util.getAccount();

        CCPDCInventoryHelper.getFPInventoryByCart(lstSKUs,a.Id,loc.Location__c, 'DT');

    }

}