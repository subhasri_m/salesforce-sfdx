public with sharing class CCFPLocationDAO {
    public static Location__c getLocation(Id sfid) {
        Location__c location = null;
        List<Location__c> locations = [
            SELECT 
            Id, 
            Name,
            Accepts_Back_Orders__c,
            Activation_Date__c,
            Address_Line_1__c,
            Address_Line_2__c,
            Allow_Partial_BOFill__c,
            Branch_Manager_Email__c,
            Branch_Manager_Name__c,
            Branch_Manager_UserID__c,
            City__c,
            Company__c,
            County__c,
            In_City__c,
            Location__c,
            Location_Area__c,
            Location_Email_Address__c,
            Location_FAX__c,
            Location_GEO_Code__c,
            Location_ID__c,
            Location_Phone__c,
            Location_Region__c,
            Location_Status__c,
            Location_Tax_Number__c,
            Location_Tax_Rate__c,
            Main_Department__c,
            Rural__c,
            State__c,
            Territory_ID__c,
            Toll_Free_Num__c,
            Zipcode__c
            FROM 
            Location__c
            WHERE
            Id = :sfid
        ];
        if (locations != null && !locations.isEmpty()) {
            location = locations[0];
        }
        return location;
    }
    
    public static Location__c getLocationForCode(String code) {
        Location__c location = null;
        List<Location__c> locations = [
            SELECT 
            Id, 
            Name,
            Accepts_Back_Orders__c,
            Activation_Date__c,
            Address_Line_1__c,
            Address_Line_2__c,
            Allow_Partial_BOFill__c,
            Branch_Manager_Email__c,
            Branch_Manager_Name__c,
            Branch_Manager_UserID__c,
            City__c,
            Company__c,
            County__c,
            In_City__c,
            Location__c,
            Location_Area__c,
            Location_Email_Address__c,
            Location_FAX__c,
            Location_GEO_Code__c,
            Location_ID__c,
            Location_Phone__c,
            Location_Region__c,
            Location_Status__c,
            Location_Tax_Number__c,
            Location_Tax_Rate__c,
            Main_Department__c,
            Rural__c,
            State__c,
            Territory_ID__c,
            Toll_Free_Num__c,
            Zipcode__c
            FROM 
            Location__c
            WHERE
            Location__c = :code
        ];
        if (locations != null && !locations.isEmpty()) {
            location = locations[0];
        }
        return location;
    }
    
    // to get served from DC/branch code in FP Storefront
    // updated below method by Sanjay on 21/08/2020 to process more than one branch location at once
    public static List<Location__c> getServedFromDC(Set<String> setBranchlocationCode){
        System.debug('setBranchlocationCode==>'+setBranchlocationCode);
        List<Location__c> lstLocation = new List<Location__c>();
        lstLocation = [Select id, SourceLoc__c,Location__c,State__c,County__c, LocalDelivery_ON__c, PickUp_ON__c,Shipping_ON__c
                       from Location__c 
                       where Location__c IN :setBranchlocationCode AND CC_FP_Available_For_Ecomm__c = TRUE];
        System.debug('lstLocation'+lstLocation);
        return lstLocation;
    }
}