@isTest
public class CCFPCheckoutCCPaymentControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void controllerTest() {
        util.initCallContext();
        CCAviCardConnectAPITest.createCardConnectSettings(ccrz.cc_CallContext.storefront);

        CCFPCheckoutCCPaymentController result = null;

        Test.startTest();
        result = new CCFPCheckoutCCPaymentController();
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.baseURL != null);
        System.assert(result.path != null);
    }

    static testmethod void validateCardTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        CCAviCardConnectAPITest.createCardConnectSettings(ccrz.cc_CallContext.storefront);
        Test.setMock(HttpCalloutMock.class, new CCAviCardConnectAPITest.CardConnectServiceMock(200, 'OK', CCAviCardConnectAPITest.SUCCESS_RESPONSE));

        ccrz.cc_RemoteActionResult result = null;

        String data = '{"name":"Todd Mitchell","token":"9416190845351111","expiration":"1221","verificationCode":"222","saveAsStoredPayment":false}';        
        String addressData = '{"billTo" : {"addressFirstline":"10 Pacific Coast","addressSecondline":"","city":"Los Angeles","stateISOCode":"CA","countryISOCode":"US", "postalCode":"90210", "firstName":"Test", "lastName":"User"}}';

        Test.startTest();
        result = CCFPCheckoutCCPaymentController.validateCard(ctx, data);
        CCFPCheckoutCCPaymentController.isSavedCard(ctx, data);
        CCFPCheckoutCCPaymentController.isStoredPayment(ctx, data);
        CCFPCheckoutCCPaymentController.saveAddress(ctx, addressData, null);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }
    
    static testmethod void validateCCTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        CCAviCardConnectAPITest.createCardConnectSettings(ccrz.cc_CallContext.storefront);
        Test.setMock(HttpCalloutMock.class, new CCAviCardConnectAPITest.CardConnectServiceMock(200, 'OK', CCAviCardConnectAPITest.SUCCESS_RESPONSE));

        ccrz.cc_RemoteActionResult result = null;

        String data = '{"name":"Todd Mitchell","token":"9416190845351111","expiration":"1221","verificationCode":"222","saveAsStoredPayment":false}';        
        String addressData = '{"addressFirstline":"10 Pacific Coast","addressSecondline":"","city":"Los Angeles","stateISOCode":"CA","countryISOCode":"US", "postal":"90210", "firstName":"Test", "lastName":"User"}';

        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItemGroup__c newCartItemGroup = new ccrz__E_CartItemGroup__c();
        newCartItemGroup.ccrz__Cart__c = cart.Id;
        insert newCartItemGroup;
        
        Test.startTest();
        ccrz.cc_CallContext.currCartId = cart.Id;
        result = CCFPCheckoutCCPaymentController.validateCC(ctx, data,addressData);
        Test.stopTest();

        System.assert(result != null);
    }

}