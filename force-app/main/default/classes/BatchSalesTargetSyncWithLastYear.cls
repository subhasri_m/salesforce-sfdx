/*
Date sinceDate = Date.newInstance(Date.today().year() - 1, 1, 1);
Date toDate = Date.newInstance(Date.today().year() - 1, 12, 31);
Database.executeBatch(new BatchSalesTargetSyncWithLastYear('a0m18000000XmLg', sinceDate, toDate), 50);

or run:
        Period pd = CommonUtil.getFiscalYearPeriod(CommonUtil.getTheSameDayLastYear(Date.today()), 'Year');
        Date sinceDate = pd.startDate; 
        Date toDate = pd.endDate;
        
        BatchSalesTargetSyncWithLastYear bth = new BatchSalesTargetSyncWithLastYear(sinceDate, toDate, 10);
        
        Database.executeBatch(new BatchSalesTargetSyncWithLastYear(sinceDate, toDate), 200);
*/
global class BatchSalesTargetSyncWithLastYear implements Database.Batchable<sObject>, Database.Stateful {

     global Date startDate;
     global Date endDate;
     
     global Integer batchSize = 200;
         
     global String informaticaUserId = Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
     global Integer totalProcessedNum {get; set;}
     global Integer totalOKNum {get; set;}
     global Integer totalNGNum {get; set;}
     
     global Id oneId {get; set;}
     global BatchSalesTargetSyncWithLastYear(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
     }
     
     global BatchSalesTargetSyncWithLastYear(Date startDate, Date endDate, Integer batchSize) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.batchSize = batchSize;
        
     }
     
   /* global BatchSalesTargetSyncWithLastYear(Id oneId, Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.oneId = oneId;
     }
     */
     
    global Database.QueryLocator start(Database.BatchableContext bc) {
        totalProcessedNum = 0;
        totalOKNum = 0;
        totalNGNum = 0;
                
        String msg = 'Batch Sales Target Calculation gets started at: ' + Datetime.now();
        Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesTargetSyncWithLastYear',
            Batch_Message__c=msg,
            Type__c = 'INFO'
            );
        //Surabhi edited 12/18
      //  insert bl;
        
        String qry = 'SELECT OwnerId, Start_Date__c, Name';
        qry += ', Gross_Profit_Sum__c';
        qry += ', Sales_Sum__c';
        qry += ', GP_Target_Growth_ThisMonth__c, GP_Target_Growth_ThisQuarter__c, GP_Target_Growth_ThisYear__c';
        qry += ', Sales_Target_Growth_ThisMonth__c, Sales_Target_Growth_ThisQuarter__c, Sales_Target_Growth_ThisYear__c';
        
        qry += ' from Sales_Target__c';
        qry += '  where ownerid != :informaticaUserId AND Start_Date__c  >= :startDate AND Start_Date__c  <= :endDate';
        
        if (!String.isEmpty(oneId)) {
            qry += ' and id = :oneId';
        }
        
        return Database.getQueryLocator(qry);
    }
    
    global void execute(Database.BatchableContext bc, List<Sales_Target__c> stList) {
        
        Integer processedNum = stList.size();
        Integer OKNum = 0;
        Integer NGNum = 0;
        
        try {
            
            OKNum = processedNum;
        
            // get the data in the next year to see if it's there
            String qry = 'SELECT OwnerId, Start_Date__c from Sales_Target__c where ';
            Boolean firstFlag = true;
            
            for(Sales_Target__c st : stList) {
                if (!firstFlag) qry += ' or '; 
                Date dt = CommonUtil.getTheSameDayNextYear(st.Start_Date__c);
                DateTime dtTime = DateTime.newInstance(dt.year(), dt.month(), dt.day());
                
                qry += '(OwnerId = \'' + st.OwnerId + '\' and Start_Date__c= ' + dtTime.format('yyyy-MM-dd') + ')';
                firstFlag = false;
            }
            system.debug('-- qry:' + qry);
            
            List<Sales_Target__c> stListNextYear = Database.Query(qry);
            system.debug('-- stListNextYear before:' + stListNextYear);
            
            List<Sales_Target__c> stListInsert = new List<Sales_Target__c>();
            // compare with the data last year, if not existing, insert
            for(Sales_Target__c st : stList) {
                
                // next year's data for the same month
                Date dt = CommonUtil.getTheSameDayNextYear(st.Start_Date__c);
                
                Boolean isExisting = false;
                for(Sales_Target__c stNextYear : stListNextYear) {
                    
                    if (dt == stNextYear.Start_Date__c && stNextYear.OwnerId == st.OwnerId) {
                        isExisting = true;
                        break;
                    } 
                }
                // if not existing, insert
                if (!isExisting) {
                    Sales_Target__c newSt = new Sales_Target__c();
                    newSt.ownerId = st.OwnerId;
                    newSt.Start_Date__c = dt;
                    
                    stListInsert.add(newSt);
                }
            }
            
            if (stListInsert.size() > 0) insert stListInsert;
    
        } catch (Exception ex) {
            
            system.debug('-- error log:' + ex);
            
            NGNum = processedNum;
            // Write result to log object
            String msg = 'Error happened : ' + ex.getMessage() + '\n\n';
            msg += ex.getStackTraceString();
            msg += '\n caused by: \n';
            msg += ex.getCause();
            for(Sales_Target__c a : stList) {
                msg += a.id + ',' + a.Name + '; ';
            }
            
            Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesTargetSyncWithLastYear',
                Batch_Message__c=msg,
                Type__c = 'ERROR'
            );
            insert bl;
            
        } finally {
            
            system.debug('-- final...:' + totalProcessedNum);
            system.debug('-- final...:' + totalOKNum);
            system.debug('-- final...:' + totalNGNum);
            
            totalProcessedNum += processedNum;
            totalOKNum += OKNum;
            totalNGNum += NGNum;
            
            system.debug('-- final complete.');
            
        }
     }
     
     global void finish(Database.BatchableContext bc) {
        
        
        String msg = 'Record retrieved : ' + totalProcessedNum +
             ', Successfully processed : ' + totalOKNum +
             ', Failed to processed : ' + totalNGNum;
        // Write result to log object
        Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchSalesTargetSyncWithLastYear',
            Batch_Message__c=msg,
            Type__c = 'INFO'
            );
        
        system.debug('-- finish:' + bl);
        
      //  insert bl;
        
        // only run for this year
        Period pd = CommonUtil.getFiscalYearPeriod(Date.today(), 'Year');
        Date sinceDate = pd.startDate; 
        Date toDate = pd.endDate;
        
        Database.executeBatch(new BatchSalesOrderSummary(sinceDate, toDate), 5);
    
        system.debug('-- finish complete.'); 
     }
}