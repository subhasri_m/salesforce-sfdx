public without sharing class CCPDCChangeOwnerHelper {
    public static void changeCartOwner(Id cartId, Id userId) {
        ccrz__E_Cart__c cart = new ccrz__E_Cart__c(Id = cartId, OwnerId = userId);
        update cart;
    }

    public static void changeCartOwnerAll(Id cartId, Id userId) {
        ccrz__E_Cart__c cart = CCPDCCartDAO.getCartOwner(cartId);
        cart.OwnerId = userId;
        cart.ccrz__User__c = userId;
        List<SObject> objs = new List<SObject>();
        objs.add(cart);
        if (cart.ccrz__BillTo__c != null) {
            objs.add(new ccrz__E_ContactAddr__c(Id = cart.ccrz__BillTo__r.Id, OwnerId = userId));
        }
        if (cart.ccrz__ShipTo__c != null && cart.ccrz__ShipTo__c != cart.ccrz__BillTo__c) {
            objs.add(new ccrz__E_ContactAddr__c(Id = cart.ccrz__ShipTo__r.Id, OwnerId = userId));
        }
        update objs;
    }
    //This method is used when submitting cart for approval
    public static void changeCartOwnerForApproval(Id cartId, Id userId, Id contactId) {
        ccrz__E_Cart__c cart = CCPDCCartDAO.getCartOwner(cartId);
        cart.OwnerId = userId;
        cart.ccrz__User__c = userId;
        cart.ccrz__Contact__c = contactId;
        //cart = '0031W000028GwfBQAS';
        List<SObject> objs = new List<SObject>();
        objs.add(cart);
        if (cart.ccrz__BillTo__c != null) {
            objs.add(new ccrz__E_ContactAddr__c(Id = cart.ccrz__BillTo__r.Id, OwnerId = userId));
        }
        if (cart.ccrz__ShipTo__c != null && cart.ccrz__ShipTo__c != cart.ccrz__BillTo__c) {
            objs.add(new ccrz__E_ContactAddr__c(Id = cart.ccrz__ShipTo__r.Id, OwnerId = userId));
        }
        System.debug('@@Rahul CartUpdate '+ objs);
        update objs;
    }

    public static void changeCartOwnerAll(Id cartId, Id userId, Id contactId, Id accountId) {
        ccrz__E_Cart__c cart = CCPDCCartDAO.getCartOwner(cartId);
        cart.OwnerId = userId;
        cart.ccrz__Account__c = accountId;
        cart.ccrz__Contact__c = contactId;
        cart.ccrz__User__c = userId;
        List<SObject> objs = new List<SObject>();
        objs.add(cart);
        if (cart.ccrz__BillTo__c != null) {
            objs.add(new ccrz__E_ContactAddr__c(Id = cart.ccrz__BillTo__r.Id, OwnerId = userId));
        }
        if (cart.ccrz__ShipTo__c != null && cart.ccrz__ShipTo__c != cart.ccrz__BillTo__c) {
            objs.add(new ccrz__E_ContactAddr__c(Id = cart.ccrz__ShipTo__r.Id, OwnerId = userId));
        }
        update objs;
    }

    public static void changeOrderOwnerAll(Id orderId, Id userId, Id contactId, Id accountId) {
        ccrz__E_Order__c order = CCPDCOrderDAO.getOrderOwner(orderId);
        order.OwnerId = userId;
        order.ccrz__Account__c = accountId;
        order.ccrz__Contact__c = contactId;
        order.ccrz__User__c = userId;
        List<SObject> objs = new List<SObject>();
        objs.add(order);
        ccrz__E_Cart__c cart = new ccrz__E_Cart__c(
            Id = order.ccrz__OriginatedCart__c,
            OwnerId = userId,
            ccrz__Account__c = accountId,
            ccrz__Contact__c = contactId,
            ccrz__User__c = userId
        );
        objs.add(cart);
        update objs;
    }

    public static void updateCart(ccrz__E_Cart__c cart) {
        update cart;
    }
}