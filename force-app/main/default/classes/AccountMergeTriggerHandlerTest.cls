@isTest
public class AccountMergeTriggerHandlerTest{
    static testMethod void AddAccountMergeRequest(){
User user1 = new User(alias = 'ceo', email='admin@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-88888',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTas@testorg.com', profileid = '00e400000013ttZ', Create_Accounts__c = true);
        insert user1;
User user2 = new User(alias = 'ceo2', email='admin2@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-99999',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='admin2@testorg.com', profileid = '00e400000013ttZ');
        insert user2;

        System.runas(user1){
            Account acc = new Account(Name = 'Test Trigger', OwnerId = User1.id, AccountNumber = '1-1221-1');
            insert acc;
            
            Account acc1 = new Account(Name = 'Duplicate Trigger', OwnerId = User2.id, AccountNumber = '1-2222-1');
            insert acc1;
            System.Debug('acc' + acc + '\nacc1' + acc1);
            Account_Merge_Request__c amr = new Account_Merge_Request__c(Master_Account__c = acc.Id, Sub_Account__c = acc1.Id, Merge_Request_Explanation__c = 'Sample Explanation');
            Insert amr;
            
            amr.Merge_Checkbox__c = True;
            Update amr;
           
        }

    }
}