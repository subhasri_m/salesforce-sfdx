public class CloseOpportunityInBulk implements database.Batchable<sObject> {
    
     public database.QueryLocator start (Database.BatchableContext BC)
    {
        String query = 'Select id, CloseDate,stagename,LeadSource,reason_lost__c from opportunity where stagename =\'01-New\' and LeadSource like \'%FP%\' and RecordType.Name != \'National Header Account\' ';
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<Opportunity>scope)
    {
        List<Opportunity> RecordUpdate = new List<Opportunity>();
       // sObject sObj = Schema.getGlobalDescribe().get(ObjectName).newSObject() ;
        
        for (opportunity s : scope){
             opportunity record = new opportunity();
            record.id = s.id;
            record.CloseDate = Date.valueOf(System.label.CloseDateOpp);
            record.StageName = '90-Closed Lost';
            record.Reason_Lost__c ='System Closed';
            RecordUpdate.add(record);                  
        }
        
        database.update(RecordUpdate, false);
        
    }
    
    public void finish(Database.BatchableContext BC)
    {
        
    }
}