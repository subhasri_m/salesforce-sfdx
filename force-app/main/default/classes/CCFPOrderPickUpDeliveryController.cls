global without sharing class CCFPOrderPickUpDeliveryController {
    global Id orderID {get;set;}
    global Id orderItemGroupID {get;set;}
    global ccrz__E_Order__c mockOrder;
    global List<ccrz__E_OrderItem__c> orderItems;
    global List<ccrz__E_OrderItemGroup__c> orderItemGroups;
    global boolean isShortShip;
    global  CCFPOrderPickUpDeliveryController() {
    }
    global String getStorefrontURL() {
        CC_Avi_Settings__mdt aviSettings = [SELECT Storefront_URL__c from CC_Avi_Settings__mdt where Storefront__c = 'parts' limit 1];
        return aviSettings.Storefront_URL__c;
    }
    global ccrz__E_Order__c getmockOrder() {
        mockOrder = new ccrz__E_Order__c();
        mockOrder = [select Id, Name, ccrz__Account__r.Name, Account_Site__c, ccrz__OrderName__c, ccrz__Contact__r.Name, ccrz__OriginatedCart__r.CC_FP_Location__r.Location__c,
                     ccrz__OriginatedCart__r.CC_FP_Location__r.Address_Line_1__c,ccrz__OriginatedCart__r.CC_FP_Location__r.Address_Line_2__c, ccrz__OriginatedCart__r.CC_FP_Location__r.City__c, ccrz__OriginatedCart__r.CC_FP_Location__r.State__c, ccrz__OriginatedCart__r.CC_FP_Location__r.Zipcode__c 
                     from ccrz__E_Order__c  where Id =:orderId];
        return mockOrder;
    }
    global Boolean getisShortShip() {
        isShortShip = false;
        for(ccrz__E_OrderItem__c orderItem: [select ccrz__Quantity__c, Shipped_Quantity__c   from ccrz__E_OrderItem__c where ccrz__Order__c =: orderId and ccrz__OrderItemGroup__c = :orderItemGroupID] ) {
            if(orderItem.ccrz__Quantity__c != orderItem.Shipped_Quantity__c) {
                isShortShip = true;
            }
        } 
        return isShortShip;
    }
    global List<ccrz__E_OrderItem__c> getorderItems() {
        orderItems = new List<ccrz__E_OrderItem__c>();
        orderItems = [select ccrz__Product__r.ccrz__SKU__c, ccrz__Product__r.DSP_Part_Number__c, ccrz__Product_Name__c, ccrz__Quantity__c, Shipped_Quantity__c   from ccrz__E_OrderItem__c where ccrz__Order__c =: orderId and ccrz__OrderItemGroup__c = :orderItemGroupID]; 
        return orderItems;
    }
    global List<ccrz__E_OrderItemGroup__c>  getorderItemGroups() {
        orderItemGroups = new List<ccrz__E_OrderItemGroup__c>();
        orderItemGroups = [Select ccrz__GroupName__c, CC_FP_Ship_From_Location_Phone_Number__c, CC_FP_Ship_From_Location__c,Invoice_Number__c from ccrz__E_OrderItemGroup__c where Id=: orderItemGroupID];         
        return orderItemGroups;
    }
}