/**
 * @description     LiveChatVisitorDetailsController
 * @author          Lalit
 * @Company         FP
 * @date            14-12-2017
 *
 * HISTORY
 * - 14-12-2017    Lalit      Created.
*/ 


public class LiveChatVisitorDetailsController {
    
    public String ContactName {get;set;}
    public String AccountName {get;set;}
    public String QueryOrComments {get;set;}
    public String ContactId {get;set;}
    public String Accountid {get; set;}
    public String Phone {get;set;}
    List<Contact> ContactList = new List<Contact>(); 
    List<Account> AccountList = new List<Account>();
    public String AccountOwnerid ;
    public List<User> GuestOwnerId = new List<User>();
    
    public LiveChatVisitorDetailsController()
    {
         
         ContactId = ApexPages.currentPage().getParameters().get('Vistorid');
         Accountid = ApexPages.currentPage().getParameters().get('EffAccid');
        
        if(ContactId!=null)
        {
            ContactList = [select Accountid,name from contact where id =:contactId limit 1];
            System.debug('contact--->'+ContactList);
            AccountList = [select id,name,ownerid from account where id =: Accountid  limit 1]; 
            System.debug('Account--->'+AccountList);
        }  
        if (!ContactList.isEmpty()){
        contactname  =  ContactList[0].name;
        if(!AccountList.isEmpty())
        {
            AccountName = AccountList[0].name;
            AccountOwnerid = AccountList[0].ownerid;
        }
        else
        {
            AccountName = 'No Account Found!!';
        }
    }
    else
        {
            contactname = 'Guest';
            AccountName = 'N/A';
        }
    } 
    
    public pagereference SaveDetails()
    {
        
        SiteRequestHandler__c request = new SiteRequestHandler__c();
		System.debug('Contact id '+ Contactid);
        if(Contactid!=null && Contactid!=''){
        request.CustomerName__c = ContactId ;
        request.AccountID__C = AccountList[0].id;
        }
         
        request.CustomerAccount__c = AccountName;
        request.Complaints_Query__c = QueryOrComments;
        if(AccountOwnerid!=null){
        request.AccountOwner__c = AccountOwnerid;
        }
        else
        { GuestOwnerId = [select id from user where username =:System.label.GuestOwnerUsername];
          request.AccountOwner__c = GuestOwnerId[0].id;
        }
        if(!AccountList.isEmpty()){
        request.AccountName__C = AccountList[0].name;
        }
        insert request;
        
        PageReference pr = new PageReference('/apex/LiveAgentThankyou?CloseVisible=false&ReturnVisible=true');
        pr.getParameters().put('key','value');
        pr.setRedirect(true); // If you want a redirect. Do not set anything if you want a forward.
        return pr;
        
    }  
    
   
    
}