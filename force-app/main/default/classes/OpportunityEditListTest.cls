@istest
public   class  OpportunityEditListTest {
    
    
    @testSetup static void setupTestData(){
        Profile saProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        
        //Create User Data
        User saUser = new User(Alias = 'newUser', Email='test123@email.com', 
                               EmailEncodingKey='UTF-8',
                               LastName='Testing',
                               LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',
                               ProfileId = saProfile.Id, 
                               TimeZoneSidKey='America/Los_Angeles',
                               UserName='test123123xxxxx@email.com',
                               Create_Accounts__c = true,
                               Salesman_Number__c = '1-1234',
                               Region__c= 'Central');   
        insert saUser;
        OppPageStages__c stageName = new OppPageStages__c (Name='Test', StageNames__c='01-New;01-Old');
        insert stageName;
        
        System.runAs(saUser){
        Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = saUser.Id);  
        insert acc;
        
        
        Opportunity opp = new Opportunity (name = 'opp1',accountid = acc.id,Type ='New Business',LeadSource ='FleetSeek',CloseDate=date.today(),stagename='01-New');
        insert opp;
        }
    }
    
    static testmethod  void Test1()
    {
        OpportunityEditList Controller = new OpportunityEditList();
        Controller.getOppsList();
        //Controller.getProductBunch();
        Controller.getReasonLost();
        Controller.updateStage = 'Closed Won';
        Controller.getOppStage();
        Controller.getOppFilterStage();
        Controller.UpdateProductBunch = 'WHEEL AND RIM - 3505';
        controller.UpdateReasonLost ='Timeframe';
        OpportunityEditList.WrapperClass wrapper = new OpportunityEditList.WrapperClass();
        List<Opportunity> opp = [select id from opportunity limit 1];
        wrapper.OpportunityId = opp[0].Id;
        wrapper.Selected = true;
         List<OpportunityEditList.wrapperclass> WrapperList  = new List<OpportunityEditList.wrapperclass>(); 
        WrapperList.add(Wrapper);
        controller.UpdateOppRecords(WrapperList);
        Controller.updateTableData();
    } 
}