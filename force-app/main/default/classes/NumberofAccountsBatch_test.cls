/**
 * @description     Number of Accounts Batch Test Class
 * @author          Lawrence Catan
 * @Company         CloudSherpas
 * @date            29.FEB.2016
 *
 * HISTORY
 * - 01.MAR.2016    Lawrence Catan      Created.
*/ 

@isTest
private class NumberofAccountsBatch_test{

    static testmethod void test() {
        // The query used by the batch job.
        BatchNumberAccountsOnUserSetting__c customSetting = new BatchNumberAccountsOnUserSetting__c();
        customsetting.AccountFilterQuery__c ='SELECT Id,OwnerID, Name FROM Account WHERE OwnerID !=null and owner.isactive =true and owner.name !=\'Informatica User\'';
        customsetting.Schedule_HoursDifference__c = 3;
        customsetting.LastRunTimeHour__c = 21;
        customsetting.Name = 'Settings';
        insert customsetting;
        String query = 'SELECT Id,OwnerID, Name FROM Account WHERE OwnerID !=null';
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        List<User> ul = new List<User>();
        for (Integer i=0;i<10;i++) {
        User TestUser = new User(
        FirstName = 'Samantha' + i,
        LastName = 'Green',
        CompanyName = 'IT Test Company',
        MobilePhone = '123-456-7890',
        
        Username = 'demo' + i + '@andomdemodomain.com.fleetpride',
        Email = 'standarduser@testorg.com',
        Alias = 'test',
        CommunityNickname = 'testing' + i,
        TimeZoneSidKey = 'America/New_York',
        LocaleSidKey = 'en_US',
        EmailEncodingKey = 'UTF-8',
        LanguageLocaleKey = 'en_US',
        number_of_accounts__c = 0,
        Street = '123 Test St',
        City = 'Testcity',
        State = 'va',
        PostalCode = '23223',
        Country = 'Japan',
        ProfileId = p.id,
            Create_Accounts__c = true,
         isactive = true);
        ul.add(TestUser);
        }
        insert ul;       
        
        system.runAs(ul[0]){
        List<Account> al = new List<Account>();
        for (Integer i=0;i<10;i++) {
        Account acc = new Account(
        Name = 'Testing Account + i',
        Type = 'Customer',
        OwnerID = ul[i].id);
        al.add(acc);
        }
        try {
            insert al;
        }
        catch (exception e) {
            system.debug('Acct Error: ' + e);
        }        
        
        Test.startTest();
        NumberofAccountsBatch batch = new NumberofAccountsBatch();
        ID batchprocessid = Database.executeBatch(batch);
        
        String strCron = '0 40 * * * ?';
        String jobId = System.schedule('Test Run', strCron, new NumberofAccountsBatch());
        CronTrigger ct = [SELECT Id, 
                                        CronExpression, 
                                        TimesTriggered, 
                                        NextFireTime 
                                        FROM CronTrigger WHERE id = :jobId];
   
       System.assertEquals(strCron, ct.CronExpression);
       Test.stopTest();
        }
    }
}