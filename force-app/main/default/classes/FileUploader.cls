public class FileUploader 
{
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
        List<ccrz__E_Product__c> RecordtoUpdate = new List<ccrz__E_Product__c>() ;
    List<ccrz__E_Product__c> RecordstoUpdateFinal = new List<ccrz__E_Product__c> (); 
    Set<String> UniquefieldCollection1 = new set<String>();
    Set<String> UniquefieldCollection2 = new set<String>();
    
    List<String> IdsToUpdate = new List<String>();
    
    
    public void ReadFile()
    {
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
            UniquefieldCollection1.add(inputvalues[0].deleteWhitespace()); 
          
        } 
        System.debug('--->'+UniquefieldCollection1.size());  
        RecordtoUpdate = [select id,ccrz__ProductStatus__c,ccrz__SKU__c,CAT_Part_Number__c from ccrz__E_Product__c where  CAT_Part_Number__c in:UniquefieldCollection1];         
        System.debug('Product id--->'+RecordtoUpdate.size()); 
        set<string> checkin = new set<string>(); 
        for(ccrz__E_Product__c var :RecordtoUpdate )
        {
            checkin.add(var.CAT_Part_Number__c);
        }
        
        for (String var :UniquefieldCollection1)
        { 
            if(!checkin.contains(var))
                System.debug('Not in record'+ var); 
        }
        
        if(!RecordtoUpdate.isEmpty())
        {
            for (ccrz__E_Product__c var :RecordtoUpdate ){
                ccrz__E_Product__c record = new ccrz__E_Product__c();
                record.id=var.id;
                record.ccrz__ProductStatus__c = 'Blocked';
                RecordstoUpdateFinal.add(record);
            }
        }  
        Update RecordstoUpdateFinal;  
    } 
}