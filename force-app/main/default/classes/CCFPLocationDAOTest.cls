@isTest
public with sharing class CCFPLocationDAOTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    static testmethod void getLocationTest() {
        Location__c location = util.createLocation('RST');
        insert location;

        Location__c testLocation = null;
        Test.startTest();
        testLocation = CCFPLocationDAO.getLocation(location.Id);
        Test.stopTest();

        System.assert(testLocation != null);
        System.assertEquals(location.Id, testLocation.Id);
    }
    
    static testmethod void getLocationForCodeTest() {
        Location__c location = util.createLocation('RST');
        insert location;

        Location__c testLocation = null;
        Test.startTest();
        testLocation = CCFPLocationDAO.getLocationForCode('RST');
        Test.stopTest();

        System.assert(testLocation != null);
        System.assertEquals(location.Id, testLocation.Id);
    }
    
    static testmethod void getServedFromDCTest() {
        Location__c location = util.createLocation('RST');
        insert location;

        List<Location__c> testLocation = new List<Location__c>();
        Test.startTest();
        testLocation = CCFPLocationDAO.getServedFromDC(new Set<String>{'RST'});
        Test.stopTest();

        System.assert(testLocation != null);
    }
}