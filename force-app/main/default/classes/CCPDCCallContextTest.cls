@isTest
public class CCPDCCallContextTest {

    static testmethod void processTest() {
  
        Test.startTest();
        CCPDCCallContext.skipPricing = true;
        CCPDCCallContext.priceMap = new Map<String, Decimal>();
        Test.stopTest();

        System.assert(CCPDCCallContext.skipPricing);
        System.assert(CCPDCCallContext.priceMap != null);

    }
}