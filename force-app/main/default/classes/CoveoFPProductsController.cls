global with sharing class CoveoFPProductsController {
    public String customerCompanyNumber {get;set;}
    public String customerAccountNumber {get;set;}
    public String customerBranch {get;set;}

    public CoveoFPProductsController() {
        if (!Account.sObjectType.getDescribe().isAccessible() || ccrz.cc_CallContext.effAccountId == '' ) {
            // If the current user is a guest user (anonymous) then the returned value will be an empty string
            return;
        } else {
            Account effAcc = [
                SELECT
                    Iseries_Company_code__c,
                    ISeries_Customer_Account__c,
                    ISeries_Customer_Branch__c
                FROM
                    Account
                WHERE
                    Id=:ccrz.cc_CallContext.effAccountId Limit 1
                ];

            customerCompanyNumber = effAcc.Iseries_Company_code__c;
            customerAccountNumber = effAcc.ISeries_Customer_Account__c;
            customerBranch = effAcc.ISeries_Customer_Branch__c;
            // TODO: check if contain crossreferences
        }
    }

    public static CCAviBoomiAPI.SeriesPricingWSResponse price(List<Map<String,Object>> prodList, String location) {
        if (UserInfo.getUserType() == 'Guest') {
            return CCPDCPricingHelper.anonymousPrice(prodList, location);
        } else {
            return CCPDCPricingHelper.priceForSpecificLocation(prodList, location);
        }
    }

    global static string getCoveoSearchToken(){
        // Generate a token using the Globals class provided by Coveo.
        // See the Globals Class documentation: https://docs.coveo.com/en/1075/coveo-for-salesforce/globals-class
        String token = CoveoV2.Globals.generateSearchToken(new Map<String, Object> {
            'searchHub' => 'FleetPrideB2BMainSearch'
        });
        return token;
    }

    @RemoteAction
    global static String getPriceList(String jsonString, String location) {
        List<Map<String,Object>> parts = new List<Map<String,Object>>();
        List<Object> items = (List<Object>) JSON.deserializeUntyped(jsonString);

        for (Object itemObj : items) {
            Map<String, Object> item = (Map<String, Object>) itemObj;
            parts.add(item);
        }
        return JSON.serialize(price(parts, location).listPricing);
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getProductInventoryForSku(ccrz.cc_RemoteActionContext ctx, List<String> skus, String branchLoc, String dcLoc) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx);
        try{
            if (UserInfo.getUserType() == 'Guest') {
                res.data = CCPDCInventoryHelper.getFPInventoryByCart(skus, null, branchLoc, dcLoc);
            } else {
                ccrz.ccLog.log(LoggingLevel.DEBUG,'P:ccrz.cc_CallContext.effAccountId', ccrz.cc_CallContext.effAccountId);
                res.data = CCPDCInventoryHelper.getFPInventoryByCart(skus, ccrz.cc_CallContext.effAccountId, branchLoc, dcLoc);
                res.success = true;
            }
        }catch(Exception e){
            //error handling here, perhaps using res.messages to pass data back
            ccrz.ccLog.log(LoggingLevel.ERROR,'ERR',e);
        }finally{
            ccrz.ccLog.log(LoggingLevel.INFO,'M:X','getProductInventoryForSku');
            ccrz.ccLog.close(res);
        }
        return res;
    }

    @RemoteAction
    global static String getVinDetails(String vin) {
        return JSON.serialize(CCFPVinHelper.getVinDetails(vin));
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult addToCart(ccrz.cc_RemoteActionContext ctx, String sku, Integer quantity, String prodBean) {
        return CCPDCProductDetailSimpleController.addToCartFP(ctx, sku, quantity, null, prodBean);
    }

    @RemoteAction
    global static String getSpecGroupTree() {
        List<ccrz__E_Category__c> categories = [
            SELECT ccrz__CategoryID__c, Spec_Group__c
            FROM ccrz__E_Category__c
            WHERE Spec_Group__c != null AND ccrz__ProductCount__c > 0
        ];

        Map<String, String> specGroups = new Map<String, String>{};
        for (ccrz__E_Category__c category : categories) {
            specGroups.put(category.Spec_Group__c, category.ccrz__CategoryID__c);
        }

        List<ccrz__E_Spec__c> specs = [
            SELECT ccrz__DisplayName__c, ccrz__SpecGroup__c
            FROM ccrz__E_Spec__c
            WHERE ccrz__UseForFilter__c = true
            AND ccrz__SpecGroup__c in :specGroups.keySet()
        ];

        Map<String, List<String>> dictionary = new Map<String, List<String>>{};
        for (ccrz__E_Spec__c spec : specs) {
            String specGroup = spec.ccrz__SpecGroup__c;
            String key = specGroups.get(specGroup);
            if (key != null && dictionary.get(key) != null) {
                // There is already an attribute for this spec group
                dictionary.get(key).add(spec.ccrz__DisplayName__c);
            } else {
                List<String> attr =  new List<String>();
                attr.add(spec.ccrz__DisplayName__c);
                dictionary.put(specGroups.get(specGroup), attr);
            }
        }

        return JSON.serialize(dictionary);

    }
}