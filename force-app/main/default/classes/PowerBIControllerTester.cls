public virtual class PowerBIControllerTester {
    //@TestVisible public static String APPLICATION_NAME = 'PowerBINative';
    @TestVisible public String application_name;
    @TestVisible public String access_token;
    @TestVisible public String refresh_token;
    @TestVisible public String expires_on;
    @TestVisible public String pbi_Token;

    public String pbi_ids {get; set;}
    public String validateResult;
    public String validateResult2;
    
    /**
    * Generic constructor
    */
    public PowerBIControllerTester () {       

        if (OAuthApp_pbi_native__c.getValues(this.application_name) != null) {
            Cookie pbi_AccessToken = ApexPages.currentPage().getCookies().get('pbi_AccessToken');
            Cookie pbi_RefreshToken = ApexPages.currentPage().getCookies().get('pbi_RefreshToken');
            Cookie pbi_ExpiresOn = ApexPages.currentPage().getCookies().get('pbi_ExpiresOn');
         
         if(pbi_AccessToken == null)
             this.access_token = '';
         else
            this.access_token = pbi_AccessToken.getValue();
            
         if(pbi_RefreshToken == null)
             this.refresh_token= '';
         else
            this.refresh_token = pbi_RefreshToken.getValue();
            
         if(pbi_ExpiresOn == null)
             this.expires_on = '';
         else
             this.expires_on = pbi_ExpiresOn.getValue();
        }
    }

    public String PBIaccess_token { 
        get {
            Cookie pbi_AccessToken = ApexPages.currentPage().getCookies().get('pbi_AccessToken');
            if(pbi_AccessToken == null)
                this.access_token = '';
            else
                this.access_token = pbi_AccessToken.getValue();
            
            return this.access_token;
        } 
        set;
        }
        
    public String PBIrefresh_token { 
        get {
            Cookie pbi_RefreshToken = ApexPages.currentPage().getCookies().get('pbi_RefreshToken');
            if(pbi_RefreshToken == null)
                this.refresh_token= '';
            else
                this.refresh_token= pbi_RefreshToken.getValue();
            
            return this.refresh_token;
        } 
        set;
        }
        
    public String PBIexpires_on { 
        get {
            Cookie pbi_ExpiresOn= ApexPages.currentPage().getCookies().get('pbi_ExpiresOn');
            if(pbi_ExpiresOn== null)
                this.expires_on= '';
            else
                this.expires_on= pbi_ExpiresOn.getValue();
            
            return this.expires_on;
        } 
        set;
        }
        
    public String PBIToken { 
        get {
            Cookie pbi_Token= ApexPages.currentPage().getCookies().get('pbi_Token');
            if(pbi_Token== null)
                this.pbi_Token= '';
            else
                this.pbi_Token= pbi_Token.getValue();
            
            return this.pbi_Token;
        } 
        set;
        }

    /**
    * If the access token is set
    *
    * @return If the access token is set
    */
    public Boolean getHasToken() {
          Boolean check;
         if(PBIaccess_token== null)
              check = false;
           else 
              check = PBIaccess_token.length()>0;
          return check;
    }

    /** The JSON result from a successful oauth call */
    public class OAuthResult {
        /** The access token */
        public String access_token {get; set;}

        /** The refresh token */
        public String refresh_token {get; set;}
        
        /** The token expiry date*/
        public String expires_on {get;set;}
    }

    public class OAuthPBIAPI {
        /** The access token */
        public String token {get; set;}

        /** The refresh token */
        public String tokenId {get; set;}
        
        /** The token expiry date*/
        public String expiration {get;set;}
    }


    public OAuthResult silentAuth() {
        String Authorization_URL = OAuthApp_pbi_native__c.getValues(this.application_name).Authorization_URL__c;
        String resource_URI = OAuthApp_pbi_native__c.getValues(this.application_name).Resource_URI__c;
        String client_id = OAuthApp_pbi_native__c.getValues(this.application_name).Client_Id__c;
        String OAuth_User = OAuthApp_pbi_native__c.getValues(this.application_name).OAuth_Username__c;
        String OAuth_Pwd = OAuthApp_pbi_native__c.getValues(this.application_name).OAuth_Password__c;
        String SF_User = userInfo.getuserId();
        String requestContent = null; 
        List<String> urlParams = new List<String> {
            'resource=' + EncodingUtil.urlEncode(resource_URI, 'UTF-8'),
            'grant_type=password',
            'client_id=' + EncodingUtil.urlEncode(client_id, 'UTF-8'),
            'scope=openid',
            'username=' + EncodingUtil.urlEncode(OAuth_User, 'UTF-8'),
            'password=' + EncodingUtil.urlEncode(OAuth_Pwd, 'UTF-8')
        };

        Http h = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(Authorization_URL);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('Accept', 'application/json');
        String body = String.join(urlParams, '&');
        req.setBody(body);
        
        HttpResponse res = h.send(req);
        validateResult = res.getBody();
        return (OAuthResult)(JSON.deserialize(res.getBody(), OAuthResult.class));
    }

    public OAuthPBIAPI PBIGenerateToken(String groupid, String reportid, String datasetid, String role, String accesstoken, String effectiveuser){

        String Authorization_URL = 'https://api.powerbi.com/v1.0/myorg/groups/' + groupid + '/reports/' + reportid + '/GenerateToken';
        String identity2 = '{"identities": [{"username": "' + effectiveuser + '","roles":["' + role + '"],"datasets":["' + datasetid + '"]}]}';

        Http h = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(Authorization_URL);
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + accesstoken);
        req.setHeader('Content-Type', 'application/json; charset=utf-8');
        req.setHeader('Accept', 'application/json');
        String body = JSON.serialize(identity2); 
        req.setBody(identity2);

        HttpResponse res = h.send(req);
        validateResult2 = res.getBody();
        return (OAuthPBIAPI)(JSON.deserialize(res.getBody(), OAuthPBIAPI.class));
    }

    public PageReference AuthenticateFromPage() {
  
        pbi_ids = ApexPages.currentPage().getParameters().get('pbi_id');
        String effectiveuser = UserInfo.getUserId();
        String groupid = PowerBI_IDs__c.getValues(pbi_ids).pbi_groupID__c;
        String reportid = PowerBI_IDs__c.getValues(pbi_ids).pbi_reportID__c;
        String datasetid = PowerBI_IDs__c.getValues(pbi_ids).pbi_datasetID__c;
        String role = PowerBI_IDs__c.getValues(pbi_ids).pbi_role__c;

        OAuthResult result = silentAuth();
        OAuthPBIAPI result2 = PBIGenerateToken(groupid, reportid, datasetid, role, result.access_token, effectiveuser);
                                
        //Store accesstoken in cookie
        Cookie accessToken = new Cookie('pbi_AccessToken', result.access_token,null,-1,false);
        Cookie refreshToken = new Cookie('pbi_RefreshToken', result.refresh_token,null,-1,false);
        Cookie expiresOn = new Cookie('pbi_ExpiresOn',result.expires_on,null,-1,false);
        Cookie pbiToken = new Cookie('pbi_Token', result2.token, null,-1,false);
        
        ApexPages.currentPage().setCookies(new Cookie[]{accessToken,refreshToken,expiresOn,pbiToken}); 
        return null;
    }
    
    public String getValidateResult()
    {
        return validateResult;
    }
}