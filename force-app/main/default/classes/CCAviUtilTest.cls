@isTest
public class CCAviUtilTest {
	static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

    }

	
	static testmethod void testgetCCAviSettingsMetaData(){
		CC_Avi_Settings__mdt testsetting = CCAviUtil.getCCAviSettingsMetaData('pdc');
		System.assert(testsetting!=null);
		System.assertEquals(String.valueOf(testsetting.DeveloperName),'pdc');
	}
	static testmethod void testCCAviUtilConstructor(){
		CCAviUtil test = new CCAviUtil();
	}
	static testmethod void testGetRecordTypeIdByNameAndSObjectType(){
		Id recordTypeId = CCAviUtil.getRecordTypeIdByNameAndSObjectType('Entity_Account', 'Account');
		System.debug(recordTypeId);
	}

}