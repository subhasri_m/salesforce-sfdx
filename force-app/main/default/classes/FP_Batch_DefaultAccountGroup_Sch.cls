Global class FP_Batch_DefaultAccountGroup_Sch implements Schedulable { 
     global void execute(SchedulableContext SC){
        Database.executeBatch(new FP_Batch_DefaultAccountGroup());
    } 
}