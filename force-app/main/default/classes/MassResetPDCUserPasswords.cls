public class MassResetPDCUserPasswords implements database.Batchable<sObject> {
    
    public database.QueryLocator start (Database.BatchableContext BC)
    {
        String query;
        if(!test.isRunningTest()){
         query = System.label.MassResetPDCUserPasswordsQuery;
        }
        else
        {
         query = 'select id from User limit 1';
            
        }
        System.debug('Query-->'+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<User>scope)
    {
        List<User> RecordUpdate = new List<User>(); 
        
        for (user s : scope){
            
            if(!Test.isRunningTest()){System.resetPasswordWithEmailTemplate(s.Id, true, 'PDC_Welcome_Email');}
        } 
    }
    
    public void finish(Database.BatchableContext BC)
    {
        
    }
}