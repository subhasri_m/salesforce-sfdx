@isTest
public class Align_Contact_Owner_with_AccountTest {
    
    @testSetup static void testSetup() {
        Account acct = new Account(Name='Test Account',Iseries_Company_code__c = '1');
        insert acct;
        Contact contObj = new Contact(AccountId=acct.Id,
                                      firstname='Test First Name',
                                      lastname='Test Last Name',
                                      email='email@testemail.com',User_Enabled__c=true);
        insert contObj;
    }
    
    @isTest 
    static void TestupsertUser() {
        
        Test.startTest();
        Contact contObj = [Select Id from Contact where email='email@testemail.com' limit 1];
        contObj.User_Enabled__c = false;
        update contObj;
        
        Test.stopTest();
        
        
    }
}