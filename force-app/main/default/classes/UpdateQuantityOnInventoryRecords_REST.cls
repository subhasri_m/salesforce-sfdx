@RestResource(urlMapping='/UpdateQuantityOnInventoryRecords/*')
global with sharing class UpdateQuantityOnInventoryRecords_REST {

    
    @HttpGet
    global static String updateZeroQuantity() {
    
        string returnMessage;
        try
        { 
            database.executeBatch(new Batch_UpdateQuantityOnInventoryRecords());   
            returnMessage = 'Success';
        }
        catch (Exception e)      { returnMessage = 'Exception';}
        return returnMessage;
    }
}