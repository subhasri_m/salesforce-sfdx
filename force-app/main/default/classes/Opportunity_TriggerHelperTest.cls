@isTest
public class Opportunity_TriggerHelperTest{
    @testSetup static void setupTestData(){
        Profile saProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        UserRole role = [SELECT Id FROM UserRole WHERE Name  ='National Account Sales Rep' limit 1];
        
        //Create User Data
        User saUser = new User(Alias = 'newUser', Email='test12123@email.com', 
                               UserRoleId = role.id ,
                               EmailEncodingKey='UTF-8',
                               LastName='Testing',
                               LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',
                               ProfileId = saProfile.Id, 
                               TimeZoneSidKey='America/Los_Angeles',
                               UserName='testxxxxx222xxx@email.com',
                               Create_Accounts__c = true,
                               Salesman_Number__c = '1-1234',
                               Region__c= 'Central');   
        insert saUser;
        
    }
    
    static testmethod  void childOppTest()
    {
        User saUser = [select id,Name from user limit 1];
        system.runAs(saUser){
            Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = saUser.Id, National_Account_Group__c='999',accountNumber=null);  
            insert acc;
            Account childAcc = new Account(Name = 'child', type = 'Customer', Iseries_company_code__c ='1',parentId=acc.id,OwnerId = saUser.Id, accountNumber = '2-1222-1', National_Account_Group__c='999');  
            insert childAcc;
            Id recTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('National Header Account').getRecordTypeId();
            Opportunity opp = new Opportunity (name = 'opp1',accountid = acc.id,Type ='New Business',LeadSource ='FP Intelligence', CloseDate=date.today(), stagename='01-New', RecordTypeId =recTypeId, OwnerId = saUser.Id );
            insert opp;
            Opportunity oppnew = [Select stagename,Amount from Opportunity limit 1];
            oppnew.StageName = '50-Closed Won';
            oppnew.Amount= 625.44;
            update oppnew;
        } 
    }
}