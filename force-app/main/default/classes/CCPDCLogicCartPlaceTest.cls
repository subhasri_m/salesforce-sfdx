@isTest
public class CCPDCLogicCartPlaceTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
            util.createCartWithSplits(m);
        }

        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.SERVICE_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccServiceCart' => 'c.CCPDCServiceCart'
                }
            },
            ccrz.ccApiTestData.LOGIC_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccLogicCartPlace' => 'c.CCPDCLogicCartPlace'
                }
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = util.STOREFRONT;
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
    }

    static testMethod void processTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        cart.ccrz__ValidationStatus__c = 'CartAuthUserValidated';
        update cart;
        User u = util.getPortalUser();

        Map<String,Object> response = null;
        Boolean isSuccess = false;

        System.runAs(u) {
        Test.startTest();

        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => 1,
            ccrz.ccApiCart.CART_ENCID => cart.ccrz__EncryptedId__c
        };
  
        response = ccrz.ccAPICart.place(request);
        isSuccess = (Boolean) response.get(ccrz.ccAPI.SUCCESS);
        Test.stopTest();
        }

        System.assert(isSuccess);

    }
    
    static testmethod void postProcessFPTest(){
        util.initCallContext();
  		ccrz__E_Order__c order = util.getOrder();
        order.ccrz__Account__c = util.getAccount().Id;
        order.ccrz__Contact__c = util.getContact().Id;
        order.ccrz__Storefront__c = 'parts';
        order.ccrz__User__c = UserInfo.getUserId();
        update order;
        System.debug('order==>'+order);
        List<ccrz__E_TransactionPayment__c> lstTransToInsert = new  List<ccrz__E_TransactionPayment__c>();
        ccrz__E_TransactionPayment__c trans = createTransactionPayment(order);
        lstTransToInsert.add(trans);
        ccrz__E_TransactionPayment__c trans2 = createTransactionPayment(order);
        lstTransToInsert.add(trans2);
        insert lstTransToInsert;
        System.debug('trans==>'+lstTransToInsert);
        Test.startTest();
        CCPDCLogicCartPlace.postProcessFP(order.Id);
        Test.stopTest();
        
    }
    
    static testmethod void postProcessFPTest2(){
        util.initCallContext();
  		ccrz__E_Order__c order = util.getOrder();
        order.ccrz__Account__c = util.getAccount().Id;
        order.ccrz__Contact__c = util.getContact().Id;
        order.ccrz__Storefront__c = 'parts';
        order.ccrz__User__c = UserInfo.getUserId();
        update order;
        System.debug('order==>'+order);
        List<ccrz__E_TransactionPayment__c> lstTransToInsert = new  List<ccrz__E_TransactionPayment__c>();
        ccrz__E_TransactionPayment__c trans = createTransactionPayment(order);
        lstTransToInsert.add(trans);

        insert lstTransToInsert;
        System.debug('trans==>'+lstTransToInsert);
        Test.startTest();
        CCPDCLogicCartPlace.postProcessFP(order.Id);
        Test.stopTest();
        
    }
    
    static testmethod void postProcessFPTest3(){
        util.initCallContext();
  		ccrz__E_Order__c order = util.getOrder();
        order.ccrz__Account__c = util.getAccount().Id;
        order.ccrz__Contact__c = util.getContact().Id;
        order.ccrz__Storefront__c = 'parts';
        order.ccrz__User__c = UserInfo.getUserId();
        update order;
        System.debug('order==>'+order);
        List<ccrz__E_TransactionPayment__c> lstTransToInsert = new  List<ccrz__E_TransactionPayment__c>();
        ccrz__E_TransactionPayment__c trans = createTransactionPayment(order);
        lstTransToInsert.add(trans);
        ccrz__E_TransactionPayment__c trans2 = createTransactionPayment(order);
        trans2.ccrz__TransactionCode__c = 'Authorization Failed';
        lstTransToInsert.add(trans2);
        insert lstTransToInsert;
        System.debug('trans==>'+lstTransToInsert);
        Test.startTest();
        CCPDCLogicCartPlace.postProcessFP(order.Id);
        Test.stopTest();
        
    }
    
    public static ccrz__E_TransactionPayment__c createTransactionPayment(ccrz__E_Order__c order) {
        ccrz__E_TransactionPayment__c payment = new ccrz__E_TransactionPayment__c(
            ccrz__Account__c = order.ccrz__Account__c, 
            ccrz__AccountNumber__c = '************1111',
            ccrz__AccountType__c = 'cc',
            ccrz__Amount__c = 0,
            ccrz__CCOrder__c = order.Id,
            ccrz__Contact__c = order.ccrz__Contact__c,
            ccrz__CurrencyISOCode__c = 'USD',
            ccrz__ExpirationMonth__c = 4,
            ccrz__ExpirationYear__c = 25,
            ccrz__RequestAmount__c = 0,
            ccrz__Storefront__c = order.ccrz__Storefront__c,
            ccrz__SubAccountNumber__c = '123',
            ccrz__Token__c = '1234567890',
            ccrz__User__c = order.ccrz__User__c
        );
        return payment;
    }

}