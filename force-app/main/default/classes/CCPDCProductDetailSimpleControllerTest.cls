@isTest
public class CCPDCProductDetailSimpleControllerTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }
    
    static testmethod void controllerTest() {
        util.initCallContext();

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Product__c requiredProduct = util.createRequiredProduct(p);

        CCPDCProductDetailSimpleController c = null;
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
        
        Test.startTest();
        c = new CCPDCProductDetailSimpleController();
        Test.stopTest();

        System.assert(c != null);
    }

    static testmethod void addToCartTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductDetailSimpleController.addToCart(ctx, 'product-01', 1,'33');
        Test.stopTest();

        System.assert(result != null);
        //System.assert(result.success);
    }
    /*static testmethod void updateCartFPTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductDetailSimpleController.updateCartFP(ctx, 'product-01', 1,'33','testloc','test');
        Test.stopTest();

        System.assert(result != null);
        //System.assert(result.success);
        
       }*/
    static testmethod void checkExistOrderListNameTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        String wishlistName = 'wishlist';
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCProductDetailSimpleController.checkExistWishlistName(ctx, wishlistName);
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    }    
    
    static testmethod void getProductFitmentyearTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz__E_Product__c p = util.getProduct();
        Product_Fitment__c pdtFtmnt = new Product_Fitment__c();
        pdtFtmnt.Model__c = 'testModel';
        pdtFtmnt.ModelDesc__c = 'modelDes';
        pdtFtmnt.Year__c = 2020;
        pdtFtmnt.Make__c = 'testMake';
        pdtFtmnt.Related_To_Product__c = p.id;

       // String year = 2021;
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
       
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        insert pdtFtmnt;
        result = CCPDCProductDetailSimpleController.getProductFitmentyear(ctx,  p.ccrz__SKU__c);
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    }    
    static testmethod void getProductFitmentmakeTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

         ccrz__E_Product__c p = util.getProduct();
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
        
        Product_Fitment__c pdtFtmnt = new Product_Fitment__c();
        pdtFtmnt.Model__c = 'testModel';
        pdtFtmnt.ModelDesc__c = 'modelDes';
        pdtFtmnt.Year__c = 2020;
        pdtFtmnt.Make__c = 'testMake';
        pdtFtmnt.Related_To_Product__c = p.id;
       
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        insert pdtFtmnt;
        result = CCPDCProductDetailSimpleController.getProductFitmentmake(ctx,'2020',  p.ccrz__SKU__c);
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    }    
     /*static testmethod void getProductFitmentmodelTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

       // String year = 2021;
         ccrz__E_Product__c p = util.getProduct();
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
       
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCProductDetailSimpleController.getProductFitmentmodel(ctx,'1995','testmake' , p.ccrz__SKU__c);
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    } */   
    static testmethod void getProductFitmentengineTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

       // String year = 2021;
         ccrz__E_Product__c p = util.getProduct();
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
        
        Product_Fitment__c pdtFtmnt = new Product_Fitment__c();
        pdtFtmnt.Model__c = 'testModel';
        pdtFtmnt.ModelDesc__c = 'modelDes';
        pdtFtmnt.Year__c = 2020;
        pdtFtmnt.Make__c = 'testMake';
        pdtFtmnt.Related_To_Product__c = p.id;
        
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        insert pdtFtmnt;
        result = CCPDCProductDetailSimpleController.getProductFitmentengine(ctx,'2020','testmake','testModel','product-01');
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    }    
      static testmethod void getProductFitmentMessageTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

       // String year = 2021;
         ccrz__E_Product__c p = util.getProduct();
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
       
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCProductDetailSimpleController.getProductFitmentMessage(ctx,'1991','testmake','testmodel','testengineBMW',p.ccrz__SKU__c);
        
      
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    }    

    static testmethod void getSkuToQtyTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCProductDetailSimpleController.getSkuToQty(ctx);
        Test.stopTest();
        System.assert(result != null);

    }
    static testmethod void addToCartContTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation)CCPDCProductDetailSimpleController.addToCartCont(ctx, 'product-01', 1);
        
        // Verify that the continuation has the proper requests
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
        //system.assert(requests.get((String)CCPDCProductDetailSimpleController.stateMap.get('RequestLabel')) != null);
        
        // Perform mock callout 
        // (i.e. skip the callout and call the callback method)
        HttpResponse response = new HttpResponse();
        response.setBody('Mock response body');   
        // Set the fake response for the continuation     
        //Test.setContinuationResponse((String)CCPDCProductDetailSimpleController.stateMap.get('RequestLabel'), response);
        // Invoke callback method
        ccrz__E_Product__c p = util.getProduct();
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
        Object result = Test.invokeContinuationMethod(new CCPDCProductDetailSimpleController(), conti);
    }

    static testmethod void addToCartContCallbackTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        String encId = ccrz.cc_CallContext.currCartId;
        ccrz.cc_RemoteActionResult result = null;
     

        List<ccrz.ccApiCart.LineData> newLines = new List<ccrz.ccApiCart.LineData>();
        ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
        newLine.sku = 'product-01';
        newLine.quantity = 10;
        newLines.add(newLine);

        Object quantityMap  = new Map<String,Map<String,Decimal>>{'123product-01' => new Map<String,Decimal>{'123product-01'=>12.99}};
        Map<String, Object> state = new Map<String, Object>{'quantityMap' => quantityMap};
        Map<String, Object> inputData = new Map<String, Object>{ccrz.ccApiCart.LINE_DATA =>newLines, ccrz.ccApi.API_VERSION => 1,ccrz.ccApiCart.CART_ENCID => encId};
        state.put('inputData',inputData);
        state.put('ccrzContext', ctx);
        state.put('RequestLabel','Continuation-1');
        //ccrz.ccApiCart.LINE_DATA
        Test.startTest();
        result =  CCPDCProductDetailSimpleController.addToCartContCallback(state);

        Test.stopTest();
    }
    
    static testmethod void getFPProductInventoriesTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<String> skus = new List<String>();
        skus.add('product-01');

       // String year = 2021;
         ccrz__E_Product__c p = util.getProduct();
        System.debug('p==>'+p);
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
       
        ccrz.cc_RemoteActionResult result = null;
        List<String> lstLoc = new List<String>();
        lstLoc.add('testLocation');
        Test.startTest();
        result = CCPDCProductDetailSimpleController.getFPProductInventories(ctx,p.ccrz__SKU__c,lstLoc);
        
      
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    }  
    
    static testmethod void getPriceTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<String> skus = new List<String>();
        skus.add('product-01');

       // String year = 2021;
         ccrz__E_Product__c p = util.getProduct();
        System.debug('p==>'+p);
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
        String prodBeans = '[{"SKU":"' + p.ccrz__SKU__c +
            '","poolNumber":"' + '1234' +
            '","productId":"' + p.Id +
            '","partNumber":"' + '123' + '"}]';
       
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCProductDetailSimpleController.getPrice(ctx,prodBeans,'testLocation');
        
      
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    } 
    static testmethod void getProductFitmentModelTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_CallContext.storefront = util.STOREFRONT;
        List<String> skus = new List<String>();
        skus.add('product-01');
        ccrz__E_Product__c p = util.getProduct();
        
        Product_Fitment__c pdtFtmnt = new Product_Fitment__c();
        pdtFtmnt.Model__c = 'testModel';
        pdtFtmnt.ModelDesc__c = 'modelDes';
        pdtFtmnt.Year__c = 2020;
        pdtFtmnt.Make__c = 'testMake';
        pdtFtmnt.Related_To_Product__c = p.id;
        

       // String year = 2021;
         
        System.debug('p==>'+p);
        
       
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        insert pdtFtmnt;
        result = CCPDCProductDetailSimpleController.getProductFitmentModel(ctx,'2020','testMake','product-01');
        
      
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    } 
    
    static testmethod void getSKUQuantityFPTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz__E_Product__c p = util.getProduct();

        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCProductDetailSimpleController.getSKUQuantityFP(ctx, p.ccrz__SKU__c);
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    } 
    static testmethod void addToCartFPTest(){
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE /><QPRICE>0.000</QPRICE><QPRCCATID /><CUSTNO>999999</CUSTNO><CUSTBR>0</CUSTBR><LOCATE>DA</LOCATE><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL /><PLEXCPT /><LLVL /><MULTPLR>0.0000</MULTPLR><PRLOOP /><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE /><QPRICE>0.000</QPRICE><QPRCCATID /><CUSTNO>999999</CUSTNO><CUSTBR>0</CUSTBR><LOCATE>DA</LOCATE><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));
		List<Map<String,Object>> prodList = new List<Map<String,Object>>();
        String location = 'DA';

        Map<String,Object> prod1 = new Map<String,Object>{
            'SKU' => 'product-01',
            'partNumber' => 'product-01',
            'poolNumber' => '123'
        };
        Map<String,Object> prod2 = new Map<String,Object>{
            'SKU' => 'product-02',
            'partNumber' => 'product-02',
            'poolNumber' => '123'
        };
        prodList.add(prod1);
        prodList.add(prod2);

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        Map<String, Decimal> response2 = null;
        
        
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz__E_Product__c p = util.getProduct();
        String prodBeans = '[{"SKU":"' + p.ccrz__SKU__c +
            '","poolNumber":"' + '1234' +
            '","productId":"' + p.Id +
            '","partNumber":"' + '123' + '"}]';

        ccrz.cc_RemoteActionResult result = null;
        ccrz.cc_CallContext.storefront = 'store1';
        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = 'store1';
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
        
        Test.startTest();
        //ccrz.cc_util_Reflection.createStorefrontSetting('parts');
        result = CCPDCProductDetailSimpleController.addToCartFP(ctx, p.ccrz__SKU__c,2,'10000', prodBeans);
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    }  
}