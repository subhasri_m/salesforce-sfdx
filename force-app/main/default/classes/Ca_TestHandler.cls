@isTest
private class Ca_TestHandler {
    
    static testmethod void test1()
    {
        test.startTest();
        ca_taskHandler Handler1 = new Ca_TaskHandler();
        Handler1.method1(); 
        Ca_SRHandler handler3 = new Ca_SRHandler();
        Handler3.method1();
        Ca_ProblemHandler handler4 = new Ca_ProblemHandler();
        Handler4.method1();
        Ca_IncidentHandler handler5 = new Ca_IncidentHandler();
        handler5.method1();
        test.stopTest();
    }
    
}