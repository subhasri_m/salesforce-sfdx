@isTest

private class TopFiveOpportunityController_Test{
@testSetup static void setupTestData(){
        Profile saProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        
        //Create User Data
        User saUser = new User(Alias = 'newUser', Email='test12123@email.com', 
                               EmailEncodingKey='UTF-8',
                               LastName='Testing',
                               LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',
                               ProfileId = saProfile.Id, 
                               TimeZoneSidKey='America/Los_Angeles',
                               UserName='testxxxxx222@email.com',
                               Create_Accounts__c = true,
                               Salesman_Number__c = '1-1234',
                               Region__c= 'Central');   
        insert saUser;
        OppPageStages__c stageName = new OppPageStages__c (Name='Test', StageNames__c='01-New;01-Old');
        insert stageName;
        
        System.runAs(saUser){
        Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = saUser.Id);  
        insert acc;
        
        
        Opportunity opp = new Opportunity (name = 'opp1',accountid = acc.id,Type ='New Business',LeadSource ='FP Intelligence',CloseDate=date.today(),stagename='01-New');
        insert opp;

        }
    }
    
    static testmethod  void Test1()
    {
        Account acc = [Select Id, Name, Type, OwnerId from Account LIMIT 1];
        ApexPages.StandardController stdController = new ApexPages.StandardController(acc);
        TopFiveOpportunityController Controller = new TopFiveOpportunityController(stdController);
        Controller.getOppsList();
        //Controller.getProductBunch();
        Controller.getReasonLost();
        Controller.updateStage = 'Closed Won';
        Controller.getOppStage();
        //Controller.getOppFilterStage();
        //Controller.UpdateProductBunch = 'WHEEL AND RIM - 3505';
        controller.UpdateReasonLost ='Timeframe';
        TopFiveOpportunityController.WrapperClass wrapper = new TopFiveOpportunityController.WrapperClass();
        List<Opportunity> opp = [select id from opportunity limit 1];
        wrapper.OpportunityId = opp[0].Id;
        wrapper.Selected = true;
         List<TopFiveOpportunityController.wrapperclass> WrapperList  = new List<TopFiveOpportunityController.wrapperclass>(); 
        WrapperList.add(Wrapper);
        controller.UpdateOppRecords(WrapperList);
        Controller.updateTableData();
    } 

}