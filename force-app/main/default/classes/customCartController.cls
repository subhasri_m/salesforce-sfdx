global class customCartController{
  global string cartId{get;set;}
  global List<CartItemWrapper> Top3cartItems{
        get {
            list<CartItemWrapper> cartItemwraplst = new list<CartItemWrapper>(); 
            List<ID> cartItemProductIds = new List<ID>();
            Map<id,ccrz__E_ProductMedia__c> imageMap = new map<id,ccrz__E_ProductMedia__c>();
            List<ccrz__E_CartItem__c> cartItemlst;
            list<ccrz__E_ProductMedia__c> prdImageList;
            if(cartId != null) {
            cartItemlst= [select id,ccrz__Product__r.name,ccrz__Product__r.FP_Brand_Name__c,ccrz__Product__r.DSP_Part_Number__c ,ccrz__Quantity__c from ccrz__E_CartItem__c where ccrz__cart__c = :cartId order by createddate asc limit 3 ];
            if (cartItemlst!=null){
            for(ccrz__E_CartItem__c cartitem:  cartItemlst)
            {
                cartItemProductIds.add(cartitem.ccrz__Product__c);
            }
            prdImageList= [select ccrz__URI__c,ccrz__Product__c from ccrz__E_ProductMedia__c where ccrz__Product__c in: cartItemProductIds and ccrz__MediaType__c = 'Product Image']; 
            if (!prdImageList.IsEmpty()){
            for(ccrz__E_ProductMedia__c prodMed : prdImageList)
            {
                imageMap.put(prodMed.ccrz__Product__c, prodMed);
            } 
            for (ccrz__E_CartItem__c cartitem : cartItemlst ){ // This loop will run at the max 3 times.
                    CartItemWrapper cartItemwrap = new CartItemWrapper ();
                    cartItemwrap.prdctName = cartitem.ccrz__Product__r.name ;
                    cartItemwrap.prdctBrand = cartitem.ccrz__Product__r.FP_Brand_Name__c ;
                    cartItemwrap.prdctPartNo= cartitem.ccrz__Product__r.DSP_Part_Number__c ;
                    cartItemwrap.cartQuantity= Integer.ValueOf(cartitem.ccrz__Quantity__c) ;
                    
                    if (imageMap.containsKey(cartitem.ccrz__Product__c))
                    cartItemwrap.prdctImageURI= imageMap.get(cartitem.ccrz__Product__c).ccrz__URI__c; 
                    else
                    cartItemwrap.prdctImageURI ='https://s3.us-east-2.amazonaws.com/epdc/Image-Missing.jpg'; 
                             
                    cartItemwraplst.add(cartItemwrap);
                }
                }
               }
                     
            }
           return cartItemwraplst ;  
        }
        set;  
    } 
    
    public class CartItemWrapper{
        public string prdctName{get;set;}
        public string prdctBrand{get;set;}
        public string prdctPartNo{get;set;}
        public Integer cartQuantity{get;set;}
        public string prdctImageURI{get;set;}
        
        public CartItemWrapper(){
            prdctName = '';
            prdctBrand = '';
            prdctPartNo = '';
            prdctImageURI = '';
            cartQuantity = 0;
        }
        
    }      
}