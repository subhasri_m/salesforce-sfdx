@isTest
private class CCPDCCartActionTotalsCtrlHelperTest {
	
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void testGetAdminsForAccount(){
    	CC_FP_Contract_Account_Permission_Matrix__c apermissionMatrix= new CC_FP_Contract_Account_Permission_Matrix__c();
		apermissionMatrix.Is_Admin__c = true;
		Account account = util.getAccount();
		Contact contact = util.getContact();
		apermissionMatrix.Account__c = account.Id;
		apermissionMatrix.Contact__c = contact.Id;
		insert apermissionMatrix;


    	List<CC_FP_Contract_Account_Permission_Matrix__c> results = CCPDCCartActionTotalsControllerHelper.getAdminsForAccount(account.Id);
    	CC_FP_Contract_Account_Permission_Matrix__c result = results.get(0);
    	System.assertEquals(result.Account__c,account.Id);

    }
	
}