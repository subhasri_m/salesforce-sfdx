global with sharing class CCPDCPDPInterchangeController {

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getRelatedProducts(ccrz.cc_RemoteActionContext ctx, String sku){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        try{
            if(sku!=null){
                ccrz__E_Product__c theproduct = CCPDCProductDAO.getProductTypeBySku(sku);
                List<Map<String, String>> resultData = new List<Map<String, String>>();
                //Id contactId = String.valueOf(ccrz.cc_CallContext.currContact.Id); not used in the code and causing exception for FP guest user access. Hence commented.
                //List<CC_FP_Contract_Account_Permission_Matrix__c> matrixList = CCFPContactAccountPermissionMatrixDAO.getAccountsForContact(contactId);
                String productId = String.valueOf(theproduct.Id);
                Set<String> productIds = new Set<String>();
                productIds.add(productId);
                List<ccrz__E_RelatedProduct__c> relProducts = CCPDCProductDAO.getRelatedProductsFromId(productIds);

                Set<String> relatedProductIds = new Set<String>();
                for(ccrz__E_RelatedProduct__c relprod : relProducts){
                    relatedProductIds.add((String.valueOf(relprod.ccrz__RelatedProduct__c)));
                }

                Map<String, Object> fetchInputData = new Map<String, Object>{
                    ccrz.ccApiProduct.PRODUCTIDLIST => relatedProductIds,
                    ccrz.ccApi.API_VERSION => 3,
                    ccrz.ccApiProduct.PARAM_INCLUDE_PRICING => true,
                    ccrz.ccAPI.SIZING => new Map<String, Object>{
                        ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
                            ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_L
                        }
                    } 
                };
                Map<String, Object> result = ccrz.ccAPIProduct.fetch(fetchInputData);
                Boolean isSuccess = (Boolean) result.get(ccrz.ccAPI.SUCCESS);
                if(isSuccess){
                    List<Map<String,Object>> productList = (List<Map<String,Object>>)result.get('productList');
                    //Map<String,Map<String, Object>> priceResults = (Map<String,Map<String, Object>>)result.get('PriceResults');
                    ////for(String pricekey: priceResults.keySet()){
                    //    Map<String, Object> currentData = priceResults.get(pricekey);
                    //    Map<String, String> newProductData = new Map<String, String>();
                    //    newProductData.put('sku',String.valueOf(currentData.get('sku')));
                    //    resultData.add(newProductData);
                    //}
                    
                    if(result != null && !result.isEmpty()){
                        for(Map<String,Object> product : productList){
                        //product.put('CCFPPricingVisibility',matrixList[0].Pricing_Visibility__c);
                        List<Map<String,Object>> medias = (List<Map<String,Object>>)product.get('EProductMedias');
                        if(medias != null){
                            for(Map<String,Object> media : medias){
                                if(String.valueOf(media.get('mediaType')) == 'Product Image Thumbnail'){
                                   product.put('thumbnailURI',String.valueOf(media.get('URI')));
                                }

                            }
                        }

                        }

                    }
                    CCAviPageUtils.buildResponseData(response,true, new Map<String,Object>{
                        'interChangeProducts' => productList
                    });

                }

            } else {
                CCAviPageUtils.buildResponseData(response,false, new Map<String,Object>{'message' => 'can not find relatedProduct'});
            }

        } catch (Exception e) {
          CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }

        return response;
    }
    
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getCrossReferenceProducts(ccrz.cc_RemoteActionContext ctx, String sku){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        try{
            if(sku!=null){                
               List<Product_Cross_Reference__c> crossReferences =  CCPDCProductDAO.getCrossReferencesForProduct(sku,ccrz.cc_CallContext.storefront);
               Map<String,String> crossReferenceMap = new Map<String, String>();               
                for(Product_Cross_Reference__c crossReference : crossReferences) {
                    if(String.isBlank(crossReference.Brand_Name__c)){
                        crossReference.Brand_Name__c = 'Other';
                    }
                    if(crossReferenceMap.ContainsKey(crossReference.Brand_Name__c)) {
                        String interchangeData = crossReferenceMap.get(crossReference.Brand_Name__c);
                        interchangeData = interchangeData + ', ' + crossReference.Interchange_Part_Number__c;
                        crossReferenceMap.put(crossReference.Brand_Name__c,  interchangeData);
                    } else {
                        crossReferenceMap.put(crossReference.Brand_Name__c,  crossReference.Interchange_Part_Number__c);
                    }
                }
                List<Product_Cross_Reference__c> crossReferenceList = new List<Product_Cross_Reference__c>();
                for(string  brand :  crossReferenceMap.keySet()) {
                    Product_Cross_Reference__c cr = new Product_Cross_Reference__c();
                    cr.Brand_Name__c = brand;
                    cr.Interchange_Part_Number__c = crossReferenceMap.get(brand);
                    crossReferenceList.add(cr);
                }                
                CCAviPageUtils.buildResponseData(response,true, new Map<String, Object>{'crossReferences' => crossReferenceList});
            } else {
                CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => 'bad sku'});
            }         
         } catch (Exception e) {
              CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }

        return response;
    }

}