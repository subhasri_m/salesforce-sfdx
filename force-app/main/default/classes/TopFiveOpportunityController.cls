public class TopFiveOpportunityController { 
    
    public Boolean updateSuccessful{get;set;} //Added By Likhit on 13-Oct-2020
    public string Accid{get; set;}
    private final Account Accountid ; 
    public Boolean IsAllowed {get;set;}
    public String updateStage{get; set;}
    public String SelectedStage {get;set;}
    public String UpdateReasonLost {get; set;}
    public String header{get; set;}  
    List<OppPageStages__c> CustomSetting;
    List<WrapperClass> WrapperList {get; set;}
    List<WrapperClass> SelectedOppList {get; set;}
    public Boolean IsUserOSR_ISR {get;set;}
    public Boolean IsNotOSR_ISR {get;set;}
    public Boolean IsNatSR {get;set;}// if user is Sales Rep (National)
    
    public TopFiveOpportunityController (ApexPages.StandardController stdController) 
    {
        
        this.Accountid = (Account)stdController.getRecord();
        IsAllowed=false;
        Accid = Accountid.id;
        Account acc = [Select ownerId, Iseries_Company_code__c from Account where id =:Accountid.id limit 1];
        Boolean hasCustomPermission = FeatureManagement.checkPermission('Sales_Calls_Full_Access');
        if (acc.OwnerId == UserInfo.getUserId() || hasCustomPermission == true || acc.Iseries_Company_code__c =='2'){
            IsAllowed=true;
        }
        IsNatSR= false;
        CustomSetting = OppPageStages__c.getAll().values();
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        
        
        
        if (PROFILE[0].Name == '*FleetPride: Sales Rep (OSR)' || PROFILE[0].Name == '*FleetPride: Sales Rep (ISRs)')
        {
            IsUserOSR_ISR = true;
            IsNotOSR_ISR  = false;
            
        }
        
        else 
        {
            IsUserOSR_ISR = false;
            IsNotOSR_ISR = true;
        }
        if ( PROFILE[0].Name == '*FleetPride: Sales Rep (National)' ){
            IsNatSR = true;
            IsNotOSR_ISR  = false;
        }
    }
    
    public List<SelectOption> getOppStage()
    {
        List<SelectOption> options = new List<SelectOption>();
        String ValidStageName = CustomSetting[0].StageNames__c;
        List<String> StageNamesList =  ValidStageName.split(';');
        System.debug('StageNamesList--'+StageNamesList);
        Set<string> StageNamesSet = new Set<string>();
        StageNamesSet.addall(StageNamesList );
        Schema.DescribeFieldResult fieldResult = Opportunity.Stagename.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('','--Select--')); 
        System.debug('StageNamesSet-->'+StageNamesSet);
        for( Schema.PicklistEntry f : ple)
        {
            
            System.debug('values-->'+f.getLabel());
            if (StageNamesSet.contains(f.getLabel()) && f.getLabel() !='01-New'){ 
                
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }       
        return options;
    }
    
    public List<SelectOption> getReasonLost()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Opportunity.Reason_Lost__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('','--Select--'));   
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    
    public List<WrapperClass> getOppsList() 
        
    {
        WrapperList = new List<WrapperClass>();
        List<Opportunity> OppList = new List<Opportunity>(); 
        if (Accountid!=null)
        {
            OppList =  [select id,LeadSource,createdDate,CloseDate,Reason_Lost__c,recordtype.name, Name,StageName,Type,owner.name, Account.name, accountid, ownerid,amount  from opportunity where accountid =:Accountid.id and LeadSource ='FP Intelligence' and Stagename like '%New%' order by amount desc limit 6];
            if (!opplist.isEmpty()){
                
            }
        }
        
        for (Opportunity OppVar :OppList)
        {
            WrapperClass OWC = new WrapperClass(); 
            owc.LeadSource =OppVar.LeadSource;
            Owc.createdDate  = date.newinstance(OppVar.CreatedDate.year(), OppVar.CreatedDate.month(), OppVar.CreatedDate.day());
            owc.ReasonLost = OppVar.Reason_Lost__c;
            owc.ClosedDate = OppVar.CloseDate;
            owc.AccountName = OppVar.Account.name;
            owc.OpportunityName = OppVar.Name;
            owc.OpportunityStage = oppvar.StageName;
            owc.OppAmount = Oppvar.amount;
            owc.OpportunityOwner = oppvar.owner.name;
            owc.OpportunityId = oppvar.id;
            owc.OpportunityType = oppvar.recordtype.name;
            WrapperList.add(OWC); 
        }
        
        return WrapperList; 
    }  
    
    public pagereference updateTableData() {
        //this.accountname=ApexPages.currentPage().getParameters().get('accountname');
        SelectedOppList=new List<WrapperClass>();
        for(WrapperClass item: WrapperList)
        {
            if(item.selected)
            {
                SelectedOppList.add(item);
            } 
        }         
        
        System.debug('Selected-->'+SelectedOppList);
        if(SelectedOppList.isEmpty())
        {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'No items were selected to process'));}
        
        if(updateStage==null)	
        {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Please select a stage before proceeding'));}	
        
        if(!SelectedOppList.isEmpty() && updateStage!=null )
        {
            UpdateOppRecords(SelectedOppList);
        }
        return null;
    }
    
    @testVisible
    private void UpdateOppRecords(List<WrapperClass> SelectedList)
    { 
        Boolean ValidUpdate = true;    
        List<Opportunity> UpdateOppList = new List<Opportunity>();
        for (WrapperClass itemVar: SelectedList)
        {
            Opportunity OppRecord = new Opportunity(); 
            OppRecord.id = itemVar.OpportunityId;
            OppRecord.StageName= updateStage;
            
            
            opprecord.Reason_Lost__c = UpdateReasonLost;
            
            
            if (updateStage.containsignoreCase('Lost') && opprecord.Reason_Lost__c ==null)
                
            {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Select Reason Lost'));
             ValidUpdate = false;
             updateSuccessful = false; /*Added by likhit on 13-Oct-2020. 
										On the basis of this updateSuccessful Variable 
										It redirects TopFiveOpp page to Account page */
            }   
            
            UpdateOppList.add(OppRecord);
        }
        
        if (!UpdateOppList.isEmpty() && ValidUpdate == true)
        {
            update UpdateOppList;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Updated Successfully'));
            updateSuccessful = true; /*Added by likhit on 13-Oct-2020. 
										On the basis of this updateSuccessful Variable 
										It redirects TopFiveOpp page to Account page */
        }
        
    }
    
    
    public class WrapperClass
    {
        Public String AccountName{get;set;}
        Public String OpportunityType {get;set;}
        public String Accountid{get;set;}
        
        Public Date createdDate  {get;set;}
        Public String LeadSource {get;set;} 
        Public String ReasonLost {get;set;}
        Public date ClosedDate {get;set;}
        
        Public String OpportunityId{get;set;}
        Public string OpportunityStage{get;set;}
        Public String OpportunityName{get;set;}
        Public String OpportunityOwner{get;set;}
        Public Decimal OppAmount{get; set;}
        public Boolean Selected{get;set;}
        
        public WrapperClass()
        {
            AccountName='';
            Accountid='';
            OpportunityType='';
            OpportunityId='';
            OpportunityStage='';
            OpportunityName='';
            OpportunityOwner='';
            Selected = false;
            OppAmount = 0;
            
        }
        
    }
    
}