public without sharing class CCPDCUserDAO {
    /**
     *
     * Query User records and associated Contact data for the specified accounts
     *
     */
    public static Map<Id, User> queryUsersForAccounts(Set<Id> accountIds){
        return new Map<Id, User>([
            SELECT
                Id
                , Username
                , Email
                , IsActive
                , ProfileId
                , LocaleSidKey 
                , TimeZoneSidKey
                , LanguageLocaleKey
                , EmailEncodingKey
                , ContactId
                , Contact.Firstname
                , Contact.Lastname
                , Contact.Email
                , Contact.Phone
                , Contact.AccountId
                , Contact.ccrz__CompanyName__c
            FROM
                User
            WHERE
                Contact.AccountId in :accountIds
                AND IsActive = true
        ]);
    }

    public static User queryUserForUserId(Id userId){
        return [
            SELECT
                Id
                , Username
                , IsActive
                , ProfileId
                , LocaleSidKey 
                , TimeZoneSidKey
                , LanguageLocaleKey
                , EmailEncodingKey  
                , ContactId
                , Contact.Firstname
                , Contact.Lastname
                , Contact.Email
                , Contact.Phone
                , Contact.AccountId
            FROM
                User
            WHERE
                Id = :userId
            LIMIT
                1
        ];
    }

    public static User queryUserForUserName(String name){
        User theUser = null;
        List<User> userList = [
            SELECT
                Id
                , Username
                , IsActive
                , ProfileId
                , LocaleSidKey 
                , TimeZoneSidKey
                , LanguageLocaleKey
                , EmailEncodingKey  
                , ContactId
                , Contact.Firstname
                , Contact.Lastname
                , Contact.Email
                , Contact.Phone
                , Contact.AccountId
            FROM
                User
            WHERE
                Username = :name
        ];

        if (!userList.isEmpty()) {
            theUser = userList[0];
        }

        return theUser;
    }

    public static User queryUserForContactId(Id contactId){
        return [
            SELECT
                Id
                , Username
                , IsActive
                , ProfileId
                , LocaleSidKey 
                , TimeZoneSidKey
                , LanguageLocaleKey
                , EmailEncodingKey  
                , ContactId
                , Contact.Firstname
                , Contact.Lastname
                , Contact.Email
                , Contact.Phone
                , Contact.AccountId
            FROM
                User
            WHERE
                ContactId = :contactId
            LIMIT
                1
        ];
    }

    public static Contact queryContactForContactId(Id contactId){
        return [
            SELECT
                Id
            FROM
                Contact
            WHERE
                Id = :contactId
            LIMIT
                1
        ];
    }

    public static Map<Id, User> getUsersMap(List<String> ids) {
        return new Map<Id, User>([
            SELECT
                Id,
                Username,
                ContactId,
                Contact.Firstname,
                Contact.Lastname,
                Contact.Name
            FROM
                User
            WHERE
                Id IN :ids
        ]);
    }
    
    public static Account geteffAccount (String accountID){
    return [select OwnerID, Name from Account where id = : accountID limit 1];
    }
    
    public static User getAccOwner (String ownerID){
    return [select FirstName, LastName, Phone, Email from User where Id = :ownerID limit 1];
    }
    
}