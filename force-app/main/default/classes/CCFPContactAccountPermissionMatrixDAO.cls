public without sharing class CCFPContactAccountPermissionMatrixDAO {
    public static List<CC_FP_Contract_Account_Permission_Matrix__c> getPermissionsForAccount(Id accountId) {
        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = [
            SELECT 
                Id, 
                Name,
                Account__c,
                Allow_Access_To_Accounting_Reports__c,
                Allow_Access_to_Sales_Reports__c,
                Allow_AR_Payments__c,
                Allow_Backorder_Release__c,
                Allow_Backorders__c,
                Allow_Checkout__c,
                Allow_Non_Free_Shipping_Orders__c,
                Allow_Override_ShipTo__c,
                Contact__c,
                Is_Admin__c,
                Maximum_Order_Limit__c,
                Pricing_Visibility__c
            FROM 
                CC_FP_Contract_Account_Permission_Matrix__c
            WHERE
                Account__c = :accountId
        ];
        return permissions;
    }

    public static CC_FP_Contract_Account_Permission_Matrix__c getPermissionsForAccountAndContact(Id accountId, Id contactId) {
        CC_FP_Contract_Account_Permission_Matrix__c matrix = null;
        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = [
            SELECT 
                Id, 
                Name,
                Account__c,
                Allow_Access_To_Accounting_Reports__c,
                Allow_Access_to_Sales_Reports__c,
                Allow_AR_Payments__c,
                Allow_Backorder_Release__c,
                Allow_Backorders__c,
                Allow_Checkout__c,
                Allow_Non_Free_Shipping_Orders__c,
                Allow_Override_ShipTo__c,
                Contact__c,
                Is_Admin__c,
                Maximum_Order_Limit__c,
                Pricing_Visibility__c,
                Can_View_Invoices__c
            FROM 
                CC_FP_Contract_Account_Permission_Matrix__c
            WHERE
                Account__c = :accountId AND Contact__c = :contactId
        ];
        if (permissions != null && !permissions.isEmpty()) {
            matrix = permissions[0];
        }
        return matrix;
    }

    public static List<CC_FP_Contract_Account_Permission_Matrix__c> getAccountsForContact(Id contactId) {
        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = [
            SELECT 
                Id, 
                Name,
                Pricing_Visibility__c,
                Account__c,
                Account__r.Id,
                Account__r.Name,
                Account__r.AccountNumber,
                Account__r.BillingStreet,
                Account__r.BillingCity,
                Account__r.BillingState,
                Account__r.BillingPostalCode,
                Account__r.BillingCountry,
                Account__r.ShippingStreet,
                Account__r.ShippingCity,
                Account__r.ShippingState,
                Account__r.ShippingPostalCode,
                Account__r.ShippingCountry,
                Account__r.ccrz__E_AccountGroup__c
            FROM 
                CC_FP_Contract_Account_Permission_Matrix__c
            WHERE
                Contact__c = :contactId
        ];
        return permissions;
    }

    public static List<CC_FP_Contract_Account_Permission_Matrix__c> getAdminsForAccount(Id accountId) {
        
        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = [
            SELECT 
                Id, 
                Name,
                Account__c,
                Allow_Checkout__c,
                Contact__c,                
                Is_Admin__c                                
            FROM 
                CC_FP_Contract_Account_Permission_Matrix__c
            WHERE
                Account__c = :accountId AND (Is_Admin__c = TRUE)
        ];
        return permissions;
    }

    public static CC_FP_Contract_Account_Permission_Matrix__c getPermissionsById(Id theId) {
        CC_FP_Contract_Account_Permission_Matrix__c permission = null;
        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = [
            SELECT 
                Id, 
                Name,
                Account__c,
                Allow_Access_To_Accounting_Reports__c,
                Allow_Access_to_Sales_Reports__c,
                Allow_AR_Payments__c,
                Allow_Backorder_Release__c,
                Allow_Backorders__c,
                Allow_Checkout__c,
                Allow_Non_Free_Shipping_Orders__c,
                Allow_Override_ShipTo__c,
                Contact__c,
                Is_Admin__c,
                Maximum_Order_Limit__c,
                Pricing_Visibility__c
            FROM 
                CC_FP_Contract_Account_Permission_Matrix__c
            WHERE
                Id = :theId
        ];

        if (!permissions.isEmpty()) {
            permission = permissions[0];
        }
        return permission;
    }
}