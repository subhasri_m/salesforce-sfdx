public with sharing class CCFPBoomiOrderPlacementAPI {

    /**
     *
     * Query the orders including:
     *  Order Data
     *  Shipping Data
     *  Account Data
     *  Order Item Data
     *  Product Data
     *  Order Item Groups
     *  Separately: Transaction Payment details for the order items
     *  
     * Group the order items using the Order Item Groups
     * Create CCAviBoomiAPI.CreateOrderCCRequest records for each order item group
     * Associate each Transaction Payment with the correct order request based on order item group
     * Make the requests
     *
    */
    //@future(callout=true)
    public static void placeOrder(Id orderId, List<ccrz__E_TransactionPayment__c> transactionPaymentList) {
        ccrz__E_Order__c theOrder = CCPDCOrderDAO.getOrderDetails(orderId);
        try {

            // Create map of OrderItemGroup Id => OrderItems
            Map<Id, List<ccrz__E_OrderItem__c>> groupToOrderItemMap = new Map<Id, List<ccrz__E_OrderItem__c>>();
            Set<Id> theIds = new Set<Id>();

            for(ccrz__E_OrderItem__c orderItem :theOrder.ccrz__E_OrderItems__r){
                List<ccrz__E_OrderItem__c> oiList = groupToOrderItemMap.get(orderItem.ccrz__OrderItemGroup__c);
                if(oiList == null){
                    oiList = new List<ccrz__E_OrderItem__c>();
                    groupToOrderItemMap.put(orderItem.ccrz__OrderItemGroup__c, oiList);
                }
                oiList.add(orderItem);
                theIds.add(orderItem.Id);
            }

            // Create map of OrderItemGroup => TransactionPayments
            Map<Id, ccrz__E_TransactionPayment__c> groupToPaymentMap = new Map<Id, ccrz__E_TransactionPayment__c>();
            for(ccrz__E_TransactionPayment__c tp :transactionPaymentList){
                groupToPaymentMap.put(tp.CC_Order_Item_Group__c, tp);
            }

            Map<Id, List<CC_FP_Required_Order_Item__c>> requiredOrderItemMap = findRequiredOrderItems(theIds);

            // Create list of CreateOrderCCRequests
            List<CCAviBoomiAPI.CreateOrderCCRequest> createOrderRequests = new List<CCAviBoomiAPI.CreateOrderCCRequest>();
            // TODO get user and get perms, if use has no BO perm dont create BO
            Id accountId = ccrz.cc_CallContext.effAccountId;
            Id contactId = ccrz.cc_CallContext.currContact.Id;
            CCPDCUserPermissionsHelper.UserPermissions perms = CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);             
            Boolean allowBO = perms.allowBackorders;

            // For each order item group, call createOrderRequest
            for(ccrz__E_OrderItemGroup__c oiGroup :theOrder.ccrz__E_OrderItemGroups__r){

                if (!oiGroup.CC_FP_Is_Backorder__c || !theOrder.CC_FP_Is_Will_Call__c || theOrder.CC_FP_Is_Ship_Remainder__c || !allowBO) { // Don't send backorders for willcall

                    // Get the Order Items
                    List<ccrz__E_OrderItem__c> oiList = groupToOrderItemMap.get(oiGroup.Id);
                    // Get the payment
                    ccrz__E_TransactionPayment__c thePayment = groupToPaymentMap.get(oiGroup.Id);
                    if(thePayment == null){
                        thePayment = groupToPaymentMap.values()[0];
                    }
                    // Create the request
                    CCAviBoomiAPI.CreateOrderCCRequest orderRequest = createOrderRequest(theOrder, oiList, oiGroup, thePayment, requiredOrderItemMap);

                    // Add the request to the list
                    createOrderRequests.add(orderRequest);
                }
            }

            if (!createOrderRequests.isEmpty()) {
                CCAviBoomiAPI.CreateOrderCCResponse response = CCAviBoomiAPI.getCreateOrderCC(theOrder.ccrz__Storefront__c, createOrderRequests);
                 //RB JIRA PE-1630
                 if (response.requestBody.length() > 130000)
                  theOrder.BoomiRequest__c = (response.requestBody).substring(0, 130000);
                else
                  theOrder.BoomiRequest__c = response.requestBody;
                //RB JIRA PE-1630
                theOrder.BoomiResponse__c = response.responseBody;
                theOrder.BoomiResponseStatus__c = response.responseCode;
                if(response.apiCallSuccess){
                    theOrder.ccrz__OrderStatus__c = 'In Process';
                }
            }
        
        }
        catch(Exception e) {
            theOrder.BoomiResponse__c = 'Non-Boomi exception: ' + e.getMessage() + ' => ' + e.getStackTraceString();
            System.debug('Exception ' + e.getMessage());
            System.debug('Trace ' + e.getStackTraceString());
            System.debug('Cause ' + e.getCause());
        }
        update theOrder;
        // return response;
    }

    public static CCAviBoomiAPI.CreateOrderCCRequest createOrderRequest(ccrz__E_Order__c theOrder, List<ccrz__E_OrderItem__c> theOrderItems, ccrz__E_OrderItemGroup__c theOrderItemGroup, ccrz__E_TransactionPayment__c thePayment, Map<Id, List<CC_FP_Required_Order_Item__c>> requiredOrderItemMap){
        CCAviBoomiAPI.CreateOrderCCRequest theRequest = new CCAviBoomiAPI.CreateOrderCCRequest();

        // Get the right transaction payment
        // Create Header
        // Create Details
        // Create Payment

        theRequest.header = createHeader(theOrder, theOrderItemGroup, thePayment);
        theRequest.listDetails = createOrderDetails(theOrder, theOrderItems, theOrderItemGroup, requiredOrderItemMap);
        theRequest.payment = createPayment(thePayment);

        return theRequest;
    }

    public static CCAviBoomiAPI.CreateOrderCCHeader createHeader(ccrz__E_Order__c theOrder, ccrz__E_OrderItemGroup__c theGroup, ccrz__E_TransactionPayment__c thePayment){
        CCAviBoomiAPI.CreateOrderCCHeader ordHeader = new CCAviBoomiAPI.CreateOrderCCHeader();

        // Set static elements
        ordHeader.TicketStatus      = 'H'; // default to 'H'
        ordHeader.TicketType        = 'I'; // default to 'I'
        ordHeader.In_Out_City       = ''; // default to empty
        ordHeader.FreightCode       = 'P'; // default to 'P'

        // Set elements from the Order
        ordHeader.SellingCompany    = 2;
        ordHeader.CustomerNumber    = Integer.valueOf(theOrder.ccrz__Account__r.ISeries_Customer_Account__c);
        ordHeader.CustomerBranch    = Integer.valueOf(theOrder.ccrz__Account__r.ISeries_Customer_Branch__c);
        //ordHeader.SellingLocation = theOrder.CC_FP_Location__r.Location__c;
        if(theGroup.CC_FP_Is_Backorder__c){
            ordHeader.SellingLocation   = theOrder.CC_FP_Location__r.Location__c;
        }
        else {
            ordHeader.SellingLocation   = theGroup.CC_FP_DC_ID__c;
        }
        ordHeader.SalesDate         = convertToDateTime(theOrder.ccrz__OrderDate__c);
        ordHeader.CreatedDate       = convertToDateTime(theOrder.ccrz__OrderDate__c); // what format does the date need to be? May need to be 10/25/2017
        ordHeader.OMS_ReservationID = theOrder.ccrz__OriginatedCart__r.Reservation_Id__c;
        // remove 11-20-2017        ordHeader.PO_OrderNo        = theOrder.ccrz__PONumber__c; // default to empty
        // remove 11-20-2017        ordHeader.OrderBy           = theOrder.CreatedBy.Username; // what should this be?
        // If payment is PO, send 21. For Credit Cart, send 11.
        Integer paymentType = 22;
        if('io'.equalsIgnoreCase(thePayment.ccrz__AccountType__c)){
            paymentType = 21;
        }
        ordHeader.Cash_or_Charge    = paymentType;

        // Set elements from the Order Item Group
        ordHeader.CC_ShipmentID     = theOrder.ccrz__Storefront__c == 'parts' ? theGroup.Name : theGroup.Id; //ccrz__GroupName__c; // see if blank will work. We don't have this info
        ordHeader.ShipToAddr1       = theOrder.ccrz__ShipTo__r.ccrz__AddressFirstline__c;
        ordHeader.ShipToAddr2       = theOrder.ccrz__ShipTo__r.ccrz__AddressSecondLine__c;
        ordHeader.ShipToCity        = theOrder.ccrz__ShipTo__r.ccrz__City__c;
        ordHeader.ShipToCounty      = theOrder.ccrz__ShipTo__r.CC_FP_County__c;
        ordHeader.ShipToState       = theOrder.ccrz__ShipTo__r.ccrz__State__c;
        String zip = theOrder.ccrz__ShipTo__r.ccrz__PostalCode__c;
        if (zip != null) {
            zip = zip.remove('-');
        }
        ordHeader.ShipToZip         = Integer.valueOf(zip); 
        ordHeader.ShipToCountry     = theOrder.ccrz__ShipTo__r.ccrz__Country__c;
        ordHeader.PostalCode        = theOrder.ccrz__ShipTo__r.ccrz__PostalCode__c;

        // Order Item Group CC FP Total Amount
        ordHeader.TicketTotal       = theGroup.CC_FP_Total_Amount__c;
        // Order Item Group CC FP Total Amount - CC FP Tax Amount
        ordHeader.InventoryTotal    = theGroup.CC_FP_Subtotal_Amount__c;
        // Order Item Group CC FP Tax Amount
        ordHeader.TaxTotal          = theGroup.CC_FP_Tax_Amount__c;
		
        if(theOrder.CC_FP_Is_Will_Call__c){ // added 11/21 - tested 11/20 previously
            if(theGroup.CC_FP_DC_ID__c == theOrder.CC_FP_Location__r.Location__c || theGroup.CC_FP_Is_Backorder__c == true){
                ordHeader.DeliverMethod     = 'W'; // will call
            }
            else {
                ordHeader.DeliverMethod     = 'S'; // ship
            }
        }
        else {
            ordHeader.DeliverMethod     = 'S'; // ship
        }
        if (theOrder.ccrz__Storefront__c == 'parts'){
            if(theGroup.ccrz__ShipMethod__c == 'PickUp'){
              ordHeader.DeliverMethod = 'W';
            }else if(theGroup.ccrz__ShipMethod__c == 'LocalDelivery'){
               ordHeader.DeliverMethod = 'D';
            }
            else{
               ordHeader.DeliverMethod = 'S';
            }
        }
		//ordHeader.DeliverMethod    = theOrder.ccrz__Storefront__c == 'parts' ? 'W' : ordHeader.DeliverMethod; // setting W for pickup 
        ordHeader.CC_OrderID        = theOrder.ccrz__Storefront__c == 'parts' ? theOrder.Name : theOrder.Id; 
        // changed 11/21
        // remove 11-20-2017        ordHeader.OrderNotes        = theGroup.ccrz__Note__c;
        // remove 11-20-2017        ordHeader.Status            = theGroup.Status__c;
        ordHeader.ShipToAttn        = theOrder.ccrz__Contact__r.Name;
		ordHeader.OrderComments     = theOrder.Order_Comments__c;
        ordHeader.phoneNumber        = theOrder.ccrz__Contact__r?.Phone;
        return ordHeader;
    }

    public static List<CCAviBoomiAPI.CreateOrderCCDetail> createOrderDetails(ccrz__E_Order__c theOrder, List<ccrz__E_OrderItem__c> theOrderItems, ccrz__E_OrderItemGroup__c theOrderItemGroup, Map<Id, List<CC_FP_Required_Order_Item__c>> requiredOrderItemMap) {
        List<CCAviBoomiAPI.CreateOrderCCDetail> detailList = new List<CCAviBoomiAPI.CreateOrderCCDetail>();
        CC_Avi_Settings__mdt aviSettings = [SELECT Default_Salesman_Id__c from CC_Avi_Settings__mdt where Storefront__c = :theOrder.ccrz__Storefront__c limit 1];
        Integer lineNo = 1;
        if(theOrder.ccrz__PONumber__c == null){
            theOrder.ccrz__PONumber__c = '';
        }
        //Integer lineId = 0;
        for(ccrz__E_OrderItem__c orderItem :theOrderItems){
            if (orderItem.CC_FP_PONumber__c == null){
                orderItem.CC_FP_PONumber__c = theOrder.ccrz__PONumber__c;
            }
            CCAviBoomiAPI.CreateOrderCCDetail detail = new CCAviBoomiAPI.CreateOrderCCDetail();

            // Static and calculated values
            detail.OrderBy              = theOrder.CreatedBy.Username;
            // detail.CC_LineID         = String.valueOf(lineId++); // Changed 11/21
            detail.CC_LineID            = theOrder.ccrz__Storefront__c == 'parts' ? orderItem.Name : orderItem.Id; //change 11/21
            detail.LineNote             = orderItem.ccrz__Comments__c;
            detail.LineType             = 'I';
            detail.FillBoComplete       = '0';
            detail.SellingSalesman      = aviSettings.Default_Salesman_Id__c;
            detail.CustomerPO           = orderItem.CC_FP_PONumber__c;

            // From Order or Originated Cart
            // remove 11-20-2017            detail.OMSReservationID     = theOrder.ccrz__OriginatedCart__r.Reservation_Id__c; //'12345';
            detail.SalesDate            = convertToDateTime(theOrder.ccrz__OrderDate__c); // DateTime.newInstance(2017, 10, 25, 0, 0, 0);
            //detail.DemandLocation     = theOrderItemGroup.CC_FP_DC_ID__c;
            detail.DemandLocation       = theOrder.CC_FP_Location__r.Location__c;
            detail.CreatedDate          = convertToDateTime(theOrder.ccrz__OrderDate__c); // DateTime.newInstance(2017, 10, 25, 0, 0, 0);

            // From Order Item
            detail.SellLocQtyOrdered    = orderItem.ccrz__Quantity__c; // 1;
            if(theOrderItemGroup.CC_FP_Is_Backorder__c){
                detail.SellLocQtyShipped    = 0;//
            }
            else {
                detail.SellLocQtyShipped    = orderItem.ccrz__Quantity__c; // 4;
            }
            detail.Pool                 = Integer.valueOf(orderItem.ccrz__Product__r.Pool_Number__c); // 470;
            detail.PartNumber           = orderItem.ccrz__Product__r.Part_Number__c; // 'XA2524R132';
            detail.SellingPrice         = orderItem.ccrz__Price__c; // 178;
            detail.PriceOvrAprvID       = orderItem.CC_FP_Price_Ovr_UsrID__c; // '';
            detail.PriceOvrAprDate      = orderItem.CC_FP_Price_Ovr_Date__c; // DateTime.newInstance(2017, 10, 25, 0, 0, 0);

            // From Order Item Group (none)

            detailList.add(detail);

            List<CC_FP_Required_Order_Item__c> requiredItems = requiredOrderItemMap.get(orderItem.Id);
            if (requiredItems != null && !requiredItems.isEmpty()) {
                List<CCAviBoomiAPI.CreateOrderCCDetail> requiredDetails = createOrderDetailsForRequired(theOrder, orderItem, theOrderItemGroup, requiredItems, aviSettings);                
                Decimal requiredItemsSum = 0;
                for(CC_FP_Required_Order_Item__c ri: requiredItems){
                    if(ri != null) {
                        requiredItemsSum = requiredItemsSum + ri.Price__c;
                    }
                }
                detail.SellingPrice  = detail.SellingPrice - requiredItemsSum;              
                detailList.addAll(requiredDetails);
            }
        }

        if (theOrderItemGroup.ccrz__ShipAmount__c != null && theOrderItemGroup.ccrz__ShipAmount__c > 0) {
            CCAviBoomiAPI.CreateOrderCCDetail shipDetail = createShippingOrderDetails(theOrder, theOrderItemGroup, aviSettings);            
            detailList.add(shipDetail);
        }
        return detailList;
    }

    public static List<CCAviBoomiAPI.CreateOrderCCDetail> createOrderDetailsForRequired(ccrz__E_Order__c theOrder, ccrz__E_OrderItem__c orderItem, ccrz__E_OrderItemGroup__c theOrderItemGroup, List<CC_FP_Required_Order_Item__c> requiredItems, CC_Avi_Settings__mdt aviSettings) {
        List<CCAviBoomiAPI.CreateOrderCCDetail> detailList = new List<CCAviBoomiAPI.CreateOrderCCDetail>();
        
        for(CC_FP_Required_Order_Item__c item :requiredItems){
            CCAviBoomiAPI.CreateOrderCCDetail detail = new CCAviBoomiAPI.CreateOrderCCDetail();

            // Static and calculated values
            detail.OrderBy              = theOrder.CreatedBy.Username;
            detail.CC_LineID            = theOrder.ccrz__Storefront__c == 'parts' ? item.CC_Parent_Order_Item__r.Name + '-'+ item.Name : item.Id; //change 11/21
            detail.LineType             = 'C';//core parts change from I to C
            detail.FillBoComplete       = '0';
            detail.SellingSalesman      = aviSettings.Default_Salesman_Id__c;
            detail.CustomerPO           = orderItem.CC_FP_PONumber__c;
            detail.SalesDate            = convertToDateTime(theOrder.ccrz__OrderDate__c); // DateTime.newInstance(2017, 10, 25, 0, 0, 0);
            detail.DemandLocation       = theOrder.CC_FP_Location__r.Location__c;
            detail.CreatedDate          = convertToDateTime(theOrder.ccrz__OrderDate__c); // DateTime.newInstance(2017, 10, 25, 0, 0, 0);
            detail.SellLocQtyOrdered    = orderItem.ccrz__Quantity__c; // 1;
            if(theOrderItemGroup.CC_FP_Is_Backorder__c){
                detail.SellLocQtyShipped    = 0;//
            }
            else {
                detail.SellLocQtyShipped    = orderItem.ccrz__Quantity__c; // 4;
            }
            detail.Pool                 = Integer.valueOf(item.CC_Product__r.Pool_Number__c); // 470;
            detail.PartNumber           = item.CC_Product__r.Part_Number__c; // 'XA2524R132';
            detail.SellingPrice         = item.Price__c; // 178;
            detail.PriceOvrAprvID       = orderItem.CC_FP_Price_Ovr_UsrID__c; // '';
            detail.PriceOvrAprDate      = orderItem.CC_FP_Price_Ovr_Date__c; // DateTime.newInstance(2017, 10, 25, 0, 0, 0);

            detailList.add(detail);
        }

        return detailList;
    }

    public static CCAviBoomiAPI.CreateOrderCCDetail createShippingOrderDetails(ccrz__E_Order__c theOrder, ccrz__E_OrderItemGroup__c theOrderItemGroup, CC_Avi_Settings__mdt aviSettings) {
        CCAviBoomiAPI.CreateOrderCCDetail detail = new CCAviBoomiAPI.CreateOrderCCDetail();

        // Static and calculated values
        detail.OrderBy = theOrder.CreatedBy.Username;
        detail.CC_LineID = theOrderItemGroup.Id + '-shipping';
        detail.LineNote = null;
        detail.LineType = 'N';
        detail.FillBoComplete = '0';
        detail.SellingSalesman = aviSettings.Default_Salesman_Id__c;
        detail.CustomerPO = theOrder.ccrz__PONumber__c;

        detail.SalesDate = convertToDateTime(theOrder.ccrz__OrderDate__c);
        detail.DemandLocation = theOrder.CC_FP_Location__r.Location__c;
        detail.CreatedDate = convertToDateTime(theOrder.ccrz__OrderDate__c);

        detail.SellLocQtyOrdered = 1;
        detail.SellLocQtyShipped = 1;
        detail.Pool = 0;
        detail.PartNumber = 'SHIPPING &amp; HANDLING';
        detail.SellingPrice = theOrderItemGroup.ccrz__ShipAmount__c;
        detail.PriceOvrAprvID = null;
        detail.PriceOvrAprDate = null; 

        return detail;
    }

    public static CCAviBoomiAPI.CreateOrderCCPayment createPayment(ccrz__E_TransactionPayment__c thePayment) {
        CCAviBoomiAPI.CreateOrderCCPayment payment = new CCAviBoomiAPI.CreateOrderCCPayment();
        // Common elements
        payment.AuthorizationDate           = thePayment.ccrz__TransactionTS__c; // DateTime.newInstance(2017, 10, 24, 0, 0, 0);
        payment.AuthorizationAmount         = thePayment.ccrz__Amount__c; // 101;

        // If it's credit card
        if(!'io'.equalsIgnoreCase(thePayment.ccrz__AccountType__c)){
            payment.CardBrand                   = thePayment.ccrz__PaymentType__c; // 'Visa';
            String month = String.valueOf(thePayment.ccrz__ExpirationMonth__c);
            if (month.length() == 1) {
                month = '0' + month;
            }
            String year = String.valueOf(thePayment.ccrz__ExpirationYear__c);
            if (year.length() == 1) {
                year = '0' + year;
            }
            payment.ExpirationDate              = month + year; //'0319';
            payment.RetrievalReferenceNumber    = thePayment.ccrz__TransactionCode__c; // 'ART23';
            payment.TokenId                     = thePayment.ccrz__Token__c; // '123456';
            String mid = '';

            //CCAviBillTrustSettings__c btSettings = CCAviBillTrustSettings__c.getInstance(ccrz.cc_CallContext.storefront);
            //mid = btSettings.ClientGUID__c; // merchant ID
            CCAviCardConnectSettings__c cardSettings = CCAviCardConnectSettings__c.getInstance(thePayment.ccrz__Storefront__c);
            mid = cardSettings.Merchant_Id__c;
            payment.MID                         = mid; // '123';
        }

        // If it's invoice

        return payment;
    }

    public static Map<Id, List<CC_FP_Required_Order_Item__c>> findRequiredOrderItems(Set<Id> theIds) {
        Map<Id, List<CC_FP_Required_Order_Item__c>> requiredItemMap = new Map<Id, List<CC_FP_Required_Order_Item__c>>();
        
        if (theIds != null && !theIds.isEmpty()) {
            List<CC_FP_Required_Order_Item__c> requiredOrderItems = CCPDCRequiredOrderItemDAO.getRequiredOrderItems(theIds);
            if (requiredOrderItems != null && !requiredOrderItems.isEmpty()) {
                for (CC_FP_Required_Order_Item__c item : requiredOrderItems) {
                    List<CC_FP_Required_Order_Item__c> requiredItemList = requiredItemMap.get(item.CC_Parent_Order_Item__c);
                    if (requiredItemList == null) {
                        requiredItemList = new List<CC_FP_Required_Order_Item__c>();
                        requiredItemMap.put(item.CC_Parent_Order_Item__c, requiredItemList);
                    }
                    requiredItemList.add(item);
                }
            }
        }
        return requiredItemMap;

    }

    public static DateTime convertToDateTime(Date theDate) {
        DateTime t = null; 
        if (theDate != null) {
            t = theDate;
            t = t.addHours(t.hour());
        }
        return t;
    }

}