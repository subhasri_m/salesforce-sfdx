/*
Author: Surabhi Agrawal, Cloudaction
Name: Ca Update Wrong Owner on SOS and Sales target Class Ca_UpdateSOSOwner- 
Purpose: Gets all the SOS records with wrong owner or null Sales target and corrects it. 
Ca_BatchUpdateSOSOwner- Schedules this class which can be change through Schedule Jobs->
Manage Test class -- Ca_TestBatchUpdateSOSOwner and Ca_TestUpdateSOSOwner 
Formula - Calculates the woring Owner SOS Owner Salesman Number




*/







global class Ca_UpdateSOSOwner implements
Database.Batchable<sObject>, Database.Stateful {

         

        // instance member to retain state across transactions

        global Integer recordsProcessed = 0;
        id informaticaUserId2;
global Database.QueryLocator start(Database.BatchableContext bc) {
    
    // exclude the integration records
        String informaticaUserId = Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
          informaticaUserId2 =Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
            return Database.getQueryLocator(
            
            'SELECT id,name, Account_Owner_Name__c,Sales_Target__c, Account_Salesman_Number__c,SOS_Owner_Salesman_Number__c,Ca_Owner_Name__c, Account_Owner_ID__c,CreatedDate,Order_Date__c,OwnerId,Salesman_Number__c FROM Sales_Order_Summary__c where ((SOS_Owner_Salesman_Number__c=\'Wrong Owner\' or Sales_Target__c= null)    and  (Order_Date__c = THIS_YEAR or Order_Date__c = LAST_YEAR  ))' 

             
            );

        }

     
        global void execute(Database.BatchableContext bc, List<Sales_Order_Summary__c> scope){
            //SalesOrderSummaryTriggerHandler.runOnce = true;

            // process each batch of records
            Set<Id> ownerIdSet = new Set<Id>();
             list<Sales_Order_Summary__c> updatesosOwnerList = new list<Sales_Order_Summary__c>() ;
             List<Sales_Order_Summary__c> sosListAffected = new List<Sales_Order_Summary__c>();
            List<Sales_Order_Summary__c> sosList = new List<Sales_Order_Summary__c>();
             // exclude the integration records
             String informaticaUserId = Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
        
             //Get all the Users in a Map with thri Saleman Number
               Map<string,id> User_Map = new Map<string,id>();
               list<user> user_List= [Select id,Name, Salesman_Number__c from user where Salesman_Number__c != NULL and  IsActive= TRUE    ];
               string UserId;  
            // System.debug('******- Getting User LIST. 14');     
    
            for(User u :user_List)  {
                   User_Map.PUT(u.Salesman_Number__c, u.id);
                }

           

            for (Sales_Order_Summary__c sos  : scope) {
                
              if(sos.Account_Owner_Name__c=='Informatica User'){
                    sos.OwnerId= informaticaUserId2;
                    sos.Salesman_Number__c=sos.Account_Salesman_Number__c;
                    sos.Sales_Target__c=null;
                     updatesosOwnerList.add(sos);
                    
                    
                } 
             if(sos.Account_Owner_Name__c!='Informatica User') {
                if(User_Map.get(sos.Account_Salesman_Number__c)!= null){         
                   sos.OwnerId=User_Map.get(sos.Account_Salesman_Number__c); 
                   
                   updatesosOwnerList.add(sos);
                   ownerIdSet.add(sos.Account_Owner_ID__c);
                    sosListAffected.add(sos);
                 }
                }
     
            }     

       //To update the wrong owner on the SOS
        if(updatesosOwnerList.size()>0){
            // don't run trigger on this update
            //Ca_SalesOrderSummary_owner_update.runOnce = true;
          update updatesosOwnerList;
        }
        
        
           //To update the Sales Target 
          // if (ownerIdSet.size() > 0) {
            SalesOrderSummaryTriggerHandler.moveSos(ownerIdSet, sosListAffected);
    

       // }
  // }// end of for
 }            

     
        global void finish(Database.BatchableContext bc){

            System.debug(recordsProcessed + ' records processed. Shazam!');

            AsyncApexJob job = [SELECT Id, Status, NumberOfErrors,

                JobItemsProcessed,

                TotalJobItems, CreatedBy.Email

                FROM AsyncApexJob

                WHERE Id = :bc.getJobId()];

            // call some utility to send email

            //EmailUtils.sendMessage(a, recordsProcessed);

        }   

     

    }