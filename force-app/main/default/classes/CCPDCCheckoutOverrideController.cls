global with sharing class CCPDCCheckoutOverrideController {
    public String emptyVar {get;set;}

    @RemoteAction
    global static ccrz.cc_RemoteActionResult checkoutPreprocess(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        ccrz__E_Cart__c cart;

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            String cartId = ccrz.cc_CallContext.currCartId;

            cart = CCPDCCartDAO.getCartExtra(cartId);
            if(String.valueOf(cart.ccrz__Contact__c) != String.valueOf(ccrz.cc_CallContext.currContact.Id)) {
           	 	cart.ccrz__Contact__c = ccrz.cc_CallContext.currContact.Id;
            }

            String accountId = ccrz.cc_CallContext.effAccountId;

            Account theAccount = CCPDCAccountDAO.getAccount(accountId);
            CCPDCFulfillmentHelper.setTaxRate(cart, theAccount);

            Boolean reserveInventoryFlag = false;
            Boolean recheckAvailabilityFlag = false;
            // cart.Reservation_Id__c = null; // reset reservation id - MT 1/3/18: this now happens in checkInventory based on the 'cancelReservation' flag
            cart.CC_FP_Split_Response__c = null; // reset any split
            CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse  fulFilResponse;
            try{
              fulFilResponse = CCPDCInventoryHelper.checkInventory(
                cart,
                cart.ccrz__E_CartItems__r,
                reserveInventoryFlag,
                recheckAvailabilityFlag,
                true
            );  
            }catch (Exception e){
              system.debug(' OMS Exception');  
            }
                        
            
            Map<String, Object> extraData = CCPDCInventoryHelper.createExtraData(cart);
            extraData.put('cartItemList', cart.ccrz__E_CartItems__r);
            // if WillCall and there are BO, redirect to cart 
            if(fulFilResponse != null && fulFilResponse.backOrderLines != null && fulFilResponse.backOrderLines.size() > 0){
                if(cart != null && cart.CC_FP_Is_Ship_Remainder__c != null && cart.CC_FP_Is_Will_Call__c) {
                    Boolean CCFPIsShipRemainder =  cart.CC_FP_Is_Ship_Remainder__c;
                    Boolean CCFPIsWillCall =  cart.CC_FP_Is_Will_Call__c;                    
                    Boolean isTrueWillCall = CCFPIsWillCall && !CCFPIsShipRemainder;
                    if(isTrueWillCall) {
                        extraData.put('isWCError', true); 
                    }                        
                }
            }            
            response.data = extraData;

            response.success = (!cart.CC_FP_Has_Validation_Message__c);
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
            cart.CC_FP_Validation_Message__c = e.getMessage() + ' => ' + e.getStackTraceString();
        }
        finally {
            if (cart != null) {
                update cart;
                if (cart.ccrz__E_CartItems__r != null) {
                    update cart.ccrz__E_CartItems__r;
                }
                CCPDCFulfillmentHelper.updateTax(cart.ccrz__EncryptedId__c);
            }
        }
        return response;
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult checkoutPreprocessFP(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        ccrz__E_Cart__c cart;

        try {
             String cartId = ccrz.cc_CallContext.currCartId;
            cart = CCPDCCartDAO.getCartExtra(cartId);
            if(String.valueOf(cart.ccrz__Contact__c) != String.valueOf(ccrz.cc_CallContext.currContact.Id)) {
           	 	cart.ccrz__Contact__c = ccrz.cc_CallContext.currContact.Id;
            }     
            CCFPFulFillMentHelper.doSPlitsFP(cart);    
            response.success = true;
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
            cart.CC_FP_Validation_Message__c = e.getMessage() + ' => ' + e.getStackTraceString();
        }
        finally {
            if (cart != null) {                  
                update cart;
                if (cart.ccrz__E_CartItems__r != null) {
                    update cart.ccrz__E_CartItems__r;
                }
            }
        }
        return response;
    }
     @RemoteAction
    global static ccrz.cc_RemoteActionResult getSalesTaxAmount(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        ccrz__E_Cart__c cart;
        
        try {
           
            String cartId = ccrz.cc_CallContext.currCartId;
            cart = CCPDCCartDAO.getCartExtra(cartId);
            if(String.valueOf(cart.ccrz__Contact__c) != String.valueOf(ccrz.cc_CallContext.currContact.Id)) {
           	 	cart.ccrz__Contact__c = ccrz.cc_CallContext.currContact.Id;
            }                
            String accountId = ccrz.cc_CallContext.effAccountId;
           	Account theAccount = CCPDCAccountDAO.getAccount(accountId);
            CCFPFulFillMentHelper.getTaxAmountForGroup(cart, theAccount);
            Map<String, Object> extraData = CCPDCInventoryHelper.createExtraData(cart);
            extraData.put('cartItemList', cart.ccrz__E_CartItems__r);
            response.data = extraData;
            response.success = (!cart.CC_FP_Has_Validation_Message__c);
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
            cart.CC_FP_Validation_Message__c = e.getMessage() + ' => ' + e.getStackTraceString();
        }
        finally {
            if (cart != null) {                  
                update cart;
                if (cart.ccrz__E_CartItems__r != null) {
                    update cart.ccrz__E_CartItems__r;
                }
            }
        }
        return response;
        
    }
}