public with sharing class DashboardGrowthController2 {

    public Map<Integer,gaugeData> data {get;set;}
    
    public Map<Integer,gaugeData> dataMap {get;set;}
    
    public String region,userId;
    public String roleName;
    public String roleLevel {get;set;}
    public String DirLeadDashBoard{get;set;}
    public String TSMLeadDashBoard{get;set;}
    public String OSRLeadDashBoard{get;set;}
    
    // Sales_Target__c score {get;set;}
    // double NPS_score{get;set;}
    public DashboardGrowthController2(){
        String informaticaUserId = Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
        
        //List of report used for the links
        List<Report> reportList = new List<Report>();            
        
        //Dynamic Link for leaderboard Dashboards
        List<String> dashboardNames = new List<String>{'Sales_Director_Leaderboard', 'TSM_Leaderboard', 'OSR_Leaderboard'};
        List<Dashboard> dashboardList = [SELECT Id, DeveloperName FROM Dashboard WHERE DeveloperName IN: dashboardNames];
        Map<String,Id> dashboardMap = new Map<String,Id>();
        
        for(Dashboard d: dashboardList){
            dashboardMap.put(d.DeveloperName, d.Id);
        }
        
        DirLeadDashBoard = '/' +dashboardMap.get('Sales_Director_Leaderboard');
        TSMLeadDashBoard = '/' +dashboardMap.get('TSM_Leaderboard');
        OSRLeadDashBoard = '/' +dashboardMap.get('OSR_Leaderboard');
        
        
        //User for dynamic showing of leaderboards YTD Drill down reports
        Id systemAdminProfileId = [Select Id, Name FROM Profile WHERE Name = 'System Administrator'][0].id;
        User u = [SELECT Id, Region__c, UserRoleId, ProfileId  FROM User WHERE Id = :UserInfo.getUserId()];
        UserRole us = [SELECT Id, DeveloperName, Name FROM UserRole WHERE Id = :u.UserRoleId LIMIT 1];
        region = u.Region__c;
        roleName = us.Name;
        userId=u.id;
        //User for dynamic showing of leaderboards YTD Drill down reports
        if(u.ProfileId == systemAdminProfileId){
            roleLevel = 'Admin';
        }
        
        else if(roleName.containsIgnoreCase('Director')){
            roleLevel = 'Director';
        }
        
        else if(roleName.containsIgnoreCase('Manage')){
            roleLevel = 'TSM';
        }
        
        else if(roleName.containsIgnoreCase('Rep')){
            roleLevel = 'OSR';
        }
        
        region = u.Region__c;
    
        Integer i=0;
        dataMap = new Map<Integer,gaugeData>();
        
        //Dynamic Link for the Reports MTD Chart
        List<String> reportDevNames = new List<String>{'Growth_Chart_MTD_Sales_New', 'Growth_Chart_QTD_Sales_New', 'Growth_Chart_YTD_Sales_New',
                                                       'Growth_Chart_MTD_GP_New', 'Growth_Chart_QTD_GP_New', 'Growth_Chart_YTD_GP_New'};
            
        reportList = [SELECT Id, DeveloperName FROM Report WHERE DeveloperName IN : reportDevNames];
        Map<String, Id> reportMap = new Map<String, Id>();
        for(Report reportRec : reportList) {
            reportMap.put(reportRec.DeveloperName, reportRec.Id);
        }
        
        
        ////////////////// Month
        // for MTD Chart
       // Period pd = CommonUtil.getFiscalYearPeriod(Date.today(), 'Month'); //Today-1 , changes- Surabhi edited 2/1/2017
       Period pd = CommonUtil.getFiscalYearPeriod(Date.today().addDays(-1), 'Month'); // COde to show Yesterday's Dashboard 
       //Period pd = CommonUtil.getFiscalYearPeriod(Date.today().addDays(-10), 'Month'); // code for testing 
        Date startDate = pd.startDate; 
        Date endDate = pd.endDate;
        List<User> teamUserList = CommonUtil.getUserSetInOneTeamUnderLeader(UserInfo.getUserId());

        
                //Surabhi Agrawal @Cloudaction 10/11/2016
        //List<Sales_Target__c> groupedResultsThis1= [SELECT Sales_Sum__c, Gross_Profit_Sum__c,YTD_Sales__c,QTD_Sales__c , Sales_Target_Growth_ThisMonth__c, GP_Target_Growth_ThisMonth__c, Sales_Target_Growth_Total_Month_Target__c, GP_Target_Growth_Total_Month_Target__c  FROM Sales_Target__c where ownerid != :informaticaUserId and ownerid in:teamUserList  and Start_Date__c >= :startDate and Start_Date__c <= :endDate];
          //Surabhi Agrawal @cloudaction 12/7/2016 -- Target for TSM and SD, Owner = $user.id, not Team 
               List<Sales_Target__c> groupedResultsThis1= [SELECT Sales_Sum__c, Gross_Profit_Sum__c,YTD_Sales__c,QTD_Sales__c , Sales_Target_Growth_ThisMonth__c, GP_Target_Growth_ThisMonth__c, Sales_Target_Growth_Total_Month_Target__c, GP_Target_Growth_Total_Month_Target__c  FROM Sales_Target__c where ownerid != :informaticaUserId and ownerid =: UserInfo.getUserId()  and Start_Date__c >= :startDate and Start_Date__c <= :endDate];
 
        system.debug('********** Value of  groupedResultsThis1'+groupedResultsThis1);
        Decimal sumTotalSales = 0;
        Decimal sumTotalGP = 0;
        
        Decimal sumTargetSales = 0;
        Decimal sumTargetGP = 0;
        
        Decimal sumTargetSalesMonthTotal = 0;
        Decimal sumTargetGPMonthTotal = 0;


        for(Sales_Target__c ar1 : groupedResultsThis1 ){
         sumTotalSales= ar1.Sales_Sum__c;
         system.debug('********sumTotalSales'+sumTotalSales+'******ar1.Sales_Sum__c'+ ar1.Sales_Sum__c );
         
         sumTotalGP=ar1.Gross_Profit_Sum__c;
         
         sumTargetSales=ar1.Sales_Target_Growth_ThisMonth__c;
         sumTargetGP=ar1.GP_Target_Growth_ThisMonth__c;
          
         sumTargetSalesMonthTotal= ar1.Sales_Target_Growth_Total_Month_Target__c;
         sumTargetGPMonthTotal=ar1.GP_Target_Growth_Total_Month_Target__c;
        
        
        }
        
        //Surabhi Edited 10/16
        
        GaugeData MTDMonth = new GaugeData('Sales Month', 'salesmonth', sumTotalSales, sumTargetSales, '/' + reportMap.get('Growth_Chart_MTD_Sales_New'));
        //MTDMonth.wholePeriodTotal = String.ValueOf(sumTargetSalesMonthTotal.round(roundingMode.HALF_UP).format());
        MTDMonth.wholePeriodTotal = String.ValueOf(sumTargetSalesMonthTotal.round(roundingMode.HALF_UP).format());
        dataMap.put(0, MTDMonth);
        
        // for GP MTD Chart
        GaugeData GPMTDMonth = new GaugeData('GP Month', 'gpmonth', sumTotalGP, sumTargetGP, '/' + reportMap.get('Growth_Chart_MTD_GP_New'));
       // GPMTDMonth.wholePeriodTotal = String.ValueOf(sumTargetGPMonthTotal.round(roundingMode.HALF_UP).format());
       GPMTDMonth.wholePeriodTotal=String.ValueOf(sumTargetGPMonthTotal.round(roundingMode.HALF_UP).format());
        dataMap.put(1, GPMTDMonth);
        
        
        ////////////// Quarter
       // pd = CommonUtil.getFiscalYearPeriod(Date.today(), 'Month');//Today-1 , changes- Surabhi edited 2/1/2017
        pd = CommonUtil.getFiscalYearPeriod(Date.today().addDays(-1), 'Month'); // COde to show Yesterday's Dashboard 
       // pd = CommonUtil.getFiscalYearPeriod(Date.today().addDays(-10), 'Month'); // code for testing   
        startDate = pd.startDate; 
        endDate = pd.endDate;

                  //Surabhi Agrawal @Cloudaction 10/14/2016
        List<Sales_Target__c> groupedResultsThis2= [SELECT Sales_Sum__c,GP_QTD__c, Sales_Target_Growth_ThisQuarter__c,GP_Target_Growth_ThisQuarter__c,Sales_Target_Growth_Total_Quarter_Target__c,GP_Target_Growth_Total_Quarter_Target__c ,  Gross_Profit_Sum__c,YTD_Sales__c,QTD_Sales__c , Sales_Target_Growth_ThisMonth__c, GP_Target_Growth_ThisMonth__c, Sales_Target_Growth_Total_Month_Target__c, GP_Target_Growth_Total_Month_Target__c  FROM Sales_Target__c where ownerid != :informaticaUserId and ownerid =: UserInfo.getUserId()  and Start_Date__c >= :startDate and Start_Date__c <= :endDate];
        system.debug('********** Value of  groupedResultsThis2'+'Size= '+groupedResultsThis2.size()+groupedResultsThis2 );
        Decimal sumTotalSales2 = 0;
        Decimal sumTotalGP2 = 0;
        
        Decimal sumTargetSales2 = 0;
        Decimal sumTargetGP2 = 0;
        
        Decimal sumTargetSalesQuarterTotal = 0;
        Decimal sumTargetGPQuarterTotal = 0;


        for(Sales_Target__c ar2 : groupedResultsThis2 ){
         //edited-10/28---- Flip of Qts and YTD
         //sumTotalSales= ar.QTD_Sales__c;
         //sumTotalSales2= ar2.YTD_Sales__c;
         sumTotalSales2= ar2.QTD_Sales__c;
         system.debug('********sumTotalSales'+sumTotalSales+'******ar2.Sales_Sum__c'+ ar2.Sales_Sum__c );
         
         sumTotalGP2=ar2.GP_QTD__c;
         
         sumTargetSales2=ar2.Sales_Target_Growth_ThisQuarter__c;
         sumTargetGP2=ar2.GP_Target_Growth_ThisQuarter__c;
          
         sumTargetSalesQuarterTotal= ar2.Sales_Target_Growth_Total_Quarter_Target__c;
         sumTargetGPQuarterTotal=ar2.GP_Target_Growth_Total_Quarter_Target__c;
        
        
        }
        
        // for QTD Chart
        GaugeData QTDQuarter = new GaugeData('Sales Quarter', 'salesqtr', sumTotalSales2, sumTargetSales2, '/' + reportMap.get('Growth_Chart_QTD_Sales_New'));
        QTDQuarter.wholePeriodTotal = String.ValueOf(sumTargetSalesQuarterTotal.round(roundingMode.HALF_UP).format());
        dataMap.put(2, QTDQuarter);
        
        // for GP QTD Chart
        GaugeData GPQTDQuarter = new GaugeData('GP Quarter', 'gpqtr', sumTotalGP2, sumTargetGP2, '/' + reportMap.get('Growth_Chart_QTD_GP_New'));
        GPQTDQuarter.wholePeriodTotal = String.ValueOf(sumTargetGPQuarterTotal.round(roundingMode.HALF_UP).format());
        dataMap.put(3, GPQTDQuarter);
        
        
        //////////////////////////// Year
        //pd = CommonUtil.getFiscalYearPeriod(Date.today(), 'Month'); //Today-1 , changes- Surabhi edited 2/1/2017
        pd = CommonUtil.getFiscalYearPeriod(Date.today().addDays(-1), 'Month'); // COde to show Yesterday's Dashboard 
       // pd = CommonUtil.getFiscalYearPeriod(Date.today().addDays(-10), 'Month'); // code for testing   
   
          //pd = CommonUtil.getFiscalYearPeriod(Date.today().addDays(-1), 'Month');
        startDate = pd.startDate; 
        endDate = pd.endDate;
        system.debug('Start Date and End Date '+startDate+'End Date'+endDate);
      
       //10/25/2016
        List<Sales_Target__c> groupedResultsThis3= [SELECT Sales_Sum__c,GP_QTD__c, Gross_Profit_Sum__c,YTD_Sales__c,QTD_Sales__c ,Sales_Target_Growth_Total_Year_Target__c ,  GP_YTD__c, GP_Target_Growth_Total_Quarter_Target__c,GP_Target_Growth_ThisYear__c,GP_Target_Growth_Total_Year_Target__c,Sales_Target_Growth_ThisYear__c  FROM Sales_Target__c where ownerid != :informaticaUserId and ownerid =: UserInfo.getUserId() and Start_Date__c >= :startDate and Start_Date__c <= :endDate];
        system.debug('********** Value of  groupedResultsThis3'+groupedResultsThis3);
        //list<> groupedResultsThis = [SELECT SUM(Sales_Sum__c), SUM(Gross_Profit_Sum__c), SUM(Sales_Target_Growth_ThisYear__c), SUM(GP_Target_Growth_ThisYear__c), SUM(Sales_Target_Growth_Total_Year_Target__c), SUM(GP_Target_Growth_Total_Year_Target__c) FROM Sales_Target__c where ownerid != :informaticaUserId and ownerid in:teamUserList and Start_Date__c >= :startDate and Start_Date__c <= :endDate];
        
        sumTotalSales = 0;
        sumTotalGP = 0;
        
        sumTargetSales = 0;
        sumTargetGP = 0;
        
        Decimal sumTargetSalesYearTotal = 0;
        Decimal sumTargetGPYearTotal = 0;
        
        for (Sales_Target__c  ar3 : groupedResultsThis3) {
          
          //Edit 10/28- Flip of YTD TO QTD  
         sumTotalSales= ar3.YTD_Sales__c;
        // sumTotalSales= ar3.QTD_Sales__c;
         system.debug('********sumTotalSales'+sumTotalSales+'******ar3.Sales_Sum__c'+ ar3.Sales_Sum__c );
         
         sumTotalGP=ar3.GP_YTD__c;
         
         sumTargetSales=ar3.Sales_Target_Growth_ThisYear__c;
         sumTargetGP=ar3.GP_Target_Growth_ThisYear__c;
          
         sumTargetSalesYearTotal= ar3.Sales_Target_Growth_Total_Year_Target__c;
         sumTargetGPYearTotal=ar3.GP_Target_Growth_Total_Year_Target__c;
        
          
        }
        
        // for YTD Chart
        GaugeData YTDYear = new GaugeData('Sales Year', 'salesyear', sumTotalSales, sumTargetSales, '/' + reportMap.get('Growth_Chart_YTD_Sales_New'));
        YTDYear.wholePeriodTotal = String.ValueOf(sumTargetSalesYearTotal.round(roundingMode.HALF_UP).format());
        dataMap.put(4, YTDYear);
        
        // for GP YTD Chart
        GaugeData GPYTDYear = new GaugeData('GP Year', 'gpyear', sumTotalGP, sumTargetGP, '/' + reportMap.get('Growth_Chart_YTD_GP_New'));
        GPYTDYear.wholePeriodTotal = String.ValueOf(sumTargetGPYearTotal.round(roundingMode.HALF_UP).format());
        dataMap.put(5, GPYTDYear);
        
    }
    
    public void loadData(){
    
    }
    
    public class gaugeData {
        public String name { get; set; }
        public String div { get; set; }
        public decimal score { get; set; }
        public decimal pct { get; set; }
        public decimal deg { get; set; }
        public decimal max { get; set; }
        public decimal pctRound {get; set;}
        public String wholePeriodTotal {get; set;}
        
        public String maxFormat{get; set;}
        public String scoreFormat {get; set;}
        public String link {get;set;}

        public gaugeData(String name, String div, decimal scre, decimal max, String link) {
            // if(max == 0) {
            //     max= 1;
            // } 
            this.link = link;
            //this.name = name;
            this.name = '';
            this.score = scre;
            this.div = div;
            this.max  = max;
            deg = 90;
            if (max == 0) {
                pct = 0;
                deg = 180;
            } else {
                pct = (scre / max ) * 100 ;
                deg = pct * 180 / 120;
            }
            
            if(pct > 120){deg = 180; }
            this.pctRound = pct.intValue();
            // this.maxFormat = max.setScale(2).format();
            // this.scoreFormat = score.setscale(2).format();
            this.maxFormat = String.ValueOf(max.round(roundingMode.HALF_UP).format());
           // system.debug('**********Value of score.round(roundingMode.HALF_UP'+ score.round(roundingMode.HALF_UP));
            this.scoreFormat = String.ValueOf(score.round(roundingMode.HALF_UP).format());
        }
    }
    
}