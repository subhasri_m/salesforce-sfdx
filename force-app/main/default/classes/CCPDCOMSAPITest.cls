@isTest
public class CCPDCOMSAPITest {

    public static final String REQUEST_STRING = '{"access_token":"68355f47-bc45-4f62-b303-827443c4fd6d","token_type":"bearer","expires_in":7200,"scope":"all refreshToken","refresh_token":"739033fa74b1a419173876b5e623381"}';

    static testmethod void executeRequestsTest() {
        createOMSSettings('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new OMSServiceMock(200, 'OK', REQUEST_STRING));

        CCPDCOMSAPI api = null;

        Test.startTest();
        api = new CCPDCOMSAPI('DefaultStore');
        api.executeRequests();        
        Test.stopTest();

        CC_PDC_OMS_Settings__c settings = api.omsSettings;
        System.assert(api != null);
        System.assert(api.omsSettings != null);
        System.assertEquals('68355f47-bc45-4f62-b303-827443c4fd6d', settings.Access_Token__c);
    }

    static testmethod void accessTokenTest() {
        CCPDCOMSAPI.AccessToken accessTokenObj = (CCPDCOMSAPI.AccessToken)JSON.deserialize(REQUEST_STRING, CCPDCOMSAPI.AccessToken.class);
        System.assert(accessTokenObj != null);
        System.assertEquals('68355f47-bc45-4f62-b303-827443c4fd6d', accessTokenObj.access_token);
        System.assertEquals('bearer', accessTokenObj.token_type);
        System.assertEquals(7200, accessTokenObj.expires_in);
        System.assertEquals('all refreshToken', accessTokenObj.scope);
        System.assertEquals('739033fa74b1a419173876b5e623381', accessTokenObj.refresh_token);
    }

    public static void createOMSSettings(String storefront) {
        CC_PDC_OMS_Settings__c settings = new CC_PDC_OMS_Settings__c();
        settings.Name = storefront;
        settings.Access_Token__c = null;
        settings.Auth_URL__c = 'https://fleetpride.enspirecommerce.com/api/v1/security/access/token';
        settings.Expiry_Date__c = DateTime.now().addDays(1);
        settings.Expiry_Threshold__c = 120;
        settings.Cancel_Fulfillment_Request_URL__c = 'https://fleetpride.enspirecommerce.com/api/v1/inventory/cancelReservation';
        settings.Fulfillment_Plan_URL__c = 'https://fleetpride.enspirecommerce.com/api/v1/inventory/getOptimizedFulfillmentPlan';
        settings.Nearest_DC_URL__c = 'https://fleetpride.enspirecommerce.com/api/v1/site/nearest';
        settings.Password__c = 'testpwd';
        settings.Username__c = 'test@pdc.com';
        insert settings;
    }

    public static void createOMSSettingsWithToken(String storefront) {
        CC_PDC_OMS_Settings__c settings = new CC_PDC_OMS_Settings__c();
        settings.Name = storefront;
        settings.Access_Token__c = '68355f47-bc45-4f62-b303-827443c4fd6d';
        settings.Auth_URL__c = 'https://fleetpride.enspirecommerce.com/api/v1/security/access/token';
        settings.Expiry_Date__c = DateTime.now().addDays(1);
        settings.Expiry_Threshold__c = 120;
        settings.Cancel_Fulfillment_Request_URL__c = 'https://fleetpride.enspirecommerce.com/api/v1/inventory/cancelReservation';
        settings.Fulfillment_Plan_URL__c = 'https://fleetpride.enspirecommerce.com/api/v1/inventory/getOptimizedFulfillmentPlan';
        settings.Nearest_DC_URL__c = 'https://fleetpride.enspirecommerce.com/api/v1/site/nearest';
        settings.Password__c = 'testpwd';
        settings.Username__c = 'test@pdc.com';
        insert settings;
    }

    public class OMSServiceMock implements HttpCalloutMock {

        public Integer code {get; set;}
        public String status {get; set;}
        public List<String> bodys {get; set;}

        public OMSServiceMock(Integer code, String status, String body) {
            this.code = code;
            this.status = status;
            this.bodys = new List<String>{body};
        }

        public OMSServiceMock(Integer code, String status, List<String> bodys) {
            this.code = code;
            this.status = status;
            this.bodys = bodys;
        }

        public HTTPResponse respond(HTTPRequest request) {
            HttpResponse response = new HttpResponse();
            if (this.bodys != null && !this.bodys.isEmpty()) {
                response.setBody(this.bodys[0]);
                this.bodys.remove(0);
            }
            response.setStatusCode(this.code);
            response.setStatus(this.status);
            return response;
        }
    }

}