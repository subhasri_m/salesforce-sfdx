public without sharing class CCPDCExceptionService{
    
    public static void sendExceptionEmail(String ClassName, String methodName, String exceptionString){
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();   
         OrgWideEmailAddress[] owea = [select Id, Address from OrgWideEmailAddress 
                                                   where Address = 'help@fleetpride.com'];
        OrgWideEmailAddress[] owe = [select Id, Address from OrgWideEmailAddress 
                                       where Address = 'pdcsales@e-pdc.com'];
        Organization org = [Select Id, IsSandbox From Organization LIMIT 1];
        if (org.isSandbox && owea.size() > 0 ) {
            message.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        if ( !org.isSandbox && owe.size() > 0 ) {
            message.setOrgWideEmailAddressId(owe.get(0).Id);
        }
        message.toAddresses = new List<String>{'amit.singh1@puresoftware.com','divya.goel@puresoftware.com'};
        message.optOutPolicy = 'FILTER';
        message.subject = 'The method '+ methodName +'of Class '+ClassName+' Throwing an Exception';
        String emailBody = '<p><font face="verdana">';
        emailBody+= 'Hello Team,<br /><br/>';
        emailBody+= 'The method '+ methodName +'of Class '+ClassName+' Throwing an Exception. Exception is <br /><br/>';
        emailBody+= exceptionString;
        message.htmlBody = emailBody;
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        ccrz.ccLog.log('#### results ', results);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }

}