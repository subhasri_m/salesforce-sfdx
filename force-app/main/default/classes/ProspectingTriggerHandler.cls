public class ProspectingTriggerHandler {

    public static boolean isFirstBeforeInsert = true;
    public static boolean isFirstAfterInsert = true;
    
    public static void onBeforeInsert(List<Prospecting__c> prosList){
        if(ProspectingTriggerHandler.isFirstBeforeInsert){
            ProspectingTriggerHandler.isFirstBeforeInsert = false;
            updateOwner(prosList);
        }
    }
    
    public static void onAfterInsert(List<Prospecting__c> prosList){
        if(ProspectingTriggerHandler.isFirstAfterInsert){
            ProspectingTriggerHandler.isFirstAfterInsert = false;
            updateLatest(prosList);
        }
    }
    
    public static void updateOwner(List<Prospecting__c> prosList){
        System.debug('* ** *** updateOwner');
        //Set of Sales Numbers
        Set<String> salesNum = new Set<String>();
        
        //Map of unique Sales Numbers to their User
        Map<String,Id> userMap = new Map<String,Id>();        
        
        
        //Populating SalesNum Set of Ids with Prospecting Custom Id field
        for(Prospecting__c p: prosList){
            //if(p.User__c == null){
                System.debug('* ** *** salesNum ' + p.Id + ' "' + p.Id__c + '"');
                salesNum.add(p.Id__c);
            //}
        }
        
        //Query for all users that have the same Salesman Number   
        //loop throug the userList and populate the userMap with users SalesMan number and User, populate the userIds set
        for(User u : [SELECT Id,Name,Salesman_Number__c,Salesman_Right__c FROM User WHERE Salesman_Right__c IN: salesNum ]){
            System.debug('* ** *** salesman ' + u.Salesman_Right__c  + ' ' + u.Name + ' ' + u.Id);
            userMap.put(u.Salesman_Right__c, u.Id);
        }
        
        
        //Loop throught the Trigger New and Populate the Owner Id and the IsLatest Field
        for(Prospecting__c p: prosList){
            if(userMap.containsKey(p.Id__c)){
                //p.OwnerId = userMap.get(p.Id__c);
                p.User__c = userMap.get(p.Id__c);
                System.debug('* ** *** user set to ' + p.User__c + ' ' + p.Id__c + p.Id + ' ');
             }
        }
    }

    //Check or uncheck the latest field
    public static void updateLatest(List<Prospecting__c> prosList){
        
        //Prospecting records for update
        List<Prospecting__c> updateList = new List<Prospecting__c>();
        Set<Id> prosIds = new Set<Id>();
        
        //Store the combination of the User's salesman number, year, and month
        Set<String> userYearMonth = new Set<String>();
        Set<String> latestFound = new Set<String>();
        
        //Get the userYearMonth for every new Prospecting record
        for(Prospecting__c  p: [SELECT Id, UserYearMonth__c FROM  Prospecting__c  WHERE Id IN :prosList ]){
            userYearMonth.add(p.UserYearMonth__c );
            prosIds.add(p.Id);
        }
        
        //Get all the Prospecting records
        for(Prospecting__c p : [SELECT Id, Is_Latest__c, UserYearMonth__c  FROM Prospecting__c  WHERE UserYearMonth__c IN :userYearMonth ORDER BY Report_Date__c DESC, CreatedDate DESC]){
            System.debug('* ** *** ' + p.Id + ' ' + p.Is_Latest__c);
            //If the latest has been found, record is not the latest
            if(latestFound.contains(p.UserYearMonth__c)){
                if(p.Is_Latest__c || prosIds.contains(p.Id)){
                    System.debug('* ** *** ' + p.Id + ' not latest');
                    p.Is_Latest__c = false;
                    updateList.add(p);
                }
            }else{
                //Otherwise, set record as the latest
                if(!p.Is_Latest__c || prosIds.contains(p.Id)){
                    System.debug('* ** *** ' + p.Id + ' is latest');
                    p.Is_Latest__c = true;
                    updateList.add(p);
                }
                latestFound.add(p.UserYearMonth__c);
            }            
        }
        
        if(!updateList.isEmpty()){
            update updateList;
        }
        
    }
    



}