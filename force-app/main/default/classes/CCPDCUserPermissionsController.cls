public with sharing class CCPDCUserPermissionsController {
    public String jsonData {get; set;}

	public CCPDCUserPermissionsController() {
        jsonData = '{}';
        try {
            CCPDCUserPermissionsHelper.UserPermissions permissions = new CCPDCUserPermissionsHelper.UserPermissions();
              System.debug(System.LoggingLevel.ERROR, 'User Info type ' + UserInfo.getUserType());
            if (UserInfo.getUserType() != 'Guest') {
                ccrz.cc_CallContext.initRemoteContext(new ccrz.cc_RemoteActionContext());
                Id accountId = ccrz.cc_CallContext.effAccountId;
                Id contactId = ccrz.cc_CallContext.currContact.Id;
                permissions = CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);
                if (permissions != null) {
                    jsonData = JSON.serialize(permissions);
                }
            }
        }
        catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
        }
	}
}