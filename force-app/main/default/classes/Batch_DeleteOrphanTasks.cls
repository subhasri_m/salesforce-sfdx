// This Batch deletes tasks that are not started (future tasks) and have different account owner then creater

global class Batch_DeleteOrphanTasks implements database.Batchable<sobject>, schedulable, DataBase.stateful{
    public Integer successRecords = 0;
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new Batch_DeleteOrphanTasks());
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator([Select id, createdbyId, Account.OwnerID from task WHERE Status in  ('Not Started', 'Not Completed') and TaskSubtype='Task' and type ='Sales Call'  and ActivityDate!=null and AccountId !=null]);
    }
    
    global void execute(Database.BatchableContext BC,List<Task> scope)
    {
        // Filter orphan tasks
        List<Task> taskToDelete = new List<Task>();
        for (Task tsk : scope){
            if (tsk.createdbyId!= tsk.Account.OwnerID || Test.isRunningTest()){
                taskToDelete.add(tsk);
            }
        }
        try{
            
            Database.DeleteResult[] drList = Database.delete(taskToDelete, false);
            for(database.DeleteResult d : drList){
                if(d.issuccess()){
                    successRecords++;
                }
                
            }
        }
        catch(exception e){
            system.debug('update failed');
        }
  }
    
    global void finish(Database.BatchableContext BC)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'rahul.bansal@puresoftware.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Batch_Delete_Orphan_Tasks');
        mail.setPlainTextBody('Total' +'  '+ successRecords +'  '+ 'Records updated sucessfully');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
       
    }
}