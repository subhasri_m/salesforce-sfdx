@isTest
public class CCPDCHeaderControllerTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        Map<String,Map<String,Object>> testData = new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.SERVICE_SETTINGS => new Map<String,Object>{
                util.STOREFRONT => new Map<String,Object>{
                    'ccServiceCart' => 'c.CCPDCServiceCart'
                }
            }
        };
        ccrz.ccApiTestData.setupData(testData);

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testMethod void testGetCoveoSearchToken(){
        Test.StartTest();
        String token = CCPDCHeaderController.getCoveoSearchToken();
        System.AssertEquals(null, token);
        Test.StopTest();
    }

    static testmethod void getFreeShippingInfoShippingQtyTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        Location__c loc = util.getLocation();
        cart.CC_FP_Location__c = loc.Id;
        update cart;

        ccrz__E_CartItem__c ci = util.getCartItem();
        List<ccrz__E_CartItem__c> cis = [
            SELECT
                Id,
                CC_FP_QTY_Shipping__c
            FROM
                ccrz__E_CartItem__c
            WHERE
                Id =: ci.Id
        ];
        if(!cis.isEmpty()){
          ccrz__E_CartItem__c newci = cis.get(0);
          newci.CC_FP_QTY_Shipping__c = 5;
          update newci;
        }


        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;
       
        Test.startTest();
        result = CCPDCHeaderController.getFreeShippingInfo(ctx, ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void getFreeShippingInfoQtyTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        Location__c loc = util.getLocation();
        cart.CC_FP_Location__c = loc.Id;
        update cart;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCHeaderController.getFreeShippingInfo(ctx, ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }


    static testmethod void getFreeShippingInfoNoInventoryTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        Location__c loc = util.getLocation();
        cart.CC_FP_Location__c = loc.Id;
        update cart;

        ccrz__E_Product__c p = util.getProduct();
        List<ccrz__E_ProductInventoryItem__c> inv = [SELECT Id FROM ccrz__E_ProductInventoryItem__c WHERE ccrz__ProductItem__c = :p.Id];
        delete inv;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCHeaderController.getFreeShippingInfo(ctx, ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void getFreeShippingInfoNotEnoughInventoryTest() {
        util.initCallContext();

        ccrz__E_Cart__c cart = util.getCart();
        Location__c loc = util.getLocation();
        cart.CC_FP_Location__c = loc.Id;
        update cart;

        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.ccrz__Quantity__c = 11;
        update cartItem;

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCHeaderController.getFreeShippingInfo(ctx, ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }
    
    static testmethod void logSearchText() {
        util.initCallContext();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;
        String searchText = 'Test123';
        Test.startTest();
		result = CCPDCHeaderController.logSearchText(ctx, searchText);
		Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }
    static testmethod void getNearestFleetPrideBranchTest() {
        util.initCallContext();
        String RESPONSE_STRING = '[{"fid":"BSL","location_name":"FleetPride","address_1":"2467 State Road","address_2":"","city":"Bensalem","region":"PA","post_code":"19020","country":"US","lat":"40.0708575","lng":"-74.9396008","distance":"2.2","hours:primary":{"label":"Primary Hours","name":"primary","days":{"Sunday":"closed","Monday":[{"open":"07:30","close":"17:00"}],"Tuesday":[{"open":"07:30","close":"17:00"}],"Wednesday":[{"open":"07:30","close":"17:00"}],"Thursday":[{"open":"07:30","close":"17:00"}],"Friday":[{"open":"07:30","close":"17:00"}],"Saturday":"closed"}}}]';    

        Test.setMock(HttpCalloutMock.class, new RIOSEOMock(200, 'OK', RESPONSE_STRING));
        
		ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;
        String ipAddress = '10.38.1.1';
        Account acc  = util.getAccount();
        acc.CC_FP_Preferred_Ship_From_Location__c = 'BSL';
        update acc;
        Test.startTest();
		result = CCPDCHeaderController.getNearestFleetPrideBranch(ctx, ipAddress);
		Test.stopTest();
		System.assert(result != null);
    }

	static testmethod void getNearestFleetPrideBranchTest2() {
        util.initCallContext();
        String RESPONSE_STRING = '[{"fid":"BSL","location_name":"FleetPride","address_1":"2467 State Road","address_2":"","city":"Bensalem","region":"PA","post_code":"19020","country":"US","lat":"40.0708575","lng":"-74.9396008","distance":"2.2","hours:primary":{"label":"Primary Hours","name":"primary","days":{"Sunday":"closed","Monday":[{"open":"07:30","close":"17:00"}],"Tuesday":[{"open":"07:30","close":"17:00"}],"Wednesday":[{"open":"07:30","close":"17:00"}],"Thursday":[{"open":"07:30","close":"17:00"}],"Friday":[{"open":"07:30","close":"17:00"}],"Saturday":"closed"}}}]';    

        Test.setMock(HttpCalloutMock.class, new RIOSEOMock(200, 'OK', RESPONSE_STRING));
        
		ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;
        Account acc  = util.getAccount();
        acc.CC_FP_Preferred_Ship_From_Location__c = 'BSL';
        update acc;
        Location__c loc = util.getLocation();
        loc.Location__c = 'BSL';
        loc.ZipCode__c = '19020';
        update loc;
        String ipAddress = '10.38.1.1';
        Test.startTest();
        ccrz__E_Cart__c updatedCart = [SELECT  Id, ccrz__ShipTo__c FROM ccrz__E_Cart__c WHERE Id =: Util.getCart().Id ];
        ccrz.cc_CallContext.currCartId =updatedCart.Id;
		result = CCPDCHeaderController.getNearestFleetPrideBranch(ctx, ipAddress);
		Test.stopTest();
		System.assert(result != null);
    }
    
    static testmethod void getFleetPrideBranchesTest() {
        util.initCallContext();
		ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;
        String zipcode = '75247';
        Test.startTest();
		result = CCPDCHeaderController.getFleetPrideBranches(ctx, zipcode);
		Test.stopTest();
		System.assert(result != null);
    }
    static testmethod void constructorTest() {
        util.initCallContext();
        
		ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;
        String ipAddress = '10.38.1.1';
        Test.startTest();
        Account theAccount = util.getAccount();
        theAccount.National_Account_Group__c = 'testAccGrp';
        update theAccount;
        CCPDCHeaderController initiateCons = new CCPDCHeaderController();
        PageReference pageRef = Page.CCPDCHeaderBIE;
        Test.setCurrentPage(pageRef);
        // put the lead id as a parameter
        ApexPages.currentPage().getHeaders().put('User-Client-IP',ipAddress);
        String ipAddressRet = ApexPages.currentPage().getHeaders().get('User-Client-IP');
        Test.stopTest();
		System.assert(ipAddressRet != null);
    }
    
     static testmethod void getCurrentCartShippingValuesTest() {
         ccrz.cc_CallContext.storefront = util.STOREFRONT;
         Account a = util.getAccount();
		ccrz__E_Product__c p = util.getProduct();
         System.debug('product==>'+p);
        List<ccrz__E_ProductInventoryItem__c> inventory = [SELECT Id, ccrz__QtyAvailable__c,Location__r.Location_Status__c,Location__r.Location_Type__c 
                                                           FROM ccrz__E_ProductInventoryItem__c 
                                                           WHERE ccrz__ProductItem__c = :p.Id 
                                                           AND ccrz__InventoryLocationCode__c = :CCPDCTestUtil.DEFAULT_LOCATION_CODE];
		ccrz__E_ProductInventoryItem__c pii = inventory[0];
         pii.Location__r.Location_Status__c = 'Active';
         pii.Location__r.Location_Type__c ='DC';
         update pii;
         System.debug('pii==>'+ pii);
         
         util.initCallContext();
         Map<String,Decimal> result = new Map<String,Decimal>();
         ccrz__E_Cart__c cart = util.getCart();
         Test.startTest();
         result = CCPDCHeaderController.getCurrentCartShippingValues(cart.ccrz__EncryptedId__c, a.Id);
         Test.stopTest();
         System.assert(result != null);
     }
    
    static testmethod void processLocationDataTest() {
        util.initCallContext();
		ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String RESPONSE_STRING = '[{"fid":"BSL","location_name":"FleetPride","address_1":"2467 State Road","address_2":"","city":"Bensalem","region":"PA","post_code":"19020","country":"US","lat":"40.0708575","lng":"-74.9396008","distance":"2.2","hours:primary":{"label":"Primary Hours","name":"primary","days":{"Sunday":"closed","Monday":[{"open":"07:30","close":"17:00"}],"Tuesday":[{"open":"07:30","close":"17:00"}],"Wednesday":[{"open":"07:30","close":"17:00"}],"Thursday":[{"open":"07:30","close":"17:00"}],"Friday":[{"open":"07:30","close":"17:00"}],"Saturday":"closed"}}}]';    
		Test.setMock(HttpCalloutMock.class, new RIOSEOMock(200, 'OK', RESPONSE_STRING));
        CCFPLocationRioSeoAPI.Location  loc= null;
        List<CCFPLocationRioSeoAPI.Location> lstLoc = new List<CCFPLocationRioSeoAPI.Location>();
        Location__c location = util.createLocation('BSL');
        insert location;
        Test.startTest();
        loc = CCFPLocationRioSeoAPI.getNearestFleetPrideBranch('','19020','BSL', true);  
        lstLoc.add(loc);
        CCPDCHeaderController.processLocationData(lstLoc);
		Test.stopTest();
    }
    
    public class RIOSEOMock implements HttpCalloutMock {
        public Integer code {get; set;}
        public String status {get; set;}
        public List<String> bodys {get; set;}
        
        public RIOSEOMock(Integer code, String status, String body) {
            this.code = code;
            this.status = status;
            this.bodys = new List<String>{body};
                }
        
        public HTTPResponse respond(HTTPRequest request) {
            HttpResponse response = new HttpResponse();
            if (this.bodys != null && !this.bodys.isEmpty()) {
                response.setBody(this.bodys[0]);
                this.bodys.remove(0);
            }
            response.setStatusCode(this.code);
            response.setStatus(this.status);
            return response;
        }
    }
   static testmethod void getHeaderInfoFPTest() {
        util.initCallContext();
		ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
		result = CCPDCHeaderController.getHeaderInfoFP(ctx);
		Test.stopTest();
		System.assert(result != null);
    } 
    static testmethod void updateCartFPLocationTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        Location__c loc = util.getLocation();
        loc.Location__c = 'LXG';
        update loc;
        cart.CC_FP_Location__c = loc.Id;
        update cart;
        
        Account acc = util.getAccount();
        
        ccrz.cc_CallContext.effAccountId = acc.Id;
        ccrz.cc_CallContext.currCartId = cart.ccrz__EncryptedId__c;
        ccrz.cc_CallContext.currPageParameters.put('branchLoc','BSL');
        
		ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        Location__c loc2 = loc;
        loc2.Location__c = 'BSL';
        update loc2;
        
		result = CCPDCHeaderController.updateCartFPLocation(ctx);
		Test.stopTest();
		System.assert(result != null);
    }
    static testmethod void updateCartFPLocationTest2() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        Location__c loc = util.getLocation();
        loc.Location__c = 'LXG';
        update loc;
        //cart.CC_FP_Location__c == loc.ID;
        //update cart;
        Account acc = util.getAccount();
        acc.CC_FP_Preferred_Ship_From_Location__c = 'CC';
        update acc;
        
        ccrz.cc_CallContext.effAccountId = acc.Id;
        ccrz.cc_CallContext.currCartId = cart.ccrz__EncryptedId__c;
        ccrz.cc_CallContext.currPageParameters.put('branchLoc','LXG');
        
		ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
		result = CCPDCHeaderController.updateCartFPLocation(ctx);
		Test.stopTest();
		System.assert(result != null);
    }

	/*static testmethod void GetShipToInformationTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        
        Account acc = util.getAccount();
        acc.CC_FP_Preferred_Ship_From_Location__c = 'LXG';
        update acc;
        
        
        Test.startTest();
		CCPDCHeaderController.GetShipToInformation(cart.Id,acc.Id);
		Test.stopTest();
    })*/
}