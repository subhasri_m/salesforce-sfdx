@isTest
public class CCPDCProductCategoryDAOTest {
    static testmethod void getProductCategorySKUsTest() {        
		CCPDCTestUtil util = new CCPDCTestUtil();        
        Map<String, Object> m = util.initData();
        ccrz__E_Product__c p = (ccrz__E_Product__c)m.get('product');
        ccrz__E_Category__c category = (ccrz__E_Category__c) m.get('category');
        ccrz.cc_CallContext.storefront = util.STOREFRONT;

        List<ccrz__E_ProductCategory__c> items = null;
        Test.startTest();
        items = CCPDCProductCategoryDAO.getProductCategorySKUs(category.Id);
        Test.stopTest();

        System.assert(items != null);
    }	
		
}