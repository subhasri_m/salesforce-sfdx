/*
 Override cc_api_ProductQuantityRule class to return increment qty from Salespack field. 
*/
global class CCPDCProductQuantityRule extends ccrz.cc_api_ProductQuantityRule {
	global override Map<String,Map<String,Object>> getRules(List<String> skus) {
		Map<String,Map<String,Object>> outputData  = new Map<String,Map<String,Object>>();
		List<ccrz__E_Product__c> listProducts =  CCPDCProductDAO.getSalespackForSkus(skus);

		for(ccrz__E_Product__c product : listProducts) {
			Integer salespack;
			try {
                if(ccrz.cc_CallContext.storefront == Label.FP_StoreName) {
                    salespack = Integer.valueOf(product.FP_Salespack__c);
                } else {
					salespack = Integer.valueOf(product.Salespack__c);
                }
			}
			catch(Exception ex){

			}
			salespack = (salespack == null || salespack < 1)?1:salespack;

			Map<String,Object> rules = new Map<String,Object>();
			rules.put(ccrz.cc_api_ProductQuantityRule.PARM_INCREMENT, salespack);
			//rules.put(ccrz.cc_api_ProductQuantityRule.PARM_SKIP_INCREMENT, salespack);
			//rules.put(ccrz.cc_api_ProductQuantityRule.PARM_MISC_DETAILS, 'Quantity can be increased only in the increment of ' + salespack);

			outputData.put(product.ccrz__SKU__c, rules);


		}

		return outputData;
	}
}