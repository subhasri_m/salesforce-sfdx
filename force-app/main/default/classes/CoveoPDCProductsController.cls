global with sharing class CoveoPDCProductsController {

    public static CCAviBoomiAPI.SeriesPricingWSResponse price(List<Map<String,Object>> prodList) {
        return CCPDCPricingHelper.price(prodList);
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult isLocalDeliveryAllowed(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx);
        try{
            String accountId = (String) ccrz.cc_CallContext.currAccountId;
            Account acc = [SELECT Id, Local_Delivery_Allowed__c FROM Account WHERE Id =:accountId];
            res.success = true;
            // if (UserInfo.getUserType() == 'Guest') {
            //     res.data = CCPDCInventoryHelper.getFPInventoryByCart(skus, null, branchLoc);
            // } else {
            //     ccrz.ccLog.log(LoggingLevel.DEBUG,'P:ccrz.cc_CallContext.effAccountId', ccrz.cc_CallContext.effAccountId);
            //     res.data = CCPDCInventoryHelper.getFPInventoryByCart(skus, ccrz.cc_CallContext.effAccountId, branchLoc);
            //     res.success = true;
            // }
            res.data = acc.Local_Delivery_Allowed__c;
        }catch(Exception e){
            //error handling here, perhaps using res.messages to pass data back
            ccrz.ccLog.log(LoggingLevel.ERROR,'ERR',e);
        }finally{
            ccrz.ccLog.log(LoggingLevel.INFO,'M:X','isLocalDeliveryAllowed');
            ccrz.ccLog.close(res);
        }
        return res;
    }

    @RemoteAction
    global static String getPriceList(String jsonString) {
        List<Map<String,Object>> parts = new List<Map<String,Object>>();
        List<Object> items = (List<Object>) JSON.deserializeUntyped(jsonString);

        for (Object itemObj : items) {
            Map<String, Object> item = (Map<String, Object>) itemObj;
            parts.add(item);
        }
        return JSON.serialize(price(parts).listPricing);
    }


    @RemoteAction
    global static ccrz.cc_RemoteActionResult getProductInventoryForSku(ccrz.cc_RemoteActionContext ctx, List<String> skus) {
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx);
        try{
            ccrz.ccLog.log(LoggingLevel.DEBUG,'P:ccrz.cc_CallContext.effAccountId', ccrz.cc_CallContext.effAccountId);
            res.data = CCPDCInventoryHelper.getInventory(skus, ccrz.cc_CallContext.effAccountId);
            res.success = true;
        }catch(Exception e){
            //error handling here, perhaps using res.messages to pass data back
            ccrz.ccLog.log(LoggingLevel.ERROR,'ERR',e);
        }finally{
            ccrz.ccLog.log(LoggingLevel.INFO,'M:X','getProductInventoryForSku');
            ccrz.ccLog.close(res);
        }
        return res;
    }

    @RemoteAction
    global static String getVinDetails(String vin) {
        return JSON.serialize(CCFPVinHelper.getVinDetails(vin));
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult addToCart(ccrz.cc_RemoteActionContext ctx, String sku, Integer quantity, String prodBean) {
        // TODO: add shipping method
        return CCPDCProductDetailSimpleController.addToCart(ctx, sku, quantity, prodBean);
    }

    @RemoteAction
    global static String getSpecGroupTree() {
        List<ccrz__E_Category__c> categories = [
            SELECT ccrz__CategoryID__c, Spec_Group__c
            FROM ccrz__E_Category__c
            WHERE Spec_Group__c != null AND ccrz__ProductCount__c > 0
        ];

        Map<String, String> specGroups = new Map<String, String>{};
        for (ccrz__E_Category__c category : categories) {
            specGroups.put(category.Spec_Group__c, category.ccrz__CategoryID__c);
        }

        List<ccrz__E_Spec__c> specs = [
            SELECT ccrz__DisplayName__c, ccrz__SpecGroup__c
            FROM ccrz__E_Spec__c
            WHERE ccrz__UseForFilter__c = true
            AND ccrz__SpecGroup__c in :specGroups.keySet()
        ];

        Map<String, List<String>> dictionary = new Map<String, List<String>>{};
        for (ccrz__E_Spec__c spec : specs) {
            String specGroup = spec.ccrz__SpecGroup__c;
            String key = specGroups.get(specGroup);
            if (key != null && dictionary.get(key) != null) {
                // There is already an attribute for this spec group
                dictionary.get(key).add(spec.ccrz__DisplayName__c);
            } else {
                List<String> attr =  new List<String>();
                attr.add(spec.ccrz__DisplayName__c);
                dictionary.put(specGroups.get(specGroup), attr);
            }
        }

        return JSON.serialize(dictionary);

    }
}