@isTest
public class CCPDCProductDetailsBIEControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }    

    static testmethod void controllerTest() {
        util.initCallContext();

        ccrz__E_Product__c p = util.getProduct();

        CCPDCProductDetailsBIEController c = null;
        ApexPages.currentPage().getParameters().put('sku', p.ccrz__SKU__c);
        
        Test.startTest();
        c = new CCPDCProductDetailsBIEController();
        Test.stopTest();

        System.assert(c != null);
        System.assertEquals('Product', c.productType);
    }
}