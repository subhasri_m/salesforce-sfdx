@isTest
public class CCPDCProductListControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Map<String, Object> m;
        System.runAs(thisUser) {
            m = util.initData();
        }
        
        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = 'DefaultStore';
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        settings.Enable_Boomi_On_All__c = true;
        insert settings;

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        update cart;

        ccrz__E_PriceList__c priceListtest = (ccrz__E_PriceList__c) m.get('priceList');

        priceListtest.Name = 'Enterprise';
        update priceListtest;
        System.debug(priceListtest);
       
        List<ccrz__E_PageLabel__c> labels = new List<ccrz__E_PageLabel__c>{
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_Ship', ccrz__Value__c = 'CC_PDC_Restrict_Ship'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_WillCall_No_Inv', ccrz__Value__c = 'CC_PDC_Restrict_WillCall_No_Inv'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_BackOrderNotAllowed', ccrz__Value__c = 'CC_PDC_Restrict_BackOrderNotAllowed'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_WillCall_AdjustQty', ccrz__Value__c = 'CC_PDC_Restrict_WillCall_AdjustQty'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_WillCall_NoPerm', ccrz__Value__c = 'CC_PDC_Restrict_WillCall_NoPerm'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_BackOrderModal_Line1', ccrz__Value__c = 'CC_PDC_Restrict_BackOrderModal_Line1'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_BackOrderModal_Line2', ccrz__Value__c = 'CC_PDC_Restrict_BackOrderModal_Line2'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_BackOrderModal_Wc_Ship', ccrz__Value__c = 'CC_PDC_Restrict_BackOrderModal_Wc_Ship'),
            new ccrz__E_PageLabel__c(Name = 'CC_PDC_Restrict_BackOrderModal_Wc_ShipSP', ccrz__Value__c = 'CC_PDC_Restrict_BackOrderModal_Wc_ShipSP')
        };
        insert labels;

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void checkRestrictionsShipTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"1","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void getPriceFromBOOMITest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Account account = util.getAccount();
        ctx.effAccountId = String.valueOf(account.Id);

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Cart__c cart = util.getCart();

        String data = '[{}]';
        String data2 = '[{ "SKU":"ANC-3116", "poolNumber":"682","partNumber":"3116"},{ "SKU":"ANC-3116", "poolNumber":"682","partNumber":"3116"},{ "SKU":"ANC-3116", "poolNumber":"682","partNumber":"3116"}]';

        ccrz.cc_RemoteActionResult result = null;
        ccrz.cc_RemoteActionResult result2 = null;

        Test.startTest();
        result = CCPDCProductListController.getPriceFromBOOMI(ctx, data);
        result2 = CCPDCProductListController.getPriceFromBOOMI(ctx, data2);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result2 != null);
    }

    static testmethod void getPriceFromBOOMIContTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Account account = util.getAccount();
        ctx.effAccountId = String.valueOf(account.Id);

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Cart__c cart = util.getCart();
        List<String> datainmap = new List<String>();

        //Map<String,Object> data1 = new Map<String,Object>();
        //data1.put('SKU',p.ccrz__SKU__c);
        //data1.put('poolNumber','123');
        //data1.put('productId',p.Id);
        //data1.put('partNumber',p.ccrz__SKU__c);
        //datainmap.add(data1);
        String data1 = '{ "SKU":"product-01", "poolNumber":"123","partNumber":"product-01","productId":"'+p.Id+'"}';
        datainmap.add(data1);
        String data2 = JSON.serialize(datainmap);
        String data = '[{}]';//'[{"SKU":"ANC-3116","poolNumber":"682","partNumber":"3116"},{ "SKU":"ANC-3116", "poolNumber":"682","partNumber":"3116"},{ "SKU":"ANC-3116", "poolNumber":"682","partNumber":"3116"}]';

        // Invoke the continuation by calling the action method
        Continuation conti = (Continuation)CCPDCProductListController.getPriceFromBOOMICont(ctx, data2);
        
        // Verify that the continuation has the proper requests
        Map<String, HttpRequest> requests = conti.getRequests();
        system.assert(requests.size() == 1);
        system.assert(requests.get((String)CCPDCProductListController.stateMap.get('RequestLabel')) != null);
        
        // Perform mock callout 
        // (i.e. skip the callout and call the callback method)
        HttpResponse response = new HttpResponse();
        response.setBody('Mock response body');   
        // Set the fake response for the continuation     
        Test.setContinuationResponse((String)CCPDCProductListController.stateMap.get('RequestLabel'), response);
        // Invoke callback method
        Object result = Test.invokeContinuationMethod(new CCPDCProductListController(), conti);
    }

    static testmethod void getPriceFromBOOMIContNoBoomiTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Account account = util.getAccount();
        ctx.effAccountId = String.valueOf(account.Id);

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Cart__c cart = util.getCart();

        String data = '[{}]';//'[{"SKU":"ANC-3116","poolNumber":"682","partNumber":"3116"},{ "SKU":"ANC-3116", "poolNumber":"682","partNumber":"3116"},{ "SKU":"ANC-3116", "poolNumber":"682","partNumber":"3116"}]';

        // Invoke the continuation by calling the action method
        Object  conti = CCPDCProductListController.getPriceFromBOOMICont(ctx, data); //(ccrz.cc_RemoteActionResult)
     //           Continuation conti = (Continuation)CCPDCProductListController.getPriceFromBOOMICont(ctx, data);
         // Verify that the continuation has the proper requests

        system.assert(conti != null);
        //system.assert(requests.get((String)CCPDCProductListController.stateMap.get('RequestLabel')) != null);
        
        // Perform mock callout 
        // (i.e. skip the callout and call the callback method)
        HttpResponse response = new HttpResponse();
        response.setBody('Mock response body');   
        // Set the fake response for the continuation     
        //Test.setContinuationResponse((String)CCPDCProductListController.stateMap.get('RequestLabel'), response);
        // Invoke callback method
        //Object result = Test.invokeContinuationMethod(new CCPDCProductListController(), conti);
    }

    static testmethod void doCombinedAddToCartActionTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Account account = util.getAccount();
        ctx.effAccountId = String.valueOf(account.Id);

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"15","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.doCombinedAddToCartAction(ctx, data, 'ANC-3116', 1, '20');
        Test.stopTest();

        System.assert(result != null);
    }

    static testmethod void checkRestrictionsShipBackorderTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;
        
        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        a.OK_to_Backorder__c = 'Yes';
        update a;

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"15","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsShipBackorderNotAllowedTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;
        
        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"15","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
        System.assertEquals('CC_PDC_Restrict_BackOrderNotAllowed', result.messages[0].message);
    }

    static testmethod void checkRestrictionsShipNoInventoryNoBackorderTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        update a;

        ccrz__E_Product__c p = util.getProduct();
        List<ccrz__E_ProductInventoryItem__c> inv = [SELECT Id FROM ccrz__E_ProductInventoryItem__c WHERE ccrz__ProductItem__c = :p.Id];
        delete inv;

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"5","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
        System.assertEquals('CC_PDC_Restrict_BackOrderNotAllowed', result.messages[0].message);
    }

    static testmethod void checkRestrictionsShipNoInventoryTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        a.OK_to_Backorder__c = 'Yes';
        update a;

        ccrz__E_Product__c p = util.getProduct();
        List<ccrz__E_ProductInventoryItem__c> inv = [SELECT Id FROM ccrz__E_ProductInventoryItem__c WHERE ccrz__ProductItem__c = :p.Id];
        delete inv;

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"5","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsWillCallShipTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        a.OK_to_Backorder__c = 'Yes';
        update a;

        ccrz__E_Product__c p = util.getProduct();
        List<ccrz__E_ProductInventoryItem__c> inv = [SELECT Id FROM ccrz__E_ProductInventoryItem__c WHERE ccrz__ProductItem__c = :p.Id];
        delete inv;

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"15","ffType":"WILLCALL_SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsWillCallShipBackOrderTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        perms.Maximum_Order_Limit__c = 1000000.00;
        insert perms;


        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        a.OK_to_Backorder__c = 'Yes';
        update a;

        ccrz__E_Product__c p = util.getProduct();
        //List<ccrz__E_ProductInventoryItem__c> inv = [SELECT Id FROM ccrz__E_ProductInventoryItem__c WHERE ccrz__ProductItem__c = :p.Id];
        //delete inv;

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"15","ffType":"WILLCALL_SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsShipHasSalespackTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        perms.Maximum_Order_Limit__c = 1000000.00;
        insert perms;


        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        a.OK_to_Backorder__c = 'Yes';
        update a;

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Product__c updatep = [SELECT  Id, Name, ccrz__SKU__c, Salespack__c FROM ccrz__E_Product__c WHERE Id =: p.Id];
        //List<ccrz__E_ProductInventoryItem__c> inv = [SELECT Id FROM ccrz__E_ProductInventoryItem__c WHERE ccrz__ProductItem__c = :p.Id];
        //delete inv;
        updatep.Salespack__c = 3;
        update updatep;
        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"15","ffType":"WILLCALL_SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsWillCallNoPermissionsTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"1","ffType":"WILLCALL"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
        System.assertEquals('CC_PDC_Restrict_WillCall_NoPerm', result.messages[0].message);
    }

    static testmethod void checkRestrictionsWillCallTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(true);
        insert perms;

        Account a = util.getAccount();
        a.Will_Call_Allowed__c = true;
        update a;

        ccrz__E_Product__c p = util.getProduct();
        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+cart.ccrz__ShipTo__c+'","qty":"1","ffType":"WILLCALL"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsStateTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz__E_Product__c p = util.getProduct();

        List<Product_Restriction__c> productRestrictions = new List<Product_Restriction__c>{
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restriction_Area_Type__c = 'State', Restriction_Country__c = 'US', Restriction_State__c = 'IL')
        };
        insert productRestrictions;

        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr;    

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+addr.Id+'","qty":"1","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
        System.assertEquals('CC_PDC_Restrict_Ship', result.messages[0].message);
    }

    static testmethod void checkRestrictionsStateCityTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz__E_Product__c p = util.getProduct();

        List<Product_Restriction__c> productRestrictions = new List<Product_Restriction__c>{
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restriction_Area_Type__c = 'State', Restriction_Country__c = 'US', Restriction_State__c = 'IL'),
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restrcition_Type__c = 'Un-Restricted', Restriction_Area_Type__c = 'City', Restriction_Country__c = 'US', Restriction_State__c = 'IL', Restriction_City__c = 'Chicago')
        };
        insert productRestrictions;

        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr;    

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+addr.Id+'","qty":"1","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsCountryTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz__E_Product__c p = util.getProduct();

        List<Product_Restriction__c> productRestrictions = new List<Product_Restriction__c>{
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restriction_Area_Type__c = 'Country', Restriction_Country__c = 'US', Restriction_State__c = 'IL')
        };
        insert productRestrictions;

        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr;    

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+addr.Id+'","qty":"1","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
        System.assertEquals('CC_PDC_Restrict_Ship', result.messages[0].message);
    }

    static testmethod void checkRestrictionsCountryStateTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz__E_Product__c p = util.getProduct();

        List<Product_Restriction__c> productRestrictions = new List<Product_Restriction__c>{
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restriction_Area_Type__c = 'Country', Restriction_Country__c = 'US', Restriction_State__c = 'IL'),
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restrcition_Type__c = 'Un-Restricted', Restriction_Area_Type__c = 'State', Restriction_Country__c = 'US', Restriction_State__c = 'IL')
        };
        insert productRestrictions;

        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr;    

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+addr.Id+'","qty":"1","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsCountryZipTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz__E_Product__c p = util.getProduct();

        List<Product_Restriction__c> productRestrictions = new List<Product_Restriction__c>{
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restriction_Area_Type__c = 'Country', Restriction_Country__c = 'US', Restriction_State__c = 'IL'),
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restrcition_Type__c = 'Un-Restricted', Restriction_Area_Type__c = 'ZipCode', Restriction_Country__c = 'US', Restriction_State__c = 'IL', Restriction_ZipCode__c = '60606')
        };
        insert productRestrictions;

        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr;    

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+addr.Id+'","qty":"1","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsCountryCityTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz__E_Product__c p = util.getProduct();

        List<Product_Restriction__c> productRestrictions = new List<Product_Restriction__c>{
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restriction_Area_Type__c = 'Country', Restriction_Country__c = 'US', Restriction_State__c = 'IL'),
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restrcition_Type__c = 'Un-Restricted', Restriction_Area_Type__c = 'City', Restriction_Country__c = 'US', Restriction_State__c = 'IL', Restriction_City__c = 'Chicago')
        };
        insert productRestrictions;

        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr;    

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+addr.Id+'","qty":"1","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void checkRestrictionsZipTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz__E_Product__c p = util.getProduct();

        List<Product_Restriction__c> productRestrictions = new List<Product_Restriction__c>{
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restriction_Area_Type__c = 'ZipCode', Restriction_Country__c = 'US', Restriction_ZipCode__c = '60606')
        };
        insert productRestrictions;

        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr;    

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+addr.Id+'","qty":"1","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
        System.assertEquals('CC_PDC_Restrict_Ship', result.messages[0].message);
    }

    static testmethod void checkRestrictionsCityTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz__E_Product__c p = util.getProduct();

        List<Product_Restriction__c> productRestrictions = new List<Product_Restriction__c>{
            new Product_Restriction__c(Related_to_Product__c = p.Id, Restriction_Status__c = true, Restriction_Area_Type__c = 'City', Restriction_Country__c = 'US', Restriction_State__c = 'IL', Restriction_City__c = 'Chicago')
        };
        insert productRestrictions;

        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr;    

        ccrz__E_Cart__c cart = util.getCart();

        String data = '{"productId":"'+p.Id+'","sku":"'+p.ccrz__SKU__c+'","dc":"'+CCPDCTestUtil.DEFAULT_LOCATION_CODE+'","sfid":"'+addr.Id+'","qty":"1","ffType":"SHIP"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.checkRestrictions(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
        System.assertEquals('CC_PDC_Restrict_Ship', result.messages[0].message);
    }

    static testmethod void addToCartTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCProductListController.addToCart(ctx, 'product-01', 1,'50');
        Test.stopTest();

        System.assert(result != null);
        //System.assert(result.success);
    }
    static testmethod void checkExistOrderListNameTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        String wishlistName = 'wishlist';
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCProductListController.checkExistWishlistName(ctx, wishlistName);
        System.debug(result);
        Test.stopTest();
        System.assert(result != null);

    }
}