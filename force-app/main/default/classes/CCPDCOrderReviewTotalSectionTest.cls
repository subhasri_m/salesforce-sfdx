@isTest
private class CCPDCOrderReviewTotalSectionTest {
	
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }


        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void testSetTaxRate(){

        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesDefaultTaxRateResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesDefaultTaxRateResponseList><ISeriesDefaultTaxRate><DefaultTaxRate>0.082500</DefaultTaxRate></ISeriesDefaultTaxRate><ISeriesDefaultTaxRate><ErrorMessage></ErrorMessage></ISeriesDefaultTaxRate></wss:ISeriesDefaultTaxRateResponseList></wss:getISeriesDefaultTaxRateResponse></S:Body></S:Envelope>'));
        util.initCallContext();
    	ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz__E_Cart__c cartid = util.getCart();
        Account a = util.getAccount();
        ctx.storefront = 'DefaultStore';
        ctx.currentCartId = String.valueOf(cartid.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);


        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = 'DefaultStore';
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        upsert settings;

    	Location__c alocation = new Location__c();
    	alocation.Address_Line_1__c = '499 South Pickett Street';
    	alocation.Location__c = 'ALEVA';
    	alocation.Location_ID__c = 'ALEVA';
    	alocation.Name = 'Alexandria-VA';
        alocation.City__c= 'Alexandria';
        alocation.County__c = 'Alexandria';
        alocation.State__c = 'VA';
        alocation.Zipcode__c = '22304';
        alocation.In_City__c = true;

        insert alocation;  
        List<Location__c> locWithIds = [SELECT Id, Location_ID__c FROM Location__c WHERE Location_ID__c = 'ALEVA'];
        Location__c locWithId = locWithIds.get(0);
        List<ccrz__E_Cart__c> carts = [SELECT Id, CC_FP_Location__c, ccrz__BillTo__c, ccrz__ShipTo__c,ccrz__TaxExemptFlag__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = 'enc123'];
        ccrz__E_Cart__c cart = carts.get(0);
        cart.CC_FP_Location__c = locWithId.Id;
        cart.ccrz__TaxExemptFlag__c = false;

        List<ccrz__E_ContactAddr__c> contactAddresses= [SELECT Id, ccrz__AddressFirstline__c,ccrz__AddressSecondline__c,ccrz__AddressThirdline__c,ccrz__City__c,ccrz__State__c,ccrz__Country__c,ccrz__PostalCode__c,CC_FP_County__c,CC_FP_Location__c FROM ccrz__E_ContactAddr__c WHERE ccrz__PostalCode__c = '11111'];
        ccrz__E_ContactAddr__c contactAddress = contactAddresses.get(0);
        contactAddress.ccrz__AddressFirstline__c = '499 South Pickett Street';
        contactAddress.ccrz__City__c = 'Alexandria';
        contactAddress.ccrz__State__c = 'VA';
        contactAddress.ccrz__Country__c = 'United States';
        contactAddress.CC_FP_County__c = 'Alexandria';
        contactAddress.ccrz__PostalCode__c = '22304';
        contactAddress.CC_FP_Location__c = locWithId.Id;
        ccrz__E_ContactAddr__c newContactAdd = contactAddress.clone();
        upsert(new List<ccrz__E_ContactAddr__c>{contactAddress,newContactAdd});

        cart.ccrz__BillTo__c = contactAddress.Id;
        cart.ccrz__BillTo__c = newContactAdd.Id;
        update cart;

        //System.debug('cart: '+cart);
        //System.debug('contactAddress: '+contactAddress);
        Test.startTest(); 
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCOrderReviewTotalSectionController.setTaxRate(ctx);
        Test.stopTest(); 

        List<ccrz__E_Cart__c> currentCarts = [SELECT Id, ccrz__TaxAmount__c, ccrz__TotalAmount__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = 'enc123'];
        ccrz__E_Cart__c currentCart  = currentCarts.get(0);
        System.assert(result.success);
        System.assertEquals(currentCart.ccrz__TaxAmount__c,0.824175);


    }


    static testmethod void testSetTaxRateException(){
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz__E_Cart__c cartid = util.getCart();
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        Location__c alocation = new Location__c();
        alocation.Address_Line_1__c = '499 South Pickett Street';
        alocation.Location__c = 'ALEVA';
        alocation.Location_ID__c = 'ALEVA';
        alocation.Name = 'Alexandria-VA';
        alocation.City__c= 'Alexandria';
        alocation.County__c = 'Alexandria';
        alocation.State__c = 'VA';
        alocation.Zipcode__c = '22304';
        alocation.In_City__c = true;

        insert alocation;  
        List<Location__c> locWithIds = [SELECT Id, Location_ID__c FROM Location__c WHERE Location_ID__c = 'ALEVA'];
        Location__c locWithId = locWithIds.get(0);
        List<ccrz__E_Cart__c> carts = [SELECT Id, CC_FP_Location__c, ccrz__BillTo__c, ccrz__ShipTo__c,ccrz__TaxExemptFlag__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c = 'enc123'];
        ccrz__E_Cart__c cart = carts.get(0);
        cart.CC_FP_Location__c = locWithId.Id;
        cart.ccrz__TaxExemptFlag__c = false;

        List<ccrz__E_ContactAddr__c> contactAddresses= [SELECT Id, ccrz__AddressFirstline__c,ccrz__AddressSecondline__c,ccrz__AddressThirdline__c,ccrz__City__c,ccrz__State__c,ccrz__Country__c,ccrz__PostalCode__c,CC_FP_County__c,CC_FP_Location__c FROM ccrz__E_ContactAddr__c WHERE ccrz__PostalCode__c = '11111'];
        ccrz__E_ContactAddr__c contactAddress = contactAddresses.get(0);
        contactAddress.ccrz__AddressFirstline__c = '499 South Pickett Street';
        contactAddress.ccrz__City__c = 'Alexandria';
        contactAddress.ccrz__State__c = 'VA';
        contactAddress.ccrz__Country__c = 'United States';
        contactAddress.CC_FP_County__c = 'Alexandria';
        contactAddress.ccrz__PostalCode__c = '22304';
        contactAddress.CC_FP_Location__c = locWithId.Id;
        ccrz__E_ContactAddr__c newContactAdd = contactAddress.clone();
        upsert(new List<ccrz__E_ContactAddr__c>{contactAddress,newContactAdd});

        cart.ccrz__BillTo__c = contactAddress.Id;
        cart.ccrz__BillTo__c = newContactAdd.Id;
        update cart;

        Test.startTest(); 
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCOrderReviewTotalSectionController.setTaxRate(ctx);
        Test.stopTest(); 
        System.assert(!result.success);


    }

    public class BoomiServiceMock implements HttpCalloutMock {

        public Integer code {get; set;}
        public String status {get; set;}
        public String body {get; set;}

        public BoomiServiceMock(Integer code, String status, String body) {
            this.code = code;
            this.status = status;
            this.body = body;
        }

        public HTTPResponse respond(HTTPRequest request) {
            HttpResponse response = new HttpResponse();
            if (this.body != null) {
                response.setBody(body);
            }
            response.setStatusCode(this.code);
            response.setStatus(this.status);
            return response;
        }
    }
	
}