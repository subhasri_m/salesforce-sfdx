public class CCPDCReportingController {
    
    public String IsUserLoggedIn {get;set;}
    public Boolean ShowWithPricing  {get;set;}
    public Boolean ShowWithoutPricing  {get;set;}
    public String BaseURL {get;set;}
    public String EffectiveAccountid {get;set;}
     public Boolean AccountingReportsAllowed {get;set;}
    public Boolean SalesReportsAllowed {get;set;}
    public String matrixID {get; set;}
    public CCPDCReportingController(){
        EffectiveAccountid =apexpages.currentpage().getparameters().get('effectiveAccount');
        IsUserLoggedIn  = UserInfo.getUsertype();
        BaseURL = System.label.BaseURL;
        FetchContact();
    } 
    
    
        public void FetchContact()
    {
        String Userid = Userinfo.getUserId();
        List<User> lstUser = [Select Id,ContactId from User  where Id = :Userid limit 1];  
        if (!lstUser.isEmpty())
        {  List<CC_FP_Contract_Account_Permission_Matrix__c> PermissionMatrix = new   List<CC_FP_Contract_Account_Permission_Matrix__c> ();
            If (Test.isRunningTest())
           {
             PermissionMatrix= [select id,Pricing_Visibility__c, Allow_Access_To_Accounting_Reports__c,Allow_Access_to_Sales_Reports__c,Account__c from CC_FP_Contract_Account_Permission_Matrix__c  limit 1 ];
      
           }
            
            else
            {
                   PermissionMatrix= [select id,Pricing_Visibility__c, Allow_Access_To_Accounting_Reports__c,Allow_Access_to_Sales_Reports__c,Account__c from CC_FP_Contract_Account_Permission_Matrix__c where Contact__c =: lstUser[0].ContactId and Account__c =: EffectiveAccountid limit 1 ];
         
            }
            if(!PermissionMatrix.isEmpty()){
                AccountingReportsAllowed = PermissionMatrix[0].Allow_Access_To_Accounting_Reports__c;
                SalesReportsAllowed = PermissionMatrix[0].Allow_Access_to_Sales_Reports__c;
                Boolean PricingCheck = PermissionMatrix[0].Pricing_Visibility__c; 
                if(PricingCheck==true)
                {  ShowWithPricing = true;ShowWithoutPricing = false; }
 				else
                {  ShowWithPricing = false; ShowWithoutPricing = true; }
  			  matrixID = PermissionMatrix[0].id;
            }
        } 
    }
}