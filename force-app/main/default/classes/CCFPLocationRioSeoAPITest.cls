@isTest
public class CCFPLocationRioSeoAPITest {
    public static final string ipAddress = '76.80.192.82';
    public static final String RESPONSE_STRING = '[{"fid":"BSL","location_name":"FleetPride","address_1":"2467 State Road","address_2":"","city":"Bensalem","region":"PA","post_code":"19020","country":"US","lat":"40.0708575","lng":"-74.9396008","distance":"2.2"}]';    
    static CCPDCTestUtil util = new CCPDCTestUtil();
    
    static testmethod void getNearestFleetPrideBranchTestPositive() {
        Test.setMock(HttpCalloutMock.class, new RIOSEOMock(200, 'OK', RESPONSE_STRING));
        CCFPLocationRioSeoAPI.Location  loc= null;
        
        Location__c location = util.createLocation('BSL');
        insert location;
            
        Test.startTest();
        loc = CCFPLocationRioSeoAPI.getNearestFleetPrideBranch(ipAddress,'75247',location.Location__c, true);        
        Test.stopTest();
    } 
        static testmethod void getNearestFleetPrideBranchTestNegative() {
        Test.setMock(HttpCalloutMock.class, new RIOSEOMock(500, 'Failure', RESPONSE_STRING));
        CCFPLocationRioSeoAPI.Location  loc= null;
        Test.startTest();
        loc = CCFPLocationRioSeoAPI.getNearestFleetPrideBranch(ipAddress,'','',true);        
        Test.stopTest();
    }
    static testmethod void getNearestFleetPrideBranchTestException() {
        Test.setMock(HttpCalloutMock.class, new RIOSEOMock(200, 'OK', 'Invalid String'));
        CCFPLocationRioSeoAPI.Location  loc= null;
        Test.startTest();
        loc = CCFPLocationRioSeoAPI.getNearestFleetPrideBranch(ipAddress,'','', true);        
        Test.stopTest();
    } 
    public class RIOSEOMock implements HttpCalloutMock {
        public Integer code {get; set;}
        public String status {get; set;}
        public List<String> bodys {get; set;}
        
        public RIOSEOMock(Integer code, String status, String body) {
            this.code = code;
            this.status = status;
            this.bodys = new List<String>{body};
                }
        
        public HTTPResponse respond(HTTPRequest request) {
            HttpResponse response = new HttpResponse();
            if (this.bodys != null && !this.bodys.isEmpty()) {
                response.setBody(this.bodys[0]);
                this.bodys.remove(0);
            }
            response.setStatusCode(this.code);
            response.setStatus(this.status);
            return response;
        }
    }
    
}