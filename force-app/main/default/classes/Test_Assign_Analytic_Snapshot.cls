@isTest
public class Test_Assign_Analytic_Snapshot{

    static testMethod void Assign_Analytic_Snapshot(){
    Test.startTest();
    
User user1 = new User(alias = 'ceo', email='admin@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-88888',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTas@testorg.com', profileid = '00e400000013ttZ');
        insert user1;
User user2 = new User(alias = 'ceo2', email='admin2@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-99999',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='admin2@testorg.com', profileid = '00e400000013ttZ');
        insert user2;
        
     system.runas(user1){   
Analytic_Snapshot_Data__c asnap = new Analytic_Snapshot_Data__c (Number_of_Records__c = 471, OwnerId__c = User1.id, territory__c = 'PDC', Region__c = 'PDC');
insert asnap;



asnap.OwnerId__c = user2.id;
Update asnap;
}
Test.stopTest();
    }
}