@isTest
public class fp_ctrl_CatSiteMapTest{
    public String declarationXml { get { return '<?xml version="1.0" encoding="UTF-8"?>\n'; } }
    public String categoryId {get;set;}
    public List<fp_bean_SiteMap> prodData {get;set;}
    public fp_bean_SiteMap catData {get; set;}
    public Boolean useFurls {get; set;}
    public Boolean useNewProductListPage {get; set;}
    public final String HTTPS = 'https://';
    public String locale {get;set;}

   static testmethod void constructorTest() {
      

       String categoryId = ccrz.cc_CallContext.currPageParameters.get('categoryId');      
       Boolean useNewProductListPage = ccrz.cc_CallContext.isConfigTrue('pl.usenew', null);
       
       ccrz__E_AccountGroup__c ccaccgrp =new ccrz__E_AccountGroup__c();
       ccaccgrp.ccrz__AccountGroupID__c='002-0015920-000';
       ccaccgrp.ccrz__DataId__c='';
       ccaccgrp.ccrz__Desc__c='002-0015920-000-AMEN DIESEL INC';
       ccaccgrp.ccrz__PriceListSelectionMethod__c='Best Price';
       ccaccgrp.Name='002-0015920-000-AMEN DIESEL INC';
       insert ccaccgrp;

            fp_ctrl_CatSiteMap controller = null;

            Test.startTest();
            List<fp_bean_Sitemap> fbWOAccGrp = fp_ctrl_CatSiteMap.retrieveProductsByCategory(categoryId );
            ccrz.cc_CallContext.currAccountGroup = ccaccgrp;
            List<fp_bean_Sitemap> fb= fp_ctrl_CatSiteMap.retrieveProductsByCategory(categoryId );
            Test.stopTest();
            System.assert(fb!= null);
         //   System.assert(controller != null);
         //   System.assert(!controller.isLoaded);
          //  System.assert(controller.data != null);

    }
    static testmethod void getretrieveCatDataTest(){
        String categoryId = ccrz.cc_CallContext.currPageParameters.get('categoryId');      
        List<ccrz__E_Category__c> categoryObj = new  List<ccrz__E_Category__c>();
        ccrz__E_Category__c categoryObjvalue =new ccrz__E_Category__c();
        categoryObjvalue.ccrz__CategoryID__c='Axle & Hubs';
        categoryObjvalue.ccrz__LongDesc__c='Axle & Hubs';
        categoryObjvalue.ccrz__SEOId__c='rahul1-test-7';
        categoryObjvalue.ccrz__Sequence__c=500.0;
        categoryObjvalue.Name='PDC Parts';
        categoryObj.add(categoryObjvalue);
        
        ccrz__E_Category__c categoryObjvalue2 =new ccrz__E_Category__c();
        categoryObjvalue2.ccrz__CategoryID__c='xle & Hubs';
        categoryObjvalue2.ccrz__LongDesc__c='xle & Hubs';
        categoryObjvalue2.ccrz__SEOId__c='ahul1-test-7';
        categoryObjvalue2.ccrz__Sequence__c=600.0;
        categoryObjvalue2.Name='PDC Parts';
        categoryObj.add(categoryObjvalue2);
     //   insert categoryObjvalue2;
        
        ccrz__E_Category__c categoryObjvalue3 =new ccrz__E_Category__c();
        categoryObjvalue3.ccrz__CategoryID__c='bxle & Hubs';
        categoryObjvalue3.ccrz__LongDesc__c='bxle & Hubs';
        categoryObjvalue3.ccrz__SEOId__c='hul1-test-7';
        categoryObjvalue3.ccrz__Sequence__c=700.0;
        categoryObjvalue3.Name='PDC Parts';
        categoryObj.add(categoryObjvalue3);
        insert categoryObj;
        
      
        fp_bean_SiteMap beansitemap = new fp_bean_SiteMap();
  //      beansitemap = new fp_bean_SiteMap(categoryId);
        ccrz__E_Category__c categoryval =new ccrz__E_Category__c();
        categoryval.ccrz__CategoryID__c='Axoole & Hubs';
        categoryval.ccrz__LongDesc__c='Axoole & Hubs';
        categoryval.ccrz__SEOId__c='testul1-test-7';
        categoryval.ccrz__Sequence__c=900.0;
        categoryval.Name='testPDC Parts';
        insert categoryval;
        // [SELECT Id, LastModifiedDate FROM ccrz__E_Category__c WHERE Id =: categoryId LIMIT 1];
       
        Test.startTest();
        fp_bean_SiteMap fb= fp_ctrl_CatSiteMap.retrieveCatData(categoryval.id);
        Test.stopTest();
        System.assert(fb!= null);
      
    }
    
    static testmethod void getretrieveProductsByCategoryTest(){
        ccrz__E_Category__c cccategoryobj= new ccrz__E_Category__c();
        cccategoryobj.ccrz__CategoryID__c   ='Axle & Hubs';
        cccategoryobj.ccrz__LongDesc__c ='Axle & Hubs';
        cccategoryobj.ccrz__SEOId__c ='disc-brake-pad';
        cccategoryobj.ccrz__Sequence__c =500;  
        cccategoryobj.Name  = 'Disc Brake Pad';
        cccategoryobj.Spec_Group__c =' 006-03-007';
        insert cccategoryobj;
        
        ccrz.cc_CallContext.storefront = 'testStoreFront';
        
        ccrz__E_Product__c prodobj=new ccrz__E_Product__c();
        prodobj.CAT_Part_Number__c='POW-90-12393';
        prodobj.ccrz__CoolerpakFlag__c = false;
        prodobj.ccrz__DryiceFlag__c = false;
        prodobj.ccrz__SKU__c=  'POW-9012393';
        prodobj.ccrz__StartDate__c = System.today();
        prodobj.ccrz__EndDate__c = System.today().addDays(2);
        prodobj.ccrz__ProductType__c = 'Test';
        prodobj.ccrz__Storefront__c = 'testStoreFront';
        prodobj.ccrz__ProductStatus__c = 'Released';
        insert prodobj;
        
        fp_ctrl_CatSiteMap catsite= new fp_ctrl_CatSiteMap();
        catsite.categoryId = cccategoryobj.Id;
        
        ccrz__E_ProductCategory__c ccpc = new ccrz__E_ProductCategory__c();
        ccpc.ccrz__Category__c =cccategoryobj.id;
        ccpc.ccrz__IsCanonicalPath__c=false;
        ccpc.ccrz__ProductCategoryId__c='POW-9012393-Spring Attaching Parts';
        ccpc.ccrz__Product__c =prodobj.id;
        ccpc.ccrz__Sequence__c = 500.0;
        ccpc.ccrz__StartDate__c = system.today();
        ccpc.ccrz__EndDate__c= system.today();
        insert ccpc;
        
        
        
        ccrz__E_AccountGroup__c ccaccgrp =new ccrz__E_AccountGroup__c();
        ccaccgrp.ccrz__AccountGroupID__c='002-0015920-000';
        ccaccgrp.ccrz__DataId__c='';
        ccaccgrp.ccrz__Desc__c='002-0015920-000-AMEN DIESEL INC';
        ccaccgrp.ccrz__PriceListSelectionMethod__c='Best Price';
        ccaccgrp.Name='002-0015920-000-AMEN DIESEL INC';
        insert ccaccgrp;
        
        ccrz.cc_CallContext.currAccountGroup = ccaccgrp;
        
        ccrz__E_PriceList__c ccpricelist= new ccrz__E_PriceList__c();
        ccpricelist.ccrz__CurrencyISOCode__c='USD';
        ccpricelist.ccrz__Desc__c='FP Base Price List';
        ccpricelist.ccrz__Enabled__c=true;
      //  ccpricelist.ccrz__EndDate__c=Date.parse('2099-12-31');
        ccpricelist.ccrz__PricelistId__c='FP_Base_Price_List';
     //   ccpricelist.ccrz__Seller__c='';
     //   ccpricelist.ccrz__StartDate__c=Date.parse('2020-06-08');
        ccpricelist.ccrz__Storefront__c='parts';
        //ccpricelist.ccrz__AccountGroup__c = ccaccgrp;
        ccpricelist.ccrz__Storefront__c = 'testStoreFront';
        ccpricelist.ccrz__StartDate__c = System.today();
        ccpricelist.ccrz__EndDate__c = System.today();
        insert ccpricelist;
        
        ccrz__E_AccountGroupPriceList__c acgrpList = new ccrz__E_AccountGroupPriceList__c();
        acgrpList.ccrz__Pricelist__c=ccpricelist.id;
        acgrpList.ccrz__AccountGroup__c=ccaccgrp.id;
        
        insert acgrpList ;
        
         ccrz__E_PriceListItem__c ccpricelisitem = new ccrz__E_PriceListItem__c();
        ccpricelisitem.ccrz__EndDate__c =system.today();
        ccpricelisitem.ccrz__PricelistItemId__c ='Test Id';
        ccpricelisitem.ccrz__Pricelist__c = ccpricelist.id;
        ccpricelisitem.ccrz__Price__c =450.0;
        ccpricelisitem.ccrz__Product__c=prodobj.id;
        ccpricelisitem.ccrz__RecurringPrice__c = false;
        ccpricelisitem.ccrz__StartDate__c =system.today();
        insert ccpricelisitem;
        
        
        //List<ghhg> lst = 
        
        fp_ctrl_CatSiteMap.retrieveProductsByCategory(cccategoryobj.Id);
       
    
    
    }
    static testmethod void getAllowedPricelistsTest(){
      
       //  List<ccrz__E_AccountGroup__c > returnResults = null;
        fp_ctrl_CatSiteMap catsite= new fp_ctrl_CatSiteMap();
         Map<Id,ccrz__E_PriceList__c> returnResults = new Map<Id,ccrz__E_PriceList__c>();
      
     
        ccrz__E_AccountGroup__c ccaccgrp =new ccrz__E_AccountGroup__c();
        ccaccgrp.ccrz__AccountGroupID__c='002-0015920-000';
        ccaccgrp.ccrz__DataId__c='';
        ccaccgrp.ccrz__Desc__c='002-0015920-000-AMEN DIESEL INC';
        ccaccgrp.ccrz__PriceListSelectionMethod__c='Best Price';
        ccaccgrp.Name='002-0015920-000-AMEN DIESEL INC';
        insert ccaccgrp;
        
        ccrz__E_PriceList__c ccpricelist= new ccrz__E_PriceList__c();
        ccpricelist.ccrz__CurrencyISOCode__c='USD';
        ccpricelist.ccrz__Desc__c='FP Base Price List';
        ccpricelist.ccrz__Enabled__c=true;
      //  ccpricelist.ccrz__EndDate__c=Date.parse('2099-12-31');
        ccpricelist.ccrz__PricelistId__c='FP_Base_Price_List';
     //   ccpricelist.ccrz__Seller__c='';
     //   ccpricelist.ccrz__StartDate__c=Date.parse('2020-06-08');
        ccpricelist.ccrz__Storefront__c='parts';
        insert ccpricelist;
        
        
        
       
        ccrz__E_AccountGroupPriceList__c acgrpList = new ccrz__E_AccountGroupPriceList__c();
        acgrpList.ccrz__Pricelist__c=ccpricelist.id;
        acgrpList.ccrz__AccountGroup__c=ccaccgrp.id;
        insert acgrpList ;
        String storefrontName = ccrz.cc_CallContext.storefront;
          
        Test.startTest();
        returnResults = fp_ctrl_CatSiteMap.getAllowedPricelists(storefrontName ,ccaccgrp.id);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    
    static testmethod void baseURLTest(){
        fp_ctrl_CatSiteMap objCtrlSiteMap = new fp_ctrl_CatSiteMap();
        Map<String,Object> mapStrVsObj = new  Map<String,Object>();
        
        mapStrVsObj.put('Site_Secure_Domain__c', (Object)'https://www.test.com');
        ccrz.cc_CallContext.storeFrontSettings = mapStrVsObj;
        Test.startTest();
        String baseURL = objCtrlSiteMap.baseURL;
        String declarationXml = objCtrlSiteMap.declarationXml;// = '<?xml version="1.0" encoding="UTF-8"?>\n';
        Test.stopTest();
        System.assert(objCtrlSiteMap.baseURL != null);
    }
        
        
 }       
        
        
  /*  }
    static testmethod void getCartTest() {
        util.initCallContext();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCShipToController.getCart(ctx);
        Test.stopTest();

        System.assert(result != null);
  //      System.assert(result.success);
    }*/