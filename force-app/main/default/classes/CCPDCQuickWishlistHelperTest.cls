@isTest
public class CCPDCQuickWishlistHelperTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    public static String TEST_REQUEST = 'product-01,10\nproduct-na,20\n';
    public static String TEST_JSON_REQUEST = '[{"partNumber":"product-01","quantity":10},{"partNumber":"product-na","quantity":20}]';
    public static String TEST_JSON_REQUEST_2 = '[{"partNumber":"product-01","quantity":10}]';

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void getQuickOrderItemsTest() {
        util.initCallContext();
        List<CCPDCQuickWishlistHelper.QuickOrderItemWL> items = new List<CCPDCQuickWishlistHelper.QuickOrderItemWL>();
        CCPDCQuickWishlistHelper.QuickOrderItemWL item = new CCPDCQuickWishlistHelper.QuickOrderItemWL();
        item.partNumber = 'product-01';
        item.quantity = 10;
        items.add(item);
        item = new CCPDCQuickWishlistHelper.QuickOrderItemWL();
        item.partNumber = 'product-na';
        item.quantity = 20;
        items.add(item);

        List<CCPDCQuickWishlistHelper.QuickOrderItemWL> data = null;
        Test.startTest();
        data = CCPDCQuickWishlistHelper.getQuickOrderItemsWL(items);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(2, data.size());
    }

    static testmethod void getQuickOrderItemsConflictTest() {
        util.initCallContext();
        ccrz__E_Product__c prod1 = util.createProduct('DupPart', 'dup-part-1', 'Standard', util.STOREFRONT);
        insert prod1;
        ccrz__E_Product__c prod2 = util.createProduct('DupPart', 'dup-part-2', 'Standard', util.STOREFRONT);
        insert prod2;

        List<CCPDCQuickWishlistHelper.QuickOrderItemWL> items = new List<CCPDCQuickWishlistHelper.QuickOrderItemWL>();
        CCPDCQuickWishlistHelper.QuickOrderItemWL item = new CCPDCQuickWishlistHelper.QuickOrderItemWL();
        item.partNumber = 'product-01';
        item.quantity = 10;
        items.add(item);
        item = new CCPDCQuickWishlistHelper.QuickOrderItemWL();
        item.partNumber = 'DupPart';
        item.quantity = 20;
        items.add(item);

        List<CCPDCQuickWishlistHelper.QuickOrderItemWL> data = null;
        Test.startTest();
        data = CCPDCQuickWishlistHelper.getQuickOrderItemsWL(items);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(2, data.size());
    }

    static testmethod void validateTest() {
        util.initCallContext();
        List<CCPDCQuickWishlistHelper.QuickOrderItemWL> items = new List<CCPDCQuickWishlistHelper.QuickOrderItemWL>();
        CCPDCQuickWishlistHelper.QuickOrderItemWL item = new CCPDCQuickWishlistHelper.QuickOrderItemWL();
        item.sku = 'product-01';
        item.partNumber = 'product-01';
        item.quantity = 10;
        items.add(item);
        item = new CCPDCQuickWishlistHelper.QuickOrderItemWL();
        item.partNumber = 'product-na';
        item.quantity = 20;
        items.add(item);

        Boolean data = null;
        Test.startTest();
        data = CCPDCQuickWishlistHelper.validate(items);
        Test.stopTest();

        System.assert(data != null);
        System.assert(!data);
    }

    static testmethod void addToCartTest() {
        util.initCallContext();
        List<CCPDCQuickWishlistHelper.QuickOrderItemWL> items = new List<CCPDCQuickWishlistHelper.QuickOrderItemWL>();
        CCPDCQuickWishlistHelper.QuickOrderItemWL item = new CCPDCQuickWishlistHelper.QuickOrderItemWL();
        item.sku = 'product-01';
        item.partNumber = 'product-01';
        item.quantity = 10;
        items.add(item);

        String data = null;
        Test.startTest();
        data = CCPDCQuickWishlistHelper.addToCart(items);
        Test.stopTest();

        System.assert(data != null);
    }
    
    static testmethod void parseQuickOrderItemTest() {
        util.initCallContext();

        List<CCPDCQuickWishlistHelper.QuickOrderItemWL> data = null;
        Test.startTest();
        data = CCPDCQuickWishlistHelper.parseQuickOrderItem(TEST_JSON_REQUEST);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(2, data.size());
    }

    static testmethod void parseQuickOrderRequestTest() {
        util.initCallContext();

        List<CCPDCQuickWishlistHelper.QuickOrderItemWL> data = null;
        Test.startTest();
        data = CCPDCQuickWishlistHelper.parseQuickOrderRequest(TEST_REQUEST);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(2, data.size());
    }

    static testmethod void createQuickOrderRequestTest() {
        util.initCallContext();
        List<CCPDCQuickWishlistHelper.QuickOrderItemWL> items = new List<CCPDCQuickWishlistHelper.QuickOrderItemWL>();
        CCPDCQuickWishlistHelper.QuickOrderItemWL item = new CCPDCQuickWishlistHelper.QuickOrderItemWL();
        item.partNumber = 'product-01';
        item.quantity = 10;
        items.add(item);
        item = new CCPDCQuickWishlistHelper.QuickOrderItemWL();
        item.partNumber = 'product-na';
        item.quantity = 20;
        items.add(item);

        String data = null;
        Test.startTest();
        data = CCPDCQuickWishlistHelper.createQuickOrderRequest(items);
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(TEST_REQUEST, data);
    }

}