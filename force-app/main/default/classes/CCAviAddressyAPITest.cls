@isTest
public class CCAviAddressyAPITest {

    /*
     Test http error.
    */
    static testmethod void testFind500() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.FIND_500));

        Test.startTest();
        CCAviAddressyAPI.FindResponse findResponse = CCAviAddressyAPI.find('DefaultStore', '17022 King James Way');
        Test.stopTest();

        System.assert(!findResponse.success);
    }

    /*
     Test successful result from find api.
    */
    static testmethod void testFindSuccess() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.FIND_SUCCESS));

        Test.startTest();
        CCAviAddressyAPI.FindResponse findResponse = CCAviAddressyAPI.find('DefaultStore', '17022 King James Way', 'US|US|ENG|20877-MD-GAITHERSBURG--WAY-KING_JAMES--17022');
        Test.stopTest();

        System.assert(findResponse.success);
        System.assert(findResponse.addresses.size() > 0);
    }

    /*
     Test error result from find api.
    */
    static testmethod void testFindError() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.FIND_ERROR));

        Test.startTest();
        CCAviAddressyAPI.FindResponse findResponse = CCAviAddressyAPI.find('DefaultStore', '17022 King James Way');
        Test.stopTest();

        System.assert(!findResponse.success);
    }

    /*
     Test exception from find api.
    */
    static testmethod void testFindThrowException() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.FIND_THROWEXCEPTION));

        Test.startTest();
        CCAviAddressyAPI.FindResponse findResponse = CCAviAddressyAPI.find('DefaultStore', '17022 King James Way');
        Test.stopTest();

        System.assert(!findResponse.success);
    }

    /*
     Test unknown data from find api.
    */
    static testmethod void testFindUnknown() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.FIND_UNKNOWN));

        Test.startTest();
        CCAviAddressyAPI.FindResponse findResponse = CCAviAddressyAPI.find('DefaultStore', '17022 King James Way');
        Test.stopTest();

        System.assert(!findResponse.success);
    }

    /*
     Test http error.
    */
    static testmethod void testRetrieve500() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.RETRIEVE_500));

        Test.startTest();
        CCAviAddressyAPI.RetrieveResponse retrieveResponse = CCAviAddressyAPI.retrieveAddress('DefaultStore', 'US|US|B|V212153598');
        Test.stopTest();

        System.assert(!retrieveResponse.success);
    }

    /*
     Test successful result from retrieve api.
    */
    static testmethod void testRetrieveSuccess() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.RETRIEVE_SUCCESS));

        Test.startTest();
        CCAviAddressyAPI.RetrieveResponse retrieveResponse = CCAviAddressyAPI.retrieveAddress('DefaultStore', 'US|US|B|V212153598');
        Test.stopTest();

        System.assert(retrieveResponse.success);
    }

    /*
     Test error result from retrieve api.
    */
    static testmethod void testRetrieveError() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.RETRIEVE_ERROR));

        Test.startTest();
        CCAviAddressyAPI.RetrieveResponse retrieveResponse = CCAviAddressyAPI.retrieveAddress('DefaultStore', 'US|US|B|V212153598');
        Test.stopTest();

        System.assert(!retrieveResponse.success);
    }

    /*
     Test exception from retrieve api.
    */
    static testmethod void testRetrieveThrowException() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.RETRIEVE_THROWEXCEPTION));

        Test.startTest();
        CCAviAddressyAPI.RetrieveResponse retrieveResponse = CCAviAddressyAPI.retrieveAddress('DefaultStore', 'US|US|B|V212153598');
        Test.stopTest();

        System.assert(!retrieveResponse.success);
    }

    /*
     Test unknown data from retrieve api.
    */
    static testmethod void testRetrieveUnknown() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.RETRIEVE_UNKNOWN));

        Test.startTest();
        CCAviAddressyAPI.RetrieveResponse retrieveResponse = CCAviAddressyAPI.retrieveAddress('DefaultStore', 'US|US|B|V212153598');
        Test.stopTest();

        System.assert(!retrieveResponse.success);
    }

    /*
     Test successful validateCompleteAddress method.
    */
    static testmethod void testValidAddressSuccess() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.VALID_ADDRESS));

        Test.startTest();
        Boolean success = CCAviAddressyAPI.validateCompleteAddress('DefaultStore', '17022 King James Way GAITHERSBURG MD 20877-2235 UNITED STATES');
        Test.stopTest();

        System.assert(success);
    }

    /*
     Test un-successful validateCompleteAddress method.
    */
    static testmethod void testValidAddressFailure() {
        createAddressySettings('DefaultStore');
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.FIND_SUCCESS));

        Test.startTest();
        Boolean success = CCAviAddressyAPI.validateCompleteAddress('DefaultStore', '17022 King James Way GAITHERSBURG MD 20877-2235 UNITED STATES');
        Test.stopTest();

        System.assert(!success);
    }

    /*
     Initialize custom settings data.
    */
    public static void createAddressySettings(String storefront) {
        CCAviAddressySettings__c settings = new CCAviAddressySettings__c();
        settings.Name = storefront;
        settings.Key__c = 'AA11-AA11-AA11-AA11';
        settings.Find_End_Point__c = 'https://services.postcodeanywhere.co.uk/Capture/Interactive/Find/v1.00/json3ex.ws';
        settings.Retrieve_End_Point__c = 'https://services.postcodeanywhere.co.uk/Capture/Interactive/Retrieve/v1.00/json3ex.ws';
        insert settings;
    }

    /*
        Enum to tell mock class which data to be returned. 
    */
    public enum AddressyAPIMockResult {FIND_500, FIND_SUCCESS, FIND_ERROR, FIND_THROWEXCEPTION, FIND_UNKNOWN,
                                       RETRIEVE_500, RETRIEVE_SUCCESS, RETRIEVE_ERROR, RETRIEVE_THROWEXCEPTION, RETRIEVE_UNKNOWN,
                                       VALID_ADDRESS, RETRIEVE_SUCCESS2}

    public class AddressyAPIMockException extends Exception {}

    /*
        Mock class to imitate the API call to Addressy
    */
    public class AddressyAPIMock implements HttpCalloutMock {
        AddressyAPIMockResult returnResult {get; set;}

        public AddressyAPIMock(AddressyAPIMockResult result) {
            returnResult = result;
        }

        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {        
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');            
            res.setStatusCode(200);

            String responseBody = '';
            if (returnResult == AddressyAPIMockResult.FIND_500 || returnResult == AddressyAPIMockResult.RETRIEVE_500) {
                res.setStatusCode(500);
            }
            else if (returnResult == AddressyAPIMockResult.FIND_SUCCESS) {
                responseBody = '{"Items":[{"Id":"US|US|B|V212153598|17022","Type":"Address","Text":"17022 King James Way","Highlight":"0-5,6-10","Description":"Gaithersburg MD 20877"},{"Id":"US|US|ENG|20877-MD-GAITHERSBURG--WAY-KING_JAMES--17022","Type":"BuildingNumber","Text":"17022 King James Way Apt","Highlight":"0-5,6-10","Description":"Gaithersburg MD 20877 - 202 Addresses"},{"Id":"US|US|B|V212155733|17022","Type":"Address","Text":"17022 King James Way","Highlight":"0-5,6-10","Description":"Gaithersburg MD 20877"},{"Id":"US|US|B|Y123796206|17022","Type":"Address","Text":"17022 King Rd","Highlight":"0-5,6-10","Description":"Trenton MI 48183"},{"Id":"US|US|B|X207163041|17022","Type":"Address","Text":"17022 King Rd","Highlight":"0-5,6-10","Description":"Bowling Green OH 43402"},{"Id":"US|US|B|Y124188327|17022","Type":"Address","Text":"17022 King Rd","Highlight":"0-5,6-10","Description":"Mineral Point WI 53565"},{"Id":"US|US|B|Y224971860|17022","Type":"Address","Text":"17022 Kingfish Ln W","Highlight":"0-5,6-10","Description":"Summerland Key FL 33042"},{"Id":"US|US|B|W120690906|17022","Type":"Address","Text":"17022 King Rd","Highlight":"0-5,6-10","Description":"Mc Gregor IA 52157"},{"Id":"US|US|B|W120897944|17022","Type":"Address","Text":"17022 King Rd","Highlight":"0-5,6-10","Description":"Lawson MO 64062"},{"Id":"US|US|B|W212823503|17022","Type":"Address","Text":"17022 King Arthurs Ct","Highlight":"0-5,6-10","Description":"Newalla OK 74857"}]}';
            }
            else if (returnResult == AddressyAPIMockResult.VALID_ADDRESS) {
                responseBody = VALID_ADDRESS_STRING;
            }
            else if (returnResult == AddressyAPIMockResult.RETRIEVE_SUCCESS) {
                responseBody = RETRIEVE_SUCCESS_STRING;
            }
            else if (returnResult == AddressyAPIMockResult.RETRIEVE_SUCCESS2) {
                responseBody = RETRIEVE_SUCCESS_STRING2;
            }            
            else if (returnResult == AddressyAPIMockResult.FIND_ERROR || returnResult == AddressyAPIMockResult.RETRIEVE_ERROR) {
                responseBody = '{"Items":[{"Error":"2","Description":"Unknown key","Cause":"The key you are using to access the service was not found.","Resolution":"Please check that the key is correct. It should be in the form AA11-AA11-AA11-AA11."}]}';
            }
            else if (returnResult == AddressyAPIMockResult.FIND_THROWEXCEPTION || returnResult == AddressyAPIMockResult.RETRIEVE_THROWEXCEPTION) {
                throw new AddressyAPIMockException('Exception result');
            }
            else if (returnResult == AddressyAPIMockResult.FIND_UNKNOWN || returnResult == AddressyAPIMockResult.RETRIEVE_UNKNOWN) {
                responseBody = '{"foo":"bar"}';
            }
            res.setBody(responseBody);
            return res;
        }        
    }

    public static final String VALID_ADDRESS_STRING = '{"Items":[{"Id":"US|US|B|V212153598|17022","Type":"Address","Text":"17022 King James Way","Highlight":"0-5,6-10","Description":"Gaithersburg MD 20877"}]}';
    public static final String RETRIEVE_SUCCESS_STRING = '{"Items":[{"Id":"US|US|B|V212153598","DomesticId":"V212153598","Language":"ENG","LanguageAlternatives":"ENG","Department":"","Company":"","SubBuilding":"","BuildingNumber":"17022","BuildingName":"","SecondaryStreet":"","Street":"King James Way","Block":"","Neighbourhood":"","District":"","City":"Gaithersburg","Line1":"17022 King James Way","Line2":"","Line3":"","Line4":"","Line5":"","AdminAreaName":"Montgomery","AdminAreaCode":"031","Province":"MD","ProvinceName":"Maryland","ProvinceCode":"MD","PostalCode":"20877-2235","CountryName":"United States","CountryIso2":"US","CountryIso3":"USA","CountryIsoNumber":840,"SortingNumber1":"","SortingNumber2":"","Barcode":"","POBoxNumber":"","Label":"17022 King James Way GAITHERSBURG MD 20877-2235 UNITED STATES","Type":"Residential","DataLevel":"Range","Field1":"","Field2":"","Field3":"","Field4":"","Field5":"","Field6":"","Field7":"","Field8":"","Field9":"","Field10":"","Field11":"","Field12":"","Field13":"","Field14":"","Field15":"","Field16":"","Field17":"","Field18":"","Field19":"","Field20":""}]}';
    public static final String RETRIEVE_SUCCESS_STRING2 =  '{"Items":[{"Id":"US|US|B|Y214362453|5119","DomesticId":"Y214362453","Language":"ENG","LanguageAlternatives":"ENG","Department":"","Company":"","SubBuilding":"","BuildingNumber":"5119","BuildingName":"","SecondaryStreet":"","Street":"Highgrove Ln NW","Block":"","Neighbourhood":"","District":"","City":"Rochester","Line1":"5119 Highgrove Ln NW","Line2":"","Line3":"","Line4":"","Line5":"","AdminAreaName":"Olmsted","AdminAreaCode":"109","Province":"MN","ProvinceName":"Minnesota","ProvinceCode":"MN","PostalCode":"55901-2085","CountryName":"United States","CountryIso2":"US","CountryIso3":"USA","CountryIsoNumber":840,"SortingNumber1":"","SortingNumber2":"","Barcode":"","POBoxNumber":"","Label":"5119 Highgrove Ln NW ROCHESTER MN 55901-2085 UNITED STATES","Type":"Residential","DataLevel":"Range","Field1":"","Field2":"","Field3":"","Field4":"","Field5":"","Field6":"","Field7":"","Field8":"","Field9":"","Field10":"","Field11":"","Field12":"","Field13":"","Field14":"","Field15":"","Field16":"","Field17":"","Field18":"","Field19":"","Field20":""}]}';
}