@isTest
public class CCAviCartDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void getCartForQuoteTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCAviCartDAO.getCartForQuote(cart.ccrz__EncryptedId__c);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }

    static testmethod void getCartItemByIdTest(){
        util.initCallContext();
        ccrz__E_CartItem__c cartItem = util.getCartItem();

        ccrz__E_CartItem__c returnedCartItem = null;

        Test.startTest();
        returnedCartItem = CCAviCartDAO.getCartItemById(cartItem.Id);
        Test.stopTest();

        System.assert(returnedCartItem != null);
        System.assertEquals(cartItem.Id, returnedCartItem.Id);
    }

    static testmethod void getContactInfoInCartTest(){
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz__E_Cart__c returnedCart = null;

        Test.startTest();
        returnedCart = CCAviCartDAO.getContactInfoInCart(cart.ccrz__EncryptedId__c);
        Test.stopTest();

        System.assert(returnedCart != null);
        System.assertEquals(cart.Id, returnedCart.Id);
    }
}