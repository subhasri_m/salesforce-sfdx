public class PowerBIGetAccountGUID {
    public string AccountId { get; set; }
    public Account acc { get; set; }
    
    /**
    * Generic constructor
    */
    public PowerBIGetAccountGUID (ApexPages.StandardController controller) {  
        AccountId = ApexPages.CurrentPage().getparameters().get('account');
        acc = [SELECT id, name, AccountNumber FROM Account WHERE id =: AccountId];
    }
}