global with sharing class CCPDCCheckoutCCPaymentController {
    public String baseURL {get; set;}
    public String path {get; set;}

    global CCPDCCheckoutCCPaymentController() {

        String store = ccrz.cc_CallContext.storefront;
        CCAviCardConnectSettings__c settings = CCAviCardConnectSettings__c.getInstance(store);
        this.baseURL = settings.Base_URL__c;
        this.path = settings.Hosted_Tokenizer_Path__c;
    }


    @RemoteAction
    global static ccrz.cc_RemoteActionResult isStoredPayment(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);

            String storefront = ccrz.cc_CallContext.storefront;
            String token = (String)input.get('token');
            Id accountId = ccrz.cc_CallContext.effAccountId;
            CCAviCardConnectAPI.CardConnectResponse authResponse = CCAviCardConnectAPI.zeroAuthorization(storefront, (String)input.get('token'), (String)input.get('expiration'), (String)input.get('verificationCode'), (String)input.get('name'));
            List<ccrz__E_StoredPayment__c> findCard = [SELECT Id FROM ccrz__E_StoredPayment__c WHERE ccrz__Token__c  =:token AND ccrz__EffectiveAccountID__c=:accountId AND CreatedById =:UserInfo.getUserId()];


            if (authResponse.success && (findCard == null || findCard.isEmpty())) {
                CCAviPageUtils.buildResponseData(response, true, new Map<String,Object>{'input' => authResponse}
                );

            } else {

                if(findCard!=null){
                    if(authResponse != null &&  authResponse.message != null && authResponse.message == 'Non-numeric CVV') { authResponse.message = 'CC_PDC_CC_CVVInvalid';
                        CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'input' => authResponse, 'message' => 'CC_PDC_CC_CVVInvalid'}
                        );
                    } else {

                        CCAviPageUtils.buildResponseData(response, false,  new Map<String,Object>{'input' => authResponse, 'message' => 'Card Already Exists'}
                        );
                    }
                }
                else {
                    CCAviPageUtils.buildResponseData(response, true, new Map<String,Object>{'input' => authResponse}
                    );

                }

            }


            //response.success = authResponse.success;
            //Map<String,Object> result = new Map<String,Object>();
            //result.put('input', authResponse);
            //response.data = result;


        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                    new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }


    @RemoteAction
    global static ccrz.cc_RemoteActionResult validateCard(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);




            String storefront = ccrz.cc_CallContext.storefront;

            CCAviCardConnectAPI.CardConnectResponse authResponse = CCAviCardConnectAPI.zeroAuthorization(storefront, (String)input.get('token'), (String)input.get('expiration'), (String)input.get('verificationCode'), (String)input.get('name'));

            response.success = authResponse.success;
            Map<String,Object> result = new Map<String,Object>();

            if(authResponse != null &&  authResponse.message != null && authResponse.message == 'Non-numeric CVV') { authResponse.message = 'CC_PDC_CC_CVVInvalid';
            }
            result.put('input', authResponse);
            response.data = result;
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }

    @RemoteAction
    global static boolean isSavedCard(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);

        String storefront = ccrz.cc_CallContext.storefront;
        String token = (String) input.get('token');
        Id accountId = ccrz.cc_CallContext.effAccountId;
        List<ccrz__E_StoredPayment__c> findCard = [SELECT Id FROM ccrz__E_StoredPayment__c WHERE ccrz__Token__c = :token AND ccrz__EffectiveAccountID__c = :accountId AND CreatedById = :UserInfo.getUserId()];

        if (findCard == null || findCard.isEmpty()) {
            return true; } else { return false;
        }

    }

}