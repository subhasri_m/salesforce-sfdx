global with sharing class CCPDCContactUsFormController {

	@RemoteAction
	global static ccrz.cc_RemoteActionResult createContactUsMessage(ccrz.cc_RemoteActionContext ctx, String data){

		ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
		try {
			Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
			CC_PDC_ContactUs_Message__c contactus_message = new CC_PDC_ContactUs_Message__c();
			contactus_message.Company_Name__c = (String)input.get('companyName');
			contactus_message.Contact_Email__c = (String)input.get('contactEmail');
			contactus_message.Contact_Name__c = (String)input.get('contactName');
			contactus_message.Contact_Phone__c = (String)input.get('phoneNumber');
			contactus_message.Contact_Reason__c = (String)input.get('contactReason');
			contactus_message.Message__c = (String)input.get('message');
			contactus_message.Address1__c = (String)input.get('Address1');
			contactus_message.Address2__c = (String)input.get('Address2');
			contactus_message.City__c = (String)input.get('City');
			contactus_message.State__c = (String)input.get('contactusstate');
			contactus_message.ZipCode__c = (String)input.get('ZipCode');
			contactus_message.Country__c = (String)input.get('Country');
			
			insert contactus_message;
			CCAviPageUtils.buildResponseData(response, true, new Map<String, Object>{'record' => contactus_message});
			
		} 
		catch (Exception e) {
			CCAviPageUtils.buildResponseData(response, false,
				new Map<String, Object>{'error' => e.getMessage(), 'cause' => e.getCause(), 'lineno' => e.getLineNumber(), 'stack' => e.getStackTraceString()}
			);
		}

		return response;


	}

	@RemoteAction
	global static Map<String, String> getAccountInfo(ccrz.cc_RemoteActionContext ctx){
        ccrz.cc_CallContext.initRemoteContext(new ccrz.cc_RemoteActionContext());
		Map<String, String> returnedData  = new Map<String, String>();

        Id theAccountId = ccrz.cc_CallContext.effAccountId;
        Contact theContact = ccrz.cc_CallContext.currContact;
        if (theAccountId != null && theContact != null) {
        	Account theAccount = CCPDCAccountDAO.getAccount(theAccountId);
        	theContact = [SELECT Name, Email FROM Contact WHERE Id = :theContact.Id];


			if (theAccount != null) {
				returnedData.put('account', theAccount.Name);
			}
			if (theContact != null) {
				returnedData.put('name', theContact.Name);
				returnedData.put('email', theContact.Email);
			}
		}

		return returnedData;
	}

/*
	@RemoteAction
	global static List<String> getAccountInfo(String encryptedCartID){
		ccrz__E_Cart__c cartContactInfo = CCAviCartDAO.getContactInfoInCart(encryptedCartID);

		List<String> returnedData  = new List<String>();

		returnedData.add((String)cartContactInfo.ccrz__Account__r.Name);
		returnedData.add((String)cartContactInfo.ccrz__Contact__r.Name);
		returnedData.add((String)cartContactInfo.ccrz__Contact__r.Email);

		return returnedData;
	}

	@RemoteAction
	global static String createCart() {
		return CCAviCartManager.createCart();
	}
*/
}