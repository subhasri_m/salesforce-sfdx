global without sharing class CCPDCHeaderController {
    public Boolean isNotPDCLogo {get;set;} // if true, not use PDC Logo
    public String EffAccountid {get;set;}
    public String ipAddress {get;set;}
    public Boolean isLocalDeliveryAllowed {get;set;}
    public CCPDCHeaderController() {
        isNotPDCLogo = false; 
        
        if (UserInfo.getUserType() != 'Guest') {
            EffAccountid = ccrz.cc_CallContext.effAccountId; 
        }
        if (EffAccountid != null){
            Account acc = CCPDCAccountDAO.getAccount(EffAccountid);
            System.debug('acc==>'+acc);
            // assigning NAM logo if account belongs to specific National Account Group
            if(String.isNotBlank(acc.National_Account_Group__c)) {
                if (Label.NAM_logo_accounts_group_PDC.contains(acc.National_Account_Group__c)){
                    isNotPDCLogo= true;
                } 
            }
              isLocalDeliveryAllowed = acc.Local_Delivery_Allowed__c;
        }  
        
        ipAddress = ApexPages.currentPage().getHeaders().get('User-Client-IP');        
        // True-Client-IP has the value when the request is coming via the caching integration.
        if (ipAddress == '' || ipAddress == null) {
            ipAddress = ApexPages.currentPage().getHeaders().get('True-Client-IP'); 
        }
        // X-Salesforce-SIP has the value when no caching integration or via secure URL.
        if (ipAddress == '' || ipAddress == null) {
            ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        } // get IP address when no caching (sandbox, dev, secure urls)
        if (ipAddress == '' || ipAddress == null) {
            ipAddress = ApexPages.currentPage().getHeaders().get('X-Forwarded-For');
        } // get IP address from standard header if proxy in use
              
    }
    
    global static string getCoveoSearchToken(){
        return CoveoFPProductsController.getCoveoSearchToken();
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult getFreeShippingInfo(ccrz.cc_RemoteActionContext ctx, String accountId){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        try{
            //Id accountId = ccrz.cc_CallContext.effAccountId;
            String cartId = ccrz.cc_CallContext.currCartId;
            Map<String,Decimal> freeShippingValuesOnAccount = new Map<String,Decimal>();
            Map<String,Object> resultData = new Map<String,Object>();
            
            
            Map<String,Object> currentCartValue = CCPDCHeaderController.getCurrentCartShippingValues(cartId,accountId);
            
            freeShippingValuesOnAccount= CCPDCUtilShippingLogic.currentAccountFreeFeightValues(accountId);
            Decimal currentFreeEligibleValue = CCPDCUtilShippingLogic.currentQualifyFeightValue(cartId);
            Decimal accountFreeLTLShippingAmount = freeShippingValuesOnAccount.get('LTLFreightThresholdValue');
            
            resultData.put('accountFreeShippingAmount',accountFreeLTLShippingAmount);
            resultData.put('currentFreeEligibleValue',currentFreeEligibleValue);
            
            resultData.putAll(currentCartValue);
            
            
            
            CCAviPageUtils.buildResponseData(response,true,resultData);
        }        
        catch(Exception e){
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        
        
        return response;
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getHeaderInfoFP(ccrz.cc_RemoteActionContext ctx){
        ccrz.cc_RemoteActionResult response = ccrz.cc_CallContext.init(ctx);
        try{
            String cartId  = ccrz.cc_CallContext.currCartId;
            ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(cartID);
            Integer cartItemCount = 0;
            if(cart != null && cart.ccrz__E_CartItems__r != null) {
            	 cartItemCount = cart.ccrz__E_CartItems__r.size();
            }
            Map<String, Object> cartheaderResponse = new Map<String, Object>();
            cartheaderResponse.put('cartItemCount',cartItemCount);
            cartheaderResponse.put('cartTotal','0');
            cartheaderResponse.put('cartId',cartId);
            CCAviPageUtils.buildResponseData(response,true,cartheaderResponse);
        }
        catch(Exception e){
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response;
    }         
    
    public static Map<String,Decimal> getCurrentCartShippingValues(String cartId,Id accountId){
        Map<String,Decimal> currentCartValues = new Map<String,Decimal>();        
        ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(cartId);        
        Integer totalProdsQty = 0;
        Decimal cartSubtotal = 0;
        Decimal cartOrderSubtotal = 0;
        List<ccrz__E_Product__c> productsInCart = new List<ccrz__E_Product__c>();
        List<String> productSkus = new List<String>();
        List<CCRZ__E_CartItem__c> itemsToUpdate = new List<CCRZ__E_CartItem__c>();
        Map<String, Integer> skuQtyInCart = new Map<String,Integer>();
        
        for(CCRZ__E_CartItem__c item : cart.ccrz__E_CartItems__r){
            productSkus.add(item.ccrz__Product__r.ccrz__SKU__c);
            if(!skuQtyInCart.containsKey(item.ccrz__Product__r.ccrz__SKU__c)){
                skuQtyInCart.put(item.ccrz__Product__r.ccrz__SKU__c, Integer.valueOf(item.ccrz__Quantity__c));    
            }else{
                Integer qty = skuQtyInCart.get(item.ccrz__Product__r.ccrz__SKU__c);
                skuQtyInCart.put(item.ccrz__Product__r.ccrz__SKU__c, qty+Integer.valueOf(item.ccrz__Quantity__c));
            }
            
        }
        Map<String, Integer> prodAndInv = new Map<String, Integer>();
        Map<String, Integer> prodAndInvbackUp = new Map<String, Integer>();
        System.debug('productSkus==>'+productSkus);
        Map<String, CCPDCInventoryHelper.Inventory> prodInventory = CCPDCInventoryHelper.getInventory(productSkus,accountId);
        System.debug('prodInventory==>'+prodInventory);
        if((!prodInventory.isEmpty() && prodInventory != null) || Test.isRunningTest()){
            for(String sku : prodInventory.keySet()){
                CCPDCInventoryHelper.Inventory currentProduct = prodInventory.get(sku);
                prodAndInv.put(sku,currentProduct.totalQuantity);
                
            }
            prodAndInvbackUp = prodAndInv.clone();
            for(CCRZ__E_CartItem__c item : cart.ccrz__E_CartItems__r){
                CCPDCInventoryHelper.Inventory currentProduct = prodInventory.get(String.valueOf(item.ccrz__Product__r.ccrz__SKU__c));
                Integer aviQty = prodAndInv.get(item.ccrz__Product__r.ccrz__SKU__c);
                if(currentProduct != null){
                    if(item.ccrz__Quantity__c > aviQty && aviQty != 0){
                        totalProdsQty += aviQty;
                        prodAndInv.put(item.ccrz__Product__r.ccrz__SKU__c,0);
                        item.CC_FP_QTY_Shipping__c = aviQty;
                        item.CC_FP_QTY_Backorder__c = item.ccrz__Quantity__c - aviQty;
                        item.CC_FP_Item_Order_Subtotal__c = item.ccrz__Quantity__c * item.ccrz__Price__c;
                        item.ccrz__SubAmount__c = aviQty * item.ccrz__Price__c; 
                        itemsToUpdate.add(item);
                        
                    } else if(aviQty == 0){
                        item.CC_FP_QTY_Shipping__c = aviQty;
                        item.CC_FP_QTY_Backorder__c = item.ccrz__Quantity__c;
                        item.CC_FP_Item_Order_Subtotal__c = item.ccrz__Quantity__c * item.ccrz__Price__c;
                        itemsToUpdate.add(item);
                        
                    } else {
                        if(item.CC_FP_QTY_Shipping__c == null){
                            item.CC_FP_QTY_Shipping__c = item.ccrz__Quantity__c;
                            totalProdsQty += Integer.valueOf(item.ccrz__Quantity__c);
                            item.CC_FP_Item_Order_Subtotal__c = item.ccrz__Quantity__c * item.ccrz__Price__c;
                            prodAndInv.put(item.ccrz__Product__r.ccrz__SKU__c,aviQty-Integer.valueOf(item.ccrz__Quantity__c));
                            itemsToUpdate.add(item);
                        } else {
                            if(item.ccrz__Quantity__c < aviQty){
                                item.CC_FP_QTY_Shipping__c = item.ccrz__Quantity__c;
                            }
                            totalProdsQty += Integer.valueOf(item.CC_FP_QTY_Shipping__c);
                            prodAndInv.put(item.ccrz__Product__r.ccrz__SKU__c,aviQty-Integer.valueOf(item.ccrz__Quantity__c));
                            item.CC_FP_Item_Order_Subtotal__c = item.ccrz__Quantity__c * item.ccrz__Price__c;
                            itemsToUpdate.add(item);
                        }
                        
                        //cartSubtotal += item.ccrz__ItemTotal__c;
                    } 
                } else {
                    item.CC_FP_QTY_Backorder__c = item.ccrz__Quantity__c;
                    item.CC_FP_QTY_Shipping__c = 0;
                    item.ccrz__SubAmount__c = 0;
                    item.CC_FP_Item_Order_Subtotal__c = item.ccrz__Quantity__c * item.ccrz__Price__c;
                    itemsToUpdate.add(item);                       
                    
                }
                
                
            }
            
            update itemsToUpdate;
            ccrz__E_Cart__c updatedcart = CCPDCCartDAO.getCart(cartId);
            for(CCRZ__E_CartItem__c item : updatedcart.ccrz__E_CartItems__r){
                cartSubtotal += item.CC_FP_QTY_Shipping__c * item.ccrz__Price__c;
                if(item.CC_FP_Item_Order_Subtotal__c != null){
                    cartOrderSubtotal += item.CC_FP_Item_Order_Subtotal__c; 
                }
                
            }
            
            cart.CC_FP_Order_Subtotal__c = cartOrderSubtotal;
            cart.CC_FP_Shipping_Total_Qty__c = totalProdsQty;  
            update cart;          
        } else {
            for(CCRZ__E_CartItem__c item : cart.ccrz__E_CartItems__r){
                item.CC_FP_QTY_Backorder__c = item.ccrz__Quantity__c;
                item.CC_FP_QTY_Shipping__c = 0;
                item.ccrz__SubAmount__c = item.ccrz__Quantity__c * item.ccrz__Price__c;
                item.CC_FP_Item_Order_Subtotal__c = item.ccrz__Quantity__c * item.ccrz__Price__c;
                itemsToUpdate.add(item);
                prodAndInvbackUp.put(item.ccrz__Product__r.ccrz__SKU__c,0);
            }
            update itemsToUpdate;
            cart.CC_FP_Order_Subtotal__c = 0;
            cart.CC_FP_Shipping_Total_Qty__c = 0; 
            update cart;
            
        }
        
        Integer qtyChecker = 0;
        for(String sku: skuQtyInCart.keySet()){
            if (skuQtyInCart.get(sku)!= null && prodAndInvbackUp.get(sku)!=null)
                qtyChecker += Math.min(skuQtyInCart.get(sku),prodAndInvbackUp.get(sku));
        }
        if(qtyChecker != totalProdsQty){
            totalProdsQty = qtyChecker;
        }
        currentCartValues.put('shipTotalQty',totalProdsQty);
        currentCartValues.put('cartSubtotal',cartSubtotal);        
        return currentCartValues; 
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult logSearchText(ccrz.cc_RemoteActionContext ctx, String searchText){       
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx);
        try {
            Id contactId = ccrz.cc_CallContext.currContact.Id;
            SearchText__c search = new SearchText__c();
            search.SearchTerm__c = searchText;
            search.Contact__c = contactId;
            insert search;
            res.success = true;
        }
        catch(Exception e){
            CCAviPageUtils.buildResponseData(res, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return res;
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getNearestFleetPrideBranch(ccrz.cc_RemoteActionContext ctx, string ipAddress){       
        ccrz.cc_RemoteActionResult response = ccrz.cc_CallContext.init(ctx);
        try {
            //-- Added on 14/09/2020
            String zipCode = '';
            String fid = '';
            Account acc; 
            String preferredLoc;
            String shipFromLoc; 
            Boolean prfrdSameAsShpfrm = true; // if preferredLoc == shipFromLoc
            if(UserInfo.getUserType() != 'Guest'){
                acc = CCPDCAccountDAO.getAccount(ccrz.cc_CallContext.effAccountId);
                preferredLoc = acc.CC_FP_Preferred_Ship_From_Location__c;
                shipFromLoc = acc.Ship_from_Location__c;
                System.debug('preferredLoc==>'+preferredLoc);
                System.debug('shipFromLoc==>'+shipFromLoc);
                
                if(!String.isBlank(preferredLoc)){
                    Location__c loc = [Select Id,Name,Location__c,ZipCode__c
                                       FROM Location__c
                                       WHERE Location__c = :preferredLoc LIMIT 1
                                      ];
                    if(loc.ZipCode__c != null && loc.ZipCode__c.length() >= 5){
                        zipCode = loc.ZipCode__c.subString(0,5);
                        fid = loc.Location__c;
                    }
                }
                else if(!String.isBlank(shipFromLoc)){
                    Location__c loc = [Select Id,Name,Location__c,ZipCode__c
                                       FROM Location__c
                                       WHERE Location__c = :shipFromLoc LIMIT 1
                                      ];
                    if(loc.ZipCode__c != null && loc.ZipCode__c.length() >= 5){
                        zipCode = loc.ZipCode__c.subString(0,5);
                        fid = loc.Location__c;
                    }
                }
            }
            //--End
            System.debug('zipCode==>'+zipCode);
            System.debug('fid==>'+fid);
            if (!String.isBlank(preferredLoc) && preferredLoc != shipFromLoc){
            	prfrdSameAsShpfrm = false; // local delivery not allowed    
            }
            Map<String,Object> locationData = new Map<String,Object>();
            CCFPLocationRioSeoAPI.Location location = CCFPLocationRioSeoAPI.getNearestFleetPrideBranch(ipAddress,zipCode,fid, prfrdSameAsShpfrm); //  passing empty zipcode    
		     //==call future method to update Account
            if(String.isBlank(preferredLoc) && !String.isBlank(shipFromLoc)){
                updatePreferredLocOnAccount(acc.Id,shipFromLoc);
            }
            CCFPLocationRioSeoAPI.LocationWrapper locationWrapper = new CCFPLocationRioSeoAPI.LocationWrapper();
            locationWrapper.loclist = new List<CCFPLocationRioSeoAPI.Location>();
            locationWrapper.loclist.add(location);
            processLocationData(locationWrapper.loclist);
             Map<String, Object> shipTo = new Map<String, Object>();
            if(CCRZ.cc_CallContext.isGuest == false) {
            	shipTo = GetShipToInformation(ccrz.cc_CallContext.currCartId, ccrz.cc_CallContext.effAccountId);
            }
            locationData.put('location', location);           
            CCAviPageUtils.buildResponseData(response,true,new Map<String, Object>{'location' => location,'shipTo' => shipTo});
        }
        catch(Exception e){
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response;
    }
    
    private static Map<String, Object> GetShipToInformation(string currCartId, string effAccountId ) {
        	ccrz__E_Cart__c cart = null;
            Map<String, Object> shipTo = new Map<String, Object>();
            cart = CCPDCCartDAO.getCartShipTo(currCartId);
           //== FEC-1257 (Added on 03/11/2020)	
            if (cart == null || cart.ccrz__ShipTo__c == null)
            {
                // Here is when user created a new cart, either his older cart delete or he place the cart to order
                
                Account myAccount = [SELECT BillingStreet,BillingCity,BillingState,BillingPostalCode, BillingCountry,ShippingCountry,ShippingPostalCode,ShippingStreet,ShippingCity,ShippingState,Ship_from_Location__c FROM Account 
                                     Where Id =:effAccountId Limit 1];                              
                shipTo.put('addressFirstline', myAccount.ShippingStreet); 
                shipTo.put('city',  myAccount.ShippingCity);
                shipTo.put('country', myAccount.ShippingCountry); 
                shipTo.put('postalCode', myAccount.ShippingPostalCode); 
                shipTo.put('state', myAccount.ShippingState); 
            } else if(cart != null && cart.ccrz__ShipTo__r != null) {
                
                    shipTo.put('addressFirstline', cart.ccrz__ShipTo__r.ccrz__AddressFirstline__c); 
                    shipTo.put('city', cart.ccrz__ShipTo__r.ccrz__City__c);
                    shipTo.put('country', cart.ccrz__ShipTo__r.ccrz__Country__c); 
                    shipTo.put('postalCode', cart.ccrz__ShipTo__r.ccrz__PostalCode__c);
                    shipTo.put('state', cart.ccrz__ShipTo__r.ccrz__State__c); 
                
            }
        return shipTo;
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getFleetPrideBranches(ccrz.cc_RemoteActionContext ctx, string zipCode){       
        ccrz.cc_RemoteActionResult response = ccrz.cc_CallContext.init(ctx);
        try {
            Map<String,Object> locationData = new Map<String,Object>();
            CCFPLocationRioSeoAPI.LocationWrapper locationWrapper = CCFPLocationRioSeoAPI.getBranchLocations('',zipCode, true); //  passing empty zipcode // passing prfrdSameAsShpfrm 
            processLocationData(locationWrapper.loclist);
            locationData.put('location', locationWrapper);           
            CCAviPageUtils.buildResponseData(response,true,locationData);
        }
        catch(Exception e){
            System.debug('Error==>'+e);
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response;
    }
    
    @TestVisible
    private static void processLocationData(List<CCFPLocationRioSeoAPI.Location> locationList)
    {
        Map<String,String> mapDayStrVsVal = new Map<String,String>();
        System.debug('locationList==>'+locationList);
        Map<String,List<CCFPLocationRioSeoAPI.operationHour>> mapDayLstVsVal = new Map<String,List<CCFPLocationRioSeoAPI.operationHour>>();
        for(integer i=0; i<locationList.size(); i++){
            String todayDay = DateTime.now().format('EEEE');
            mapDayStrVsVal.put('SundayStr',locationList[i].primaryHours.days.SundayStr);
            mapDayStrVsVal.put('MondayStr',locationList[i].primaryHours.days.MondayStr);
            mapDayStrVsVal.put('TuesdayStr',locationList[i].primaryHours.days.TuesdayStr);
            mapDayStrVsVal.put('WednesdayStr',locationList[i].primaryHours.days.WednesdayStr);
            mapDayStrVsVal.put('ThursdayStr',locationList[i].primaryHours.days.ThursdayStr);
            mapDayStrVsVal.put('FridayStr',locationList[i].primaryHours.days.FridayStr);
            mapDayStrVsVal.put('SaturdayStr',locationList[i].primaryHours.days.SaturdayStr);
            
            mapDayLstVsVal.put('Sunday', locationList[i].primaryHours.days.Sunday);
            mapDayLstVsVal.put('Monday', locationList[i].primaryHours.days.Monday);
            mapDayLstVsVal.put('Tuesday', locationList[i].primaryHours.days.Tuesday);
            mapDayLstVsVal.put('Wednesday',locationList[i].primaryHours.days.Wednesday);
            mapDayLstVsVal.put('Thursday', locationList[i].primaryHours.days.Thursday);
            mapDayLstVsVal.put('Friday', locationList[i].primaryHours.days.Friday);
            mapDayLstVsVal.put('Saturday', locationList[i].primaryHours.days.Saturday);
            
            String todayDayInclude = todayDay;
            Boolean isClosedToday = False;
            String hoursOfopenMsg = '';
            Integer incrementDayByOne = 1;
            while(mapDayStrVsVal.get(todayDayInclude + 'Str') == 'Closed' && incrementDayByOne < 8){
                isClosedToday = True;
                todayDayInclude = DateTime.now().addDays(incrementDayByOne).format('EEEE') ;
                incrementDayByOne++;
            }
            
            String openHrsTodisplay;
            Integer openHrs = Integer.valueOf(mapDayLstVsVal.get(todayDayInclude)[0].close.split(':')[0]);
            if(openHrs > 12){
                openHrsTodisplay = (openHrs - 12) + ':' + mapDayLstVsVal.get(todayDayInclude)[0].close.split(':')[1] + 'pm';
            }
            else if(openHrs == 12){
                openHrsTodisplay = (12) + ':' + mapDayLstVsVal.get(todayDayInclude)[0].close.split(':')[1] + 'pm';
            }
            else{
                openHrsTodisplay = mapDayLstVsVal.get(todayDayInclude)[0].close + 'am';
            }
            
            if(isClosedToday){
                hoursOfopenMsg = 'Closed until ' + mapDayLstVsVal.get(todayDayInclude)[0].open + ' am on ' + todayDayInclude;
            }
            else{
                hoursOfopenMsg = 'Open until ' + openHrsTodisplay;
            }
            
            locationList[i].hoursOfopenMsg = hoursOfopenMsg;
            locationList[i].isClosedToday = isClosedToday;
        }
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult updateCartFPLocation(ccrz.cc_RemoteActionContext ctx){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        String cartEncId = ccrz.cc_CallContext.currCartId;  
        Boolean prfrdSameAsShpfrm = true; // flag to hide local based on prefferedloc and shipfromloc
        System.debug('cartEncId==>'+cartEncId);
        try{
           
        if (cartEncId !=null){
            ccrz__E_Cart__c thecart = CCPDCCartDAO.getCart(cartEncId);
            System.debug('thecart==>'+thecart);
            Account acc = CCPDCAccountDAO.getAccount(ccrz.cc_CallContext.effAccountId);
            System.debug('acc==>'+acc);
            String preferredLoc = acc.CC_FP_Preferred_Ship_From_Location__c;
            String updatedBranchFID = ccrz.cc_CallContext.currPageParameters.get('branchLoc');
            System.debug('updatedBranchFID==>'+updatedBranchFID);
            System.debug('preferredLoc==>'+preferredLoc);
            if((thecart.CC_FP_Location__c == null) && !String.isBlank(preferredLoc)){
                //String locCode = thecart.ccrz__Contact__r.Account.Ship_from_Location__c;
                Location__c loc = [Select Id,Name,Location__c
                                   FROM Location__c
                                   WHERE Location__c = :preferredLoc LIMIT 1
                                  ];
                thecart.CC_FP_Location__c = loc.id;
                update thecart;
            }
            else if(!String.isBlank(updatedBranchFID)){           
                if(thecart.CC_FP_Location__r.Location__c != updatedBranchFID){
                    Location__c loc = [Select Id,Name,Location__c,ZipCode__c
                                       FROM Location__c
                                       WHERE Location__c = :updatedBranchFID LIMIT 1
                                      ];
                    
                    thecart.CC_FP_Location__c = loc.Id;
                    update thecart;
                    String accId = ccrz.cc_CallContext.effAccountId;
                    acc.CC_FP_Preferred_Ship_From_Location__c = loc.Location__c;
                    update acc;
                }
            }else{
                Location__c loc = [Select Id,Name,Location__c,ZipCode__c
                                       FROM Location__c
                                       WHERE Location__c = :updatedBranchFID LIMIT 1
                                      ];
                String accId = ccrz.cc_CallContext.effAccountId;
                acc.CC_FP_Preferred_Ship_From_Location__c = loc.Location__c;
                update acc;
                
            }
            if(UserInfo.getUserType() != 'Guest'){
                if(acc.Ship_from_Location__c != updatedBranchFID){
                    setPickUpAsDOCIs(cartEncId); // update pickup as the delivery_option for all cartItems
                    prfrdSameAsShpfrm=false;
                } 
            }
            CCAviPageUtils.buildResponseData(response, true, new Map<String, Object>{'prfrdSameAsShpfrm' => prfrdSameAsShpfrm});
        }
        }
        catch(Exception e){
            System.debug('Exception in UpdatecartFPLocation==>'+e);
        }
        return response;
        
    }  
    
    @future
    public static void updatePreferredLocOnAccount(Id accId, String shipFromLoc){
        Account acc = new Account(Id = accId, CC_FP_Preferred_Ship_From_Location__c = shipFromLoc);
        update acc;
    }
    
    public static void setPickUpAsDOCIs(string encryptedId){       
        if(encryptedId != null){
            ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(encryptedId);
            for ( ccrz__E_CartItem__c cartItem : cart.ccrz__E_CartItems__r){
                cartItem.FulFillMentType__c = 'PickUp'; // all cartItems will have PickUp as DO (Delivery Option/fulfilment type)
            }
            update cart.ccrz__E_CartItems__r;
        }            
    }
     
}