@isTest
public class Test_Handler_RA_Trigger {
    @testSetup
    public static void testData(){
        User RVP = new User(alias = 'RVP', email='RVP2XXX@testorg.com',
                            emailencodingkey='UTF-8', lastname='RVP', languagelocalekey='en_US',
                            Salesman_Number__c = '3-88888',
                            localesidkey='en_US',
                            timezonesidkey='America/Los_Angeles', 
                            username='RVPXXXfp@testorg.com', profileid = '00e40000000x3dL');
        insert RVP;
        User Manager = new User(alias = 'Manager', email='Manager1XXX@testorg.com',
                                emailencodingkey='UTF-8', lastname='Manager', languagelocalekey='en_US',
                                Salesman_Number__c = '3-88888',
                                localesidkey='en_US',
                                timezonesidkey='America/Los_Angeles', 
                                username='ManagerXXXfp@testorg.com', profileid = '00e40000000x3e4',ManagerId=RVP.id);
        insert Manager;
        User TeamMember = new User(alias = 'tm', email='teamMemberXXX@testorg.com',
                                   emailencodingkey='UTF-8', lastname='TeamMember', languagelocalekey='en_US',
                                   Salesman_Number__c = '3-888818',
                                   localesidkey='en_US',
                                   timezonesidkey='America/Los_Angeles', 
                                   username='teamMemberXXXfp@testorg.com', profileid = '00e400000017YyK', ManagerId=Manager.id);
        insert TeamMember;
        Account acc = new Account(Name = 'testAcc',ownerId=TeamMember.Id);
        Insert acc;
        Ride_Along__c ra = new Ride_Along__c(team_member__c = TeamMember.Id,Reviewed_my_Feedback__c=false,Sales_Call_Complete__c=false);
        Insert ra;          
    }
    public static testmethod  void createRideManager(){
        User Manager = [Select id , name from User where lastname='Manager' limit 1 ];
        User TeamMember = [Select id , name from User where lastname='TeamMember' limit 1 ];
        //System.debug('TeamMember ' + TeamMember);
        Ride_Along__c ra = new Ride_Along__c(team_member__c = TeamMember.Id,Reviewed_my_Feedback__c=false,Sales_Call_Complete__c=false);
        Insert ra;          
        
    }
    public static testmethod  void updateRideTeammember(){
        User TeamMember = [Select id , name from User where lastname='TeamMember' limit 1 ];
        Ride_Along__c ra = [Select team_member__c,Reviewed_my_Feedback__c,Sales_Call_Complete__c from Ride_Along__c limit 1 ];
        //System.debug('UpdateRide ' + ra);
        ra.Reviewed_my_Feedback__c=true;
        ra.Sales_Call_Complete__c= true;
        update ra;
        
    }
    public static testmethod  void processEmailTest(){
        User TeamMember = [Select id , name from User where lastname='TeamMember' limit 1 ];
        Ride_Along__c ra = [Select team_member__c,Reviewed_my_Feedback__c,Sales_Call_Complete__c from Ride_Along__c limit 1 ];
        // System.debug('processEmailTest ' + ra);
        String message = Handler_Ride_Along_Trigger.processEmail(String.valueOf(ra.Id));   
        
    }
}