global class CCPDCLogicCartPlace extends ccrz.ccLogicCartPlace {
    global override Map<String, Object> process(Map<String, Object> inputData) {
        ccrz.ccLog.log('******************CCPDCLogicCartPlace process***************************');      
        Map<String, Object> outputData = super.process(inputData);

        Boolean isSuccess = (Boolean) outputData.get(ccrz.ccAPI.SUCCESS);
        String orderId = (String) outputData.get('orderId'); 
        if (isSuccess && orderId != null) {
            CCPDCFulfillmentHelper.copyCartToOrderData(orderId);
            if(ccrz.cc_CallContext.storefront == 'parts') {
                system.debug('Inside Copy Transaction orderTransPayments @@Rahul before');
                CCFPFulfillmentHelper.copyCartTransactionDataToOrder(orderId);
                postProcessFP(orderId);   
            }else{
                postProcess(orderId);  
            }
        }    
        ccrz.ccLog.log('****************** end CCPDCLogicCartPlace process***************************');      
        return outputData;        
    }

    @future(callout=true)
    public static void postProcess(Id orderId) {
        List<ccrz__E_TransactionPayment__c> payments = CCAviTransactionPaymentDAO.getTransactionPaymentsForOrder(orderId);
        List<ccrz__E_TransactionPayment__c> newPayments = CCPDCFulfillmentHelper.doAuth(orderId, payments);
        if (newPayments != null && !newPayments.isEmpty() || test.isRunningTest()) {  
            for(ccrz__E_TransactionPayment__c tp : newPayments) {
                if(tp.ccrz__TransactionCode__c == 'Authorization Failed') {
                   	insert newPayments;
                    return; 
                }
            }
            //if authorization failed, do not send order data to Iseries
            CCFPBoomiOrderPlacementAPI.placeOrder(orderId, newPayments); 
            insert newPayments;
        }
        else {
            CCFPBoomiOrderPlacementAPI.placeOrder(orderId, payments); // IO flow
        }
    }
    
    @future(callout=true)
    public static void postProcessFP(Id orderId) {
        List<ccrz__E_TransactionPayment__c> payments = CCAviTransactionPaymentDAO.getTransactionPaymentsForOrder(orderId);
        if (payments.size() > 1) {  
            for(ccrz__E_TransactionPayment__c tp : payments) {
                if(tp.ccrz__TransactionCode__c == 'Authorization Failed') {
                    return; 
                }
            }
            //if authorization failed, do not send order data to Iseries
            CCFPBoomiOrderPlacementAPI.placeOrder(orderId, payments); 
        }
        else {
            CCFPBoomiOrderPlacementAPI.placeOrder(orderId, payments); // IO flow
        }
    }

}