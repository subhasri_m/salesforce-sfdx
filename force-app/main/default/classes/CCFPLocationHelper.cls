public with sharing class CCFPLocationHelper {
    public static LocationResponse getLocationForAccount(Id accountId) {
        LocationResponse response = null;
        Account accountPermissions = CCPDCAccountDAO.getAccountPermissions(accountId);
        if (accountPermissions != null && accountPermissions.Ship_from_Location__c != null) {
            Location__c location = CCFPLocationDAO.getLocationForCode(accountPermissions.Ship_from_Location__c);
            if (location != null) {
                response = new LocationResponse(location);
            }
        }
        return response;
    }

    public static LocationResponse getLocationForId(String locationId) {
        LocationResponse response = null;
        Location__c location = CCFPLocationDAO.getLocation(locationId);
        if (location != null) {
            response = new LocationResponse(location);
        }
        return response;
    }

    public class LocationResponse {

        public String sfid {get; set;}
        public String address1 {get; set;}
        public String address2 {get; set;}
        public String city {get; set;}
        public String state {get; set;}
        public String zip {get; set;}
        public String county {get; set;}
        public String country {get; set;}
        public String location {get; set;}


        public LocationResponse(Location__c l) {
            this.sfid = l.Id;
            this.address1 = l.Address_Line_1__c;
            this.address2 = l.Address_Line_2__c;
            this.city = l.City__c;
            this.state = l.State__c;
            this.zip = l.Zipcode__c;
            this.county = l.County__c;
            this.country = 'US';
            this.location = l.Location__c;
        }

    }
}