@isTest
private class Batch_UpdateMonthlyCallIndicator_Test {
    
     static testmethod void test() {
         
         Account acc = new Account();
         acc.name = 'test Account';
         acc.Iseries_Company_code__c = '1';
      	 insert acc;
         
          task t1 = new task();
          t1.activitydate = date.today();
          t1.Type = 'Sales Call';
          t1.WhatId = acc.id;
          insert t1;
          
      
          test.startTest();
          database.executeBatch(new Batch_UpdateMonthlyCallIndicator());          
          test.stopTest();
      }
    

}