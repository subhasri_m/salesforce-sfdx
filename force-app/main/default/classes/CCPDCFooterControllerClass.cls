global class CCPDCFooterControllerClass {
    public boolean pdc {get;set;}
    public boolean parts {get;set;}
    global CCPDCFooterControllerClass() {
        if (ccrz.cc_CallContext.storefront == 'parts'){
          parts = true;
          pdc = false;  
        }else{
          parts = false;
          pdc = true;
        }
  }
     
  @RemoteAction
    global static ccrz.cc_RemoteActionResult subscribeMailChimp(ccrz.cc_RemoteActionContext ctx, String email){
       ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        try{    
              HttpResponse mailChimpResponse = MailChimpAPI.fleetPrideSubscribe(email);
              Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(mailChimpResponse.getBody());
              If(mailChimpResponse.getStatusCode() == 200 || mailChimpResponse.getStatusCode() == 201){
                CCAviPageUtils.buildResponseData(response, true, responseMap); 
              }else{
                 CCAviPageUtils.buildResponseData(response, false, responseMap); 
              }
        } catch (Exception e) {
              CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response;
    }
   
    
}