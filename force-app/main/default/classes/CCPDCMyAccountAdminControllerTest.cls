@isTest
public class CCPDCMyAccountAdminControllerTest {
    
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void affectUserTest() {
        util.initCallContext();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c data1 = CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true);
        insert data1;

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountAdminController.affectUser(ctx, CCPDCMyAccountAdminController.ACTION_DELETE, data1.Id);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
        System.assert(result.data != null);
    }

    static testmethod void addOrUpdateUserTest() {
        util.initCallContext();
        User u = util.getPortalUser();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        String data = '{"emailAddress":"contact@test.user","username":"contact@test.user","firstName":"Test","lastName":"Contact","phoneNumber":"444-444-4444","arPayments":"false","accountingReports":"false","salesReports":"false","backorderRelease":"false","backorders":"false","allowCheckout":"true","allowNonFreeShipping":"false","overrideShipTo":"false","isAdmin":"false","canSeePricing":"true","canViewInvoices":"on"}';

        ccrz.cc_RemoteActionResult result = null;

        System.runAs(u) {
            Test.startTest();
            result = CCPDCMyAccountAdminController.addOrUpdateUser(ctx, false, data);
            Test.stopTest();
        }

        System.assert(result != null);
        System.assert(result.success);
        System.assert(result.data != null);
    }

    static testmethod void addOrUpdateUserDupTest() {
        util.initCallContext();
        User u = util.getPortalUser();
        CC_FP_Contract_Account_Permission_Matrix__c data1 = CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true);
        insert data1;
        String accountId = String.valueOf(ccrz.cc_CallContext.currAccountId);
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        String data = '{"emailAddress":"contact@test.user","username":"username@email.com","firstName":"Test","lastName":"Contact","phoneNumber":"444-444-4444","arPayments":"false","accountingReports":"false","salesReports":"false","backorderRelease":"false","backorders":"false","allowCheckout":"true","allowNonFreeShipping":"false","overrideShipTo":"false","isAdmin":"false","canSeePricing":"true","canViewInvoices":"on","aviliable-accounts":"'+accountId+'"}';

        ccrz.cc_RemoteActionResult result = null;

        System.runAs(u) {
            Test.startTest();
            result = CCPDCMyAccountAdminController.addOrUpdateUser(ctx, false, data);
            Test.stopTest();
        }

        System.assert(result != null);
        System.assert(!result.success);
        System.assert(result.data != null);
    }

    static testmethod void addOrUpdateUserUpdateTest() {
        util.initCallContext();
        User u = util.getPortalUser();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        CC_FP_Contract_Account_Permission_Matrix__c data1 = CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true);
        insert data1;

        String data = '{"contactId":"'+ccrz.cc_CallContext.currContact.Id+'","permissionId":"'+data1.Id+'","emailAddress":"contact@test.user","username":"username@email.com","firstName":"Test","lastName":"Contact","phoneNumber":"444-444-4444","arPayments":"false","accountingReports":"false","salesReports":"false","backorderRelease":"false","backorders":"false","allowCheckout":"true","allowNonFreeShipping":"false","overrideShipTo":"false","isAdmin":"false","canSeePricing":"true","canViewInvoices":"on"}';

        ccrz.cc_RemoteActionResult result = null;

        System.runAs(u) {
            Test.startTest();
            result = CCPDCMyAccountAdminController.addOrUpdateUser(ctx, true, data);
            Test.stopTest();
        }

        System.assert(result != null);
        System.assert(result.success);
        System.assert(result.data != null);
    }

    static testmethod void retrieveAccountAndUserDataTest() {
        util.initCallContext();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountAdminController.retrieveAccountAndUserData(ctx);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
        System.assert(result.data != null);
    }
    static testmethod void addAccountPermissionTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String accountId = String.valueOf(ccrz.cc_CallContext.currAccountId);

        String data = '{"contactId":"'+ccrz.cc_CallContext.currContact.Id+'","arPayments":"false","accountingReports":"false","salesReports":"false","backorderRelease":"false","backorders":"false","allowCheckout":"true","allowNonFreeShipping":"false","overrideShipTo":"false","isAdmin":"false","canSeePricing":"true","canViewInvoices":"on","add-permission-select":"'+accountId+'"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountAdminController.addAccountPermission(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
        System.assert(result.data != null);
    }

    static testmethod void addAccountPermissionExceptionTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String accountId = String.valueOf(ccrz.cc_CallContext.currAccountId);

        String data = '';
        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountAdminController.addAccountPermission(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
        System.assert(result.data != null);
    }

    static testmethod void insertUser_futureTest() {
        User u = util.getPortalUser();
        Account a = util.getAccount();
        Contact contact = util.createContact(a,'testcontact1@email.com');
        insert contact;
        List<Contact> contacts = [SELECT Id FROM Contact WHERE Email =:'testcontact1@email.com'];
        Contact newcontact = contacts.get(0);
        
        System.runAs(u) {
            Test.startTest();
             CCPDCMyAccountAdminController.insertUser_future(String.valueOf(newcontact.Id),'testfirst','testlast','contacttest5454@gmail.com','1112223333','test12345454@email.com');
            Test.stopTest();
        }
    }

    

}