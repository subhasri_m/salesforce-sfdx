public class BatchSalesTarget implements Database.Batchable<sObject> {
    
    public Database.QueryLocator start(Database.BatchableContext bc){ 
        
        return Database.getQueryLocator('SELECT Id, salesman_number__c,SalesmanDate__c FROM Sales_Order_Summary__c where ownerid  != \'00540000002LWA0\' AND Order_Date__c  = THIS_YEAR AND Owner.IsActive = true AND salesman_number__c != null ORDER BY salesman_number__c, Order_Date__c  ');
    }
    
    public void execute(Database.BatchableContext bc, List<Sales_Order_Summary__c> scope){ 
        
        Map<String,Id> salesTargetMap = new Map<String,Id>();
        Set<String> keys = new Set<String>();
        List<Sales_Order_Summary__c> updateList = new List<Sales_Order_Summary__c>();
        
        //Loop through scope and populate salesNumberSalesTargetMap
        for(Sales_Order_Summary__c sos : scope){
            keys.add(sos.SalesmanDate__c);
        }

        for(Sales_Target__c st : [SELECT Id,Salesman_Number__c,SalesmanDate__c FROM Sales_Target__c WHERE Salesman_Number__c != null AND Start_Date__c = THIS_YEAR AND SalesmanDate__c IN :keys ]){
            salesTargetMap.put(st.SalesmanDate__c,st.Id);

        }

        for(Sales_Order_Summary__c sos : scope){
            if(salesTargetMap.containsKey(sos.SalesmanDate__c)){
                sos.Sales_Target__c = salesTargetMap.get(sos.SalesmanDate__c);
                sos.Update__c = 'Update';
                updateList.add(sos);
            }

        }
        
        
        if(!updateList.isEmpty()){
            update updateList;
        }
        
    }
    
    public void finish(Database.BatchableContext bc){ 
    
    }
}