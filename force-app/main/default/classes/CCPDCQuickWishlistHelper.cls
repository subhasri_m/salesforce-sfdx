public with sharing class CCPDCQuickWishlistHelper {

    public static List<QuickOrderItemWL> getQuickOrderItemsWL(List<QuickOrderItemWL> items) {
        List<String> parts = new List<String>();
        List<QuickOrderItemWL> validItems = new List<QuickOrderItemWL>();
        Map<String, QuickOrderItemWL> partMap = new Map<String, QuickOrderItemWL>();
        for (QuickOrderItemWL item : items) {
            if (!String.isBlank(item.partNumber)) {
                item.partNumber = item.partNumber.trim();
                item.isConflict = false;
                
                parts.add(item.partNumber);
                partMap.put(item.partNumber, item);
                validItems.add(item);
            }
        }
        List<ccrz__E_Product__c> products = CCPDCProductDAO.getParts(parts, ccrz.cc_CallContext.storefront);
        for (ccrz__E_Product__c p : products) {
            for (QuickOrderItemWL item : validItems) {
                if (item.isMatch(p)) {
                    if (item.record == null) {
                        if (item.isConflict == null) {
                            item.isConflict = false;
                        }
                        if (item.isConflict) {
                            item.conflicts.add(p);
                        }
                        else {
                            item.record = p;
                            item.salesPack = p.Salespack__c;
                            item.dspPartNumber = p.DSP_Part_Number__c;
                            item.sku = p.ccrz__SKU__c;
                            item.name = p.Name != null ? p.Name : '';
                        }
                    }
                    else {
                        if (item.conflicts == null) {
                            item.conflicts = new List<ccrz__E_Product__c>();
                            item.conflicts.add(item.record);
                        }
                        item.conflicts.add(p);
                        if (item.selected != null) {
                            if (item.selected == p.ccrz__SKU__c) {
                                item.isConflict = false;
                                item.record = p;
                                item.sku = p.ccrz__SKU__c;
                                item.salesPack = p.Salespack__c;
                                item.dspPartNumber = p.DSP_Part_Number__c;
                                item.name = p.Name != null ? p.Name : '';
                            }
                            else if (item.selected != item.sku) {
                                item.isConflict = true;
                                item.record = null;
                                item.sku = null;
                            }
                        }
                        else {
                            item.isConflict = true;
                            item.record = null;
                            item.sku = null;
                        }
                    }
                    break;
                }
            }
        }
        return validItems;
    }

    public static Boolean validate(List<QuickOrderItemWL> items) {
        List<String> skulist = new List<String>();
        for (QuickOrderItemWL item : items) {
            if (item.sku != null) {
                skulist.add(item.sku);               
            }
        }        


        List<ccrz__E_Product__c> prods = CCPDCProductDAO.getSalespackForSkusMulti(skulist);    
        Map<String, Decimal> skuToSalespackMap = new Map<String, Decimal>();             
        if(prods != null && prods.size() >0) {
            for(ccrz__E_Product__c p : prods){
                if(p.Salespack__c != null){
                    skuToSalespackMap.put(p.ccrz__SKU__c, p.Salespack__c);                
                }
            }        
        }             

        Boolean isValid = true;
        for (QuickOrderItemWL item : items) {
            if (item.sku == null || item.quantity == null || item.isConflict == null || item.isConflict) {
                item.isInvalid = true;
                isValid = false;
            }
            if(skuToSalespackMap != null && skuToSalespackMap.size() > 0 && item.sku != null &&  skuToSalespackMap.get(item.sku) != null){

                if(item.quantity != null && item.quantity != 0) {
                    Decimal sp = skuToSalespackMap.get(item.sku);
                    if(Integer.valueOf(sp) == 0){
                        sp = 1;
                    }                    
                    if(item.quantity < sp || Math.mod( Integer.valueOf(item.quantity),  Integer.valueOf(sp) ) != 0) {
                        item.isInvalid = true;
                        isValid = false;
                        String name = item.dspPartNumber != null ? item.dspPartNumber : item.partNumber; 
                        //Decimal salesPack = item.salesPack; 
                        item.message = 'Product: '+name+' only sells in increments of: ' +sp;                                                
                    }  

                }
            }
        }
        return isValid;
    }

    public static String addToCart(List<QuickOrderItemWL> items) {
        String cartEncId = null;
        Boolean wasSuccessful = true;

        List<ccrz.ccApiCart.LineData> newLines = new List<ccrz.ccApiCart.LineData>();
        for (QuickOrderItemWL item : items) {
            if (item.sku == null || item.quantity == null) {
                wasSuccessful = false;
            }
            else {
                ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
                newLine.sku = item.sku;
                newLine.quantity = item.quantity;
                newLine.price = item.price;
                newLines.add(newLine);
            }
        }

        if (wasSuccessful) {
            String cartId = CCAviCartManager.getActiveCartId();

            if (cartId != null) {
                Map<String, Object> request = new Map<String,Object>{
                    ccrz.ccApi.API_VERSION => 6, 
                    ccrz.ccApiCart.CART_ENCID => cartId,
                    ccrz.ccApiCart.LINE_DATA => newLines
                };
        
                Map<String, Object> outputData = CCPDCCartManager.addTo(request);
                wasSuccessful = (Boolean)outputData.get(ccrz.ccApi.SUCCESS);
                if (wasSuccessful) {
                    cartEncId = cartId;
                }
            }
        }

        return cartEncId;
    }

    public static List<QuickOrderItemWL> parseQuickOrderItem(String json) {
        List<QuickOrderItemWL> items = (List<QuickOrderItemWL>) System.JSON.deserialize(json, List<QuickOrderItemWL>.class);
        return items;
    }

    public static List<QuickOrderItemWL> parseQuickOrderRequest(String requestString) {
        List<QuickOrderItemWL> items = new List<QuickOrderItemWL>();
        List<String> lines = requestString.split('\n');
        for (String s : lines) {
            List<String> values = s.split(',');
            QuickOrderItemWL item = new QuickOrderItemWL();
            item.partNumber = values[0];
            item.quantity = Integer.valueOf(values[1]);
            items.add(item);
        }
        return items;
    }

    public static String createQuickOrderRequest(List<QuickOrderItemWL> items) {
        String request = '';
        for (QuickOrderItemWL item : items) {
            request += item.partNumber + ',' + item.quantity + '\n';
        }
        return request;
    }

    public class QuickOrderItemWL {
        public String sku {get; set;}
        public Integer quantity {get; set;}
        public String partNumber {get; set;}
        public ccrz__E_Product__c record {get; set;}
        public Boolean isConflict {get; set;}
        public Boolean isInvalid {get; set;}
        public String selected {get; set;}
        public List<ccrz__E_Product__c> conflicts {get; set;}
        public String name {get; set;}
        public String message {get; set;}
        public Decimal price {get; set;}
        public Decimal salesPack {get; set;}
        public String dspPartNumber { get; set; }
        
        public QuickOrderItemWL() {
            this.isConflict = false;
        }

        public Boolean isMatch(ccrz__E_Product__c product) {
            Boolean isMatch = false;
            if (product.ccrz__SKU__c == partNumber || product.Name == partNumber || product.Part_Number__c == partNumber) {            
                isMatch = true;
            }
            return isMatch;
        }
    }
}