/*
Database.executeBatch(new BatchPopulateUserSalesCompany(), 50);
*/
global class BatchPopulateUserSalesCompany implements Database.Batchable<sObject>, Database.Stateful {

	 global Integer totalProcessedNum {get; set;}
	 global Integer totalOKNum {get; set;}
	 global Integer totalNGNum {get; set;}

	 global Id oneId {get; set;}
	 global BatchPopulateUserSalesCompany() {}
	 global BatchPopulateUserSalesCompany(Id oneId) {
	 	this.oneId = oneId;
	 }
	 
	 global Database.QueryLocator start(Database.BatchableContext bc) {

		totalProcessedNum = 0;
     	totalOKNum = 0;
     	totalNGNum = 0;		
		
		String msg = 'Batch Sales Target Calculation gets started at: ' + Datetime.now();
		Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchPopulateUserSalesCompany',
			Batch_Message__c=msg,
			Type__c = 'INFO'
			);
		
		insert bl;
		
		String qry = 'SELECT UserRoleId FROM User';
		
		if (!String.isEmpty(oneId)) {
            qry += ' where id = :oneId';
        }
        
	    return Database.getQueryLocator(qry);
	}
    
    global void execute(Database.BatchableContext bc, List<User> ul) {
    	
    	Integer processedNum = ul.size();
		Integer OKNum = 0;
		Integer NGNum = 0;
		
		try {
			
			OKNum = processedNum;
			
			List<User> userToUpdate = new List<User>();
			for(User u : ul) {
				Id roleId = u.UserRoleId;
			
				Set<UserRole> userRoleSet = new Set<UserRole>();
			
				CommonUtil.getParentRole(userRoleSet, CommonUtil.getRole(roleId));
				
				for(UserRole ur : userRoleSet) {
					if (ur.name == 'Company 1' || ur.name == 'Company 2') {
						u.Sales_Company__c = ur.name;
						userToUpdate.add(u);
						break;
					}
				}
				
			}
			for(User u : userToUpdate) {
				system.debug('-- ' + u);
			}
			
			update userToUpdate;
			
		} catch (Exception ex) {
			
			system.debug('-- error log:' + ex);
			
			NGNum = processedNum;
			// Write result to log object
			String msg = 'Error happened : ' + ex.getMessage() + '\n\n';
			msg += ex.getStackTraceString();
			msg += '\n caused by: \n';
			msg += ex.getCause();
			for(User a : ul) {
				msg += a.id + ',' + a.Name + '; ';
			}
			
			Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchPopulateUserSalesCompany',
				Batch_Message__c=msg,
				Type__c = 'ERROR'
			);
			insert bl;
			
        } finally {
        	
        	totalProcessedNum += processedNum;
			totalOKNum += OKNum;
			totalNGNum += NGNum;
			
			system.debug('-- final complete.');
			
        }
     }
     
     global void finish(Database.BatchableContext bc) {
 		
 		String msg = 'Record retrieved : ' + totalProcessedNum +
			 ', Successfully processed : ' + totalOKNum +
			 ', Failed to processed : ' + totalNGNum;
		// Write result to log object
		Batch_Log__c bl = new Batch_Log__c(Batch_Class__c='BatchPopulateUserSalesCompany',
			Batch_Message__c=msg,
			Type__c = 'INFO'
			);
		
		system.debug('-- finish:' + bl);
		
		insert bl;
    
    	system.debug('-- finish complete.'); 
     }
}