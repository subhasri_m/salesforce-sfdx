/*
 Name : SalesTargetHelper
 Description: Trigger Handler class
 Owner : Fleet Pride
 Modified By: lalit Arora
*/ 
public class SalesTargetHelper{
 
       public SalesTargetHelper(List<Sales_Target__c> salesTargetVar){
            for (Sales_Target__c st : salesTargetVar) {
                string focusSalesVar = string.valueOf(st.Focus5_Sales__c);
                string focusGPVar = string.valueOf(st.Focus5_GP__c);
                string focusTargetVar = string.valueOf(st.Focus5_Target__c);
                string goalPercentVar = string.valueOf(st.Goal_Percentage__c);
                string gpMTDTargetPctVar = string.valueOf(st.GP_MTD_Target_Pct__c);
                string gpQTDVar = string.valueOf(st.GP_QTD__c);
                string gpTargetAmountVar = string.valueOf(st.GP_Target_Amount__c);
                string gpTargetGrowthThisMonthVar = string.valueOf(st.GP_Target_Growth_ThisMonth__c);
                string gpTargetGrowthThisQuarterVar = string.valueOf(st.GP_Target_Growth_ThisQuarter__c);
                string gpTargetGrowthThisYearVar = string.valueOf(st.GP_Target_Growth_ThisYear__c);
                string gpYTDVar = string.valueOf(st.GP_YTD__c);
                string gpSumVar = string.valueOf(st.Gross_Profit_Sum__c);
                string qtdSalesVar = string.valueOf(st.QTD_Sales__c);
                string mtdTargetPctVar = string.valueOf(st.Sales_MTD_Target_Pct__c);
                string salesPlanAmountVar = string.valueOf(st.Sales_Plan_Amount__c);
                string salesSumVar = string.valueOf(st.Sales_Sum__c);
                string salesTargetGrowthThisMonthVar = string.valueOf(st.Sales_Target_Growth_ThisMonth__c);
                string salesTargetGrowthThisQuarter = string.valueOf(st.Sales_Target_Growth_ThisQuarter__c);
                string salesTargetGrowthThisYear = string.valueOf(st.Sales_Target_Growth_ThisYear__c);
                string YTDSalesVar = string.valueOf(st.YTD_Sales__c);
                
                string salesTargetGrowthTotalMonthTargetVar = string.valueOf(st.Sales_Target_Growth_Total_Month_Target__c);
                string salesTargetGrowthTotalQuarterTargetVar = string.valueOf(st.Sales_Target_Growth_Total_Quarter_Target__c);
                string salesTargetGrowthTotalYearTargetVar = string.valueOf(st.Sales_Target_Growth_Total_Year_Target__c);
                
                string gpTargetGrowthTotalMonthTargetVar = string.valueOf(st.GP_Target_Growth_Total_Month_Target__c);
                string gpTargetGrowthTotalQuarterTargetVar = string.valueOf(st.GP_Target_Growth_Total_Quarter_Target__c);
                string gpTargetGrowthTotalYearTargetVar = string.valueOf(st.GP_Target_Growth_Total_Year_Target__c);
                           
                if(focusSalesVar == NULL || focusSalesVar == '')
                   st.Focus5_Sales__c = 0;
                   
                if(focusGPVar == NULL || focusGPVar == '')
                   st.Focus5_GP__c = 0;
              
                if(focusTargetVar == NULL || focusTargetVar == '')
                   st.Focus5_Target__c = 0;
                   
                if(goalPercentVar == NULL || goalPercentVar == '')
                   st.Goal_Percentage__c = 0;
                   
                if(gpMTDTargetPctVar == NULL || gpMTDTargetPctVar == '')
                   st.GP_MTD_Target_Pct__c = 0;
              
                if(gpQTDVar == NULL || gpQTDVar == '')
                   st.GP_QTD__c = 0;
                   
                 if(gpTargetAmountVar == NULL || gpTargetAmountVar == '')
                   st.GP_Target_Amount__c = 0;
                   
                if(gpTargetGrowthThisMonthVar == NULL || gpTargetGrowthThisMonthVar == '')
                   st.GP_Target_Growth_ThisMonth__c = 0;
              
                if(gpTargetGrowthThisQuarterVar == NULL || gpTargetGrowthThisQuarterVar == '')
                   st.GP_Target_Growth_ThisQuarter__c = 0;
                   
                 if(gpTargetGrowthThisYearVar == NULL || gpTargetGrowthThisYearVar == '')
                   st.GP_Target_Growth_ThisYear__c = 0;
                   
                if(gpYTDVar == NULL || gpYTDVar == '')
                   st.GP_YTD__c = 0;
              
                if(gpSumVar == NULL || gpSumVar == '')
                   st.Gross_Profit_Sum__c = 0;
                   
                 if(qtdSalesVar == NULL || qtdSalesVar == '')
                   st.QTD_Sales__c = 0;
                   
                if(mtdTargetPctVar == NULL || mtdTargetPctVar == '')
                   st.Sales_MTD_Target_Pct__c= 0;
              
                if(salesPlanAmountVar == NULL || salesPlanAmountVar == '')
                   st.Sales_Plan_Amount__c = 0;
                 
                
                if(salesSumVar == NULL || salesSumVar == '')
                   st.Sales_Sum__c = 0;
              
                if(salesTargetGrowthThisMonthVar == NULL || salesTargetGrowthThisMonthVar == '')
                   st.Sales_Target_Growth_ThisMonth__c = 0;
                   
                 if(salesTargetGrowthThisQuarter == NULL || salesTargetGrowthThisQuarter == '')
                   st.Sales_Target_Growth_ThisQuarter__c = 0;
                   
                if(salesTargetGrowthThisYear == NULL || salesTargetGrowthThisYear == '')
                   st.Sales_Target_Growth_ThisYear__c = 0;
              
                if(YTDSalesVar == NULL || YTDSalesVar == '')
                   st.YTD_Sales__c = 0;   
                   
                if(salesTargetGrowthTotalMonthTargetVar == NULL || salesTargetGrowthTotalMonthTargetVar == '')
                   st.Sales_Target_Growth_Total_Month_Target__c = 0;
                   
                 if(salesTargetGrowthTotalQuarterTargetVar == NULL || salesTargetGrowthTotalQuarterTargetVar == '')
                   st.Sales_Target_Growth_Total_Quarter_Target__c = 0;
                   
                if(salesTargetGrowthTotalYearTargetVar == NULL || salesTargetGrowthTotalYearTargetVar == '')
                   st.Sales_Target_Growth_Total_Year_Target__c = 0;
              
                if(gpTargetGrowthTotalMonthTargetVar == NULL || gpTargetGrowthTotalMonthTargetVar == '')
                   st.GP_Target_Growth_Total_Month_Target__c = 0;
                   
                 if(gpTargetGrowthTotalQuarterTargetVar == NULL || gpTargetGrowthTotalQuarterTargetVar == '')
                   st.GP_Target_Growth_Total_Quarter_Target__c = 0;
                   
                if(gpTargetGrowthTotalYearTargetVar == NULL || gpTargetGrowthTotalYearTargetVar == '')
                   st.GP_Target_Growth_Total_Year_Target__c= 0; 
           
            } 
       } 
}