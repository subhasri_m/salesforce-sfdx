global class Batch_ReAssignSOS_to_Account_Owner implements Schedulable {
   global void execute(SchedulableContext sc) {
      Id batchInstanceID = Database.executeBatch(new ReAssignSOS_to_Account_Owner());
   }
}