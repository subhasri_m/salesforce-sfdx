@isTest
private class CCPDCMyAccountMyAddressBookTabCtrlTest {
	
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void testConstructor(){
    	util.initCallContext();
    	CCPDCMyAccountMyAddressBookTabController ctrl = new CCPDCMyAccountMyAddressBookTabController();
    	System.assertEquals(ctrl.numberOfAddresses,2);
    }

    static testmethod void getAddressBooksForAccountTest() {
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Account a = util.getAccount();
        ctx.storefront = 'DefaultStore';
        ctx.effAccountId = String.valueOf(a.Id);

        Test.startTest(); 
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCMyAccountMyAddressBookTabController.currentAddressBook(ctx);
        Test.stopTest(); 
 		Map<String,Object> data = (Map<String,Object>)result.data;
 		List<Map<String,String>> address = (List<Map<String,String>>)data.get('addresses');
        System.assertEquals(address.size(),2);
        

    }
    static testmethod void changeAddressTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);
        List<String> responses = new List<String>{CCAviAddressyAPITest.VALID_ADDRESS_STRING, CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyAddressBookTabController.saveAddress(ctx, input);
        Test.stopTest();

        System.assert(result != null);
        //System.assert(result.success);
    }
    static testmethod void changeAddressNoSFIDTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);
        List<String> responses = new List<String>{CCAviAddressyAPITest.VALID_ADDRESS_STRING, CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyAddressBookTabController.saveAddress(ctx, input);
        Test.stopTest();

        System.assert(result != null);
        //System.assert(result.success);
    }
    static testmethod void changeAddressFailTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyAddressBookTabController.saveAddress(ctx, input);
        Test.stopTest();

        System.assert(result != null);
        System.assert(!result.success);
    }

    static testmethod void changeAddressByIdTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;
        Location__c loc = new Location__c();
        loc.Name = 'BB';
        loc.Location__c = 'BB';
        loc.Location_Id__c = 'BB';        
        insert loc;
        Location__c loc2 = new Location__c();
        loc2.Name = 'NH';
        loc2.Location__c = 'NH';
        loc2.Location_Id__c = 'NH';        
        insert loc2;

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);
        
        List<String> responses = new List<String>{CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING2, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));
        //Test.setMock(HttpCalloutMock.class, new CCAviAddressyAPITest.AddressyAPIMock(CCAviAddressyAPITest.AddressyAPIMockResult.RETRIEVE_SUCCESS2));        

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","liftGate":true,"type":"Commercial", "addressId":"US|US|B|Y214362453|5119","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyAddressBookTabController.changeAddressById(ctx, input);
        Test.stopTest();

        System.assert(result != null, cart.ccrz__ShipTo__c);     
        //System.assert(result.success);     
    } 
    static testmethod void changeAddressByIdNoSFIDTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;
        Location__c loc = new Location__c();
        loc.Name = 'BB';
        loc.Location__c = 'BB';
        loc.Location_Id__c = 'BB';        
        insert loc;        
        Location__c loc2 = new Location__c();
        loc2.Name = 'NH';
        loc2.Location__c = 'NH';
        loc2.Location_Id__c = 'NH';        
        insert loc2;      
        
        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);        
        List<String> responses = new List<String>{CCAviAddressyAPITest.RETRIEVE_SUCCESS_STRING, CCPDCOMSHomeDCAPITest.RESPONSE_STRING};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));
        //Test.setMock(HttpCalloutMock.class, new CCAviAddressyAPITest.AddressyAPIMock(CCAviAddressyAPITest.AddressyAPIMockResult.RETRIEVE_SUCCESS2));

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","liftGate":true,"type":"Commercial","addressId":"US|US|B|Y214362453|5119","sfid":""}';                
        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyAddressBookTabController.changeAddressById(ctx, input);
        Test.stopTest();

        System.assert(result != null, cart.ccrz__ShipTo__c);    
        //System.assert(result.success);    
    }

    static testmethod void changeAddressByIdTestFail() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;
        Test.setMock(HttpCalloutMock.class, new CCAviAddressyAPITest.AddressyAPIMock(CCAviAddressyAPITest.AddressyAPIMockResult.VALID_ADDRESS));
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial", "addressId":"US|US|A|Y214362453|5119","sfid":"';
        input += cart.ccrz__ShipTo__c;
        input += '"}';

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyAddressBookTabController.changeAddressById(ctx, input);
        Test.stopTest();

        System.assert(result != null);        
    }     
    static testmethod void fetchContactAddressTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        CC_FP_Contract_Account_Permission_Matrix__c perms = util.createPermissions(false);
        insert perms;
        Test.setMock(HttpCalloutMock.class, new CCAviAddressyAPITest.AddressyAPIMock(CCAviAddressyAPITest.AddressyAPIMockResult.VALID_ADDRESS));
        CCAviAddressyAPITest.createAddressySettings(util.STOREFRONT);

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();        
        String sfid = cart.ccrz__ShipTo__c;
    
        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyAddressBookTabController.fetchContactAddress(ctx, sfid);
        Test.stopTest();

        System.assert(result != null, cart.ccrz__ShipTo__c);
        System.assert(result.success = true);
    }     
    
    static testmethod void deleteAddressTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();        
        String sfid = cart.ccrz__ShipTo__c;
    
        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCMyAccountMyAddressBookTabController.deleteAddress(ctx, sfid);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success = true);
        
    }  
    
	static testmethod void accountOwnerDataTest() {
        util.initCallContext();
        Account acc = util.getAccount();
        User theUser = null;
        Test.startTest();
        theUser = CCPDCMyAccountMyAddressBookTabController.accountOwnerData(acc.Id);
        Test.stopTest();
        System.assert(theUser != null);
                
    } 
	
}