public with sharing class DashboardMenuController{
   public Map<Integer,gaugeData> data {get;set;}
    public Map<Integer,gaugeData> scData {get;set;}
    public Map<Integer,gaugeData> focusfivedata {get;set;}
    // public Map<Integer,gaugeData> prospectingData {get;set;}
    public Date startdateMonth {get;set;}
    public Date startdateQuarter {get;set;}
    public Date startdateYear {get;set;}
    public Date enddateMonth {get;set;}
    public Date enddateQuarter {get;set;}
    public Date enddateYear {get;set;}
    public String region;
    public String roleName;
    public String roleLevel {get;set;}
    public String DirLeadDashBoard{get;set;}
    public String TSMLeadDashBoard{get;set;}
    public String OSRLeadDashBoard{get;set;}
    public String adoptionRateStr {get;set;}
    public String  winRateStr {get;set;}
    public String  wonRevenueAfterStr  {get;set;}
    public Map<String,String> reportsIdMap {get;set;}
    public Map<String,String> dashboardsIdMap {get;set;}
    public String salesMaxLink {get;set;}
    public String maydayLink {get;set;}
    public String Focus5Link {get;set;}  //Addition by Sandeep Meka 09/19/17 to make Focus 5 Drill Down for Everyone
    
    public DashboardMenuController(){    
        
         
        startdateMonth = Date.newInstance(Date.today().year(),Date.today().month(),01);
        startdateQuarter = [Select StartDate From Period Where type = 'Quarter' and StartDate = THIS_QUARTER].StartDate;
        startdateYear = Date.newInstance(Date.today().year(),01,01);
        
        enddateMonth = Date.today();
        enddateQuarter = Date.today();
        enddateYear = Date.today();
        
        Date endTargetMonth = [Select EndDate From Period Where type = 'Month' and StartDate = THIS_MONTH].EndDate.addYears(-1);
        Date endTargetQuarter = [Select EndDate From Period Where type = 'Quarter' and StartDate = THIS_QUARTER].EndDate.addYears(-1);
        Date endTargetYear = [Select EndDate From Period Where type = 'Year' and StartDate = THIS_YEAR].EndDate.addYears(-1);
        
        salesMaxLink = '';
        maydayLink = '';
        Focus5Link = ''; //Addition by Sandeep Meka 09/19/17 to make Focus 5 Drill Down for Everyone
        
        populateReportMap();
        
       // List<zsx__CustomerInsight__c> ciList = new List<zsx__CustomerInsight__c>();
        List<AggregateResult> ciARList = new List<AggregateResult>();
        
        //Dynamic Link for leaderboard Dashboards
        List<String> dashboardNames = new List<String>{'Sales_Director_Leaderboard', 'TSM_Leaderboard', 'OSR_Leaderboard'};
            List<Dashboard> dashboardList = [SELECT Id, DeveloperName FROM Dashboard WHERE DeveloperName IN: dashboardNames];
        Map<String,Id> dashboardMap = new Map<String,Id>();
        
        for(Dashboard d: dashboardList){
            dashboardMap.put(d.DeveloperName, d.Id);
        }
        
        DirLeadDashBoard = '/' +dashboardMap.get('Sales_Director_Leaderboard');
        TSMLeadDashBoard = '/' +dashboardMap.get('TSM_Leaderboard');
        OSRLeadDashBoard = '/' +dashboardMap.get('OSR_Leaderboard');
        
        //User for dynamic showing of leaderboards YTD Drill down reports
        Id systemAdminProfileId = [Select Id, Name FROM Profile WHERE Name = 'System Administrator'][0].id;
        User u = [SELECT Id, Region__c, UserRoleId, ProfileId  FROM User WHERE Id = :UserInfo.getUserId()];
        UserRole us =null;
        if (u.UserRoleId!=null)//Changed by Lalit Arora
        {
        us = [SELECT Id, DeveloperName, Name FROM UserRole WHERE Id = :u.UserRoleId LIMIT 1];
        }region = u.Region__c;
        if(us!=null) //Changed by Lalit Arora
        {
        roleName = us.Name;
        }
        else 
        {
           roleName ='User has not Role'; 
        }
        Growth_Percentage__c growthPercent = new Growth_Percentage__c();
        User curUser = [SELECT Id, Territory__c, Region__c, Number_of_Accounts__c, Number_of_Sales_Call_of_OSR__c FROM User WHERE Id =: UserInfo.getUserId()];
        
        //User for dynamic showing of leaderboards YTD Drill down reports
        if(u.ProfileId == systemAdminProfileId){
            roleLevel = 'Admin';
            //ciList = [SELECT Id, zsx__CustomerInsightStatus__c FROM zsx__CustomerInsight__c WHERE OwnerId = :UserInfo.getUserId()];
            // ciARList = [SELECT count(Id) ct, SUM(CountStatus__c) status, SUM(RevenueAfterWon__c) rev, SUM(zsx__TotalOpportunityAmount__c) amt, SUM(Response_Account__c) acct FROM zsx__CustomerInsight__c WHERE OwnerId = :UserInfo.getUserId()];
            salesMaxLink = reportsIdMap.get('Salesmax_Usage_Analysis_SD');
            maydayLink = reportsIdMap.get('Mayday_Rnk_Sales_Director_CF');
            Focus5Link = reportsIdMap.get('Focus_5_QTD'); //Addition by Sandeep Meka 09/19/17 to make Focus 5 Drill Down for Everyone
        }
        
        else if(roleName.containsIgnoreCase('President') || roleName.containsIgnoreCase('Executive')){
            roleLevel = 'President';
            //ciList = [SELECT Id, zsx__CustomerInsightStatus__c FROM zsx__CustomerInsight__c WHERE Region__c = :curUser.Region__c];
            // ciARList = [SELECT count(Id) ct, SUM(CountStatus__c) status, SUM(RevenueAfterWon__c) rev, SUM(zsx__TotalOpportunityAmount__c) amt, SUM(Response_Account__c) acct FROM zsx__CustomerInsight__c WHERE IsSameRegion__c = true];
            salesMaxLink = reportsIdMap.get('Salesmax_Usage_Analysis_SD');
            maydayLink = reportsIdMap.get('Mayday_Rnk_Sales_Director_CF');
            Focus5Link = reportsIdMap.get('Focus_5_QTD'); //Addition by Sandeep Meka 09/19/17 to make Focus 5 Drill Down for Everyone
        }
        
        else if(roleName.containsIgnoreCase('Manage')){
            roleLevel = 'TSM';
            //ciList = [SELECT Id, zsx__CustomerInsightStatus__c FROM zsx__CustomerInsight__c WHERE Territory__c = :curUser.Territory__c];
            //ciARList = [SELECT count(Id) ct, SUM(CountStatus__c) status, SUM(RevenueAfterWon__c) rev, SUM(zsx__TotalOpportunityAmount__c) amt, SUM(Response_Account__c) acct FROM zsx__CustomerInsight__c WHERE IsSameTerritory__c = true
            //            AND zsx__CustomerInsightStatus__c != 'DISMISSED' AND zsx__CustomerInsightStatus__c != 'WON' AND zsx__CustomerInsightStatus__c != 'LOST'];
            salesMaxLink = reportsIdMap.get('Salesmax_Usage_Analysis_TSM');
            maydayLink = reportsIdMap.get('Mayday_Rank_TSM_CF');
            Focus5Link = reportsIdMap.get('Focus_5_QTD'); //Addition by Sandeep Meka 09/19/17 to make Focus 5 Drill Down for Everyone
        }
        
        else if(roleName.containsIgnoreCase('Rep')){
            roleLevel = 'OSR';
            //ciList = [SELECT Id, zsx__CustomerInsightStatus__c FROM zsx__CustomerInsight__c WHERE OwnerId = :UserInfo.getUserId()];
            // ciARList = [SELECT count(Id) ct, SUM(CountStatus__c) status, SUM(RevenueAfterWon__c) rev, SUM(zsx__TotalOpportunityAmount__c) amt, SUM(Response_Account__c) acct FROM zsx__CustomerInsight__c WHERE OwnerId = :UserInfo.getUserId()
            //            AND zsx__CustomerInsightStatus__c != 'DISMISSED' AND zsx__CustomerInsightStatus__c != 'WON' AND zsx__CustomerInsightStatus__c != 'LOST'];
            salesMaxLink = reportsIdMap.get('Salesmax_Usage_Analysis_OSR');
            maydayLink = reportsIdMap.get('Mayday_Rank_OSR_CF');
            Focus5Link = reportsIdMap.get('Focus_5_QTD'); //Addition by Sandeep Meka 09/19/17 to make Focus 5 Drill Down for Everyone
        }
        
        Date firstDayThisYear = Date.newInstance(Date.today().year(), 1, 1);
        String salesMaxQry = 'select SUM(Response_Account__c), SUM(Customer_Insight_Count__c), SUM(RevenueAfterWon__c), SUM(zsx__TotalOpportunityAmount__c) from zsx__CustomerInsight__c where (closed_date__c >= :firstDayThisYear or closed_date__c = null) ';
        
        if (roleLevel == 'Director') {
            salesMaxQry += 'and IsSameRegion__c = true ';
        } else {
            salesMaxQry += 'and IsSameTerritory__c = true ';
        }
        
   //     AggregateResult[] groupedResultsSalesMax = Database.query(salesMaxQry);
        
        Decimal sumResponseAccount = 0;
        Decimal sumCustomerInsightCount = 0;
        Decimal sumRevenueAfterWon = 0;
        Decimal sumTotalOpportunityAmount = 0;
        Decimal adoptionRate = 0;
        Decimal wonRevenueAfter = 0;
        Decimal winRate = 0;
        
    /*    for (AggregateResult ar : groupedResultsSalesMax) {
            sumResponseAccount = ar.get('expr0') == null ? 0 : (Decimal)ar.get('expr0');
            sumCustomerInsightCount = ar.get('expr1') == null ? 0 : (Decimal)ar.get('expr1');
            sumRevenueAfterWon = ar.get('expr2') == null ? 0 : (Decimal)ar.get('expr2');
            sumTotalOpportunityAmount = ar.get('expr3') == null ? 0 : (Decimal)ar.get('expr3'); 
            
            if (sumCustomerInsightCount != 0) {
                adoptionRate = sumResponseAccount/sumCustomerInsightCount;
            }
            
            wonRevenueAfter = sumRevenueAfterWon;
            if (sumTotalOpportunityAmount != 0) {
                winRate = sumRevenueAfterWon / sumTotalOpportunityAmount;
            }
        }
        */
        Integer i=0;
        data = new Map<Integer,gaugeData>();
        scData = new Map<Integer,gaugeData>();
        focusfivedata = new Map<Integer,gaugeData>();
        // prospectingData = new Map<Integer,gaugeData>();
        
        List<AggregateResult> st = new List<AggregateResult>();
        List<AggregateResult> targetAmounts = new List<AggregateResult>();
        List<Report> reportList = new List<Report>();
        
        //adoptionRateStr = adoptionRate.setScale(2).format();
        //winRateStr = winrate.setScale(2).format();
        //ytdsalesStr = ytdsales.setScale(2).format();
        
        adoptionRateStr = CommonUtil.convertToPercent(adoptionRate, 2);
        winRateStr = CommonUtil.convertToPercent(winRate, 2);
        wonRevenueAfterStr = String.ValueOf(wonRevenueAfter.round(roundingMode.HALF_UP).format());
        //End of SalesMax Details
        
        //Start of MTD Chart
        // String informaticaUserId = Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
        
        //  Period pd = CommonUtil.getFiscalYearPeriod(Date.today(), 'Month');//Today-1 , changes- Surabhi edited 2/1/2017
        Period pd = CommonUtil.getFiscalYearPeriod(Date.today().addDays(-1), 'Month'); // code to yesterday's dashboard
        // Period pd = CommonUtil.getFiscalYearPeriod(Date.today().addDays(-10), 'Month'); 
        
        // Modified by Chetan for Dashboard Changes : Start
        // Dashboard should reflect data for last day (-1)
        Date startDate = pd.startDate; 
        Date endDate = pd.endDate;
        //Date startDate = Date.today().addDays(-1); edited Surabhi 1/19- The -1 did not work 
        //Date endDate = Date.today();
        //Modified by Chetan for Dashboard Changes : End
        
        List<User> teamUserList = CommonUtil.getUserSetInOneTeamUnderLeader(UserInfo.getUserId());
        
        //AggregateResult[] groupedResultsThis = [SELECT SUM(Sales_Sum__c), SUM(Gross_Profit_Sum__c), SUM(Sales_Target_Growth_ThisMonth__c), SUM(GP_Target_Growth_ThisMonth__c), SUM(Sales_Target_Growth_Total_Month_Target__c), SUM(GP_Target_Growth_Total_Month_Target__c) FROM Sales_Target__c where ownerid != :informaticaUserId and ownerid in:teamUserList and Start_Date__c >= :startDate and Start_Date__c <= :endDate]
        //Surabhi A, edited 12/7, 
        // Use similar Query for focus five 
        List<Sales_Target__c> groupedResultsThis= [SELECT Sales_Sum__c, Gross_Profit_Sum__c,YTD_Sales__c,QTD_Sales__c , Sales_Target_Growth_ThisMonth__c, GP_Target_Growth_ThisMonth__c, Sales_Target_Growth_Total_Month_Target__c, GP_Target_Growth_Total_Month_Target__c  FROM Sales_Target__c where  ownerid =: UserInfo.getUserId()  and Start_Date__c >= :startDate and Start_Date__c <= :endDate];
        
        Decimal sumTotalSales = 0;
        Decimal sumTotalGP = 0;        
        Decimal sumTargetSales = 0;
        Decimal sumTargetGP = 0;
        Decimal sumTargetSalesMonthTotal = 0;
        Decimal sumTargetGPMonthTotal = 0;
        
        
        for (Sales_Target__c ar : groupedResultsThis) {
            sumTotalSales = ar.Sales_Sum__c;
            sumTotalGP = ar.Gross_Profit_Sum__c;
            sumTargetSales = ar.Sales_Target_Growth_ThisMonth__c;
            sumTargetGP = ar.GP_Target_Growth_ThisMonth__c; 
            sumTargetSalesMonthTotal = ar.Sales_Target_Growth_Total_Month_Target__c;
            sumTargetGPMonthTotal = ar.GP_Target_Growth_Total_Month_Target__c; 
        }
        
        List<String> reportDevNames = new List<String>{'Growth_Chart_MTD_Sales_New', 
            'Growth_Chart_QTD_Sales_New', 
            'Growth_Chart_YTD_Sales_New',
            'Growth_Chart_MTD_GP_New', 
            'Growth_Chart_QTD_GP_New', 
            'Growth_Chart_YTD_GP_New'
            };
                reportList = [SELECT Id, DeveloperName FROM Report WHERE DeveloperName IN : reportDevNames];
        Map<String, Id> reportMap = new Map<String, Id>();
        for(Report reportRec : reportList) {
            reportMap.put(reportRec.DeveloperName, reportRec.Id);
        }
        
        //Report rep = [SELECT Id, DeveloperName FROM Report WHERE DeveloperName = 'Growth_Chart_MTD_Sales' LIMIT 1];
        //Report gpRep = [SELECT Id, DeveloperName FROM Report WHERE DeveloperName = 'Growth_Chart_MTD_GP' LIMIT 1];
        String linktoReport = '/';
        String gpReportLink = '/';
        
        //data for MTD Chart
        GaugeData MTDMonth = new GaugeData('Sales Month', 'salesmonth', sumTotalSales, sumTargetSales, '/' + reportMap.get('Growth_Chart_MTD_Sales_New'));
        MTDMonth.wholePeriodTotal = String.ValueOf(sumTargetSalesMonthTotal.round(roundingMode.HALF_UP).format());
        data.put(0, MTDMonth);
        // End of MTD Chart
        
        List<Sales_Target__c> groupedResultsFocusFive= [SELECT Focus5_GP__c, Focus5_Sales__c, Focus5_Target__c FROM Sales_Target__c where  ownerid =: UserInfo.getUserId()  and Start_Date__c >= :startDate and Start_Date__c <= :endDate];
        
        Decimal sumTotalSalesFocusFive = 0;
        Decimal sumTotalGPFocusFive = 0;        
        Decimal sumTargetSalesFocusFive = 0;
        
        for (Sales_Target__c sr : groupedResultsFocusFive) {
            sumTotalSalesFocusFive = sr.Focus5_Sales__c;
            sumTotalGPFocusFive = sr.Focus5_GP__c;
            sumTargetSalesFocusFive = sr.Focus5_Target__c;
        }
        
        //data for Focus Five Details MTD Chart
        GaugeData FFCMonth = new GaugeData('Focus 5 Performance Quarter to Date', 'salesmonth3', sumTotalSalesFocusFive , sumTargetSalesFocusFive  , linktoReport);
        if(sumTotalGPFocusFive!=null){ //Added by Lalit Arora
        FFCMonth.wholePeriodTotal = String.ValueOf( sumTotalGPFocusFive.round(roundingMode.HALF_UP).format());
        }
        focusfivedata.put(0, FFCMonth);
        // End of Focus Five Details MTD Chart
        
        if(roleName.containsIgnoreCase('Rep')){    
            Decimal currentSc= 0, maxTargetSc = 1;
            curUser = [SELECT Id, Number_of_Accounts__c, Number_of_Sales_Call_of_OSR__c FROM User WHERE Id =: UserInfo.getUserId()];
            List<AggregateResult> aggTask = [SELECT Count(Id) taskCount 
                                             FROM TASK 
                                             WHERE (Sales_Rep_Type__c = 'OSR' OR Sales_Rep_Type__c = 'RSR')
                                             AND Status = 'Completed' 
                                             AND RecordType.Name = 'Sales Call'
                                             AND ActivityDate = THIS_WEEK
                                             AND OwnerId = :UserInfo.getUserId()];
            
            if(aggTask.get(0).get('taskCount')!= null) {
                currentSc  = (Decimal) aggTask.get(0).get('taskCount');
            }
            
            scData.put(0,new gaugeData('Sales Calls','salesmonth2', currentSc, curUser.Number_of_Accounts__c,linktoReport));
        } 
        else if(roleName.containsIgnoreCase('Manage')){
            Decimal currentSc= 0, maxTargetSc = 1;
            curUser = [SELECT Id, Territory__c, Number_of_Accounts__c, Number_of_Sales_Call_of_OSR__c FROM User WHERE Id =: UserInfo.getUserId()];
            List<AggregateResult> terAccounts = [SELECT SUM(Number_of_Accounts__c) accCounts FROM User WHERE Territory__c = :curUser.Territory__c];
            
            if(curUser.Number_of_Accounts__c == null){
                curUser.Number_of_Accounts__c = 1;
            }
            
            if(curUser.Number_of_Sales_Call_of_OSR__c  == null){ 
                curUser.Number_of_Sales_Call_of_OSR__c  = 0;
            }
            
            if(terAccounts.get(0).get('accCounts') != null){
                maxTargetSc = (Decimal) terAccounts.get(0).get('accCounts');
            }
            
            scData.put(0,new gaugeData('Sales Calls','salesmonth2', curUser.Number_of_Sales_Call_of_OSR__c, maxTargetSc, linktoReport));
        }
        else if(roleName.containsIgnoreCase('Director')){
            Decimal currentSc= 0, maxTargetSc = 1;
            curUser = [SELECT Id, Region__c , Territory__c, Number_of_Accounts__c, Number_of_Sales_Call_of_OSR__c FROM User WHERE Id =: UserInfo.getUserId()];
            List<AggregateResult> salescallscount = [SELECT SUM(Number_of_Sales_Call_of_OSR__c) salescallscount FROM User WHERE Region__c = :curUser.Region__c];            
            List<AggregateResult> regAccounts = [SELECT SUM(Number_of_Accounts__c) accCounts FROM User WHERE Region__c = :curUser.Region__c];
            
            if(salescallscount.get(0).get('salescallscount') != null){
                currentSc = (Decimal) salescallscount.get(0).get('salescallscount');
            }
            
            if(regAccounts.get(0).get('accCounts') != null){
                maxTargetSc = (Decimal) regAccounts.get(0).get('accCounts');
            }
            
            scData.put(0,new gaugeData('Sales Calls','salesmonth2', currentSc, maxTargetSc, linktoReport));
        }        
        else {
            Decimal currentSc= 0, maxTargetSc = 1;
            curUser = [SELECT Id, Territory__c, Number_of_Accounts__c, Number_of_Sales_Call_of_OSR__c FROM User WHERE Id =: UserInfo.getUserId()];
            List<AggregateResult> terAccounts = [SELECT SUM(Number_of_Accounts__c) accCounts FROM User];
            
            if(curUser.Number_of_Accounts__c == null){
                curUser.Number_of_Accounts__c = 1;
            }
            
            if(curUser.Number_of_Sales_Call_of_OSR__c  == null){
                curUser.Number_of_Sales_Call_of_OSR__c  = 0;
            }
            
            if(terAccounts.get(0).get('accCounts') != null){
                maxTargetSc = (Decimal) terAccounts.get(0).get('accCounts');
            }
            
            scData.put(0,new gaugeData('Sales Calls','salesmonth2', curUser.Number_of_Sales_Call_of_OSR__c, maxTargetSc, linktoReport));
        }
       
      
    }
    
    public void populateReportMap() {    
        //Map<String,String> reportsIdMap = new Map<String,String>();
        reportsIdMap = new Map<String,String>();
        dashboardsIdMap  = new Map<String,String>();
        List<Report> repList = [SELECT Id, DeveloperName FROM Report];
        List<Dashboard> dbList = [SELECT Id, DeveloperName FROM Dashboard];
        
        for(Report r: repList ){
            reportsIdMap.put(r.DeveloperName,r.Id);
        }
        
        for(Dashboard db: dbList ){
            dashboardsIdMap.put(db.DeveloperName,db.Id);
        }
    }
    
    public List<ProspectingData> getProspectData() {
        List<ProspectingData> dataList = new List<ProspectingData>();
        List<String> months = new List<String>{'January','February','March','April','May','June','July','August','September','October','November','December'};
            Map<String,AggregateResult> resMap = new Map<String,AggregateResult>();
        
        for(AggregateResult ar : [SELECT SUM(Year_To_Date__c) ytd, 
                                  SUM(Month_To_Date__c) mtd, 
                                  Month_Text__c month, 
                                  SUM(Target__c) target
                                  FROM Prospecting__c 
                                  WHERE Is_Latest__c = true 
                                  AND Report_Date__c = THIS_YEAR 
                                  AND IsSameTerritory__c = true 
                                  GROUP BY Month_Text__c])
        { 
            resMap.put((String) ar.get('month'),ar);
        }
        
        for(String month : months){
            if(resMap.containsKey(month)){
                AggregateResult ar = resMap.get(month);
                if(ar != null){
                    dataList.add(new ProspectingData((String) ar.get('month'),
                                                     (Decimal) ar.get('mtd'),
                                                     (Decimal) ar.get('ytd'),
                                                     (Decimal) ar.get('target')
                                                    )
                                );                
                }
            }
        }
        
        boolean isTest = false;
        if(isTest){
            dataList.clear();
            dataList.add(new ProspectingData('January',1,1,10));
            dataList.add(new ProspectingData('February',2,3,10));
            dataList.add(new ProspectingData('March',0,3,10));
            dataList.add(new ProspectingData('April',3,6,10));
            dataList.add(new ProspectingData('May',1,7,10));
            dataList.add(new ProspectingData('June',0,7,10));
            dataList.add(new ProspectingData('July',2,9,10));
            dataList.add(new ProspectingData('August',3,12,10));
            dataList.add(new ProspectingData('September',0,12,10));
            dataList.add(new ProspectingData('October',0,12,10));
            dataList.add(new ProspectingData('November',1,13,10));
            dataList.add(new ProspectingData('December',2,15,10));
        }
        return dataList;
    }
    
    
    public class gaugeData {
        public String name { get; set; }
        public String div { get; set; }
        public decimal score { get; set; }
        public decimal pct { get; set; }
        public decimal deg { get; set; }
        public decimal max { get; set; }
        public decimal pctRound {get; set;}
        public String maxFormat{get; set;}
        public String scoreFormat {get; set;}
        public String link {get;set;}
        public decimal lowRange { get; set; }
        public decimal highRange { get; set; }
        public decimal scDeg { get; set; }
        public String wholePeriodTotal {get; set;}
        
        public gaugeData(String name, String div, decimal scre, decimal max, String link) {
            if(max ==null || max == 0 ) { //Modified By Lalit
                max= 1; 
            } 
            
            this.link = link;
            //this.name = name;
            this.name = '';
            this.score = scre;
            this.div = div;
            this.max  = max;
            
            deg = 90;
            if(scre==null || scre ==0) // Added By Lalit
            {
                pct = 0 ;
                deg =0;
                scDeg =0;
            }
            else
            {
                pct = (scre / max ) * 100 ;
                deg = (scre / max) * 150;
                 scDeg = (scre / max) * 180;
            }
            
            
            if(pct > 120) {
                deg = 180; 
            }
            
           
            
            if(pct > 100) {
                scDeg = 180;              
            }
            
            this.pctRound = pct.intValue();
            // this.maxFormat = max.setScale(2).format();
            // this.scoreFormat = score.setscale(2).format();
            if (max!=null){ // Added By Lalit Arora
            this.maxFormat = String.ValueOf(max.round(roundingMode.HALF_UP).format());
            }
            if (score!=null)
            {
            this.scoreFormat = String.ValueOf(score.round(roundingMode.HALF_UP).format());
            }
        }
    }
    
    public class ProspectingData {
        public String month { get; set; }
        public Decimal Target { get; set; }
        public Decimal MTD { get; set; }
        public Decimal YTD { get; set; }
        public ProspectingData(String month, Decimal mtd, Decimal ytd, Decimal target) {
            this.month = month;
            this.target = target;
            this.mtd = mtd;
            this.ytd = ytd;
        }
    }   
}