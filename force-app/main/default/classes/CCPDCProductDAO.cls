public with sharing class CCPDCProductDAO {
    public static List<ccrz__E_Product__c> getParts(List<String> partNumbers, String storefront) {
        List<ccrz__E_Product__c> parts = [
            SELECT 
            Id, 
            Name,
            ccrz__SKU__c,
            Part_Number__c,
            ccrz__ProductStatus__c,
            Salespack__c,
            DSP_Part_Number__c               
            FROM 
            ccrz__E_Product__c
            WHERE
            (ccrz__SKU__c IN :partNumbers OR Name IN :partNumbers OR Part_Number__c IN :partNumbers)                
            AND ccrz__Storefront__c INCLUDES (:storefront) AND ccrz__ProductStatus__c = 'Released'
        ];
        return parts;
    }
    public static List<ccrz__E_Product__c> getPartsDsp(List<String> partNumbers, String storefront) {
        List<ccrz__E_Product__c> parts = [
            SELECT 
            Id, 
            Name,
            ccrz__SKU__c,
            Part_Number__c,
            DSP_Part_Number__c,
            Salespack__c,
            FP_Brand_Name__c,
            ccrz__ProductStatus__c
            FROM 
            ccrz__E_Product__c
            WHERE
            (ccrz__SKU__c IN :partNumbers OR Name IN :partNumbers OR Part_Number__c IN :partNumbers OR
             DSP_Part_Number__c IN :partNumbers ) AND ccrz__ProductStatus__c ='Released'
        ];
        
        System.debug('getPartsDsp'+parts.size());
        return parts;
    }    
    
    public static List<Account_Product_Cross_Reference__c> searchParts(String searchString, String accountId, String storefront) {
        List<Account_Product_Cross_Reference__c> parts = [
            SELECT 
            Id, 
            Name,
            Account_Part__c,
            Related_to_Product__c,
            Related_to_Product__r.ccrz__SKU__c,
            Related_to_Account__c
            FROM 
            Account_Product_Cross_Reference__c
            WHERE
            Account_Part__c = :searchString AND
            Related_to_Account__c = :accountId AND
            Related_to_Product__r.ccrz__Storefront__c INCLUDES (:storefront)
        ];
        return parts;
    }
    
    public static List<Product_Cross_Reference__c> searchParts(String searchString, String storefront) {
        String searchStringWOHyphen = searchString.replace('-', '');
        List<Product_Cross_Reference__c> parts = [
            SELECT 
            Id, 
            Name,
            Interchange_Part_Number__c,
            Related_to_Product__c,
            Related_to_Product__r.ccrz__SKU__c
            FROM 
            Product_Cross_Reference__c
            WHERE
            ( Interchange_Part_Number__c = :searchString or  Interchange_Part_Number__c = :searchStringWOHyphen)
            AND Related_to_Product__r.ccrz__Storefront__c INCLUDES (:storefront)
        ];
        return parts;
    }
    
    public static List<Product_Cross_Reference__c> getCrossReferencesForProduct(String sku, String storefront) {
        List<Product_Cross_Reference__c> parts = [
            SELECT 
            Id, 
            Name,
            Interchange_Part_Number__c,
            Brand_Name__c,
            Related_to_Product__c,
            Related_to_Product__r.ccrz__SKU__c
            FROM 
            Product_Cross_Reference__c where Related_to_Product__r.ccrz__Storefront__c INCLUDES (:storefront) and Related_to_Product__r.ccrz__SKU__c = :sku ORDER BY Brand_Name__c ASC NULLS LAST
        ];
        return parts;
    }
    
    
    public static ccrz__E_Product__c getProductTypeBySku(String sku) {
        ccrz__E_Product__c product = null;
        List<ccrz__E_Product__c> products = [
            SELECT 
            Id,
            ccrz__ProductType__c
            FROM 
            ccrz__E_Product__c 
            WHERE 
            ccrz__SKU__c = :sku AND ccrz__ProductStatus__c = 'Released'
            LIMIT  
            1
        ];
        
        if (!products.isEmpty()) {
            product = products[0];
        }
        
        return product;
    }
    
    public static List<ccrz__E_ProductSpec__c> getSpecsFromChildren(List<Id> children, List<Id> specIds) {
        List<ccrz__E_ProductSpec__c> specs = [
            SELECT 
            ccrz__Product__c,
            ccrz__Spec__c,
            ccrz__Spec__r.Name,
            ccrz__SpecValue__c
            FROM 
            ccrz__E_ProductSpec__c 
            WHERE 
            ccrz__Product__c IN :children AND ccrz__Spec__c IN :specIds 
        ];
        return specs;        
    }
    
    public static List<ccrz__E_CompositeProduct__c> getChildProducts(Id compositeId) {
        List<ccrz__E_CompositeProduct__c> children = [
            SELECT 
            Id,
            ccrz__Component__c
            FROM 
            ccrz__E_CompositeProduct__c 
            WHERE 
            ccrz__Composite__c = :compositeId AND
            (ccrz__Component__r.ccrz__ProductStatus__c = 'Released' OR ccrz__Component__r.ccrz__ProductStatus__c = 'Not Orderable')   AND  (ccrz__Component__r.ccrz__StartDate__c = null or ccrz__Component__r.ccrz__StartDate__c <= TODAY) AND (ccrz__Component__r.ccrz__EndDate__c = null or ccrz__Component__r.ccrz__EndDate__c >= TODAY)
        ];
        return children;
    }
    
    /**
* Returns a list of ccrz__E_ProductInventoryItem__c records for the specified skus. The
* list may be empty, but will not be null.
*/
    public static List<ccrz__E_ProductInventoryItem__c> getProductInventoryForSkus(List<String> skus){
        if(skus == null || skus.size() < 1){
            return new List<ccrz__E_ProductInventoryItem__c>();
        }
        if(test.isRunningTest()){
        return [
            SELECT
            Id
            , ccrz__QtyAvailable__c
            , Location__r.Location__c
            , Location__r.City__c // FE-359
            , Location__r.State__c
            , ccrz__InventoryLocationCode__c
            , Item_Type__c
            , ccrz__ProductItem__c
            , ccrz__ProductItem__r.ccrz__Sku__c
            FROM
            ccrz__E_ProductInventoryItem__c
            WHERE
            ccrz__ProductItem__r.ccrz__Sku__c in :skus ];    
        }
        return [
            SELECT
            Id
            , ccrz__QtyAvailable__c
            , Location__r.Location__c
            , Location__r.City__c // FE-359
            , Location__r.State__c
            , ccrz__InventoryLocationCode__c
            , Item_Type__c
            , ccrz__ProductItem__c
            , ccrz__ProductItem__r.ccrz__Sku__c
            FROM
            ccrz__E_ProductInventoryItem__c
            WHERE
            ccrz__ProductItem__r.ccrz__Sku__c in :skus and Location__r.Location_Status__c = 'Active' and Location__r.Location_Type__c ='DC' ORDER BY Location__r.State__c ASC
        ];
    }
    
     public static List<ccrz__E_ProductInventoryItem__c> getProductInventoryForSkusAndLocation(List<String> skus, List<String> locations){
        if(skus == null || skus.size() < 1){
            return new List<ccrz__E_ProductInventoryItem__c>();
        }
        
        return [
            SELECT
            Id
            , ccrz__QtyAvailable__c
            , Location__r.Location__c
            , Location__r.City__c // FE-359
            , Location__r.State__c
            , ccrz__InventoryLocationCode__c
            , Item_Type__c
            , ccrz__ProductItem__c
            , ccrz__ProductItem__r.ccrz__Sku__c
            FROM
            ccrz__E_ProductInventoryItem__c
            WHERE
            ccrz__ProductItem__r.ccrz__Sku__c in :skus and Location__r.Location__c in :locations
        ];
    }
    
    public static List<ccrz__E_ProductMedia__c> getProductThumbnailURIForSkus(List<String> skus){
        if(skus == null || skus.size() < 1){
            return new List<ccrz__E_ProductMedia__c>();
        }
        
        return [
            SELECT 
            ccrz__Product__r.ccrz__SKU__c,
            ccrz__URI__c 
            FROM ccrz__E_ProductMedia__c
            WHERE ccrz__Product__r.ccrz__SKU__c in :skus AND ccrz__MediaType__c = 'Product Image Thumbnail'];
        
    }
    
    public static Map<String, ccrz__E_Product__c> getProductsForPricingBySku(Set<String> skus) {
        Map<String, ccrz__E_Product__c> products = new Map<String, ccrz__E_Product__c>();
        List<ccrz__E_Product__c> items = [
            SELECT 
            Id, 
            Name,
            Part_Number__c,
            Pool_Number__c,
            ccrz__SKU__c
            FROM 
            ccrz__E_Product__c
            WHERE
            ccrz__SKU__c IN :skus 
        ];
        for (ccrz__E_Product__c p : items) {
            products.put(p.ccrz__SKU__c, p);
        }
        return products;
    }
    
    public static Map<String, ccrz__E_Product__c> getProductsForPricingById(Set<String> ids) {
        Map<String, ccrz__E_Product__c> products = new Map<String, ccrz__E_Product__c>();
        List<ccrz__E_Product__c> items = [
            SELECT 
            Id, 
            Name,
            Part_Number__c,
            Pool_Number__c,
            ccrz__SKU__c
            FROM 
            ccrz__E_Product__c 
            WHERE
            Id IN :ids AND ccrz__ProductStatus__c = 'Released'
        ];
        for (ccrz__E_Product__c p : items) {
            products.put(p.Id, p);
        }
        return products;
    }
    
    public static List<ccrz__E_RelatedProduct__c> getRequiredProductsBySku(Set<String> skus) {
        List<ccrz__E_RelatedProduct__c> items = [
            SELECT 
            Id, 
            Name,
            ccrz__RelatedProduct__c,
            ccrz__RelatedProduct__r.Part_Number__c,
            ccrz__RelatedProduct__r.Pool_Number__c,
            ccrz__RelatedProduct__r.ccrz__SKU__c,
            ccrz__Product__c,
            ccrz__Product__r.ccrz__SKU__c
            FROM 
            ccrz__E_RelatedProduct__c
            WHERE
            ccrz__Product__r.ccrz__SKU__c IN :skus AND ccrz__RelatedProductType__c = 'Required' 
        ];
        return items;
    }
    
    public static List<ccrz__E_RelatedProduct__c> getRequiredProductsById(Set<String> ids) {
        List<ccrz__E_RelatedProduct__c> items = [
            SELECT 
            Id, 
            Name,
            ccrz__RelatedProduct__c,
            ccrz__RelatedProduct__r.Part_Number__c,
            ccrz__RelatedProduct__r.Pool_Number__c,
            ccrz__RelatedProduct__r.ccrz__SKU__c,
            ccrz__Product__c,
            ccrz__Product__r.ccrz__SKU__c
            FROM 
            ccrz__E_RelatedProduct__c
            WHERE
            ccrz__Product__r.Id IN :ids AND ccrz__RelatedProductType__c = 'Required'
        ];
        return items;
    }
    
    public static List<ccrz__E_RelatedProduct__c> getRelatedProductsFromId(Set<String> productSfids){
        if(productSfids == null || productSfids.size() < 1){
            return new List<ccrz__E_RelatedProduct__c>();
        }
        return [
            SELECT 
            Id, 
            ccrz__RelatedProduct__c, 
            ccrz__RelatedProduct__r.ccrz__SKU__c,
            ccrz__Product__c,
            ccrz__Product__r.ccrz__ProductStatus__c
            FROM 
            ccrz__E_RelatedProduct__c 
            WHERE
            ccrz__Product__c IN :productSfids AND ccrz__RelatedProductType__c = 'Interchange' 
        ];
        
    }
    
    public static List<ccrz__E_RelatedProduct__c> getRelatedSupercessionProductsFromId(Set<String> productSfids){
        if(productSfids == null || productSfids.size() < 1){
            return new List<ccrz__E_RelatedProduct__c>();
        }
        return [
            SELECT 
            Id, 
            ccrz__RelatedProduct__c, 
            ccrz__RelatedProduct__r.ccrz__SKU__c,
            ccrz__Product__c,
            ccrz__Product__r.ccrz__ProductStatus__c
            FROM 
            ccrz__E_RelatedProduct__c 
            WHERE
            ccrz__Product__c IN :productSfids AND ccrz__RelatedProductType__c = 'Supercession' AND ccrz__Product__r.ccrz__ProductStatus__c = 'Released'
        ];
        
    }
    
    public static List<ccrz__E_Product__c> getSalespackForSkus(List<String> skus) {
        return  [
            SELECT 
            Id,
            ccrz__SKU__c,
            Salespack__c,
            FP_Salespack__c
            FROM 
            ccrz__E_Product__c 
            WHERE 
            ccrz__SKU__c IN :skus  AND ccrz__ProductStatus__c = 'Released'
            LIMIT  
            1
        ];
    }
    public static List<ccrz__E_Product__c> getSalespackForSkusMulti(List<String> skus) {
        return  [
            SELECT 
            Id,
            ccrz__SKU__c,
            Salespack__c,
            FP_Salespack__c
            FROM 
            ccrz__E_Product__c 
            WHERE 
            ccrz__SKU__c IN :skus  AND ccrz__ProductStatus__c = 'Released'
        ];
    } 
    
    // to get year based on product
    public Static List<Product_Fitment__c> getProductFitmentYear(String sku){

        List<Product_Fitment__c> productfitmentList  = [    
            SELECT 
            Year__c
            FROM 
            Product_Fitment__c 
            WHERE
            Related_to_Product__r.ccrz__SKU__c = :sku order by Year__c desc  LIMIT 5000       
        ];

        return productfitmentList ;     
    }  
    // to get make based on year ,product
    public Static List<Product_Fitment__c> getProductFitmentMake(String year,String sku){
        Integer yearInt = Integer.ValueOf(year);
        List<Product_Fitment__c> productfitmentList = [
            SELECT 
            Id, 
            Make__c,
            MakeDesc__c
            FROM 
            Product_Fitment__c 
            WHERE
            Year__c =: yearInt AND 
            Related_to_Product__r.ccrz__SKU__c  =: sku ORDER BY MakeDesc__c LIMIT 50000
        ];
        return productfitmentList ;     
        
    }  
    // to get model based on year,make ,product
    public Static List<Product_Fitment__c > getProductFitmentModel(String year,String make,String sku){
        Integer yearInt = Integer.ValueOf(year);
        List<Product_Fitment__c > productfitmentList = [
            SELECT 
            Id,
            Model__c,
            ModelDesc__c
            FROM 
            Product_Fitment__c 
            WHERE
            Year__c =: yearInt AND 
            Make__c =: make AND 
            Related_to_Product__r.ccrz__SKU__c  =: sku ORDER BY ModelDesc__c LIMIT 50000
        ];
        return productfitmentList ;    
        
    }  
    
    // to get Engine based on year,make,model,product
    public Static List<Product_Fitment__c > getProductFitmentEngine(String year,String make,String model,String sku){
        Integer yearInt = Integer.ValueOf(year);
        List<Product_Fitment__c > productfitmentList = [
            SELECT 
            Id,
            Engine__c,
            EngineDesc__c,
            Note__c
            FROM 
            Product_Fitment__c 
            WHERE
            Year__c =: yearInt AND 
            Make__c=: make AND 
            Model__c =: modeL AND
            Related_to_Product__r.ccrz__SKU__c =: sku LIMIT 50000
        ];
        
        return productfitmentList ;     
    } 
    
    //adding below method for product fitment based on Year, Make , Model, Engine ,product
   public Static List<Product_Fitment__c> getProductFitmentMessage(String year,String make,String model,String engine,String sku){
        Integer yearInt = Integer.ValueOf(year);
        List<Product_Fitment__c> productfitmentList = [
            SELECT 
            Id, 
            MyNote__c,
            NonfitmentNotes__c,
            NonfitmentNotes2__c,
            NonfitmentNotes3__c,
            Note__c,
            Note1__c,
            Note2__c,
            Note3__c,
            Note4__c
            FROM 
            Product_Fitment__c 
            WHERE
            Year__c =: yearInt AND 
            MakeDesc__c=: make AND 
            ModelDesc__c =: modeL AND 
            Note__c =: engine AND 
            Related_to_Product__r.ccrz__SKU__c = :sku  
        ];

        return productfitmentList ;     
        
    }
    // get product Invetory on PDP
        public static List<ccrz__E_ProductInventoryItem__c> getProductInventoryForSku(String sku,List<String> locations){
        
        return [
            SELECT
            Id
            , ccrz__QtyAvailable__c
            , ccrz__AvailabilityMessageRT__c
            , ccrz__InventoryLocationCode__c
            FROM
            ccrz__E_ProductInventoryItem__c
            WHERE
            ccrz__ProductItem__r.ccrz__Sku__c =:sku AND ccrz__InventoryLocationCode__c in :locations  LIMIT 5
        ];
    }
}