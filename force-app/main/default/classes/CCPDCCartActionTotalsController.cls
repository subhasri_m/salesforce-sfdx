/** ------------------------------------------------------------------------------------------------------
 * @Description - Controller class for Cart Action Buttons template
 *
 * @Author      
 * @Date        Sep 2017
 *-----------------------------------------------------------------------------------------------------*/
global with sharing class CCPDCCartActionTotalsController {
    /**
     * @Description - Remote action method to send email for approval
     *
     * @Author  
     * @Date    Sep 2017
     *
     * @param   ccrz.cc_RemoteActionContext 
     * @param   String  
     * @return  ccrz.cc_RemoteActionResult
     **/
    
    public CCPDCCartActionTotalsController() {}

    @RemoteAction
    global static ccrz.cc_RemoteActionResult submitForApproval(final ccrz.cc_RemoteActionContext ctx, final String cartId, String poNum) {
        // initialize the context
        ccrz.cc_RemoteActionResult res = ccrz.cc_CallContext.init(ctx);
        res.success = false;
        try {
            // check if the cart exist.
            List<ccrz__E_Cart__c> listCart = [SELECT Id,ccrz__Name__c,ccrz__CartStatus__c,ccrz__ActiveCart__c, ccrz__PONumber__c, ccrz__Contact__r.Name, ccrz__Contact__r.Email,ccrz__Account__r.Name FROM ccrz__E_Cart__c 
                                                WHERE ccrz__EncryptedId__c = :cartId];
            if (listCart.size() == 0) {
                res.data = 'Cart not found';
            }
            else {
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();                
                Id effAccountId = ccrz.cc_CallContext.effAccountId;
                Id contactId = ccrz.cc_CallContext.currContact.Id;
                Id cartSfid = listCart.get(0).Id;
                listCart.get(0).ccrz__ActiveCart__c = false;
                listCart.get(0).ccrz__PONumber__c = poNum;
                listCart.get(0).CC_FP_Needs_Approval__c = 'Awaiting Approval';
                //adding cart name for PE-1463
                String cartName = listCart.get(0).ccrz__Name__c;

                
                Boolean contactisAdmin = false;

                User currUserInfo = [SELECT ID, Full_Name__c, Username,FirstName,LastName FROM User WHERE ContactID =:contactId LIMIT 1];

                String theContactSubmitted = listCart.get(0).ccrz__Account__r.Name+ ' '+currUserInfo.Full_Name__c+' '+ currUserInfo.Username+' - Submitted for Approval';

                CCPDCUserPermissionsHelper.UserPermissions userPerm = CCPDCUserPermissionsHelper.getUserPermissions(effAccountId, contactId);

                // get admin email address.                
                //List<CC_FP_Contract_Account_Permission_Matrix__c> lstPermissionMatrix = CCFPContactAccountPermissionMatrixDAO.getPermissionsForAccount(effAccountId);
                List<CC_FP_Contract_Account_Permission_Matrix__c> lstPermissionMatrix = CCPDCCartActionTotalsControllerHelper.getAdminsForAccount(effAccountId);

                for(CC_FP_Contract_Account_Permission_Matrix__c matrix : lstPermissionMatrix){
                    if(String.valueOf(matrix.Contact__c) == String.valueOf(contactId)){
                        contactisAdmin = true;
                    }
                }

                List<Account> parentAccounts = [SELECT Id, ParentId FROM Account WHERE Id =: effAccountId];
                System.debug('#### parentAccounts '+parentAccounts);
                List<String> lstToAddresses = new List<String>();

                if((!parentAccounts.isEmpty() && lstPermissionMatrix.isEmpty()) || contactisAdmin) {
                    Account paccount = parentAccounts.get(0);
                    lstPermissionMatrix = CCPDCCartActionTotalsControllerHelper.getAdminsForAccount(paccount.ParentId);
                }
              
                                
                for(CC_FP_Contract_Account_Permission_Matrix__c permissionMatrix : lstPermissionMatrix) {                    
                    if (permissionMatrix.Contact__c != null && permissionMatrix.Contact__r.Email != null) {                        
                        lstToAddresses.add(permissionMatrix.Contact__r.Email); 
                    }                                              
                }
                User adminUserTocheck = null;

                if(lstPermissionMatrix != null && !lstPermissionMatrix.isEmpty()){
                    Id theAdminContact = (lstPermissionMatrix.get(0)).Contact__c;
                    List<User> adminUser = [SELECT Id, Name,Username,Full_Name__c,FirstName,LastName FROM User WHERE contactid =:theAdminContact];
                    System.debug('#### theAdminContact '+theAdminContact);
                    if(!adminUser.isEmpty()){
                        System.debug('#### adminUser '+adminUser);
                        adminUserTocheck = adminUser.get(0);
                        System.debug('@@Rahul '+ cartSfid);
                        System.debug('@@Rahul '+ adminUserTocheck.Id);
                        System.debug('@@Rahul '+ theAdminContact);
                        CCPDCChangeOwnerHelper.changeCartOwnerForApproval(cartSfid,adminUserTocheck.Id,theAdminContact);
                        //listCart.get(0).ccrz__Contact__c = adminUserTocheck.Id;
                    }
                }
                String adminInfo = '';
                String adminInfoFirstName = '';
                String adminInfoLastName = '';
                if(adminUserTocheck != null){
                    adminInfo = adminUserTocheck.Full_Name__c+' '+adminUserTocheck.Username;
                    adminInfoFirstName = adminUserTocheck.FirstName;
                    adminInfoLastName = adminUserTocheck.LastName;
                }

                update listCart;
                
                /* Added to Update the PO Number */
                List<ccrz__E_CartItem__c> items = new List<ccrz__E_CartItem__c>();
                List<ccrz__E_CartItem__c> cartItems = new List<ccrz__E_CartItem__c>(
                                                      [Select Id, Name, CC_FP_PONumber__c From ccrz__E_CartItem__c Where 
                                                                  ccrz__Cart__r.ccrz__EncryptedId__c =: cartId]);
                //System.debug('#### cartItems '+cartItems);
                for (ccrz__E_CartItem__c item : cartItems) {
                    ccrz__E_CartItem__c Cartitem = new ccrz__E_CartItem__c(Id = item.Id, CC_FP_PONumber__c = poNum);
                    items.add(Cartitem);
                }
                update items;
                ccrz.ccLog.log(' Cart Item List ' + items);
                //System.debug('#### items '+items);
                /* End */

                // if no admin found.
                if (lstToAddresses.size() == 0) {
                    res.data = 'No Admin on Account';
                } else {
                  //  OrgWideEmailAddress[] owea = [select Id, Address from OrgWideEmailAddress 
                  //                                 where Address = 'help@fleetpride.com'];
                    OrgWideEmailAddress[] owe = [select Id, Address from OrgWideEmailAddress 
                                                   where Address =:System.label.FromEmail];
                  //  Organization org = [Select Id, IsSandbox From Organization LIMIT 1];
                  //  if (org.isSandbox && owea.size() > 0 ) {
                  //      message.setOrgWideEmailAddressId(owea.get(0).Id);
                  //  }
                  //  if ( !org.isSandbox && owe.size() > 0 ) {
                        message.setOrgWideEmailAddressId(owe.get(0).Id);
                  //  }
                    res.data = 'success';
                    message.toAddresses = lstToAddresses;
                    message.optOutPolicy = 'FILTER';
                    //Cart_Approval_Email_Customization__mdt[] cEmailData = [select Email_Body_Para_1__c,Email_Body_Para_2__c,Email_Body_Para_3__c,Footer_Image_URL__c,Subject_Line__c from Cart_Approval_Email_Customization__mdt where QualifiedApiName ='PDC_Cart_Submitted'];//RB
                    message.subject = 'PDC Cart Submitted for Approval';
                   // message.subject = cEmailData[0].Subject_Line__c;
                    String url = ccrz.cc_CallContext.currURL;                    
                    Integer index = url.indexOf('refURL');
                    if (index > -1) {
                        url = url.substring(0, index-1);
                    }
                    url = url.replace('cartId', 'cartID');
                    System.debug('#### URL => '+url);    
                    ccrz.ccLog.log('approval email url is' + Url);
                    String emailBody = '<p><font face="verdana">';
                    //emailBody+= 'Hello, <br /><br/>';
                    //Changed for PE-1463
                    emailBody+= adminInfoFirstName+', <br /><br/>';
                  // emailBody+= cEmailData[0].Email_Body_Para_1__c;
                    emailBody+= 'An order has been submitted for your approval on <a href="https://www.partsdistributing.com/">partsdistributing.com</a>.';
                    emailBody+= ' Click <a href="' + Url + '">here</a> to view the order from the <i>My Carts</i> page.<br/><br />'; // URL
                   //  emailBody+= ' <br /><br />'; // RB
					//emailBody+= cEmailData[0].Email_Body_Para_2__c;// RB
                    emailBody+='To submit this order for shipment, please complete the checkout process. '+' <br /><br />';
                    //emailBody+= ' <br /><br />'; // RB
                    emailBody+= 'Order Information:<br>';
                    //emailBody+= 'Cart for Approval: '+theContactSubmitted+'<br/>';
                    //Changed for PE-1463
                    emailBody+= 'Cart for Approval: '+cartName+'<br/><br/>';

                    //emailBody+= 'Approving User: '+adminInfo+'<br/>';
                    //Changed for PE-1463
                    emailBody+= 'Cart Owner: ' +currUserInfo.FirstName+ ' ' +currUserInfo.LastName+ '<br/>';
                    emailBody+= 'Approving User: '+adminInfoFirstName+'  ' +adminInfoLastName+ '<br/><br/>';
                    //emailBody+= 'Please let us know if we can be of any further assistance.';
                    //Changed for PE-1463
                    //emailBody+= cEmailData[0].Email_Body_Para_3__c; RB
                    emailBody+= 'Please contact <a href="mailto:pdcsales@partsdistributing.com">pdcsales@partsdistributing.com</a> if we can be of any further assistance.';
                    emailBody+='</font></p><br/> <br/>';
                  //emailBody+= cEmailData[0].Footer_Image_URL__c;
                    emailBody+='<img src="https://fleetpride--c.na87.content.force.com/servlet/servlet.ImageServer?id=0151W000003EDhL&oid=00D40000000Mwmn&lastMod=1546534707000" />';
                    message.htmlBody = emailBody;
                    
                    Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                    
                    if (results[0].success) {
                        System.debug('The email was sent successfully.');
                    } else {
                        System.debug('The email failed to send: ' + results[0].errors[0].message);
                    }
                    res.success = true;
                   
                }
            }
        }catch(Exception e) {
          CCAviPageUtils.buildResponseData(res, false,
                  new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
          );
        }
        return res;
    }


}