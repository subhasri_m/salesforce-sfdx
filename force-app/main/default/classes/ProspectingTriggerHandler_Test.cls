@isTest
public class ProspectingTriggerHandler_Test{
    
    //Setup Data
    @testSetup static void setupTestData(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        //Create User Data
        User u = new User(Alias = 'newUser', Email='test123@email.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='test123123@email.com',
                           Create_Accounts__c = true, Salesman_Number__c = '1-1234');   
        insert u;
        
        //Create Exisiting Prospecting Data
        Prospecting__c prospect = new Prospecting__c(); 
        prospect.Id__c = '1234';
        prospect.Year_to_Date__c = 1;
        prospect.Month_to_Date__c = 1;
        prospect.Report_Date__c = date.newInstance(2016, 1, 10);
        
        insert prospect;
    }
    
    public static TestMethod void insertProspecting(){
        
        List<Prospecting__c> prosList = new List<Prospecting__c>();
        
        //Populate Prospecting List to Insert.
        for(Integer i=0; i<3; i++ ){
            Prospecting__c pros = new Prospecting__c();
           
            pros.Id__c = '1234';
            pros.Year_to_Date__c = i;
            pros.Month_to_Date__c = i;
            pros.Report_Date__c = date.newInstance(2016, 1, 10);
            prosList.add(pros);
        }
        
        Test.startTest();
        
        insert prosList;
        
        Test.stopTest();
        
        List<Prospecting__c> prospectList = [Select Id, Is_Latest__c FROM Prospecting__c WHERE Is_Latest__c = true];
        // System.assertEquals(prospectList.size(), 1);
    }
    

}