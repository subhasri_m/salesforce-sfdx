public with sharing class CCPDCProductCategoryDAO {
    
    public static List<ccrz__E_ProductCategory__c> getProductCategorySKUs(Id categoryId) {
        List<ccrz__E_ProductCategory__c> items = [
            SELECT 
                Id, 
                ccrz__Product__r.ccrz__SKU__c, 
                ccrz__Product__r.Id, 
                ccrz__Product__r.ccrz__Sequence__c, 
                ccrz__Category__c 
            FROM ccrz__E_ProductCategory__c 
            WHERE ccrz__Category__r.Id = :categoryId  
        ];
        return items;
    }
}