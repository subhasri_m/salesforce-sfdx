@isTest
public without sharing class TestSalesOrderSummaryTrigger {

    static testMethod void deleteOld_Owner_tasks(){
    
User user1 = new User(alias = 'ceo', email='admin@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-88888',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTas@testorg.com', profileid = '00e400000013ttZ', Create_Accounts__c = true);
        insert user1;
User user2 = new User(alias = 'ceo2', email='admin2@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-99999',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='admin2@testorg.com', profileid = '00e400000013ttZ');
        insert user2;
        

System.runas(user1){

    Sales_Target__c Target = new Sales_Target__c (ownerid = user1.id, Sales_Plan_Amount__c = 1000, Start_Date__c = date.newInstance( 2014, 1, 25 ) );
    Sales_Target__c Target2 = new Sales_Target__c (ownerid = user2.id, Sales_Plan_Amount__c = 1000, Start_Date__c = date.newInstance( 2014, 1, 25 ) );
    insert Target;
    insert Target2;

    TriggerSettings__c settings = new TriggerSettings__c(SalesOrderSummaryMapTarget__c = true);
    insert settings;
    
    Account acc = new Account(Name = 'Test Trigger', OwnerId = User1.id);
    insert acc;
    
    Sales_Order_Summary__c sos = new Sales_Order_Summary__c(Account__c= acc.id, OwnerID = User1.id, Update__c = '9', Order_Date__c = date.newInstance( 2014, 1, 25 ));
    Insert sos;
    
    acc.OwnerId = User2.id;
    Update Acc;
    
    sos.update__c = '10.0';
    Update sos;
}


}}