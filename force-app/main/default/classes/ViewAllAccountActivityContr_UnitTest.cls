@isTest
public class ViewAllAccountActivityContr_UnitTest {
    @testSetup
    public static void testSetup(){
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama11uu.com',
            Username = 'puser000@amamama1177.com' + System.currentTimeMillis(),
            CompanyName = 'TEST_SEDE',
            Title = 'title_test',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Create_Accounts__c = true,
            LocaleSidKey = 'en_US'
        );
        insert u;
        System.runAs(u){
            Account acc = new Account(Name = 'Test', Type = 'Customer', OwnerId = u.Id,
                                      Customer_Call_Classification__c =  'A');  
            insert acc;
            System.assertNotEquals(null,acc.id)  ;  
            contact con = new contact (lastname = 'checkme',firstname='check',Accountid = acc.id,OwnerId=u.id,Email='test@test.com');    
            insert con;
            Event newEvent = new Event();
            newEvent.OwnerId = u.Id;
            newEvent.Subject ='Test';
            newEvent.WhatId = acc.Id;
            newEvent.IsRecurrence = true;
            newEvent.RecurrenceStartDateTime = System.today();
            newEvent.RecurrenceEndDateOnly = System.today()+30;
            newEvent.RecurrenceType = 'RecursDaily';
            newEvent.RecurrenceInterval = 1; // This means that the event will wait 1 day before recurring again
            newEvent.IsAllDayEvent =true;
            newEvent.DurationInMinutes =1440;
            newEvent.ActivityDate = System.today();
            insert newEvent;
            Task t = new Task(
                Priority ='High',
                Description='Test',
                Subject = 'Email',
                OwnerId = u.Id,
                WhatId = acc.Id,
                status = 'New',
                ActivityDate = SYSTEM.today()
            );
            insert t;
            Task t1 = new Task(
                Priority ='High',
                Description='Test',
                Subject = 'Email',
                OwnerId = u.Id,
                WhatId = acc.Id,
                status = 'New',
                ActivityDate = SYSTEM.today(),
               
                Recurring_Task_ID__c = T.id
            );
            insert t1;
            Task t2 = new Task(
                Priority ='High',
                Description='Test',
                Subject = 'Call',
                OwnerId = u.Id,
                WhatId = acc.Id,
                status = 'New',
                ActivityDate = SYSTEM.today(),
               
                Recurring_Task_ID__c = T1.id
            );
            insert t2;
            
                   Task newEvent1 = new Task();
            newEvent1.OwnerId = u.Id;
            newEvent1.Subject ='Test22222';
            newEvent1.WhatId = acc.Id;
            newEvent1.IsRecurrence = true;
            newEvent1.RECURRENCESTARTDATEONLY = System.today();
            newEvent1.RecurrenceEndDateOnly = System.today()+30;
            newEvent1.RecurrenceType = 'RecursDaily';
            newEvent1.RecurrenceInterval = 1; // This means that the event will wait 1 day before recurring again 
            insert newEvent1;
            SYSTEM.debug('newEvent1'+newEvent1);
            
        }
    }
    
    private static testMethod void unitTest_1(){
        Test.setCurrentPage(Page.ViewAllAccountActivity);
        ApexPages.currentPage().getParameters().put('Accid',[Select Id From Account LIMIT 1].Id);
        ApexPages.currentPage().getParameters().put('taskIdParam', [Select Id From Task Where Subject= 'Call' LIMIT 1].Id);
        ViewAllAccountActivityController sch = new ViewAllAccountActivityController();
       ViewAllAccountActivityController.TaskScheduleWrapper wrp= new ViewAllAccountActivityController.TaskScheduleWrapper();
        ViewAllAccountActivityController.ActivityWrapperClass wr  = new ViewAllAccountActivityController.ActivityWrapperClass();
        Task t = [Select Id, Subject, Status, WhatId, Description, Priority From Task Limit 1];
        wr.Selected = true;
        wr.DueDateDay=String.valueOf(System.today()); 
        wr.TaskId =t.Id;
        wr.Subject =t.Subject;
        wr.Status=t.Status;
        wr.DueDate=date.today(); 
        wr.whatid =t.WhatId;
        wr.Description ='SDJS';
        wr.Priority=t.Priority;
        wr.PriorityNumber =0;
        wrp.RecurrenceType = 'Weekly';
        sch.ScheduleWrapper = wrp; 
        sch.RECURRENCEDAYOFMONTH_Input = 10;
        sch.RecurrenceInterval_input = 10;
        sch.RECURRENCEINSTANCE_input = '10';
        sch.WeekdayMonthly_Input = 10;
        sch.IsMonSelected = true;
        sch.IsTueSelected = true;
        sch.IsWedSelected = true;
        sch.IsThuSelected = true;
        sch.IsFriSelected = true;
        sch.HiddenTaskId = [Select Id From Task LIMIT 1].Id;
        sch.getItems();
        sch.getRecurrenceTypeOptions();
        sch.getRecWeekDay();
        sch.getRecInstances();
        sch.showPopup();
        sch.ScheduleSalesCallClicked();
        sch.CalculateWeekMask();
        sch.closePopup();
        sch.redirectPopup();
        sch.IsRecurrenceChangedMethod();
        sch.GotoRecurrenceTask();
        sch.UpdateRecurrenceSchedule([Select Id From Task LIMIT 1].Id,[Select Id From Task where isRecurrence =false LIMIT 1].Id);
        sch.getUserWeekTasks();
        sch.UserWeekTasksGenerator();
        sch.updateTableData();
        sch.deleteRecurrenceSeries();
        sch.UpdateTaskRecords(new List<ViewAllAccountActivityController.ActivityWrapperClass> {wr});
    }
}