global class BatchSalesOrderSummaryScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        // sync last year's sales target first and the finish method will run the calculation batch BatchSalesOrderSummary
        Period pd = CommonUtil.getFiscalYearPeriod(CommonUtil.getTheSameDayLastYear(Date.today()), 'Year');
        Date sinceDate = pd.startDate; 
        Date toDate = pd.endDate;
        
        BatchSalesTargetSyncWithLastYear bth = new BatchSalesTargetSyncWithLastYear(sinceDate, toDate, 10);
        
        Database.executeBatch(new BatchSalesTargetSyncWithLastYear(sinceDate, toDate), 200);
    }
}