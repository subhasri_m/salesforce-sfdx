@IsTest
public class Batch_DeleteOrphanTasksTest {
    @testSetup
    public static void generateTestData(){
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama112244.com',
            Username = 'puser000@amamama1122.com' + System.currentTimeMillis(),
            CompanyName = 'TEST_SEDE',
            Title = 'title_test',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Create_Accounts__c = true,
            LocaleSidKey = 'en_US'
        );
        insert u;
        
        User u1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last2',
            Email = 'puser000@amamama112244.com',
            Username = 'puser000@amamama112222.com' + System.currentTimeMillis(),
            CompanyName = 'TEST_SEDE',
            Title = 'title_test',
            Alias = 'alias2',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Create_Accounts__c = true,
            LocaleSidKey = 'en_US'
        );
        insert u1;
        
        System.runAs(u){
            Account acc = new Account(Name = 'Test Trigger', ownerId=u1.Id);
            insert acc;
            System.debug('@@Rahul1 '+ acc.OwnerId);
            
            Task t2 = new Task(Subject = 'ABC Test', Status = 'Not Started', Objective__c = 'Housekeeping', Type = 'Sales Call', TaskSubtype='Task', ActivityDate = date.newinstance(2019, 10, 01), WhatID = acc.id);
            insert t2;
            Task t3 = new Task(Subject = 'ABC Test2', Status = 'Not Completed', Objective__c = 'Housekeeping', Type = 'Sales Call', TaskSubtype='Task', ActivityDate = date.newinstance(2020, 10, 01), WhatID = acc.id);
            insert t3;
           }
    }
    public static testMethod void Batch_DeleteOrphanTasks_Test(){
        Test.startTest();
            Database.executeBatch(new Batch_DeleteOrphanTasks());
        Test.stopTest();
    }

}