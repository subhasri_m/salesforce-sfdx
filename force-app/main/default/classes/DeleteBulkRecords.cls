public class DeleteBulkRecords implements database.Batchable<sObject> {
    
    List<DeleteBulkRecords__c> CustomSetting = DeleteBulkRecords__c.getAll().Values();
    
    public database.QueryLocator start (Database.BatchableContext BC)
    {
        String query = 'select id from '+CustomSetting[0].ObjectName__c+' ' +CustomSetting[0].Limit__c ;
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject>scope)
    {
        List<Sobject> RecordDelete = new List<Sobject>();
       // sObject sObj = Schema.getGlobalDescribe().get(ObjectName).newSObject() ;
        
        for (sobject s : scope){
             sObject sObj = Schema.getGlobalDescribe().get(CustomSetting[0].ObjectName__c).newSObject() ;
            sObj = (sObject)s;
            RecordDelete.add(sObj);                  
        }
        
        database.delete(recordDelete, false);
        
    }
    
    public void finish(Database.BatchableContext BC)
    {
        
    }
}