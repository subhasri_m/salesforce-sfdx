/** ------------------------------------------------------------------------------------------------------
 * @Description - Used to create the valid link 'Pay With BillTrust' to the BillTrust 
 *
 * @Author      
 * @Date        Oct 2017
 *-----------------------------------------------------------------------------------------------------*/
global with sharing class CCPDCMyAccountNavController {

    global String billConnectURL {get; set;}
    global string bataDataToEncrypted { get; set; }
    global string encryptedData { get; set; }
    global string etType { get; set; }
    global String BillTrustKey { get; set; }
    public String EffectiveAccountId {get; set ;}
    
    /**
     * @Description - controller
     *
     * @Author  
     * @Date    Oct 2017
     *
     * @param   none  
     * @return  none
     **/
    global CCPDCMyAccountNavController() {
    
           EffectiveAccountId  =apexpages.currentpage().getparameters().get('EffectiveAccount');
        billConnectURL = '';
        
        //Get the Effective Account & the custom setting values to build the link to BillTrust
        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = ccrz.cc_CallContext.currContact.Id;
        
        String AccountNumberBranchOnly ='';
        String AccountNumberParsed ='';
        String LoggedinUserName ='';
        String LoggedinUserEmail ='';

        CCPDCUserPermissionsHelper.UserPermissions permissions = CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);
        
        if(permissions.canViewInvoices) { //permissions.allowInvoice && 
            String storefront = ccrz.cc_CallContext.storefront;
            
            Account account = [SELECT Id,ISeries_Account_ID__c, AccountNumber FROm Account WHERE Id = :accountId];
            List<User> LoggedInUser = [Select id,Username,email,contactid from user where contactid = :contactId ];
            
            if (!LoggedInUser.isEmpty())
            {
                LoggedinUserName = LoggedInUser[0].Username;
                LoggedinUserEmail = LoggedInUser[0].email;
                
            }
            
            if (account!=null && account.AccountNumber!=null){
                AccountNumberBranchOnly = account.ISeries_Account_ID__c.substring(12);
                AccountNumberParsed  = account.AccountNumber.substring(2);
                if (AccountNumberParsed.contains('-0') && AccountNumberBranchOnly =='000' )
                {
                     AccountNumberParsed = AccountNumberParsed.replace('-0', '');
                }
                
                else
                {
                    AccountNumberParsed = AccountNumberParsed.substringBeforeLast('-');
                    AccountNumberParsed = AccountNumberParsed +'-'+AccountNumberBranchOnly;
                 }
            }

            CCAviBillTrustSettings__c settings =  CCAviBillTrustSettings__c.getInstance(storefront);
            
            if (settings != null) {
                String prefix = '?BTDATA=';
                /*bataDataToEncrypted = '"AN=' + account.AccountNumber + ';PB=' + 
                                settings.PB__c + ';EN=' + settings.EN__c + ';EC=' + settings.EC__c + ';TS=' + 
                                System.now().format('yyyy-MM-dd HH:mm:ss') + '"&CG="' + 
                                settings.ClientGUID__c+'"';*/
                BillTrustKey = Label.BillTrustKey;                
                bataDataToEncrypted = 'EA='+LoggedinUserEmail+';UN='+
                LoggedinUserName+';AN=' + AccountNumberParsed + ';PB=' + settings.PB__c + ';EN=' + settings.EN__c + ';EC=' + 
                settings.EC__c + ';TS=' + System.now().format('yyyy-MM-dd HH:mm:ss') + ''; 
                
                
                etType =  '&CG="' + settings.ClientGUID__c +'"&ETYPE=' + settings.ETYPE__c;           
                
                billConnectURL = settings.URL_For_eInvoiceConnect__c + prefix;
                System.debug('BillConnectURL-->'+billConnectURL);
            }            
        }
        
    }
    
}