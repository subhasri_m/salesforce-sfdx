@isTest
public with sharing class CCPDCChangeOwnerHelperTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void changeCartOwnerTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        Test.startTest();
        CCPDCChangeOwnerHelper.changeCartOwner(cart.Id, UserInfo.getUserId());
        Test.stopTest();

        ccrz__E_Cart__c c = [SELECT Id, OwnerId FROM ccrz__E_Cart__c WHERE Id = :cart.Id];

        System.assertEquals(UserInfo.getUserId(), c.OwnerId);
    }

    static testmethod void changeCartOwnerAllTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();

        Test.startTest();
        CCPDCChangeOwnerHelper.changeCartOwnerAll(cart.Id, UserInfo.getUserId());
        Test.stopTest();

        ccrz__E_Cart__c c = [SELECT Id, OwnerId FROM ccrz__E_Cart__c WHERE Id = :cart.Id];

        System.assertEquals(UserInfo.getUserId(), c.OwnerId);
    }

    static testmethod void changeCartOwnerAll2Test() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        Id newAccount = ccrz.cc_CallContext.currAccountId;
        Id newContact = ccrz.cc_CallContext.currContact.Id;

        Test.startTest();
        CCPDCChangeOwnerHelper.changeCartOwnerAll(cart.Id, UserInfo.getUserId(), newContact, newAccount);
        Test.stopTest();

        ccrz__E_Cart__c c = [SELECT Id, OwnerId FROM ccrz__E_Cart__c WHERE Id = :cart.Id];

        System.assertEquals(UserInfo.getUserId(), c.OwnerId);
    }

    static testmethod void changeOrderOwnerAllTest() {
        util.initCallContext();
        ccrz__E_Order__c order = util.getOrder();
        Id newAccount = ccrz.cc_CallContext.currAccountId;
        Id newContact = ccrz.cc_CallContext.currContact.Id;

        Test.startTest();
        CCPDCChangeOwnerHelper.changeOrderOwnerAll(order.Id, UserInfo.getUserId(), newContact, newAccount);
        Test.stopTest();

        ccrz__E_Order__c c = [SELECT Id, OwnerId FROM ccrz__E_Order__c WHERE Id = :order.Id];

        System.assertEquals(UserInfo.getUserId(), c.OwnerId);
    }

    static testmethod void updateCartTest() {
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        cart.OwnerId = UserInfo.getUserId();

        Test.startTest();
        CCPDCChangeOwnerHelper.updateCart(cart);
        Test.stopTest();

        ccrz__E_Cart__c c = [SELECT Id, OwnerId FROM ccrz__E_Cart__c WHERE Id = :cart.Id];

        System.assertEquals(UserInfo.getUserId(), c.OwnerId);
    }

}