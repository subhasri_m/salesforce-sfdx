@isTest
public class CCPDCOrderConfirmationControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();

            ccrz__E_Cart__c cart = util.createCartWithSplits(m);
            cart.CC_FP_Is_Will_Call__c = true;
            update cart;
            ccrz__E_Order__c order = util.createOrderWithSplits(m, cart);
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

    }

    static testmethod void controllerTest() {
        util.initCallContext();

        ccrz__E_Order__c order = util.getSplitOrder();

        CCPDCOrderConfirmationController c = null;
        ApexPages.currentPage().getParameters().put('o', order.ccrz__EncryptedId__c);
        
        Test.startTest();
        CCPDCFulfillmentHelper.copyCartToOrderData(order.Id);
        c = new CCPDCOrderConfirmationController();
        Test.stopTest();

        System.assert(c != null);
        System.assert(c.orderItemMapJSON != null);
        System.assert(c.ordrItmGrpMapJSON != null); 
        System.assert(c.dcLocationsMapJSON != null); 
        System.assert(c.ffType != null); 
        System.assert(c.thisDcJSON != null); 
        System.assert(c.coreProductCharges != null); 
    }
}