@isTest
public class Batch_UpdateQuantityOnInventoryTest { 
    
    
       public static testMethod void blockProductTest(){
               User u = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama11.com',
             Username = 'puser000@amamama11.com' + System.currentTimeMillis(),
             CompanyName = 'TEST_SEDE',
             Title = 'title_test',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             Create_Accounts__c = true,
             LocaleSidKey = 'en_US'
        );
        insert u;
        System.runAs(u){
            ccrz__E_Product__c product = new ccrz__E_Product__c();
            product.Name = 'Shock Absorber';
            product.ccrz__SKU__c = 'Shock Absorber';
            product.ccrz__Quantityperunit__c = 898;
            product.ccrz__StartDate__c = System.today();
            product.ccrz__EndDate__c = System.today().addDays(90);
            product.ccrz__Storefront__c = 'pdc';
            product.DSP_Part_Number__c = 'NHJHJYJGUJY';
            product.Pool_Number__c = 'NHJHJYJGU';
            
            insert product;
            
            ccrz__E_product__c product1 = new ccrz__E_product__c();
            product1.Name = 'Shock Absorber New';
            product1.ccrz__SKU__c = 'Shock Absorber New';
            product1.ccrz__Quantityperunit__c = 898;
            product1.ccrz__StartDate__c = System.today();
            product1.ccrz__EndDate__c = System.today().addDays(90);
            product1.ccrz__Storefront__c = 'pdc';
            product1.DSP_Part_Number__c = 'NHJHJY';
            product1.Pool_Number__c = 'NGJTJYHJG';
            
            insert product1;
            
            ccrz__E_product__c product2 = new ccrz__E_product__c();
            product2.Name = 'Shock Absorber WEE';
            product2.ccrz__SKU__c = 'Shock Absorber GSF';
            product2.ccrz__Quantityperunit__c = 898;
            product2.ccrz__StartDate__c = System.today();
            product2.ccrz__EndDate__c = System.today().addDays(90);
            product2.ccrz__Storefront__c = 'pdc';
            product2.DSP_Part_Number__c = 'NHJHJYWEW';
            product2.Pool_Number__c = 'NGJTJYHJGWWE';
            
            insert product2;
            
            ccrz__E_Cart__c cart = new ccrz__E_Cart__c();
            cart.ccrz__CartStatus__c = 'Open';
            insert cart;
            
            ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c();
            cartItem.ccrz__Cart__c = cart.Id;
            cartItem.ccrz__Price__c = 998;
            cartItem.ccrz__Quantity__c = 20;
            cartItem.ccrz__OriginalItemPrice__c = 2000;
            cartItem.ccrz__Product__c = product.id;
            insert cartItem;
            
            ccrz__E_CartItem__c cartItem1 = new ccrz__E_CartItem__c();
            cartItem1.ccrz__Cart__c = cart.Id;
            cartItem1.ccrz__Price__c = 998;
            cartItem1.ccrz__Quantity__c = 20;
            cartItem1.ccrz__OriginalItemPrice__c = 200;
            cartItem1.ccrz__Product__c = product1.id;
            insert cartItem1;

            ccrz__E_PriceList__c priceList = new ccrz__E_PriceList__c();
            priceList.Name = 'Test PriceList';
            priceList.ccrz__StartDate__c = System.today();
            priceList.ccrz__EndDate__c = System.today().addDays(90);
            priceList.ccrz__Storefront__c = 'pdc';
            insert priceList;
            
            ccrz__E_PriceListItem__c priceListIitem = new ccrz__E_PriceListItem__c();
            priceListIitem.ccrz__Pricelist__c = priceList.Id;
            priceListIitem.ccrz__EndDate__c = System.today().addDays(90);
            priceListIitem.ccrz__StartDate__c = System.today();
            priceListIitem.ccrz__MaxQty__c = 2938;
            priceListIitem.ccrz__MinQty__c = 10;
            priceListIitem.ccrz__Price__c = 293;
            priceListIitem.ccrz__Product__c = product.Id;
            
            insert priceListIitem;
            
            ccrz__E_PriceListItem__c priceListIitem1 = new ccrz__E_PriceListItem__c();
            priceListIitem1.ccrz__Pricelist__c = priceList.Id;
            priceListIitem1.ccrz__EndDate__c = System.today().addDays(90);
            priceListIitem1.ccrz__StartDate__c = System.today();
            priceListIitem1.ccrz__MaxQty__c = 2938;
            priceListIitem1.ccrz__MinQty__c = 10;
            priceListIitem1.ccrz__Price__c = 293;
            priceListIitem1.ccrz__Product__c = product1.Id;
            
            insert priceListIitem1;
            
            Product_Cross_Reference__c prodCrossReference = new Product_Cross_Reference__c();
            prodCrossReference.Related_to_Product__c = product.Id;
            insert prodCrossReference;
            
            Product_Cross_Reference__c prodCrossReference1 = new Product_Cross_Reference__c();
            prodCrossReference1.Related_to_Product__c = product1.Id;
            insert prodCrossReference1;
            
            Location__c loc = new Location__c();
            loc.Location_Ext_ID__c ='123SS';
            Loc.City__c = 'IRVING';
            loc.State__c = 'TX';
            Loc.Address_Line_1__c  ='600 LAS COLINAS BLVD E STE 400';
            Insert loc;
            
            ccrz__E_ProductInventoryItem__c inventory = new ccrz__E_ProductInventoryItem__c();
            inventory.ccrz__ProductItem__c  =  product1.Id;
            inventory.External_ID__c = '123';
            inventory.Location__c = loc.id;
            insert inventory;
        }
        Test.startTest();
        	database.executebatch(new Batch_UpdateQuantityOnInventoryRecords()); 
        Test.stopTest();
    }

}