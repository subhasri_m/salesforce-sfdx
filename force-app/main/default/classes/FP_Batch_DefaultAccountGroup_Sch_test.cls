@isTest
private Class FP_Batch_DefaultAccountGroup_Sch_test {

        @testSetup static void setup() { 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'newUser', Email='newuser1234@sampleeeee.commm', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='exampletest@batch.so',
                           Create_Accounts__c = true);   
        insert u;
    }
    
      @isTest static void januaryTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 01, 01);
     	ccrz__E_AccountGroup__c accgroup = new ccrz__E_AccountGroup__c(Name = 'FP-AllAccounts');  
        insert accgroup;
        Account acc = new Account(Name = 'Test', Type = 'Customer',Iseries_Company_code__c='2');  
        insert acc;
        Account acc1 = new Account(Name = 'Test', Type = 'Customer',Iseries_Company_code__c='1');  
        insert acc1;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc1.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

          
          FP_Batch_DefaultAccountGroup_Sch scheduler = new FP_Batch_DefaultAccountGroup_Sch();
          scheduler.execute(null);
          Test.stopTest();
      }
  }
  
}