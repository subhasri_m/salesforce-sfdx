public class CCAviProductDao {
    public static List<ccrz__E_Product__c> getProducts(List<ID> ids) {
        List<ccrz__E_Product__c> products = [SELECT Id,ccrz__SKU__c,Freight_Attribute__c FROM ccrz__E_Product__c WHERE ID IN :ids];
        return products;
    }

    public static Map<String,Decimal> getProductsWeight(List<ID> ids){
    	 Map<String,Decimal> productsWeightMap = new Map<String,Decimal>();
		 for(ccrz__E_Product__c prod : [SELECT Id, ccrz__ProductWeight__c FROM ccrz__E_Product__c WHERE ID IN :ids]){
		 	productsWeightMap.put(String.valueOf(prod.Id), prod.ccrz__ProductWeight__c);
		 }
		 return productsWeightMap;
    }
}