global class Batch_Update_Snapshot implements Schedulable {
   global void execute(SchedulableContext sc) {
      Id batchInstanceID = Database.executeBatch(new Update_Snapshot());
   }
}