@isTest
public class CCPDCCheckoutReviewControllerTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();

            ccrz__E_Cart__c cart = util.getCart();
            ccrz__E_CartItem__c item = util.getCartItem();
            ccrz__E_Product__c product = util.getProduct();
            ccrz__E_Product__c requiredProduct = util.createRequiredProduct(product);

            CC_FP_Required_Cart_Item__c requiredItem = new CC_FP_Required_Cart_Item__c(
                CC_Cart__c = cart.Id,
                CC_Parent_Cart_Item__c = item.Id,
                Quantity__c = 1,
                Price__c = 5.00,
                SubAmount__c = 5.00,
                CC_Product__c = requiredProduct.Id
            );
            insert requiredItem; 
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
    }

    //static testmethod void controllerTest() {
    //    util.initCallContext();

    //    CCPDCCheckoutReviewController result = null;

    //    Test.startTest();
    //    result = new CCPDCCheckoutReviewController();
    //    Test.stopTest();

    //    System.assert(result != null);
    //}

    static testmethod void updateCartTest() {
        util.initCallContext();

        List<String> responses = new List<String>{CCPDCOMSFulfillmentPlanAPITest.OMS_2_RESPONSE};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        cart.CC_FP_Tax_Rate__c = .06;
        update cart;
        
        String data = '{"sfid":"' + cart.Id + '"}';

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCCheckoutReviewController.updateCart(ctx, data);
        Test.stopTest();

        System.assert(result != null);
      //  System.assert(result.success);
    }

    static testmethod void updateCartFPTest() {
        util.initCallContext();

        List<String> responses = new List<String>{CCPDCOMSFulfillmentPlanAPITest.OMS_2_RESPONSE};
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', responses));

        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Location__c = util.getLocation().Id;
        cart.CC_FP_Tax_Rate__c = .06;
        update cart;
        
        String data = '{"sfid":"' + cart.Id + '"}';

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCCheckoutReviewController.updateCartFP(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }
    static testmethod void fetchCoreChargesTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c item = util.getCartItem();
        item.CC_FP_QTY_Shipping__c = 1;

        update item;
        ccrz__E_Product__c product = util.getProduct();
        //ccrz__E_Product__c requiredProduct = util.createRequiredProduct(product);

        CC_FP_Required_Cart_Item__c requiredItem = new CC_FP_Required_Cart_Item__c(
            CC_Cart__c = cart.Id,
            CC_Parent_Cart_Item__c = item.Id,
            Quantity__c = 1,
            Price__c = 5.00,
            SubAmount__c = 5.00,
            CC_Product__c = product.Id
        );
        insert requiredItem; 

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCCheckoutReviewController.fetchCoreCharges(ctx);
        System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);

    }
    static testmethod void fetchProductRestrictionsATCheckoutTest(){
       ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        String input = '{"firstName":"Todde","lastName":"Mitchell","address1":"5119 HIGHGROVE LN NW","city":"ROCHESTER","state":"MN","countryCode":"US","postalCode":"55901","liftGate":true,"type":"Commercial","sfid":"125161"}';
        result = CCPDCCheckoutReviewController.fetchProductRestrictionsATCheckout(ctx,input);
        System.assert(result != null);
    }
      static testmethod void deleteMultipleCartItemsTest() {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.FulFillMentType__c = 'LocalDelivery';
        List<String> data = new List<String>();
        data.add(cartItem.id);
        String serializedData = JSON.serialize(data);
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCCheckoutReviewController.deleteMultipleCartItems(ctx, serializedData);
        Test.stopTest();
        System.assert(result.success == true);
    }  
        
}