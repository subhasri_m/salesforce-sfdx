/**
    * Ca_SalesOrderSummary_owner_update_Test- <Code Coverage Test class for Ca_UpdateSOSOwner>
    * @author: Surabhi Agrawal, Cloudaction
    * @version: 1.0
*/
@isTest
private class Ca_TestUpdateSOSOwner {
//11/24
    static testmethod void test() {
      
   

      Test.startTest();
      //String CRON_EXP = '0 0 0 15 3 ? 2022';
      //System.schedule('BatchSalesOrderSummaryScheduler', CRON_EXP, new BatchSalesOrderSummaryScheduler());
      
    //  Period pd = CommonUtil.getFiscalYearPeriod(CommonUtil.getTheSameDayLastYear(Date.today()), 'Year');
   //     Date sinceDate = pd.startDate; 
  //    Date toDate = pd.endDate;
      
    //  Database.executeBatch(new BatchSalesTargetSyncWithLastYear(sinceDate, toDate), 50);
      
      
      User user1 = new User(alias = 'ceo', email='admin@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-88888',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTas@testorg.com', profileid = '00e400000013ttZ', Create_Accounts__c = true);
        user1.Growth_Rate_Q1__c = 10;
      user1.Growth_Rate_Q2__c = 10;
      user1.Growth_Rate_Q3__c = 10;
      user1.Growth_Rate_Q4__c = 10;
        insert user1;
        
        
        User user2 = new User(alias = 'ceo2', email='admin2@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing2', languagelocalekey='en_US',
        Salesman_Number__c = '3-818888',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTas2@testorg.com', profileid = '00e400000013ttZ', Create_Accounts__c = true);
        user1.Growth_Rate_Q1__c = 10;
      user1.Growth_Rate_Q2__c = 10;
      user1.Growth_Rate_Q3__c = 10;
      user1.Growth_Rate_Q4__c = 10;
        insert user2;
        
       
      System.runAs(user1)
          
       
      { CommonContextDataPrepareTest.prepareData();
         //Account acc2 = new Account(Name = 'Test Acc2', OwnerId =user2.id , AccountNumber = '1-1221-1', Salesman_Number__c='3-818888' );
        //insert acc2;
       
       
          Account acc = [select id from Account limit 1];
          acc.Salesman_Number__c='3-818888';
          acc.ownerid = user2.id;
          update acc; 
      
      
      //Database.executeBatch(new BatchSalesTargetRegen());
      
      Sales_Order_Summary__c sos = [select id from Sales_Order_Summary__c limit 1];
     // Database.executeBatch(new Ca_BatchUpdateSOSOwner());
        
        Ca_UpdateSOSOwner usos = new Ca_UpdateSOSOwner();

            Id batchId = Database.executeBatch(usos);
      //Database.executeBatch(new BatchSalesTargetRegen(sos.id));
      
      //Database.executeBatch(new BatchSalesTargetSyncWithLastYear(sinceDate, toDate), 50);
      }
      Test.stopTest();
      
      // System.assertEquals(i, 0);

    }

}