global class DeleteOverdueTasks implements Database.Batchable<sObject>
{

    String query, value, field;
    
    global DeleteOverdueTasks()
    {
    query = 'SELECT Id FROM TASK where Overdue_task__c > 14';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       return Database.getQueryLocator(query); 
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Delete Scope;
    }
     
    global void finish(Database.BatchableContext BC)
    {
    }
}