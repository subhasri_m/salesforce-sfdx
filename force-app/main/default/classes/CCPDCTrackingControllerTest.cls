@isTest
public class CCPDCTrackingControllerTest {

public static testmethod void getCarrierURLTest(){
    
    List<String> strlst = new List<String> {'UPS','AACT','RNLO','CNWY','FEDEX','CENF','FXFE','LTLG','SEFL','SAIA','DHRN','SPEDE',null};
   PageReference page = Page.CCPDCTracking;
   CCPDCTrackingController trackCon = new CCPDCTrackingController();
    for (String str : strlst)  {
  	page.getParameters().put('carriercode', str);
	page.getParameters().put('trackingNumber','12112121');
	Test.setCurrentPage(page);
	PageReference page1 = trackCon.getCarrierURL(); 
        if (str!=null)
    system.assertEquals(str,trackCon.Carrier);    
    }  
   

}
}