@isTest
public class CCAviOrderManagerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void reorderTest() {
        util.initCallContext();
        ccrz__E_Order__c order = util.getOrder();

        String data = null;
        Test.startTest();
        data = CCAviOrderManager.reorder(order.Id);
        Test.stopTest();

        System.assert(data != null);
    }

}