@isTest
public class customCartControllerTest {

     static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    } 

    static testmethod  void checkTop3cartItems(){
      util.initCallContext();
      ccrz__E_Product__c product = util.getProduct();
      ccrz__E_ProductMedia__c pm = new ccrz__E_ProductMedia__c();
                pm.ccrz__Product__c = product.Id;
                pm.ccrz__AltMessage__c = 'Image Alt';
                pm.ccrz__Enabled__c = True;
                pm.ccrz__MediaType__c = 'Product Image';
                pm.ccrz__ProductMediaSource__c = 'Test Source';
                pm.ccrz__ProductDescription__c = 'Test Image';
            
      insert pm;
      ccrz__E_Cart__c cart = util.getCart();
      ccrz__E_CartItem__c cartItem = util.getCartItem();
      customCartController CCC = new  customCartController();
      CCC.cartId= cart.id;
     // customCartController.CartItemWrapper citmwrap = new customCartController.CartItemWrapper();
       List<customCartController.CartItemWrapper> citmwrap =  ccc.Top3cartItems;
            }

}