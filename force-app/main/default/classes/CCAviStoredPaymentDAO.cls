public with sharing class CCAviStoredPaymentDAO {
    
    public static ccrz__E_StoredPayment__c getStoredPayment(Id theId) {
        ccrz__E_StoredPayment__c payment = null;
        List<ccrz__E_StoredPayment__c> payments = [
            SELECT 
                Id,
                ccrz__Account__c,
                ccrz__AccountNumber__c,
                ccrz__AccountType__c,
                ccrz__DisplayName__c,
                ccrz__EffectiveAccountID__c,
                ccrz__Enabled__c,
                ccrz__ExpMonth__c,
                ccrz__ExpYear__c,
                ccrz__Name__c,
            	ccrz__Token__c
            FROM 
                ccrz__E_StoredPayment__c 
            WHERE 
                Id = :theId 
            LIMIT  
                1
            ];

        if (!payments.isEmpty()) {
            payment = payments[0];
        }
        
        return payment;
    }
}