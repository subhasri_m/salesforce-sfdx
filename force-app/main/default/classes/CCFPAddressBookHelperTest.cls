@isTest
public class CCFPAddressBookHelperTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void getAddressBookForAccountShippingTest() {
        util.initCallContext();

        List<ccrz.cc_bean_MockContactAddress> items = null;

        Test.startTest();
        items = CCFPAddressBookHelper.getAddressBookForAccountShipping(ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(items != null);
        System.assertEquals(1, items.size());
    }

}