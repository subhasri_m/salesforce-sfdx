/*
    @Author - Amit Singh
    @Description - Test class to test the functionality and covegare if PowerBIControllerExtension, PowerBISilentOAuthController,
                   PowerBIControllerTester, PowerBIExtensionTester and PowerBIGetAccountGUID class
    @CreatedDate - 25th Jan 2018
    @Type - Apex Class

*/

@isTest
public class PowerBIControllerExtension_Test{
    @testSetup
    public static void setupTest_Data(){
     User u = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser0001@amamama.com',
             Username = 'puser0001@amamama.com' + System.currentTimeMillis(),
             CompanyName = 'TEST1',
             Title = 'titl1e',
             Alias = 'alia1',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             Create_Accounts__c = true,
             LocaleSidKey = 'en_US'
        );
        insert u;
        
        System.runAs(u){
            Account account = new Account(Name ='Test Account111');
            insert account;
            OAuthApp_pbi_native__c customSetting = new OAuthApp_pbi_native__c(
                Name='PowerBINative',
                Access_Token_URL__c = 'https://login.windows.net/common/oauth2/token',
                Authorization_URL__c = 'https://login.windows.net/common/oauth2/token',
                Client_ID__c ='2db99c57-1150-498c-a932-9dda34e9de38',
                OAuth_Password__c = 'fle1@PRI',
                OAuth_Username__c = 'service-powerbi@fleetpride.com',
                PowerBI_API_URL__c = 'https://analysis.windows.net/powerbi/api',
                Resource_URI__c = 'https://api.powerbi.com/v1.0/myorg',
                Token_Expires_On__c ='23876'
            );
            insert customSetting;
            
            PowerBI_IDs__c PbId_1 = new PowerBI_IDs__c(
                Name = 'CC_Reporting',
                pbi_datasetID__c = 'b3046b15-a7b4-4882-b6e8-0a138dac62f5',
                pbi_DefaultPage__c = 'ReportSection',
                pbi_groupID__c = '382e35bf-b8a0-4242-bd02-ca8fe0023f40',
                pbi_iPadPage__c = 'ReportSection',
                pbi_MobilePage__c = 'ReportSection',
                pbi_reportID__c = '6a087c7e-6f17-46f7-97ce-887c17af7fb1',
                pbi_role__c = 'PDCUser'
            );
            insert PbId_1;
            
            PowerBI_IDs__c PbId = new PowerBI_IDs__c(
                Name = 'PowerBIAccount',
                pbi_datasetID__c = '4d5f3aa1-a6d9-412b-b7fa-72a9b5d1795c',
                pbi_DefaultPage__c = 'ReportSection',
                pbi_groupID__c = '382e35bf-b8a0-4242-bd02-ca8fe0023f40',
                pbi_iPadPage__c = 'ReportSection3a604b2a46ecb078e608',
                pbi_MobilePage__c = 'ReportSection991b79f536087ccd044b',
                pbi_reportID__c = '8f1ea098-2618-4c6d-ac4c-98cc45f7cfb7',
                pbi_role__c = 'AllRead'
            );
            insert PbId;
       }
    }
    
    static testMethod void unit_Test(){
    
        List<Account> acc = new List<Account>([Select Id, Name From Account Where Name ='Test Account111']);
        ApexPages.StandardController stdController = new ApexPages.StandardController(acc[0]);
        
        PowerBIExtensionTester extension = new PowerBIExtensionTester(stdController);
        Test.startTest();
            Test.setCurrentPage(Page.CC_Reporting);
            ApexPages.currentPage().getParameters().put('pbi_id', 'PowerBIAccount');
            Cookie accessToken = new Cookie('pbi_AccessToken', 'result.access_token',null,-1,false);
            Cookie refreshToken = new Cookie('pbi_RefreshToken', 'result.refresh_token',null,-1,false);
            Cookie expiresOn = new Cookie('pbi_ExpiresOn','result.expires_on',null,-1,false);
            Cookie pbiToken = new Cookie('pbi_Token', 'result2.token', null,-1,false);
            ApexPages.currentPage().setCookies(new Cookie[]{accessToken,refreshToken,expiresOn,pbiToken}); 
            
            PowerBIControllerTester controller = new PowerBIControllerTester();
            Test.setMock(HttpCalloutMock.class, new PowerBIControllerTesterTest_Mock());
            controller.AuthenticateFromPage();
            
            //controller.PBIaccess_token ;
            controller.PBIrefresh_token = 'Test';
            controller.validateResult = 'Test';
            controller.validateResult2 = 'Test';
            controller.PBIexpires_on = 'Test';
            controller.PBIToken = 'Test';
            controller.getHasToken();
            controller.silentAuth();
            controller.getValidateResult();
        Test.stopTest();
    }
    
    static testMethod void unit_Test_1(){
    
        List<Account> acc = new List<Account>([Select Id, Name From Account Where Name ='Test Account111']);
        ApexPages.StandardController stdController = new ApexPages.StandardController(acc[0]);
        
        PowerBIControllerExtension extension = new PowerBIControllerExtension(stdController);
        Test.startTest();
            Test.setCurrentPage(Page.CC_Reporting);
            ApexPages.currentPage().getParameters().put('pbi_id', 'PowerBIAccount');
            ApexPages.currentPage().getParameters().put('pbi_id222', 'PowerBIAccount');
            
            String testURL = ApexPages.currentPage().getUrl().substringAfter('/apex/');
            System.debug('#### testURL');
            
            Cookie accessToken = new Cookie('pbi_AccessToken', 'result.access_token',null,-1,false);
            Cookie refreshToken = new Cookie('pbi_RefreshToken', 'result.refresh_token',null,-1,false);
            Cookie expiresOn = new Cookie('pbi_ExpiresOn','result.expires_on',null,-1,false);
            Cookie pbiToken = new Cookie('pbi_Token', 'result2.token', null,-1,false);
            ApexPages.currentPage().setCookies(new Cookie[]{accessToken,refreshToken,expiresOn,pbiToken}); 
            
            PowerBISilentOAuthController controller = new PowerBISilentOAuthController();
            Test.setMock(HttpCalloutMock.class, new PowerBIControllerTesterTest_Mock());
            controller.AuthenticateFromPage();
            
            //controller.PBIaccess_token ;
            controller.PBIrefresh_token = 'Test';
            controller.validateResult = 'Test';
            controller.validateResult2 = 'Test';
            controller.PBIexpires_on = 'Test';
            controller.PBIToken = 'Test';
            controller.getHasToken();
            controller.silentAuth();
            controller.getValidateResult();
        Test.stopTest();
    }
    
    static testMethod void unit_Test_2(){
    
        List<Account> acc = new List<Account>([Select Id, Name From Account Where Name ='Test Account111']);
        
        Test.startTest();
            Test.setCurrentPage(Page.CC_Reporting);
            ApexPages.currentPage().getParameters().put('account', acc[0].Id);
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(acc[0]);
            PowerBIGetAccountGUID extension = new PowerBIGetAccountGUID(stdController);
            
        Test.stopTest();
    }
}