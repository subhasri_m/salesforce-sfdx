@isTest
public with sharing class CCPDCUserPermissionsHelperTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void getUserPermissionsTest() {
        util.initCallContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        CCPDCUserPermissionsHelper.UserPermissions permission = null;
        Test.startTest();
        permission = CCPDCUserPermissionsHelper.getUserPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id);
        Test.stopTest();

        System.assert(permission != null);
        System.assertEquals(true, permission.isAdmin);
    }
}