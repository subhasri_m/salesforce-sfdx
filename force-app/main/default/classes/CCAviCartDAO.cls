public with sharing class CCAviCartDAO {

    public static ccrz__E_Cart__c getCartForQuote(String encryptedId) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                Name,
                ccrz__Account__c,
                ccrz__ActiveCart__c,
                ccrz__CartStatus__c,
                CC_FP_Is_Free_Shipping__c,
                CC_FP_Free_Shipping_Type__c,
                ccrz__CartType__c,
            	ccrz__ShipTo__c,	
                (SELECT ccrz__Product__c, ccrz__Product__r.Name, ccrz__Product__r.ccrz__SKU__c,ccrz__Price__c, ccrz__Quantity__c,FulFillMentType__c FROM ccrz__E_CartItems__r)
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c  = :encryptedId
        ];

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }

        return cart;
    }

    public static ccrz__E_CartItem__c getCartItemById(Id cartItemId){
        ccrz__E_CartItem__c cartItem;
        try {
            cartItem = [
                SELECT 
                    Id,
                    ccrz__Cart__c, 
                    ccrz__Product__c, 
                    ccrz__PricingType__c, 
                    ccrz__CartItemType__c, 
                    ccrz__ProductType__c, 
                    ccrz__ItemStatus__c, 
                    ccrz__StoreId__c, 
                    ccrz__OriginalItemPrice__c, 
                    ccrz__OriginalQuantity__c, 
                    ccrz__RequestDate__c, 
                    ccrz__Price__c, 
                    CC_FP_PONumber__c,
                    ccrz__Quantity__c,
                    FP_contain_Core_Price__c,
                    CC_FP_DoNotReprice__c,
                	FulFillMentType__c,
                	ccrz__Cart__r.id,
                	ccrz__SubAmount__c
                FROM 
                    ccrz__E_CartItem__c
                WHERE
                    Id = :cartItemId
            ];
        }
        catch(Exception e){
            // no-op, return null
        }
        return cartItem;
    }

    public static ccrz__E_Cart__c getContactInfoInCart(String encryptedId){
        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
            Id,
            ccrz__Account__r.Name,
            ccrz__Contact__r.Name,
            ccrz__Contact__r.Email
            FROM
        ccrz__E_Cart__c
            WHERE
            ccrz__EncryptedId__c  = :encryptedId
        ];

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }
        return cart;

    }
}