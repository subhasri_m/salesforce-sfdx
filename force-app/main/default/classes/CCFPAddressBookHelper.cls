public with sharing class CCFPAddressBookHelper {

    public static List<ccrz.cc_bean_MockContactAddress> getAddressBookForAccountShipping(Id accountId) {
        List<ccrz.cc_bean_MockContactAddress> beans = new List<ccrz.cc_bean_MockContactAddress>();
        List<ccrz__E_AccountAddressBook__c> books = CCFPAddressBookDAO.getAddressBooksForAccountShipping(accountId);
        if (books != null) {
            for (ccrz__E_AccountAddressBook__c b : books) {
                beans.add(new ccrz.cc_bean_MockContactAddress(b.ccrz__E_ContactAddress__r));
            }
        }
        return beans;
    }
}