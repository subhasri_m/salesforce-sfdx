public with sharing class CCAviOrderManager {
    private static final Integer API_VERSION = 6;

    public static String reorder(String theId) {
        String data = null;

        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => API_VERSION, 
            ccrz.ccApiOrder.PARAM_ORDER_ID => theId
        };
  
        Map<String,Object> response = null;
  
        try {
            response = ccrz.ccAPIOrder.reorder(request);
            Boolean isSuccess = (Boolean) response.get(ccrz.ccAPI.SUCCESS);
            if (isSuccess) {
                data = (String)response.get(ccrz.cc_hk_Order.PARAM_ENC_CART_ID);
            }
        }
        catch(Exception e) {
            System.debug(e);
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            throw new OrderManagerException(e.getMessage());
        }
        return data;
    }

    public class OrderManagerException extends Exception {}
}