@isTest
public class CCPDCContactUsFormControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void createContactUsMessageTest() {
        util.initCallContext();
        
        String data = '{"companyName":"Acme","contactEmail":"user@test.com","contactName":"Test User","phoneNumber":"800-888-8888","ccontactReason":"Because","message":"Hi","Address1":"33 N LaSalle","Address2":"","City":"Chicago","contactusstate":"IL","ZipCode":"60606","Country":"USA"}';

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        ccrz.cc_RemoteActionResult result = null;

        Test.startTest();
        result = CCPDCContactUsFormController.createContactUsMessage(ctx, data);
        Test.stopTest();

        System.assert(result != null);
        System.assert(result.success);
    }

    static testmethod void getAccountInfoTest() {
        util.initCallContext();

        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();

        Map<String, String> result = null;

        Test.startTest();
        result = CCPDCContactUsFormController.getAccountInfo(ctx);
        Test.stopTest();

        System.assert(result != null);
        System.assertEquals(3, result.size());
    }
}