@isTest 
public class CCPDCHeaderBIEControllerTest {
    
    static testMethod void TestMethod_1()
    {
        PDC_Holiday__c holiday = new PDC_Holiday__c(); 
        holiday.Closure_Reason__c='Holiday';
        holiday.Description__c ='check1';
        holiday.Notes__c='cecede';
        holiday.area__c = 'Illinois';
        holiday.PDC_Closed_Date__c = date.today();
        insert holiday;
        CCPDCHeaderBIEController controller = new CCPDCHeaderBIEController();
        
    }
    
    static testMethod void TestMethod_2()
    {
        PDC_Holiday__c holiday = new PDC_Holiday__c(); 
        holiday.Closure_Reason__c='Inclement Weather';
        holiday.Description__c ='check1';
        holiday.Notes__c='cecede';
                holiday.PDC_Closed_Date__c = date.today();
        holiday.area__c = 'Illinois';
        insert holiday;
        CCPDCHeaderBIEController controller = new CCPDCHeaderBIEController();
        
    }
    
    static testMethod void TestMethod_3()
    {
        PDC_Holiday__c holiday = new PDC_Holiday__c(); 
        holiday.Closure_Reason__c='PDC Closed';
        holiday.Description__c ='check1';
                holiday.PDC_Closed_Date__c = date.today();
        holiday.Notes__c='cecede';
        holiday.area__c = 'Illinois';
        insert holiday;
        CCPDCHeaderBIEController controller = new CCPDCHeaderBIEController();
        
    } 
    
    static testMethod void TestMethod_4()
    {
        PDC_Holiday__c holiday = new PDC_Holiday__c(); 
        holiday.Closure_Reason__c='Warehouse Closed';
        holiday.Description__c ='check1';
                holiday.PDC_Closed_Date__c = date.today();
        holiday.Notes__c='cecede';
        holiday.area__c = 'Illinois';
        insert holiday;
        CCPDCHeaderBIEController controller = new CCPDCHeaderBIEController();
        
    } 
    static testMethod void TestMethod_5()
    {
        PDC_Holiday__c holiday = new PDC_Holiday__c(); 
        holiday.Closure_Reason__c='PDC/Warehouse Closed';
        holiday.Description__c ='check1';
        holiday.Notes__c='cecede';
        holiday.area__c = 'Illinois';
                holiday.PDC_Closed_Date__c = date.today();
        insert holiday;
        CCPDCHeaderBIEController controller = new CCPDCHeaderBIEController();
        
    }
    
}