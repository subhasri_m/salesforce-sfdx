@isTest
public class CCPDCOMSFulfillmentPlanAPITest {

    static testmethod void executeRequestsTest() {
        CCPDCOMSAPITest.createOMSSettingsWithToken('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', OMS_3_RESPONSE));

        CCPDCOMSFulfillmentPlanAPI fulFillApi = null;

        Test.startTest();

        CCPDCOMSAPI api = new CCPDCOMSAPI('DefaultStore');
        CC_PDC_OMS_Settings__c settings = api.omsSettings;

        CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanRequest fulFilReq = new CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanRequest();
        fulFilReq.orderId = '3be4331a-d9b4-4aa0-bc6c-e5287327f47a';
        fulFilReq.reservationId = '000000000072';
        fulFilReq.recheckAvailabilityFlag = true;
        fulFilReq.reserveInventoryFlag = true;
        fulFilReq.freeFreightEligibilityFlag = false;
        fulFilReq.willCallFlag  = true;
        fulFilReq.shipUnavailableWillCallFlag = true;
        fulFilReq.customerNo = '2-45284-0';
        fulFilReq.shipToAddress = new CCPDCOMSFulFillmentPlanAPI.ShipToAddress();
        fulFilReq.shipToAddress.zipcode = '80161';
        fulFilReq.shipToAddress.residentialFlag = false;
        fulFilReq.shipToAddress.liftgateFlag = true;
        fulFilReq.shipToAddress.homeDC = 'NH';
        fulFilReq.channel = 'ECommerce';
        fulFilReq.orderTotal = 139.60;

        fulFilReq.OrderLines = new List<CCPDCOMSFulFillmentPlanAPI.OrderLine>();
        CCPDCOMSFulFillmentPlanAPI.OrderLine orderLine = new CCPDCOMSFulFillmentPlanAPI.OrderLine();
        orderLine.partNo = 'GAT-32941';
        orderLine.orderLineNo = 1;
        orderLine.quantity = 50;
        orderLine.expeditedFlag = false; 
        fulFilReq.OrderLines.add(orderLine);

        fulFilReq.backOrderLines = new List<CCPDCOMSFulFillmentPlanAPI.BackOrderLine>();

        fulFillApi = new CCPDCOMSFulfillmentPlanAPI(settings, fulFilReq);
        api.queueRequest(fulFillApi);
        api.executeRequests();

        Test.stopTest();

        System.assert(fulFillApi != null);
        System.assert(fulFillApi.fulfillmentResponse != null);
        System.assert(fulFillApi.fulfillmentResponse.success);
        System.assert(fulFillApi.fulfillmentResponse.message == null);
        System.assert(fulFillApi.fulfillmentResponse.failureReason == null);
        System.assertEquals('Success', fulFillApi.fulfillmentResponse.result);
    }

    static testmethod void executeRequestsErrorTest() {
        CCPDCOMSAPITest.createOMSSettingsWithToken('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(404, 'Bad Request', ''));

        CCPDCOMSFulfillmentPlanAPI fulFillApi = null;

        Test.startTest();

        CCPDCOMSAPI api = new CCPDCOMSAPI('DefaultStore');
        CC_PDC_OMS_Settings__c settings = api.omsSettings;

        CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanRequest fulFilReq = new CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanRequest();
        fulFilReq.orderId = '3be4331a-d9b4-4aa0-bc6c-e5287327f47a';
        fulFilReq.reservationId = '000000000072';
        fulFilReq.recheckAvailabilityFlag = true;
        fulFilReq.reserveInventoryFlag = true;
        fulFilReq.freeFreightEligibilityFlag = false;
        fulFilReq.willCallFlag  = true;
        fulFilReq.shipUnavailableWillCallFlag = true;
        fulFilReq.customerNo = '2-45284-0';
        fulFilReq.shipToAddress = new CCPDCOMSFulFillmentPlanAPI.ShipToAddress();
        fulFilReq.shipToAddress.zipcode = '80161';
        fulFilReq.shipToAddress.residentialFlag = false;
        fulFilReq.shipToAddress.liftgateFlag = true;
        fulFilReq.shipToAddress.homeDC = 'NH';
        fulFilReq.channel = 'ECommerce';
        fulFilReq.orderTotal = 139.60;

        fulFilReq.OrderLines = new List<CCPDCOMSFulFillmentPlanAPI.OrderLine>();
        CCPDCOMSFulFillmentPlanAPI.OrderLine orderLine = new CCPDCOMSFulFillmentPlanAPI.OrderLine();
        orderLine.partNo = 'GAT-32941';
        orderLine.orderLineNo = 1;
        orderLine.quantity = 50;
        orderLine.expeditedFlag = false; 
        fulFilReq.OrderLines.add(orderLine);

        fulFilReq.backOrderLines = new List<CCPDCOMSFulFillmentPlanAPI.BackOrderLine>();

        fulFillApi = new CCPDCOMSFulfillmentPlanAPI(settings, fulFilReq);
        api.queueRequest(fulFillApi);
        api.executeRequests();

        Test.stopTest();

        System.assert(fulFillApi != null);
        System.assert(fulFillApi.fulfillmentResponse != null);
        System.assert(!fulFillApi.fulfillmentResponse.success);
        System.assert(fulFillApi.fulfillmentResponse.message != null);
    }

    static testmethod void executeRequestsCancelTest() {
        CCPDCOMSAPITest.createOMSSettingsWithToken('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(200, 'OK', '{"result":"Success","recheckOccurred":false,"shipments":[],"backOrderLines":[]}'));

        CCPDCOMSFulfillmentPlanAPI fulFillApi = null;

        Test.startTest();

        CCPDCOMSAPI api = new CCPDCOMSAPI('DefaultStore');
        CC_PDC_OMS_Settings__c settings = api.omsSettings;

        CCPDCOMSFulfillmentPlanAPI.CancelReservationRequest cancelRequest = new CCPDCOMSFulfillmentPlanAPI.CancelReservationRequest();
        cancelRequest.orderId = '123';
        cancelRequest.reservationId = '456';
        CCPDCOMSFulfillmentPlanAPI cancelApi = new CCPDCOMSFulfillmentPlanAPI(settings, cancelRequest);
        api.queueRequest(cancelApi);

        api.executeRequests();

        Test.stopTest();

        System.assert(cancelApi != null);
        System.assert(cancelApi.cancelRequest != null);
        System.assert(cancelApi.cancelResponseBody != null);
        System.assert(cancelApi.cancelResponse != null);
        System.assert(cancelApi.cancelResponse.success);
        System.assert(cancelApi.cancelResponse.message == null);
        System.assert(!cancelApi.cancelResponse.recheckOccurred);
        System.assert(cancelApi.cancelResponse.shipments.isEmpty());
        System.assert(cancelApi.cancelResponse.backOrderLines.isEmpty());
        System.assertEquals('Success', cancelApi.cancelResponse.result);
    }

    static testmethod void executeRequestsCancelFailTest() {
        CCPDCOMSAPITest.createOMSSettingsWithToken('DefaultStore');
        Test.setMock(HttpCalloutMock.class, new CCPDCOMSAPITest.OMSServiceMock(500, 'OK', '{"result":"Success","recheckOccurred":false,"shipments":[],"backOrderLines":[]}'));

        CCPDCOMSFulfillmentPlanAPI fulFillApi = null;

        Test.startTest();

        CCPDCOMSAPI api = new CCPDCOMSAPI('DefaultStore');
        CC_PDC_OMS_Settings__c settings = api.omsSettings;

        CCPDCOMSFulfillmentPlanAPI.CancelReservationRequest cancelRequest = new CCPDCOMSFulfillmentPlanAPI.CancelReservationRequest();
        cancelRequest.orderId = '123';
        cancelRequest.reservationId = '456';
        CCPDCOMSFulfillmentPlanAPI cancelApi = new CCPDCOMSFulfillmentPlanAPI(settings, cancelRequest);
        api.queueRequest(cancelApi);

        api.executeRequests();

        Test.stopTest();

        System.assert(cancelApi != null);
        System.assert(cancelApi.cancelRequest != null);
        System.assert(cancelApi.cancelResponse != null);
        System.assert(!cancelApi.cancelResponse.success);
    }

    public final static String OMS_3_RESPONSE = '{"orderId":"3be4331a-d9b4-4aa0-bc6c-e5287327f47a","result":"Success","recheckOccurred":false,"shipments":[],"backOrderLines":[]}';
    public final static String OMS_1_RESPONSE = '{"orderId":"5f9fa746-93e8-4426-9b2c-dc5bfa5263ec","result":"Success","homeDC":"AT","recheckOccurred":false,"shipments":[{"dc":"AT","expedited":false,"costToFP":0.0,"costToCustomer":0.0,"transitTime":0,"lines":[{"partNo":"BAL-100","orderLineNo":"1","quantity":10}]}],"backOrderLines":[{"partNo":"BAL-100","orderLineNo":"2","quantity":10}]}';
    public final static String OMS_2_RESPONSE = '{"orderId":"72d19459-b850-4f19-915a-173609281797","result":"Success","reservationId":"000000001147","homeDC":"AT","recheckOccurred":false,"shipments":[{"dc":"AT","carrier":"UPS","expedited":false,"costToFP":6.2,"costToCustomer":6.2,"mode":"Parcel","service":"GND","transitTime":0,"lines":[{"partNo":"BAL-100","orderLineNo":"10","quantity":1}]}],"backOrderLines":[{"partNo":"BAL-100","orderLineNo":"2","quantity":10}]}';
}