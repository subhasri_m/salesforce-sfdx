global without sharing class CCFPSiteRegisterController {
    @RemoteAction
    //global static Boolean sendEmail(String userEmail, Boolean accountNumberFlag) {
    global static ccrz.cc_RemoteActionResult sendEmail(ccrz.cc_RemoteActionContext ctx, String userEmail, String accountNumber, String billingZipCode, Boolean accountNumberFlag, String locationCode) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx); 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        CC_Avi_Settings__mdt aviSettings = [SELECT Customer_Service_Email_Address__c, Org_Wide_Email_Address__c from CC_Avi_Settings__mdt where Storefront__c = 'parts' limit 1];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = :aviSettings.Org_Wide_Email_Address__c];
        
        List<EmailTemplate> lstEmailTemplates = new List<EmailTemplate>();
        User usr = [select accountId, contactId from user where Username=:userEmail];
        String contactId = usr.contactId;
        String acctId = usr.accountId;
        List<Account> lstAcc = new List<Account>();
        lstAcc = [Select Id, Name from Account where id =:acctId];
        Account acct = new Account();
        if(!lstAcc.isEmpty()){
           acct = lstAcc[0]; 
        }
        System.debug('contactId==>'+contactId);
        if(accountNumberFlag == false && (String.isNotBlank(accountNumber) || String.isNotBlank(billingZipCode) )) {
        	lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = 'FP_New_User_With_Account_Notification'];
            List<CC_FP_Contract_Account_Permission_Matrix__c> adminContacts = [select Contact__c,Contact__r.Email from  CC_FP_Contract_Account_Permission_Matrix__c where Is_Admin__c = true and Contact__r.AccountId=:acctId  LIMIT 1];
            mail.settargetObjectId(contactId);
            mail.setTreatTargetObjectAsRecipient(false); 
            if(adminContacts.size() > 0) {
                mail.setToAddresses(new list<string>{adminContacts[0].Contact__r.Email});
                ccrz.cclog.log(System.LoggingLevel.DEBUG,'M:X','CCFPSiteRegisterController sending email to contactAdmin:' + adminContacts[0].Contact__r.Email );
            } else {
                mail.setToAddresses(new list<string>{aviSettings.Customer_Service_Email_Address__c});
                ccrz.cclog.log(System.LoggingLevel.DEBUG,'M:X','CCFPSiteRegisterController sending email to CS:' + aviSettings.Customer_Service_Email_Address__c );
            }
        } else if(accountNumberFlag == true) {
            lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = 'FP_New_User_With_No_Account_Notification'];
            mail.settargetObjectId(contactId);
            mail.setTreatTargetObjectAsRecipient(false); 
            mail.setToAddresses(new list<string>{aviSettings.Customer_Service_Email_Address__c});
            ccrz.cclog.log(System.LoggingLevel.DEBUG,'M:X','CCFPSiteRegisterController sending email to CS:' + aviSettings.Customer_Service_Email_Address__c );
        }        
        if(lstEmailTemplates.size() > 0 ) {
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
            mail.setSaveAsActivity(false);
            mail.setWhatId(acct.Id); // Enter your record Id whose merge field you want to add in template
            if(!Test.isRunningTest()){
                Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            } 
        }
        return response;
    }
     @RemoteAction
    global static ccrz.cc_RemoteActionResult callCreateCustomerBoomi(ccrz.cc_RemoteActionContext ctx, String newCustomerJSON, String locationCode) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx); 
        Map<String, Object> formData = (Map<String, Object>) JSON.deserializeUntyped(newCustomerJSON);
        String addressValidationMessage = '';
        String firstName = '';
        String streetNumber = '';
        if(formData.containsKey('firstName')) {
            firstName = (String)formData.get('firstName');
        }
        String lastName = '';
        if(formData.containsKey('lastName')) {
            lastName = (String)formData.get('lastName');
        }
        String fullName = firstName + ' ' + lastName;
        String contactEmail = '';
        if(formData.containsKey('username')) {
            contactEmail = (String)formData.get('username');
        }
        String companyName = '';
        if(formData.containsKey('companyName')) {
            companyName = (String)formData.get('companyName');
        }
        String contactPhone = '';
        if(formData.containsKey('primaryPhone')) {
            contactPhone = (String)formData.get('primaryPhone');
        }
        String billingStreet1 = '';
        if(formData.containsKey('billingAddress.address1')) {
            billingStreet1 = (String)formData.get('billingAddress.address1');
            if(String.isNotBlank(billingStreet1)) {
                String[] address = billingStreet1.trim().split(' ');
                streetNumber = address[0];
            }
        }
        String billingStreet2 = '';
        if(formData.containsKey('billingAddress.address2')) {
            billingStreet2 = (String)formData.get('billingAddress.address2');
        }
        String billingCity = '';
        if(formData.containsKey('billingAddress.city')) {
            billingCity = (String)formData.get('billingAddress.city');
        }
        String billingState = '';
        if(formData.containsKey('billingAddress.stateCode')) {
            billingState = (String)formData.get('billingAddress.stateCode');
        }
        String billingZip = '';
        if(formData.containsKey('billingAddress.postalCode')) {
            billingZip = (String)formData.get('billingAddress.postalCode');
        }
        String billingCountry = 'US';
        
        String useManualFlag = '';
        if(formData.containsKey('addressBilling')) {
            useManualFlag = (String)formData.get('addressBilling');
        }
        if(formData.containsKey('username')) {
            String userName = (String) formData.get('username');
            List<User> userList = [Select Id from User where UserName = :userName];
            if(userList.size() > 0) {
                CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => 'SiteRegistration_UsernameEmailError' });
                return response;
            } 
        }
        if(useManualFlag != 'Manual') {
            String addressString = billingStreet1 + ' ' + billingStreet2 +  ' ' + billingCity + ' ' + billingState + ' ' + billingZip + ' ' + billingCountry;
            CCAviAddressyAPI.FindResponse findResponse = CCAviAddressyAPI.find('parts', addressString);
            if (findResponse.success) {
                if(findResponse.addresses.size() > 1){
                    addressValidationMessage = 'SiteRegistration_MultipleAddressesFound';
                    CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => addressValidationMessage,'addresses' => findResponse.addresses });
                    return response;
                } else if (findResponse.addresses.size() == 1 && findResponse.addresses[0].Type == 'Address') {
                    //success scenario
                } else {
                    addressValidationMessage = 'SiteRegistration_UnableToLocateAddress'; 
                    CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => addressValidationMessage });
                    return response;
                }            
            } else {     
                 addressValidationMessage = 'SiteRegistration_UnableToLocateAddress';
                 CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => addressValidationMessage });
                 return response;            
            }
        }
        
      	List<Account> matchingAcctList = [select Id, BillingStreet from Account where BillingStreet like :streetNumber + '%' and BillingPostalCode like :billingZip + '%'];
        if(matchingAcctList.size() > 0) {
            for(Account acct: matchingAcctList) {
            if(String.isNotBlank(acct.BillingStreet)) {
                String[] address = acct.BillingStreet.trim().split(' ');
                String accountStreetNumber = address[0];
                    if(accountStreetNumber == streetNumber) {
                        CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => 'SiteRegistration_AccountMatchError'});
                        return response;
                    }
                }    
            }
         } 
                          
        
        
        List<CCAviBoomiAPI.CreateCustomerRequest> lstReq = new List<CCAviBoomiAPI.CreateCustomerRequest>();
        
        CCAviBoomiAPI.CreateCustomerRequest req = new CCAviBoomiAPI.CreateCustomerRequest();
        req.Company = '1';
        req.Location = locationCode;
        req.CustomerName = companyName;
        if(fullName.length() > 20) {
          req.ContactName = fullName.substring(0,20);
        } else {
             req.ContactName = fullName;
        }
        req.BillToAddressLine1 = billingStreet1;
        req.BillToAddressLine2 = billingStreet2;
        req.BillToCity = billingCity;
        req.BillToState = billingState;
        req.BilltoZip = billingZip;
        req.BillToCounty = '';
        if(fullName.length() > 30) {
        	req.ShippingAttention = fullName.substring(0,30);
        } else {
            req.ShippingAttention = fullName;
        }
        req.ShipToAddressLine1 = billingStreet1;
        req.ShipToAddressLine2 = billingStreet2;
        req.ShipToCity = billingCity;
        req.ShipToState = billingState;
        req.ShipToZip = billingZip;
        req.ShipToCounty = '';
        req.ShipToCountry = billingCountry;
        req.CreateDate = Datetime.now().format('yyyy-MM-dd');
        req.EmailAddress = contactEmail;
        req.TelephoneNumber = contactPhone;
        
        lstReq.add(req);
        
        CCAviBoomiAPI.CreateCustomerResponseWrapper apiResponse = CCAviBoomiAPI.CreateCustomerBoomi('parts',lstReq);
        ccrz.cclog.log(System.LoggingLevel.DEBUG,'M:X','CCFPSiteRegisterController boomi Request:' + lstReq );
        ccrz.cclog.log(System.LoggingLevel.DEBUG,'M:X','CCFPSiteRegisterController boomi Response:' + apiResponse );
        List<CCAviBoomiAPI.CreateCustomerResponse> custResponseList = apiResponse.responseList;
        if(custResponseList != null && custResponseList.size() > 0) {
            if (custResponseList[0].CustomerNumber == '0') {
                response.success = false;
            } else {
                response.success = true;
                response.data = custResponseList[0];
            }
        } else {
            response.success = false;
        }       
        return response;
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult updateAccount(ccrz.cc_RemoteActionContext ctx, String userEmail, String customerNumber, String customerBranch, String locationCode, String addressChoice) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        User usr = [select accountId from user where Username=:userEmail];
        String acctId = usr.accountId;
        Account acct = [Select Id, Name,Type, Salesman_Number__c, Ship_from_Location__c, ISeries_Account_ID__c,  Iseries_Company_code__c,ISeries_Customer_Account__c,ISeries_Customer_Branch__c from Account where id =:acctId];
		acct.ISeries_Customer_Account__c = customerNumber;
        acct.ISeries_Customer_Branch__c = customerBranch;
        acct.Iseries_Company_code__c = '1';
        acct.Type = 'Customer';
        acct.Ship_from_Location__c = locationCode;
        acct.AccountNumber = acct.Iseries_Company_code__c + '-' + acct.ISeries_Customer_Account__c + '-' + acct.ISeries_Customer_Branch__c;
        acct.ISeries_Account_ID__c = acct.Iseries_Company_code__c.leftPad(3, '0') + '-' + acct.ISeries_Customer_Account__c.leftPad(7, '0') + '-' + acct.ISeries_Customer_Branch__c.leftPad(3, '0');
        acct.Salesman_Number__c = '1-978';
        acct.account_status__c = 'CASH ONLY';
        if(addressChoice == 'Manual') {
 	       acct.Customer_Address_Not_Verified__c = true;
        }
        update acct;
      
        return response;
    }
    
     @RemoteAction
     global static ccrz.cc_RemoteActionResult retriveAddress (ccrz.cc_RemoteActionContext ctx, String addressId) {
        ccrz.cc_CallContext.initRemoteContext(ctx);
        ccrz.cc_RemoteActionResult res = new ccrz.cc_RemoteActionResult();
        CCAviAddressyAPI.RetrieveResponse validatedAddress = new CCAviAddressyAPI.RetrieveResponse();
        String validatedAddressString = '';
         if(addressId != 'Manual') {
            validatedAddress = CCAviAddressyAPI.retrieveAddress('parts', addressId);
			res.data = validatedAddress;
        	res.success = true;
        } else {
        	res.success = false;
        }
        res.inputContext = ctx;
        return res;
    }
     @RemoteAction
     global static ccrz.cc_RemoteActionResult findNationalAccount (ccrz.cc_RemoteActionContext ctx, Boolean accountNumberFlag, String userName, String accountNumber, String billingZipCode) {
         ccrz.cc_RemoteActionResult res = CCAviPageUtils.remoteInit(ctx);
         
         List<User> userList = [Select Id from User where UserName = :userName];
         if(userList.size() > 0) {
             res.data  = 'SiteRegistration_UsernameEmailError';
             return res;
         } 
         
         if(accountNumberFlag == false) {
             String iSeriesCustomerAccount = '';
             String iSeriesCustomerBranch = '0';
             if(String.isNotBlank(accountNumber)) {
                 if(accountNumber.contains('-')) {
                     String[] iSeriesInfo = accountNumber.split('-');
                     iSeriesCustomerAccount = String.valueOf(Integer.valueOf(iSeriesInfo[0]));
                     iSeriesCustomerBranch = String.valueOf(Integer.valueOf(iSeriesInfo[1]));
                 } else {
                     iSeriesCustomerAccount = String.valueOf(Integer.valueOf(accountNumber));
                 }
             }
             
             List<Account> acctList = [Select Id, Name, National_Account_Group__c from Account where Iseries_Company_code__c = '1' and ISeries_Customer_Account__c = :iSeriesCustomerAccount and ISeries_Customer_Branch__c = :iSeriesCustomerBranch and BillingPostalCode like :billingZipCode + '%'];
             if(acctList.size() > 0) {
                 if(acctList[0].National_Account_Group__c != '0' && String.isNotBlank(acctList[0].National_Account_Group__c)) {
                     res.success = true; // National Account
                 } else {
                     res.success = false;  // Not a National Account
                 } 
             }   
         } else {
             res.success = false; //National Account Check Not Required
         }
         return res;
    }


}