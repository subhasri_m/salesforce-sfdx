global class BatchAccountSO implements Database.Batchable<sObject> {
    
    Integer month;
    List<Sales_Order_Summary__c> salesOrderList = new List<Sales_Order_Summary__c>();
    
    Set<Id> errorId = new Set<Id>{'0014000000xE1fp', '0014000000xE1fl', '0014000000xE71G', '0014000000xE1fK', '0014000000xE1fV', '0014000000xE70R', '0014000000xE71E', '0014000000xDTJn', '0014000000xE1qm', '0014000000xE1gA', '0014000000xE6bn', '0014000000xE1ex', '0014000000xE6ej', '0014000000xDQi7', '0014000000xDUp8AAG', '00140000016DSI1AAO'};
    
    String a = '0014000000xDSFS';
    String b = '001c00000106tlz';
    String c = '0014000000xDHfAAAW';

    //String query = 'SELECT Id, Account__c, Order_Date__c, Sales__c FROM Sales_Order_Summary__c WHERE Order_Date__c = :dateLastYear AND Sales__c != 0';
    //String query = 'SELECT Id, Account__c, Order_Date__c, Sales__c FROM Sales_Order_Summary__c WHERE Order_Date__c = :dateLastYear AND (Account__c = :a OR Account__c = :b) AND Sales__c != 0';
    String query = 'SELECT Id, Account__c, Order_Date__c, Sales__c FROM Sales_Order_Summary__c WHERE Order_Date__c >= :startDate AND Order_Date__c < :endDate AND Account__c != :errorId AND Sales__c != 0 ORDER BY Account__c ASC';
    
    //Map<Integer, Map<Id, Decimal>> monthSale =  new Map<Integer, Map<Id, Decimal>>();
    
    global BatchAccountSO(Integer mnth) {
        month = mnth;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){ 
        System.debug('@@@@@@@@@@@@@@@@@@ start');
        Integer year = System.Today().year();
        Date endDate = date.newInstance(year, month, 01);
        Date startDate = date.newInstance(year, 01, 01);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Sales_Order_Summary__c> scope){ 
        Map<Id, Decimal> idToSales = new Map<Id, Decimal>();
        for(Sales_Order_Summary__c so: scope){
            if(!idToSales.containsKey(so.Account__c)){
                idToSales.put(so.Account__c, so.Sales__c);
            }
            else{
                idToSales.put(so.Account__c, idToSales.get(so.Account__c) + so.Sales__c);
            }
        }
        List<Account> accDML = new List<Account>();
        for(Id i: idToSales.keySet()){
           Account acc = new Account(Id = i, Year_to_Date_Sales2__c = idToSales.get(i));
           accDML.add(acc);
        }
        if(accDML.size()>0){
            update accDML;
        }
        
    }
 
    global void finish(Database.BatchableContext bc){ 
        System.debug('@@@@@@@@@@@@@@@@@@ finish');
        System.debug('Finished Execution: ' + System.now().format());
    }   
    
}