public without sharing class CCPDCMyAccountAdminHelper {


	public static void insertContact(Contact c){
		insert c;
	}
	public static void updateContact(Contact c){
		update c;
	}
	public static void deleteContact(Contact c){
		delete c;
	}
	public static void insertUser(User u){
		insert u;
	}
	public static void updateUser(User u){
		update u;
	}
	public static void resetPassword(User u){
		System.resetPassword(u.Id, true);
	}
	public static void assignProfileToUser(Id profileId, User u){
		u.ProfileId = profileId;
	}

	public static Contact queryContactById(Id contactId){
		Contact c = [
			SELECT
				  Id
				, AccountId
			FROM
				Contact
			WHERE
				Id = :contactId
			LIMIT
				1
		];
		return c;
	}

	public static Map<Id, ccrz__E_AccountAddressBook__c> getShippingAddresses(Set <Id> accountIds){
		return new Map<Id, ccrz__E_AccountAddressBook__c>([
			SELECT
				Id
				, ccrz__Account__c

				// Fields from ContactAddr
				, ccrz__E_ContactAddress__r.Id
				, ccrz__E_ContactAddress__r.ccrz__AddressFirstline__c
				, ccrz__E_ContactAddress__r.ccrz__AddressSecondline__c
				, ccrz__E_ContactAddress__r.ccrz__AddressThirdline__c
				, ccrz__E_ContactAddress__r.ccrz__City__c
				, ccrz__E_ContactAddress__r.ccrz__StateISOCode__c
				, ccrz__E_ContactAddress__r.ccrz__CountryISOCode__c
				, ccrz__E_ContactAddress__r.ccrz__PostalCode__c
			FROM 
				ccrz__E_AccountAddressBook__c
			WHERE
				ccrz__Account__c in :accountIds
			AND
				ccrz__AddressType__c = 'Shipping'
		]);
	}
	public class CCContact {
		// Contact fields
		public String username {get;set;}
		public Id contactId {get;set;}
		public String firstName {get;set;}
		public String lastName {get;set;}
		public String emailAddress {get;set;}
		public String phoneNumber {get;set;}
		public Boolean isAdmin {get;set;}

		// Account Fields
		public Id accountId {get;set;}
		public String accountName {get;set;}
		// User fields
		public Id userId {get;set;}
		public Boolean isActive {get;set;}

		// Permission Matrix
		public CC_FP_Contract_Account_Permission_Matrix__c perms {get;set;}
	}


	public static List<CCContact> fetchContactList(Id selectedActId){
		// Query permission matrix for current effective account

		List<Account> accounts = [SELECT Id, Name FROM Account WHERE ParentId =:selectedActId];
		List<Id> accountIds = new List<Id>();
		accountIds.add(selectedActId);
		if(accounts != null && !accounts.isEmpty()){
			for(Account account:accounts){
				accountIds.add(account.Id);
			}
		} 

		List<CC_FP_Contract_Account_Permission_Matrix__c> permList = getPermissionsForAccount(accountIds);

		Map<Id, CC_FP_Contract_Account_Permission_Matrix__c> contactToPermMap = new Map<Id, CC_FP_Contract_Account_Permission_Matrix__c>();
		for(CC_FP_Contract_Account_Permission_Matrix__c perm :permList){
			contactToPermMap.put(perm.Contact__c, perm);
		}


        Map<Id, User> contactToUserMap = new Map<Id, User>();
		List<User> userList = [
			SELECT
				Id
				, Username
				, IsActive
				, ContactId
				, Contact.AccountId
				, Contact.FirstName
				, Contact.LastName
				, Contact.Email
				, Contact.Phone
			FROM
				User
			WHERE
				IsActive = true
			AND
				ContactId IN :contactToPermMap.keySet()
		];

        for (User u : userList) {
            contactToUserMap.put(u.ContactId, u);
        }

		List<CCContact> contactList = new List<CCContact>();
		Map<Id,String> accIdToName = new Map<Id,String>();
		Set<Id> accIds = new Set<Id>();
		// If the user is not active, or there is no permission matrix, do not list the contact
		// Build the CCContact objects
		for(CC_FP_Contract_Account_Permission_Matrix__c perm : permList) {
            User u = contactToUserMap.get(perm.Contact__c);
            if (u != null) {

                CCContact con = new CCContact();
                con.username = u.Username;
                con.contactId = u.Contact.Id;
                con.firstName = u.Contact.Firstname;
                con.lastName = u.Contact.Lastname;
                con.emailAddress = u.Contact.Email;
                con.phoneNumber = u.Contact.Phone;
                con.accountId = perm.Account__c;
                con.userId = u.Id;
                con.isActive = u.IsActive;
                con.perms = perm;
                accIds.add(u.Contact.AccountId);
                contactList.add(con);
            }
		}

		List<Account> accountsName = [SELECT Id, Name FROM Account WHERE Id IN:accIds];

		for(Account acc:accountsName){
			accIdToName.put(acc.Id, acc.Name);
		}
		for(CCContact contact : contactList){
			contact.accountName = accIdToName.get(contact.accountId);
		}
		return contactList;
    }
    public static List<CC_FP_Contract_Account_Permission_Matrix__c> getPermissionsForAccount(List<Id> accountIds) {
        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = [
            SELECT 
                Id, 
                Name,
                Account__c,
                Can_View_Invoices__c,
                Allow_Access_To_Accounting_Reports__c,
                Allow_Access_to_Sales_Reports__c,
                Allow_AR_Payments__c,
                Allow_Backorder_Release__c,
                Allow_Backorders__c,
                Allow_Checkout__c,
                Allow_Non_Free_Shipping_Orders__c,
                Allow_Override_ShipTo__c,
                Contact__c,
                Is_Admin__c,
                Maximum_Order_Limit__c,
                Pricing_Visibility__c
            FROM 
                CC_FP_Contract_Account_Permission_Matrix__c
            WHERE
                Account__c IN:accountIds
        ];
        return permissions;
    }		



    public static void updatePermissions(CC_FP_Contract_Account_Permission_Matrix__c perms){
    	update perms;
    }

    public static void insertPermissions(CC_FP_Contract_Account_Permission_Matrix__c perms, Id accountId, Id contactId){
        perms.Account__c = accountId;
        perms.Contact__c = contactId;
        insert perms;

    }

    public static void insertPermissions(CC_FP_Contract_Account_Permission_Matrix__c perms){
        insert perms;

    }

    public static void deletePermissions(CC_FP_Contract_Account_Permission_Matrix__c perms){
        delete perms;
    }

}