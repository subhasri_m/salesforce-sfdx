public class OpportunityEditList {
    
    public String updateStage{get; set;}
    public String SelectedStage {get;set;}
    public String UpdateProductBunch {get;set;}
    public String UpdateReasonLost {get; set;}
    public String header{get; set;} 
   public  String Accountid {get; set;}
   List<OppPageStages__c> CustomSetting;
    List<WrapperClass> WrapperList {get; set;}
    List<WrapperClass> SelectedOppList {get; set;}
    public Boolean IsUserOSR_ISR {get;set;}
    public Boolean IsNotOSR_ISR {get;set;}
    
   
    public OpportunityEditList ()
        
    {
        Accountid = ApexPages.currentPage().getParameters().get('Accountid');
         CustomSetting = OppPageStages__c.getAll().values();
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        if (PROFILE[0].Name == '*FleetPride: Sales Rep (OSR)' || PROFILE[0].Name == '*FleetPride: Sales Rep (ISRs)' )
        {
        IsUserOSR_ISR  = true;
        IsNotOSR_ISR  = false;
        }
        
        else 
        {
        IsUserOSR_ISR = false;
        IsNotOSR_ISR = true;
        }
        
    }
    
    
    public List<WrapperClass> getOppsList() 
        
    {
        WrapperList = new List<WrapperClass>();
        List<Opportunity> OppList = new List<Opportunity>(); 
        System.debug('Accountid--'+Accountid);
        if (Accountid!=null)
        {
        if(selectedStage !=null){
            OppList =  [select id,LeadSource,createdDate,CloseDate,Reason_Lost__c,recordtype.name, Name,StageName,Type,owner.name, Account.name, accountid, ownerid,amount from opportunity where accountid =:Accountid and LeadSource ='FP Intelligence' and StageName like :SelectedStage order by amount desc  limit 500];
            }
            else
          {
            OppList =  [select id,LeadSource,createdDate,CloseDate,Reason_Lost__c,recordtype.name, Name,StageName,Type,owner.name, Account.name, accountid, ownerid,amount from opportunity where accountid =:Accountid and LeadSource ='FP Intelligence' order by amount desc  limit 500];
          
          }  
            
            if (!opplist.isEmpty()){   header = oppList[0].account.name  + '\'s Opportunities';
            }
        }
        else
        {
        system.debug('selectedStage -->'+selectedStage );
        if(selectedStage !=null){
            OppList =  [select id,LeadSource,createdDate,CloseDate,Reason_Lost__c,recordtype.name, Name,StageName,Type,owner.name, Account.name, accountid, ownerid,amount from opportunity where ownerid =:UserInfo.getUserId() and LeadSource ='FP Intelligence'  and StageName like :SelectedStage order by amount desc  limit 500 ];
            }
            else
            {
            OppList =  [select id,LeadSource,createdDate,CloseDate,Reason_Lost__c,recordtype.name, Name,StageName,Type,owner.name, Account.name, accountid, ownerid,amount from opportunity where ownerid =:UserInfo.getUserId() and LeadSource ='FP Intelligence' order by amount desc  limit 500 ];
            
            }
            if (!opplist.isEmpty()){    header = oppList[0].owner.name + '\'s Opportunities';
            }
        } 
        
        List<WrapperClass>  ClosedWrapperList = new List<WrapperClass> ();
        for (Opportunity OppVar :OppList)
        {
            WrapperClass OWC = new WrapperClass(); 
            owc.LeadSource =OppVar.LeadSource;
            Owc.createdDate  = date.newinstance(OppVar.CreatedDate.year(), OppVar.CreatedDate.month(), OppVar.CreatedDate.day());
            owc.AccountName = OppVar.Account.name;
            owc.ReasonLost = OppVar.Reason_Lost__c;
            owc.ClosedDate = OppVar.CloseDate;
            owc.OpportunityName = OppVar.Name;
            owc.OpportunityStage = oppvar.StageName;
           
            owc.OppAmount = Oppvar.amount;
            owc.OpportunityOwner = oppvar.owner.name;
            owc.OpportunityId = oppvar.id;
            owc.OpportunityType = oppvar.recordtype.name; 
           
            if (oppvar.stageName.containsIgnoreCase('Closed')){
            owc.isClosed  = false;
            ClosedWrapperList.add(owc);
            }
            else{
            WrapperList.add(OWC); 
            }
           
            
        }
        
         WrapperList.addall(ClosedWrapperList);
        
        return WrapperList; 
    } 
    
    public List<SelectOption> getOppStage()
    {
        List<SelectOption> options = new List<SelectOption>();
        String ValidStageName = CustomSetting[0].StageNames__c;
        List<String> StageNamesList =  ValidStageName.split(';');
        System.debug('StageNamesList--'+StageNamesList);
        Set<string> StageNamesSet = new Set<string>();
        StageNamesSet.addall(StageNamesList );
        Schema.DescribeFieldResult fieldResult = Opportunity.Stagename.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
          options.add(new SelectOption('','--Select--')); 
        System.debug('StageNamesSet-->'+StageNamesSet);
        for( Schema.PicklistEntry f : ple)
        {
        
        System.debug('values-->'+f.getLabel());
        if (StageNamesSet.contains(f.getLabel()) && f.getLabel() !='01-New'){ 
       
             options.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }       
        return options;
    }
    
    public List<SelectOption> getOppFilterStage()
    {
        List<SelectOption> options = new List<SelectOption>();
        String ValidStageName = CustomSetting[0].StageNames__c;
        List<String> StageNamesList =  ValidStageName.split(';');
        System.debug('StageNamesList--'+StageNamesList);
        Set<string> StageNamesSet = new Set<string>();
        StageNamesSet.addall(StageNamesList );
        Schema.DescribeFieldResult fieldResult = Opportunity.Stagename.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
          options.add(new SelectOption('','--Select--')); 
        System.debug('StageNamesSet-->'+StageNamesSet);
        for( Schema.PicklistEntry f : ple)
        {
        
        System.debug('values-->'+f.getLabel());
        if (StageNamesSet.contains(f.getLabel())){ 
       
             options.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }       
        return options;
    }
    
    
    public List<SelectOption> getReasonLost()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Opportunity.Reason_Lost__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('','--Select--'));   
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    public pagereference updateTableData() {
        //this.accountname=ApexPages.currentPage().getParameters().get('accountname');
        SelectedOppList=new List<WrapperClass>();
        for(WrapperClass item: WrapperList)
        {
            if(item.selected)
            {
                SelectedOppList.add(item);
            } 
        }         
        
        System.debug('Selected-->'+SelectedOppList);
        if(SelectedOppList.isEmpty())
        {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'No items were selected to process'));}
        
        if(!SelectedOppList.isEmpty())
        {
            UpdateOppRecords(SelectedOppList);
        }
        return null;
    }
    
    @testVisible
    private void UpdateOppRecords(List<WrapperClass> SelectedList)
    { 
        Boolean ValidUpdate = true;    
        List<Opportunity> UpdateOppList = new List<Opportunity>();
        for (WrapperClass itemVar: SelectedList)
        {
            Opportunity OppRecord = new Opportunity(); 
            OppRecord.id = itemVar.OpportunityId;
            OppRecord.StageName= updateStage;
           
            
            opprecord.Product_Bunch__c = UpdateProductBunch ;
            opprecord.Reason_Lost__c = UpdateReasonLost;
            
            
            if ((updateStage==null  || updateStage.containsignoreCase('Lost')) && opprecord.Reason_Lost__c ==null)
                
            {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Select Reason Lost'));
             ValidUpdate = false;
            }   
            
            UpdateOppList.add(OppRecord);
        }
        
        if (!UpdateOppList.isEmpty() && ValidUpdate == true)
        {
            update UpdateOppList;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Updated Successfully'));
        }
        
    }
    
    public class WrapperClass
    {
        
        Public String AccountName{get;set;}
        Public Date createdDate  {get;set;}
        Public String LeadSource {get;set;} 
        Public String ReasonLost {get;set;}
        Public date ClosedDate {get;set;}
        public String Accountid{get;set;}
        Public String OpportunityType{get;set;}
        Public String OpportunityId{get;set;}
        Public string OpportunityStage{get;set;}
        Public String OpportunityName{get;set;}
        Public String OpportunityOwner{get;set;}
        Public Decimal OppAmount{get; set;}
        public Boolean Selected{get;set;}
        public Boolean isClosed {get;set;}
        
        public WrapperClass()
        {
            AccountName='';
            Accountid='';
            OpportunityType='';
            OpportunityId='';
            OpportunityStage='';
            OpportunityName='';
            OpportunityOwner='';
            Selected = false;
            OppAmount = 0;
            createdDate  =null;
         String LeadSource=''; 
         String ReasonLost=''; 
         date ClosedDate =null;
        String Accounti =''; 
        isClosed = true;
            
            
        }
        
    }
    
}