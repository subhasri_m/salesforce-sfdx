/**
* AVI Page Utils
*
* Description:
* This is a helper class that should be used by controllers that expose remote action methods
* to initialize the Call Context, and to create response messages to add to the remote action response.
*
*/
public with sharing class CCAviPageUtils {
    /**
     * Initialize the call context from the remote action context.  Call this method at the beginning of each
     * remote action method:
     *
     * ccrz.cc_RemoteActionResult res = CCAviPageUtils.remoteInit(ctx);
     *
     */
    public static ccrz.cc_RemoteActionResult remoteInit(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult result = new ccrz.cc_RemoteActionResult();
        result.success = false;
        result.inputContext = ctx;
        ccrz.cc_CallContext.initRemoteContext(ctx);
        // DCLoc - DC Location
        // BranchLoc - Branch Location
        // Below code sets location and fullfilment type parameters into the callContext bean that can be utilized in any remote action controller method
        for (String parms : ctx.queryParams.keySet()){
            if (parms == 'BranchLoc'){
                ccrz.cc_CallContext.currPageParameters.put('branchLoc', String.ValueOf(ctx.queryParams.get('BranchLoc')));  
            }
            if (parms == 'DCLoc'){
                ccrz.cc_CallContext.currPageParameters.put('dcLoc', String.ValueOf(ctx.queryParams.get('DCLoc')));  
            } 
            if (parms == 'selectedShippingMethod'){
                if (String.ValueOf(ctx.queryParams.get('selectedShippingMethod')) == 'fp_shipping_option1'){
                   ccrz.cc_CallContext.currPageParameters.put('deliveryOption','PickUp' );  
                }else if (String.ValueOf(ctx.queryParams.get('selectedShippingMethod')) == 'fp_shipping_option2'){
                    ccrz.cc_CallContext.currPageParameters.put('deliveryOption','ShipTo');  
                }
                else if (String.ValueOf(ctx.queryParams.get('selectedShippingMethod')) == 'fp_shipping_option3'){
                    ccrz.cc_CallContext.currPageParameters.put('deliveryOption','LocalDelivery');  
                }
            } 
        }
        return result;
    }


    /**
     * Call this method to add messages to the remote action response.
     *
     *
     * ccrz.cc_RemoteActionResult res = CCAviPageUtils.remoteInit(ctx);
     * res.messages.add(cc_gew_ctrl_OrderAttribs.createMessage(ccrz.cc_bean_Message.MessageSeverity.ERROR, null, e.getMessage() + ': ' + e.getStackTraceString(), 'messagingSection-Error'));
     *
     */
    public static ccrz.cc_bean_Message createMessage(ccrz.cc_bean_Message.MessageSeverity severity, String labelName, Object data, String className) {
        ccrz.cc_bean_Message msg = new ccrz.cc_bean_Message();
        msg.type = ccrz.cc_bean_Message.MessageType.CUSTOM;
        msg.classToAppend = className;
        msg.labelId = labelName;
        if (data != null)
            msg.message = String.valueOf(data);
        msg.severity = severity;
        return msg;
    }

    /**
     * Call this method to add custom data to the response.
     *
     * Parameters:
     *  response - the initialized cc_RemoteActionResult
     *  data - a Map<String, Object> of all the structures built in the remote action method.
     *
     * Example:
     *  ccrz.cc_RemoteActionResult res = CCAviPageUtils.remoteInit(ctx);
     *  List<String> myStringList = new List<String>{ ... };
     *  String endpoint = 'http:// ... ';
     *  CCAviPageUtils.buildResponseData(response, true, 
     *      new Map<String,Object>{
     *          'endpoint' => endpoint,
     *          'myStringList' => myStringList
     *      }
     *  );
     *  
     *
     */
     public static void buildResponseData(ccrz.cc_RemoteActionResult response, Boolean success, Map<String, Object> data){
        response.data = data;
        if(success == null){
            response.success = true;
        }
        else {
            response.success = success;
        }
     }
	public static ccrz.cc_RemoteActionResult combineResults (ccrz.cc_RemoteActionResult res1, ccrz.cc_RemoteActionResult res2){
		if(res1.data != null){
			System.debug(res1.data);
			System.debug(res2.data);
			((Map<String, Object>)res1.data).putAll(((Map<String, Object>)res2.data));
		}
		else{
			res1.data = res2.data;
		}
		res1.messages.addAll(res2.messages);

		res1.success &= res2.success;
		return res1;		
	}     
}