global with sharing class CCFPShipToController {
    
    public String data {get; set;}
    public Boolean isLoaded {get; set;}
   
    global CCFPShipToController() {
        isLoaded = false;
        data = '{}';
    }
    
    /*private static void addLocationOnCartWithUpdate(Map<String, Object> cart) {
        // get the Location from ship to address and update location field of cart, instead of location from Account     
        String cartId = (String) cart.get('sfid');
        String cartEncId = (String) cart.get('encryptedId');
        ccrz__E_Cart__c curCart = CCPDCCartDAO.getCartOrderView(cartEncId);
        String locationId = (String) cart.get('CCFPLocation');
        Boolean isWillCall = Boolean.valueOf(cart.get('CCFPIsWillCall'));
        if (locationId == null) {
            Id accountId = ccrz.cc_CallContext.effAccountId;
            CCFPLocationHelper.LocationResponse response = CCFPLocationHelper.getLocationForAccount(accountId);
            if (response != null) {
                cart.put('CCFPLocation', response.sfid);
                cart.put('location', response);
                // if it is will-call then don't update cart location with shipTo location, use account DC location
                // if (cartId != null && !isWillCall) {                  
                //     updateCartWithNewLocationDc(Id.valueOf(cartId));
                // }else if(cartId != null && isWillCall){
                curCart.CC_FP_Location__c = Id.valueOf(response.sfid);
                update curCart;
                //  }
            }
        }
        else {
            
            CCFPLocationHelper.LocationResponse response = CCFPLocationHelper.getLocationForId(locationId);
            cart.put('location', response);
            //  if (cartId != null && !isWillCall) {                  
            //      updateCartWithNewLocationDc(Id.valueOf(cartId));
            //   }else if(cartId != null && isWillCall){
            curCart.CC_FP_Location__c = Id.valueOf(response.sfid);
            update curCart;
            //   }
            //updateCartWithNewLocationDc(cartId);            
        }
        ccrz__E_Cart__c cartGettingUpdate = CCPDCCartDAO.getCartById(cartId);
        if(!cart.containsKey(('shipTo'))){
            cartGettingUpdate.ccrz__ShipTo__c = cartGettingUpdate.ccrz__BillTo__c;
            cart.put('shipTo',cartGettingUpdate.ccrz__BillTo__c);
            cart.put('shipToAddress',cartGettingUpdate.ccrz__BillTo__c);
        }
        update cartGettingUpdate; 
        
        //return cart;      
    }*/
       
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getCart(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        response.success = false;
        
        try {
            String cartId = getEncCartId();
            Id contactId = ccrz.cc_CallContext.currContact.Id;
            Map<String, Object> data = new Map<String, Object>();
            Contact con = [SELECT Id, Will_Call_As_Default__c FROM Contact WHERE Id=:contactId];
            Boolean wcDefault = con.Will_Call_As_Default__c;
            
            if (cartId != null) {
                ccrz__E_Cart__c cart = CCPDCCartDAO.getCartShipTo(cartId);
                if (cart != null) {
                    data.put('CCFPIsShipRemainder', cart.CC_FP_Is_Ship_Remainder__c);
                    data.put('willCallDefault', wcDefault);
                    data.put('CCFPIsWillCall', cart.CC_FP_Is_Will_Call__c);
                    data.put('CCFPLocation', cart.CC_FP_Location__c);
                    data.put('shipTo', cart.ccrz__ShipTo__c);
                    data.put('encryptedId', cart.ccrz__EncryptedId__c);
                    data.put('sfid', cart.Id);
                    Map<String, Object> shipTo = new Map<String, Object>();
                    if(cart.ccrz__ShipTo__c == null && cart.ccrz__BillTo__c != null)
                    {
                        data.put('shipTo', cart.ccrz__BillTo__c);
                        shipTo.put('addressFirstline', cart.ccrz__BillTo__r.ccrz__AddressFirstline__c); 
                        shipTo.put('city', cart.ccrz__BillTo__r.ccrz__City__c);
                        shipTo.put('country', cart.ccrz__BillTo__r.ccrz__Country__c); 
                        shipTo.put('postalCode', cart.ccrz__BillTo__r.ccrz__PostalCode__c);
                        shipTo.put('sfid', cart.ccrz__BillTo__c);  
                        shipTo.put('state', cart.ccrz__BillTo__r.ccrz__State__c); 
                        data.put('shipToAddress', shipTo);
                        cart.ccrz__ShipTo__c = cart.ccrz__BillTo__c;
                        update cart;
                        ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','shipToController 1');
                    }else if (cart.ccrz__ShipTo__c == null && cart.ccrz__BillTo__c == null)
                    {
                        ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','shipToController 2');
                        // Here is when user created a new cart, either his older cart delete or he place the cart to order
                        if(ctx.effAccountId != null || ccrz.cc_CallContext.effAccountId !=null){
                            Id currentaccountId;
                            if(ctx.effAccountId != null){
                                currentaccountId = ctx.effAccountId;
                            }else{
                                currentaccountId = ccrz.cc_CallContext.effAccountId;
                            }
                            if(currentaccountId != null){
                              
                                Account myAccount = [SELECT BillingStreet,BillingCity,BillingState,BillingPostalCode, BillingCountry,ShippingCountry,ShippingPostalCode,ShippingStreet,ShippingCity,ShippingState,Ship_from_Location__c FROM Account 
                                                        Where Id =:currentaccountId Limit 1];
                                
                                ccrz__E_ContactAddr__c billTo_addr = new ccrz__E_ContactAddr__c();
                                billTo_addr.ccrz__AddressFirstline__c = myAccount.BillingStreet;
                                billTo_addr.ccrz__City__c = myAccount.BillingCity;  
                                billTo_addr.ccrz__State__c = myAccount.BillingState;
                                billTo_addr.ccrz__PostalCode__c = myAccount.BillingPostalCode; 
                                billTo_addr.ccrz__Country__c = myAccount.BillingCountry;
                                insert billTo_addr; 
                                
                                ccrz__E_ContactAddr__c ShipTo_addr = new ccrz__E_ContactAddr__c();
                                ShipTo_addr.ccrz__AddressFirstline__c = myAccount.ShippingStreet;
                                ShipTo_addr.ccrz__City__c = myAccount.ShippingCity;  
                                ShipTo_addr.ccrz__State__c = myAccount.ShippingState;
                                ShipTo_addr.ccrz__PostalCode__c = myAccount.ShippingPostalCode; 
                                ShipTo_addr.ccrz__Country__c = myAccount.ShippingCountry;
                                insert ShipTo_addr;
                                cart.ccrz__BillTo__c = billTo_addr.id;
                                cart.ccrz__ShipTo__c = ShipTo_addr.id;
                                update cart;    
                                data.put('shipTo', ShipTo_addr.id);
                                shipTo.put('addressFirstline', ShipTo_addr.ccrz__AddressFirstline__c); 
                                shipTo.put('city', ShipTo_addr.ccrz__City__c);
                                shipTo.put('country', ShipTo_addr.ccrz__Country__c); 
                                shipTo.put('postalCode', ShipTo_addr.ccrz__PostalCode__c);
                                shipTo.put('sfid', ShipTo_addr.Id);  
                                shipTo.put('state', ShipTo_addr.ccrz__State__c);    
                                data.put('shipToAddress', shipTo);    
                            }
                         }    
                    }else{
                        ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','shipToController 3');
                        data.put('shipToAddress', shipTo);
                        shipTo.put('addressFirstline', cart.ccrz__ShipTo__r.ccrz__AddressFirstline__c);
                        shipTo.put('city', cart.ccrz__ShipTo__r.ccrz__City__c);
                        shipTo.put('country', cart.ccrz__ShipTo__r.ccrz__Country__c);
                        shipTo.put('postalCode', cart.ccrz__ShipTo__r.ccrz__PostalCode__c);
                        shipTo.put('sfid', cart.ccrz__ShipTo__c);
                        shipTo.put('state', cart.ccrz__ShipTo__r.ccrz__State__c);
                     }
                    
                    //addLocationOnCartWithUpdate(data);
                    CCAviPageUtils.buildResponseData(response, true, data);                
                }
                else {
                    CCAviPageUtils.buildResponseData(response, false, data);                
                }
            }
            else {
                CCAviPageUtils.buildResponseData(response, false, data);                
                
            }
            
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                                             new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
                                            );
        } finally{
            ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','CCPDCShipToController.getCart');
            ccrz.ccLog.close(response);
        }  
        return response;
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult changeFulfillment(ccrz.cc_RemoteActionContext ctx, String sfid, Boolean isWillCall, Boolean isShipRemainder) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        
        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = ccrz.cc_CallContext.currContact.Id;
        
        try {
            if (sfid != null && sfid != '') {
                Boolean hasPermission = true;
                if (isWillCall) {
                    CCPDCUserPermissionsHelper.UserPermissions permissions =  CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);
                    hasPermission = permissions.allowWillCall;
                }
                else {
                    isShipRemainder = false;
                }
                
                if (hasPermission) {
                    ccrz__E_Cart__c updateCart = new ccrz__E_Cart__c(Id = sfid, CC_FP_Is_Will_Call__c = isWillCall, CC_FP_Is_Ship_Remainder__c = isShipRemainder);
                    if(isWillCall == false || (isWillCall && isShipRemainder))
                        updateCart.Will_call_Changed__c = true;
                    
                    update updateCart;
                    response.success = true;                
                }
                else {
                    response.success = false;
                }
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                                             new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
                                            );
        }
        return response;
    }
    
    //call OMS to get HomeDC for the zipcode.
    /*private static String getHomeDCFromOMS(String zipCode) {
        List<CCPDCHomeDCResponse> nearestDCList;
        
        String storefront = ccrz.cc_CallContext.storefront;
        CCPDCOMSAPI api = new CCPDCOMSAPI(storefront);
        CC_PDC_OMS_Settings__c settings = api.omsSettings;
        // Integer sites = 2;
        Integer sites = 1;
        CCPDCOMSHomeDCAPI homeDCApi = new CCPDCOMSHomeDCAPI(zipCode, sites, settings);
        api.queueRequest(homeDCApi);
        api.executeRequests();
        nearestDCList = homeDCApi.nearestDCList;
        
        // return nearestDCList.size() > 0?nearestDCList[0].refName:'';
        System.debug('dc for zipcode ' + zipCode + ' is ' + nearestDCList);
        return nearestDCList.size() > 0?nearestDCList[0].dcNbr:'';
    }*/
    @RemoteAction
    global static ccrz.cc_RemoteActionResult addressValidation(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            String address1 = (String) input.get('address1');
			String address2 = (String) input.get('address2');
			String address3 = (String) input.get('address3');
			String city = (String) input.get('city');
			String country = 'US';
			String firstName = (String) input.get('firstName');
			String lastName = (String) input.get('lastName');
			String zip = (String) input.get('postalCode');
			String state = (String) input.get('state');
			Boolean hasLiftGate = (Boolean) input.get('liftGate');
            String addressType = (String) input.get('type');
			String storefront = ccrz.cc_CallContext.storefront;
			String addressString = address1 + ' ' + address2 + ' ' + address3 + ' ' + city + ' ' + state + ' ' + zip + ' ' + country;
            Boolean correctAddress = false;
			String addressCheckMessage = '';
			CCAviAddressyAPI.RetrieveResponse detailResponse;
			CCAviAddressyAPI.FindResponse findResponse = CCAviAddressyAPI.find(storefront, addressString);
            if (findResponse.success) {
                if(findResponse.addresses.size() > 1){
                    response.data = findResponse.addresses;                        
                    return response;
                }else if (findResponse.addresses.size() == 1 && findResponse.addresses[0].Type == 'Address') {
                    response.success = true;
                    correctAddress = true;
                }else{
                    response.success = false;
                    addressCheckMessage = 'Unable to locate the address.';  
                }
                
            }else {
                response.success = false;
                addressCheckMessage = 'Unable to validate the address.';
            }
            if (!correctAddress) {
                    // todo: show addressCheckMessage to user.
                    CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'message' => addressCheckMessage, 'findResponse'=>findResponse});
                } 
            
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response;
    }    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult changeAddress(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        
        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = ccrz.cc_CallContext.currContact.Id;
        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            CCPDCUserPermissionsHelper.UserPermissions permissions =  CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);
            Boolean hasPermission = permissions.allowOverrideShipTo;
            String sfid = (String) input.get('sfid');
            if (hasPermission) {             
                    input.put('country', 'US');
                    updateAddress(ctx, input, response);
                    return response;
                }                           
            else {
                response.success = false;
                response.data = input;
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                                             new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
                                            );
        }
        return response;
    }
    
    private static void updateAddress(ccrz.cc_RemoteActionContext ctx,Map<string, object> input,ccrz.cc_RemoteActionResult response) {
        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = ccrz.cc_CallContext.currContact.Id;
        String cartId = ccrz.cc_CallContext.currCartId;
        
        String sfid = (String) input.get('sfid');
        String address1 = (String) input.get('address1');
        String address2 = (String) input.get('address2');
        String address3 = (String) input.get('address3');
        String city = (String) input.get('city');
        String country = (String) input.get('country');
        String firstName = (String) input.get('firstName');
        String lastName = (String) input.get('lastName');
        String zip = (String) input.get('postalCode');
        String state = (String) input.get('state');
        Boolean hasLiftGate = (Boolean) input.get('liftGate');
        Boolean saveToAddressBook;
        if(test.isRunningTest())
        {
            saveToAddressBook=  Boolean.valueof(input.get('saveToAddressBook')); 
        }
        else
        {
            saveToAddressBook = (Boolean) input.get('saveToAddressBook'); 
        }
        String addressType = (String) input.get('type');
        String storefront = ccrz.cc_CallContext.storefront;
		if(sfid != null && sfid != ''){
            ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
                Id = sfid,
                ccrz__AddressFirstline__c = address1,
                ccrz__AddressSecondline__c = address2,
                ccrz__AddressThirdline__c = address3,
                ccrz__City__c = city,
                ccrz__Country__c = country,
                ccrz__FirstName__c = firstName,
                ccrz__LastName__c = lastName,
                ccrz__PostalCode__c = (zip != null?zip.substring(0,5):zip),
                ccrz__State__c = state,
                CC_FP_Has_Lift_Gate__c = hasLiftGate,
                CC_FP_Type__c = addressType//,
                //CC_FP_Location__c = location == null ? null : location.Id
            );
            update addr;
            response.success = true;
            response = getCart(ctx);
            //updateCartWithNewLocationDc(cartId);
            
            if(saveToAddressBook) {
                List<ccrz__E_AccountAddressBook__c> existingAcctAddr = [Select Id from ccrz__E_AccountAddressBook__c where ccrz__Account__c =: accountId and ccrz__AddressType__c = 'Shipping' and ccrz__E_ContactAddress__c =: addr.Id];
                If(existingAcctAddr.isEmpty()){
                    ccrz__E_AccountAddressBook__c acctAddr = new ccrz__E_AccountAddressBook__c(
                        ccrz__Account__c = accountId, ccrz__AddressType__c = 'Shipping', ccrz__E_ContactAddress__c = addr.Id
                    );
                    insert acctAddr;    
                }
                
                response.success = true;
                response = getCart(ctx);                    
            }                                  
            
        } else {
            ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
                //Id = sfid,
                ccrz__AddressFirstline__c = address1,
                ccrz__AddressSecondline__c = address2,
                ccrz__AddressThirdline__c = address3,
                ccrz__City__c = city,
                ccrz__Country__c = country,
                ccrz__FirstName__c = firstName,
                ccrz__LastName__c = lastName,
                ccrz__PostalCode__c = (zip != null?zip.substring(0,5):zip),
                ccrz__State__c = state,
                //CC_FP_County__c = county,
                CC_FP_Has_Lift_Gate__c = hasLiftGate,
                CC_FP_Type__c = addressType//,
                //CC_FP_Location__c = location == null ? null : location.Id
            );
            insert addr;
            response.success = true;
            response = getCart(ctx);
            //updateCartWithNewLocationDc(cartId);
            
            if(saveToAddressBook) {
                ccrz__E_AccountAddressBook__c acctAddr = new ccrz__E_AccountAddressBook__c(
                    ccrz__Account__c = accountId,      ccrz__AddressType__c = 'Shipping',
                    ccrz__E_ContactAddress__c = addr.Id
                );
                insert acctAddr;
                response.success = true;
                response = getCart(ctx);                    
            }                                  
            
        }  
        
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult changeAddressById(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        
        Id accountId = ccrz.cc_CallContext.effAccountId;
        Id contactId = ccrz.cc_CallContext.currContact.Id;
        String cartId = ccrz.cc_CallContext.currCartId;
        
        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            
            CCPDCUserPermissionsHelper.UserPermissions permissions =  CCPDCUserPermissionsHelper.getUserPermissions(accountId, contactId);
            Boolean hasPermission = permissions.allowOverrideShipTo;
            
            String sfid = (String) input.get('sfid');
            String firstName = (String) input.get('firstName');
            String lastName = (String) input.get('lastName');            
            String addressId = (String) input.get('addressId');
            String addressType = (String) input.get('type');            
            Boolean hasLiftGate = (Boolean) input.get('liftGate');
            Boolean saveToAddressBook = (Boolean) input.get('saveToAddressBook');
            
            String storefront = ccrz.cc_CallContext.storefront;
            if (hasPermission) {
                Boolean correctAddress = false;
                String addressCheckMessage = '';                                         
                CCAviAddressyAPI.RetrieveResponse detailResponse = CCAviAddressyAPI.retrieveAddress(storefront,  addressId);
                if (detailResponse.success) {
                    // get DC   by zipcode
                    //String homeDC = getHomeDCFromOMS((detailResponse.address.ZipCode != null?detailResponse.address.ZipCode.substring(0,5):detailResponse.address.ZipCode));
                    //Location__c location = CCFPLocationDAO.getLocationForCode(homeDC);                    
                    
                    if(sfid != null && sfid != ''){
                        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
                            Id = sfid,
                            ccrz__AddressFirstline__c = detailResponse.address.Line1,
                            ccrz__AddressSecondline__c = detailResponse.address.Line2,
                            ccrz__AddressThirdline__c = detailResponse.address.Line3,
                            ccrz__City__c = detailResponse.address.City,
                            ccrz__Country__c = detailResponse.address.CountryCode2,
                            ccrz__FirstName__c = firstName,
                            ccrz__LastName__c = lastName,
                            ccrz__PostalCode__c = (detailResponse.address.ZipCode != null?detailResponse.address.ZipCode.substring(0,5):detailResponse.address.ZipCode),
                            ccrz__State__c = detailResponse.address.StateCode,
                            CC_FP_County__c = detailResponse.address.County,
                            CC_FP_Has_Lift_Gate__c = hasLiftGate,
                            CC_FP_Type__c = addressType//,
                            //CC_FP_Location__c = location == null ? null : location.Id
                        );
                        update addr;
                        response = getCart(ctx);
                        correctAddress = true;  
                        //updateCartWithNewLocationDc(cartId);  
                              
                        if(saveToAddressBook) {
                            List<ccrz__E_AccountAddressBook__c> existingAcctAddr = [Select Id from ccrz__E_AccountAddressBook__c where ccrz__Account__c =: accountId and ccrz__AddressType__c = 'Shipping' and ccrz__E_ContactAddress__c =: addr.Id];
                            If(existingAcctAddr.isEmpty()){
                                ccrz__E_AccountAddressBook__c acctAddr = new ccrz__E_AccountAddressBook__c(
                                    ccrz__Account__c = accountId, ccrz__AddressType__c = 'Shipping', ccrz__E_ContactAddress__c = addr.Id
                                );
                                insert acctAddr;    
                            }
                            
                            response.success = true;
                            correctAddress = true;                   
                        }                                                
                        
                    } else {
                        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
                            //Id = sfid,
                            ccrz__AddressFirstline__c = detailResponse.address.Line1,
                            ccrz__AddressSecondline__c = detailResponse.address.Line2,
                            ccrz__AddressThirdline__c = detailResponse.address.Line3,
                            ccrz__City__c = detailResponse.address.City,
                            ccrz__Country__c = detailResponse.address.CountryCode2,
                            ccrz__FirstName__c = firstName,
                            ccrz__LastName__c = lastName,
                            ccrz__PostalCode__c = (detailResponse.address.ZipCode != null?detailResponse.address.ZipCode.substring(0,5):detailResponse.address.ZipCode),
                            ccrz__State__c = detailResponse.address.StateCode,
                            CC_FP_County__c = detailResponse.address.County,
                            CC_FP_Has_Lift_Gate__c = hasLiftGate,
                            CC_FP_Type__c = addressType//,
                            //CC_FP_Location__c = location == null ? null : location.Id
                        );
                        insert addr;                    
                        correctAddress = true;
                        
                        //updateCartWithNewLocationDc(cartId);                                        
                        if(saveToAddressBook) {
                            ccrz__E_AccountAddressBook__c acctAddr = new ccrz__E_AccountAddressBook__c(
                                ccrz__Account__c = accountId,
                                ccrz__AddressType__c = 'Shipping',
                                ccrz__E_ContactAddress__c = addr.Id
                            );
                            insert acctAddr;    
                            correctAddress = true;                      
                            response.success = true;
                        }                        
                        
                    }
                    
                    response = getCart(ctx);
                    
                } else {
                    addressCheckMessage = 'Unable to validate address.';
                }
                if (!correctAddress) {
                    // todo: show addressCheckMessage to user.
                    CCAviPageUtils.buildResponseData(response, false,
                                                     new Map<String,Object>{'message' => addressCheckMessage}
                                                    );
                }              
            }
            else {
                response.success = false;
                response.data = input;
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                                             new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
                                            );
        }
        return response;
    }         
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getAddressBook(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        
        try {
            ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(ctx.currentCartId);
            List<ccrz.cc_bean_MockContactAddress> book = CCFPAddressBookHelper.getAddressBookForAccountShipping(Id.valueOf(cart.ccrz__Account__c));
            CCAviPageUtils.buildResponseData(response, true,
                                             new Map<String,Object>{'addresses' => book}
                                            );
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                                             new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
                                            );
        }finally{
            ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','getAddressBook');
            ccrz.ccLog.close(response);
        }
        return response;
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult changeAddressFromBook(ccrz.cc_RemoteActionContext ctx, String sfid, String shipToId) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        Id accountId = ccrz.cc_CallContext.effAccountId;
        String cartId = ccrz.cc_CallContext.currCartId;
       try {
             if (sfid != null && sfid != '' && shipToId != null && shipToId != '') {
                
               ccrz__E_ContactAddr__c input = CCFPContactAddressDAO.getAddress(sfid);               
                
                ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
                    Id = shipToId,
                    ccrz__AddressFirstline__c = input.ccrz__AddressFirstline__c,
                    ccrz__AddressSecondline__c = input.ccrz__AddressSecondline__c,
                    ccrz__AddressThirdline__c = input.ccrz__AddressThirdline__c,
                    ccrz__City__c = input.ccrz__City__c,
                    ccrz__Country__c = input.ccrz__Country__c,
                    ccrz__FirstName__c = input.ccrz__FirstName__c,
                    ccrz__LastName__c = input.ccrz__LastName__c,
                    ccrz__PostalCode__c = input.ccrz__PostalCode__c,
                    ccrz__State__c = input.ccrz__State__c,
                    CC_FP_Has_Lift_Gate__c = input.CC_FP_Has_Lift_Gate__c,
                    CC_FP_Type__c = input.CC_FP_Type__c//,
                    //CC_FP_Location__c = input.CC_FP_Location__c
                );
                update addr;
                response = getCart(ctx);
                //updateCartWithNewLocationDc(cartId);
            }
            else {
                response.success = false;
            }       }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                                             new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
                                            );
        }
        return response;
    }
    /*@RemoteAction
    global static ccrz.cc_RemoteActionResult setCartWithNewLocationDc(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        String cartId = getEncCartId();
        if(cartId != null){
            try{
                updateCartWithNewLocationDc(cartId);
                response.success = true;
                
            }catch(Exception e){
                CCAviPageUtils.buildResponseData(response, false,
                                                 new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
                                                );
            }            
        }        
        return response;        
    }*/
    
    /*private static void updateCartWithNewLocationDc(String cartId){    
        ccrz__E_Cart__c cart = [SELECT Id, CC_FP_Location__c, ccrz__ShipTo__c, ccrz__ShipTo__r.CC_FP_Location__c FROM ccrz__E_Cart__c WHERE ccrz__EncryptedId__c =: cartId OR Id=: cartId];
        if(cart != null) {            
            Id cartShipToLocation = cart.ccrz__ShipTo__r.CC_FP_Location__c;      
            if(cartShipToLocation != null){
                cart.CC_FP_Location__c = cartShipToLocation;
                update cart;
            } else {
                //oms call
                ccrz__E_ContactAddr__c updatedAddr = CCFPContactAddressDAO.getAddress(cart.ccrz__ShipTo__c);
                String zip = updatedAddr.ccrz__PostalCode__c.substring(0,5);                                  
                String homeDC = getHomeDCFromOMS(zip);
                Location__c location = CCFPLocationDAO.getLocationForCode(homeDC);          
                updatedAddr.CC_FP_Location__c = location.Id;
                update updatedAddr;
                cart.CC_FP_Location__c = location.Id;
                update cart;
            }
        }
        
    }*/
    
    private static String getEncCartId() {
        String cartId = ccrz.cc_CallContext.currCartId;
        if (cartId != null) {
            ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(cartId);
            if (cart == null || cart.ccrz__CartStatus__c != 'Open') {
                cartId = null;
            }
        }
        if (cartId == null) {
            Map<String, Object> data = CCAviCartManager.getActiveCart();
            if (data != null) {
                cartId = (String) data.get(CCAviCartManager.ENCRYPTED_ID_KEY);
            }
        }   
        return cartId;     
    }
    //willCall As Default
    @RemoteAction
    global static Integer changeDefaultWillCall(Boolean isWillCall) {
        String currcartId = getEncCartId();
        updateWillCall(currcartId,isWillCall);
        return 0;
    }
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult setDefaultWillCall(ccrz.cc_RemoteActionContext ctx, Boolean deFaultWillCall) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        try{
            Id contactId = ccrz.cc_CallContext.currContact.Id;
            //setWillCall(contactId,deFaultWillCall);
            updateContact(contactId,deFaultWillCall);
            response.success = true;
        }catch(Exception e){
            CCAviPageUtils.buildResponseData(response, false,
                                             new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
                                            );
        }  
        return response;
        
    }
    
    @future
    public static void updateWillCall(String currcartId, Boolean isWillCall){
        ccrz__E_Cart__c currcart = CCPDCCartDAO.getCart(currcartId);
        currcart.CC_FP_Is_Will_Call__c = isWillCall;
        update currcart;
        
    }
    //CloudCraze JLL - combining remote functions to save server round trip time
    @RemoteAction
    global static ccrz.cc_RemoteActionResult doCombinedGetCartSetNewLocationAction(ccrz.cc_RemoteActionContext ctx){
        ccrz.cc_RemoteActionResult cartResponse = getCart(ctx);
        /* Commenting out the code block for performance issues. Need to investigate the underlying
requirement to have the setCartWithNewLocationDC within this flow.

ccrz.cc_RemoteActionResult cartLocationResponse = setCartWithNewLocationDc(ctx);
//ccrz.cc_RemoteActionResult combinedResult = CCAviPageUtils.combineResults(cartLocationResponse, cartResponse);

if(cartLocationResponse.success){
cartResponse.success = true;
}
else{
cartResponse.success = false;
}
*/
        return cartResponse;
    }
    
    
    private static void updateContact(String contactId, Boolean deFaultWillCall){
        Contact con = [SELECT Id, Will_Call_As_Default__c FROM Contact WHERE Id=:contactId];
        con.Will_Call_As_Default__c = deFaultWillCall;
        update con;
    }
    
}