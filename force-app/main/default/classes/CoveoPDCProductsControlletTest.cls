@isTest
public class CoveoPDCProductsControlletTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = util.STOREFRONT;
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';
        insert settings;


        String SpecGroupId = 'oF8DSFDS78H6DH5A965HS2';

        // List<ccrz__E_Product__c> productList = new List<ccrz__E_Product__c>();

        // Create group
        ccrz__E_Category__c specGroupObject = new ccrz__E_Category__c(
            ccrz__CategoryID__c = 'My SpecGroup Name',
            Spec_Group__c = SpecGroupId
        );
        insert specGroupObject;

        // Create product
        ccrz__E_Product__c product1 = new ccrz__E_Product__c(
            Name = 'product 1',
            ccrz__SKU__c = 'asd-12345'
        );
        insert product1;

        // Link product to Group
        ccrz__E_ProductCategory__c productCategory1 = new ccrz__E_ProductCategory__c(
            ccrz__Product__c = product1.id,
            ccrz__Category__c = specGroupObject.id
        );
        insert productCategory1;

        // Add specs
        List<ccrz__E_Spec__c> specList = new List<ccrz__E_Spec__c>();
        specList.add(new ccrz__E_Spec__c(
            ccrz__DisplayName__c = 'Spec Test',
            ccrz__SpecGroup__c = specGroupObject.Spec_Group__c,
            ccrz__UseForFilter__c = true
        ));
        specList.add(new ccrz__E_Spec__c(
            ccrz__DisplayName__c = 'Spec Test not for filter',
            ccrz__SpecGroup__c = specGroupObject.Spec_Group__c,
            ccrz__UseForFilter__c = false
        ));
        insert specList;
    }

    static testmethod void priceTest() {
        /* This is a temporary method.
        * To delete once the customer login has been implemented
        */
        util.initCallContext();
        Test.setMock(HttpCalloutMock.class, new CCAviBoomiAPITest.BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE /><QPRICE>0.000</QPRICE><QPRCCATID /><CUSTNO>999999</CUSTNO><CUSTBR>0</CUSTBR><LOCATE>DA</LOCATE><PARTNO>product-02</PARTNO></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL /><PLEXCPT /><LLVL /><MULTPLR>0.0000</MULTPLR><PRLOOP /><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE /><QPRICE>0.000</QPRICE><QPRCCATID /><CUSTNO>999999</CUSTNO><CUSTBR>0</CUSTBR><LOCATE>DA</LOCATE><PARTNO>product-01</PARTNO></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        String jsonString = '[{"partNumber":"B99","poolNumber":"845","quantity":"1"},{"partNumber":"SC1600","poolNumber":"230","quantity":"1"}]';
        String response = null;

        Test.startTest();
        response = CoveoPDCProductsController.getPriceList(jsonString);
        Test.stopTest();

        System.assert(response != null);
    }

    static testmethod void vinDetailsTest() {
        Test.setMock(HttpCalloutMock.class, new CCVintelligenceAPITest.VintelligenceServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><ns0:decodeVinResponse xmlns:ns0="http://webservice.vindecoder.polk.com/"><VinResponse><vin>1HTJSSKK2CJ062123</vin><returnCode>0</returnCode><correctedVin></correctedVin><errorBytes>                 </errorBytes><fields name="ACES_ASPIRATION_NAME">Turbocharged</fields><fields name="ACES_ASP_ID">6</fields><fields name="ACES_BASE_VEHICLE">123188</fields><fields name="ACES_BLOCK_TYPE"></fields><fields name="ACES_BODY_NMBR_DR">4</fields><fields name="ACES_BODY_STYLE_CONFIG_ID">158</fields><fields name="ACES_BODY_TYPE">70</fields><fields name="ACES_BODY_TYPE_NAME">Straight Truck - Medium Conventional</fields><fields name="ACES_CC_DISPLACEMENT">6369</fields><fields name="ACES_CI_DISPLACEMENT">389</fields><fields name="ACES_CYLINDERS">8</fields><fields name="ACES_CYL_HEAD_TYPE_ID">5</fields><fields name="ACES_CYL_HEAD_TYPE_NM">SOHC</fields><fields name="ACES_DRIVE_ID">12</fields><fields name="ACES_DRIVE_TYPE_NAME">4 X 2</fields><fields name="ACES_ENGINE_BASE_ID">6038</fields><fields name="ACES_ENGINE_CONFIG_ID">18167</fields><fields name="ACES_ENGINE_DESIGNATION_ID">4844</fields><fields name="ACES_ENGINE_DESIGNATION_NAME">MaxxForce 7</fields><fields name="ACES_ENG_VIN_ID">1</fields><fields name="ACES_FUEL">6</fields><fields name="ACES_FUEL_DELIVERY">5</fields><fields name="ACES_FUEL_DEL_CONFIG_ID">239</fields><fields name="ACES_FUEL_DEL_TYPE_NAME">FI</fields><fields name="ACES_FUEL_TYPE_NAME">DIESEL</fields><fields name="ACES_LITERS">6.4</fields><fields name="ACES_MAKE_ID">71</fields><fields name="ACES_MAKE_NAME">International</fields><fields name="ACES_MDL_ID">22656</fields><fields name="ACES_MODEL_NAME">TerraStar</fields><fields name="ACES_POWER_OUTPUT_ID">1</fields><fields name="ACES_REGION_ID">1</fields><fields name="ACES_REGION_NAME">United States</fields><fields name="ACES_RESERVED"></fields><fields name="ACES_SUB_MDL_ID">20</fields><fields name="ACES_SUB_MODEL_NAME">Base</fields><fields name="ACES_VALVES_ID"></fields><fields name="ACES_VALVES_PER_ENG"></fields><fields name="ACES_VEHICLE_ID">200069</fields><fields name="ACES_VEH_ENGCONFIG_ID">391091</fields><fields name="ACES_VEH_TYPE_NAME">Medium/Heavy Truck</fields><fields name="ACES_VEH_TYP_ID">2187</fields><fields name="ACES_YEAR_ID">2012</fields><fields name="BAT_KW_RATG_CD"></fields><fields name="BAT_TYP_CD"></fields><fields name="BAT_TYP_DESC"></fields><fields name="BAT_VLT_DESC"></fields><fields name="BODY_STYLE_CD">ST</fields><fields name="BODY_STYLE_DESC">STRAIGHT TRUCK</fields><fields name="CMMRCL_TRLR_BODY_SPC_CD"></fields><fields name="CMMRCL_TRLR_BODY_SPC_DESC"></fields><fields name="CMMRCL_TRLR_LEN"></fields><fields name="CMMRCL_TRLR_VSL_CPCT">0</fields><fields name="CMMRCL_TRLR_VSL_MATR_CD"></fields><fields name="CMMRCL_TRLR_VSL_MATR_DESC"></fields><fields name="DOOR_CNT">0</fields><fields name="DRV_TYP_CD">RWD</fields><fields name="DRV_TYP_DESC">Rear Wheel Drive</fields><fields name="ENG_ASP_SUP_CHGR_CD"></fields><fields name="ENG_ASP_SUP_CHGR_DESC"></fields><fields name="ENG_ASP_TRBL_CHGR_CD"></fields><fields name="ENG_ASP_TRBL_CHGR_DESC"></fields><fields name="ENG_ASP_VVTL_CD"></fields><fields name="ENG_ASP_VVTL_DESC"></fields><fields name="ENG_BLCK_TYP_CD"></fields><fields name="ENG_BLCK_TYP_DESC"></fields><fields name="ENG_CBRT_BRLS"></fields><fields name="ENG_CBRT_TYP_CD"></fields><fields name="ENG_CBRT_TYP_DESC"></fields><fields name="ENG_CLNDR_RTR_CNT">8</fields><fields name="ENG_CYCL_CNT"></fields><fields name="ENG_DISPLCMNT_CC">0</fields><fields name="ENG_DISPLCMNT_CI">390</fields><fields name="ENG_DISPLCMNT_CL">6.4</fields><fields name="ENG_FUEL_CD">D</fields><fields name="ENG_FUEL_DESC">Diesel</fields><fields name="ENG_FUEL_INJ_TYP_CD"></fields><fields name="ENG_FUEL_INJ_TYP_DESC"></fields><fields name="ENG_HEAD_CNFG_CD"></fields><fields name="ENG_HEAD_CNFG_DESC"></fields><fields name="ENG_MDL_CD">165175</fields><fields name="ENG_MDL_DESC">MAXXFORCE 7</fields><fields name="ENG_MFG_CD">165</fields><fields name="ENG_MFG_DESC">International</fields><fields name="ENG_TRK_DUTY_TYP_CD">ME</fields><fields name="ENG_TRK_DUTY_TYP_DESC">Medium Duty</fields><fields name="ENG_VIN_CD">SK</fields><fields name="ENG_VLVS_PER_CLNDR">0</fields><fields name="ENG_VLVS_TOTL">0</fields><fields name="FRNT_TYRE_SIZE_CD"></fields><fields name="FRNT_TYRE_SIZE_DESC"></fields><fields name="FRONT_TIRE_OP1_SIZE_CD"></fields><fields name="FRONT_TIRE_OP1_SIZE_DESC"></fields><fields name="FRONT_TIRE_OP2_SIZE_CD"></fields><fields name="FRONT_TIRE_OP2_SIZE_DESC"></fields><fields name="FRONT_TIRE_OP3_SIZE_CD"></fields><fields name="FRONT_TIRE_OP3_SIZE_DESC"></fields><fields name="FRONT_TIRE_OP4_SIZE_CD"></fields><fields name="FRONT_TIRE_OP4_SIZE_DESC"></fields><fields name="INCOMPLETE_IND">Y</fields><fields name="MAK_CD">INT</fields><fields name="MAK_NM">INTERNATIONAL</fields><fields name="MDL_CD">4774</fields><fields name="MDL_DESC">TERRASTAR</fields><fields name="MDL_YR">2012</fields><fields name="MFG_BAS_MSRP">0</fields><fields name="MFG_CD">C162</fields><fields name="MFG_DESC">Navistar International</fields><fields name="MOTR_CYCL_USAG_CD"></fields><fields name="MOTR_CYCL_USAG_DESC"></fields><fields name="NCI_MAK_ABBR_CD">INTL</fields><fields name="NCI_SERIES_ABBR_CD">999</fields><fields name="OPT1_TRIM_DESC"></fields><fields name="OPT2_TRIM_DESC"></fields><fields name="OPT3_TRIM_DESC"></fields><fields name="OPT4_TRIM_DESC"></fields><fields name="ORGN_CD">D</fields><fields name="ORGN_DESC">Domestic</fields><fields name="PLNT_CD">J</fields><fields name="PLNT_CITY_NM">GARLAND</fields><fields name="PLNT_CNTRY_NM">United States</fields><fields name="PLNT_ISO_CNTRY_CD">USA</fields><fields name="PLNT_STAT_PROV_CD">TX</fields><fields name="PLNT_STAT_PROV_NM">TEXAS</fields><fields name="PRIC_VAR">0</fields><fields name="PROACTIVE_IND">N</fields><fields name="REAR_TIRE_OP1_SIZE_CD"></fields><fields name="REAR_TIRE_OP1_SIZE_DESC"></fields><fields name="REAR_TIRE_OP2_SIZE_CD"></fields><fields name="REAR_TIRE_OP2_SIZE_DESC"></fields><fields name="REAR_TIRE_OP3_SIZE_CD"></fields><fields name="REAR_TIRE_OP3_SIZE_DESC"></fields><fields name="REAR_TIRE_OP4_SIZE_CD"></fields><fields name="REAR_TIRE_OP4_SIZE_DESC"></fields><fields name="REAR_TIRE_SIZE_CD"></fields><fields name="REAR_TIRE_SIZE_DESC"></fields><fields name="SALE_CNTRY_CD"></fields><fields name="SALE_CNTRY_DESC"></fields><fields name="SEGMENTATION_CD">9</fields><fields name="SEGMENTATION_DESC">Commercial Truck</fields><fields name="SHIP_WGHT_LBS">0</fields><fields name="SHIP_WGHT_VAR_LBS">0</fields><fields name="TRIM_DESC"></fields><fields name="TRK_BED_LEN_CD"></fields><fields name="TRK_BED_LEN_DESC"></fields><fields name="TRK_BRK_TYP_CD">HYD</fields><fields name="TRK_BRK_TYP_DESC">HYDRAULIC</fields><fields name="TRK_CAB_CNFG_CD">CMN</fields><fields name="TRK_CAB_CNFG_DESC">Medium Conventional</fields><fields name="TRK_FRNT_AXL_CD">SF</fields><fields name="TRK_FRNT_AXL_DESC">Setforward</fields><fields name="TRK_GRSS_VEH_WGHT_RATG_CD">5</fields><fields name="TRK_GRSS_VEH_WGHT_RATG_DESC">16,001 - 19,500#</fields><fields name="TRK_REAR_AXL_CD">S</fields><fields name="TRK_REAR_AXL_DESC">Single</fields><fields name="TRK_TNG_RAT_CD"></fields><fields name="TRK_TNG_RAT_DESC"></fields><fields name="TRK_TRLR_AXL_CNT"></fields><fields name="VEH_TYP_CD">T</fields><fields name="VEH_TYP_DESC">Truck</fields><fields name="VEH_VIN_POS_DESC">OMVSSEEGKYPNNNNNN</fields><fields name="VINA_BODY_TYPE_CD">CC</fields><fields name="VINA_SERIES_ABBR_CD">TERTST</fields><fields name="VIN_MDLYR_CHAR">C</fields><fields name="VIN_PATRN">INT  2012 1HTJSSKK\\CCJ\\R000001-999999</fields><fields name="WHL_BAS_LNGST_INCHS"></fields><fields name="WHL_BAS_SHRST_INCHS"></fields><fields name="WHL_CNT">4</fields><fields name="WHL_DRVN_CNT">2</fields></VinResponse></ns0:decodeVinResponse></S:Body></S:Envelope>'));

        String vin = '1HTJSSKK2CJ062123';
        String response = null;

        Test.startTest();
        response = CoveoPDCProductsController.getVinDetails(vin);
        Test.stopTest();

        System.assert(response != null);
    }

    static testmethod void getSpecGroupTreeTest() {
        String specTree = null;
        Test.startTest();
        specTree = CoveoPDCProductsController.getSpecGroupTree();
        Test.stopTest();
        System.assert(specTree == '{"My SpecGroup Name":["Spec Test"]}', specTree);
    }
    
    static testmethod void getProductInventoryForSkuTest() {
        util.initCallContext();
		ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        List<String> skus = new List<String>();
        skus.add('product-01');
        Test.startTest();
        CoveoPDCProductsController.getProductInventoryForSku(ctx,skus);
        Test.stopTest();
    }

}