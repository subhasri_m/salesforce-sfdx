@isTest
public class CCAviBoomiAPITest {
    
    static CCPDCTestUtil util = new CCPDCTestUtil();
    static Map<String, Object> m = new Map<String, Object>();

   @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }

        ccrz.cc_util_Reflection.createStorefrontSetting('parts');
        
        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = 'store1';
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
    }

    static testmethod void authorizationFailPricingTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(401, 'Unauthorized', '<html><head></head><body><h2>HTTP ERROR 401</h2></body></html>'));

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        HttpRequest requestCont = null;
        List<CCAviBoomiAPI.SeriesPricingWSRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
        CCAviBoomiAPI.SeriesPricingWSRequest request;

        request =  new CCAviBoomiAPI.SeriesPricingWSRequest();
        request.AccountNumber = '2222';
        request.CustomerBranch = '2';
        request.PoolNumber = '845';
        request.Location = 'ABQ';
        request.PartNumber = 'B99';
        request.Quantity = '1';
        request.Price = '5';
        request.SPCIND = '';
        request.COMPY = '1';
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesPricingWS('store1', lstRequests);
        requestCont = CCAviBoomiAPI.getISeriesPricingWSWithCont('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals('401: Unauthorized Access', response.message);
        System.assert(requestCont != null);
    }

    static testmethod void noInputPricingTest() {
        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        List<CCAviBoomiAPI.SeriesPricingWSRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
        CCAviBoomiAPI.SeriesPricingWSRequest request;        

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesPricingWS('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals('Input list is empty', response.message);
    }

    static testmethod void invalidStorePricingTest() {
        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        List<CCAviBoomiAPI.SeriesPricingWSRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
        CCAviBoomiAPI.SeriesPricingWSRequest request;

        request =  new CCAviBoomiAPI.SeriesPricingWSRequest();
        request.AccountNumber = '2222';
        request.CustomerBranch = '2';
        request.PoolNumber = '845';
        request.Location = 'ABQ';
        request.PartNumber = 'B99';
        request.Quantity = '1';
        request.Price = '5';
        request.SPCIND = '';
        request.COMPY = '1';
        lstRequests.add(request);     

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesPricingWS('store2', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals('Invalid store front', response.message);
    }

    static testmethod void invalidResponsePricingTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(500, 'Internal Server Error', '<html><head></head><body><h2>Internal Server Error</h2></body></html>'));

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        List<CCAviBoomiAPI.SeriesPricingWSRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
        CCAviBoomiAPI.SeriesPricingWSRequest request;

        request =  new CCAviBoomiAPI.SeriesPricingWSRequest();
        request.AccountNumber = '2222';
        request.CustomerBranch = '2';
        request.PoolNumber = '845';
        request.Location = 'ABQ';
        request.PartNumber = 'B99';
        request.Quantity = '1';
        request.Price = '5';
        request.SPCIND = '';
        request.COMPY = '1';
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesPricingWS('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
    }

    static testmethod void httpExceptionPricingTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceExceptionMock());

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        List<CCAviBoomiAPI.SeriesPricingWSRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
        CCAviBoomiAPI.SeriesPricingWSRequest request;

        request =  new CCAviBoomiAPI.SeriesPricingWSRequest();
        request.AccountNumber = '2222';
        request.CustomerBranch = '2';
        request.PoolNumber = '845';
        request.Location = 'ABQ';
        request.PartNumber = 'B99';
        request.Quantity = '1';
        request.Price = '5';
        request.SPCIND = '';
        request.COMPY = '1';
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesPricingWS('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
    }


    static testmethod void validPricingTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getISeriesPricingWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:ISeriesPricingWSResponseList><ISeriesPricing><Pricing><PRICE>7.820</PRICE><LEVEL>4</LEVEL><PLEXCPT>L</PLEXCPT><LLVL>5</LLVL><MULTPLR>0.3900</MULTPLR><PRLOOP>POOL</PRLOOP><VELCTY>N</VELCTY><CSTMTRX>Y</CSTMTRX><BASEPRICE>16.240</BASEPRICE><GOTPRICE>Y</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2222</CUSTNO><PARTNO>B99</PARTNO><POOL>100</POOL></Pricing></ISeriesPricing><ISeriesPricing><Pricing><PRICE>10000.000</PRICE><LEVEL/><PLEXCPT/><LLVL/><MULTPLR>0.0000</MULTPLR><PRLOOP/><VELCTY>N</VELCTY><CSTMTRX>N</CSTMTRX><BASEPRICE>0.000</BASEPRICE><GOTPRICE>N</GOTPRICE><NATQUOTE/><QPRICE>0.000</QPRICE><QPRCCATID/><CUSTNO>2121</CUSTNO><PARTNO>9</PARTNO><POOL>100</POOL></Pricing></ISeriesPricing></wss:ISeriesPricingWSResponseList></wss:getISeriesPricingWSResponse></S:Body></S:Envelope>'));

        CCAviBoomiAPI.SeriesPricingWSResponse response = null;
        List<CCAviBoomiAPI.SeriesPricingWSRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesPricingWSRequest>();
        CCAviBoomiAPI.SeriesPricingWSRequest request;

        request =  new CCAviBoomiAPI.SeriesPricingWSRequest();
        request.AccountNumber = '2222';
        request.CustomerBranch = '2';
        request.PoolNumber = '845';
        request.Location = 'ABQ';
        request.PartNumber = 'B99';
        request.Quantity = '1';
        request.Price = '5';
        request.SPCIND = '';
        request.COMPY = '1';
        lstRequests.add(request);

        request =  new CCAviBoomiAPI.SeriesPricingWSRequest();
        request.AccountNumber = '2121';
        request.CustomerBranch = '2';
        request.PoolNumber = '845';
        request.Location = 'ABQ';
        request.PartNumber = '9';
        request.Quantity = '1';
        request.Price = '5';
        request.SPCIND = '';
        request.COMPY = '1';
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesPricingWS('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(response.success);
        System.assert(response.message == null);
        System.assertEquals(2, response.listPricing.size());
    }

    static testmethod void authorizationFailOrderTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(401, 'Unauthorized', '<html><head></head><body><h2>HTTP ERROR 401</h2></body></html>'));

        CCAviBoomiAPI.CreateOrderCCResponse response = null;
        List<CCAviBoomiAPI.CreateOrderCCRequest> lstRequests =  new List<CCAviBoomiAPI.CreateOrderCCRequest>();
        CCAviBoomiAPI.CreateOrderCCRequest request;

        request =  new CCAviBoomiAPI.CreateOrderCCRequest();
        request.header = new CCAviBoomiAPI.CreateOrderCCHeader();
        request.listDetails = new List<CCAviBoomiAPI.CreateOrderCCDetail>();
        CCAviBoomiAPI.CreateOrderCCDetail detail = new CCAviBoomiAPI.CreateOrderCCDetail();
        request.listDetails.add(detail);
        request.payment = new CCAviBoomiAPI.CreateOrderCCPayment();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getCreateOrderCC('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
        System.assertEquals('401: Unauthorized Access', response.message);
    }

    static testmethod void noInputOrderTest() {
        CCAviBoomiAPI.CreateOrderCCResponse response = null;
        List<CCAviBoomiAPI.CreateOrderCCRequest> lstRequests =  new List<CCAviBoomiAPI.CreateOrderCCRequest>();
        CCAviBoomiAPI.CreateOrderCCRequest request;        

        Test.startTest();
        response = CCAviBoomiAPI.getCreateOrderCC('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
        System.assertEquals('Input list is empty', response.message);
    }

    static testmethod void invalidStoreOrderTest() {
        CCAviBoomiAPI.CreateOrderCCResponse response = null;
        List<CCAviBoomiAPI.CreateOrderCCRequest> lstRequests =  new List<CCAviBoomiAPI.CreateOrderCCRequest>();
        CCAviBoomiAPI.CreateOrderCCRequest request;

        request =  new CCAviBoomiAPI.CreateOrderCCRequest();
        request.header = new CCAviBoomiAPI.CreateOrderCCHeader();
        request.listDetails = new List<CCAviBoomiAPI.CreateOrderCCDetail>();
        CCAviBoomiAPI.CreateOrderCCDetail detail = new CCAviBoomiAPI.CreateOrderCCDetail();
        request.listDetails.add(detail);
        request.payment = new CCAviBoomiAPI.CreateOrderCCPayment();
        lstRequests.add(request);    

        Test.startTest();
        response = CCAviBoomiAPI.getCreateOrderCC('store2', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
        System.assertEquals('Invalid store front', response.message);
    }

    static testmethod void invalidResponseOrderTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(500, 'Internal Server Error', '<html><head></head><body><h2>Internal Server Error</h2></body></html>'));

        CCAviBoomiAPI.CreateOrderCCResponse response = null;
        List<CCAviBoomiAPI.CreateOrderCCRequest> lstRequests =  new List<CCAviBoomiAPI.CreateOrderCCRequest>();
        CCAviBoomiAPI.CreateOrderCCRequest request;

        request =  new CCAviBoomiAPI.CreateOrderCCRequest();
        request.header = new CCAviBoomiAPI.CreateOrderCCHeader();
        request.listDetails = new List<CCAviBoomiAPI.CreateOrderCCDetail>();
        CCAviBoomiAPI.CreateOrderCCDetail detail = new CCAviBoomiAPI.CreateOrderCCDetail();
        request.listDetails.add(detail);
        request.payment = new CCAviBoomiAPI.CreateOrderCCPayment();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getCreateOrderCC('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
    }

    static testmethod void httpExceptionOrderTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceExceptionMock());

        CCAviBoomiAPI.CreateOrderCCResponse response = null;
        List<CCAviBoomiAPI.CreateOrderCCRequest> lstRequests =  new List<CCAviBoomiAPI.CreateOrderCCRequest>();
        CCAviBoomiAPI.CreateOrderCCRequest request;

        request =  new CCAviBoomiAPI.CreateOrderCCRequest();
        request.header = new CCAviBoomiAPI.CreateOrderCCHeader();
        request.listDetails = new List<CCAviBoomiAPI.CreateOrderCCDetail>();
        CCAviBoomiAPI.CreateOrderCCDetail detail = new CCAviBoomiAPI.CreateOrderCCDetail();
        request.listDetails.add(detail);
        request.payment = new CCAviBoomiAPI.CreateOrderCCPayment();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getCreateOrderCC('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
    }


    static testmethod void validOrderTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getCreateOrderCCResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:CreateOrderCCResponseList><OrderStatus><Status></Status><ErrorMessage></ErrorMessage></OrderStatus></wss:CreateOrderCCResponseList></wss:getCreateOrderCCResponse></S:Body></S:Envelope>'));

        CCAviBoomiAPI.CreateOrderCCResponse response = null;
        List<CCAviBoomiAPI.CreateOrderCCRequest> lstRequests =  new List<CCAviBoomiAPI.CreateOrderCCRequest>();
        CCAviBoomiAPI.CreateOrderCCRequest request;

        request =  new CCAviBoomiAPI.CreateOrderCCRequest();
        request.header = new CCAviBoomiAPI.CreateOrderCCHeader();
        request.listDetails = new List<CCAviBoomiAPI.CreateOrderCCDetail>();
        CCAviBoomiAPI.CreateOrderCCDetail detail = new CCAviBoomiAPI.CreateOrderCCDetail();
        request.listDetails.add(detail);
        request.payment = new CCAviBoomiAPI.CreateOrderCCPayment();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getCreateOrderCC('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(response.apiCallSuccess);
        System.assert(response.message == null);
        System.assertEquals(1, response.listOrderStatus.size());
    }
    
    static testmethod void authorizationFailDefaultTaxRateTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(401, 'Unauthorized', '<html><head></head><body><h2>HTTP ERROR 401</h2></body></html>'));

        CCAviBoomiAPI.SeriesDefaultTaxRateResponse response = null;
        List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest>();
        CCAviBoomiAPI.SeriesDefaultTaxRateRequest request;

        request =  new CCAviBoomiAPI.SeriesDefaultTaxRateRequest();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesDefaultTaxRate('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
        System.assertEquals('401: Unauthorized Access', response.message);
    }

    static testmethod void noInputDefaultTaxRateTest() {
        CCAviBoomiAPI.SeriesDefaultTaxRateResponse response = null;
        List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest>();
        CCAviBoomiAPI.SeriesDefaultTaxRateRequest request;        

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesDefaultTaxRate('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
        System.assertEquals('Input list is empty', response.message);
    }

    static testmethod void invalidStoreDefaultTaxRateTest() {

        CCAviBoomiAPI.SeriesDefaultTaxRateResponse response = null;
        List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest>();
        CCAviBoomiAPI.SeriesDefaultTaxRateRequest request;

        request =  new CCAviBoomiAPI.SeriesDefaultTaxRateRequest();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesDefaultTaxRate('store2', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
        System.assertEquals('Invalid store front', response.message);
    }

    static testmethod void invalidResponseDefaultTaxRateTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(500, 'Internal Server Error', '<html><head></head><body><h2>Internal Server Error</h2></body></html>'));

        CCAviBoomiAPI.SeriesDefaultTaxRateResponse response = null;
        List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest>();
        CCAviBoomiAPI.SeriesDefaultTaxRateRequest request;

        request =  new CCAviBoomiAPI.SeriesDefaultTaxRateRequest();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesDefaultTaxRate('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
    }

    static testmethod void httpExceptionDefaultTaxRateTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceExceptionMock());

        CCAviBoomiAPI.SeriesDefaultTaxRateResponse response = null;
        List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest>();
        CCAviBoomiAPI.SeriesDefaultTaxRateRequest request;

        request =  new CCAviBoomiAPI.SeriesDefaultTaxRateRequest();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesDefaultTaxRate('store1', lstRequests);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.apiCallSuccess);
    }


    static testmethod void validDefaultTaxRateTest() {
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(200, 'OK', TAX_RESPONSE));

        CCAviBoomiAPI.SeriesDefaultTaxRateResponse response = null;
        List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesDefaultTaxRateRequest>();
        CCAviBoomiAPI.SeriesDefaultTaxRateRequest request;

        request =  new CCAviBoomiAPI.SeriesDefaultTaxRateRequest();
        lstRequests.add(request);

        request =  new CCAviBoomiAPI.SeriesDefaultTaxRateRequest();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getISeriesDefaultTaxRate('store1', lstRequests);
        Test.stopTest();
    }

    static testmethod void validBackorderCancellationWS(){
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:getBackorderCancellationWSResponse xmlns:wss="http://www.boomi.com/connector/wss"><wss:BackorderCancellationWSResponseList><BackorderCancellationStatus><TicketStatus>Success</TicketStatus></BackorderCancellationStatus>></wss:BackorderCancellationWSResponseList></wss:getBackorderCancellationWSResponse></S:Body></S:Envelope>'));
        CCAviBoomiAPI.SeriesBackorderCancellationWSResponse response = null;
        List<CCAviBoomiAPI.SeriesBackorderCancellationWSRequest> lstRequests =  new List<CCAviBoomiAPI.SeriesBackorderCancellationWSRequest>();

        CCAviBoomiAPI.SeriesBackorderCancellationWSRequest request;
        request = new CCAviBoomiAPI.SeriesBackorderCancellationWSRequest();
        lstRequests.add(request);

        Test.startTest();
        response = CCAviBoomiAPI.getBackorderCancellationWS('store1', lstRequests);
        Test.stopTest();

    }

    public class BoomiServiceMock implements HttpCalloutMock {

        public Integer code {get; set;}
        public String status {get; set;}
        public String body {get; set;}

        public BoomiServiceMock(Integer code, String status, String body) {
            this.code = code;
            this.status = status;
            this.body = body;
        }

        public HTTPResponse respond(HTTPRequest request) {
            HttpResponse response = new HttpResponse();
            if (this.body != null) {
                response.setBody(body);
            }
            response.setStatusCode(this.code);
            response.setStatus(this.status);
            return response;
        }
    }


    public class BoomiServiceExceptionMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest request) {
           throw new BoomiAPIMockException('Exception result');
        }
    }

    public static final String TAX_RESPONSE = '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:querySalestaxCCResponse xmlns:wss="http://www.boomi.com/connector/wss"><SalesTaxResponse> <Response> <ShipmentID>a7j1W000000TfHGQA0</ShipmentID> <TaxAmount>42.21</TaxAmount> <ReturnCode>200</ReturnCode> <CodeDescription>Successful </CodeDescription> </Response> <Response> <ShipmentID>a7j1W000000TfHHQA0</ShipmentID> <TaxAmount>20.00</TaxAmount> <ReturnCode>200</ReturnCode> <CodeDescription>Successful </CodeDescription> </Response> <Total> <TotalTaxAmount>62.21</TotalTaxAmount> </Total> </SalesTaxResponse></wss:querySalestaxCCResponse></S:Body></S:Envelope>';

    public class BoomiAPIMockException extends Exception {}
    
    static testmethod void getFleetPrideIseriesTaxRateTest(){
        
        util.initCallContext();
        
        Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(200, 'OK', '<?xml version="1.0" encoding="UTF-8"?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:querySalestaxCCResponse xmlns:wss="http://www.boomi.com/connector/wss"><SalesTaxResponse> <Response> <ShipmentID>a7j1W000000TfHGQA0</ShipmentID> <TaxAmount>42.21</TaxAmount> <ReturnCode>200</ReturnCode> <CodeDescription>Successful </CodeDescription> </Response> <Response> <ShipmentID>a7j1W000000TfHHQA0</ShipmentID> <TaxAmount>20.00</TaxAmount> <ReturnCode>200</ReturnCode> <CodeDescription>Successful </CodeDescription> </Response> <Total> <TotalTaxAmount>62.21</TotalTaxAmount> </Total> </SalesTaxResponse></wss:querySalestaxCCResponse></S:Body></S:Envelope>'));
        CCAviBoomiAPI.FleetPrideIseriesTaxResponseWrapper response = null;
        List<CCAviBoomiAPI.FleetPrideIseriesTaxRequest> lstRequests =  new List<CCAviBoomiAPI.FleetPrideIseriesTaxRequest>();

        CCAviBoomiAPI.FleetPrideIseriesTaxRequest request;
        request = new CCAviBoomiAPI.FleetPrideIseriesTaxRequest();
        Account theAccount = util.getAccount();
        theAccount.Iseries_Company_code__c = 'testComCode';
        ccrz__E_Cart__c theCart = util.getCart();
        Location__c loc = util.getLocation();
        loc.Location__c = 'DT';
        update loc;
      
        theCart.CC_FP_Location__r = loc;
        update theCart;
        
        ccrz__E_ContactAddr__c addr = new ccrz__E_ContactAddr__c(
            ownerId = util.getPortalUser().Id,
            ccrz__AddressFirstline__c = '123 Main St',
            ccrz__City__c = 'Chicago',
            ccrz__State__c = 'IL',
            ccrz__Country__c = 'US',
            ccrz__PostalCode__c = '60606'
        );    
        insert addr; 
        
        ccrz__E_ContactAddr__c address = addr;
        ccrz__E_CartItemGroup__c ciGroup = util.createCartItemGroup(thecart, address, 'Shipment-1', 'Carrier', false, 6.00);
        
        CCAviBoomiAPI.FleetPrideIseriesTaxHeader headerRequest = new CCAviBoomiAPI.FleetPrideIseriesTaxHeader();
                        //header request
                        headerRequest.ShipmentID = ciGroup.Id;               
                        headerRequest.SellingCompany = theAccount.Iseries_Company_code__c;
                        headerRequest.CustomerNumber = theAccount.ISeries_Customer_Account__c;
                        headerRequest.CustomerBranch = theAccount.ISeries_Customer_Branch__c;
                        headerRequest.SellingLocation = theCart.CC_FP_Location__r.Location__c;
                        headerRequest.SalesDate = String.valueOf(Date.today().format());
                        headerRequest.ShipToAddr1 = theCart.CC_FP_Location__r.Address_Line_1__c;
                        headerRequest.ShipToAddr2 = theCart.CC_FP_Location__r.Address_Line_2__c;
                        headerRequest.ShipToCity = theCart.CC_FP_Location__r.City__c;
                        //headerRequest.ShipToState = theCart.CC_FP_Location__r.State__c;
                        headerRequest.ShipToCountry = 'US';
                        String county = theCart.CC_FP_Location__r.County__c;
                        if (county == null) {
                            county = ' ';
                        }
                        headerRequest.ShipToCounty = county;
                        headerRequest.DeliverMethod = 'W'; //willcall
                        headerRequest.TicketTotal = String.valueOf(ciGroup.CC_FP_Subtotal_Amount__c);
        
        
        //taxdetail
        ccrz__E_CartItem__c ci = util.getCartItem();
         ccrz__E_Product__c p = Util.getProduct();
        ci.ccrz__Product__r = p;
        update ci;
        
         List<CCAviBoomiAPI.FleetPrideIseriesTaxDetail> detailRequests = new List<CCAviBoomiAPI.FleetPrideIseriesTaxDetail>();                     
        CCAviBoomiAPI.FleetPrideIseriesTaxDetail detailRequest = new CCAviBoomiAPI.FleetPrideIseriesTaxDetail();
                             detailRequest.LineID = ci.Id;
                             detailRequest.LineType = 'I';
                             //detailRequest.Pool = ci.ccrz__Product__r.Pool_Number__c;
                             //detailRequest.PartNumber = ci.ccrz__Product__r.Part_Number__c;
                             detailRequest.QtyOrdered = String.valueOf(ci.ccrz__Quantity__c);
                             detailRequest.QtyShipped = String.valueOf(ci.CC_FP_QTY_Shipping__c);
                             detailRequest.SellingPrice = String.valueOf(ci.ccrz__Price__c);
                             detailRequests.add(detailRequest);
        
        request.TaxHeaderRequest = headerRequest;
        request.TaxDetailRequestList = detailRequests;
        lstRequests.add(request);

       Test.startTest();
        response = CCAviBoomiAPI.getFleetPrideIseriesTaxRate('store1', lstRequests);
        Test.stopTest();
      

    }
    
    static testmethod void CreateCustomerBoomiTest(){
        CCAviBoomiSettings__c settings = new CCAviBoomiSettings__c();
        settings.Name = 'parts';
        settings.End_Point__c = 'https://fleetpride.boomi.com';
        settings.Username__c = 'un1';
        settings.Password__c = 'pwd1';  
        insert settings;
        
      Test.setMock(HttpCalloutMock.class, new BoomiServiceMock(200, 'OK', '<?xml version=1.0 encoding=UTF-8?><S:Envelopexmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><wss:createCustomerResponsexmlns:wss="http://www.boomi.com/connector/wss"><wss:CustomerResponseList><AccountStatus><CustomerNumber>0</CustomerNumber><CustomerBranch>0</CustomerBranch><Success>N</Success><ReturnCode>402</ReturnCode><CodeDesc>Error during Customer Create.</CodeDesc>'+
				'</AccountStatus></wss:CustomerResponseList></wss:createCustomerResponse></S:Body></S:Envelope>'));
        
        List<CCAviBoomiAPI.CreateCustomerRequest> lstReq = new List<CCAviBoomiAPI.CreateCustomerRequest>();
        
        CCAviBoomiAPI.CreateCustomerRequest req = new CCAviBoomiAPI.CreateCustomerRequest();
        req.Company = '1';
        req.Location = 'DT';
        req.CustomerName = 'Sanjay55';
        req.ContactName = 'Sanjay56';
        req.BillToAddressLine1 = '198 W ROCK ISLAND RD';
        req.BillToAddressLine2 = '';
        req.BillToCity = 'GRAND PRAIRIE';
        req.BillToState = 'TX';
        req.BilltoZip = '75050';
        req.BillToCounty = 'DALLAS';
        req.ShippingAttention = 'test44';
        req.ShipToAddressLine1 = '2802 E MAIN ST';
        req.ShipToAddressLine2 = '';
        req.ShipToCity = 'GRAND PRAIRIE';
        req.ShipToState = 'TX';
        req.ShipToZip = '75051';
        req.ShipToCounty = 'DALLAS';
        req.ShipToCountry = 'US';
        req.CreateDate = '2020-10-28';
        req.EmailAddress = 'test22@gmail.com';
        req.TelephoneNumber = '5634445566';
        
        lstReq.add(req);
        
        CCAviBoomiAPI.CreateCustomerResponseWrapper res;
        
        Test.startTest();
        res = CCAviBoomiAPI.CreateCustomerBoomi('parts',lstReq);
        System.assert(res != null);
        Test.stopTest();

    }
    
    
}