@isTest
public class CCAviPageContentControllerTest {
  private static final String TEST_STOREFRONT = 'teststore';

    static testmethod void constructorTest() {
        setupTestData();

        ccrz.cc_CallContext.storefront = TEST_STOREFRONT;

        Test.setMock(HttpCalloutMock.class, new HttpCalloutTestMock());

        CCAviPageContentController res1 = null;
        CCAviPageContentController res2 = null;
        CCAviPageContentController res3 = null;
        CCAviPageContentController res4 = null;

        Test.startTest();
        ApexPages.currentPage().getParameters().put('contentKey' , 'pc001');
        res1 = new CCAviPageContentController();
        ApexPages.currentPage().getParameters().put('contentKey' , 'pc002');
        res2 = new CCAviPageContentController();
        ApexPages.currentPage().getParameters().put('contentKey' , 'pc003');
        res3 = new CCAviPageContentController();
        ApexPages.currentPage().getParameters().put('contentKey' , 'pc004');
        res4 = new CCAviPageContentController();
        Test.stopTest();

        System.assert(res1 != null);
        System.assertEquals('http://www.text.com/mycontent1.html', res1.contentURL);
        System.assert(res2!= null);
        System.assertEquals('http://c86.318.mwp.accessdomain.com/wp-json/wp/v2/pages/578', res2.contentURL);
        System.assert(res3 != null);
        System.assertEquals('http://www.text.com/mycontent3.html', res3.contentURL);
        System.assert(res4 != null);
        System.assertEquals(null, res4.contentURL);

    }

    private static void setupTestData() {
        List<CC_Avi_Page_Content__c> pageContents = new List<CC_Avi_Page_Content__c> {
            new CC_Avi_Page_Content__c(
                ContentKey__c    = 'pc001',
                ContentURL__c = 'http://www.text.com/mycontent1.html',
                ContentType__c  = 'WP-JSON',
                Storefront__c  = TEST_STOREFRONT,
                CSS_Urls__c = 'http:/www.css.com/mycss1.theme'
            ),
            new CC_Avi_Page_Content__c(
                ContentKey__c    = 'pc002',
                ContentURL__c = 'http://c86.318.mwp.accessdomain.com/wp-json/wp/v2/pages/578',
                ContentType__c  = 'WP-JSON',
                Storefront__c  = TEST_STOREFRONT,
                ContentBody__c = '',
                CSS_Urls__c = 'http:/www.css.com/mycss2.theme'
            ),
            new CC_Avi_Page_Content__c(
                ContentKey__c    = 'pc003',
                ContentURL__c = 'http://www.text.com/mycontent3.html',
                ContentType__c  = 'FRAGMENT',
                Storefront__c  = TEST_STOREFRONT,
                CSS_Urls__c = 'http:/www.css.com/mycss3.theme'
            )
        };

        insert pageContents;
    }

    public class HttpCalloutTestMock implements HttpCalloutMock {

        public HTTPResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"id": 578,"date": "2017-01-05T14:21:15","date_gmt": "2017-01-05T14:21:15","modified": "2017-01-12T21:47:45","modified_gmt": "2017-01-12T21:47:45","slug": "about","status": "publish","type": "page","title": {"rendered": "About"},"content": {"rendered":"Helping Deliver Your Competitive Edge","protected":"false"}}');
            response.setStatusCode(200);
            response.setStatus('OK');
            return response;
        }
    }
}