@istest
public class DeleteBulkRecordsTest {

    static testmethod void test1()
    {
      // The query used by the batch job.
        DeleteBulkRecords__c customSetting = new DeleteBulkRecords__c();
        customsetting.ObjectName__c ='Account';
        customsetting.Limit__c = 'LIMIT 1';
        customsetting.name  = 'Test';
        insert customsetting;
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        
        User TestUser = new User(
        FirstName = 'Samanthca12',
        LastName = 'Green1c22',
        CompanyName = 'IT Test Company',
        MobilePhone = '123-456-7890', 
        Username = 'demo1234' + '@ancdomdemodomain.com.fleetpride',
        Email = 'standarcduser@testorg.com',
        Alias = 'testcc',
        CommunityNickname = 'testing123',
        TimeZoneSidKey = 'America/New_York',
        LocaleSidKey = 'en_US',
        EmailEncodingKey = 'UTF-8',
        LanguageLocaleKey = 'en_US',
        number_of_accounts__c = 0,
        Street = '123 Test St',
        City = 'Testcity',
        State = 'va',
        PostalCode = '23223',
        Country = 'Japan',
        ProfileId = p.id,
        Create_Accounts__c = true,
         isactive = true);
        
        insert TestUser;       
        
        system.runAs(TestUser){
        List<Account> al = new List<Account>();
        
        Account acc = new Account(Name = 'Testing Account', Type = 'Customer',OwnerID = TestUser.id);
        al.add(acc);
        insert al;
        } 
       Database.executeBatch(new DeleteBulkRecords()); 
            
    }

}