/**************************
@Author: Alvin Claridad
@Date: 08/12/2015

Trigger for transferring the value of the losing account in the Merge Process

**************************/

public class AccountMergeTriggerHandler{

    public static void AccountMergeRequestTriggerHandler(Account_Merge_Request__c[] TriggerNew,Map<Id, Account_Merge_Request__c> TriggerOldMap){
         List<Account_Merge_Request__c> req = new List<Account_Merge_Request__c>();
         Id mergedRecordType = [SELECT Id from RecordType WHERE DeveloperName = 'Merged' LIMIT 1].Id;
         
         for(Account_Merge_Request__c amr: TriggerNew){
          System.Debug('>>>> before' + amr);
             if(amr.Merge_Checkbox__c){
                 amr.Lost_Account__c = amr.Sub_AccountName__c;
                 amr.Lost_Account_Number__c = amr.Sub_Account_Number__c;
                 amr.Lost_Account_Owner__c = amr.Sub_Account_Owner__c;
                 amr.Status__c = 'Done';
                 amr.RecordTypeId = mergedRecordType; 
                 //req.add(amr);
                 System.Debug('>>>>' + amr);
             }
             
         }
         
         if(!req.isEmpty()){
            // update req;
             System.Debug('>>>'+ req);
         }
         
    }

}