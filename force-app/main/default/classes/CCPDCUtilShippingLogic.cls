public with sharing class CCPDCUtilShippingLogic {
  static final Decimal FREE_PARCEL_DEFAULT_SHIPPING_VALUE = 400;
  static final Decimal FREE_LTL_DEFAULT_SHIPPING_VALUE = 1800;
  static final Decimal FREE_PARCEL_WEIGHT_LIMIT= 100;

  public static Map<String,Decimal> currentAccountFreeFeightValues(String accountID){
    Map<String, Decimal> returnData = new Map<String,Decimal>();

    List<Account> accounts = CCPDCAccountDAO.getAccountFreeShipping(accountID);
    if(accounts != null && !accounts.isEmpty() && accounts[0].LTL_Freight_Threshold__c != null){
      returnData.put('LTLFreightThresholdValue',accounts[0].LTL_Freight_Threshold__c);
    } else {
      returnData.put('LTLFreightThresholdValue',FREE_LTL_DEFAULT_SHIPPING_VALUE);
    }

    if(accounts != null && !accounts.isEmpty() && accounts[0].Parcel_Freight_Threshold__c != null){
      returnData.put('ParcelFreightThresholdValue',accounts[0].Parcel_Freight_Threshold__c);
    } else {
      returnData.put('ParcelFreightThresholdValue',FREE_PARCEL_DEFAULT_SHIPPING_VALUE);
    }

    if(accounts != null && !accounts.isEmpty() && accounts[0].Parcel_Weight_Limit__c != null){
      returnData.put('ParcelWeightLimit',accounts[0].Parcel_Weight_Limit__c);
    } else {
      returnData.put('ParcelWeightLimit',FREE_PARCEL_WEIGHT_LIMIT);
    }

    return returnData;
  }

  public static Decimal currentQualifyFeightValue(String cartID){
    if(cartID != null){
      ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(cartID);
      Decimal totalEligibleValue = 0;
      List<ccrz__E_Product__c> productsInCart = new List<ccrz__E_Product__c>();
      Set<Id> productsQualify = new Set<Id>();
      List<ID> productIds = new List<ID>();
      List<CCRZ__E_CartItem__c> qualifiedItems = new List<CCRZ__E_CartItem__c>();
      for(CCRZ__E_CartItem__c item : cart.ccrz__E_CartItems__r){
         productIds.add(item.ccrz__Product__c);
      }
      productsInCart = CCAviProductDao.getProducts(productIds);
      for(ccrz__E_Product__c checkproduct : productsInCart){
        if(checkproduct.Freight_Attribute__c != 'X' && checkproduct.Freight_Attribute__c != 'x'){
          productsQualify.add(checkproduct.Id);
        }
      }
      for(CCRZ__E_CartItem__c item : cart.ccrz__E_CartItems__r){
        if(productsQualify.contains(item.ccrz__Product__c)){
          qualifiedItems.add(item);
        }
      }
      for(CCRZ__E_CartItem__c item : qualifiedItems){
        if(item.CC_FP_Item_Order_Subtotal__c == null){
          totalEligibleValue += (item.ccrz__ItemTotal__c);   
        } else{
          totalEligibleValue += (item.CC_FP_Item_Order_Subtotal__c);
        }
      }
      return totalEligibleValue;
    } else{
      return 0.00;
    }
  }

  public static Decimal currentParcelShippingWeight(String cartID){
    Decimal totalParcelWeight = 0.00;
    if(cartID != null){
      ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(cartID);
      List<ccrz__E_Product__c> productsInCart = new List<ccrz__E_Product__c>();
      Set<Id> productsQualify = new Set<Id>();
      List<ID> productIds = new List<ID>();
      List<ID> productQualifyIds = new List<ID>();
      List<CCRZ__E_CartItem__c> qualifiedItems = new List<CCRZ__E_CartItem__c>();
      for(CCRZ__E_CartItem__c item : cart.ccrz__E_CartItems__r){
         productIds.add(item.ccrz__Product__c);
      }
      productsInCart = CCAviProductDao.getProducts(productIds);
      for(ccrz__E_Product__c checkproduct : productsInCart){
        if(checkproduct.Freight_Attribute__c != 'X' && checkproduct.Freight_Attribute__c != 'x'){
          productsQualify.add(checkproduct.Id);
        }
      }
      productQualifyIds.addAll(productsQualify);
      Map<String, Decimal> prodMap = CCAviProductDao.getProductsWeight(productQualifyIds);

      for(CCRZ__E_CartItem__c item : cart.ccrz__E_CartItems__r){
        Decimal weight = prodMap.get(String.valueOf(item.ccrz__Product__c));
        if(weight != null){
          totalParcelWeight += weight * item.CC_FP_QTY_Shipping__c;
        }
      }

    }
    return totalParcelWeight;
  }



  /* The section comment out below is a logic to find back order base on order ID */
  // public static List<ccrz__E_OrderItem__c> getYFreightAttributeOrderItems(String orderID){
  //   /** Getting order items from an order ID, find products in these order items, if product.Freight_Attribute__c  **/
  //   List<ID> orderIds = new List<ID>();
  //   List<ID> orderItemIds = new List<ID>();
  //   List<ID> productIds = new List<ID>();
  //   List<String> encryptedOrderIds = new List<String>();
  //   encryptedOrderIds.add(orderID);
  //   orderIds.add(Id.valueOf(orderID));
  //   Map<ID, ccrz__E_Order__c> newOrderMap = CCAviOrderDAO.getOrdersMap(orderIds);
  //   Id myId = Id.valueOf(orderID);
  //   ccrz__E_Order__c backOrder = newOrderMap.get(myId);
  //   encryptedOrderIds.add(backOrder.ccrz__EncryptedId__c);
  //   List<ccrz__E_OrderItem__c> newOrderItemMap = CCAviOrderDAO.getOrderItems(encryptedOrderIds);
  //
  //   for(ccrz__E_OrderItem__c item : newOrderItemMap){
  //     productIds.add(item.ccrz__Product__c);
  //   }
  //
  //   List<ccrz__E_Product__c> products = CCAviProductDao.getProducts(productIds);
  //   List<ccrz__E_Product__c> yAttProduct = new List<ccrz__E_Product__c>();
  //   for(ccrz__E_Product__c product : products){
  //     if(product.Freight_Attribute__c == 'Y' || product.Freight_Attribute__c == 'y'){
  //       yAttProduct.add(product);
  //       System.debug(product.ID);
  //     }
  //   }
  // 
  //
  //  return newOrderItemMap;
  // }




}