/**
 * @description     Number of Accounts Batch Handler
 * @author          Lawrence Catan
 * @Company         CloudSherpas
 * @date            29.FEB.2016
 *
 * HISTORY
 * - 29.FEB.2016    Lawrence Catan      Created.
*/ 

global class NumberofAccountsBatchHandler implements Schedulable{
    NumberofAccountsBatch batch = new NumberofAccountsBatch();
    
    global void execute(SchedulableContext sc) {
    Database.executeBatch(batch);
    }
}