public with sharing class CCAviUtil {
	public CCAviUtil() {
		
	}

    public static CC_Avi_Settings__mdt getCCAviSettingsMetaData(String storefront){
        List<String> storefrontNames = new List<String>{
            storefront,
            storefront.toUpperCase(),
            storefront.toLowerCase()
        };
         return [
            SELECT
                Id
                , DeveloperName
                , Storefront__c
                , Branch_Request_Record_Types__c
            FROM
                CC_Avi_Settings__mdt
            WHERE
                Storefront__c in :storefrontNames
            LIMIT
                1
        ];
    }

    public static Id getRecordTypeIdByNameAndSObjectType(String recordTypeName, String mySobject)
    {
    	return [
    		SELECT 
    			ID,
    			Name,
    			DeveloperName
    		FROM
    			RecordType
    		WHERE
    			SObjectType = :mySobject
    		AND
    			IsActive = TRUE
    		AND
    			DeveloperName = :recordTypeName
    		LIMIT
    			1
    	].Id ;
    }
}