@isTest
public class CCPDCTopProductsControllerTest {
   static CCPDCTestUtil util = new CCPDCTestUtil();
 @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
            ccrz__E_Product__c product = util.getProduct();
        	product.ccrz__ProductStatus__c = 'Released';
        	product.ccrz__Sequence__c =1;
            update product;
        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

    }
    
    static testmethod void checkTopProductsController(){
        
        CCPDCTopProductsController cpc = new CCPDCTopProductsController();
        List<CCPDCTopProductsController.wrapperProd> tpcwrap=  cpc.wraprodlst;
        String comma = cpc.COMMA;
        string endChar= cpc.CRLF;
        system.assertEquals(1, tpcwrap.size());
        
    }
}