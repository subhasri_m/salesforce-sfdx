global with sharing class CCPDCCheckoutPaymentController {

    @RemoteAction
    global static ccrz.cc_RemoteActionResult updateCart(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse fulFilResponse;
        ccrz__E_Cart__c cart;

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            String po = (String) input.get('poNumber');

            String cartId = ccrz.cc_CallContext.currCartId;
            cart = CCPDCCartDAO.getCartExtra(cartId);
            cart.ccrz__PONumber__c = po;
            String storefront = ccrz.cc_CallContext.storefront;
            String sfid = (String) input.get('sfid');

            // Call the Inventory service. Reserve the inventory
            Boolean reserveInventoryFlag = true; // production value is 'true'
            Boolean recheckAvailabilityFlag = true;
            fulFilResponse = CCPDCInventoryHelper.checkInventory(
                cart,
                cart.ccrz__E_CartItems__r,
                reserveInventoryFlag,
                recheckAvailabilityFlag,
                false
            );

            response.success = (!cart.CC_FP_Has_Validation_Message__c);

            // This block merges data in the cart items queried from the database with
            // data passed into the remote action method
            List<ccrz__E_CartItem__c> items = new List<ccrz__E_CartItem__c>();
            List<Object> cartItems = (List<Object>) input.get('cartItems');
            Map<String,string> itemPOmap = new Map<String,string>();
            for (Object o : cartItems) {
                Map<String, Object> m = (Map<String, Object>) o;
                String itemId = (String) m.get('itemID');
                String poNum = (String) m.get('CCFPPONumber');
                itemPOmap.put(itemId,poNum);
                //ccrz__E_CartItem__c item = new ccrz__E_CartItem__c(Id = itemId, CC_FP_PONumber__c = poNum);
                //items.add(item);
            }
            
            for (ccrz__E_CartItem__c item : [Select Id, CC_FP_PONumber__c from ccrz__E_CartItem__c where id in : itemPOmap.keySet()])
            {
                if (item.CC_FP_PONumber__c !=null){
                  items.add(item);
                }
                else{
                    item.CC_FP_PONumber__c=itemPOmap.get(item.Id);
                    items.add(item);
                }
            }
            update items;

            ccrz__E_Cart__c currentCart = CCPDCCartDAO.getCartExtra(cartId);
            if (fulFilResponse.result == 'Success') {
                if ((fulFilResponse.shipments == null || fulFilResponse.shipments.isEmpty()) &&  (fulFilResponse.backOrderLines == null || fulFilResponse.backOrderLines.isEmpty()) ) {
                    ccrz__E_Cart__c splitCart = CCPDCCartDAO.getCartSplitResponse(cartId);
                    if (splitCart.CC_FP_Split_Response__c != null) {
                        CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse splitResponse = (CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse) JSON.deserialize(splitCart.CC_FP_Split_Response__c, CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse.class);
                        CCPDCFulfillmentHelper.doSplits(currentCart, splitResponse);
                    }
                }
                // Recheck did not occur, but there are order lines or backorder lines
                else {
                    CCPDCFulfillmentHelper.doSplits(currentCart, fulFilResponse);
                }
            }
            else {
                ccrz__E_Cart__c splitCart = CCPDCCartDAO.getCartSplitResponse(cartId);
                if (splitCart.CC_FP_Split_Response__c != null) {
                    CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse splitResponse = (CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse) JSON.deserialize(splitCart.CC_FP_Split_Response__c, CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse.class);
                    CCPDCFulfillmentHelper.doSplits(currentCart, splitResponse);
                }
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
            cart.CC_FP_Validation_Message__c = e.getMessage() + ' => ' + e.getStackTraceString();
            CCPDCExceptionService.sendExceptionEmail('CCPDCCheckoutPaymentController', 
                    'updateCart', String.valueOf(e.getMessage()) + ' => ' + String.valueOf(e.getStackTraceString()));
        }
        finally {
            if (cart != null) {
                update cart;
                CCPDCFulfillmentHelper.updateTax(cart.ccrz__EncryptedId__c);
            }

            ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','getPriceFromBOOMI');
            ccrz.ccLog.close(response);
        }
        return response;
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult updateCartFP(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
       // CCPDCOMSFulfillmentPlanAPI.FulfillmentPlanResponse fulFilResponse;
        ccrz__E_Cart__c cart;

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);
            String po = (String) input.get('poNumber');
            String orderComments = (String) input.get('orderComments');

            String cartId = ccrz.cc_CallContext.currCartId;
            cart = CCPDCCartDAO.getCartExtra(cartId);
            cart.ccrz__PONumber__c = po;
            cart.Order_Comments__c = orderComments;
            String storefront = ccrz.cc_CallContext.storefront;
            String sfid = (String) input.get('sfid');
            // This block merges data in the cart items queried from the database with
            // data passed into the remote action method
            List<ccrz__E_CartItem__c> items = new List<ccrz__E_CartItem__c>();
            List<Object> cartItems = (List<Object>) input.get('cartItems');
            Map<String,string> itemPOmap = new Map<String,string>();
            for (Object o : cartItems) {
                Map<String, Object> m = (Map<String, Object>) o;
                String itemId = (String) m.get('itemID');
                String poNum = (String) m.get('CCFPPONumber');
                itemPOmap.put(itemId,poNum);
            }
            
            for (ccrz__E_CartItem__c item : [Select Id, CC_FP_PONumber__c from ccrz__E_CartItem__c where id in : itemPOmap.keySet()])
            {
                if (item.CC_FP_PONumber__c !=null){
                  items.add(item);
                }
                else{
                    item.CC_FP_PONumber__c=itemPOmap.get(item.Id);
                    items.add(item);
                }
            }
            update items;

            ccrz__E_Cart__c currentCart = CCPDCCartDAO.getCartExtra(cartId);
            //CCPDCFulfillmentHelper.doSplitsFP(currentCart); splitting is moved to ccpdccheckoutoverrideController
            response.success = (!cart.CC_FP_Has_Validation_Message__c);
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        finally {
            if (cart != null) {
                update cart;
              //  CCPDCFulfillmentHelper.updateTax(cart.ccrz__EncryptedId__c);
            }

            ccrz.ccLog.close(response);
        }
        return response;
    }

}