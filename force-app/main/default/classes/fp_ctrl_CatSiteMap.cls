public with sharing class fp_ctrl_CatSiteMap {
    public String declarationXml { get { return '<?xml version="1.0" encoding="UTF-8"?>\n'; } }
    public String categoryId {get;set;}
    public List<fp_bean_SiteMap> prodData {get;set;}
    public fp_bean_SiteMap catData {get; set;}
    public Boolean useFurls {get; set;}
    public Boolean useNewProductListPage {get; set;}
    public final String HTTPS = 'https://';
    public String locale {get;set;}

    public String baseURL {
        get {
            if (null == baseURL) {
                // Ensure the URL starts with https and does not have a trailing slash
                baseURL = ((String) ccrz.cc_CallContext.storeFrontSettings.get('Site_Secure_Domain__c')).removeEnd('/');
                baseURL = !baseURL.startsWithIgnoreCase(HTTPS) ? HTTPS + baseURL : baseURL;
            }
            return baseURL;
        }
        private set;
    }

    public fp_ctrl_CatSiteMap(){
        categoryId = ccrz.cc_CallContext.currPageParameters.get('categoryId');      
        useNewProductListPage = ccrz.cc_CallContext.isConfigTrue('pl.usenew', null);
        // Grab The Locale --> For Anonymous Users This Will Be Pulled From The cclcl URL Param --> Falling Back To The Browsers Locale
        locale = ccrz.cc_CallContext.userLocale;
        try{
            this.prodData = (List<fp_bean_SiteMap>) retrieveProductsByCategory(categoryId); //Get our entitled products for the sitemap based on this category
            useFurls = (Boolean) ccrz.cc_CallContext.isConfigTrue('ui.usefurls', null);
            this.catData = (fp_bean_SiteMap)retrieveCatData(categoryId);
        }catch(Exception e){
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
        }
    }

    @TestVisible private static fp_bean_SiteMap retrieveCatData(final String categoryID) {
        List<ccrz__E_Category__c> categoryObj =  [SELECT Id, LastModifiedDate FROM ccrz__E_Category__c WHERE Id =: categoryId LIMIT 1];
        fp_bean_SiteMap categoryData;
        if(!categoryObj.isEmpty()) {
            categoryData = new fp_bean_SiteMap(categoryId);
            categoryData.lastModDate = categoryObj.get(0).LastModifiedDate.format('yyyy-MM-dd');
            if (ccrz.cc_CallContext.isConfigTrue('ui.usefurls', null) || Test.isRunningTest()) {
                Map<String, Object> categoryUrlMap = (Map<String, Object>) ccrz.ccAPICategory.getUrlFor(new Map<String, Object>{
                    ccrz.ccAPI.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
                    ccrz.ccAPICategory.CATEGORYIDLIST => new List<String>{
                        categoryID
                    }
                }).get(ccrz.ccAPICategory.CATEGORYURLMAP);
                if (ccrz.ccUtil.isKeyValued(categoryUrlMap, categoryId)) {
                    categoryData.friendlyUrl = Site.getPathPrefix() + (String) categoryUrlMap.get(categoryId);
                }
            }
        }
        return categoryData;
    }

     @TestVisible private static List<fp_bean_SiteMap> retrieveProductsByCategory(final String categoryID){
        List<fp_bean_SiteMap> dataSet = new List<fp_bean_SiteMap>();
        //Get the Storefront Name for filtering
        String storefrontName = ccrz.cc_CallContext.storefront;
        System.debug(System.LoggingLevel.info, 'StorefrontName is: '+storefrontName);
        Id accountGroupId = null;
        try {
            ccrz__E_AccountGroup__c accountGroup = ccrz.cc_CallContext.currAccountGroup;
            accountGroupId = accountGroup.id;
        } catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            System.debug(System.LoggingLevel.ERROR, e.getStackTraceString());
            accountGroupId = null;
        }
        List<String> statuses = new List<String> { 'Released', 'Not Orderable' };
        String invalidType = 'Coupon';        
        // Get all the listed products in this category. Limited to 10000 products per category.
        Map<String, String> productsInCategory = new Map<String, String>();
        List<Map<String, Object>> productList = new List<Map<String, Object>>();
        Map<String, Object> productUrlMap = new Map<String, Object>();
         
       //==Changes made on 31/07/2020 By Sanjay to make query dynamic to run includes dynamically
       String queryStr = 'SELECT p.ccrz__Product__r.Id, p.ccrz__Product__r.ccrz__SKU__c, p.ccrz__Product__r.ccrz__SEOId__c ' + 
            ' FROM ccrz__E_ProductCategory__c p ' +
            ' WHERE p.ccrz__Category__c = :categoryId ' + 
            ' AND p.ccrz__Product__r.ccrz__Storefront__c INCLUDES ('+ '\''+storefrontName+'\')' + 
            ' AND p.ccrz__Product__r.ccrz__ProductStatus__c IN :statuses ' + 
            ' AND p.ccrz__Product__r.ccrz__ProductType__c != :invalidType ' + 
            ' AND p.ccrz__Product__r.ccrz__StartDate__c <= TODAY ' + 
            ' AND p.ccrz__Product__r.ccrz__EndDate__c >= TODAY LIMIT 10000';
         System.debug('queryStr==>'+queryStr);
        for(ccrz__E_ProductCategory__c pli: (List<ccrz__E_ProductCategory__c>)Database.query(queryStr))
        {
            //add each match to the return value set by the sku for quick order
            productList.add(new Map<String, Object>{
                'sfid' => pli.ccrz__Product__r.Id,
                'SEOId' => pli.ccrz__Product__r.ccrz__SEOId__c,
                'SKU' => pli.ccrz__Product__r.ccrz__SKU__c
            });
            productsInCategory.put(pli.ccrz__Product__r.Id, pli.ccrz__Product__r.ccrz__SKU__c);
        }
        if((ccrz.cc_CallContext.isConfigTrue('ui.usefurls', null) && ccrz.ccUtil.isNotEmpty(productList))) {
            productUrlMap = (Map<String, Object>)ccrz.ccAPIProduct.getUrlFor(new Map<String, Object>{
                ccrz.ccAPI.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
                ccrz.ccAPIProduct.PRODUCTLIST => productList
            }).get(ccrz.ccAPIProduct.PRODUCTURLMAP);
        }
        //determine the correct pricelists to use (it matches the storefront name and account group)
        Map<Id,ccrz__E_PriceList__c> allowedPricelists = new Map<Id,ccrz__E_PriceList__c>([
            SELECT Id, Name
            FROM ccrz__E_PriceList__c
            WHERE
            Id IN (SELECT ccrz__PriceList__c FROM ccrz__E_AccountGroupPriceList__c WHERE ccrz__AccountGroup__c = :accountGroupId) AND
            ccrz__Storefront__c INCLUDES (:storefrontName) AND
            ccrz__StartDate__c <=  TODAY AND
            ccrz__EndDate__c >=  TODAY
        ]);
        if ((allowedPricelists.size() * productsInCategory.size()) > 35000 || Test.isRunningTest()) {
            // we might have too many pricelistitems so we'll have to do this the slow way.
            Map<String, fp_bean_SiteMap> productsInDataset = new Map<String, fp_bean_SiteMap>();
            Integer loopCycles = 0;
            for(ccrz__E_Pricelist__c pl: allowedPricelists.values()) {
                // sanity check, since we're doing a query in a data-driven loop
                if(loopCycles >= 30) {// try to stay under the execution limit and query limit
                    break;
                }
                for(ccrz__E_PriceListItem__c pli:[
                    SELECT Id, ccrz__Product__r.ccrz__SKU__c, ccrz__Product__r.Id, ccrz__Product__r.LastModifiedDate
                    FROM ccrz__E_PriceListItem__c
                    WHERE ccrz__PriceList__r.Id = :pl.Id AND
                    ccrz__Product__r.ccrz__SKU__c NOT IN :productsInDataset.keySet() AND ccrz__Product__c IN :productsInCategory.keySet()
                    LIMIT 40000
                ]){
                    //add each match to the return value set by the sku for quick order
                    if(productsInDataset.containsKey(pli.ccrz__Product__r.ccrz__SKU__c)) {
                        continue;
                    } else {
                        productsInDataset.put(pli.ccrz__Product__r.ccrz__SKU__c, new fp_bean_SiteMap(pli.ccrz__Product__r.ID, pli.ccrz__Product__r.ccrz__SKU__c, pli.ccrz__Product__r.LastModifiedDate));
                    }
                }
                loopCycles++;
            }
            dataSet.addAll(productsInDataset.values());
        } else {
            // we'll definitely have less than 35000, so we can do this the quick way.
            List<ccrz__E_PriceListItem__c> resultList = [
                SELECT ccrz__Product__r.ID, ccrz__Product__r.ccrz__SKU__c, ccrz__Product__r.LastModifiedDate
                FROM ccrz__E_PriceListItem__c
                WHERE
                ccrz__Pricelist__c IN :allowedPricelists.values() AND
                ccrz__Product__r.ccrz__SKU__c IN :productsInCategory.values() AND
                (ccrz__Product__r.ccrz__ProductStatus__c = 'Released' OR ccrz__Product__r.ccrz__ProductStatus__c = 'Not Orderable' )AND
                ccrz__Product__r.ccrz__ProductType__c != 'Coupon' AND
                ccrz__StartDate__c <=  TODAY AND
                ccrz__EndDate__c >=  TODAY
                LIMIT :40000
            ];

            Map<String, String> productsInDataset = new Map<String, String>();
            for (ccrz__E_PriceListItem__c pi: resultList) {
                if(productsInDataset.containsKey(pi.ccrz__Product__r.ID)){
                    continue;
                } else {
                    productsInDataset.put(pi.ccrz__Product__r.ID, pi.ccrz__Product__r.ID);
                    fp_bean_SiteMap prod = new fp_bean_SiteMap(pi.ccrz__Product__r.ID, pi.ccrz__Product__r.ccrz__SKU__c, pi.ccrz__Product__r.LastModifiedDate);
                    if(productUrlMap.containsKey(pi.ccrz__Product__r.Id)){
                        prod.friendlyUrl = Site.getPathPrefix() + (String)productUrlMap.get(pi.ccrz__Product__r.Id);
                    }
                    dataSet.add(prod);
                }
            }
        }
        return dataSet;
    }

    //Gets the price lists that are allowed for this account group
    @TestVisible private static Map<Id, ccrz__E_PriceList__c> getAllowedPricelists(String storefrontName, Id accountGroupId){
        Map<Id,ccrz__E_PriceList__c> allowedPricelists = new Map<Id,ccrz__E_PriceList__c>([
            SELECT Id, Name
            FROM ccrz__E_PriceList__c
            WHERE
                Id IN (SELECT ccrz__PriceList__c FROM ccrz__E_AccountGroupPriceList__c WHERE ccrz__AccountGroup__c = :accountGroupId) AND
                ccrz__Storefront__c INCLUDES (:storefrontName) AND
                ccrz__StartDate__c <=  TODAY AND
                ccrz__EndDate__c >=  TODAY
        ]);
        return allowedPricelists;
    }
}