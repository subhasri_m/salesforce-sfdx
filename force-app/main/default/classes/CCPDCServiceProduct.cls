global with sharing class CCPDCServiceProduct extends ccrz.ccServiceProduct {
     global override Map<String, Object> getFieldsMap(Map<String, Object> inputData) {
        inputData = super.getFieldsMap(inputData);
        String objectFields = (String)inputData.get(ccrz.ccService.OBJECTFIELDS);
        objectFields += ',FP_Brand_Code__c' 
            + ',FP_Brand_Name__c' 
            + ',CORE_Part_Number__c' 
            + ',Core_Pool_Number__c'
            + ',Freight_Attribute__c'
            + ',Manufacturer_Part__c'
            + ',Part_Number__c'
            + ',Pool_Number__c'
            + ',DSP_Part_Number__c'
            + ',Salespack__c'
            + ',FP_Salespack__c'
            + ',CC_FP_Pricing_Failed__c'
            + ',Universal_fit__c';
		return new Map<String, Object> {ccrz.ccService.OBJECTFIELDS => objectFields};
            
            }
    global virtual override Map<String, Object> getSubQueryMap(Map<String, Object> inputData){
        Map<String, Object> retData = super.getSubQueryMap(inputData);
        retData.put('Product_Inventory_Items__r', '(SELECT Id, Name, AvailabilityMessage__c, AvailabilityMessageRT__c, QtyAvailable__c FROM Product_Inventory_Items__r LIMIT 1)');
       return retData;
    } 
	

    global override Map<String, Object> fetch(Map<String, Object> inputData) {
        Map<String, Object> theMap = super.fetch(inputData);
        system.debug('theMap==>'+theMap);
        List<Map<String, Object>> inputProductList   = (List<Map<String, Object>>) theMap.get('productList');
        if(ccrz.cc_CallContext.storefront == Label.FP_StoreName) { 
            for(Map<String, Object> product: inputProductList) {
                //For FP Storefront, the salespack should be FPSalesPack field
                product.put('salespack', product.get('FPSalespack'));
            }
            theMap.put('productList', inputProductList);
        }
        if(inputProductList.size() > 1) 
        {
            return theMap;
        }
        Object sku;
        for(Map<String, Object> product: inputProductList) {
            sku = product.get('SKU');            
        }
        List<Product_Cross_Reference__c> crossReferences =  CCPDCProductDAO.getCrossReferencesForProduct(String.valueOf(sku),ccrz.cc_CallContext.storefront);
        List<Product_Cross_Reference__c> ECrossReferences = CCPDCServiceProduct.formatCrossReferences(crossReferences); // transform cross-reference in key value pairs
        for(Map<String, Object> product: inputProductList) {
            product.put('ECrossReferencesS',ECrossReferences); 
        } 
        theMap.put('productList', inputProductList);
        return theMap;
    }   
    
    global override Map<String, Object> search(Map<String, Object> inputData) {

        String searchString = (String) inputData.get(ccrz.ccService.SEARCHSTRING);
        System.debug('inputData==>'+inputData);
        Map<String, Object> theMap = super.search(inputData);
        System.debug('theMap==>'+theMap);
        System.debug('ccrz.ccService.SEARCHRESULTS==>'+ccrz.ccService.SEARCHRESULTS);
        Set<String> searchResults = (Set<String>) theMap.get(ccrz.ccService.SEARCHRESULTS);
        Map<String, Object> searchMap = (Map<String, Object>) theMap.get(ccrz.ccAPIProduct.PRODUCTSEARCHMAP);
        System.debug('searchResults==>'+searchResults);
        if((searchResults != null && searchResults.size() > 2000) ){
            // dont extend results, truncate to 250 only
            Set<String> limitedSearchResults = new Set<String>();
            Integer count = 0;
            for (String productId : searchResults) {
                if (count < 2000) {  limitedSearchResults.add(productId);}
                else {  searchMap.remove(productId);}
                count++;
            }
            theMap.put(ccrz.ccService.SEARCHRESULTS, limitedSearchResults);
            
        } else if (searchResults != null ){
            // extend results, then truncate to 250 only
            theMap = extendSearch(searchString, theMap);
            Set<String> limitedSearchResults = new Set<String>();
            Integer count = 0;
            for (String productId : searchResults) {
                if (count < 2000) {            limitedSearchResults.add(productId);
                                  } else {            searchMap.remove(productId);
                                         }
                count++;
            }
            theMap.put(ccrz.ccService.SEARCHRESULTS, limitedSearchResults);        
        }
        return theMap;
    }
    
    @Testvisible
    private Map<String, Object> extendSearch(String searchString, Map<String, Object> outputData) {
        
        Account effAccount = ccrz.cc_CallContext.effAccount;
        if (effAccount != null) {
            String effAccountId = effAccount.Id;
            if (effAccount.ParentId != null) {                
                effAccountId = effAccount.ParentId;
            }
            Set<String> searchResults = (Set<String>) outputData.get(ccrz.ccService.SEARCHRESULTS);
            if(Test.isRunningTest()){
                ccrz__E_Product__c product = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
                searchResults.add(product.id);
            }
            Map<String, Object> searchMap = (Map<String, Object>) outputData.get(ccrz.ccAPIProduct.PRODUCTSEARCHMAP);
            List<Account_Product_Cross_Reference__c> items = CCPDCProductDAO.searchParts(searchString, effAccountId, ccrz.cc_CallContext.storefront);
            for (Account_Product_Cross_Reference__c item : items) {
                searchMap.put(item.Related_to_Product__c, item.Related_to_Product__r.ccrz__SKU__c);
                searchResults.add(item.Related_to_Product__c);
            }
            List<Product_Cross_Reference__c> moreItems = CCPDCProductDAO.searchParts(searchString, ccrz.cc_CallContext.storefront);
            for (Product_Cross_Reference__c item : moreItems) {
                searchMap.put(item.Related_to_Product__c, item.Related_to_Product__r.ccrz__SKU__c);
                searchResults.add(item.Related_to_Product__c);
            }
            /*supercession
			1. After the supersession OLD part has inventory -
			In this case, the Old Part status will be "released" and old part should be at the top of the search result and New part should be 2nd part on the list.
			2. After the supersession, Old Part has NO Inventory            
			In this case, Fleetpride team will change the Old part status as "Not Orderable". CC should show only the new part at the top of the search results
			*/
            system.debug('Search Results' + searchResults);
            List<ccrz__E_RelatedProduct__c> relProducts = CCPDCProductDAO.getRelatedSupercessionProductsFromId(searchResults);
           
            if(relProducts != null && relProducts.size() >0){                   
                for(ccrz__E_RelatedProduct__c rp: relProducts){
                    if(rp.ccrz__Product__r != null && rp.ccrz__Product__r.ccrz__ProductStatus__c != null){
                        if(rp.ccrz__Product__r.ccrz__ProductStatus__c == 'Released') {
                            if(rp != null && rp.ccrz__RelatedProduct__c != null && rp.ccrz__RelatedProduct__r.ccrz__SKU__c != null){
                                searchMap.put(rp.ccrz__RelatedProduct__c, rp.ccrz__RelatedProduct__r.ccrz__SKU__c);
                                searchResults.add(rp.ccrz__RelatedProduct__c);
                            }                            
                        } else if (rp.ccrz__Product__r.ccrz__ProductStatus__c == 'Not Orderable'){
                            if(rp != null && rp.ccrz__RelatedProduct__c != null && rp.ccrz__RelatedProduct__r.ccrz__SKU__c != null){
                                searchMap.put(rp.ccrz__RelatedProduct__c, rp.ccrz__RelatedProduct__r.ccrz__SKU__c);
                                searchResults.add(rp.ccrz__RelatedProduct__c);
                            }  
                            // remove Old part
                            if(searchMap.get(rp.ccrz__Product__c) != null){     searchMap.remove(rp.ccrz__Product__c);
                                                                          }                            
                            
                        }
                    }
                }
            }
        }
        return outputData;
    }
    
    @testvisible
    private Static List<Product_Cross_Reference__c> formatCrossReferences(List<Product_Cross_Reference__c> crossReferences ) {
        Map<String,String> crossReferenceMap = new Map<String, String>();               
        for(Product_Cross_Reference__c crossReference : crossReferences) {
            if(String.isBlank(crossReference.Brand_Name__c)){
                crossReference.Brand_Name__c = 'Other';
            }
            if(crossReferenceMap.ContainsKey(crossReference.Brand_Name__c)) {
                String interchangeData = crossReferenceMap.get(crossReference.Brand_Name__c);
                interchangeData = interchangeData + ', ' + crossReference.Interchange_Part_Number__c;
                crossReferenceMap.put(crossReference.Brand_Name__c,  interchangeData);
            } else {
                crossReferenceMap.put(crossReference.Brand_Name__c,  crossReference.Interchange_Part_Number__c);
            }
        }
        List<Product_Cross_Reference__c> ECrossReferences = new List<Product_Cross_Reference__c>();
        for(string  brand :  crossReferenceMap.keySet()) {
            Product_Cross_Reference__c cr = new Product_Cross_Reference__c();
            cr.Brand_Name__c = brand;
            cr.Interchange_Part_Number__c = crossReferenceMap.get(brand);
            ECrossReferences.add(cr);
        }
       return ECrossReferences;  
    }
    
}