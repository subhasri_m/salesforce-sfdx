@isTest
public class ScheduleSalesCallController_UnitTest {
    @testSetup
    public static void testSetup(){
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama11uu.com',
            Username = 'puser000@amamama1177.com' + System.currentTimeMillis(),
            CompanyName = 'TEST_SEDE',
            Title = 'title_test',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles', 
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            Create_Accounts__c = true,
            LocaleSidKey = 'en_US'
        );
        insert u;
        System.runAs(u){
            
            Account acc = new Account();
            acc.name = 'check';
            insert acc;
            
            Task t = new Task(
            	Priority ='High',
                Description='Test',
                Subject = 'Email',
                OwnerId = u.Id,
                Related_to__c = acc.id,
                status = 'New',
                ActivityDate = SYSTEM.today()
            );
            insert t;
        }
    }
    
    private static testMethod void unitTest_1(){
        ScheduleSalesCallController sch = new ScheduleSalesCallController();
        ScheduleSalesCallController.ActivityWrapperClass wrapper = new ScheduleSalesCallController.ActivityWrapperClass();
        sch.RECURRENCEDAYOFMONTH_Input = 10;
        sch.RecurrenceInterval_input = 10;
        sch.RECURRENCEINSTANCE_input = '10';
        sch.WeekdayMonthly_Input = 10;
        sch.IsMonSelected = true;
        sch.IsTueSelected = true;
        sch.IsWedSelected = true;
        sch.IsThuSelected = true;
        sch.IsFriSelected = true;
        sch.getItems();
        sch.getRecWeekDay();
        sch.getRecInstances();
        sch.getMonthDates();
        sch.getRecurrenceTypeOptions();
        sch.assetClicked();
        sch.getTaskObjective();
        sch.showWeekDayNow();
        sch.createNewTasks();
        sch.CalculateWeekMask();
        sch.assignAccCallPriority();
        sch.updateenddate();
        sch.redirectPopup(); //Added By Likhit on 12-Oct-2020.
    }
}