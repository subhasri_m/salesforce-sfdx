@isTest
public class CCPDCOrderDAOTest {

    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();

        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }

    static testmethod void getCartOwnerTest(){
        util.initCallContext();
        ccrz__E_Order__c order = util.getOrder();

        ccrz__E_Order__c returnedOrder = null;

        Test.startTest();
        returnedOrder = CCPDCOrderDAO.getOrderOwner(order.Id);
        Test.stopTest();

        System.assert(returnedOrder != null);
        System.assertEquals(order.Id, returnedOrder.Id);
    }

    static testmethod void getOrderDetailsTest(){
        util.initCallContext();
        ccrz__E_Order__c order = util.getOrder();

        ccrz__E_Order__c returnedOrder = null;

        Test.startTest();
        returnedOrder = CCPDCOrderDAO.getOrderDetails(order.Id);
        Test.stopTest();

        System.assert(returnedOrder != null);
        System.assertEquals(order.Id, returnedOrder.Id);
    }

    static testmethod void getOrderCustomTest(){
        util.initCallContext();
        ccrz__E_Order__c order = util.getOrder();

        ccrz__E_Order__c returnedOrder = null;

        Test.startTest();
        returnedOrder = CCPDCOrderDAO.getOrderCustom(order.Id);
        Test.stopTest();

        System.assert(returnedOrder != null);
        System.assertEquals(order.Id, returnedOrder.Id);
    }

}