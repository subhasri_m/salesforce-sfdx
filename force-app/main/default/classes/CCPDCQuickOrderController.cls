global with sharing class CCPDCQuickOrderController {
    public Boolean IsUserLoggedIn {get;set;}
    public string LoginMessage{get;set;}
    global static Map<String, Object> stateMap;
    
    public CCPDCQuickOrderController() // Added by Lalit Arora to Show error message when user is not logged in
    {
        IsUserLoggedIn =true;
        LoginMessage ='';
           String info  = UserInfo.getUsertype(); 
            if (info=='Guest')
            {  
              IsUserLoggedIn = false;
              LoginMessage = System.label.UploadErrorMessage;
            }
        
    } 
    @RemoteAction
    global static ccrz.cc_RemoteActionResult addBulk(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            String currcartId = ccrz.cc_CallContext.currCartId;
            List<CCPDCQuickOrderHelper.QuickOrderItem> input = (List<CCPDCQuickOrderHelper.QuickOrderItem>) JSON.deserialize(data, List<CCPDCQuickOrderHelper.QuickOrderItem>.class);

            List<CCPDCQuickOrderHelper.QuickOrderItem> items = CCPDCQuickOrderHelper.getQuickOrderItems(input);

            Map<String,Object> output = new Map<String,Object>();
            Map<Id,CCPDCQuickOrderHelper.QuickOrderItem> qiMap = new Map<Id,CCPDCQuickOrderHelper.QuickOrderItem>();
            output.put('items', items);

            if(!items.isEmpty()){
                for(CCPDCQuickOrderHelper.QuickOrderItem qi : items){
                    if(qi.record != null){
                        qiMap.put(qi.record.Id,qi);  
                    }

                }
                ccrz__E_Cart__c currentCart = CCPDCCartDAO.getCart(currcartId);
  				if(currentCart != null && currentCart.ccrz__E_CartItems__r != null) {
                    for(ccrz__E_CartItem__c ci : currentCart.ccrz__E_CartItems__r){
                        if(qiMap.containsKey(ci.ccrz__Product__c)){
                            CCPDCQuickOrderHelper.QuickOrderItem nqi = qiMap.get(ci.ccrz__Product__c);
                            nqi.boomiPrice = ci.ccrz__Price__c;
                        }
                    }
                }

                output.put('qiMap',qiMap);

            }

            if (CCPDCQuickOrderHelper.validate(items)) {
                String cartId = CCPDCQuickOrderHelper.addToCart(items);
                ccrz__E_Cart__c afterAddCart = CCPDCCartDAO.getCart(cartId);
                List<ccrz__E_CartItem__c> aftercartItems = afterAddCart.ccrz__E_CartItems__r;
                for(ccrz__E_CartItem__c ci:aftercartItems){
                    ci.ccrz__OriginalItemPrice__c = ci.ccrz__Price__c;
                }
                update aftercartItems;


                if (cartId != null) {
                    output.put('cartId', currcartId);
                    CCAviPageUtils.buildResponseData(response, true, output);
                }
                else {
                    CCAviPageUtils.buildResponseData(response, false, output);                
                }
            }
            else {
                CCAviPageUtils.buildResponseData(response, false, output);                
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }

    @RemoteAction
    global static Object addBulkCont(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        Continuation con = new Continuation(120);
        con.continuationMethod = 'addBulkContCallback';
        try {
            String currcartId = ccrz.cc_CallContext.currCartId;
            List<CCPDCQuickOrderHelper.QuickOrderItem> input = (List<CCPDCQuickOrderHelper.QuickOrderItem>) JSON.deserialize(data, List<CCPDCQuickOrderHelper.QuickOrderItem>.class);

            List<CCPDCQuickOrderHelper.QuickOrderItem> items = CCPDCQuickOrderHelper.getQuickOrderItems(input);

            Map<String,Object> output = new Map<String,Object>();
            Map<Id,CCPDCQuickOrderHelper.QuickOrderItem> qiMap = new Map<Id,CCPDCQuickOrderHelper.QuickOrderItem>();
            output.put('items', items);

            if(!items.isEmpty()){
                for(CCPDCQuickOrderHelper.QuickOrderItem qi : items){
                    if(qi.record != null){
                        qiMap.put(qi.record.Id,qi);  
                    }

                }
                ccrz__E_Cart__c currentCart = CCPDCCartDAO.getCart(currcartId);

                for(ccrz__E_CartItem__c ci : currentCart.ccrz__E_CartItems__r){
                    if(qiMap.containsKey(ci.ccrz__Product__c)){
                        CCPDCQuickOrderHelper.QuickOrderItem nqi = qiMap.get(ci.ccrz__Product__c);
                        nqi.boomiPrice = ci.ccrz__Price__c;
                    }
                }

                output.put('qiMap',qiMap);

            }

            if (CCPDCQuickOrderHelper.validate(items)) {
                String cartEncId = null;
                Boolean wasSuccessful = true;

                List<ccrz.ccApiCart.LineData> newLines = new List<ccrz.ccApiCart.LineData>();
                for (CCPDCQuickOrderHelper.QuickOrderItem item : items) {
                    if (item.sku == null || item.quantity == null) {
                        wasSuccessful = false;
                    }
                    else {
                        ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
                        newLine.sku = item.sku;
                        newLine.quantity = item.quantity;
                        if(item.boomiPrice != null){
                           newLine.price = item.boomiPrice;
                        }
                        newLines.add(newLine);
                    }
                }

                if (wasSuccessful) {
                    String cartId = CCAviCartManager.getActiveCartId();

                    if (cartId != null) {
                        Map<String, Object> inputData = new Map<String,Object>{
                            ccrz.ccApi.API_VERSION => 6, 
                            ccrz.ccApiCart.CART_ENCID => cartId,
                            ccrz.ccApiCart.LINE_DATA => newLines
                        };
                
                        Map<String,Object> outputData = null;
                        Map<String, Decimal> priceMap = null;
                        Map<String, Decimal> requiredPricingMap = null;

                        List<Object> linedata = (List<Object>) inputData.get(ccrz.ccApiCart.LINE_DATA);
                        if (linedata != null && !linedata.isEmpty()) {

                            Map<String, Map<String, Decimal>> quantityMap = CCPDCPricingHelper.getQuantityMap(linedata);
                            requiredPricingMap = CCPDCCartManager.getRequiredProducts(quantityMap);

                            HttpRequest httpRequest = CCPDCPricingHelper.priceWithCont(linedata, quantityMap);
                            con = new Continuation(120);
                            stateMap = new Map<String, Object>();
                            stateMap.put('RequestLabel', con.addHttpRequest(httpRequest));
                            stateMap.put('ccrzContext', ctx);
                            stateMap.put('inputData', inputData);
                            stateMap.put('linedata', linedata);
                            stateMap.put('quantityMap', quantityMap);
                            stateMap.put('requiredPricingMap', requiredPricingMap);
                            stateMap.put('cartId', cartId);
                            stateMap.put('currcartId', currcartId);
                            con.state = stateMap;
                            con.continuationMethod = 'addBulkContCallback';
                            return con;
                            //CCAviBoomiAPI.SeriesPricingWSResponse response = CCPDCPricingHelper.price(linedata, quantityMap);
                            //if (response != null && response.success && response.listPricing != null) {
                            //    priceMap = CCPDCPricingHelper.createPriceMap(response);
                            //    //CCPDCCallContext.priceMap = priceMap;
                            //}
                        }
                    }
                }
            }

        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return null;
    }    

    public static ccrz.cc_RemoteActionResult addBulkContCallback() {
        return addBulkContCallback(stateMap);
    }

    public static ccrz.cc_RemoteActionResult addBulkContCallback(Object state) {
        Map<String, Decimal> priceMap = null;
        Map<String,Object> outputData = null;
        Map<String, Decimal> requiredPricingMap = null;

        Map<String,Object> inputData = (Map<String,Object>)((Map<String,Object>)state).get('inputData');
        Map<String,Map<String,Decimal>> quantityMap = (Map<String,Map<String,Decimal>>)((Map<String,Object>)state).get('quantityMap');
        List<Object> data = (List<Object>)inputData.get(ccrz.ccApiCart.LINE_DATA);
        CCAviBoomiAPI.SeriesPricingWSResponse boomiresponse = CCPDCPricingHelper.priceWithContCallback(state);
        if (boomiresponse != null && boomiresponse.success && boomiresponse.listPricing != null) {
            priceMap = CCPDCPricingHelper.createPriceMap(boomiresponse);
            //CCPDCCallContext.priceMap = priceMap;
        }
        outputData = ccrz.ccAPICart.addTo(inputData);

        ccrz.cc_RemoteActionContext ctx = (ccrz.cc_RemoteActionContext)((Map<String,Object>)state).get('ccrzContext');
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        Boolean isSuccess = (Boolean) outputData.get(ccrz.ccAPI.SUCCESS);
        String cartId = null;
        String cartEncId = null;
        if (isSuccess && requiredPricingMap != null && !requiredPricingMap.isEmpty()) {
            cartId = (String)outputData.get(ccrz.ccApiCart.CART_ENCID);
            CCPDCCartManager.updateRequiredProducts(cartId, priceMap);
        }

        Boolean wasSuccessful = (Boolean)outputData.get(ccrz.ccApi.SUCCESS);
        if (wasSuccessful) {
            cartEncId = (String)outputData.get(ccrz.ccApiCart.CART_ENCID);
        }
        cartId = cartEncId;
        ccrz__E_Cart__c afterAddCart = CCPDCCartDAO.getCart(cartId);
        List<ccrz__E_CartItem__c> aftercartItems = afterAddCart.ccrz__E_CartItems__r;
        for(ccrz__E_CartItem__c ci:aftercartItems){
            ci.ccrz__OriginalItemPrice__c = ci.ccrz__Price__c;
        }
        update aftercartItems;

        Map<String,Object> output = new Map<String,Object>();
        if (cartId != null) {
            output.put('cartId', (String)((Map<String,Object>)state).get('currcartId'));
            CCAviPageUtils.buildResponseData(response, true, output);
        }
        else {
            CCAviPageUtils.buildResponseData(response, false, output);                
        }
        return response;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult validate(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            List<CCPDCQuickOrderHelper.QuickOrderItem> input = (List<CCPDCQuickOrderHelper.QuickOrderItem>) JSON.deserialize(data, List<CCPDCQuickOrderHelper.QuickOrderItem>.class);

            List<CCPDCQuickOrderHelper.QuickOrderItem> items = CCPDCQuickOrderHelper.getQuickOrderItems(input);

            Map<String,Object> output = new Map<String,Object>();
            output.put('items', items);

            if (CCPDCQuickOrderHelper.validate(items)) {
                CCAviPageUtils.buildResponseData(response, true, output);                
            }
            else {
                CCAviPageUtils.buildResponseData(response, false, output);                
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }
}