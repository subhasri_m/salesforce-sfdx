public without sharing class CCPDCCartDAO {
   public static List<ccrz__E_CartItem__c> getBranchNonSupportedCIs(String encryptedId){
        ccrz__E_Cart__c cart1 = [
            SELECT
            Id,
            CC_FP_Location__c
            FROM 
            ccrz__E_Cart__c
            WHERE
            ccrz__EncryptedId__c =:encryptedId
            limit 1
        ];
       
       Location__c currBranch = [Select PickUp_ON__c,Shipping_ON__c,LocalDelivery_ON__c from location__c where id =: cart1.CC_FP_Location__c limit 1 ];
       Set<String> notAllowedBranches = new Set<String>();
       if(!currBranch.PickUp_ON__c){
           notAllowedBranches.add('PickUp'); 
       }
       if(!currBranch.Shipping_ON__c){
           notAllowedBranches.add('ShipTo');
       }
       if(!currBranch.LocalDelivery_ON__c){
           notAllowedBranches.add('LocalDelivery');
       }
        List<ccrz__E_CartItem__c> cartItemList = [
            SELECT 
            Id,
            ccrz__Product__r.Name ,
            ccrz__Product__r.DSP_Part_Number__c,
            ccrz__Price__c,
            FulFillMentType__c
            FROM 
            ccrz__E_CartItem__c 
            WHERE ccrz__Cart__c =: cart1.Id
            AND   FulFillMentType__c in: notAllowedBranches ];

            return cartItemList;

    }
    
    public static List<ccrz__E_CartItem__c> getZeroPriceCartItems(String encryptedId){
        ccrz__E_Cart__c cart1 = [
            SELECT
            Id
            FROM 
            ccrz__E_Cart__c
            WHERE
            ccrz__EncryptedId__c =:encryptedId
            limit 1
        ];
        
        List<ccrz__E_CartItem__c> cartItemList = [
            SELECT 
            Id,
            ccrz__Product__r.Name ,
            ccrz__Product__r.DSP_Part_Number__c,
            ccrz__Price__c
            FROM 
            ccrz__E_CartItem__c 
            WHERE ccrz__Price__c = 0
        	AND   ccrz__Cart__c =: cart1.Id ];

            return cartItemList;

    }
    public static List<ccrz__E_Cart__c> getCartsFromAccount(String effectiveAccountId){
        List<ccrz__E_Cart__c> cartList = [
            SELECT 
            Id, 
            ccrz__Name__c,
            ccrz__SubtotalAmount__c,
            ownerId,
            ccrz__EncryptedId__c,
            CC_FP_Needs_Approval__c,
            ccrz__User__c,
            lastModifiedDate
            FROM 
            ccrz__E_Cart__c 
            WHERE ccrz__EffectiveAccountID__c =: effectiveAccountId 
                AND ccrz__CartStatus__c = 'Open'
                AND ccrz__CartType__c = 'Cart' ORDER BY CC_FP_Needs_Approval__c DESC NULLS LAST];

            return cartList;

    }
	public static ccrz__E_Cart__c getCart(String encryptedId) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                Name,
                ccrz__Name__c,
                ccrz__Account__c,
                ccrz__ActiveCart__c,
                ccrz__CartStatus__c,
                ccrz__CartType__c,
                CC_FP_Location__c,
                ccrz__BillTo__c,
                ccrz__ShipTo__c,
                CC_FP_Location__r.Location__c,
                CC_FP_Order_Subtotal__c,
                CC_FP_Shipping_Total_Qty__c,
                ccrz__ShipTo__r.ccrz__PostalCode__c,
                ccrz__ShipTo__r.CC_FP_Type__c,
                ccrz__ShipTo__r.CC_FP_Has_Lift_Gate__c,
                ccrz__ShipTo__r.CC_FP_Local_DC_ID__c,
                CC_FP_Is_Will_Call__c,
                CC_FP_Is_Ship_Remainder__c,
            	ccrz__Contact__r.Account.Ship_from_Location__c,
                (SELECT ccrz__Product__c, ccrz__Product__r.ccrz__ProductStatus__c, ccrz__Product__r.Name, 
                    ccrz__Product__r.ccrz__SKU__c,ccrz__Price__c, ccrz__Quantity__c,ccrz__ItemTotal__c, 
                    CC_FP_QTY_Shipping__c, CC_FP_QTY_Backorder__c,CC_FP_Item_Order_Subtotal__c,
                    ccrz__SubAmount__c, CC_FP_Pricing_Failed__c,ccrz__OriginalItemPrice__c,
                    FP_contain_Core_Price__c, CC_FP_PONumber__c,  FulFillMentType__c FROM ccrz__E_CartItems__r)
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c  = :encryptedId
        ];

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }

        return cart;
    }
    public static ccrz__E_Cart__c getCartById(String cartId) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                Name,
                ccrz__Name__c,
                ccrz__Account__c,
                ccrz__ActiveCart__c,
                ccrz__CartStatus__c,
                ccrz__EncryptedId__c,
                ccrz__CartType__c,
                ccrz__BillTo__c,
                ccrz__ShipTo__c,
                ccrz__ShipTo__r.ccrz__PostalCode__c,
                ccrz__ShipTo__r.CC_FP_Type__c,
                ccrz__ShipTo__r.CC_FP_Has_Lift_Gate__c,
            	CC_Cardconnect_Response__c,
            	ccrz__ShipTo__r.CC_FP_Local_DC_ID__c,
                (SELECT ccrz__Product__c,  FulFillMentType__c, ccrz__Product__r.Name, ccrz__Product__r.ccrz__SKU__c,ccrz__Price__c, ccrz__Quantity__c,ccrz__ItemTotal__c,ccrz__SubAmount__c FROM ccrz__E_CartItems__r)
            FROM
                ccrz__E_Cart__c
            WHERE
                Id  = :cartId
        ];

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }

        return cart;
    }
    
    public static ccrz__E_Cart__c getContactInfoInCart(String encryptedId){
        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
            Id,
            ccrz__Account__r.Name,
            ccrz__Contact__r.Name,
            ccrz__Contact__r.Email
            FROM
        ccrz__E_Cart__c
            WHERE
            ccrz__EncryptedId__c  = :encryptedId
        ];

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }
        return cart;

    }
    public static ccrz__E_Cart__c getBackOrderCartFromAccountId(String accountId){
        ccrz__E_Cart__c cart = null;
        List<ccrz__E_Cart__c> cartList =[
        SELECT 
        Id,
        Name,
        CC_FP_Free_Shipping_Type__c,
        CC_FP_Is_Free_Shipping__c,
        ccrz__EncryptedId__c,
        (SELECT Id, FulFillMentType__c, ccrz__Product__c, ccrz__Product__r.Name, ccrz__Product__r.ccrz__SKU__c,ccrz__Price__c, ccrz__Quantity__c,ccrz__ItemTotal__c, CC_FP_PONumber__c,PO_Number__c,BO_ID__c,BO_Line_Number__c FROM ccrz__E_CartItems__r)
        FROM 
        ccrz__E_Cart__c 
        WHERE 
        ccrz__Account__r.Id  =:accountId AND ccrz__CartType__c = 'BackOrder'];

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }
        return cart;
    }

    // Query based on the provided Encrypted Cart Id
    public static ccrz__E_Cart__c getCartExtra(String theId) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                Name,
                ccrz__EncryptedId__c,
                ccrz__Storefront__c,
                CC_FP_Free_Shipping_Type__c,
                CC_FP_Is_Free_Shipping__c,
                CC_FP_Is_Ship_Remainder__c,
                CC_FP_Has_Validation_Message__c,
                CC_FP_Is_Will_Call__c,
                CC_FP_Location__c,
                CC_FP_Tax_Rate__c,
                ccrz__PONumber__c,
                ccrz__Contact__r.CUSTNO__c,
                ccrz__Account__r.AccountNumber,
                ccrz__TotalAmount__c,
                ccrz__ShipTo__c,
            	ccrz__ShipTo__r.ccrz__AddressFirstline__c,
            	ccrz__ShipTo__r.ccrz__AddressSecondline__c,
                ccrz__ShipTo__r.ccrz__City__c,
                ccrz__ShipTo__r.ccrz__State__c,
                ccrz__ShipTo__r.CC_FP_County__c,
                ccrz__ShipTo__r.ccrz__PostalCode__c,
                ccrz__ShipTo__r.CC_FP_Location__c,
                ccrz__ShipTo__r.CC_FP_Type__c,
                ccrz__ShipTo__r.CC_FP_Has_Lift_Gate__c,
                ccrz__ShipTo__r.CC_FP_Local_DC_ID__c,
                Reservation_Id__c,
                ccrz__BillTo__c,
                ccrz__BillTo__r.ccrz__City__c,
                ccrz__BillTo__r.ccrz__State__c,
                ccrz__BillTo__r.CC_FP_County__c,
                ccrz__BillTo__r.ccrz__PostalCode__c,
                ccrz__BillTo__r.CC_FP_Location__c,
            	CC_FP_Location__r.Location__c,
          	    CC_FP_Location__r.Address_Line_1__c,
                CC_FP_Location__r.Address_Line_2__c,
                CC_FP_Location__r.City__c,
                CC_FP_Location__r.State__c,
                CC_FP_Location__r.County__c,
                CC_FP_Location__r.Zipcode__c,
                CC_FP_Location__r.In_City__c,
                ccrz__SubtotalAmount__c,
                ccrz__TaxExemptFlag__c,
                ccrz__TaxAmount__c,
                (SELECT 
                    ccrz__Quantity__c,
                 	FulFillMentType__c,
                    CC_FP_QTY_Backorder__c,
                    CC_FP_QTY_Shipping__c,
                    ccrz__Price__c,
                    ccrz__SubAmount__c,
                    ccrz__CartItemGroup__c,
                    ccrz__Product__c, 
                    ccrz__Product__r.Name, 
                    ccrz__Product__r.ccrz__SKU__c,
                    ccrz__Product__r.FP_Brand_Code__c,
                    ccrz__Product__r.FP_Brand_Name__c,
                    ccrz__Product__r.CORE_Part_Number__c,
                    ccrz__Product__r.Core_Pool_Number__c,
                    ccrz__Product__r.Freight_Attribute__c,
                    ccrz__Product__r.Manufacturer_Part__c,
                    ccrz__Product__r.Part_Number__c,
                    ccrz__Product__r.Pool_Number__c,
                    ccrz__Product__r.DSP_Part_Number__c,
                    ccrz__Product__r.ccrz__ShippingWeight__c,
                    CC_FP_PONumber__c,
                    ccrz__Product__r.ccrz__UnitOfMeasure__c,
                    ccrz__UnitOfMeasure__c,
                    CC_FP_Backorder_Line_Number__c,
                    CC_FP_Line_Number__c
                FROM ccrz__E_CartItems__r)
            FROM
                ccrz__E_Cart__c
            WHERE
            ccrz__EncryptedId__c = :theId
        ];
        
        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }
        return cart;
    }

    public static ccrz__E_Cart__c getCartOwner(String theId) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                ccrz__Name__c,
                OwnerId,
                ccrz__Account__c,
                ccrz__Contact__c,
                ccrz__User__c,
                ccrz__ShipTo__c,
                ccrz__ShipTo__r.Id,
                ccrz__ShipTo__r.OwnerId,
                ccrz__BillTo__c,
                ccrz__BillTo__r.Id,
                ccrz__BillTo__r.OwnerId
            FROM
                ccrz__E_Cart__c
            WHERE
                Id  = :theId
        ];

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }

        return cart;
    }

    public static ccrz__E_CartCoupon__c getCartCouponCode(String cartId) {
        ccrz__E_CartCoupon__c couponCode = null;
         List<ccrz__E_CartCoupon__c> couponCodeList = [ 
            SELECT  Id, ccrz__Cart__r.Id, ccrz__Coupon__r.ccrz__CouponCode__c 
            FROM ccrz__E_CartCoupon__c
            WHERE ccrz__Cart__r.Id =: cartId
        ];
        
        if (!couponCodeList.isEmpty()) {
            couponCode = couponCodeList[0];
        }        
        return couponCode;
    }

    public static ccrz__E_Cart__c getCartSfid(String sfid) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                Name,
                ccrz__Account__c,
                ccrz__ActiveCart__c,
                ccrz__CartStatus__c,
                ccrz__CartType__c,
                (SELECT ccrz__Product__c, ccrz__Product__r.Name, ccrz__Product__r.ccrz__SKU__c,ccrz__Price__c, ccrz__Quantity__c FROM ccrz__E_CartItems__r)
            FROM
                ccrz__E_Cart__c
            WHERE
                Id  = :sfid
        ];

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }

        return cart;
    }
    
    public static Map<Id, ccrz__E_CartItem__c> getCartItemsForPricing(Set<Id> ids) {
        Map<Id, ccrz__E_CartItem__c> items = new Map<Id, ccrz__E_CartItem__c>([
            SELECT 
                Id, 
                Name,
                ccrz__Quantity__c,
                ccrz__Price__c,
                ccrz__SubAmount__c,
                ccrz__Product__c,
                ccrz__Product__r.Part_Number__c,
                ccrz__Product__r.Pool_Number__c,
                ccrz__Product__r.ccrz__SKU__c,
                CC_FP_DoNotReprice__c,
                ccrz__OriginalItemPrice__c,
                CC_FP_Price_Timestamp__c,
                CC_FP_Price_Override__c,
                CC_FP_CSR_Price_Override__c
            FROM 
                ccrz__E_CartItem__c
            WHERE
                Id IN :ids
        ]);
        return items;
    }

    public static ccrz__E_Cart__c getCartForPricing(String cartId) {
        ccrz__E_Cart__c cart = null;
        List<ccrz__E_Cart__c> cartList = new List<ccrz__E_Cart__c>([
            SELECT 
                Id, 
                Name,
                (SELECT 
                    Id,
                    ccrz__Cart__c,
                    ccrz__Quantity__c,
                    ccrz__Price__c,
                    ccrz__OriginalItemPrice__c,
                    ccrz__SubAmount__c,
                    ccrz__Product__c, 
                    ccrz__Product__r.Name, 
                    ccrz__Product__r.ccrz__SKU__c,
                    ccrz__Product__r.Part_Number__c,
                    ccrz__Product__r.Pool_Number__c,
                   FulFillMentType__c
                FROM ccrz__E_CartItems__r),
                (SELECT 
                    Id,
                    CC_Parent_Cart_Item__c,
                    Quantity__c,
                    Price__c,
                    SubAmount__c,
                    CC_Product__c, 
                    CC_Product__r.Name, 
                    CC_Product__r.ccrz__SKU__c,
                    CC_Product__r.Part_Number__c,
                    CC_Product__r.Pool_Number__c
                FROM CC_FP_Required_Cart_Items__r)
            FROM 
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c = :cartId
        ]);

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }

        return cart;
    }

    public static List<ccrz__E_CartItem__c> getCartItems(Set<Id> ids) {
        List<ccrz__E_CartItem__c> items = new List<ccrz__E_CartItem__c>([
             SELECT 
                Id,
                ccrz__Cart__c,
                ccrz__Quantity__c,
                ccrz__Price__c,
                ccrz__SubAmount__c,
                CC_FP_Item_Order_Subtotal__c,
                CC_FP_DoNotReprice__c,
                CC_FP_QTY_Backorder__c,
                CC_FP_QTY_Shipping__c,
             	FulFillMentType__c

            FROM 
                ccrz__E_CartItem__c
            WHERE
                Id IN :ids
            ]);

        return items;
    }

    public static List<ccrz__E_CartItemGroup__c> getCartItemGroupsForCart(Id theId) {

        List<ccrz__E_CartItemGroup__c> items = [
            SELECT 
                Id
            FROM 
                ccrz__E_CartItemGroup__c
            WHERE
                ccrz__Cart__c =  :theId
            ];

        return items;

    }
    
     public static List<ccrz__E_CartItemGroup__c> getCartItemGroupDetailsForCart(Id theId) {

        List<ccrz__E_CartItemGroup__c> items = [
            SELECT 
                Id,
            	ccrz__ShipMethod__c,
            	CC_FP_Subtotal_Amount__c,
            	CC_FP_Tax_Amount__c,
            	CC_FP_Total_Amount__c             	
            FROM 
                ccrz__E_CartItemGroup__c
            WHERE
                ccrz__Cart__c =  :theId
            ];

        return items;

    }
    
    public static ccrz__E_Cart__c getCartItemGroupDetails(String theId) {

        ccrz__E_Cart__c cart = [
            SELECT 
                Id,
            	ccrz__TotalAmount__c,
            	(Select Name, CC_FP_Subtotal_Amount__c,ccrz__GroupName__c, CC_FP_Total_Amount__c from ccrz__E_CartItemGroups__r)            	
            FROM 
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c =  :theId LIMIT 1
            ];

        return cart;

    }

    public static ccrz__E_Cart__c getCartOrderView(String encryptedId) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                Name,
                ccrz__ShipTo__r.ccrz__City__c,
                ccrz__ShipTo__r.ccrz__State__c,
                ccrz__ShipTo__r.CC_FP_County__c,
                ccrz__ShipTo__r.ccrz__PostalCode__c,
                ccrz__ShipTo__r.CC_FP_Location__c,
                ccrz__BillTo__r.ccrz__City__c,
                ccrz__BillTo__r.ccrz__State__c,
                ccrz__BillTo__r.CC_FP_County__c,
                ccrz__BillTo__r.ccrz__PostalCode__c,
                ccrz__BillTo__r.CC_FP_Location__c,
                CC_FP_Location__r.City__c,
                CC_FP_Location__r.State__c,
                CC_FP_Location__r.County__c,
                CC_FP_Location__r.Zipcode__c,
                CC_FP_Location__r.In_City__c,
                ccrz__SubtotalAmount__c,
                ccrz__TaxExemptFlag__c,
                CC_FP_Location__c,
                ccrz__TaxAmount__c
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c  = :encryptedId
        ];

        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }

        return cart;
    }

    public static ccrz__E_Cart__c getCartCustom(String theId) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                Name,
                ccrz__EncryptedId__c,
                ccrz__PONumber__c,
                ccrz__ShipTo__c,
                ccrz__ShipAmount__c,
                ccrz__TaxSubTotalAmount__c,
                CC_FP_Free_Shipping_Type__c,
                CC_FP_Is_Free_Shipping__c,
                CC_FP_Is_Ship_Remainder__c,
                CC_FP_Is_Will_Call__c,
                CC_FP_Location__c,
                CC_FP_Tax_Rate__c,
            	Order_Comments__c,
                Reservation_Id__c,
                (SELECT 
                    Id,
                    Name,
                    ccrz__Quantity__c,
                    ccrz__Product__c, 
                    CC_FP_Backorder_Line_Number__c,
                    CC_FP_Line_Number__c,
                    CC_FP_PONumber__c,
                    ccrz__CartItemGroup__c,
                    ccrz__CartItemGroup__r.ccrz__GroupName__c
               FROM ccrz__E_CartItems__r),
                (SELECT 
                    Id,
                    Name,
                    ccrz__GroupName__c,
                    CC_FP_Is_Backorder__c,
                    CC_FP_DC_ID__c,
                    CC_FP_Subtotal_Amount__c,
                    CC_FP_Tax_Amount__c,
                    CC_FP_Total_Amount__c
                FROM ccrz__E_CartItemGroups__r),
                (SELECT 
                    Id,
                    CC_Parent_Cart_Item__c,
                    Quantity__c,
                    Price__c,
                    SubAmount__c,
                    CC_Product__c
                FROM CC_FP_Required_Cart_Items__r)
            FROM
                ccrz__E_Cart__c
            WHERE
                Id = :theId
        ];
        
        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }
        return cart;
    }

    public static ccrz__E_Cart__c getCartSplitResponse(String theId) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                Name,
                ccrz__EncryptedId__c,
                ccrz__ShipAmount__c,
                ccrz__SubtotalAmount__c,
                ccrz__TaxAmount__c,
                ccrz__TaxExemptFlag__c,
                CC_FP_Split_Response__c,
                CC_FP_Tax_Rate__c
            FROM
                ccrz__E_Cart__c
            WHERE
                ccrz__EncryptedId__c = :theId
        ];
        
        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }
        return cart;
    }

    public static ccrz__E_Cart__c getCartShipTo(String theId) {

        ccrz__E_Cart__c cart = null;

        List<ccrz__E_Cart__c> cartList = [
            SELECT
                Id,
                Name,
                ccrz__EncryptedId__c,
                ccrz__Storefront__c,
                CC_FP_Is_Ship_Remainder__c,
                CC_FP_Is_Will_Call__c,
                CC_FP_Location__c,
                ccrz__BillTo__c,
                ccrz__ShipTo__c,
            	ccrz__BuyerPhone__c,
            	ccrz__BuyerEmail__c,
                ccrz__BillTo__r.ccrz__AddressFirstline__c,
                ccrz__BillTo__r.ccrz__AddressSecondline__c,
                ccrz__BillTo__r.ccrz__AddressThirdline__c,
                ccrz__BillTo__r.ccrz__City__c,
                ccrz__BillTo__r.ccrz__State__c,
                ccrz__BillTo__r.ccrz__Country__c,
                ccrz__BillTo__r.CC_FP_County__c,
                ccrz__BillTo__r.ccrz__PostalCode__c,

                ccrz__ShipTo__r.ccrz__AddressFirstline__c,
                ccrz__ShipTo__r.ccrz__AddressSecondline__c,
                ccrz__ShipTo__r.ccrz__AddressThirdline__c,
                ccrz__ShipTo__r.ccrz__City__c,
                ccrz__ShipTo__r.ccrz__State__c,
                ccrz__ShipTo__r.ccrz__Country__c,
                ccrz__ShipTo__r.CC_FP_County__c,
                ccrz__ShipTo__r.ccrz__PostalCode__c,
                ccrz__ShipTo__r.CC_FP_Location__c,
                ccrz__ShipTo__r.CC_FP_Type__c,
                ccrz__ShipTo__r.CC_FP_Has_Lift_Gate__c,
                ccrz__ShipTo__r.CC_FP_Local_DC_ID__c
            FROM
                ccrz__E_Cart__c
            WHERE
            ccrz__EncryptedId__c = :theId
        ];
        
        if (!cartList.isEmpty()) {
            cart = cartList[0];
        }
        return cart;
    }
    
    public static Map<String,ccrz__E_ProductMedia__c> getFPSkusMedia(Set<String> Skus){
        Map<String,ccrz__E_ProductMedia__c> skuMediaMap = new Map<String,ccrz__E_ProductMedia__c>();
        List <ccrz__E_ProductMedia__c> prodMedialst = [Select ccrz__URI__c,ccrz__MediaType__c, ccrz__Product__r.ccrz__SKU__c,ID from ccrz__E_ProductMedia__c where  ccrz__Product__r.ccrz__SKU__c in : skus and ccrz__MediaType__c = 'Product Image'];
        for (ccrz__E_ProductMedia__c media : prodMedialst ){
            skuMediaMap.put(media.ccrz__Product__r.ccrz__SKU__c, media);
        }
        return skuMediaMap;
    }
   public static String getCartLocationCode(String cartId){
         if(cartId==null){
             return '';
         }
       ccrz__E_Cart__c cart = [Select CC_FP_Location__r.Location__c, Name from ccrz__E_Cart__c where ccrz__EncryptedId__c =: cartId limit 1 ];
       return cart.CC_FP_Location__r.Location__c;
    }
    public static List<ccrz__E_CartItem__c> getCartItemsFromSKU(String encryptedId, String Sku){
        ccrz__E_Product__c product = [SELECT Id,ccrz__SKU__c, Salespack__c FROM ccrz__E_Product__c WHERE ccrz__SKU__c =: Sku limit 1];
		ccrz__E_Cart__c cart = null;
        List<ccrz__E_Cart__c> cartlst = new List<ccrz__E_Cart__c>([
            SELECT 
            Id,
            	( SELECT id,ccrz__Product__c, ccrz__Product__r.ccrz__ProductStatus__c, ccrz__Product__r.Name, 
             		ccrz__Product__r.ccrz__SKU__c,ccrz__Price__c, ccrz__Quantity__c,ccrz__ItemTotal__c, 
            		CC_FP_QTY_Shipping__c, CC_FP_QTY_Backorder__c,CC_FP_Item_Order_Subtotal__c,
             		ccrz__SubAmount__c, CC_FP_Pricing_Failed__c,ccrz__OriginalItemPrice__c,
             		FP_contain_Core_Price__c, CC_FP_PONumber__c,  FulFillMentType__c FROM ccrz__E_CartItems__r where ccrz__Product__c =:product.id )
           FROM
            ccrz__E_Cart__c
           WHERE
            ccrz__EncryptedId__c  = :encryptedId
        ]);
		cart = cartlst[0];
        return cart.ccrz__E_CartItems__r;
    }
   
}