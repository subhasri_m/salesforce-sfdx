global with sharing class CCPDCMyAccountMyCartsController {
    
    @RemoteAction
    global static ccrz.cc_RemoteActionResult getCarts(ccrz.cc_RemoteActionContext ctx) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        String effAccountId = String.valueOf(ccrz.cc_CallContext.effAccountId);
        Id contactId = String.valueOf(ccrz.cc_CallContext.currContact.Id);
        try {
            List<Map<String, Object>> outputCartList = new List<Map<String, Object>>();
            List<ccrz__E_Cart__c> cartList = CCPDCCartDAO.getCartsFromAccount(effAccountId);
            //List<CC_FP_Contract_Account_Permission_Matrix__c> matrixList = CCFPContactAccountPermissionMatrixDAO.getAccountsForContact(contactId);
            
            if(!cartList.isEmpty()){
                for(ccrz__E_Cart__c cart : cartList){
                    map<String, Object> cartmap = new map<String, Object>();
                    cartmap.put('name',cart.ccrz__Name__c);
                    cartmap.put('lastModifiedDate',cart.lastModifiedDate);
                    cartmap.put('sfid',cart.Id);
                    cartmap.put('subtotalAmount',cart.ccrz__SubtotalAmount__c);
                    cartmap.put('encryptedId',cart.ccrz__EncryptedId__c);
                    cartmap.put('ownerId',cart.ownerId);
                    cartmap.put('cartStatus',cart.CC_FP_Needs_Approval__c);
                    cartmap.put('contactUserId',cart.ccrz__User__c);
                    //cartmap.put('pricingVisibility',matrixList[0].Pricing_Visibility__c);
                    outputCartList.add(cartmap);
                }
                processCartData(outputCartList);
                CCAviPageUtils.buildResponseData(response, true, 
                    new Map<String,Object>{'carts' => outputCartList}
                );   

            } else {
                CCAviPageUtils.buildResponseData(response, false, 
                    new Map<String,Object>{'carts' => outputCartList}
                ); 
            }
            return response;
            //Map<String, Object> request = new Map<String,Object>{
            //    ccrz.ccApi.API_VERSION => 6, 
            //    ccrz.ccApiCart.CARTTYPE => 'Cart', 
            //    ccrz.ccApiCart.CARTSTATUS => 'Open', 
            //    ccrz.ccApiCart.BYEFFECTIVEACCOUNT => ccrz.cc_CallContext.effAccountId,
            //    ccrz.ccAPI.SIZING => new Map<String, Object>{
            //        ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
            //            ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
            //        }
            //    }
            //};
  
            //Map<String,Object> data = ccrz.ccAPICart.fetch(request);
            //Boolean isSuccess = (Boolean) data.get(ccrz.ccAPI.SUCCESS);
            //if (isSuccess) {
            //    List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) data.get(ccrz.ccAPICart.CART_OBJLIST);
            //    processCartData(outputCartList);
            //    CCAviPageUtils.buildResponseData(response, true, 
            //        new Map<String,Object>{'carts' => outputCartList}
            //    );                
            //}
            //else {
            //    response.success = false;
            //}

        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response;
    }

    private static void processCartData(List<Map<String, Object>> outputCartList) {
        if (outputCartList != null && !outputCartList.isEmpty()) {
            Set<String> ownerIds = new Set<String>();
            for (Map<String, Object> m : outputCartList) {
                String ownerId = (String) m.get('ownerId');
                if (ownerId != null) {
                    ownerIds.add(ownerId);
                    m.put('isOwner', ownerId.equals(ccrz.cc_CallContext.currUserId));
                }
                String lastModifiedDate = String.valueOfGmt((DateTime)m.get('lastModifiedDate')) ;
                if (lastModifiedDate != null) {
                    lastModifiedDate = lastModifiedDate.replace('T',' ').replace('Z', '');
                    try {
                        Datetime myDate = Datetime.valueOfGmt(lastModifiedDate);
                        m.put('lastModifiedDateStr', myDate.format('MM/dd/yyyy h:mm a', UserInfo.getTimeZone().getID()));
                    }
                    catch (Exception e) {
                        m.put('lastModifiedDateStr', lastModifiedDate);
                    }
                }
            }
            if (!ownerIds.isEmpty()) {
                Map<Id, User> userMap = CCPDCUserDAO.getUsersMap(new List<String>(ownerIds));
                for (Map<String, Object> m : outputCartList) {
                    String ownerId = (String) m.get('ownerId');
                    if (ownerId != null) {
                        User u = userMap.get(ownerId);
                        if (u != null) {
                            m.put('ownerName', u.Contact.Name);
                        }
                    }
                }
            }
        }
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult changeCartOwner(ccrz.cc_RemoteActionContext ctx, String cartId) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            if (cartId != null) {
                CCPDCChangeOwnerHelper.changeCartOwnerAll(cartId, ccrz.cc_CallContext.currUserId);
                response.success = true;
            }
            else {
                response.success = false;
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult cloneCart(ccrz.cc_RemoteActionContext ctx, String cartId) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        response.success = false;

        try {
            if (cartId != null) {
                ccrz__E_Cart__c cart = CCPDCCartDAO.getCartOwner(cartId);
                Id oldOwner = cart.OwnerId;
                Id oldAccount = cart.ccrz__Account__c;
                Id oldContact = cart.ccrz__Contact__c;

                Id newOwner = ccrz.cc_CallContext.currUserId;
                Id newAccount = ccrz.cc_CallContext.currAccountId;
                Id newContact = ccrz.cc_CallContext.currContact.Id;

                CCPDCChangeOwnerHelper.changeCartOwnerAll(cartId, newOwner, newContact, newAccount);
                String newId = CCAviCartManager.cloneCart(cartId);
                CCPDCChangeOwnerHelper.changeCartOwnerAll(cartId, oldOwner, oldContact, oldAccount);
                if (newId != null) {
                    response.data = new Map<String,Object>{'encId' => newId};
                    response.success = true;
                }
            }
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response;
    }

    @RemoteAction
     global static ccrz.cc_RemoteActionResult setNewCartValues(ccrz.cc_RemoteActionContext ctx, String oldCart, String newCart){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);
        String currentCartId = ctx.currentCartId;
        try {
            ccrz__E_Cart__c anewcart =  CCPDCCartDAO.getCart(newCart);
            ccrz__E_Cart__c aoldcart = CCPDCCartDAO.getCartById(oldCart);
            ccrz__E_Cart__c currentCart = CCPDCCartDAO.getCart(currentCartId);

            ccrz__E_ContactAddr__c oldshipto = CCFPContactAddressDAO.getAddress(aoldcart.ccrz__ShipTo__c);
            ccrz__E_ContactAddr__c oldbillto = CCFPContactAddressDAO.getAddress(aoldcart.ccrz__BillTo__c);
            
            ccrz__E_ContactAddr__c newshipto = oldshipto.clone();
            ccrz__E_ContactAddr__c newbillto = oldbillto.clone();
            newshipto.ccrz__FirstName__c ='firstname';
            newshipto.ccrz__LastName__c ='lastname';
            insert newshipto;
            insert newbillto;

            anewcart.ccrz__BillTo__c = newbillto.Id;
            anewcart.ccrz__ShipTo__c = newshipto.Id;

            List<ccrz__E_CartItem__c> newcartitems =  anewcart.ccrz__E_CartItems__r;
            List<ccrz__E_CartItem__c> oldcartitems=  aoldcart.ccrz__E_CartItems__r;
            List<ccrz__E_CartItem__c> items = new List<ccrz__E_CartItem__c>();
            if(!oldcartitems.isEmpty() && !newcartitems.isEmpty()){
                for(Integer i = 0; i<newcartitems.size();i++){
                    ccrz__E_CartItem__c ci = newcartitems.get(i);
                    ccrz__E_CartItem__c oldci = oldcartitems.get(i);
                    ci.ccrz__Price__c = oldci.ccrz__Price__c;
                    ci.ccrz__SubAmount__c = oldci.ccrz__SubAmount__c;
                    items.add(ci);
                }
            }
            if(currentCart != null && anewcart != null){
                currentCart.ccrz__ActiveCart__c = true;
                anewcart.ccrz__ActiveCart__c = false;  
                update anewcart;
                update currentCart;
            }


            update items;
                CCAviPageUtils.buildResponseData(response, true, 
                    new Map<String,Object>{'cartsitems' => items}
                );   

        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response;   
    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult setName(ccrz.cc_RemoteActionContext ctx, String cartId, String name){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            ccrz__E_Cart__c cart = CCPDCCartDAO.getCartOwner(cartId);
            cart.ccrz__Name__c = name;
            update cart;
            response.success = true;

        }catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response; 

    }
    @RemoteAction
    global static ccrz.cc_RemoteActionResult deleteCart(ccrz.cc_RemoteActionContext ctx, String cartId){
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            ccrz__E_Cart__c deletecart = CCPDCCartDAO.getCartById(cartId);
            String newcart = '';
            
            if(String.valueOf(deletecart.ccrz__EncryptedId__c) == String.valueOf(ccrz.cc_CallContext.currCartId)){
                newcart = CCAviCartManager.createCart();
                ccrz__E_Cart__c cart = CCPDCCartDAO.getCart(newcart);
                ccrz__E_ContactAddr__c newshipto;
                ccrz__E_ContactAddr__c newbillto;

                ccrz__E_ContactAddr__c oldshipto = CCFPContactAddressDAO.getAddress(deletecart.ccrz__ShipTo__c);
                ccrz__E_ContactAddr__c oldbillto = CCFPContactAddressDAO.getAddress(deletecart.ccrz__BillTo__c);
                if(oldshipto != null && oldbillto != null){
                    newshipto = oldshipto.clone();
                    newbillto = oldbillto.clone();
                

                newshipto.ccrz__FirstName__c ='firstname';
                newshipto.ccrz__LastName__c ='lastname';
                upsert newshipto;
                upsert newbillto;

                cart.ccrz__BillTo__c = newbillto.Id;
                cart.ccrz__ShipTo__c = newshipto.Id;

                
                }
                cart.ccrz__ActiveCart__c = true;
                update cart;

            }
            //cart.ccrz__Name__c = name;

            delete deletecart;


            CCAviPageUtils.buildResponseData(response, true, 
                    new Map<String,Object>{'newCart' => newcart,'cartId' =>cartId}
            ); 


        }catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false, new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()});
        }
        return response; 

    }

}