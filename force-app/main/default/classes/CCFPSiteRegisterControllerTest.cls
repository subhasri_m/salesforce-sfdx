@isTest
public class CCFPSiteRegisterControllerTest {
    static User thisUser;
    static CCPDCTestUtil util = new CCPDCTestUtil();
    
    @testSetup
    static void testSetup() {        
        thisUser = [SELECT Id,AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        thisUser.Email = 'test@gmail.com';
        update thisUser;
        
        System.debug('thisUser==>'+thisUser.AccountId);
        Account acc = new Account(Id=thisUser.AccountId);
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }
    
    //Added by Likhit
  	  static testmethod void callCreateCustomerBoomiTest() {
         util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
          createAddressySettings('parts');
          
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new AddressyAPIMock(AddressyAPIMockResult.FIND_SUCCESS));

        //CCAviAddressyAPI.FindResponse findResponse = CCAviAddressyAPI.find('DefaultStore', '17022 King James Way', 'US|US|ENG|20877-MD-GAITHERSBURG--WAY-KING_JAMES--17022');
        
          
        String newCustomerJson = '{"addressValidationMessage":"validationmessage","firstName":"Some Name","lastname":"lastName","username":"test@email.com","companyName":"1","primaryPhone":"99999999999","billingAddress.address1":"123 dfgh","billingAddress.address2":"123 hhh","billingAddress.stateCode":"TX","billingAddress.postalCode":"75051","streetNumber":"12345"}';
        
        Test.startTest(); 
        CCFPSiteRegisterController.callCreateCustomerBoomi(ctx,newCustomerJson,'457677');
        CCFPSiteRegisterController.retriveAddress(ctx, 'US|US|B|V212153598');
        CCFPSiteRegisterController.findNationalAccount(ctx, false, 'test2@gmail.com', '234567', '75051');
        Test.stopTest();
    }
    
    static testmethod void sendEmailTest() {
        util.initCallContext();
        User theUser = util.getPortalUser();
        theUser.Email = 'username@email.com';
        theUser.UserName = 'username@email.com'; 
        //theUser.AccountId = acc.Id;
        update theUser;
        Contact theContact = util.getContact();
        
        
        
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Test.startTest();
        Account acc = util.getAccount();//new Account(Id=theUser.AccountId,AccountNumber='212601');
        CCFPSiteRegisterController.sendEmail(ctx, 'username@email.com','234567', '75051', false,'457677');
        CCFPSiteRegisterController.sendEmail(ctx, 'username@email.com','234567', '75051', true,'457677');
        Test.stopTest();
         }
    
    
    static testmethod void updateAccountTest() {
        util.initCallContext();
        
        //ccount(ccrz.cc_RemoteActionContext ctx, String userEmail, String customerNumber, String customerBranch, String locationCode
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Test.startTest(); 
        User usr = util.getPortalUser();
        System.debug('usrName==>'+usr.Username);
        CCFPSiteRegisterController.updateAccount(ctx, usr.Username, '123456', 'Customerbranch','457677', 'Manual');
        Test.stopTest();
        }
    
    /*
     Initialize custom settings data.
    */
    public static void createAddressySettings(String storefront) {
        CCAviAddressySettings__c settings = new CCAviAddressySettings__c();
        settings.Name = storefront;
        settings.Key__c = 'AA11-AA11-AA11-AA11';
        settings.Find_End_Point__c = 'https://services.postcodeanywhere.co.uk/Capture/Interactive/Find/v1.00/json3ex.ws';
        settings.Retrieve_End_Point__c = 'https://services.postcodeanywhere.co.uk/Capture/Interactive/Retrieve/v1.00/json3ex.ws';
        insert settings;
    }
    
    /*
        Enum to tell mock class which data to be returned. 
    */
    public enum AddressyAPIMockResult {FIND_500, FIND_SUCCESS, FIND_ERROR, FIND_THROWEXCEPTION, FIND_UNKNOWN,
                                       RETRIEVE_500, RETRIEVE_SUCCESS, RETRIEVE_ERROR, RETRIEVE_THROWEXCEPTION, RETRIEVE_UNKNOWN,
                                       VALID_ADDRESS, RETRIEVE_SUCCESS2}

    public class AddressyAPIMockException extends Exception {}

    /*
        Mock class to imitate the API call to Addressy
    */
    public class AddressyAPIMock implements HttpCalloutMock {
        AddressyAPIMockResult returnResult {get; set;}

        public AddressyAPIMock(AddressyAPIMockResult result) {
            returnResult = result;
        }

        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {        
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');            
            res.setStatusCode(200);

            String responseBody = '';
            if (returnResult == AddressyAPIMockResult.FIND_500 || returnResult == AddressyAPIMockResult.RETRIEVE_500) {
                res.setStatusCode(500);
            }
            else if (returnResult == AddressyAPIMockResult.FIND_SUCCESS) {
                responseBody = '{"Items":[{"Id":"US|US|B|V212153598|17022","Type":"Address","Text":"17022 King James Way","Highlight":"0-5,6-10","Description":"Gaithersburg MD 20877"}]}';
            }
            else if (returnResult == AddressyAPIMockResult.VALID_ADDRESS) {
                responseBody = VALID_ADDRESS_STRING;
            }
            else if (returnResult == AddressyAPIMockResult.RETRIEVE_SUCCESS) {
                responseBody = RETRIEVE_SUCCESS_STRING;
            }
            else if (returnResult == AddressyAPIMockResult.RETRIEVE_SUCCESS2) {
                responseBody = RETRIEVE_SUCCESS_STRING2;
            }            
            else if (returnResult == AddressyAPIMockResult.FIND_ERROR || returnResult == AddressyAPIMockResult.RETRIEVE_ERROR) {
                responseBody = '{"Items":[{"Error":"2","Description":"Unknown key","Cause":"The key you are using to access the service was not found.","Resolution":"Please check that the key is correct. It should be in the form AA11-AA11-AA11-AA11."}]}';
            }
            else if (returnResult == AddressyAPIMockResult.FIND_THROWEXCEPTION || returnResult == AddressyAPIMockResult.RETRIEVE_THROWEXCEPTION) {
                throw new AddressyAPIMockException('Exception result');
            }
            else if (returnResult == AddressyAPIMockResult.FIND_UNKNOWN || returnResult == AddressyAPIMockResult.RETRIEVE_UNKNOWN) {
                responseBody = '{"foo":"bar"}';
            }
            res.setBody(responseBody);
            return res;
        }        
    }
    
    public static final String VALID_ADDRESS_STRING = '{"Items":[{"Id":"US|US|B|V212153598|17022","Type":"Address","Text":"17022 King James Way","Highlight":"0-5,6-10","Description":"Gaithersburg MD 20877"}]}';
    public static final String RETRIEVE_SUCCESS_STRING = '{"Items":[{"Id":"US|US|B|V212153598","DomesticId":"V212153598","Language":"ENG","LanguageAlternatives":"ENG","Department":"","Company":"","SubBuilding":"","BuildingNumber":"17022","BuildingName":"","SecondaryStreet":"","Street":"King James Way","Block":"","Neighbourhood":"","District":"","City":"Gaithersburg","Line1":"17022 King James Way","Line2":"","Line3":"","Line4":"","Line5":"","AdminAreaName":"Montgomery","AdminAreaCode":"031","Province":"MD","ProvinceName":"Maryland","ProvinceCode":"MD","PostalCode":"20877-2235","CountryName":"United States","CountryIso2":"US","CountryIso3":"USA","CountryIsoNumber":840,"SortingNumber1":"","SortingNumber2":"","Barcode":"","POBoxNumber":"","Label":"17022 King James Way GAITHERSBURG MD 20877-2235 UNITED STATES","Type":"Residential","DataLevel":"Range","Field1":"","Field2":"","Field3":"","Field4":"","Field5":"","Field6":"","Field7":"","Field8":"","Field9":"","Field10":"","Field11":"","Field12":"","Field13":"","Field14":"","Field15":"","Field16":"","Field17":"","Field18":"","Field19":"","Field20":""}]}';
    public static final String RETRIEVE_SUCCESS_STRING2 =  '{"Items":[{"Id":"US|US|B|Y214362453|5119","DomesticId":"Y214362453","Language":"ENG","LanguageAlternatives":"ENG","Department":"","Company":"","SubBuilding":"","BuildingNumber":"5119","BuildingName":"","SecondaryStreet":"","Street":"Highgrove Ln NW","Block":"","Neighbourhood":"","District":"","City":"Rochester","Line1":"5119 Highgrove Ln NW","Line2":"","Line3":"","Line4":"","Line5":"","AdminAreaName":"Olmsted","AdminAreaCode":"109","Province":"MN","ProvinceName":"Minnesota","ProvinceCode":"MN","PostalCode":"55901-2085","CountryName":"United States","CountryIso2":"US","CountryIso3":"USA","CountryIsoNumber":840,"SortingNumber1":"","SortingNumber2":"","Barcode":"","POBoxNumber":"","Label":"5119 Highgrove Ln NW ROCHESTER MN 55901-2085 UNITED STATES","Type":"Residential","DataLevel":"Range","Field1":"","Field2":"","Field3":"","Field4":"","Field5":"","Field6":"","Field7":"","Field8":"","Field9":"","Field10":"","Field11":"","Field12":"","Field13":"","Field14":"","Field15":"","Field16":"","Field17":"","Field18":"","Field19":"","Field20":""}]}';

    
  
}