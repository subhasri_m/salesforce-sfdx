@isTest
public with sharing class CCPDCUserPermissionsControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void constructorTest() {
        util.initCallContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        CCPDCUserPermissionsController controller = null;
        Test.startTest();
        controller = new CCPDCUserPermissionsController();
        Test.stopTest();

        System.assert(controller != null);
        System.assert(controller.jsonData != null);
    }
}