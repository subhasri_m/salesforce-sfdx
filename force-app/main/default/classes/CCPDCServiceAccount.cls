global with sharing class CCPDCServiceAccount extends ccrz.ccServiceAccount {
    // Override and return a WO sharing DAO.
    global override Map<String, Object> initSvcDAO(Map<String, Object> inputData){
        return new Map<String, Object>{ccrz.ccService.SVCDAO => new ccrz.ccServiceDAOWO(inputData)};
    }
}