@isTest
private class CCAviPageUtilsTest {
    
    @isTest static void test_remoteInit() {

        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        
        ccrz.cc_RemoteActionResult result = CCAviPageUtils.remoteInit(ctx);
  
        System.assert(result != NULL);
    }

    @isTest static void test_createMessage() {
        
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ccrz.cc_RemoteActionResult result = CCAviPageUtils.remoteInit(ctx);

        ccrz.cc_bean_Message msg =  CCAviPageUtils.createMessage(ccrz.cc_bean_Message.MessageSeverity.INFO, 'label', ctx, 'testclass');
        result.messages.add(msg);
        System.assert(result != NULL);
        System.assert(result.messages.size() > 0);
    }

    @isTest static void test_buildResposeData() {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ctx.queryParams.put('BranchLoc','BSL');
        ctx.queryParams.put('DCLoc', 'VS');
        ctx.queryParams.put('selectedShippingMethod','fp_shipping_option1');
		ccrz.cc_RemoteActionResult result = CCAviPageUtils.remoteInit(ctx);

        CCAviPageUtils.buildResponseData(result, true, new Map<String, Object>{'ctx' => ctx});

        CCAviPageUtils.buildResponseData(result, null, new Map<String, Object>{'ctx' => ctx});

        System.assert(result != NULL);

    }

    @isTest static void test_combineResults() {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ccrz.cc_RemoteActionResult result1 = CCAviPageUtils.remoteInit(ctx);
        ccrz.cc_RemoteActionResult result2 = CCAviPageUtils.remoteInit(ctx);
        CCAviPageUtils.buildResponseData(result1, true, new Map<String, Object>{'ctx' => ctx});
        CCAviPageUtils.buildResponseData(result2, true, new Map<String, Object>{'ctx' => ctx});

        ccrz.cc_RemoteActionResult result = CCAviPageUtils.combineResults(result1, result2);

        System.assert(result != NULL);
    }
  
}