@isTest
public class CCPDCServiceAccountTest {
    static testmethod void initSvcDAOTest() {

        Map<String,Object> input = new Map<String,Object>();

        Map<String,Object> data = null;

        Test.startTest();
        CCPDCServiceAccount hook = new CCPDCServiceAccount();
        data = hook.initSvcDAO(input);
        Test.stopTest();

        System.assert(data != null);
    }
}