@isTest
public class Test_Salesforce_Request_Trigger{
    static testMethod void deleteOld_Owner_tasks(){
    
    Test.startTest();
User user1 = new User(alias = 'ceo', email='admin@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-88888',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='adminTas@testorg.com', profileid = '00e400000013ttZ');
        insert user1;
User user2 = new User(alias = 'ceo2', email='admin2@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        Salesman_Number__c = '3-99999',
        localesidkey='en_US',
        timezonesidkey='America/Los_Angeles', username='admin2@testorg.com', profileid = '00e400000013ttZ');
        insert user2;

system.runas(user1){
Salesforce_Request__c SFRQ = new Salesforce_Request__c(Supervisor_Requester__c= User1.id, OwnerId = User1.id, Start_Date__c = system.today(), User_Type__c = 'New User' );
insert SFRQ ;

SFRQ.Supervisor_Requester__c= User2.id;
Update SFRQ;
}
 Test.stopTest();
 }
}