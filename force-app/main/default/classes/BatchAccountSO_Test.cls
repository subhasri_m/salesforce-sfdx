@isTest
private class BatchAccountSO_Test {
    
     @testSetup static void setup() { 
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'newUser', Email='newuser1234@sampleeeee.commm', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = p.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='exampletest@batch.so',
                           Create_Accounts__c = true);   
        insert u;
    }
    
    
    

    @isTest static void januaryTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 01, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(01);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void februaryTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 02, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(02);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void marchTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 03, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(03);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void aprilTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 04, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(04);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void mayTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 05, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(05);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void juneTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 06, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(06);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void julyTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 07, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(07);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void augustTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 08, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(08);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void septTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 09, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(09);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void octoberTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 10, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(10);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void novemberTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 11, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(11);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
  
  @isTest static void decemberTest() { 
      
      //User u = [SELECT Id FROM User WHERE Id = '00540000002LWA0'];
      User u2 = [SELECT Id, UserName FROM User WHERE UserName = 'exampletest@batch.so'];
      System.runAs(u2){
        List<Sales_Order_Summary__c> soList = new List<Sales_Order_Summary__c>();
        Integer lastYear = System.Today().year() - 1;
        Date dateLastYear = date.newInstance(lastYear, 12, 01);
      
        Account acc = new Account(Name = 'Test', Type = 'Customer');  
        insert acc;
        Sales_Order_Summary__c so = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so);
        Sales_Order_Summary__c so2 = new Sales_Order_Summary__c(Sales__c = 1344, Order_Date__c = dateLastYear, Account__c = acc.Id);
        soList.add(so2);
        
        insert soList;
        
        Test.startTest();

            BatchAccountSO  batchApex1 = new BatchAccountSO(12);
            Database.QueryLocator ql = batchApex1.start(null);
            batchApex1.execute(null, soList);
            batchApex1.Finish(null);
        
        Test.stopTest();
      }
  }
}