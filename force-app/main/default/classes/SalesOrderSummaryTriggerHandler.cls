public with sharing class SalesOrderSummaryTriggerHandler {
    
    public static boolean runOnce = false;
    
    public static void syncSalesOrderSummary(Map<Id, Sales_Order_Summary__c> oldSOSMap, List<Sales_Order_Summary__c> newSOSList) {
        
        if (runOnce) return;
        runOnce = true;
        
        Set<Id> ownerIdSet = new Set<Id>();
        
        // exclude the integration records
        String informaticaUserId = Fleet_Pride_Common__c.getOrgDefaults().Integration_User_Id__c;
        
        List<Sales_Order_Summary__c> sosList = [select Order_Date__c, Account_Owner_ID__c, Sales_Target__c, Sales_Target__r.Start_Date__c, Sales_Target__r.OwnerId from Sales_Order_Summary__c where id in :newSOSList and ownerId != :informaticaUserId];
        
        List<Sales_Order_Summary__c> sosListAffected = new List<Sales_Order_Summary__c>();
        
        for (Sales_Order_Summary__c sos : sosList) {
            
            if (oldSOSMap != null) {
            
                Sales_Order_Summary__c oldSos = oldSOSMap.get(sos.id);
                
                // if account owner or date is changed
                if (oldSos.Account_Owner_ID__c != sos.Account_Owner_ID__c || oldSos.Order_Date__c != sos.Order_Date__c) {
                    ownerIdSet.add(sos.Account_Owner_ID__c); sosListAffected.add(sos);
                    
                }
                
            } else {
                ownerIdSet.add(sos.Account_Owner_ID__c);
                sosListAffected.add(sos);
            }
        }
        
        if (ownerIdSet.size() > 0) {
            moveSos(ownerIdSet, sosListAffected);
        }
    }
        
    public static void moveSos(Set<Id> ownerIdSet, List<Sales_Order_Summary__c> sosList) {
        
        // only retrieve in the time range
        Date minDate = Date.newInstance(9999, 1, 1);
        Date maxDate = Date.newInstance(1111, 1, 1);
        
        Boolean noOrderDate = true;
        for (Sales_Order_Summary__c sos : sosList) {
            if (sos.Order_Date__c == null) continue;
            noOrderDate = false;
            if (minDate > sos.Order_Date__c) minDate = sos.Order_Date__c;
            if (maxDate < sos.Order_Date__c) maxDate = sos.Order_Date__c;
            
        }
        // do nothing if no order date
        if (noOrderDate) return;
        
        // perpare sales target record first
        List<Sales_Target__c> stList = [select ownerId, Start_Date__c from Sales_Target__c where ownerId in :ownerIdSet and Start_Date__c >= :minDate and Start_Date__c <= :maxDate]; 
        
        Map<String, Sales_Target__c> stListMap = new Map<String, Sales_Target__c>();
        
        for (Sales_Order_Summary__c sos : sosList) {
            
            system.debug('-- Sales_Order_Summary__c Order_Date__c:' + sos.Order_Date__c);
            
            if (sos.Order_Date__c == null) continue;
            
            Integer numberOfDays = Date.daysInMonth(sos.Order_Date__c.year(), sos.Order_Date__c.month());
            
            Date fromDate = Date.newInstance(sos.Order_Date__c.year(), sos.Order_Date__c.month(), 1);
            Date toDate = Date.newInstance(sos.Order_Date__c.year(), sos.Order_Date__c.month(), numberOfDays);
            
            Boolean stThere = false;
            // check per owner per month if there is a st, if not create one
            for (Sales_Target__c st : stList) {
                if (st.Start_Date__c >= fromDate && st.Start_Date__c <= toDate && st.OwnerId == sos.Account_Owner_ID__c) {  stThere = true;
                   
                    break;
                }
            }
            
            // if not existing
            if (!stThere) {
                String strKey = sos.Account_Owner_ID__c + String.valueOf(fromDate);
                
               // 12/6- Surabhi, Do not create Sales Targets, Sales Target will be loaded from Pentaho
               /* if (stListMap.get(strKey) == null) {
                        
                    Sales_Target__c newSt = new Sales_Target__c();
                    newSt.ownerId = sos.Account_Owner_ID__c;
                    newSt.Start_Date__c = fromDate;
                    
                    stListMap.put(strKey, newSt);
                }
               */
               
                
            }
        }
        
        if (!stListMap.isEmpty()) {
            List<Sales_Target__c> stListNew = new List<Sales_Target__c>(); stListNew.addAll(stListMap.values());
            
            
            system.debug('-- create stListNew:' + stListNew);
            
          //  insert stListNew;
            
            stList.addAll(stListNew);
        }
        
        system.debug('-- all stList:' + stList);
        
        List<Sales_Order_Summary__c> sosListUpdate = new List<Sales_Order_Summary__c>();
        
        system.debug('-- all sosList:' + sosList);
        
        for (Sales_Order_Summary__c sos : sosList) {
            if (sos.Order_Date__c != null) {
                // find the sales target with that owner and that month
                Integer numberOfDays = Date.daysInMonth(sos.Order_Date__c.year(), sos.Order_Date__c.month());
                Date fromDate = Date.newInstance(sos.Order_Date__c.year(), sos.Order_Date__c.month(), 1);
                Date toDate = Date.newInstance(sos.Order_Date__c.year(), sos.Order_Date__c.month(), numberOfDays);
            
                for (Sales_Target__c st : stList) {
                    if (st.Start_Date__c >= fromDate && st.Start_Date__c <= toDate && st.OwnerId == sos.Account_Owner_ID__c && sos.Sales_Target__c != st.id) {
                        sos.Sales_Target__c = st.id; sosListUpdate.add(sos);break;
                       
                        
                    }
                }
            }
        }
        
        system.debug('-- update sosListUpdate:' + sosListUpdate);
        
        if (sosListUpdate.size() > 0) {
            
            // don't run trigger on this update
            SalesOrderSummaryTriggerHandler.runOnce = true;
            update sosListUpdate;
            system.debug('-- update complete:' + sosListUpdate);
        }
        
    }
}