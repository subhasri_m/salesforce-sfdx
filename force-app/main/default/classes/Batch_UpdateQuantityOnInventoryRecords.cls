global class Batch_UpdateQuantityOnInventoryRecords  implements Database.Batchable<sObject>,schedulable {
    
    Integer totalCount; 
    
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new Batch_UpdateQuantityOnInventoryRecords());
    }    
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        
            
        String query ='';   
        If(Test.isRunningTest()){
            query = 'select id,ccrz__QtyAvailable__c,lastmodifiedby.name,lastmodifieddate from ccrz__E_ProductInventoryItem__c where ccrz__QtyAvailable__c !=0'; 
        }else{
            query = 'select id,ccrz__QtyAvailable__c,lastmodifiedby.name,lastmodifieddate from ccrz__E_ProductInventoryItem__c where lastmodifieddate <>TODAY and  ccrz__QtyAvailable__c !=0 '; // Limit 10 
        }    
        System.debug('Query--> '+query );
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<ccrz__E_ProductInventoryItem__c>scope){
        
        List<ccrz__E_ProductInventoryItem__c> updateInventory = new List<ccrz__E_ProductInventoryItem__c>(); 
       
            for (ccrz__E_ProductInventoryItem__c Inventory : Scope)
            {
                ccrz__E_ProductInventoryItem__c invent = new ccrz__E_ProductInventoryItem__c();
                invent.id = Inventory.id; 
                invent.ccrz__QtyAvailable__c = 0;
                updateInventory.add(invent);
            }
            
            update updateInventory;
 
        
    }
    
    global void finish(Database.BatchableContext BC){
        
        
    }
    
    
    
}