public class PowerBIControllerExtensionUser extends PowerBISilentOAuthController {

    private final User objUser;
       Public String FederationID {get;set;}

    /**
    * Generic constructor
    */
    public PowerBIControllerExtensionUser (ApexPages.StandardController stdController) {       
        this.objUser = (User)stdController.getRecord();
      getFederationID();
        
        
    }
    
    Public void getFederationID()
    {
      	User LoggedinUser  = [select Id,FederationIdentifier from user where id =:UserInfo.getUserId() limit 1] ;
        List<WinRateFederationFilter__mdt> CustomMeta  = [select id, FederationIdentifierFilter__c from WinRateFederationFilter__mdt limit 1];
        String FederationIdFilters;
        if (!CustomMeta.isEmpty()){
            FederationIdFilters = CustomMeta[0].FederationIdentifierFilter__c;
        }
        System.debug('FederationIdFilters-->'+FederationIdFilters);
        System.debug('FederationIdentifier-->'+LoggedinUser.FederationIdentifier);
        List<String> FederationIdFiltersList = FederationIdFilters.split(',');
        if(FederationIdFiltersList.contains(LoggedinUser.FederationIdentifier))
        {
           FederationID = System.label.WinRateFederationID;
        }
        else
        {
            FederationID = LoggedinUser.FederationIdentifier;
        }
        
        System.debug('FederationID-->'+FederationID);
        
    }
    
    
}