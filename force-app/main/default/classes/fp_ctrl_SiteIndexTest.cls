@isTest
public with sharing class fp_ctrl_SiteIndexTest {
    public String declarationXml { get { return '<?xml version="1.0" encoding="UTF-8"?>\n'; } }
    public List<fp_bean_SiteMap> cats {get;set;}
    public final String HTTPS = 'https://';
    
    static testmethod void getAllowedPricelistsTest(){
        
        fp_ctrl_CatSiteMap catsite= new fp_ctrl_CatSiteMap();
        Map<Id,ccrz__E_PriceList__c> returnResults = new Map<Id,ccrz__E_PriceList__c>();
        ccrz__E_AccountGroup__c ccaccgrp =new ccrz__E_AccountGroup__c();
        ccaccgrp.ccrz__AccountGroupID__c='002-0015920-000';
        ccaccgrp.ccrz__DataId__c='';
        ccaccgrp.ccrz__Desc__c='002-0015920-000-AMEN DIESEL INC';
        ccaccgrp.ccrz__PriceListSelectionMethod__c='Best Price';
        ccaccgrp.Name='002-0015920-000-AMEN DIESEL INC';
        insert ccaccgrp;
        
        ccrz__E_PriceList__c ccpricelist= new ccrz__E_PriceList__c();
        ccpricelist.ccrz__CurrencyISOCode__c='USD';
        ccpricelist.ccrz__Desc__c='FP Base Price List';
        ccpricelist.ccrz__Enabled__c=true;
        ccpricelist.ccrz__PricelistId__c='FP_Base_Price_List';
        ccpricelist.ccrz__Storefront__c='parts';
        insert ccpricelist;
        
        ccrz__E_AccountGroupPriceList__c acgrpList = new ccrz__E_AccountGroupPriceList__c();
        acgrpList.ccrz__Pricelist__c=ccpricelist.id;
        acgrpList.ccrz__AccountGroup__c=ccaccgrp.id;
        insert acgrpList ;
        String storefrontName = ccrz.cc_CallContext.storefront;
        
        Test.startTest();
        returnResults = fp_ctrl_SiteIndex.getAllowedPricelists(storefrontName ,ccaccgrp.id);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    static testmethod void retrieveCategoriesByProductsTest(){
        
        List<fp_bean_SiteMap> returnResults =new  List<fp_bean_SiteMap>();
        ccrz__E_Product__c ccp =new ccrz__E_Product__c ();
        ccp.CAT_Part_Number__c='NPC-105322';
        ccp.ccrz__CoolerpakFlag__c  =false;
        ccp.ccrz__DryiceFlag__c =false;
        ccp.ccrz__FilterData__c='a8f1W000000LDf7=NEAPCO|a8f1W000000LDf8=Driveline Accessory|a8f1W000000LEFF=2011|a8f1W000000fxio=Powertrain|a8f1W000000fxip=Driveline|a8f1W000000fxyr=105322|a8f1W000000fxys=105322|a8f1W000000fxyt=105322|';
        ccp.ccrz__GroundFlag__c =false;
        ccp.ccrz__HazardousFlag__c =false;  
        ccp.ccrz__InventoryType__c  ='STANDARD';
        ccp.ccrz__Is_Subscription_Auto_Renewal__c=false;
        ccp.ccrz__Is_Subscription_Available__c =false;
        ccp.ccrz__Is_Subscription_Only__c   = false;
        ccp.ccrz__LeadTime__c   =1.0;
        ccp.ccrz__LongDescRT__c =' NON SPLINED END YOKE<br><br><br><b>Cross Reference Information</b> <br><br>105322<br>';
        ccp.ccrz__OvernightFlag__c  =false;
        ccp.ccrz__ProductId__c  ='NPC-105322';
        ccp.ccrz__ProductIndexStatus__c ='Current';
        ccp.ccrz__ProductStatus__c ='Released';
        ccp.ccrz__ProductType__c ='Product';
        ccp.ccrz__Quantityperunit__c  =1.0;  
        ccp.ccrz__Sequence__c=29071.0 ;
        ccp.ccrz__SKU__c='NPC-105322';
        
        insert ccp;
        
        ccrz__E_Category__c cccategoryobj= new ccrz__E_Category__c();
        cccategoryobj.ccrz__CategoryID__c   ='Axle & Hubs';
        cccategoryobj.ccrz__LongDesc__c ='Axle & Hubs';
        cccategoryobj.ccrz__SEOId__c ='disc-brake-pad';
        cccategoryobj.ccrz__Sequence__c =500;  
        cccategoryobj.Name  =' Disc Brake Pad';
        cccategoryobj.Spec_Group__c =' 006-03-007';
        insert cccategoryobj;
        
        ccrz__E_Product__c prodobj=new ccrz__E_Product__c();
        prodobj.CAT_Part_Number__c='POW-90-12393';
        prodobj.ccrz__CoolerpakFlag__c = false;
        prodobj.ccrz__DryiceFlag__c = false;
        prodobj.ccrz__SKU__c=  'POW-9012393';
        
        insert prodobj;
        
        ccrz__E_ProductCategory__c ccpc = new ccrz__E_ProductCategory__c();
        ccpc.ccrz__Category__c =cccategoryobj.id;
        ccpc.ccrz__IsCanonicalPath__c=false;
        ccpc.ccrz__ProductCategoryId__c='POW-9012393-Spring Attaching Parts';
        ccpc.ccrz__Product__c =prodobj.id;
        ccpc.ccrz__Sequence__c = 500.0;
        ccpc.ccrz__StartDate__c =system.today();
        ccpc.ccrz__EndDate__c=system.today();
        insert ccpc;
        
        Test.startTest();
        returnResults = fp_ctrl_SiteIndex.retrieveCategoriesByProducts();
        Test.stopTest();
        System.assert(returnResults != null);
        
    } 
    static testmethod void entitledProductsByAccountGroupTest(){
        String storefrontName = ccrz.cc_CallContext.storefront; 
        Map<String, String> returnResults = new Map<String, String>();
        //   Map<String, String> retValues= new Map<String, String>();
        
        // List<fp_bean_SiteMap> returnResults =new List<fp_bean_SiteMap>();
        ccrz__E_PriceList__c ccpricelist= new ccrz__E_PriceList__c();
        ccpricelist.ccrz__CurrencyISOCode__c='USD';
        ccpricelist.ccrz__Desc__c='FP Base Price List';
        ccpricelist.ccrz__Enabled__c=true;
        ccpricelist.ccrz__PricelistId__c='FP_Base_Price_List';
        ccpricelist.ccrz__Storefront__c='parts';
        insert ccpricelist;
        
        ccrz__E_Product__c ccp =new ccrz__E_Product__c ();
        ccp.CAT_Part_Number__c='NPC-105322';
        ccp.ccrz__CoolerpakFlag__c  =false;
        ccp.ccrz__DryiceFlag__c =false;
        ccp.ccrz__FilterData__c='a8f1W000000LDf7=NEAPCO|a8f1W000000LDf8=Driveline Accessory|a8f1W000000LEFF=2011|a8f1W000000fxio=Powertrain|a8f1W000000fxip=Driveline|a8f1W000000fxyr=105322|a8f1W000000fxys=105322|a8f1W000000fxyt=105322|';
        ccp.ccrz__GroundFlag__c =false;
        ccp.ccrz__HazardousFlag__c =false;  
        ccp.ccrz__InventoryType__c  ='STANDARD';
        ccp.ccrz__Is_Subscription_Auto_Renewal__c=false;
        ccp.ccrz__Is_Subscription_Available__c =false;
        ccp.ccrz__Is_Subscription_Only__c   = false;
        ccp.ccrz__LeadTime__c   =1.0;
        ccp.ccrz__LongDescRT__c =' NON SPLINED END YOKE<br><br><br><b>Cross Reference Information</b> <br><br>105322<br>';
        ccp.ccrz__OvernightFlag__c  =false;
        ccp.ccrz__ProductId__c  ='NPC-105322';
        ccp.ccrz__ProductIndexStatus__c ='Current';
        ccp.ccrz__ProductStatus__c ='Released';
        ccp.ccrz__ProductType__c ='Product';
        ccp.ccrz__Quantityperunit__c  =1.0;  
        ccp.ccrz__Sequence__c=29071.0 ;
        ccp.ccrz__SKU__c='NPC-105322';
        insert ccp;
        
        ccrz__E_Product__c ccp1 =new ccrz__E_Product__c ();
        ccp1.CAT_Part_Number__c='NPC-105329';
        ccp1.ccrz__CoolerpakFlag__c  =false;
        ccp1.ccrz__DryiceFlag__c =false;
        ccp1.ccrz__FilterData__c='a8f1W000000LDf7=NEAPCO|a8f1W000000LDf8=Driveline Accessory|a8f1W000000LEFF=2011|a8f1W000000fxio=Powertrain|a8f1W000000fxip=Driveline|a8f1W000000fxyr=105322|a8f1W000000fxys=105322|a8f1W000000fxyt=105322|';
        ccp1.ccrz__GroundFlag__c =false;
        ccp1.ccrz__HazardousFlag__c =false;  
        ccp1.ccrz__InventoryType__c  ='STANDARD';
        ccp1.ccrz__Is_Subscription_Auto_Renewal__c=false;
        ccp1.ccrz__Is_Subscription_Available__c =false;
        ccp1.ccrz__Is_Subscription_Only__c   = false;
        ccp1.ccrz__LeadTime__c   =1.0;
        ccp1.ccrz__LongDescRT__c =' NON SPLINED END YOKE<br><br><br><b>Cross Reference Information</b> <br><br>105322<br>';
        ccp1.ccrz__OvernightFlag__c  =false;
        ccp1.ccrz__ProductId__c  ='NPC-105328';
        ccp1.ccrz__ProductIndexStatus__c ='Current';
        ccp1.ccrz__ProductStatus__c ='Not Orderable';
        ccp1.ccrz__ProductType__c ='Product';
        ccp1.ccrz__Quantityperunit__c  =1.0;  
        ccp1.ccrz__Sequence__c=29071.0 ;
        ccp1.ccrz__SKU__c='NPC-105382';
        insert ccp1;
        
        // Map<Id, ccrz__E_PriceList__c> allowedPricelists = getAllowedPricelists(storefrontName, accountGroupId);         
        
        ccrz__E_PriceListItem__c ccpricelisitem = new ccrz__E_PriceListItem__c();
        ccpricelisitem.ccrz__EndDate__c =system.today();
        ccpricelisitem.ccrz__PricelistItemId__c ='Test Id';
        ccpricelisitem.ccrz__Pricelist__c = ccpricelist.id;
        ccpricelisitem.ccrz__Price__c =450.0;
        ccpricelisitem.ccrz__Product__c=ccp.id;
        ccpricelisitem.ccrz__RecurringPrice__c = false;
        ccpricelisitem.ccrz__StartDate__c =system.today();
        insert ccpricelisitem;
        
        ccrz__E_PriceListItem__c ccpricelisitem1 = new ccrz__E_PriceListItem__c();
        ccpricelisitem1.ccrz__EndDate__c =system.today();
        ccpricelisitem1.ccrz__PricelistItemId__c ='Test Idi';
        ccpricelisitem1.ccrz__Pricelist__c = ccpricelist.id;
        ccpricelisitem1.ccrz__Price__c =458.0;
        ccpricelisitem1.ccrz__Product__c=ccp1.id;
        ccpricelisitem1.ccrz__RecurringPrice__c = false;
        ccpricelisitem1.ccrz__StartDate__c =system.today();
        insert ccpricelisitem1;
        
        ccrz__E_AccountGroup__c ccaccgrp =new ccrz__E_AccountGroup__c();
        ccaccgrp.ccrz__AccountGroupID__c='002-0015920-000';
        ccaccgrp.ccrz__DataId__c='';
        ccaccgrp.ccrz__Desc__c='002-0015920-000-AMEN DIESEL INC';
        ccaccgrp.ccrz__PriceListSelectionMethod__c='Best Price';
        ccaccgrp.Name='002-0015920-000-AMEN DIESEL INC';
        insert ccaccgrp;
        
        Map<Id, ccrz__E_PriceList__c> allowedPricelists = fp_ctrl_SiteIndex.getAllowedPricelists(storefrontName, ccaccgrp.ccrz__AccountGroupID__c);         
        Map<String, String> retValues1= new Map<String, String>();
        for(ccrz__E_PriceListItem__c pli:[
            SELECT Id, ccrz__Product__r.ccrz__SKU__c 
            FROM ccrz__E_PriceListItem__c
            WHERE
            ccrz__Pricelist__c IN :allowedPricelists.values() AND
            (ccrz__Product__r.ccrz__ProductStatus__c = 'Released' OR ccrz__Product__r.ccrz__ProductStatus__c = 'Not Orderable' )AND
            ccrz__Product__r.ccrz__ProductType__c != 'Coupon' AND
            ccrz__StartDate__c <=  TODAY AND
            ccrz__EndDate__c >=  TODAY
            LIMIT :10000
        ]){
            if(retValues1.containsKey(ccpricelisitem.ccrz__Product__r.ccrz__SKU__c)){
                
            }else{
                retValues1.put(ccpricelisitem.ccrz__Product__r.ccrz__SKU__c, ccpricelisitem.ccrz__Product__r.ccrz__SKU__c);
            }
        }
        //  insert retValues1.values();
        
        Test.startTest();
        ccrz.cc_CallContext.currAccountGroup = ccaccgrp;
        returnResults = fp_ctrl_SiteIndex.entitledProductsByAccountGroup(storefrontName);
        Test.stopTest();
        System.assert(returnResults != null);
    }
    
    static testmethod void baseURLTest(){
        fp_ctrl_SiteIndex objCtrlSiteIndex = new fp_ctrl_SiteIndex();
        Map<String,Object> mapStrVsObj = new  Map<String,Object>();
        
        mapStrVsObj.put('Site_Secure_Domain__c', (Object)'https://www.test.com');
        ccrz.cc_CallContext.storeFrontSettings = mapStrVsObj;
        Test.startTest();
        String baseURL = objCtrlSiteIndex.baseURL;
        String declarationXml = objCtrlSiteIndex.declarationXml;// = '<?xml version="1.0" encoding="UTF-8"?>\n';
        Test.stopTest();
        System.assert(objCtrlSiteIndex.baseURL != null);
    }  
}