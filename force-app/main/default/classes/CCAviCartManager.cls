public with sharing class CCAviCartManager {

    private static final Integer API_VERSION = 5;
    public static final String ENCRYPTED_ID_KEY = 'encryptedId';
    public static final String CART_ITEMS_KEY = 'ECartItemsS';
    public static final String PRODUCT_KEY = 'product';
    public static final String SFID_KEY = 'sfid';
    public static final String PRODUCT_ITEM_KEY = 'productItem';
    public static final String QUANTITY_KEY = 'quantity';
    public static final String SKU_KEY = 'sku';
    public static final String PRODUCT_ID_KEY = 'id';

    public static String createCart() {
        String data = null;

        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => API_VERSION 
        };
  
        Map<String,Object> response = null;
  
        try {
            response = ccrz.ccAPICart.create(request);
            Boolean isSuccess = (Boolean) response.get(ccrz.ccAPI.SUCCESS);
            if (isSuccess) {
                data = (String)response.get(ccrz.ccApiCart.CART_ENCID);
            }
        }
        catch(Exception e) {
            System.debug(e);
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            throw new CartManagerException(e.getMessage());
        }
        return data;
    }

    public static void addToCart(String cartId, String theId, String sku, Integer qty) {
        addToCart(cartId, new List<Object> {new Map<String, Object> {PRODUCT_ID_KEY => theId, SKU_KEY => sku, QUANTITY_KEY => qty}});
    }

    public static void addToCart(String cartId, List<Object> items) {
        String errorMessage = null;

        List<ccrz.ccApiCart.LineData> newLines = new List<ccrz.ccApiCart.LineData>();
        for (Object o : items) {
            Map<String, Object> item = (Map<String, Object>) o;
            ccrz.ccApiCart.LineData newLine = new ccrz.ccApiCart.LineData();
            newLine.productId = (String)item.get(PRODUCT_ID_KEY);
            newLine.sku = (String)item.get(SKU_KEY);
            newLine.quantity = (Integer)item.get(QUANTITY_KEY);
            newLines.add(newLine);
        }

        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => API_VERSION, 
            ccrz.ccApiCart.CART_ENCID => cartId,
            ccrz.ccApiCart.LINE_DATA => newLines
        };
  
        Map<String,Object> response = null;
  
        try {
            response = ccrz.ccAPICart.addTo(request);
            Boolean isSuccess = (Boolean) response.get(ccrz.ccAPI.SUCCESS);
            if (!isSuccess) {
                List<ccrz.cc_bean_Message> messageList = (List<ccrz.cc_bean_Message>) response.get(ccrz.ccApi.MESSAGES);
                String message = 'Add to cart failed.';
                if (messageList != null && !messageList.isEmpty()) {
                    for (ccrz.cc_bean_Message m : messageList) {
                        message += ' ' + m.message;
                    }
                }
                errorMessage = message;
            }
        }
        catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            throw new CartManagerException(e.getMessage());
        }

        if (errorMessage != null) {
            System.debug(System.LoggingLevel.ERROR, errorMessage);
            throw new CartManagerException(errorMessage);            
        }

    }

    public static void removeFromCart(String cartId, String item) {
        removeFromCart(cartId, new Set<String>{item});
    }

    public static void removeFromCart(String cartId, Set<String> items) {
        String errorMessage = null;

        List<ccrz.ccApiCart.LineData> removedLines = new List<ccrz.ccApiCart.LineData>();
        for (String i : items) {
            ccrz.ccApiCart.LineData removeLine = new ccrz.ccApiCart.LineData();
            removeLine.sfid = i;
            removedLines.add(removeLine);
        }

        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => API_VERSION,
            ccrz.ccApiCart.CART_ENCID => cartId,
            ccrz.ccApiCart.LINE_DATA => removedLines
        };

        Map<String,Object> response = null;
  
        try {
            response = ccrz.ccAPICart.removeFrom(request);
            Boolean isSuccess = (Boolean) response.get(ccrz.ccAPI.SUCCESS);
            if (!isSuccess) {
                List<ccrz.cc_bean_Message> messageList = (List<ccrz.cc_bean_Message>) response.get(ccrz.ccApi.MESSAGES);
                String message = 'Remove from cart failed.';
                if (messageList != null && !messageList.isEmpty()) {
                    for (ccrz.cc_bean_Message m : messageList) {
                        message += ' ' + m.message;
                    }
                }
                errorMessage = message;
            }
        }
        catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            throw new CartManagerException(e.getMessage());
        }

        if (errorMessage != null) {
            System.debug(System.LoggingLevel.ERROR, errorMessage);
            throw new CartManagerException(errorMessage);            
        }
    }

    public static Map<String, Object> getActiveCart() {
        Map<String, Object> data = null;
        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => API_VERSION, 
            ccrz.ccAPICart.BYOWNER => ccrz.cc_CallContext.currUserId,
            ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_M,
            ccrz.ccAPICart.ACTIVECART => true
        };
  
        Map<String,Object> response = null;
  
        try {            
            System.debug(request);
            response = ccrz.ccAPICart.fetch(request);
            System.debug(response);
            Boolean isSuccess = (Boolean) response.get(ccrz.ccAPI.SUCCESS);
            if (isSuccess) {
                List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) response.get(ccrz.ccAPICart.CART_OBJLIST);
                if (outputCartList != null && !outputCartList.isEmpty()) {
                    data = outputCartList[0];
                    List<Map<String, Object>> productList = (List<Map<String, Object>>) response.get(ccrz.ccAPIProduct.PRODUCTLIST);
                    if (productList != null && !productList.isEmpty()) {
                        addProductToCart(data, productList);
                    }
                }
            }
        }
        catch(Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            throw new CartManagerException(e.getMessage());
        }
        return data;
    }

    public static String getActiveCartId() {
        String cartId = null;
        Map<String, Object> data = getActiveCart();
        if (data != null) {
            cartId = (String) data.get(ENCRYPTED_ID_KEY);
        }
        else {
            cartId = createCart();
        }
        return cartId;
    }

    public static Map<String, Object> getCart(String theId) {
        Map<String, Object> data = null;

        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => API_VERSION, 
            ccrz.ccAPICart.CART_ENCID => theId
        };
  
        Map<String,Object> response = null;
  
        try {
            response = ccrz.ccAPICart.fetch(request);
            Boolean isSuccess = (Boolean) response.get(ccrz.ccAPI.SUCCESS);
            if (isSuccess) {
                List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) response.get(ccrz.ccAPICart.CART_OBJLIST);
                if (outputCartList != null && !outputCartList.isEmpty()) {
                    data = outputCartList[0];
                    List<Map<String, Object>> productList = (List<Map<String, Object>>) response.get(ccrz.ccAPIProduct.PRODUCTLIST);
                    if (productList != null && !productList.isEmpty()) {
                        addProductToCart(data, productList);
                    }
                }
            }
        }
        catch(Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            throw new CartManagerException(e.getMessage());
        }
        return data;
    }

    private static void addProductToCart(Map<String, Object> cart, List<Map<String, Object>> productList) {
        Map<String, Map<String, Object>> productMap = new Map<String, Map<String, Object>>();
        for (Map<String, Object> m : productList) {
            String key = (String) m.get(SFID_KEY);
            productMap.put(key, m);
        }
        List<Map<String, Object>> items = (List<Map<String, Object>>) cart.get(CART_ITEMS_KEY);
        if(items != null) {
            for (Map<String, Object> item : items) {
                String key = (String) item.get(PRODUCT_KEY);
                Map<String, Object> productItem = productMap.get(key);
                item.put(PRODUCT_ITEM_KEY, productItem);
            }
        }
    }

    public static Map<String, Object> getXLCart(String theId) {

        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => API_VERSION, 
            ccrz.ccAPI.SIZING => new Map<String, Object>{
                ccrz.ccAPICart.ENTITYNAME => new Map<String, Object>{
                    ccrz.ccAPI.SZ_DATA => ccrz.ccAPI.SZ_XL
                }
            },
            ccrz.ccAPICart.CART_ENCID => theId
        };
  
        Map<String,Object> response = null;
  
        try {
            response = ccrz.ccAPICart.fetch(request);
        }
        catch(Exception e) {
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            throw new CartManagerException(e.getMessage());
        }
        return response;
    }
    
    public static String cloneCart(String theId) {
        String data = null;

        Map<String, Object> request = new Map<String,Object>{
            ccrz.ccApi.API_VERSION => API_VERSION, 
            ccrz.ccAPICart.CART_ID => theId
        };
  
        Map<String,Object> response = null;
  
        try {
            response = ccrz.ccAPICart.cloneCart(request);
            Boolean isSuccess = (Boolean) response.get(ccrz.ccAPI.SUCCESS);
            if (isSuccess) {
                data = (String)response.get(ccrz.ccApiCart.CART_ENCID);
            }
        }
        catch(Exception e) {
            System.debug(e);
            System.debug(System.LoggingLevel.ERROR, e.getMessage());
            throw new CartManagerException(e.getMessage());
        }
        return data;
    }

    public class CartManagerException extends Exception {}
}