@isTest
private class CCPDCMyAccountAdminHelperTest {
	
    static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();
        }
    }

    static testmethod void fetchContactListTest() {
        util.initCallContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        List<CCPDCMyAccountAdminHelper.CCContact> contacts = null;
        Test.startTest();
        contacts = CCPDCMyAccountAdminHelper.fetchContactList(ccrz.cc_CallContext.currAccountId);
        Test.stopTest();

        System.assert(contacts != null);
        System.assertEquals(1, contacts.size());
    }

    static testmethod void getPermissionsForAccountTest() {
        util.initCallContext();

        List<CC_FP_Contract_Account_Permission_Matrix__c> data = new List<CC_FP_Contract_Account_Permission_Matrix__c> {
            CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true)
        };
        insert data;

        List<CC_FP_Contract_Account_Permission_Matrix__c> permissions = null;
        Test.startTest();        
        permissions = CCPDCMyAccountAdminHelper.getPermissionsForAccount(new List<Id>{ccrz.cc_CallContext.currAccountId});
        Test.stopTest();

        System.assert(permissions != null);
        System.assertEquals(1, permissions.size());
    }

    static testmethod void updatePermissionsTest() {
        util.initCallContext();

        CC_FP_Contract_Account_Permission_Matrix__c data = CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true);
        insert data;

        Test.startTest();
        data.Is_Admin__c = false;
        CCPDCMyAccountAdminHelper.updatePermissions(data);
        Test.stopTest();

        System.assert(!data.Is_Admin__c);
    }

    static testmethod void insertPermissionsTest() {
        util.initCallContext();

        CC_FP_Contract_Account_Permission_Matrix__c data = CCFPContactAccountPermMatrixDAOTest.createPermissions(ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id, true);

        Test.startTest();
        CCPDCMyAccountAdminHelper.insertPermissions(data, ccrz.cc_CallContext.currAccountId, ccrz.cc_CallContext.currContact.Id);
        Test.stopTest();

        System.assert(data.Id != null);
    }

    static testmethod void contactTest() {
        util.initCallContext();

        Contact con = new Contact(
            FirstName = 'Test',
            Lastname = 'Contact',
            Accountid = ccrz.cc_CallContext.currAccountId,
            Email = 'test.contact@test.com'
        );

        CCPDCMyAccountAdminHelper.insertContact(con);
        System.assert(con.Id != null);
        CCPDCMyAccountAdminHelper.updateContact(con);
        System.assert(con.Id != null);
        Contact con1 = CCPDCMyAccountAdminHelper.queryContactById(con.Id);
        System.assertEquals(con.Id, con1.Id);
        CCPDCMyAccountAdminHelper.deleteContact(con);
        List<Contact> cons = [Select Id, IsDeleted from Contact WHERE Id = :con.Id AND IsDeleted = false];
        System.assert(cons != null && cons.isEmpty());

    }

    static testmethod void userTest() {
        util.initCallContext();

        Contact con = new Contact(
            FirstName = 'Test',
            Lastname = 'Contact',
            Accountid = ccrz.cc_CallContext.currAccountId,
            Email = 'test.contact@test.com'
        );
        insert con;

        Profile userProfile = util.getCustomerCommunityProfile();

        User u = new User(
            FirstName         = 'Test',
            LastName          = 'Contact',
            Email             = 'test.contact@test.com',
            Alias             = 'test',
            Username          = 'test.contact@test.com',
            LocaleSidKey      = 'en_US',
            TimeZoneSidKey    = 'GMT',
            ProfileID         = userProfile.id,
            LanguageLocaleKey = 'en_US',
            EmailEncodingKey  = 'UTF-8',
            ContactId         = con.id
        );

        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            CCPDCMyAccountAdminHelper.insertUser(u);
            System.assert(u.Id != null);
            u.LastName = 'User';
            CCPDCMyAccountAdminHelper.updateUser(u);
            System.assert(u.Id != null);
            CCPDCMyAccountAdminHelper.assignProfileToUser(userProfile.Id, u);
            System.assert(u.Id != null);
        }

    }

    static testmethod void getShippingAddressesTest() {
        util.initCallContext();

        Map<Id, ccrz__E_AccountAddressBook__c> data = null;

        Test.startTest();
        data = CCPDCMyAccountAdminHelper.getShippingAddresses(new Set<Id>{ccrz.cc_CallContext.currAccountId});
        Test.stopTest();

        System.assert(data != null);
        System.assertEquals(1, data.size());
    }
	
}