/** ------------------------------------------------------------------------------------------------------
 * @Description - Used to make api call to Addressy for retrieving matching US address for partial address given & Retrieve county
 *
 * @Author      
 * @Date        Sep 2017
 *-----------------------------------------------------------------------------------------------------*/
public with sharing class CCAviAddressyAPI {

    /**
     * @Description - Initial Call to find matching Addressy addresses
     *
     * @Author  
     * @Date    Sep 2017
     *
     * @param   String 
     * @param   String  
     * @return  FindResponse
     **/
    public static FindResponse find(String storefront, String address) {
        return find(storefront, address, null);
    }
    
    /**
     * @Description - Subsequent Call to find matching Addressy addresses inside a particular location like 
     *                Building/Apt, in container will be the ID returned, in Initial call or subsequent calls 
     *                 to pinpoint the final address being searched.
     *
     * @Author  
     * @Date    Sep 2017
     *
     * @param   String 
     * @param   String
     * @param   String
     * @return  FindResponse
     **/
    public static FindResponse find(String storefront, String address, String container) {
        System.debug('Entered CCAviAddressyAPI.find(); Storefront: ' + storefront + '; Address: ' + address + '; Container: ' + container);

        FindResponse response = new FindResponse();

        try {
            // read Addressy config data from settings for this store front
            CCAviAddressySettings__c settings =  CCAviAddressySettings__c.getInstance(storefront);
            if (settings == null) {                
                response.success = false;
                response.message  = 'Invalid store front';
                System.debug('Exiting CCAviAddressyAPI.find(); Response: ' + JSON.serialize(response));
                return response;
            }
             
            CCAviHttpUtil httpRequest = new CCAviHttpUtil();
            httpRequest.method = CCAviHttpUtil.REQUEST_TYPE.GET;
            httpRequest.requestContentType = CCAviHttpUtil.CONTENT_TYPE.JSON;
            httpRequest.endpoint = settings.Find_End_Point__c;
            httpRequest.addQueryParameter('Key', settings.Key__c);
            httpRequest.addQueryParameter('Countries', 'US');
            httpRequest.addQueryParameter('Text', address);
            if (container != null) {
                httpRequest.addQueryParameter('Container', container);
            }

            HttpResponse httpResponse = httpRequest.submitRequest();

            if (httpResponse.getStatusCode() == 200) {
                String responseBody =  httpResponse.getBody(); 
                System.debug(responseBody);
                Map<String, Object> topObject = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
                // response should have key Items
                if (topObject.containsKey('Items')) {
                    List<Object> listItems = (List<Object>)topObject.get('Items');
                    if (listItems.size() > 0) {
                        for (Object obj : listItems) {
                            Map<String, Object> item = (Map<String, Object>)obj;
                            // if the response has Error key then an error has occured.
                            if (item.containsKey('Error')) {
                                response.message = JSON.serialize(item);
                                break;
                            }

                            FindItem findItem = new FindItem();
                            findItem.Id = (String)item.get('Id');
                            findItem.Type = (String)item.get('Type');
                            findItem.Text = (String)item.get('Text');
                            findItem.Description = (String)item.get('Description');
                            if(findItem.Type == 'Address') {
                            	response.addresses.add(findItem);
                            }

                            response.success = true;
                        }

                    }
                    else {
                        // no matching addresses. so return empty list.
                        response.success = true;
                    }
                }
                else {
                    response.message = 'Unknown Data from addressy';
                }
            }
            else {
                response.success = false;
                response.message  = 'Http error. Status Code: ' + httpResponse.getStatusCode();
            }
        }
        catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, e);
            response.success = false;
            response.message = e.getMessage();// + e.getStackTraceString();
        }

        System.debug('Exiting CCAviAddressyAPI.find(); Response: ' + JSON.serialize(response));
        return response;
    }
    
    /**
     * @Description - 
     *
     * @Author  
     * @Date    Sep 2017
     *
     * @param   String 
     * @param   String
     * @return  Boolean
     **/
    public static Boolean validateCompleteAddress(String storefront, String address) {
        FindResponse response = find(storefront, address);
        
        if (response.success && response.addresses.size() == 1 && response.addresses[0].Type == 'Address') {
            return true;
        }
        
        return false;
    }
    
    /**
     * @Description - Call to get complete address using Addressy Id retrived from find call
     *
     * @Author  
     * @Date    Sep 2017
     *
     * @param   String 
     * @param   String
     * @return  FindResponse
     **/
    public static RetrieveResponse retrieveAddress(String storefront, String id) {
        System.debug('Entered CCAviAddressyAPI.retrieveAddress(); Storefront: ' + storefront + '; Id: ' + id);

        RetrieveResponse response = new RetrieveResponse();

        try {
            // read Addressy config data from settings for this store front
            CCAviAddressySettings__c settings =  CCAviAddressySettings__c.getInstance(storefront);
            if (settings == null) {                
                response.success = false;
                response.message  = 'Invalid store front';
                System.debug('Exiting CCAviAddressyAPI.retrieveAddress(); Response: ' + JSON.serialize(response));
                return response;
            }
             
            CCAviHttpUtil httpRequest = new CCAviHttpUtil();
            httpRequest.method = CCAviHttpUtil.REQUEST_TYPE.GET;
            httpRequest.requestContentType = CCAviHttpUtil.CONTENT_TYPE.JSON;
            httpRequest.endpoint = settings.Retrieve_End_Point__c;
            httpRequest.addQueryParameter('Key', settings.Key__c);
            httpRequest.addQueryParameter('Id', id);

            HttpResponse httpResponse = httpRequest.submitRequest();

            if (httpResponse.getStatusCode() == 200) {
                String responseBody =  httpResponse.getBody(); 
                System.debug(responseBody);
                Map<String, Object> topObject = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
                // response should have key Items
                if (topObject.containsKey('Items')) {
                    List<Object> listItems = (List<Object>)topObject.get('Items');
                    if (listItems.size() == 1) {
                        for (Object obj : listItems) {
                            Map<String, Object> item = (Map<String, Object>)obj;
                            // if the response has Error key then an error has occured.
                            if (item.containsKey('Error')) {
                                response.message = JSON.serialize(item);
                                break;
                            }

                            
                            response.success = true;
                            response.address = new Address();
                            response.address.Id = (String)item.get('Id');
                            response.address.Line1 = (String)item.get('Line1');
                            response.address.Line2 = (String)item.get('Line2');
                            response.address.Line3 = (String)item.get('Line3');
                            response.address.City = (String)item.get('City');
                            response.address.StateName = (String)item.get('ProvinceName');
                            response.address.StateCode = (String)item.get('ProvinceCode');
                            response.address.ZipCode = (String)item.get('PostalCode');
                            response.address.CountryName = (String)item.get('CountryName');
                            response.address.CountryCode2 = (String)item.get('CountryIso2');
                            response.address.CountryCode3 = (String)item.get('CountryIso3');
                            response.address.County = (String)item.get('AdminAreaName');

                        }

                    }
                    else {
                        // no matching addresses. so return empty list.
                        response.message = 'Id not found';
                    }
                }
                else {
                    response.message = 'Unknown Data from addressy';
                }
            }
            else {
                response.success = false;
                response.message  = 'Http error. Status Code: ' + httpResponse.getStatusCode();
            }
        }
        catch (Exception e) {
            System.debug(System.LoggingLevel.ERROR, e);
            response.success = false;
            response.message = e.getMessage();// + e.getStackTraceString();
        }

        System.debug('Exiting CCAviAddressyAPI.retrieveAddress(); Response: ' + JSON.serialize(response));
        return response;
    }

    public class FindItem {
        public String Id {get; set;}
        public String Type {get; set;}
        public String Text {get; set;}
        public String Description {get; set;}
    }
    public class FindResponse {
        public String message {get; set;}
        public Boolean success {get; set;}
        public List<FindItem> addresses {get; set;}

        public FindResponse() {
            success = false;
            addresses = new List<FindItem>();
        }
    }
    
    public class Address {
        public String Id {get; set;}
        public String Line1 {get; set;}
        public String Line2 {get; set;}
        public String Line3 {get; set;}
        public String City {get; set;}
        public String StateName {get; set;}
        public String StateCode {get; set;}
        public String ZipCode {get; set;}
        public String CountryName {get; set;}
        public String CountryCode2 {get; set;}
        public String CountryCode3 {get; set;}
        public String County {get; set;}
    }
    public class RetrieveResponse {
        public String message {get; set;}
        public Boolean success {get; set;}
        public Address address {get; set;}

        public RetrieveResponse() {
            success = false;
        }
    }
}