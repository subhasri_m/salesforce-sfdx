global with sharing class CCPDCStoredPaymentController {
    public String baseURL {get; private set;}
    public String path {get; private set;}

    public String accountNumber {get; private set;}
    public String displayName {get; private set;}
    public Boolean isEnabled {get; private set;}
    public String expirationMonth {get; private set;}
    public String expirationYear {get; private set;}
    public String storedPaymentId {get; private set;}

    global CCPDCStoredPaymentController() {
        String pid = ApexPages.currentPage().getParameters().get('pid');
        String pt = ApexPages.currentPage().getParameters().get('pt');
        String mode = ApexPages.currentPage().getParameters().get('mode');

        if (pt == 'cc') {
            if (mode == 'edit') {
                if (pid != null) {
                    ccrz__E_StoredPayment__c payment = CCAviStoredPaymentDAO.getStoredPayment(pid);
                    if (payment != null) {
                        storedPaymentId = payment.Id;
                        accountNumber = payment.ccrz__AccountNumber__c;
                        displayName = payment.ccrz__DisplayName__c;
                        isEnabled = payment.ccrz__Enabled__c;
                        expirationMonth = String.valueOf(payment.ccrz__ExpMonth__c);
                        if (expirationMonth != null && expirationMonth.length() == 1) {
                            expirationMonth = '0' + expirationMonth;
                        }
                        expirationYear = String.valueOf(payment.ccrz__ExpYear__c);
                    }
                }
            }
            else if (mode == 'new') {
                String store = ccrz.cc_CallContext.storefront;
                CCAviCardConnectSettings__c settings = CCAviCardConnectSettings__c.getInstance(store);
                this.baseURL = settings.Base_URL__c;
                this.path = settings.Hosted_Tokenizer_Path__c;
            }
        }
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult updateStoredPayment(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);

            ccrz__E_StoredPayment__c payment = new ccrz__E_StoredPayment__c();
            payment.Id = (Id) input.get('storedPaymentId');
            payment.ccrz__DisplayName__c = (String) input.get('displayName');
            payment.ccrz__ExpMonth__c = Integer.valueOf(input.get('expirationMonth'));
            payment.ccrz__ExpYear__c = Integer.valueOf(input.get('expirationYear'));
            payment.ccrz__Enabled__c = (Boolean) input.get('isEnabled');
            update payment;

            response.success = true;
        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }

    @RemoteAction
    global static ccrz.cc_RemoteActionResult createStoredPayment(ccrz.cc_RemoteActionContext ctx, String data) {
        ccrz.cc_RemoteActionResult response = CCAviPageUtils.remoteInit(ctx);

        try {
            Map<String, Object> input = (Map<String, Object>) JSON.deserializeUntyped(data);

            String storefront = ccrz.cc_CallContext.storefront;
            String token = (String)input.get('token');
            Id accountId = ccrz.cc_CallContext.effAccountId;
            CCAviCardConnectAPI.CardConnectResponse authResponse = CCAviCardConnectAPI.zeroAuthorization(storefront, (String)input.get('token'), (String)input.get('expiration'), (String)input.get('verificationCode'), (String)input.get('name'));
            List<ccrz__E_StoredPayment__c> findCard = [SELECT Id FROM ccrz__E_StoredPayment__c WHERE ccrz__Token__c  =:token AND ccrz__EffectiveAccountID__c=:accountId AND CreatedById =:UserInfo.getUserId()];

            
            if (authResponse.success && (findCard == null || findCard.isEmpty())) {

                ccrz__E_StoredPayment__c payment = new ccrz__E_StoredPayment__c();
                payment.ccrz__Account__c = ccrz.cc_CallContext.effAccountId;
                payment.ccrz__AccountNumber__c = (String) input.get('accountNumber');
                payment.ccrz__AccountType__c = (String) input.get('accountType');
                payment.ccrz__DisplayName__c = (String) input.get('displayName');
                payment.ccrz__EffectiveAccountID__c = ccrz.cc_CallContext.effAccountId;
                payment.ccrz__ExpMonth__c = Integer.valueOf(input.get('expirationMonth'));
                payment.ccrz__ExpYear__c = Integer.valueOf(input.get('expirationYear'));
                payment.ccrz__Enabled__c = (Boolean) input.get('isEnabled');
                payment.ccrz__Storefront__c = storefront;
                payment.ccrz__Token__c = (String) input.get('token');
                payment.ccrz__User__c = ccrz.cc_CallContext.currUserId;

                insert payment;
                CCAviPageUtils.buildResponseData(response, true, 
                    new Map<String,Object>{'input' => authResponse}
                );

            } else {              

                if(findCard!=null){
                    if(authResponse != null &&  authResponse.message != null && authResponse.message == 'Non-numeric CVV') {
                        authResponse.message = 'CC_PDC_CC_CVVInvalid';
                        CCAviPageUtils.buildResponseData(response, false, 
                                new Map<String,Object>{'input' => authResponse, 'message' => 'CC_PDC_CC_CVVInvalid'}
                        );                        
                    } else if(authResponse != null &&  authResponse.message != null && authResponse.message != 'Approval'){
                        CCAviPageUtils.buildResponseData(response, false, 
                                new Map<String,Object>{'input' => authResponse, 'message' => authResponse.message}
                        );
                    } else {
                        CCAviPageUtils.buildResponseData(response, false, 
                                new Map<String,Object>{'input' => authResponse, 'message' => 'Card Already Exists'}
                        );
                    }  
                }
                else {
                    CCAviPageUtils.buildResponseData(response, true, 
                        new Map<String,Object>{'input' => authResponse}
                        );

                }

            }


            //response.success = authResponse.success;
            //Map<String,Object> result = new Map<String,Object>();
            //result.put('input', authResponse);
            //response.data = result;


        }
        catch (Exception e) {
            CCAviPageUtils.buildResponseData(response, false,
                new Map<String,Object>{'error' => e.getMessage(),'cause' => e.getCause(),'lineno' => e.getLineNumber(),'stack' => e.getStackTraceString()}
            );
        }
        return response;
    }
}