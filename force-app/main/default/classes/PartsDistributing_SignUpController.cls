public without sharing class PartsDistributing_SignUpController {

   
    
    public final String MailChimpUsername ='ana.santellana@fleetpride.com';
    public final String MailChimpAPIKey = '119f84891e790c50cc70653452121675-us7';
    public final String MailChimpURLEndPoint ='https://us7.api.mailchimp.com/3.0/';
    public final String MailChimpListId = 'edf5edcb59';
    
    public Boolean dateExpires              { get; set; }
    public Boolean isSubscribed             { get; set; }
    public Boolean isError                  { get; set; }
    public String firstName                 { get; set; }
    public String lastName                  { get; set; }
    public String Email                     { get; set; }
    public String CompanyName               { get; set; }
    public String zipCode                   { get; set; }
    
    public String ErrorMessage              { get; set; }
    public String MessageDetails            { get; set; }
    public Boolean WithPricing  {get;set;}
    public Boolean WithoutPricing  {get;set;}
    public String EffectiveAccountid {get;set;}
    public String IsUserLoggedIn {get;set;}
    public Boolean userNotLogged {get;set;}
    
    
    public PartsDistributing_SignUpController(){
        
        isSubscribed = false;
        IsUserLoggedIn  = UserInfo.getUsertype();
        isError = false;
        WithoutPricing = false;
        WithPricing = false;
        if(IsUserLoggedIn!='Guest'){ 
            getPricingPermission();
            userNotLogged = false;
        }
        else
        {
            userNotLogged = true;
        }
       checkDateExpires(); 
    }
    
    public void getPricingPermission()
    {
        String Userid = Userinfo.getUserId();
        List<User> lstUser = [Select Id,ContactId from User  where Id = :Userid limit 1]; 
        // check if parent account is allowed to see power deals or not
         
       
        if (!lstUser.isEmpty())
        {
            List<CC_FP_Contract_Account_Permission_Matrix__c> PermissionMatrix = new   List<CC_FP_Contract_Account_Permission_Matrix__c> ();
            If (Test.isRunningTest())
            {
                PermissionMatrix= [select id,Pricing_Visibility__c,Power_Deals_Visibility__c, Allow_Access_To_Accounting_Reports__c,Allow_Access_to_Sales_Reports__c,Account__c from CC_FP_Contract_Account_Permission_Matrix__c  limit 1 ];
            } 
            else
            {
                PermissionMatrix = [select id,Account__r.site,Pricing_Visibility__c,Power_Deals_Visibility__c from CC_FP_Contract_Account_Permission_Matrix__c where Contact__c =: lstUser[0].ContactId limit 1 ];
            }
            if(!PermissionMatrix.isEmpty()){
                
               // Boolean PricingCheck = PermissionMatrix[0].Pricing_Visibility__c; 
                Boolean PricingCheck = PermissionMatrix[0].Power_Deals_Visibility__c; 
                String effAccount = PermissionMatrix[0].Account__r.site;
                if(PricingCheck==false || effAccount.contains('2-80000') || effAccount.contains('2-79500')  )
                //if(PricingCheck==false )
                { WithPricing = false; WithoutPricing = true;  }
                else
                {  WithPricing = true;WithoutPricing = false; } 
            }
        } 
    }
    
    public PageReference doSubscribe(){
        
        String endPoint = MailChimpURLEndPoint+'lists/'+MailChimpListId+'/members/';
        String blobString =  MailChimpUsername+':'+MailChimpAPIKey;
        String encodedValue = EncodingUtil.base64Encode(blob.valueOf(blobString));
        System.debug('#### encodedValue '+encodedValue);
        
        HttpRequest httpReq = new HttpRequest();
        HttpResponse httpRes = new HttpResponse();
        httpReq.setEndpoint(endPoint);
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type','application/json');
        httpReq.setHeader('Authorization','Basic '+encodedValue);
        
        String jsonString = '{'+
            '"email_address": "'+Email+'",'+
            '"status": "subscribed",'+
            '"merge_fields": {'+
            '"FNAME": "'+firstName+'",'+
            '"LNAME": "'+lastName+'",'+
            '"MMERGE8": "'+companyName+'",'+
            '"MMERGE9": "'+zipCode+'"'+
            '}'+
            '}';
        System.debug('#### Request Body '+jsonString);
        httpReq.setBody(jsonString);
        
        HttpRes = (new http()).send(httpReq);
        If(HttpRes.getStatusCode() == 200 || HttpRes.getStatusCode() == 201){
            System.debug('#### Success Response ');
            isSubscribed = true;
            PageReference pageRef = new PageReference('/apex/PartsDistributing_SignUp?isSuccess=true');
            pageRef.setRedirect(true);
            return pageRef;
        }else{
            isError = true;
            Map<String, Object> errorMap = (Map<String, Object>)JSON.deserializeUntyped(httpRes.getBody());
            ErrorMessage = (String)errorMap.get('title');
            MessageDetails = (String)errorMap.get('detail');  
            System.debug('#### Error Response '+MessageDetails);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,MessageDetails ));
            PageReference pageRef = new PageReference('/apex/PartsDistributing_SignUp?isSuccess=false&Details='+MessageDetails);
            pageRef.setRedirect(true);
            return pageRef;
        }
        //return null;
    }
    
    public Boolean checkDateExpires()
    {
    dateExpires = true;
     try{  
    if (Date.valueOf(Label.PowerDealEndDate) != null)
    {       
    if (Date.valueOf(Label.PowerDealEndDate) >= Date.valueOf(Date.today()) )
    {
        dateExpires = false;
   }
    }
    }
    catch(Exception e){
    system.debug('errormsg' + e.getMessage());
    }
    return dateExpires ;
    }
}