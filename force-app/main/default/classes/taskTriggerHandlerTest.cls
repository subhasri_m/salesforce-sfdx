@isTest
public class taskTriggerHandlerTest{
       static testmethod void CheckOnUpdateflag(){
        Account acc = new Account(Name = 'Test Trigger');
           insert acc;
        Task t2 = new Task(Subject = 'ABC', Status = 'Completed', Objective__c = 'Housekeeping', Type = 'Sales Call', WhatID = acc.id);
        insert t2;
        List<Task> tsklst = new List<Task>();
        List<Task> tsklst2 = new List<Task>();
        for (Integer i =0 ; i<=10 ;i++){
            Task t = new Task(Subject = 'ABC', Status = 'Completed', Objective__c = 'Housekeeping', Type = 'Sales Call', WhatID = acc.id);   
            tsklst.add(t);
        }
        for (Integer i =0 ; i<=10 ;i++){
            Task t = new Task(Subject = 'ABC', Status = 'Not Completed', Objective__c = 'Housekeeping', Type = 'Sales Call', WhatID = acc.id);   
            tsklst.add(t);
        }
        Insert tsklst;
        
        List<Task> tasks = [select id,whatid,Objective__c,LatestFiveCompltActivity__c, status from task];
        for (task t : tasks){
            if (t.Status != 'Completed'){
                t.Status= 'Completed';
                tsklst2.add(t);
            }else
            {
                t.Status= 'Not Completed';
                tsklst2.add(t);
            }
        }
        update tsklst2;
    }
    
    static testMethod void InsertRecurrenceTask(){
        
        
        
        Account acc = new Account(Name = 'Test Trigger');
        insert acc;
        System.debug('Accid -->'+acc.Id);
        Task t = new Task(Status = 'Not Started', Objective__c = 'Housekeeping', IsRecurrence = true,RecurrenceStartDateOnly = date.newinstance(2019, 10, 01) , RecurrenceEndDateOnly = date.newinstance(2019, 11, 01), RecurrenceType = 'RecursWeekly', RecurrenceInterval = 2 ,RecurrenceDayOfWeekMask = 8,Type = 'Sales Call', WhatID = acc.id);
        try{
            Insert t;
            system.debug('Task generated'+t.WhatId);
            delete t;    
        } catch(Exception e) {
            
        }
        
        Account acc1 = new Account(Name = 'Test Trigger');
        insert acc1;
        
        Map<id,task> mapvar = new map<id,task>();
        List<task> tasklist = new list<task>();
        Task t1 = new Task(Status = 'Not Started', Objective__c = 'Housekeeping', IsRecurrence = true,RecurrenceStartDateOnly = date.newinstance(2019, 10, 01) , RecurrenceEndDateOnly = date.newinstance(2019, 10, 01), RecurrenceType = 'RecursWeekly', RecurrenceInterval = 2 ,RecurrenceDayOfWeekMask = 8,Type = 'Sales Call', WhatID = acc1.id);
        try{
            Insert t1;
            system.debug('Task generated'+t.WhatId);
            mapvar.put(t1.id, t1);
            tasklist.add(t1);
        } catch(Exception e) {
            
        } 
        TaskTriggerHandler.CheckMonthlyCallonInsert(tasklist);
        TaskTriggerHandler.DepopulateAccountRecussrenceEndDate(mapvar);
        TaskTriggerHandler.updateDaysSinceLastSalesCall(tasklist,mapvar);
        TaskTriggerHandler.CheckMonthlyCallonUpdate(tasklist,mapvar);  
        
        
        
        
    }
    
    static testMethod void updateRecurrenceTask(){
        
        
        Account acc = new Account(Name = 'Test Trigger');
        insert acc;
        system.debug('check me--->'+acc.id);
        Task t = new Task(Status = 'Not Started', Objective__c = 'Housekeeping', IsRecurrence = true,RecurrenceStartDateOnly = date.newinstance(2019, 10, 01) , RecurrenceEndDateOnly = date.newinstance(2019, 11, 01), RecurrenceType = 'RecursWeekly', RecurrenceInterval = 2 ,RecurrenceDayOfWeekMask = 8,Type = 'Sales Call', WhatID = acc.id);
        try{
            Insert t;
            system.debug('Task generated'+t.WhatId);
        } catch(Exception e) {
            
        }
        
        List<Task> tasks = [select id,whatid,Objective__c,IsRecurrence,ActivityDate from task];
        
        for (task t2: tasks)
        {
            if(t2.IsRecurrence!=true){
                t2.Objective__c = 'abc';
                t2.ActivityDate = date.Today().addDays(2);
            }
        }
        
        update tasks;
        
        List<Task> tasks2 = [select id,whatid,status,Objective__c,IsRecurrence,ActivityDate from task];
        for (task t2: tasks2)
        {
            if(t2.IsRecurrence!=true){
                t2.status='Completed';
            }
        }
        
        update tasks2; 
        
    }
    
    
    static testMethod void UpdateUser(){ 
        
        Account acc = new Account(Name = 'Test Trigger');
        insert acc;
        
        Task t = new Task(Status = 'Completed', Objective__c = 'Housekeeping', IsRecurrence = true,RecurrenceStartDateOnly = date.newinstance(2019, 10, 01) , RecurrenceEndDateOnly = date.newinstance(2019, 11, 01), RecurrenceType = 'RecursWeekly', RecurrenceInterval = 2 ,RecurrenceDayOfWeekMask = 8,Type = 'Sales Call',  WhatID = acc.id);
        try{
            Insert t;  system.debug('Task generated'+t.WhatId);
        } catch(Exception e) {
            
        }
        
        List<task> t1 = [select id from task];
        
        delete t1;
        
    }
    
    static testmethod void tempCover(){ 
        tasktriggerHandler.testmethodcoverage_Temp_TobeRemove();
    }
}