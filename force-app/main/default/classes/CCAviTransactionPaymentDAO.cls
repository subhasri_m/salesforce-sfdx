public with sharing class CCAviTransactionPaymentDAO {
    public static List<ccrz__E_TransactionPayment__c> getTransactionPaymentsForOrder(Id theId) {
        List<ccrz__E_TransactionPayment__c> payments = [
            SELECT 
                Id,
                ccrz__Account__c,
                ccrz__AccountNumber__c,
                ccrz__AccountType__c,
                ccrz__Address__c,
                ccrz__Amount__c,
                ccrz__BillTo__c,
                ccrz__CCOrder__c,
                ccrz__Comments__c,
                ccrz__Contact__c,
                ccrz__CurrencyISOCode__c,
                ccrz__ExpirationMonth__c,
                ccrz__ExpirationYear__c,
                ccrz__ParentTransactionPayment__c,
                ccrz__PaymentType__c,
                ccrz__RequestAmount__c,
                ccrz__SoldTo__c,
                ccrz__SourceTransactionPayment__c,
                ccrz__StoredPayment__c,
                ccrz__Storefront__c,
                ccrz__SubAccountNumber__c,
                ccrz__Token__c,
                ccrz__TransactionCode__c,
                ccrz__TransactionType__c,
                ccrz__TransactionTS__c,
                ccrz__VerificationCode__c,
                ccrz__User__c,
                CC_Order_Item_Group__c
            FROM 
                ccrz__E_TransactionPayment__c 
            WHERE 
                ccrz__CCOrder__c = :theId 
            ];
        
        return payments;
    }
    public static List<ccrz__E_TransactionPayment__c> getTransactionPaymentsForCarts(Id theId) {
        List<ccrz__E_TransactionPayment__c> payments = [
            SELECT 
                Id,
                ccrz__Account__c,
                ccrz__AccountNumber__c,
                ccrz__AccountType__c,
                ccrz__Address__c,
                ccrz__Amount__c,
                ccrz__BillTo__c,
                ccrz__CCOrder__c,
                ccrz__Comments__c,
                ccrz__Contact__c,
                ccrz__CurrencyISOCode__c,
                ccrz__ExpirationMonth__c,
                ccrz__ExpirationYear__c,
                ccrz__ParentTransactionPayment__c,
                ccrz__PaymentType__c,
                ccrz__RequestAmount__c,
                ccrz__SoldTo__c,
                ccrz__SourceTransactionPayment__c,
                ccrz__StoredPayment__c,
                ccrz__Storefront__c,
                ccrz__SubAccountNumber__c,
                ccrz__Token__c,
                ccrz__TransactionCode__c,
                ccrz__TransactionType__c,
                ccrz__TransactionTS__c,
                ccrz__VerificationCode__c,
                ccrz__User__c,
                CC_Order_Item_Group__c,
            	CC_Cart_Item_Group__c,
            	CC_Cart__c,
            	CC_Cart_Item_GroupName__c
            FROM 
                ccrz__E_TransactionPayment__c 
            WHERE 
                CC_Cart__c = :theId AND CC_Cart_Item_Group__c !=null and ccrz__TransactionCode__c != 'Authorization Failed'
            ];
        
        return payments;
    }
}