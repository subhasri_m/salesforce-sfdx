@isTest
public class TopFiveComptTaskAccountUtilityTest {
  
    
    @testSetup
    static void testSetup() {
       Account acc = new Account(Name = 'Test Trigger');
       insert acc;
       List<Task> tsklst = new List<Task>();
       for (Integer i =0 ; i<=10 ;i++){
            Task t = new Task(Subject = 'ABC', Status = 'Completed', Objective__c = 'Housekeeping', Type = 'Sales Call', WhatID = acc.id);   
            tsklst.add(t);
        }
        insert tsklst;
    } 
    
    
    public static testMethod void TestTopFiveComptTaskAccountUtility(){

        Test.startTest();
        database.executebatch(new TopFiveComptTaskAccountUtility());
        Test.stopTest();
       
    }
    
    
}