global class Batch_DeleteOverdueTasks implements Schedulable {
   global void execute(SchedulableContext sc) {
      Id batchInstanceID = Database.executeBatch(new DeleteOverdueTasks());
   }
}