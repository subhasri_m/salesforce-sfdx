global class CCPDCLogicCartPrice extends ccrz.ccLogicCartPrice {
    
    global static Map<String, Decimal> priceMap = new Map<String, Decimal>();
    global static Map<Id, ccrz__E_CartItem__c> extraMap = null;
    global static Map<String, Decimal> requiredPricingMap = null;
    global static List<CC_FP_Required_Cart_Item__c> requiredItems = new List<CC_FP_Required_Cart_Item__c>();
    global static Map<String, Map<String, ccrz__E_RelatedProduct__c>> requiredProductsMap = null;
    global static Map<Id, Map<String, CC_FP_Required_Cart_Item__c>> requiredCartItemMap = null;
    global static Map<String,Map<String, Decimal>> localPriceMap = new Map<String,Map<String, Decimal>>(); 
    
    global override Map<String, Object> process(Map<String, Object> inputData) {
        System.debug('In CCPDCLogicCartPrice Block');
        ccrz.ccLog.log('CCPDCLogicCartPrice process CCPDCCallContext.updatePriceMap' +CCPDCCallContext.updatePriceMap);
        //CCPDCCallContext.skipPricing = true;
        Map<String, Object> outputData = super.process(inputData);
        //ccrz.ccLog.log('end CCAviLogicCartPrice process');
        return outputData;
    }
    
    global override Map<String, Object> fetchPricingData(Map<String, Object> inputData) {
        //ccrz.ccLog.log('CCAviLogicCartPrice fetchPricingData'); 
        
        Long beginOfSearch = DateTime.now().getTime();
        ccrz.ccLog.log('pricing-service CCPDCLogicCartPrice begin of fetchPricingData: ' + beginOfSearch);  
        //ccrz.ccLog.log('inputData: ' + inputData);
        List<ccrz__E_CartItem__c> items = (List<ccrz__E_CartItem__c>) inputData.get('cartItemList');    
        if (items != null && !items.isEmpty()) {
            Map<String, Map<String, Decimal>> quantityMap = CCPDCPricingHelper.getQuantityMap(items);
            
            requiredPricingMap = getRequiredProducts(quantityMap);
            CCAviBoomiSettings__c settings =  CCAviBoomiSettings__c.getInstance((String) inputData.get('ccrz__Storefront__c'));
            Boolean boomiAllProducts = settings.Enable_Boomi_On_All__c;
            Integer timeBetweenPricing = (Integer) settings.Time_Between_Pricing__c;
            if (timeBetweenPricing == NULL) {
                timeBetweenPricing = 60;
            }
            
            ccrz__E_Cart__c cart = (ccrz__E_Cart__c)inputData.get('cartToPrice');
            if(ccrz.cc_CallContext.storefront == 'parts') {
                timeBetweenPricing = 0;                  
                Location__c loc = CCFPLocationDAO.getLocation(cart.CC_FP_Location__c);
                ccrz.cc_CallContext.currPageParameters.put('branchLoc', loc.Location__c);  
            }
            
            //ccrz.ccLog.log('CCPDCLogicCartPrice cart ' + cart.ccrz__EffectiveAccountID__c); 
            Map<Id, ccrz__E_CartItem__c> itemsTimestamps = new Map<Id, ccrz__E_CartItem__c> ([SELECT Id,CC_FP_Price_Timestamp__c,ccrz__Product__r.Part_Number__c, ccrz__Product__r.Pool_Number__c FROM ccrz__E_CartItem__c WHERE Id IN :items]);
            //Map<String, Decimal> productPriceLookup = new Map<String, Decimal>();
            Account acc = [SELECT Id, ccrz__E_AccountGroup__c FROM Account WHERE Id =:cart.ccrz__EffectiveAccountID__c];
            String accGrpId = String.valueOf(acc.ccrz__E_AccountGroup__c);
            List<String> priceListId = new List<String>();
            List<String> productIds = new List<String>();
            
            Map<String,Set<String>> pricelistItemsChecker = new Map<String,Set<String>>();
            List<ccrz__E_AccountGroupPriceList__c> accGrpPriceList = [SELECT Id, ccrz__Pricelist__c FROM ccrz__E_AccountGroupPriceList__c WHERE ccrz__AccountGroup__c =:accGrpId];
            for(ccrz__E_AccountGroupPriceList__c accGrpPrice : accGrpPriceList){
                priceListId.add(String.valueOf(accGrpPrice.ccrz__Pricelist__c ));    
            }
            
            for(ccrz__E_CartItem__c item :items){
                productIds.add(item.ccrz__Product__c);
            }
            List<ccrz__E_PriceListItem__c> pricelistItems = [SELECT Id,ccrz__Product__c,ccrz__Pricelist__r.name,ccrz__Price__c, ccrz__Product__r.Part_Number__c, ccrz__Product__r.Pool_Number__c FROM ccrz__E_PriceListItem__c WHERE ccrz__Product__c IN: productIds AND ccrz__Pricelist__c IN:priceListId];
            for(ccrz__E_PriceListItem__c pricelistItem : pricelistItems){
                if(!pricelistItemsChecker.isEmpty() && pricelistItemsChecker.containsKey(pricelistItem.ccrz__Product__c)){
                    Set<String> plnames = pricelistItemsChecker.get(pricelistItem.ccrz__Product__c);
                    plnames.add(pricelistItem.ccrz__Pricelist__r.name);
                }else{
                    Set<String> plnames =new Set<String>();
                    plnames.add(pricelistItem.ccrz__Pricelist__r.name);
                    pricelistItemsChecker.put(pricelistItem.ccrz__Product__c,plnames);
                }
                
                String prKey = String.valueOf(pricelistItem.ccrz__Product__r.Pool_Number__c) + String.valueOf(pricelistItem.ccrz__Product__r.Part_Number__c);
                if(pricelistItem.ccrz__Pricelist__r.name == 'Enterprise'){
                    if(!localPriceMap.isEmpty() && localPriceMap.containsKey(prKey)){
                        (localPriceMap.get(prKey)).put('Enterprise',pricelistItem.ccrz__Price__c);
                    } else if(pricelistItem.ccrz__Price__c >= 0){
                        localPriceMap.put(prKey,new Map<String,Decimal>{'Enterprise' => pricelistItem.ccrz__Price__c});
                    }
                } else {
                    if(!localPriceMap.isEmpty() && localPriceMap.containsKey(prKey)){
                        Map<String, Decimal> currentProdPricing = localPriceMap.get(prKey);
                        if(currentProdPricing.containsKey('BestPrice') && currentProdPricing.get('BestPrice') > pricelistItem.ccrz__Price__c){
                            currentProdPricing.put('BestPrice',pricelistItem.ccrz__Price__c);
                        }else if(!currentProdPricing.containsKey('BestPrice')){
                            currentProdPricing.put('BestPrice',pricelistItem.ccrz__Price__c);
                        }
                        
                    }else{
                        localPriceMap.put(prKey,new Map<String,Decimal>{'BestPrice'=>pricelistItem.ccrz__Price__c});
                    }
                }
            }
            ccrz.ccLog.log('CCPDCLogicCartPrice productPriceLookup: ' + pricelistItemsChecker);
            List<ccrz__E_CartItem__c> newItems = new List<ccrz__E_CartItem__c>();
            Datetime now = Datetime.now();
            boolean refreshPriceMap = FALSE;
            for (ccrz__E_CartItem__c cartItem : items) {
                System.debug('cartItem='+cartItem);
                System.debug('itemsTimestamps='+ itemsTimestamps);
                Datetime dt = itemsTimestamps.get(cartItem.Id).CC_FP_Price_Timestamp__c;
                List<String> listplNames;
                if(pricelistItemsChecker.containsKey(cartItem.ccrz__Product__c)){
                    listplNames = new List<String>(pricelistItemsChecker.get(cartItem.ccrz__Product__c));
                }
                
                ccrz.ccLog.log('CCAviLogicCartPrice pricemap.put:ccrz__Price__c '+dt+',');
                System.debug('dt '+dt);
                System.debug('listplNames.size() '+listplNames.size());
                System.debug('listplNames[0] '+listplNames[0]);
                System.debug('boomiAllProducts '+boomiAllProducts);
                
                
                
                if (dt != NULL && (listplNames.size()==1 && listplNames[0]=='Enterprise' || boomiAllProducts)) {
                    ccrz.ccLog.log('CCPDCLogicCartPrice condition 1.5');
                    if (dt.addMinutes(timeBetweenPricing) < now) {
                        cartItem.CC_FP_Price_Timestamp__c = now;
                        newItems.add(cartItem);
                        refreshPriceMap = TRUE;
                        ccrz.ccLog.log('CCPDCLogicCartPrice condition 1.0');
                    } else {
                        String key = (String) itemsTimestamps.get(cartItem.Id).ccrz__Product__r.Pool_Number__c + (String) itemsTimestamps.get(cartItem.Id).ccrz__Product__r.Part_Number__c;
                        ccrz.ccLog.log('CCPDCLogicCartPrice condition 1.6');
                        ccrz.ccLog.log('CCPDCLogicCartPrice condition 1.6'+cartItem);
                        if(cartItem.ccrz__Price__c > 0 && !boomiAllProducts){
                            pricemap.put(key,Decimal.valueOf(String.valueOf(cartItem.ccrz__Price__c)));
                            //ccrz.ccLog.log('CCPDCLogicCartPrice pricemap.put:ccrz__Price__c ' + Decimal.valueOf(String.valueOf(cartItem.Id)));
                            ccrz.ccLog.log('CCPDCLogicCartPrice condition 1.1');
                            System.debug('cartItem.ccrz__OriginalItemPrice__c'+cartItem.ccrz__OriginalItemPrice__c);
                        } else if(cartItem.ccrz__OriginalItemPrice__c > 0 && !boomiAllProducts){
                            pricemap.put(key,Decimal.valueOf(String.valueOf(cartItem.ccrz__OriginalItemPrice__c)));
                            ccrz.ccLog.log('CCPDCLogicCartPrice condition 1.2');
                            ccrz.ccLog.log('CCPDCLogicCartPrice pricemap.put:ccrz__OriginalItemPrice__c' + Decimal.valueOf(String.valueOf(cartItem.ccrz__OriginalItemPrice__c)));
                            ccrz.ccLog.log('CCPDCLogicCartPrice condition 1.2');
                        } else{
                            newItems.add(cartItem);
                            ccrz.ccLog.log('CCPDCLogicCartPrice condition 1.3');
                        }
                        
                    }
                    ccrz.ccLog.log('CCPDCLogicCartPrice dt: ' + dt );
                }
                else {
                    if((listplNames.size()==1 && listplNames[0]=='Enterprise') || boomiAllProducts){
                        cartItem.CC_FP_Price_Timestamp__c = now;
                        newItems.add(cartItem);
                        refreshPriceMap = TRUE;
                    }
                    ccrz.ccLog.log('CCPDCLogicCartPrice condition 1.4');
                    
                }
            }
            
            ccrz.ccLog.log('CCAviLogicCartPrice newItems: ' + newItems);
            if(!newItems.isEmpty()){
                Map<String, Decimal> boomiReturnMap = new Map<String, Decimal>();
                boomiReturnMap = price(newItems, quantityMap);
                ccrz.ccLog.log('CCAviLogicCartPrice boomiReturnMap: ' + boomiReturnMap);
                if(boomiReturnMap != null){
                    priceMap.putAll(boomiReturnMap);
                }
            }
            ccrz.ccLog.log('CCAviLogicCartPrice priceMap: ' + priceMap);
        }
        Map<String, Object> outputData = super.fetchPricingData(inputData);
        ccrz.ccLog.log('outputData: ' + outputData);
        ccrz.ccLog.log('end CCAviLogicCartPrice fetchPricingData--ccApiProduct.PRODUCTPRICINGDATA');     
        Long endOfSearch = DateTime.now().getTime();
        return outputData;  
    }
    
    global override Map<String, Object> applyPricing(Map<String, Object> inputData) {
        ccrz.ccLog.log('CCAviLogicCartPrice applyPricing');
        List<ccrz__E_CartItem__c> items = (List<ccrz__E_CartItem__c>) inputData.get('cartItemsToPrice');
        if (items != null && !items.isEmpty()) {
            extraMap = getAdditionalInfo(items);
            if (requiredPricingMap != null && !requiredPricingMap.isEmpty()) {
                requiredProductsMap = findRequiredProducts(extraMap);
                requiredCartItemMap = findRequiredCartItems(extraMap);
            }
        }
        Map<String, Object> outputData = super.applyPricing(inputData);
        
        if (requiredItems != null && !requiredItems.isEmpty()) {
            upsert requiredItems;
        }
        
        ccrz.ccLog.log('end CCAviLogicCartPrice applyPricing');
        return outputData;
    }
    
    global override Map<String, Object> priceItem(Map<String, Object> inputData) {
        ccrz.ccLog.log('CCPDCLogicCartPrice priceItem pricemap'+priceMap);
        
        Map<String, Object> outputData = inputData;
        ccrz__E_CartItem__c item = (ccrz__E_CartItem__c) inputData.get('cartItem');
        List<ccrz__E_CartItem__c> boomiItems = new List<ccrz__E_CartItem__c>();
        ccrz__E_CartItem__c moreInfoItem = extraMap.get(item.Id);
        //System.debug('moreInfoItem'+moreInfoItem);
        // System.debug('moreInfoItem.CC_FP_DoNotReprice__c'+moreInfoItem.CC_FP_DoNotReprice__c);
        if (extraMap != null && priceMap != null && moreInfoItem != null && moreInfoItem.CC_FP_DoNotReprice__c != true)  {
            ccrz.ccLog.log('CCPDCLogicCartPrice priceItem 1 '+item);
            String key = moreInfoItem.ccrz__Product__r.Pool_Number__c + moreInfoItem.ccrz__Product__r.Part_Number__c;
            Decimal price = priceMap.get(key);
            if (price != null && price != 10000) {
                item.ccrz__Price__c = price;
                item.ccrz__OriginalItemPrice__c = price;
                item.ccrz__SubAmount__c = item.ccrz__Price__c * item.ccrz__Quantity__c;
                item.CC_FP_Pricing_Failed__c = false;
                ccrz.ccLog.log('CCPDCLogicCartPrice priceItem boomi correct'+price);
            }else if(price == 10000 && ccrz.cc_CallContext.StoreFront == 'parts'){
                // this is added for FP Storefront only
                item.ccrz__Price__c = 0;
                item.ccrz__OriginalItemPrice__c = 0;
                item.ccrz__SubAmount__c = 0;
                item.CC_FP_Pricing_Failed__c = false;
            }
            else if (localPriceMap.containsKey(key)) {
                Map<String,Decimal> piMap = localPriceMap.get(key);
                if(piMap.containsKey('BestPrice')){
                    item.ccrz__Price__c = piMap.get('BestPrice');
                    item.ccrz__SubAmount__c = item.ccrz__Price__c * item.ccrz__Quantity__c;
                    item.CC_FP_Pricing_Failed__c = false;
                }else if(piMap.containsKey('Enterprise') && piMap.get('Enterprise') > 0){
                    item.ccrz__Price__c = 0;
                    item.ccrz__SubAmount__c = item.ccrz__Price__c * item.ccrz__Quantity__c;
                    item.CC_FP_Pricing_Failed__c = true;
                    ccrz.ccLog.log('CC_FP_Pricing_Failed__c = true 1');
                }else {
                    if (item.ccrz__OriginalItemPrice__c != null) {
                        ccrz.ccLog.log('CC_FP_Pricing_Failed__c = true 3'+item.ccrz__OriginalItemPrice__c);
                        item.ccrz__Price__c = item.ccrz__OriginalItemPrice__c; item.ccrz__SubAmount__c = item.ccrz__OriginalItemPrice__c * item.ccrz__Quantity__c;
                    }                 
                    item.CC_FP_Pricing_Failed__c = true;
                    ccrz.ccLog.log('CC_FP_Pricing_Failed__c = true 4');
                    
                }
            }
            else {
                if (item.ccrz__OriginalItemPrice__c != null) {
                    item.ccrz__Price__c = item.ccrz__OriginalItemPrice__c;
                    item.ccrz__SubAmount__c = item.ccrz__OriginalItemPrice__c * item.ccrz__Quantity__c;
                }
                item.CC_FP_Pricing_Failed__c = true;
                ccrz.ccLog.log('CC_FP_Pricing_Failed__c = true 2 ');
                
            }
            
            if (requiredPricingMap != null && !requiredPricingMap.isEmpty()) {
                processRequiredProducts(item, moreInfoItem, requiredProductsMap, requiredCartItemMap);
            }
        }
        else {
            ccrz.ccLog.log('CCPDCLogicCartPrice priceItem 2 '+item);
            String key = moreInfoItem.ccrz__Product__r.Pool_Number__c + moreInfoItem.ccrz__Product__r.Part_Number__c;
            if(priceMap.containsKey(key) && priceMap.get(key)!=0){
                item.ccrz__Price__c = priceMap.get(key);
                item.ccrz__OriginalItemPrice__c = priceMap.get(key);
                item.ccrz__SubAmount__c = item.ccrz__OriginalItemPrice__c * item.ccrz__Quantity__c;
                ccrz.ccLog.log('CCPDCLogicCartPrice priceItem  condi 1'+key);
            }
            else if (localPriceMap.containsKey(key)) {
                Map<String,Decimal> piMap = localPriceMap.get(key);
                if(piMap.containsKey('BestPrice')){
                    item.ccrz__Price__c = piMap.get('BestPrice');
                    item.ccrz__SubAmount__c = item.ccrz__Price__c * item.ccrz__Quantity__c;
                    item.CC_FP_Pricing_Failed__c = false;
                    ccrz.ccLog.log('CCPDCLogicCartPrice priceItem  condi 2'+key);
                }
                else if(piMap.containsKey('Enterprise') && piMap.get('Enterprise') > 0){
                    item.ccrz__Price__c = piMap.get('Enterprise');item.ccrz__SubAmount__c = item.ccrz__Price__c * item.ccrz__Quantity__c;item.CC_FP_Pricing_Failed__c = false;
                    ccrz.ccLog.log('CCPDCLogicCartPrice priceItem  condi 3'+key);
                } 
                else {
                    if (item.ccrz__OriginalItemPrice__c != null) {
                        item.ccrz__Price__c = item.ccrz__OriginalItemPrice__c;
                        item.ccrz__SubAmount__c = item.ccrz__OriginalItemPrice__c * item.ccrz__Quantity__c;
                        ccrz.ccLog.log('CCPDCLogicCartPrice priceItem  condi 4:'+item.ccrz__OriginalItemPrice__c);
                    }
                }
            }
            if (moreInfoItem.CC_FP_DoNotReprice__c != true) {
                item.CC_FP_Pricing_Failed__c = false;
                //ccrz.ccLog.log('CCPDCLogicCartPrice priceItem condition 2');
            }
        }
        
        //ccrz.ccLog.log('end CCPDCLogicCartPrice priceItem');
        return outputData;
    }
    
    @testVisible
    private Map<String, Decimal> price(List<ccrz__E_CartItem__c> cartItems, Map<String, Map<String, Decimal>> quantityMap) {
        Map<String, Decimal> priceMap = null;
        Long beginOfSearch = DateTime.now().getTime();
        //ccrz.ccLog.log('pricing-service CCPDCLogicCartPrice begin of price: ' + beginOfSearch);
        
        if (cartItems != null && !cartItems.isEmpty()) {
            System.debug('#### cartItems '+cartItems);
            //CCAviBoomiAPI.SeriesPricingWSResponse response = CCPDCPricingHelper.price(cartItems, quantityMap);
            CCAviBoomiAPI.SeriesPricingWSResponse response = CCPDCPricingHelper.price(cartItems);
            ccrz.ccLog.log(System.LoggingLevel.INFO,'M:X','CCPDCLogicCartPrice price @@Rahul:response'+response);
            System.debug(JSON.serializePretty(response));
            ccrz.ccLog.log('pricing-service CCPDCLogicCartPrice boomi response ' + JSON.serializePretty(response));
            if (response != null && response.success && response.listPricing != null) {
                priceMap = CCPDCPricingHelper.createPriceMap(response);
            }
        }
        Long endOfSearch = DateTime.now().getTime();
        return priceMap;
    }
    
    private HttpRequest priceWithCont(List<ccrz__E_CartItem__c> cartItems, Map<String, Map<String, Decimal>> quantityMap) {
        ccrz.ccLog.log('Inside priceWithCont');
        Map<String, Decimal> priceMap = null;
        HttpRequest request = new HttpRequest();
        if (cartItems != null && !cartItems.isEmpty()) {
            request = CCPDCPricingHelper.priceWithCont(cartItems);
        }
        return request; 
    }
    
    @testVisible
    private Map<String, Decimal> priceWithContCallback(Object state) {
        CCAviBoomiAPI.SeriesPricingWSResponse response = CCPDCPricingHelper.priceWithContCallback(state);
        if (response != null && response.success && response.listPricing != null) {
            priceMap = CCPDCPricingHelper.createPriceMap(response);
        }
        return priceMap;
    }
    @testVisible
    private Continuation getPriceMap(List<ccrz__E_CartItem__c> cartItems, Map<String, Map<String, Decimal>> quantityMap) {
        HttpRequest httpRequest = priceWithCont(cartItems, quantityMap);
        Continuation con = new Continuation(120);
        Map<String,Object> stateMap = new Map<String, Object>();
        stateMap.put('RequestLabel', con.addHttpRequest(httpRequest));
        con.state = stateMap;
        con.continuationMethod = 'priceWithContCallback';
        return con;
    }
    
    private Map<Id, ccrz__E_CartItem__c> getAdditionalInfo(List<ccrz__E_CartItem__c> cartItems) {
        
        Map<Id, ccrz__E_CartItem__c> itemMap = null;
        Set<Id> cartItemIds = new Set<Id>();
        if (cartItems != null && !cartItems.isEmpty()) {
            for (ccrz__E_CartItem__c item : cartItems) {
                cartItemIds.add(item.Id);
            }
            itemMap = CCPDCCartDAO.getCartItemsForPricing(cartItemIds);
        }
        return itemMap;
    }
    
    public static Map<String, Decimal> getRequiredProducts(Map<String, Map<String, Decimal>> quantityMap) {
        Map<String, Decimal> requiredMap = new Map<String, Decimal>();
        
        Map<String, Decimal> quantityIdMap = quantityMap.get('sfid');
        
        if (quantityIdMap != null && !quantityIdMap.isEmpty()) {
            List<ccrz__E_RelatedProduct__c> required = CCPDCProductDAO.getRequiredProductsById(quantityIdMap.keySet());
            if (required != null && !required.isEmpty()) {
                for (ccrz__E_RelatedProduct__c r : required) {
                    Decimal quantity = quantityIdMap.get(r.ccrz__Product__c);
                    if (quantity == null) {
                        quantity = 1;
                    }
                    requiredMap.put(r.ccrz__RelatedProduct__c, quantity);
                }
            }
            quantityIdMap.putAll(requiredMap);
        }
        return requiredMap;
    }
    
    public static Map<String, Map<String, ccrz__E_RelatedProduct__c>> findRequiredProducts(Map<Id, ccrz__E_CartItem__c> extraMap) {
        
        Map<String, Map<String, ccrz__E_RelatedProduct__c>> requiredMap = new Map<String, Map<String, ccrz__E_RelatedProduct__c>>();
        if (extraMap != null && !extraMap.isEmpty()) {
            Set<String> productIds = new Set<String>();
            for (ccrz__E_CartItem__c item : extraMap.values()) {
                productIds.add(item.ccrz__Product__c);
            }
            List<ccrz__E_RelatedProduct__c> required = CCPDCProductDAO.getRequiredProductsById(productIds);
            if (required != null && !required.isEmpty()) {
                for (ccrz__E_RelatedProduct__c r : required) {
                    Map<String, ccrz__E_RelatedProduct__c> relatedMap = requiredMap.get(r.ccrz__Product__r.ccrz__SKU__c);
                    if (relatedMap == null) {
                        relatedMap = new Map<String, ccrz__E_RelatedProduct__c>();
                        requiredMap.put(r.ccrz__Product__r.ccrz__SKU__c, relatedMap);
                    }
                    relatedMap.put(r.ccrz__RelatedProduct__r.ccrz__SKU__c, r);                    
                }
               
                CCAviBoomiAPI.SeriesPricingWSResponse response = CCPDCPricingHelper.priceCoreProducts(required);
                if (response != null && response.success && response.listPricing != null) {
                    if(priceMap == null) {
                        priceMap = new Map<String, Decimal>();
                    }
                    for (CCAviBoomiAPI.Pricing p : response.listPricing) {
                        if (p.ErrorMessage == null && p.PRICE != null) {
                            String key = p.PoolNumber + p.PartNumber;
                            priceMap.put(key, p.PRICE);
                        }
                    }
                } 
            }
            
        }
        
        return requiredMap;
    }
    
    public static Map<Id, Map<String, CC_FP_Required_Cart_Item__c>> findRequiredCartItems(Map<Id, ccrz__E_CartItem__c> extraMap) {
        
        Map<Id, Map<String, CC_FP_Required_Cart_Item__c>> requiredItemMap = new Map<Id, Map<String, CC_FP_Required_Cart_Item__c>>();
        if (extraMap != null && !extraMap.isEmpty()) {
            List<CC_FP_Required_Cart_Item__c> requiredCartItems = CCPDCRequiredCartItemDAO.getRequiredCartItems(extraMap.keySet());
            if (requiredCartItems != null && !requiredCartItems.isEmpty()) {
                for (CC_FP_Required_Cart_Item__c item : requiredCartItems) {
                    Map<String, CC_FP_Required_Cart_Item__c> requiredSkuMap = requiredItemMap.get(item.CC_Parent_Cart_Item__c);
                    if (requiredSkuMap == null) {
                        requiredSkuMap = new Map<String, CC_FP_Required_Cart_Item__c>();
                        requiredItemMap.put(item.CC_Parent_Cart_Item__c, requiredSkuMap);
                    }
                    requiredSkuMap.put(item.CC_Product__r.ccrz__SKU__c, item);
                }
            }
        }
        return requiredItemMap;
        
    }
    
    public static void processRequiredProducts(ccrz__E_CartItem__c item, ccrz__E_CartItem__c moreInfoItem, Map<String, Map<String, ccrz__E_RelatedProduct__c>> requiredMap, Map<Id, Map<String, CC_FP_Required_Cart_Item__c>> requiredItemMap) {
        //ccrz.ccLog.log('processRequiredProducts totalPrice ');
        
        
        Map<String, ccrz__E_RelatedProduct__c> prodList = requiredMap.get(moreInfoItem.ccrz__Product__r.ccrz__SKU__c);
        if (prodList != null && !prodList.isEmpty()) {
            Decimal basePrice = 0;
            if(item.ccrz__Price__c > item.ccrz__OriginalItemPrice__c && item.ccrz__OriginalItemPrice__c != 0)
                basePrice = item.ccrz__OriginalItemPrice__c;
            else 
                basePrice = item.ccrz__Price__c;
            Decimal totalPrice = basePrice;
            ccrz.ccLog.log('processRequiredProducts totalPrice ' + totalPrice);
            Map<String, CC_FP_Required_Cart_Item__c> requiredList = requiredItemMap.get(item.Id);
            ccrz.ccLog.log('processRequiredProducts requiredList ' + requiredList);
            ccrz.ccLog.log('processRequiredProducts priceMap ' + priceMap);
            for (ccrz__E_RelatedProduct__c p : prodList.values()) {
                CC_FP_Required_Cart_Item__c ritem  = null;
                if (requiredList != null) {
                    ritem = requiredList.get(p.ccrz__RelatedProduct__r.ccrz__SKU__c);
                }
                ccrz.ccLog.log('processRequiredProducts ritem ' + ritem);
                if (ritem == null) {
                    
                    ritem = new CC_FP_Required_Cart_Item__c(
                        CC_Cart__c = item.ccrz__Cart__c,
                        CC_Product__c = p.ccrz__RelatedProduct__c,
                        CC_Parent_Cart_Item__c = item.Id,
                        Quantity__c = item.ccrz__Quantity__c
                    );
                }
                else {
                    ritem.Quantity__c = item.ccrz__Quantity__c;
                }
                String key = p.ccrz__RelatedProduct__r.Pool_Number__c + p.ccrz__RelatedProduct__r.Part_Number__c;
                Decimal price = priceMap.get(key);
                ccrz.ccLog.log('processRequiredProducts priceMap p ' + p);
                if (price != null && price != 10000) {
                    ritem.Price__c = price;
                    if (ritem.Quantity__c != null && ritem.Price__c != null) {
                        ritem.SubAmount__c = ritem.Price__c * ritem.Quantity__c;
                    }
                    if (ritem.SubAmount__c != null) {
                        totalPrice += ritem.Price__c;
                        ccrz.ccLog.log('processRequiredProducts ritem.Price__c : ' + ritem.Price__c);
                        ccrz.ccLog.log('processRequiredProducts totalPrice 2 : ' + totalPrice);
                    }
                }else if(ritem.Price__c != null){
                    totalPrice += ritem.Price__c;
                    item.CC_FP_Pricing_Failed__c = false;
                }
                else {
                    item.CC_FP_Pricing_Failed__c = true;
                    ccrz.ccLog.log('CC_FP_Pricing_Failed__c = true 3 ');
                }
                requiredItems.add(ritem);
            }
            item.ccrz__Price__c = totalPrice;
            item.CC_FP_Service_Price__c = basePrice;
            item.ccrz__SubAmount__c = item.ccrz__Price__c * item.ccrz__Quantity__c;
        }
    }
    
}