@isTest
public class TriggerOnCartItemHelplerTest {
static CCPDCTestUtil util = new CCPDCTestUtil();

    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
            Map<String, Object> m = util.initData();

        }

        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);

        CCPDCOMSAPITest.createOMSSettingsWithToken(util.STOREFRONT);
    }
     static testmethod void CheckUpdatePricingType(){
      ccrz__E_Cart__c cart = util.getCart();
      ccrz__E_CartItem__c cartItem = util.getCartItem();
      cartitem.CC_FP_DoNotReprice__c=true;
      Test.startTest();
      update cartitem; 
      Test.stopTest();
     // system.assert(cart.CartItemCheck__c) ;
    }
    
    static testmethod void checkUpdatefirstCartItemCheck(){
      ccrz__E_Cart__c cart = util.getCart();
      ccrz__E_CartItem__c cartItem = util.getCartItem(); 
     // system.assert(cart.CartItemCheck__c) ;
    }
     static testmethod void checkdeletefirstCartItemCheck(){
      ccrz__E_Cart__c cart = util.getCart();
      ccrz__E_CartItem__c cartItem = util.getCartItem(); 
      Test.startTest();
      delete  cartItem;  
      Test.stopTest();
    //  system.assert(!cart.CartItemCheck__c) ;
    }
}