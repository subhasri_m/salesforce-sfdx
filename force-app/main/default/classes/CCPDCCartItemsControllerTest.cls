@isTest
public class CCPDCCartItemsControllerTest {
    static CCPDCTestUtil util = new CCPDCTestUtil();
    
    @testSetup
    static void testSetup() {
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser) {
            Map<String, Object> m = util.initData();
        }
        
        ccrz.cc_util_Reflection.createStorefrontSetting(util.STOREFRONT);
    }
    
    static testmethod void controllerTest() {
        util.initCallContext();
        CCPDCCartItemsController ctrl = new CCPDCCartItemsController();
        //Map<String, Object> inventory = (Map<String, Object>) JSON.deserializeUntyped(ctrl.productInventory);
        //System.assert(inventory.containsKey('product-01'));
        
    }
    
    static testmethod void deleteMultipleCartItemsTest() {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.FulFillMentType__c = 'PickUp';
        List<String> data = new List<String>();
        data.add(cartItem.id);
        String serializedData = JSON.serialize(data);
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCCartItemsController.deleteMultipleCartItems(ctx, serializedData);
        Test.stopTest();
        System.assert(result.success == true);
    }
    
    static testmethod void setFreeShippingInfoLTLTest(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        Id cartId = cart.Id;
        ccrz__E_Cart__c newcart = [SELECT
                                   Id,
                                   CC_FP_Order_Subtotal__c
                                   FROM
                                   ccrz__E_Cart__c
                                   WHERE
                                   Id = :cartId];
        newcart.CC_FP_Order_Subtotal__c = 3700.00;
        update newcart;
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        ctx.currentCartId = String.valueOf(cart.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);
        Test.startTest();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.FulFillMentType__c = 'PickUp';
        update cartItem;
        cartItem.ccrz__Price__c = 1850;
        cartItem.ccrz__Quantity__c = 2;
        cartItem.ccrz__SubAmount__c = 3700;
        update cartItem;
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCCartItemsController.setFreeShippingInfo(ctx);
        Map<String, Object> data = (Map<String,Object>)result.data;
        Test.stopTest();
        
        System.assert(result.success);
        //System.debug('CurrentFreeShipping : '+String.valueOf(data.get('currentFreeShipping')));
        System.assertEquals(String.valueOf(data.get('currentFreeShipping')),'3700.00');
        
        
    }
    
    static testmethod void setFreeShippingInfoNotFreeTest(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        Id cartId = cart.Id;
        ccrz__E_Cart__c newcart = [SELECT
                                   Id,
                                   CC_FP_Order_Subtotal__c
                                   FROM
                                   ccrz__E_Cart__c
                                   WHERE
                                   Id = :cartId];
        newcart.CC_FP_Order_Subtotal__c = 80.00;
        update newcart;
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        ctx.currentCartId = String.valueOf(cart.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);
        Test.startTest();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.FulFillMentType__c = 'PickUp';
        update cartItem;
        cartItem.ccrz__Price__c = 40;
        cartItem.ccrz__Quantity__c = 2;
        cartItem.ccrz__SubAmount__c = 80;
        update cartItem;
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCCartItemsController.setFreeShippingInfo(ctx);
        Map<String, Object> data = (Map<String,Object>)result.data;
        Test.stopTest();
        
        System.assert(result.success);
        System.assertEquals(String.valueOf(data.get('currentFreeShipping')),'80.00');
    }
    
    static testmethod void setFreeShippingInfoParcelTest(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        Id cartId = cart.Id;
        ccrz__E_Cart__c newcart = [SELECT
                                   Id,
                                   CC_FP_Order_Subtotal__c
                                   FROM
                                   ccrz__E_Cart__c
                                   WHERE
                                   Id = :cartId];
        newcart.CC_FP_Order_Subtotal__c = 600.00;
        update newcart;
        
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        ctx.currentCartId = String.valueOf(cart.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);
        Test.startTest();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.FulFillMentType__c = 'PickUp';
        update cartItem;
        cartItem.ccrz__Price__c = 300;
        cartItem.ccrz__Quantity__c = 2;
        cartItem.ccrz__SubAmount__c = 600;
        update cartItem;
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCCartItemsController.setFreeShippingInfo(ctx);
        Map<String, Object> data = (Map<String,Object>)result.data;
        Test.stopTest();
        
        System.assert(result.success);
        System.assertEquals(String.valueOf(data.get('currentFreeShipping')),'600.00');
    }
    
    static testmethod void setFreeShippingInfoExceptionTest(){
        util.initCallContext();
        
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ctx.storefront = 'pdc';
        
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCCartItemsController.setFreeShippingInfo(ctx);
        System.assert(!result.success);
    }
    
    static testmethod void splitCartItemTest(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_Product__c prod = util.getProduct();
        ccrz__E_Product__c updateProd = [SELECT Id, Salespack__c FROM ccrz__E_Product__c WHERE Id =:prod.Id];
        updateProd.Salespack__c = 6;
        update updateProd;
        
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        ctx.currentCartId = String.valueOf(cart.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);
        Test.startTest();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.FulFillMentType__c = 'PickUp';
        update cartItem;
        List<ccrz__E_CartItem__c> cartItems = [SELECT Id,ccrz__SubAmount__c, ccrz__Price__c,ccrz__Quantity__c,ccrz__OriginalQuantity__c FROM ccrz__E_CartItem__c WHERE Id =: cartItem.Id];
        ccrz__E_CartItem__c updatedCartItem = cartItems.get(0);
        updatedCartItem.ccrz__Price__c = 300;
        updatedCartItem.ccrz__Quantity__c = 12;
        updatedCartItem.ccrz__SubAmount__c = 1200;
        updatedCartItem.ccrz__OriginalQuantity__c = 12;
        update updatedCartItem;
        
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCCartItemsController.splitCartItem(ctx,String.valueOf(updatedCartItem.Id));
        Map<String, Object> data = (Map<String,Object>)result.data;
        Test.stopTest();
        ccrz__E_CartItem__c resultCartItem = (ccrz__E_CartItem__c)data.get('newCartItem');
        ccrz__E_CartItem__c resultCartItem2 = (ccrz__E_CartItem__c)data.get('originalCartItem');
        
        //  List<ccrz__E_CartItem__c> qitems = [SELECT Id,ccrz__Price__c FROM ccrz__E_CartItem__c WHERE Id =: resultCartItem.Id];
        //  List<ccrz__E_CartItem__c> qitems2 = [SELECT Id,ccrz__Price__c FROM ccrz__E_CartItem__c WHERE Id =: resultCartItem2.Id];
        
        //   ccrz__E_CartItem__c qitem = qitems.get(0);
        //   ccrz__E_CartItem__c qitem2 = qitems2.get(0);
        //   System.assert(result.success);
        //   System.assertEquals(qitem.ccrz__Price__c,qitem2.ccrz__Price__c);
    }
    
    static testmethod void splitCartItemNoSalespackTest(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        ctx.currentCartId = String.valueOf(cart.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);
        Test.startTest();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.FulFillMentType__c = 'PickUp';
        update cartItem;
        List<ccrz__E_CartItem__c> cartItems = [SELECT Id,ccrz__SubAmount__c, ccrz__Price__c,ccrz__Quantity__c,ccrz__OriginalQuantity__c FROM ccrz__E_CartItem__c WHERE Id =: cartItem.Id];
        ccrz__E_CartItem__c updatedCartItem = cartItems.get(0);
        updatedCartItem.ccrz__Price__c = 300;
        updatedCartItem.ccrz__Quantity__c = 12;
        updatedCartItem.ccrz__SubAmount__c = 1200;
        updatedCartItem.ccrz__OriginalQuantity__c = 12;
        update updatedCartItem;
        
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCCartItemsController.splitCartItem(ctx,String.valueOf(updatedCartItem.Id));
        Map<String, Object> data = (Map<String,Object>)result.data;
        Test.stopTest();
        ccrz__E_CartItem__c resultCartItem = (ccrz__E_CartItem__c)data.get('newCartItem');
        ccrz__E_CartItem__c resultCartItem2 = (ccrz__E_CartItem__c)data.get('originalCartItem');
        
        //  List<ccrz__E_CartItem__c> qitems = [SELECT Id,ccrz__Price__c FROM ccrz__E_CartItem__c WHERE Id =: resultCartItem.Id];
        //   List<ccrz__E_CartItem__c> qitems2 = [SELECT Id,ccrz__Price__c FROM ccrz__E_CartItem__c WHERE Id =: resultCartItem2.Id];
        
        //   ccrz__E_CartItem__c qitem = qitems.get(0);
        //  ccrz__E_CartItem__c qitem2 = qitems2.get(0);
        //  System.assert(result.success);
        //   System.assertEquals(qitem.ccrz__Price__c,qitem2.ccrz__Price__c);
    }
    
    
    static testmethod void splitCartItemExceptionTest(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        ctx.currentCartId = String.valueOf(cart.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);
        Test.startTest();
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.ccrz__Price__c = 300;
        cartItem.ccrz__Quantity__c = 4;
        cartItem.ccrz__SubAmount__c = 1200;
        cartItem.FulFillMentType__c = 'PickUp';
        update cartItem;
        
        ccrz.cc_RemoteActionResult result = null;
        result = CCPDCCartItemsController.splitCartItem(ctx,String.valueOf(cartItem.Id));
        Map<String, Object> data = (Map<String,Object>)result.data;
        Test.stopTest();
        System.assert(!result.success);
    }
    
    static testmethod void setCurrentCartQuantityAndBackorderTest(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        ctx.currentCartId = String.valueOf(cart.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);
        
        ccrz__E_CartItem__c item = util.getCartItem();
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        
        ccrz.cc_RemoteActionResult result = null;
        
        
        String data = '[{"id":"'+item.Id+'","totalInv":140,"productId":"'+product.Id+'","backorderQuantity":21,"price":159.12}]';
        Test.startTest();
        result = CCPDCCartItemsController.setCurrentCartQuantityAndBackorder(ctx, data);
        //System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);
        
    }
    
    static testmethod void setCurrentCartQuantityAndBackorderInputNullTest(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        ctx.currentCartId = String.valueOf(cart.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);
        
        ccrz__E_CartItem__c item = util.getCartItem();
        List<ccrz__E_Product__c> products = [SELECT Id, Name, ccrz__SKU__c FROM ccrz__E_Product__c WHERE Name = 'Product 1'];
        ccrz__E_Product__c product = products.get(0);
        
        ccrz.cc_RemoteActionResult result = null;
        
        
        String data = '[]';
        Test.startTest();
        result = CCPDCCartItemsController.setCurrentCartQuantityAndBackorder(ctx, data);
        //System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);
        
    }
    
    static testmethod void fetchUpdatedCartItemsTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        ccrz.cc_RemoteActionResult result = null;
        
        Test.startTest();
        result = CCPDCCartItemsController.fetchUpdatedCartItems(ctx, false, 'AVI');
        //System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);
        
    }
    
    
    static testmethod void fetchUpdatedCartItemsSalesPackTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        ccrz__E_Product__c product = util.getProduct();
        
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.ccrz__Quantity__c = 8;
        update cartItem;
        
        ccrz__E_Product__c updateProd = [SELECT Id,Salespack__c FROM ccrz__E_Product__c WHERE Id =:product.Id];
        updateProd.Salespack__c = 4;
        update updateProd;
        
        ccrz.cc_RemoteActionResult result = null;
        
        Test.startTest();
        result = CCPDCCartItemsController.fetchUpdatedCartItems(ctx, false, 'AVI');
        //System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);
        
    }
    
    static testmethod void fetchUpdatedCartItemsSalesPackGreatThanQtyTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        ccrz__E_Product__c product = util.getProduct();
        
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.ccrz__Quantity__c = 2;
        update cartItem;
        
        ccrz__E_Product__c updateProd = [SELECT Id,Salespack__c FROM ccrz__E_Product__c WHERE Id =:product.Id];
        updateProd.Salespack__c = 4;
        update updateProd;
        
        ccrz.cc_RemoteActionResult result = null;
        
        Test.startTest();
        result = CCPDCCartItemsController.fetchUpdatedCartItems(ctx, false, 'AVI');
        //System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);
        
    }
    static testmethod void fetchUpdatedCartItemsWillCallSalespackTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        ccrz.cc_RemoteActionResult result = null;
        
        ccrz__E_Product__c product = util.getProduct();
        ccrz__E_Product__c updateProd = [SELECT Id,Salespack__c FROM ccrz__E_Product__c WHERE Id =:product.Id];
        updateProd.Salespack__c = 4;
        update updateProd;
        
        Test.startTest();
        result = CCPDCCartItemsController.fetchUpdatedCartItems(ctx, true, 'AVI');
        //System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);
        
        
    }
    
    static testmethod void fetchUpdatedCartItemsWillCallTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        ccrz.cc_RemoteActionResult result = null;
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        
        cart.CC_FP_Is_Will_Call__c = true;
        cart.CC_FP_Is_Ship_Remainder__c = false;
        update cart;
        Test.startTest();
        result = CCPDCCartItemsController.fetchUpdatedCartItems(ctx, true, 'AVI');
        //System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);
        
    }
    
    static testmethod void fetchUpdatedCartItemsProductNotReleased(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        ccrz.cc_RemoteActionResult result = null;
        ccrz__E_Cart__c cart = util.getCart();
        ccrz__E_Product__c product = util.createProduct('Product x', 'product-0199x');
        insert product;
        
        ccrz__E_CartItem__c newCartItem = util.createCartItem(cart, 10.99 , 4, product, null);
        newCartItem.ccrz__CartItemId__c = 'cartitem12345';
        insert newCartItem;
        
        product.ccrz__ProductStatus__c = 'Not Orderable';
        update product;        
        
        cart.CC_FP_Is_Will_Call__c = false;
        cart.CC_FP_Is_Ship_Remainder__c = false;
        update cart;
        
        Test.startTest();
        result = CCPDCCartItemsController.fetchUpdatedCartItems(ctx, true, 'AVI');        
        Test.stopTest();
        
        System.assert(result != null);
        System.assert(cart.ccrz__E_CartItems__r != null);         
        
    }    
    
    static testmethod void fetchUpdatedCartItemsWillCallLessThanInventoryTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        ccrz.cc_RemoteActionResult result = null;
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        ccrz__E_Product__c product = util.getProduct();
        ccrz__E_Product__c updateProdone = [SELECT Id,Salespack__c FROM ccrz__E_Product__c WHERE Id =:product.Id];
        updateProdone.Salespack__c = 1;
        update updateProdone;
        
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        
        
        ccrz__E_Product__c newproduct = util.createProduct('Product 3', 'product-0199');
        newproduct.Salespack__c = 4;
        insert newproduct;
        
        //ccrz__E_Product__c updateProd = [SELECT Id,Salespack__c FROM ccrz__E_Product__c WHERE Id =:newproduct.Id];
        //updateProd.Salespack__c = 2;
        //update updateProd;
        
        ccrz__E_CartItem__c newCartItem = util.createCartItem(cart, 10.99 , 4, newproduct, null);
        newCartItem.ccrz__CartItemId__c = 'cartitem12345';
        insert newCartItem;
        
        ccrz__E_CartItem__c newCartItem2 = util.createCartItem(cart, 12.99 , 4, newproduct, null);
        newCartItem2.ccrz__CartItemId__c = 'cartitem1234566';
        insert newCartItem2;
        
        
        cartItem.ccrz__Quantity__c = 2;
        update cartItem;
        
        Location__c loc = util.createLocation('loc1');
        insert loc;
        
        ccrz__E_ProductInventoryItem__c theinv = util.createProductInventoryItem(newproduct.Id, 'inventory-1',loc,10);
        insert theinv;
        
        cart.CC_FP_Is_Will_Call__c = true;
        cart.CC_FP_Is_Ship_Remainder__c = false;
        update cart;
        
        Test.startTest();
        result = CCPDCCartItemsController.fetchUpdatedCartItems(ctx, true, 'AVI');
        
        
        //System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);
        
    }
    
    static testmethod void fetchCoreChargesTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        ccrz__E_CartItem__c item = util.getCartItem();
        item.CC_FP_QTY_Shipping__c = 1;
        
        update item;
        ccrz__E_Product__c product = util.getProduct();
        ccrz__E_Product__c requiredProduct = util.createRequiredProduct(product);
        
        CC_FP_Required_Cart_Item__c requiredItem = new CC_FP_Required_Cart_Item__c(
            CC_Cart__c = cart.Id,
            CC_Parent_Cart_Item__c = item.Id,
            Quantity__c = 1,
            Price__c = 5.00,
            SubAmount__c = 5.00,
            CC_Product__c = product.Id
        );
        insert requiredItem; 
        
        ccrz.cc_RemoteActionResult result = null;
        
        Test.startTest();
        result = CCPDCCartItemsController.fetchCoreCharges(ctx);
        //System.debug(result);
        Test.stopTest();
        
        System.assert(result != null);
        
    }
    static testmethod void fetchProductRestrictionsTest(){
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        util.initCallContext();
        ccrz__E_Cart__c cart = util.getCart();
        Account a = util.getAccount();
        ctx.storefront = 'pdc';
        ctx.currentCartId = String.valueOf(cart.ccrz__EncryptedId__c);
        ctx.effAccountId = String.valueOf(a.Id);
        
        ccrz.cc_RemoteActionResult result = null;
        
        Test.startTest();
        result = CCPDCCartItemsController.fetchProductRestrictions(ctx);
        Test.stopTest();
        
        
    }
    
    static testmethod void checkExistOrderListNameTest(){
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        String wishlistName = 'wishlist';
        ccrz.cc_RemoteActionResult result = null;
        Test.startTest();
        result = CCPDCCartItemsController.checkExistWishlistName(ctx, wishlistName);
        //System.debug(result);
        Test.stopTest();
        System.assert(result != null);
        
    } 
    
    static testmethod void testGetBackorderCartItems() {
        ccrz__E_CartItem__c item = [SELECT Id,ccrz__Quantity__c FROM ccrz__E_CartItem__c];
        item.ccrz__Quantity__c = 50;
        update item;
        
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz.cc_RemoteActionResult result = null;
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        cart.ccrz__CartType__c = 'BackOrder';
        update cart;
        result = CCPDCCartItemsController.checkBackorder(ctx);
        System.assert(result != null);
    }
    // static testmethod void updateQtyPricingCallTest(){
    //     util.initCallContext();
    //     ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
    //     String data = '[{ "itemId":"a7H1k0000006YFtEAM", "poolNumber":"682","partNumber":"3116"},{ "itemId":"a7H1k0000006YFtEAM", "poolNumber":"682","partNumber":"3116"},{ "itemId":"a7H1k0000006YFtEAM", "poolNumber":"682","partNumber":"3116"}]';
    //     //String data = '["{"itemId":"a7H1k0000006YFtEAM", "partNumber":";"3116","poolNumber":"681"}"]';
    //    ccrz.cc_RemoteActionResult result = null;
    //     Test.startTest();
    //     result = CCPDCCartItemsController.updateQtyPricingCall(ctx, data);
    //     Test.stopTest();
    //     System.assert(result != null);
    
    //  } 
    static testmethod void fetchFPUpdatedCartItemsTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        Location__c loc = util.createLocation('loc1');
        insert loc;
        ccrz.cc_CallContext.currPageParameters.put('branchLoc','loc1');
        
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        result = CCPDCCartItemsController.fetchFPUpdatedCartItems(ctx);
        System.assert(result != null);
    }
    
    static testmethod void fetchFPUpdatedCartItemsPickupTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        Location__c loc = util.createLocation('loc1');
        insert loc;
        ccrz.cc_CallContext.currPageParameters.put('branchLoc','loc1');
        
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.FulFillMentType__c = 'PickUp';
        cartItem.ccrz__Quantity__c = 2;
        update cartItem;
        
        
        
        
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        result = CCPDCCartItemsController.fetchFPUpdatedCartItems(ctx);
        System.assert(result != null);
    }
    
    static testmethod void fetchFPUpdatedCartItemsQuantityLTTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        Location__c loc = util.createLocation('loc1');
        insert loc;
        ccrz.cc_CallContext.currPageParameters.put('branchLoc','loc1');
        
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        
        ccrz__E_CartItem__c cartItem = util.getCartItem();
        cartItem.FulFillMentType__c = 'PickUp';
        cartItem.ccrz__Quantity__c = 0;
        
        update cartItem;
        
        
        
        
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        result = CCPDCCartItemsController.fetchFPUpdatedCartItems(ctx);
        System.assert(result != null);
    }
    
    static testmethod void fetchFPUpdatedCartItemsQuantityGTTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        Location__c loc = util.createLocation('loc1');
        insert loc;
        ccrz.cc_CallContext.currPageParameters.put('branchLoc','loc1');
        
        ccrz__E_Cart__c cart = util.getCart();
        cart.CC_FP_Is_Will_Call__c = True;
        cart.CC_FP_Is_Ship_Remainder__c = False;
        update cart;
        
        ccrz__E_Product__c newproduct = util.createProduct('Product 3', 'product-0199');
        newproduct.Salespack__c = 4;
        newproduct.ccrz__SKU__c = 'testSKU';
        insert newproduct;
        
        
        ccrz__E_CartItem__c newCartItem = util.createCartItem(cart, 10.99 , 4, newproduct, null);
        newCartItem.ccrz__CartItemId__c = 'cartitem12345';
        newCartItem.ccrz__Cart__c = cart.Id;
        insert newCartItem;
        
        ccrz__E_Product__c pdt = util.getproduct();
        pdt.ccrz__SKU__c = 'AIR-6951';
        update pdt;
        
        ccrz__E_CartItem__c newCartItem2 = util.createCartItem(cart, 100.99 , 100000, pdt, null);
        newCartItem2.ccrz__CartItemId__c = 'cartitem123';
        newCartItem2.ccrz__Cart__c = cart.Id;
        newCartItem2.FulFillMentType__c = 'PickUp';
        insert newCartItem2;
        
        
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        result = CCPDCCartItemsController.fetchFPUpdatedCartItems(ctx);
        System.assert(result != null);
    }
    
    static testmethod void checkZeroPriceCartItemsTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        result = CCPDCCartItemsController.checkZeroPriceCartItems(ctx);
        System.assert(result != null);
    }
    static testmethod void checkBranchSupportedFFTYpeTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        
        result = CCPDCCartItemsController.checkBranchSupportedFFTYpe(ctx);
        System.assert(result != null);
    }
    
    static testmethod void updateCartFPLocationTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Location__c loc = util.createLocation('loc1');
        insert loc;
        result = CCPDCCartItemsController.updateCartFPLocation(ctx,'loc1');
        System.assert(result != null);
    }
    
    static testmethod void fetchFleetPrideProductRestrictionsTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Location__c loc = util.createLocation('loc1');
        insert loc;
        result = CCPDCCartItemsController.fetchFleetPrideProductRestrictions(ctx);
        System.assert(result != null);
    }
    
    static testmethod void getFPProductInventoriesTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        Location__c loc = util.createLocation('loc1');
        insert loc;
        ccrz.cc_CallContext.currCartId = Util.getCart().Id;
        result = CCPDCCartItemsController.getFPProductInventories(ctx);
        System.assert(result != null);
    }
    
    static testmethod void changeFFTypeFPTest() {
        ccrz.cc_RemoteActionResult  result = null;
        util.initCallContext();
        ccrz.cc_RemoteActionContext ctx = util.getRemoteActionContext();
        ccrz__E_cartItem__c cartItem = Util.getCartItem();
        ccrz.cc_CallContext.currCartId = Util.getCart().Id;
        result = CCPDCCartItemsController.changeFFTypeFP(ctx,cartItem.Id,'PickUp');
        System.assert(result != null);
    }
    
    
}